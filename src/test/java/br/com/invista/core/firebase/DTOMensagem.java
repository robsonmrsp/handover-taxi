package br.com.invista.core.firebase;

public class DTOMensagem {

	private int idCorrida;
	private long timestamp;

	public DTOMensagem(int idCorrida, long timestamp) {
		this.setIdCorrida(idCorrida);
		this.setTimestamp(timestamp);
	}

	public int getIdCorrida() {
		return idCorrida;
	}

	public void setIdCorrida(int idCorrida) {
		this.idCorrida = idCorrida;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

}
