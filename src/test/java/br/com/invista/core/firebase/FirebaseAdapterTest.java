package br.com.invista.core.firebase;

import java.util.Date;
import java.util.Random;

import org.junit.BeforeClass;
import org.junit.Test;

import br.com.invista.comunicacao.Action;
import br.com.invista.comunicacao.FirebaseAdapter;
import br.com.invista.comunicacao.NotificaoCorrida;

public class FirebaseAdapterTest {

	FirebaseAdapter firebaseAdapter = new FirebaseAdapter();

	@BeforeClass
	public static void setUp() throws Exception {
	}

	@Test
	public void deveSerPossivelEnviarMensagemParaFirebase() throws InterruptedException {
		System.out.println("Iniciando teste");

		firebaseAdapter.send(new NotificaoCorrida(new Random().nextInt(1000), new Date().getTime(), 1),
				/* em caso de sucesso */new Action() {
					public void execute() {
						System.out.println("Sucesso ao enviar mensagem");
					}
				}, /* em caso de erro ao enviar mensagem */new Action() {
					public void execute() {
						System.err.println("Não foi possivel enviar mensagem ");
					}
				});

		System.out.println("FirebaseAdapterTest.deveSerPossivelEnviarMensagemParaFirebase()");
		Thread.sleep(50000L);
	}

	// {
	// "rules": {
	// ".read": true,
	// ".write": true
	// }
	// }
}
