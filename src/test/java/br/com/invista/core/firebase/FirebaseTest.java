package br.com.invista.core.firebase;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import br.com.invista.comunicacao.Firebase;

public class FirebaseTest {
	public static class User {

		public String date_of_birth;
		public String full_name;
		public String nickname;

		public User(String date_of_birth, String full_name) {
			super();
			this.date_of_birth = date_of_birth;
			this.full_name = full_name;
			this.nickname = "";
		}

		public User(String date_of_birth, String full_name, String nickname) {
			super();
			this.date_of_birth = date_of_birth;
			this.full_name = full_name;
			this.nickname = nickname;
		}

	}

	@BeforeClass
	public static void setUp() throws Exception {
//		System.out.println("FirebaseTest.setUp()");
//		FirebaseTest.class.getResourceAsStream("jsetup.json");
//		FirebaseOptions options = new FirebaseOptions.Builder()//
//				.setServiceAccount(FirebaseTest.class.getResourceAsStream("/keys/jsetup.json"))//
//				.setDatabaseUrl("https://jsetup-4cd0a.firebaseio.com/").build();
//		FirebaseApp.initializeApp(options);
//		System.out.println("FirebaseTest.setUp()" + options);
	}

	@Test
	public void criaRef() throws InterruptedException {

//		System.out.println("FirebaseTest.criaRef()");
//		final FirebaseDatabase database = FirebaseDatabase.getInstance();
//		DatabaseReference ref = database.getReference("server/fireblog/");
//
//		DatabaseReference usersRef = ref.child("users");
//		// Map<String, User> users = new HashMap<String, User>();
//		// users.put("chico", new User("June 23, 1912", "Alan Turing"));
//		// users.put("ze", new User("December 9, 1906", "Grace Hopper"));
//		//
//		// usersRef.setValue(users);
//
//		usersRef.child("alanisawesome").setValue(new User("June 23, 1912", "Alan Turing"), new DatabaseReference.CompletionListener() {
//			@Override
//			public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//				if (databaseError != null) {
//					System.out.println("Data could not be saved " + databaseError.getMessage());
//				} else {
//					System.out.println("Data saved successfully.");
//				}
//			}
//		});
//
//		usersRef.child("gracehop").setValue(new User("December 9, 1906", "Grace Hopper"), new DatabaseReference.CompletionListener() {
//			@Override
//			public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//				if (databaseError != null) {
//					System.out.println("Data could not be saved " + databaseError.getMessage());
//				} else {
//					System.out.println("Data saved successfully.");
//				}
//			}
//		});
//
//		// Esse tempo é só pra manter a thead viva e esperar o envio da
//		// mensagem.
//		Thread.sleep(20000L);
//		// usersRef.push();
//		System.out.println("FirebaseTest.criaRef()");
	}
}
