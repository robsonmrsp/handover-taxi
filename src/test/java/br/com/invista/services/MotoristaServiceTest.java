package br.com.invista.services;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.hibernate.criterion.Restrictions;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.invista.model.Corrida;
import br.com.invista.model.Motorista;
import br.com.invista.service.CorridaService;
import br.com.invista.service.MotoristaService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:beans-teste.xml" })
@Transactional
@SuppressWarnings("unchecked")
/**
 * 
 * @author robson
 *
 */
@Ignore
public class MotoristaServiceTest {
	@Inject
	MotoristaService motoristaService;

	@Inject
	CorridaService corridaService;

	@Test
	public void motoristasPorConstraints() {

		List<Corrida> corridas = corridaService.all();
		List<Motorista> motoristas = new ArrayList<Motorista>();
		if (corridas.size() > 0) {
			// motoristas.addAll(motoristaService.getByCorridaConstraints(corridas.get(0)));
		}

		Assert.assertTrue("Deveria ser possivel encontrar uma soma mesmo que zero: ", motoristas.size() > 0);
	}
}
