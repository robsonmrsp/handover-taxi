package br.com.invista.services;

import javax.inject.Inject;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.invista.persistence.DaoCliente;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:beans-teste.xml" })
@Transactional
@SuppressWarnings("unchecked")
/**
 * 
 * @author robson
 *
 */
public class CentroCustoServiceTest {
	@Inject
	DaoCliente daoCliente;

	@Test
	public void dashboardPorFuncionario() {
		Criteria searchCriteria = daoCliente.criteria();
	
		searchCriteria.setProjection(Projections.sum("limiteMensal"));
		searchCriteria.createAlias("centroCusto", "centroCusto_");
		
		searchCriteria.add(Restrictions.eq("centroCusto_.id", 23));
		
		Double sum = (Double) searchCriteria.uniqueResult();

		Assert.assertNotNull("Deveria ser possivel encontrar uma soma mesmo que zero: ", sum);
	}
}
