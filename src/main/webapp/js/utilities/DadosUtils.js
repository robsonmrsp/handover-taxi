define(function(require) {
	// requires aqui

	var Dados = {
		BANCOS : [
			{
				codigo : '237',
				nome : 'BANCO BRADESCO S.A.'
			},
			{
				codigo : '101',
				nome : 'BANCO DO BRASIL S.A.'
			},
			{
				codigo : '745',
				nome : 'BANCO CITIBANK S.A.'
			},
			{
				codigo : '341',
				nome : 'BANCO ITAU S.A.'
			},
			{
				codigo : '004',
				nome : 'BANCO DO NORDESTE DO BRASIL S.A.'
			}, 
			{
				codigo : '389',
				nome : 'BANCO MERCANTIL DO BRASIL S.A.'
			},
			{
				codigo : '422',
				nome : 'BANCO SAFRA S.A.'
			},
			{
				codigo : '033',
				nome : 'BANCO SANTANDER S.A.'
			},
			{
				codigo : '104',
				nome : 'CAIXA ECONOMICA FEDERAL'
			},
//			{
//			codigo : '001',
//			nome : 'BANCO DO BRASIL S.A.'
//		}, {
//			codigo : '003',
//			nome : 'BANCO DA AMAZONIA S.A.'
//		}, {
//			codigo : '004',
//			nome : 'BANCO DO NORDESTE DO BRASIL S.A.'
//		}, {
//			codigo : '021',
//			nome : 'BANESTES S.A BANCO DO ESTADO DO ESPIRITO SANTO'
//		}, {
//			codigo : '024',
//			nome : 'BANCO DE PERNAMBUCO S.A.-BANDEPE'
//		}, {
//			codigo : '025',
//			nome : 'BANCO ALFA S/A'
//		}, {
//			codigo : '027',
//			nome : 'BANCO DO ESTADO DE SANTA CATARINA S.A.'
//		}, {
//			codigo : '029',
//			nome : 'BANCO BANERJ S.A.'
//		}, {
//			codigo : '031',
//			nome : 'BANCO BEG S.A.'
//		}, {
//			codigo : '033',
//			nome : 'BANCO SANTANDER S.A.'
//		}, {
//			codigo : '034',
//			nome : 'BANCO DO ESTADO DO AMAZONAS S.A.'
//		}, {
//			codigo : '036',
//			nome : 'BANCO BRADESCO BBI S.A.'
//		}, {
//			codigo : '037',
//			nome : 'BANCO DO ESTADO DO PARA S.A.'
//		}, {
//			codigo : '038',
//			nome : 'BANCO BANESTADO S.A.'
//		}, {
//			codigo : '039',
//			nome : 'BANCO DO ESTADO DO PIAUI S.A. - BEP'
//		}, {
//			codigo : '040',
//			nome : 'BANCO CARGILL S.A'
//		}, {
//			codigo : '041',
//			nome : 'BANCO DO ESTADO DO RIO GRANDE DO SUL S.A.'
//		}, {
//			codigo : '044',
//			nome : 'BANCO BVA SA'
//		}, {
//			codigo : '045',
//			nome : 'BANCO OPPORTUNITY S.A.'
//		}, {
//			codigo : '047',
//			nome : 'BANCO DO ESTADO DE SERGIPE S.A.'
//		}, {
//			codigo : '062',
//			nome : 'HIPERCARD BANCO MÚLTIPLO S.A'
//		}, {
//			codigo : '063',
//			nome : 'BANCO IBI S.A - BANCO MULTIPLO'
//		}, {
//			codigo : '065',
//			nome : 'LEMON BANK BANCO MÚLTIPLO S..A'
//		}, {
//			codigo : '066',
//			nome : 'BANCO MORGAN STANLEY S.A'
//		}, {
//			codigo : '069',
//			nome : 'BPN BRASIL BANCO MÚLTIPLO S.A.'
//		}, {
//			codigo : '070',
//			nome : 'BRB - BANCO DE BRASILIA S.A.'
//		}, {
//			codigo : '072',
//			nome : 'BANCO RURAL MAIS S.A.'
//		}, {
//			codigo : '073',
//			nome : 'BB BANCO POPULAR DO BRASL S.A.'
//		}, {
//			codigo : '074',
//			nome : 'BANCO J.SAFRA S.A.'
//		}, {
//			codigo : '075',
//			nome : 'BANCO CR2 S.A.'
//		}, {
//			codigo : '076',
//			nome : 'BANCO KDB DO BRASIL S.A.'
//		}, {
//			codigo : '096',
//			nome : 'BANCO BM&F DE SERVIÇOS DE LIQUIDAÇÃO E CUSTÓDIA S.A.'
//		}, {
//			codigo : '116',
//			nome : 'BANCO ÚNICO S.A.'
//		}, {
//			codigo : '151',
//			nome : 'BANCO NOSSA CAIXA S.A'
//		}, {
//			codigo : '175',
//			nome : 'BANCO FINASA S.A.'
//		}, {
//			codigo : '184',
//			nome : 'BANCO ITAÚ - BBA S.A.'
//		}, {
//			codigo : '204',
//			nome : 'BANCO BRADESCO CARTÕES S.A'
//		}, {
//			codigo : '208',
//			nome : 'BANCO UBS PACTUAL S.A.'
//		}, {
//			codigo : '212',
//			nome : 'BANCO MATONE S.A.'
//		}, {
//			codigo : '213',
//			nome : 'BANCO ARBI S.A.'
//		}, {
//			codigo : '214',
//			nome : 'BANCO DIBENS S.A.'
//		}, {
//			codigo : '215',
//			nome : 'BANCO ACOMERCIAL E DE INVESTIMENTO SUDAMERIS S.A.'
//		}, {
//			codigo : '217',
//			nome : 'BANCO JOHN DEERE S.A.'
//		}, {
//			codigo : '218',
//			nome : 'BANCO BONSUCESSO S.A.'
//		}, {
//			codigo : '222',
//			nome : 'BANCO CLAYON BRASIL S/A'
//		}, {
//			codigo : '224',
//			nome : 'BANCO FIBRA S.A.'
//		}, {
//			codigo : '225',
//			nome : 'BANCO BRASCAN S.A.'
//		}, {
//			codigo : '229',
//			nome : 'BANCO CRUZEIRO DO SUL S.A.'
//		}, {
//			codigo : '230',
//			nome : 'UNICARD BANCO MÚLTIPLO S.A.'
//		}, {
//			codigo : '233',
//			nome : 'BANCO GE CAPITAL S.A'
//		}, {
//			codigo : '237',
//			nome : 'BANCO BRADESCO S.A.'
//		}, {
//			codigo : '241',
//			nome : 'BANCO CLASSICO S.A.'
//		}, {
//			codigo : '243',
//			nome : 'BANCO MAXIMA S.A.'
//		}, {
//			codigo : '246',
//			nome : 'BANCO ABC-BRASIL S.A.'
//		}, {
//			codigo : '248',
//			nome : 'BANCO BOAVISTA INTERATLANTICO S.A.'
//		}, {
//			codigo : '249',
//			nome : 'BANCO INVESTCRED UNIBANCO S.A.'
//		}, {
//			codigo : '250',
//			nome : 'BANCO SCHAHIN'
//		}, {
//			codigo : '252',
//			nome : 'BANCO FININVEST S.A.'
//		}, {
//			codigo : '254',
//			nome : 'PARANÁ BANCO S.A.'
//		}, {
//			codigo : '263',
//			nome : 'BANCO CACIQUE S.A.'
//		}, {
//			codigo : '265',
//			nome : 'BANCO FATOR S.A.'
//		}, {
//			codigo : '266',
//			nome : 'BANCO CEDULA S.A.'
//		}, {
//			codigo : '300',
//			nome : 'BANCO DE LA NACION ARGENTINA'
//		}, {
//			codigo : '318',
//			nome : 'BANCO BMG S.A.'
//		}, {
//			codigo : '320',
//			nome : 'BANCO INDUSTRIAL E COMERCIAL S.A.'
//		}, {
//			codigo : '341',
//			nome : 'BANCO ITAU S.A.'
//		}, {
//			codigo : '356',
//			nome : 'BANCO ABN AMRO REAL S.A.'
//		}, {
//			codigo : '366',
//			nome : 'BANCO SOCIETE GENERALE BRASIL S.A'
//		}, {
//			codigo : '370',
//			nome : 'BANCO WESTLB DO BRASIL S.A.'
//		}, {
//			codigo : '376',
//			nome : 'BANCO J.P. MORGAN S.A.'
//		}, {
//			codigo : '389',
//			nome : 'BANCO MERCANTIL DO BRASIL S.A.'
//		}, {
//			codigo : '394',
//			nome : 'BANCO BMC S.A.'
//		}, {
//			codigo : '399',
//			nome : 'HSBC BANK BRASIL S.A.-BANCO MULTIPLO'
//		}, {
//			codigo : '409',
//			nome : 'UNIBANCO - UNIAO DE BANCOS BRASILEIROS S.A.'
//		}, {
//			codigo : '412',
//			nome : 'BANCO CAPITAL S.A.'
//		}, {
//			codigo : '422',
//			nome : 'BANCO SAFRA S.A.'
//		}, {
//			codigo : '453',
//			nome : 'BANCO RURAL S.A.'
//		}, {
//			codigo : '456',
//			nome : 'BANCO DE TOKYO-MITSUBISHI UFJ BRASIL S.A.'
//		}, {
//			codigo : '464',
//			nome : 'BANCO SUMITOMO MITSUI BRASILEIRO S.A.'
//		}, {
//			codigo : '477',
//			nome : 'CITIBANK N.A.'
//		}, {
//			codigo : '479',
//			nome : 'BANCO ITAUBANK S.A.'
//		}, {
//			codigo : '487',
//			nome : 'DEUTSCHE BANK S. A. - BANCO ALEMAO'
//		}, {
//			codigo : '488',
//			nome : 'JPMORGAN CHASE BANK, NATIONAL ASSOCIATION'
//		}, {
//			codigo : '492',
//			nome : 'ING BANK N.V.'
//		}, {
//			codigo : '494',
//			nome : 'BANCO DE LA REPUBLICA ORIENTAL DEL URUGUAY'
//		}, {
//			codigo : '495',
//			nome : 'BANCO DE LA PROVINCIA DE BUENOS AIRES'
//		}, {
//			codigo : '505',
//			nome : 'BANCO CREDIT SUISSE (BRASIL) S.A.'
//		}, {
//			codigo : '600',
//			nome : 'BANCO LUSO BRASILEIRO S.A.'
//		}, {
//			codigo : '604',
//			nome : 'BANCO INDUSTRIAL DO BRASIL S. A.'
//		}, {
//			codigo : '610',
//			nome : 'BANCO VR S.A.'
//		}, {
//			codigo : '611',
//			nome : 'BANCO PAULISTA S.A.'
//		}, {
//			codigo : '612',
//			nome : 'BANCO GUANABARA S.A.'
//		}, {
//			codigo : '613',
//			nome : 'BANCO PECUNIA S.A.'
//		}, {
//			codigo : '623',
//			nome : 'BANCO PANAMERICANO S.A.'
//		}, {
//			codigo : '626',
//			nome : 'BANCO FICSA S.A.'
//		}, {
//			codigo : '630',
//			nome : 'BANCO INTERCAP S.A.'
//		}, {
//			codigo : '633',
//			nome : 'BANCO RENDIMENTO S.A.'
//		}, {
//			codigo : '634',
//			nome : 'BANCO TRIANGULO S.A.'
//		}, {
//			codigo : '637',
//			nome : 'BANCO SOFISA S.A.'
//		}, {
//			codigo : '638',
//			nome : 'BANCO PROSPER S.A.'
//		}, {
//			codigo : '641',
//			nome : 'BANCO ALVORADA S.A'
//		}, {
//			codigo : '643',
//			nome : 'BANCO PINE S.A.'
//		}, {
//			codigo : '652',
//			nome : 'BANCO ITAÚ HOLDING FINANCEIRA S.A.'
//		}, {
//			codigo : '653',
//			nome : 'BANCO INDUSVAL S.A.'
//		}, {
//			codigo : '654',
//			nome : 'BANCO A.J. RENNER S.A.'
//		}, {
//			codigo : '655',
//			nome : 'BANCO VOTORANTIM S.A.'
//		}, {
//			codigo : '707',
//			nome : 'BANCO DAYCOVAL S.A.'
//		}, {
//			codigo : '719',
//			nome : 'BANIF - BANCO INTERNACIONAL DO FUNCHAL (BRASIL), S.A.'
//		}, {
//			codigo : '721',
//			nome : 'BANCO CREDIBEL S.A.'
//		}, {
//			codigo : '734',
//			nome : 'BANCO GERDAU S.A.'
//		}, {
//			codigo : '735',
//			nome : 'BANCO POTTENCIAL S.A.'
//		}, {
//			codigo : '738',
//			nome : 'BANCO MORADA S.A.'
//		}, {
//			codigo : '739',
//			nome : 'BANCO BGN S.A.'
//		}, {
//			codigo : '740',
//			nome : 'BANCO BARCLAYS S.A.'
//		}, {
//			codigo : '741',
//			nome : 'BANCO RIBEIRAO PRETO S.A.'
//		}, {
//			codigo : '743',
//			nome : 'BANCO SEMEAR S.A.'
//		}, {
//			codigo : '744',
//			nome : 'BANKBOSTON N.A.'
//		}, {
//			codigo : '745',
//			nome : 'BANCO CITIBANK S.A.'
//		}, {
//			codigo : '746',
//			nome : 'BANCO MODAL S.A.'
//		}, {
//			codigo : '747',
//			nome : 'BANCO RABOBANK INTERNATIONAL BRASIL S.A.'
//		}, {
//			codigo : '748',
//			nome : 'BANCO COOPERATIVO SICREDI S.A.'
//		}, {
//			codigo : '749',
//			nome : 'BANCO SIMPLES S.A.'
//		}, {
//			codigo : '751',
//			nome : 'DRESDNER BANK BRASIL S.A. BANCO MULTIPLO.'
//		}, {
//			codigo : '752',
//			nome : 'BANCO BNP PARIBAS BRASIL S.A.'
//		}, {
//			codigo : '753',
//			nome : 'BANCO COMERCIAL URUGUAI S.A.'
//		}, {
//			codigo : '756',
//			nome : 'BANCO COOPERATIVO DO BRASIL S.A. - BANCOOB'
//		}, {
//			codigo : '757',
//			nome : 'BANCO KEB DO BRASIL S.A.'
//		} 
			]

	}

	return Dados;
})