/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var CorridaModel = BaseModel.extend({

		urlRoot : 'rs/crud/corridas',

		defaults : {
			id : null,
			telefone : '',
			dataCorrida : '',
			dataAgendamento : '',
			tipoPagamento : '',
			valor : '',
			observacao : '',
			statusCorrida : '',
			seqEmAtendimento : '',
			motoGrande : '',
			bau : '',
			padronizada : '',
			enderecoCorridas : null,
			distanciaMotoCorridas : null,
			atendente : null,
			motorista : null,
			cliente : null,
			contato : null,
			empresa : null,

		},
		getClientCorridaPorTelefone : function(options) {
			this.url = 'rs/crud/corridas/clienteCorrida'
			this.fetch(options);
		}

	});
	return CorridaModel;
});
