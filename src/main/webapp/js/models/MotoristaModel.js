/* generated: 18/10/2016 23:48:44 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var MotoristaModel = BaseModel.extend({

		urlRoot : 'rs/crud/motoristas',

		defaults : {
			id: null,
	    	nome : '',    	
	    	nomeReduzido : '',    	
	    	viatura : '',    	
	    	email : '',    	
	    	ddd : '',    	
	    	fone : '',    	
	    	statusMotorista : '',    	
	    	ddd2 : '',    	
	    	fone2 : '',    	
	    	dddWhatsapp : '',    	
	    	whatsapp : '',    	
	    	dataNascimento : '',    	
	    	rg : '',    	
	    	orgaoExpedidor : '',    	
	    	dataExpedicao : '',    	
	    	naturalidade : '',    	
	    	cpf : '',    	
	    	cnh : '',    	
	    	emissaoCnh : '',    	
	    	categoriaCnh : '',    	
	    	validadeCnh : '',    	
	    	foto1 : '',    	
	    	foto2 : '',    	
	    	foto3 : '',    	
	    	endereco : '',    	
	    	numeroEndereco : '',    	
	    	complemento : '',    	
	    	uf : '',    	
	    	cidade : '',    	
	    	bairro : '',    	
	    	cep : '',    	
	    	referenciaEndereco : '',    	
	    	mae : '',    	
	    	pai : '',    	
	    	conjuge : '',    	
	    	referenciaPessoal1 : '',    	
	    	foneReferencia1 : '',    	
	    	parentescoReferencia1 : '',    	
	    	referenciaPessoal2 : '',    	
	    	foneReferencia2 : '',    	
	    	parentescoReferencia2 : '',    	
	    	marcaVeiculo : '',    	
	    	modeloVeiculo : '',    	
	    	placaVeiculo : '',    	
	    	corVeiculo : '',    	
	    	anoVeiculo : '',    	
	    	renavam : '',    	
	    	chassi : '',    	
	    	categoria : '',    	
	    	imei : '',    	
	    	cartaoCredito : '',    	
	    	cartaoDebito : '',    	
	    	voucher : '',    	
	    	eticket : '',    	
	    	senha : '',    	
	    	padronizada : '',    	
	    	bau : '',    	
	    	motoGrande : '',
	    	observacao : '',
	    	latitude : '',
	    	longitude : '',
			punicaos : null,
			mensagemMotoristas : null,
			motoristaBloqueados : null,
			corridas : null,
			plantoes : null,
		
		}
	});
	return MotoristaModel;
});
