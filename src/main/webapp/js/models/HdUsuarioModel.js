/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var HdUsuarioModel = BaseModel.extend({

		urlRoot : 'rs/crud/hdUsuarios',

		defaults : {
			id: null,
	    	nome : '',    	
	    	senha : '',    	
	    	email : '',    	
	    	tipo : '',    	
	    	perfil : '',    	
	    	statusUsuario : '',    	
	    	datainclusao : '',    	
	    	usuarioinclusao : '',    	
			empresa : null,
		
		}
	});
	return HdUsuarioModel;
});
