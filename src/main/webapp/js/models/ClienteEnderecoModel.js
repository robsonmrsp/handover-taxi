/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var ClienteEnderecoModel = BaseModel.extend({

		urlRoot : 'rs/crud/enderecoFavoritos',

		defaults : {
			id: null,
	    	endereco : '',    	
	    	numero : '',    	
	    	complemento : '',    	
	    	uf : '',    	
	    	cidade : '',    	
	    	bairro : '',    	
	    	cep : '',    	
	    	referencia : '',    	
	    	tipo : '',    	
	    	favorito : '',    	
			clienteEnderecoCoords : null,
			cliente : null,
		
		}
	});
	return ClienteEnderecoModel;
});
