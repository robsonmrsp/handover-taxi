/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var EnderecoCorridaModel = BaseModel.extend({

		urlRoot : 'rs/crud/enderecoCorridas',

		defaults : {
			id: null,
	    	endereco : '',
	    	logradouro : '',
	    	numero : '',    	
	    	complemento : '',    	
	    	tarefa : '',    	
	    	bairro : '',    	
	    	contato : '',    	
	    	uf : '',    	
	    	cidade : '',    	
	    	cep : '',    	
	    	referencia : '',    	
	    	descricao : '',    	
	    	ordem : '',    	
	    	latitude : '',    	
	    	longitude : '',    	
	    	enderecoFormatado : '',    	
	    	origem : '',    	
			eventos : null,
			corrida : null,
		
		}
	});
	return EnderecoCorridaModel;
});
