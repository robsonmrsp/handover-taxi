/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var ClienteModel = BaseModel.extend({

		urlRoot : 'rs/crud/clientes',

		defaults : {
			id: null,
	    	nome : '',    	
	    	email : '',
	    	username : '',   
	    	senha : '',    	
	    	tipo : '',    	
	    	telefone : '',    	
	    	inativo : '',    	
	    	usuarioCadastro : '',    	
	    	dataCadastro : '',    	
	    	matricula : '',    	
	    	limiteMensal : '',    	
	    	cnpjCpf : '',    	
	    	cnpj : '',    	
	    	cpf : '',    	
	    	autorizaEticket : '',    	
	    	observacao : '',    	
	    	enderecosFavoritos : null,
			telefonesFavoritos : null,
			motoristaBloqueados : null,
			empresa : null,
			centroCusto : null,
		
		}
	});
	return ClienteModel;
});
