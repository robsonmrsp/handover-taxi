/* generated: 04/11/2016 11:29:27 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var StatusTrackerModel = BaseModel.extend({

		urlRoot : 'rs/crud/statusTrackers',

		defaults : {
			id: null,
	    	latitude : '',    	
	    	longitude : '',    	
	    	timestamp : '',    	
	    	speed : '',    	
	    	accuracy : '',    	
	    	direction : '',    	
	    	altitude : '',    	
	    	bateryLevel : '',    	
	    	gpsEnabled : '',    	
	    	wifiEnabled : '',    	
	    	mobileEnabled : '',    	
			motorista : null,
		
		}
	});
	return StatusTrackerModel;
});
