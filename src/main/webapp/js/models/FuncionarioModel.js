/* generated: 18/10/2016 01:22:05 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FuncionarioModel = BaseModel.extend({

		urlRoot : 'rs/crud/clientes',

		defaults : {
			id : null,
			nome : '',
			email : '',
			senha : '',
			tipo : '',
			inativo : '',
			usuarioCadastro : '',
			dataCadastro : '',
			matricula : '',
			limiteMensal : '',
			cnpjCpf : '',
			cnpj : '',
			cpf : '',
			autorizaEticket : '',
			observacao : '',
			clienteEnderecos : null,
			clienteFones : null,
			motoristaBloqueados : null,
			empresa : null,
			centroCusto : null,
			telefone : '',

		}
	});
	return FuncionarioModel;
});
