/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var CorridaEnderecoModel = BaseModel.extend({

		urlRoot : 'rs/crud/corridaEnderecos',

		defaults : {
			id: null,
	    	enderecoOrigem : '',    	
	    	numeroOrigem : '',    	
	    	complementoOrigem : '',    	
	    	bairroOrigem : '',    	
	    	ufOrigem : '',    	
	    	cidadeOrigem : '',    	
	    	cepOrigem : '',    	
	    	referenciaOrigem : '',    	
	    	enderecoDestino : '',    	
	    	numeroDestino : '',    	
	    	complementoDestino : '',    	
	    	bairroDestino : '',    	
	    	ufDestino : '',    	
	    	cidadeDestino : '',    	
	    	cepDestino : '',    	
	    	referenciaDestino : '',    	
	    	descricao : '',    	
			corrida : null,
		
		}
	});
	return CorridaEnderecoModel;
});
