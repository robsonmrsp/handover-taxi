/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var EmpresaModel = BaseModel.extend({

		urlRoot : 'rs/crud/empresas',

		defaults : {
			id: null,
	    	nomeFantasia : '',    	
	    	cnpj : '',    	
	    	contrato : '',    	
	    	razaoSocial : '',    	
	    	endereco : '',    	
	    	numeroEndereco : '',    	
	    	complemento : '',    	
	    	uf : '',    	
	    	cidade : '',    	
	    	bairro : '',    	
	    	cep : '',    	
	    	referenciaEndereco : '',    	
	    	ddd : '',    	
	    	fone : '',    	
	    	utilizaVoucher : '',    	
	    	utilizaEticket : '',    	
	    	observacaoTaxista : '',    	
	    	observacaoCentral : '',    	
	    	observacaoFinanceiro : '',    	
	    	percentualDesconto : '',    	
	    	inscricaoMunicipal : '',    	
	    	inscricaoEstadual : '',    	
	    	emiteNf : '',    	
	    	percentualIss : '',    	
	    	percentualIrf : '',    	
	    	percentualInss : '',    	
	    	diaVencimento : '',    	
	    	percentualMotorista : '',    	
	    	banco : '',    	
	    	agencia : '',    	
	    	conta : '',    	
	    	email : '',    	
	    	usuarioCadastro : '',    	
	    	dataCadastro : '',    	
	    	statusEmpresa : '',
	    	nomeCliente1 : '',
	    	nomeCliente2 : '',
	    	foneCliente1 : '',
	    	foneCliente2 : '',
	    	nomeFornecedor1 : '',
	    	nomeFornecedor2 : '',
	    	foneFornecedor1 : '',
	    	foneFornecedor2 : '',
			centroCustos : null,
			hdUsuarios : null,
			contatos : null,
			socios : null,
			telefoneFavoritos : null,
			faixaVouchers : null,
			clientes : null,
		
		}
	});
	return EmpresaModel;
});
