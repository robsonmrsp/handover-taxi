/* generated: 03/09/2016 22:18:34 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var UserModel = BaseModel.extend({

		urlRoot : 'rs/crud/users',

		defaults : {
			id : null,
			name : '',
			username : '',
			password : '',
			enable : '',
	    	
			email : '',    	
	    	tipo : '',    	
	    	statusUsuario : '',
	    	
			image : '',
			roles : null,
			perfil : null,
			empresa : null,
			owner : null,
			
			// MAs issonão vai servir pra muita coisa não....
			// conversa com o Edilson, o perfil já é definido em Roles.
			// roda lá
			// Deixa eu te mostrar a outra view para voce paralelizar

		},
		getCurrentUser : function(options) {
			this.url = 'rs/crud/users/current';
			this.fetch(options)
		},
		changePassword : function(options) {
			this.url = 'rs/crud/users/changePassword';
			this.save({}, options);
		},
		changeUser : function(options) {
			this.url = 'rs/crud/users/changeUser/' + this.id;
			this.save({}, options);
		}

	});
	return UserModel;
});
