define(function(require) {
	var UserModel = require('models/UserModel');
	var cliente = new UserModel().fetch({
		async: false,
		url: 'rs/crud/users/current',
		success: function(_coll, _resp, _opt) {
			console.info('Consulta para coletar user');
		},
		error: function(_coll, _resp, _opt) {
			console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
		},
	}).responseJSON;
	return cliente;
});
