/* generated: 28/11/2016 23:13:05 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseCollection = require('collections/BaseCollection');
	var MotivoPunicaoModel = require('models/MotivoPunicaoModel');
	var BaseCollection = require('collections/BaseCollection');	
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var MotivoPunicaoCollection = BaseCollection.extend({
		model : MotivoPunicaoModel,
		
		url : 'rs/crud/motivoPunicaos/all',
		
	});
	return MotivoPunicaoCollection;
});