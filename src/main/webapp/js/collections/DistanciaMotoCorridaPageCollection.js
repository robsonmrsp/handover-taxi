/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var DistanciaMotoCorridaModel = require('models/DistanciaMotoCorridaModel');
	// End of "Import´s definition"
	
	var DistanciaMotoCorridasCollection = Backbone.PageableCollection.extend({

		model : DistanciaMotoCorridaModel,

		
		url : 'rs/crud/distanciaMotoCorridas',

		mode : 'server',
		state : {
			firstPage : 1,
			lastPage : null,
			currentPage : 1,
			pageSize :10,
			totalPages : null,
			totalRecords : null,
			sortKey : null,
			order : -1
		},

		queryParams : {
			totalPages : null,
			pageSize : "pageSize",
			totalRecords : null,
			sortKey : "orderBy",
			order : "direction",
			directions : {
				"-1" : "asc",
				"1" : "desc"
			}
		},
	});

	return DistanciaMotoCorridasCollection;
});