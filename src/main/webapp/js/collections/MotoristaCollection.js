/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseCollection = require('collections/BaseCollection');
	var MotoristaModel = require('models/MotoristaModel');
	var BaseCollection = require('collections/BaseCollection');	
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var MotoristaCollection = BaseCollection.extend({
		model : MotoristaModel,
		
		url : 'rs/crud/motoristas/all',
		
		motoristasOnline : function(options) {
			this.url = 'rs/crud/motoristas/motoristasOnline';
			this.fetch(options);
			
		},
	});
	
	return MotoristaCollection;
});