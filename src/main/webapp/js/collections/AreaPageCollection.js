/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var AreaModel = require('models/AreaModel');
	// End of "Import´s definition"

	var AreasCollection = Backbone.PageableCollection.extend({

		model : AreaModel,

		comparator : 'ordem',

		// Testar essa lógica
		movePraCima : function(model) {
			var index = this.indexOf(model);

			if (index > 0) {
				this.troca(index, index - 1);
			}
		},

		movePraBaixo : function(model) {
			var index = this.indexOf(model);

			if (index < this.models.length) {
				this.troca(index, index + 1);
			}
		},

		troca : function(indexA, indexB) {

			var modelA = this.models[indexA];
			var ordemA = modelA.get('ordem');

			var modelB = this.models[indexB];
			var ordemB = modelB.get('ordem');
			modelA.set('ordem', ordemB);
			modelB.set('ordem', ordemA);

//			this.models[indexA] = this.models.splice(indexB, 1, this.models[indexA])[0];

			console.log('descricao' + modelA.get('descricao') + ' - > ordem' + modelA.get('ordem'));
			console.log('descricao' + modelB.get('descricao') + ' - > ordem' + modelB.get('ordem'));
			this.sort();

		},
		url : 'rs/crud/areas',

		mode : 'server',
		state : {
			firstPage : 1,
			lastPage : null,
			currentPage : 1,
			pageSize : 10,
			totalPages : null,
			totalRecords : null,
			sortKey : null,
			order : -1
		},

		queryParams : {
			totalPages : null,
			pageSize : "pageSize",
			totalRecords : null,
			sortKey : "orderBy",
			order : "direction",
			directions : {
				"-1" : "asc",
				"1" : "desc"
			}
		},
	});

	return AreasCollection;
});