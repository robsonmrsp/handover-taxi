/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseCollection = require('collections/BaseCollection');
	var PaisModel = require('models/PaisModel');
	var BaseCollection = require('collections/BaseCollection');	
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var PaisCollection = BaseCollection.extend({
		model : PaisModel,
		
		url : 'rs/crud/paiss/all',
		
	});
	return PaisCollection;
});