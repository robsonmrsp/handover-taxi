/* generated: 28/11/2016 23:13:05 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var MotivoPunicaoModel = require('models/MotivoPunicaoModel');
	// End of "Import´s definition"
	
	var MotivoPunicaosCollection = Backbone.PageableCollection.extend({

		model : MotivoPunicaoModel,

		
		url : 'rs/crud/motivoPunicaos',

		mode : 'server',
		state : {
			firstPage : 1,
			lastPage : null,
			currentPage : 1,
			pageSize :10,
			totalPages : null,
			totalRecords : null,
			sortKey : null,
			order : -1
		},

		queryParams : {
			totalPages : null,
			pageSize : "pageSize",
			totalRecords : null,
			sortKey : "orderBy",
			order : "direction",
			directions : {
				"-1" : "asc",
				"1" : "desc"
			}
		},
	});

	return MotivoPunicaosCollection;
});