/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseCollection = require('collections/BaseCollection');
	var ClienteEnderecoModel = require('models/ClienteEnderecoModel');
	var BaseCollection = require('collections/BaseCollection');	
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var ClienteEnderecoCollection = BaseCollection.extend({
		model : ClienteEnderecoModel,
		
		url : 'rs/crud/enderecoFavoritos/all',
		
	});
	return ClienteEnderecoCollection;
});