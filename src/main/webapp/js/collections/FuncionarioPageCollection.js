/* generated: 18/10/2016 01:22:05 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var FuncionarioModel = require('models/FuncionarioModel');
	// End of "Import´s definition"
	
	var FuncionariosCollection = Backbone.PageableCollection.extend({

		model : FuncionarioModel,

		
		url : 'rs/crud/clientes',

		mode : 'server',
		state : {
			firstPage : 1,
			lastPage : null,
			currentPage : 1,
			pageSize :10,
			totalPages : null,
			totalRecords : null,
			sortKey : null,
			order : -1
		},

		queryParams : {
			totalPages : null,
			pageSize : "pageSize",
			totalRecords : null,
			sortKey : "orderBy",
			order : "direction",
			directions : {
				"-1" : "asc",
				"1" : "desc"
			}
		},
	});

	return FuncionariosCollection;
});