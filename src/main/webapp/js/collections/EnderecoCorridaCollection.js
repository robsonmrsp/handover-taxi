/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseCollection = require('collections/BaseCollection');
	var EnderecoCorridaModel = require('models/EnderecoCorridaModel');
	var BaseCollection = require('collections/BaseCollection');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var EnderecoCorridaCollection = BaseCollection.extend({
		model : EnderecoCorridaModel,

		url : 'rs/crud/enderecoCorridas/all',

		comparator : 'ordem',

		// Testar essa lógica
		movePraCima : function(model) {
			var index = this.indexOf(model);

			if (index > 0) {
				this.troca(index, index - 1);
			}
		},

		movePraBaixo : function(model) {
			var index = this.indexOf(model);

			if (index < this.models.length) {
				this.troca(index, index + 1);
			}
		},

		troca : function(indexA, indexB) {

			var modelA = this.models[indexA];
			var ordemA = modelA.get('ordem');

			var modelB = this.models[indexB];
			var ordemB = modelB.get('ordem');
			modelA.set('ordem', ordemB);
			modelB.set('ordem', ordemA);

			this.models[indexA] = this.models.splice(indexB, 1, this.models[indexA])[0];
			this.sort();
		}
	});
	return EnderecoCorridaCollection;
});