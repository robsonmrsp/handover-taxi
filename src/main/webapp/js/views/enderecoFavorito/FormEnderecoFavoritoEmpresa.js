/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var SuggestGmapBox = require('views/components/SuggestGmapBox');
	var EnderecoFavoritoModel = require('models/ClienteEnderecoModel');
	var EnderecoFavoritoCollection = require('collections/ClienteEnderecoCollection');
	var EnderecoFavoritoPageCollection = require('collections/ClienteEnderecoPageCollection');
	var FormEnderecoFavoritoEmpresaTemplate = require('text!views/enderecoFavorito/tpl/FormEnderecoFavoritoEmpresaTemplate.html');
	
	//Filter import
	var EmpresaModal = require('views/modalComponents/EmpresaModal');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormEnderecoFavoritoEmpresa = Marionette.LayoutView.extend({
		template : _.template(FormEnderecoFavoritoEmpresaTemplate),

		regions : {
			modalEmpresaRegion : '#empresaModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click 	.go-back-link' : 'goBack',
			'click #searchEmpresaModal' : 'showModalEmpresa',
		},
		
		ui : {
			// empresa
			inputEmpresaId : '#inputEmpresaId',
			inputEmpresaNomeFantasia : '#inputEmpresaNomeFantasia',
			inputEmpresaCnpj : '#inputEmpresaCnpj',
			inputEmpresaContrato : '#inputEmpresaContrato',
			inputEmpresaRazaoSocial : '#inputEmpresaRazaoSocial',
			
			// endereco
			inputId : '#inputId',
			inputEndereco : '#inputEndereco',
			inputNumero : '#inputNumero',
			inputComplemento : '#inputComplemento',
			inputUf : '#inputUf',
			inputCidade : '#inputCidade',
			inputBairro : '#inputBairro',
			inputCep : '#inputCep',
			inputReferencia : '#inputReferencia',
			inputTipo : '#inputTipo',
			inputFavorito : '#inputFavorito',
		
			// so o capiroto sabe
			inputEmpresaId : '#inputEmpresaId',
			inputEmpresaNome : '#inputEmpresaNome',
			form : '#formEmpresaEndereco',
		},

		initialize : function(options) {
			var that = this;
			this.empresa = options.empresa

			this.empresaModal = new EmpresaModal({
				onSelectModel : function(model) {
					that.onSelectEmpresa(model);
				},
			});
			this.on('show', function() {
				this.modalEmpresaRegion.show(this.empresaModal);		
				this.ui.inputCep.mask('99.999-999');
			
//				this.enderecoPartida = new SuggestGmapBox({
//					el : that.ui.inputEnderecoCombo,
//
//					onSelect : function(endereco) {
//						that.jsonValue = endereco;
//						that.model.set(endereco);
//						that.populaEndereco(endereco);
//					},
//				});
				
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});

				this.carregaEmpresa(this.empresa);
			});
		},

		carregaEmpresa : function(empresa) {
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));
			this.ui.inputEmpresaCnpj.val(empresa.get('cnpj'));
			this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
			this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));
		},

		goBack : function() {
			util.goPage('app/empresa/' + this.empresa.get('id') + '/enderecos');
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var empresaEndereco = that.getModel();

			if (this.isValid()) {
				empresaEndereco.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Endereco favorito salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/empresa/' + that.empresa.get('id') + '/enderecos');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro', _resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		clearForm : function() {
			util.clear('inputId');
			util.clear('inputEndereco'); 
			util.clear('inputNumero'); 
			util.clear('inputComplemento'); 
			util.clear('inputUf'); 
			util.clear('inputCidade'); 
			util.clear('inputBairro'); 
			util.clear('inputCep'); 
			util.clear('inputReferencia'); 
			util.clear('inputTipo'); 
			util.clear('inputFavorito'); 
			util.clear('inputClienteId');
			util.clear('inputClienteNome');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var empresaEndereco = that.model; 
			empresaEndereco.set({
				id: util.escapeById('inputId') || null,
		    	endereco : util.escapeById('inputEndereco'), 
		    	numero : util.escapeById('inputNumero'), 
		    	complemento : util.escapeById('inputComplemento'), 
		    	uf : util.escapeById('inputUf'), 
		    	cidade : util.escapeById('inputCidade'), 
		    	bairro : util.escapeById('inputBairro'),
				cep : util.onlyNumber(util.escapeById('inputCep')), // this is wrong
		    	referencia : util.escapeById('inputReferencia'), 
		    	tipo : util.escapeById('inputTipo'), 
		    	favorito : util.escapeById('inputFavorito'), 
				empresa : that.empresa.toJSON(),
			});
			return empresaEndereco;
		},
		 		
		showModalEmpresa : function() {
			// add more before the modal is open
			this.empresaModal.showPage();
		},

		onSelectEmpresa : function(empresa) {
			this.empresaModal.hidePage();	
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNome.val(empresa.get('nome'));		
		},
				
		
	});

	return FormEnderecoFavoritoEmpresa;
});