/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var EnderecoFavoritoModel = require('models/ClienteEnderecoModel');
	var EnderecoFavoritoCollection = require('collections/ClienteEnderecoCollection');
	var EnderecoFavoritoPageCollection = require('collections/ClienteEnderecoPageCollection');
	var PageEnderecoFavoritoEmpresaTemplate = require('text!views/enderecoFavorito/tpl/PageEnderecoFavoritoEmpresaTemplate.html');
	
	//Filter import
	var EmpresaModal = require('views/modalComponents/EmpresaModal');
	
	// End of "Import´s" definition

	var PageEnderecoFavoritoEmpresa = Marionette.LayoutView.extend({
		template : _.template(PageEnderecoFavoritoEmpresaTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			modalEmpresaRegion : '#empresaModal',
		},
		
		events : {
			'click 	#reset' : 'resetEmpresaEndereco',			
			'click #searchEmpresaModal' : 'showModalEmpresa',
			'keypress' : 'treatKeypress',
			'click  .novo-endereco' : 'novoEndereco',
			'click 	.voltar-empresa' : 'voltarEmpresa',

			'click 	.search-button' : 'searchEmpresaEndereco',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
			// empresa
			inputEmpresaId : '#inputEmpresaId',
			inputEmpresaNomeFantasia : '#inputEmpresaNomeFantasia',
			inputEmpresaCnpj : '#inputEmpresaCnpj',
			inputEmpresaContrato : '#inputEmpresaContrato',
			inputEmpresaRazaoSocial : '#inputEmpresaRazaoSocial',

			// endereco
			inputEndereco : '#inputEndereco',
			inputNumero : '#inputNumero',
			inputComplemento : '#inputComplemento',
			inputUf : '#inputUf',
			inputCidade : '#inputCidade',
			inputBairro : '#inputBairro',
			inputCep : '#inputCep',
			inputReferencia : '#inputReferencia',
			inputTipo : '#inputTipo',
			inputFavorito : '#inputFavorito',
		
			// so o capiroto sabe
			inputEmpresaId : '#inputEmpresaId',
			inputEmpresaNome : '#inputEmpresaNome',
			form : '#formEmpresaEnderecoFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchEmpresaEndereco();
	    	}
		},

		initialize : function(options) {
			var that = this;
			
			this.empresa = options.empresa;
			
			this.empresaEnderecos = new EnderecoFavoritoPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.empresaEnderecos,
			});

			this.counter = new Counter({
				collection : this.empresaEnderecos,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.empresaEnderecos,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});
			
			this.empresaModal = new EmpresaModal({
				onSelectModel : function(model) {
					that.onSelectEmpresa(model);
				},
			});
			this.on('show', function() {
				this.searchEmpresaEndereco();
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.modalEmpresaRegion.show(this.empresaModal);		
				this.carregaEmpresa(this.empresa);
			});
		},

		carregaEmpresa : function(empresa) {
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));
			this.ui.inputEmpresaCnpj.val(empresa.get('cnpj'));
			this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
			this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));
		},
		 
		searchEmpresaEndereco : function(){
			var that = this;

			
			if(!util.escapeById('inputEndereco')){
				this.empresaEnderecos.filterQueryParams = {
						endereco : util.escapeById('inputAvancadoEndereco'),
			    		numero : util.escapeById('inputNumero'),
			    		complemento : util.escapeById('inputComplemento'),
			    		uf : util.escapeById('inputUf'),
			    		cidade : util.escapeById('inputCidade'),
			    		bairro : util.escapeById('inputBairro'),
			    		cep : util.escapeById('inputCep'),
			    		referencia : util.escapeById('inputReferencia'),
			    		tipo : util.escapeById('inputTipo'),
			    		favorito : util.escapeById('inputFavorito'),
					    empresa : that.empresa.get('id'),
					}
			} else {
				this.empresaEnderecos.filterQueryParams = {
						endereco : util.escapeById('inputEndereco'),
						empresa : that.empresa.get('id'),
					}
			}
			
			this.empresaEnderecos.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid empresaEndereco');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		
		resetEmpresaEndereco : function(){
			this.empresaEnderecos.reset();
			util.clear('inputEmpresaId');
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "endereco",
				editable : false,
				sortable : true,
				label 	 : "Logradouro",
				cell 	 : "string",
			}, 
			{
				name : "numero",
				editable : false,
				sortable : true,
				label 	 : "Número",
				cell 	 : "string",
			}, 
			{
				name : "bairro",
				editable : false,
				sortable : true,
				label 	 : "Bairro",
				cell 	 : "string",
			}, 
			{
				name : "cep",
				editable : false,
				sortable : true,
				label 	 : "Cep",
				cell 	 : "string",
			}, 
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Empresa endereco',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Empresa endereco',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new EnderecoFavoritoModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.empresaEnderecos.remove(model);
							util.showSuccessMessage('Empresa endereco removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		novoEndereco : function() {
			util.goPage("app/empresa/" + this.empresa.get('id') + "/newEndereco");
		},

		voltarEmpresa: function() {
			util.goPage("app/empresas");
		},

		editModel : function(model) {
			util.goPage("app/empresa/" + this.empresa.get('id') + "/editEndereco/" + model.get('id'));
		},

		showModalEmpresa : function() {
			this.empresaModal.showPage();
		},
			
		onSelectEmpresa : function(empresa) {
			this.empresaModal.hidePage();	
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNome.val(empresa.get('nome'));		
		},
		

	});

	return PageEnderecoFavoritoEmpresa;
});
