/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');
	var CustomNumberCell = require('views/components/CustomNumberCell');
	var EnderecoFavoritoModel = require('models/ClienteEnderecoModel');
	var EnderecoFavoritoCollection = require('collections/ClienteEnderecoCollection');
	var EnderecoFavoritoPageCollection = require('collections/ClienteEnderecoPageCollection');
	var PageEnderecoFavoritoClienteTemplate = require('text!views/enderecoFavorito/tpl/PageEnderecoFavoritoClienteTemplate.html');
	//Filter import
	var ClienteModal = require('views/modalComponents/ClienteModal');
	// End of "Import´s" definition
	var PageEnderecoFavoritoCliente = Marionette.LayoutView.extend({
		template: _.template(PageEnderecoFavoritoClienteTemplate),
		regions: {
			gridRegion: '#grid',
			counterRegion: '#counter',
			paginatorRegion: '#paginator',
			modalClienteRegion: '#clienteModal',
		},
		events: {
			'click 	#reset': 'resetClienteEndereco',
			'click #searchClienteModal': 'showModalCliente',
			'keypress': 'treatKeypress',
			'click  .novo-endereco': 'novoEndereco',
			'click 	.voltar-cliente': 'voltarCliente',
			'click 	.search-button': 'searchClienteEndereco',
			'click .show-advanced-search-button': 'toggleAdvancedForm',
		},
		ui: {
			// cliente
			inputClienteNome: '#inputClienteNome',
			inputClienteEmail: '#inputClienteEmail',
			inputClienteTipo: '#inputClienteTipo',
			// endereco
			inputEndereco: '#inputEndereco',
			inputNumero: '#inputNumero',
			inputComplemento: '#inputComplemento',
			inputUf: '#inputUf',
			inputCidade: '#inputCidade',
			inputBairro: '#inputBairro',
			inputCep: '#inputCep',
			inputReferencia: '#inputReferencia',
			inputTipo: '#inputTipo',
			inputFavorito: '#inputFavorito',
			// so o capiroto sabe
			inputClienteId: '#inputClienteId',
			inputClienteNome: '#inputClienteNome',
			form: '#formClienteEnderecoFilter',
			advancedSearchForm: '.advanced-search-form',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		toggleAdvancedForm: function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},
		treatKeypress: function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchClienteEndereco();
			}
		},
		initialize: function(options) {
			var that = this;
			this.cliente = options.cliente;
			this.clienteEnderecos = new EnderecoFavoritoPageCollection();
			this.grid = new Backgrid.Grid({
				className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns: this.getColumns(),
				emptyText: "Sem registros",
				collection: this.clienteEnderecos,
			});
			this.counter = new Counter({
				collection: this.clienteEnderecos,
			});
			this.paginator = new Backgrid.Extension.Paginator({
				columns: this.getColumns(),
				collection: this.clienteEnderecos,
				className: ' paging_simple_numbers',
				uiClassName: 'pagination',
			});
			this.clienteModal = new ClienteModal({
				onSelectModel: function(model) {
					that.onSelectCliente(model);
				},
			});
			this.on('show', function() {
				this.searchClienteEndereco();
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.modalClienteRegion.show(this.clienteModal);
				this.carregaCliente(this.cliente);
				this.checkButtonAuthority();
			});
		},
		carregaCliente: function(cliente) {
			this.ui.inputClienteId.val(cliente.get('id'));
			this.ui.inputClienteNome.val(cliente.get('nome'));
			this.ui.inputClienteEmail.val(cliente.get('email'));
			this.ui.inputClienteTipo.val(cliente.get('tipo'));
		},
		searchClienteEndereco: function() {
			var that = this;
			if (!util.escapeById('inputEndereco')) {
				this.clienteEnderecos.filterQueryParams = {
					endereco: util.escapeById('inputAvancadoEndereco'),
					numero: util.escapeById('inputNumero'),
					complemento: util.escapeById('inputComplemento'),
					uf: util.escapeById('inputUf'),
					cidade: util.escapeById('inputCidade'),
					bairro: util.escapeById('inputBairro'),
					cep: util.escapeById('inputCep'),
					referencia: util.escapeById('inputReferencia'),
					tipo: util.escapeById('inputTipo'),
					favorito: util.escapeById('inputFavorito'),
					cliente: that.cliente.get('id'),
				}
			}
			else {
				this.clienteEnderecos.filterQueryParams = {
					endereco: util.escapeById('inputEndereco'),
					cliente: that.cliente.get('id'),
				}
			}
			this.clienteEnderecos.fetch({
				success: function(_coll, _resp, _opt) {
					console.info('Consulta para o grid clienteEndereco');
				},
				error: function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete: function() {},
			})
		},
		resetClienteEndereco: function() {
			this.clienteEnderecos.reset();
			util.clear('inputClienteId');
		},
		getColumns: function() {
			var that = this;
			var columns = [{
				name: "endereco",
				editable: false,
				sortable: true,
				label: "Logradouro",
				cell: "string",
			}, {
				name: "numero",
				editable: false,
				sortable: true,
				label: "Número",
				cell: "string",
			}, {
				name: "bairro",
				editable: false,
				sortable: true,
				label: "Bairro",
				cell: "string",
			}, {
				name: "cep",
				editable: false,
				sortable: true,
				label: "Cep",
				cell: "string",
			}, {
				name: "acoes",
				label: "Ações(Editar, Deletar)",
				sortable: false,
				cell: GeneralActionsCell.extend({
					buttons: that.getCellButtons(),
					context: that,
				})
			}];
			return columns;
		},
		getCellButtons: function() {
			var that = this;
			var buttons = this.checkGridButtonAuthority();
			return buttons;
		},
		deleteModel: function(model) {
			var that = this;
			var modelTipo = new EnderecoFavoritoModel({
				id: model.id,
			});
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success: function() {
							that.clienteEnderecos.remove(model);
							util.showSuccessMessage('Cliente endereco removido com sucesso!');
						},
						error: function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro', _resp);
						}
					});
				}
			});
		},
		novoEndereco: function() {
			util.goPage("app/cliente/" + this.cliente.get('id') + "/newEndereco");
		},
		voltarCliente: function() {
			util.goPage("app/clientes");
		},
		editModel: function(model) {
			util.goPage("app/cliente/" + this.cliente.get('id') + "/editEndereco/" + model.get('id'));
		},
		showModalCliente: function() {
			this.clienteModal.showPage();
		},
		onSelectCliente: function(cliente) {
			this.clienteModal.hidePage();
			this.ui.inputClienteId.val(cliente.get('id'));
			this.ui.inputClienteNome.val(cliente.get('nome'));
		},
		// vitoriano : chunck : check grid buttons authority
		checkGridButtonAuthority: function() {
			var that = this;
			var buttons = [];
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_ENDERECO_EDITAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'edita_ficha_button',
						type: 'primary',
						icon: 'icon-pencil fa-pencil',
						hint: 'Editar Cliente endereco',
						onClick: that.editModel,
					});
				}
				if (e.authority == 'ROLE_DELETAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'delete_button',
						type: 'danger',
						icon: 'icon-trash fa-trash',
						hint: 'Remover Cliente endereco',
						onClick: that.deleteModel,
					});
				}
			})
			return buttons;
		},
		// vitoriano : chunk
		// vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_ENDERECO_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
	});
	return PageEnderecoFavoritoCliente;
});