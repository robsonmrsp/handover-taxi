/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var SuggestGmapBox = require('views/components/SuggestGmapBox');
	var EnderecoFavoritoModel = require('models/ClienteEnderecoModel');
	var EnderecoFavoritoCollection = require('collections/ClienteEnderecoCollection');
	var EnderecoFavoritoPageCollection = require('collections/ClienteEnderecoPageCollection');
	var FormEnderecoFavoritoEmpresaTemplate = require('text!views/enderecoFavorito/tpl/FormEnderecoFavoritoClienteTemplate.html');
	//Filter import
	var ClienteModal = require('views/modalComponents/ClienteModal');
	// End of "Import´s" definition
	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################
	var FormEnderecoFavoritoCliente = Marionette.LayoutView.extend({
		template: _.template(FormEnderecoFavoritoEmpresaTemplate),
		regions: {
			modalClienteRegion: '#clienteModal',
		},
		events: {
			'click 	.save': 'save',
			'click 	.saveAndContinue': 'saveAndContinue',
			'click 	.go-back-link': 'goBack',
			'click #searchClienteModal': 'showModalCliente',
		},
		ui: {
			// cliente
			inputClienteNome: '#inputClienteNome',
			inputClienteEmail: '#inputClienteEmail',
			inputClienteTipo: '#inputClienteTipo',
			// endereco
			inputId: '#inputId',
			inputEnderecoCombo: '.inputEnderecoCombo',
			inputEndereco: '#inputEndereco',
			inputNumero: '#inputNumero',
			inputComplemento: '#inputComplemento',
			inputUf: '#inputUf',
			inputCidade: '#inputCidade',
			inputBairro: '#inputBairro',
			inputReferencia: '#inputReferencia',
			inputCep: '#inputCep',
			// so o capiroto sabe
			inputClienteId: '#inputClienteId',
			inputClienteNome: '#inputClienteNome',
			form: '#formClienteEndereco',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		initialize: function(options) {
			var that = this;
			this.cliente = options.cliente
			this.clienteModal = new ClienteModal({
				onSelectModel: function(model) {
					that.onSelectCliente(model);
				},
			});
			this.on('show', function() {
				this.modalClienteRegion.show(this.clienteModal);
				this.enderecoFavorito = new SuggestGmapBox({
					el: that.ui.inputEnderecoCombo,
					onSelect: function(endereco) {
						that.jsonValue = endereco;
						that.model.set(endereco);
						that.populaEndereco(endereco);
					},
				});
				this.ui.inputCep.mask('99.999-999');
				this.ui.form.validationEngine('attach', {
					promptPosition: "topLeft",
					isOverflown: false,
					validationEventTrigger: "change"
				});
				this.carregaCliente(this.cliente);
				this.checkButtonAuthority();
			});
		},
		carregaCliente: function(cliente) {
			this.ui.inputClienteId.val(cliente.get('id'));
			this.ui.inputClienteNome.val(cliente.get('nome'));
			this.ui.inputClienteEmail.val(cliente.get('email'));
			this.ui.inputClienteTipo.val(cliente.get('tipo'));
		},
		goBack: function() {
			util.goPage('app/cliente/' + this.cliente.get('id') + '/enderecos');
		},
		saveAndContinue: function() {
			this.save(true)
		},
		save: function(continua) {
			var that = this;
			var clienteEndereco = that.getModel();
			if (this.isValid()) {
				clienteEndereco.save({}, {
					success: function(_model, _resp, _options) {
						util.showSuccessMessage('Endereco favorito salvo com sucesso!');
						that.clearForm();
						if (continua != true) {
							util.goPage('app/cliente/' + that.cliente.get('id') + '/enderecos');
						}
					},
					error: function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro', _resp);
					}
				});
			}
			else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},
		clearForm: function() {
			util.clear('inputId');
			util.clear('inputId');
			util.clear('inputEndereco');
			util.clear('inputEnderecoCombo');
			util.clear('inputNumero');
			util.clear('inputComplemento');
			util.clear('inputUf');
			util.clear('inputCidade');
			util.clear('inputBairro');
			util.clear('inputCep');
			util.clear('inputReferencia');
			util.clear('inputTipo');
			util.clear('inputFavorito');
			util.clear('inputClienteId');
			util.clear('inputClienteNome');
		},
		isValid: function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition: "topLeft",
				isOverflown: false,
				validationEventTrigger: "change"
			});
		},
		getModel: function() {
			var that = this;
			var clienteEndereco = that.model;
			clienteEndereco.set({
				id: util.escapeById('inputId') || null,
				endereco: util.escapeById('inputEndereco'),
				numero: util.escapeById('inputNumero'),
				complemento: util.escapeById('inputComplemento'),
				uf: util.escapeById('inputUf'),
				cidade: util.escapeById('inputCidade'),
				bairro: util.escapeById('inputBairro'),
				cep: util.onlyNumber(util.escapeById('inputCep')), // this is wrong
				referencia: util.escapeById('inputReferencia'),
				tipo: util.escapeById('inputTipo'),
				favorito: util.escapeById('inputFavorito'),
				cliente: that.cliente.toJSON(),
			});
			return clienteEndereco;
		},
		showModalCliente: function() {
			// add more before the modal is open
			this.clienteModal.showPage();
		},
		onSelectCliente: function(cliente) {
			this.clienteModal.hidePage();
			this.ui.inputClienteId.val(cliente.get('id'));
			this.ui.inputClienteNome.val(cliente.get('nome'));
		},
		populaEndereco: function(endereco) {
			this.ui.inputEndereco.val(endereco.logradouro);
			this.ui.inputNumero.val(endereco.numero);
			this.ui.inputCep.val(endereco.cep);
			this.ui.inputBairro.val(endereco.bairro);
			this.ui.inputCidade.val(endereco.cidade);
			this.ui.inputUf.val(endereco.uf);
		},
		// vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_ENDERECO_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
				if(e.authority == 'ROLE_ENDERECO_EDITAR' || e.authority == 'ROLE_ADMIN') {
					saveButton.show();
				}
			})
		},
	});
	return FormEnderecoFavoritoCliente;
});