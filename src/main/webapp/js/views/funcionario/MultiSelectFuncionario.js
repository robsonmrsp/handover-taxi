/* generated: 18/10/2016 01:22:05 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectFuncionario = require('views/funcionario/ModalMultiSelectFuncionario');
	var MultiSelectFuncionarioTemplate = require('text!views/funcionario/tpl/MultiSelectFuncionarioTemplate.html');

	var MultiSelectFuncionario = Marionette.LayoutView.extend({
		template : _.template(MultiSelectFuncionarioTemplate),

		regions : {
			modalMultiSelectFuncionarioRegion : '#modalMultiSelectFuncionarios',
			gridFuncionariosModalRegion : '#gridMultiselectFuncionarios',
		},

		initialize : function() {
			var that = this;

			this.funcionarios = this.collection;

			this.gridFuncionarios = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.funcionarios,
			});

			this.modalMultiSelectFuncionario = new ModalMultiSelectFuncionario({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectFuncionarioRegion.show(that.modalMultiSelectFuncionario);
				that.gridFuncionariosModalRegion.show(that.gridFuncionarios);
			});
		},
		clear : function(){
			this.modalMultiSelectFuncionario.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "nome",
				editable : false,
				sortable : false,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : false,
				label 	 : "E-mail",
				cell 	 : "string",
			}, 
			{
				name : "senha",
				editable : false,
				sortable : false,
				label 	 : "Senha",
				cell 	 : "string",
			}, 
			{
				name : "tipo",
				editable : false,
				sortable : false,
				label 	 : "Tipo",
				cell 	 : "string",
			}, 
			{
				name : "inativo",
				editable : false,
				sortable : false,
				label 	 : "Inativo",
				cell 	 : "string",
			}, 
			{
				name : "usuarioCadastro",
				editable : false,
				sortable : false,
				label 	 : "Usuario cadastro",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "dataCadastro",
				editable : false,
				sortable : false,
				label 	 : "Data cadastro",
				cell 	 : "string",
			}, 
			{
				name : "matricula",
				editable : false,
				sortable : false,
				label 	 : "Matricula",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "limiteMensal",
				editable : false,
				sortable : false,
				label 	 : "Limite mensal",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "cnpjCpf",
				editable : false,
				sortable : false,
				label 	 : "CNPJ CPF",
				cell 	 : "string",
			}, 
			{
				name : "cnpj",
				editable : false,
				sortable : false,
				label 	 : "CNPJ",
				cell 	 : "string",
			}, 
			{
				name : "cpf",
				editable : false,
				sortable : false,
				label 	 : "CPF",
				cell 	 : "string",
			}, 
			{
				name : "autorizaEticket",
				editable : false,
				sortable : false,
				label 	 : "Autoriza eticket",
				cell 	 : "string",
			}, 
			{
				name : "observacao",
				editable : false,
				sortable : false,
				label 	 : "Observação",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectFuncionario
});
