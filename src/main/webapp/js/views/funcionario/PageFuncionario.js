/* generated: 18/10/2016 01:22:05 */
define(function(require) {
    // Start "Import´s Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var roles = require('adapters/auth-adapter').roles;
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var CustomStringCell = require('views/components/CustomStringCell');
    var Counter = require('views/components/Counter');
    var ActionsCell = require('views/components/ActionsCell');
    var GeneralActionsCell = require('views/components/GeneralActionsCell');
    var CustomNumberCell = require('views/components/CustomNumberCell');
    var FuncionarioModel = require('models/FuncionarioModel');
    var FuncionarioCollection = require('collections/FuncionarioCollection');
    var FuncionarioPageCollection = require('collections/FuncionarioPageCollection');
    var PageFuncionarioTemplate = require('text!views/funcionario/tpl/PageFuncionarioTemplate.html');
    var ModalEmpresa = require('views/modalComponents/EmpresaModal');
    var ModalCentroCusto = require('views/modalComponents/CentroCustoModal');
    // End of "Import´s" definition
    var PageFuncionario = Marionette.LayoutView.extend({
        template: _.template(PageFuncionarioTemplate),
        regions: {
            gridRegion: '#grid',
            counterRegion: '#counter',
            paginatorRegion: '#paginator',
            modalEmpresaRegion: '#empresaModal',
            modalCentroCustoRegion: '#centroCustoModal',
        },
        events: {
            'click 	#reset': 'resetFuncionario',
            'click #searchEmpresaModal': 'showModalEmpresa',
            'click #searchCentroCustoModal': 'showModalCentroCusto',
            'keypress': 'treatKeypress',
            'click 	.voltar-empresa': 'voltarEmpresa',
            'click 	.novo-funcionario': 'novoFuncionario',
            'click 	.search-button': 'searchFuncionario',
            'click .show-advanced-search-button': 'toggleAdvancedForm',
        },
        ui: {
            inputEmpresaNomeFantasia: '#inputEmpresaNomeFantasia',
            inputEmpresaCnpj: '#inputEmpresaCnpj',
            inputEmpresaContrato: '#inputEmpresaContrato',
            inputEmpresaRazaoSocial: '#inputEmpresaRazaoSocial',
            inputNome: '#inputNome',
            inputEmail: '#inputEmail',
            inputSenha: '#inputSenha',
            inputTipo: '#inputTipo',
            inputInativo: '#inputInativo',
            inputEmpresaId: '#inputEmpresaId',
            inputEmpresaNome: '#inputEmpresaNome',
            inputCentroCustoId: '#inputCentroCustoId',
            inputCentroCustoNome: '#inputCentroCustoNome',
            form: '#formFuncionarioFilter',
            advancedSearchForm: '.advanced-search-form',
            newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
        },
        toggleAdvancedForm: function() {
            this.ui.advancedSearchForm.slideToggle("slow");
        },
        treatKeypress: function(e) {
            if (util.enterPressed(e)) {
                e.preventDefault();
                this.searchFuncionario();
            }
        },
        initialize: function(options) {
            var that = this;
            this.empresa = options.empresa;
            this.funcionarios = new FuncionarioPageCollection();
            this.grid = new Backgrid.Grid({
                className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
                columns: this.getColumns(),
                emptyText: "Sem registros",
                collection: this.funcionarios
            });
            this.counter = new Counter({
                collection: this.funcionarios,
            });
            this.paginator = new Backgrid.Extension.Paginator({
                columns: this.getColumns(),
                collection: this.funcionarios,
                className: ' paging_simple_numbers',
                uiClassName: 'pagination',
            });
            this.modalEmpresa = new ModalEmpresa({
                onSelectModel: function(model) {
                    that.onSelectEmpresa(model);
                },
            });
            this.modalCentroCusto = new ModalCentroCusto({
                onSelectModel: function(model) {
                    that.onSelectCentroCusto(model);
                },
            });
            this.on('show', function() {
                this.searchFuncionario();
                that.gridRegion.show(that.grid);
                that.counterRegion.show(that.counter);
                that.paginatorRegion.show(that.paginator);
                this.modalEmpresaRegion.show(this.modalEmpresa);
                this.modalCentroCustoRegion.show(this.modalCentroCusto);
                this.carregaEmpresa(this.empresa);
                this.checkButtonAuthority();
            });
        },
        carregaEmpresa: function(empresa) {
            this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));
            this.ui.inputEmpresaCnpj.val(empresa.get('cnpj'));
            this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
            this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));
        },
        searchFuncionario: function() {
            var that = this;
            this.funcionarios.filterQueryParams = {
                nome: util.escapeById('inputNome'),
                email: util.escapeById('inputEmail'),
                senha: util.escapeById('inputSenha'),
                tipo: 'FUNCIONARIO',
                inativo: util.escapeById('inputInativo'),
                empresa: util.escapeById('inputEmpresaId'),
                centroCusto: util.escapeById('inputCentroCustoId'),
                empresa: that.empresa.get('id'),
            }
            this.funcionarios.fetch({
                success: function(_coll, _resp, _opt) {
                    console.info('Consulta para o grid funcionario');
                },
                error: function(_coll, _resp, _opt) {
                    console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
                },
                complete: function() {},
            })
        },
        resetFuncionario: function() {
            this.ui.form.get(0).reset();
            this.funcionarios.reset();
            util.clear('inputEmpresaId');
            util.clear('inputCentroCustoId');
        },
        getColumns: function() {
            var that = this;
            var columns = [{
                name: "nome",
                editable: false,
                sortable: true,
                label: "Nome",
                cell: "string",
            }, {
                name: "email",
                editable: false,
                sortable: true,
                label: "E-mail",
                cell: "string",
            }, {
                name: "tipo",
                editable: false,
                sortable: true,
                label: "Tipo",
                cell: "string",
            }, {
                name: "inativo",
                editable: false,
                sortable: true,
                label: "Status",
                cell: "string",
                formatter: _.extend({}, Backgrid.CellFormatter.prototype, {
                    fromRaw: function(rawValue, model) {
                        return (rawValue ? "Inativo" : "Ativo");
                    }
                })
            }, {
                name: "centroCusto.descricao",
                editable: false,
                sortable: true,
                label: "Centro custo",
                cell: CustomStringCell.extend({
                    fieldName: 'centroCusto.descricao',
                }),
            }, {
                name: "acoes",
                label: "Ações(Editar, Deletar)",
                sortable: false,
                cell: GeneralActionsCell.extend({
                    buttons: that.getCellButtons(),
                    context: that,
                })
            }];
            return columns;
        },
        getCellButtons: function() {
            var that = this;
            var buttons = this.checkGridButtonAuthority();
            return buttons;
        },
        deleteModel: function(model) {
            var that = this;
            var modelTipo = new FuncionarioModel({
                id: model.id,
            });
            util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
                if (yes) {
                    modelTipo.destroy({
                        success: function() {
                            that.funcionarios.remove(model);
                            util.showSuccessMessage('Funcionario removido com sucesso!');
                        },
                        error: function(_model, _resp) {
                            util.showErrorMessage('Problema ao remover o registro', _resp);
                        }
                    });
                }
            });
        },
        novoFuncionario: function() {
            util.goPage("app/empresa/" + this.empresa.get('id') + "/newFuncionario");
        },
        voltarEmpresa: function() {
            util.goPage("app/empresas");
        },
        editModel: function(model) {
            util.goPage("app/empresa/" + this.empresa.get('id') + "/editFuncionario/" + model.get('id'));
        },
        showModalEmpresa: function() {
            this.modalEmpresa.showPage();
        },
        onSelectEmpresa: function(empresa) {
            this.modalEmpresa.hidePage();
            this.ui.inputEmpresaId.val(empresa.get('id'));
            this.ui.inputEmpresaNome.val(empresa.get('nome'));
        },
        showModalCentroCusto: function() {
            this.modalCentroCusto.showPage();
        },
        onSelectCentroCusto: function(centroCusto) {
            this.modalCentroCusto.hidePage();
            this.ui.inputCentroCustoId.val(centroCusto.get('id'));
            this.ui.inputCentroCustoNome.val(centroCusto.get('nome'));
        },
        // vitoriano : chunck : check grid buttons authority
        checkGridButtonAuthority: function() {
            var that = this;
            var buttons = [];
            $.grep(roles, function(e) {
                if (e.authority == 'ROLE_FUNCIONARIO_EDITAR' || e.authority == 'ROLE_ADMIN') {
                    buttons.push({
                        id: 'edita_ficha_button',
                        type: 'primary',
                        icon: 'icon-pencil fa-pencil',
                        hint: 'Editar Funcionario',
                        onClick: that.editModel,
                    });
                }
                if (e.authority == 'ROLE_FUNCIONARIO_DELETAR' || e.authority == 'ROLE_ADMIN') {
                    buttons.push({
                        id: 'delete_button',
                        type: 'danger',
                        icon: 'icon-trash fa-trash',
                        hint: 'Remover Funcionario',
                        onClick: that.deleteModel,
                    });
                }
            })
            return buttons;
        },
        // vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_FUNCIONARIO_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
    });
    return PageFuncionario;
});