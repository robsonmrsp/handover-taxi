/* generated: 18/10/2016 01:22:05 */
define(function(require) {
    //Start "Import´s" Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var roles = require('adapters/auth-adapter').roles;
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var TemplateFormFuncionarios = require('text!views/funcionario/tpl/FormFuncionarioTemplate.html');
    var FuncionarioModel = require('models/FuncionarioModel');
    var FuncionarioCollection = require('collections/FuncionarioCollection');
    var ModalEmpresa = require('views/modalComponents/EmpresaModal');
    var ModalCentroCusto = require('views/modalComponents/CentroCustoModal');
    // End of "Import´s" definition
    // #####################################################################################################
    // ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
    // BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
    // #####################################################################################################	
    var FormFuncionarios = Marionette.LayoutView.extend({
    template: _.template(TemplateFormFuncionarios),
        regions: {
            modalEmpresaRegion: '#empresaModal',
            modalCentroCustoRegion: '#centroCustoModal',
        },
        events: {
            'click 	.save': 'save',
            'click 	.saveAndContinue': 'saveAndContinue',
            'click #searchEmpresaModal': 'showModalEmpresa',
            'click #searchCentroCustoModal': 'showModalCentroCusto',
            'click 	.go-back-link': 'goBack',
            'change  #inputCpf': 'uniqueCpf',
            'change  #inputTelefone': 'uniqueTelefone',
            'change #inputLimiteMensal': 'acionaLimiteDisponivelCC',
        },
        ui: {
            inputEmpresaNomeFantasia: '#inputEmpresaNomeFantasia',
            inputEmpresaCnpj: '#inputEmpresaCnpj',
            inputEmpresaContrato: '#inputEmpresaContrato',
            inputEmpresaRazaoSocial: '#inputEmpresaRazaoSocial',
            inputId: '#inputId',
            inputNome: '#inputNome',
            inputTelefone: '#inputTelefone',
            inputEmail: '#inputEmail',
            inputSenha: '#inputSenha',
            inputTipo: '#inputTipo',
            inputInativo: '#inputInativo',
            inputUsuarioCadastro: '#inputUsuarioCadastro',
            inputDataCadastro: '#inputDataCadastro',
            groupInputDataCadastro: '#groupInputDataCadastro',
            inputMatricula: '#inputMatricula',
            inputLimiteMensal: '#inputLimiteMensal',
            inputCnpjCpf: '#inputCnpjCpf',
            inputCnpj: '#inputCnpj',
            inputCpf: '#inputCpf',
            inputAutorizaEticket: '#inputAutorizaEticket',
            inputObservacao: '#inputObservacao',
            inputEmpresaId: '#inputEmpresaId',
            inputEmpresaNome: '#inputEmpresaNome',
            inputCentroCustoId: '#inputCentroCustoId',
            inputCentroCustoDescricao: '#inputCentroCustoDescricao',
            spanLimiteDisponivel: '#limiteDisponivel',
            inputLimiteDisponivel: '#inputLimiteDisponivel',
            groupInputLimiteDisponivel: '#groupInputLimiteDisponivel',
            form: '#formFuncionario',
            newButton: 'a#new',
            saveButton: 'a#save',
            saveContinueButton: 'a#saveContinue',
        },
        initialize: function(options) {
            this.empresa = options.empresa;
            var that = this;
            this.modalCentroCusto = new ModalCentroCusto({
                onSelectModel: function(model) {
                    that.onSelectCentroCusto(model);
                },
            });
            this.modalCentroCusto.setValue(this.model.get('centroCusto'));
            this.modalCentroCusto.setEmpresa(this.empresa.get('id'));
            this.on('show', function() {
                this.modalCentroCustoRegion.show(this.modalCentroCusto);
                this.ui.inputUsuarioCadastro.formatNumber(2);
                this.ui.groupInputDataCadastro.datetimepicker({
                    pickTime: false,
                    language: 'pt_BR',
                });
                this.ui.inputDataCadastro.datetimepicker({
                    pickTime: false,
                    language: 'pt_BR',
                });
                this.ui.inputTelefone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
                this.ui.inputDataCadastro.mask('99/99/9999');
                this.ui.inputLimiteMensal.mask('###.###.###.###,##', {
                    reverse: true
                });
                this.ui.inputLimiteMensal.val(util.printFormatNumber(this.model.get('limiteMensal')));
                this.ui.inputCnpj.mask('99.999.999/9999-99');
                this.ui.inputCpf.mask('999.999.999-99');
                this.ui.form.validationEngine('attach', {
                    promptPosition: "topLeft",
                    isOverflown: false,
                    validationEventTrigger: "change"
                });
                if (this.model.get('id')) this.limiteDisponivelCC(this.model.get('centroCusto'));
                this.carregaEmpresa(this.empresa);
                this.checkButtonAuthority();
            });
        },
        carregaEmpresa: function(empresa) {
            this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));
            this.ui.inputEmpresaCnpj.val(empresa.get('cnpj'));
            this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
            this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));
        },
        goBack: function() {
            util.goPage('app/empresa/' + this.empresa.get('id') + '/funcionarios');
        },
        saveAndContinue: function() {
            this.save(true)
        },
        save: function(continua) {
            var that = this;
            var funcionario = that.getModel();
            if (this.isValid()) {
                if (!this.limiteDisponivelCC(funcionario.get('centroCusto'))) return;
                funcionario.save({}, {
                    success: function(_model, _resp, _options) {
                        util.showSuccessMessage('Funcionario salvo com sucesso!');
                        that.clearForm();
                        if (continua != true) {
                            util.goPage('app/empresa/' + that.empresa.get('id') + '/funcionarios');
                        }
                    },
                    error: function(_model, _resp, _options) {
                        util.showErrorMessage('Problema ao salvar registro', _resp);
                    }
                });
            }
            else {
                util.showMessage('error', 'Verifique campos em destaque!');
            }
        },
        clearForm: function() {
            util.clear('inputId');
            util.clear('inputNome');
            util.clear('inputEmail');
            util.clear('inputSenha');
            util.clear('inputTipo');
            util.clear('inputInativo');
            util.clear('inputTelefone');
            util.clear('inputUsuarioCadastro');
            util.clear('inputDataCadastro');
            util.clear('inputMatricula');
            util.clear('inputLimiteMensal');
            util.clear('inputCnpjCpf');
            util.clear('inputCnpj');
            util.clear('inputCpf');
            util.clear('inputAutorizaEticket');
            util.clear('inputObservacao');
            util.clear('inputEmpresaId');
            util.clear('inputEmpresaNome');
            util.clear('inputCentroCustoId');
            util.clear('inputCentroCustoDescricao');
        },
        isValid: function() {
            return this.ui.form.validationEngine('validate', {
                promptPosition: "topLeft",
                isOverflown: false,
                validationEventTrigger: "change"
            });
        },
        getModel: function() {
            var that = this;
            var funcionario = that.model;
            funcionario.set({
                id: util.escapeById('inputId') || null,
                nome: util.escapeById('inputNome'),
                email: util.escapeById('inputEmail'),
                senha: util.escapeById('inputSenha'),
                tipo: util.escapeById('inputTipo'),
                telefone: util.escapeById('inputTelefone'),
                inativo: util.escapeById('inputInativo'),
                usuarioCadastro: util.escapeById('inputUsuarioCadastro', true),
                dataCadastro: util.escapeById('inputDataCadastro'),
                matricula: util.escapeById('inputMatricula', true),
                limiteMensal: util.escapeById('inputLimiteMensal', true),
                cnpjCpf: util.escapeById('inputCnpjCpf'),
                cnpj: util.escapeById('inputCnpj'),
                cpf: util.escapeById('inputCpf'),
                autorizaEticket: util.escapeById('inputAutorizaEticket'),
                observacao: util.escapeById('inputObservacao'),
                empresa: that.empresa.toJSON(),
                centroCusto: that.modalCentroCusto.getJsonValue(),
            });
            return funcionario;
        },
        showModalEmpresa: function() {
            // add more before the modal is open
            this.modalEmpresa.showPage();
        },
        showModalCentroCusto: function() {
            // add more before the modal is open
            this.modalCentroCusto.showPage();
        },
        onSelectEmpresa: function(empresa) {
            this.modalEmpresa.hidePage();
            this.ui.inputEmpresaId.val(empresa.get('id'));
            this.ui.inputEmpresaNome.val(empresa.get('nome'));
        },
        onSelectCentroCusto: function(centroCusto) {
            this.modalCentroCusto.hidePage();
            this.ui.inputCentroCustoId.val(centroCusto.get('id'));
            this.ui.inputCentroCustoDescricao.val(centroCusto.get('descricao'));
            this.limiteDisponivelCC(centroCusto.toJSON());
        },
        //TODO: Refatorar função utilizando variáveis globais e, se for o caso, dividir a função
        limiteDisponivelCC: function(centroCusto) {
            var limiteUtilizado = 0;
            //Monta valor de limiteUtilizado com os funcionários do CC menos o funcionário corrente
            for (var index in centroCusto.clientes)
                if (this.model.get('id') != centroCusto.clientes[index].id) limiteUtilizado += centroCusto.clientes[index].limiteMensal;
            //Levar tratamento para a máscara
            //Se o valor inputado for inteiro, ele é convertido para float pq a máscara apaga valores inteiros
            if (this.ui.inputLimiteMensal.val() % 1 == 0) this.ui.inputLimiteMensal.val(util.formatFinalNumber(this.ui.inputLimiteMensal.val()));
            //Substitui a virgula do inputLimiteMensal e transforma para float para operações.
            var limiteFuncionarioEdit = parseFloat(util.strToNum(this.ui.inputLimiteMensal.val()));
            //TODO: Criar variável global para limite Utilizado
            this.ui.inputLimiteDisponivel.val(limiteUtilizado);
            //Se o limite do funcionário + o limiteUtilizado (limiteFuncionarioEdit) for NaN ou o limite do CC for estourado, o cálculo é resetado
            if (isNaN(limiteFuncionarioEdit) || centroCusto.valorLimite < limiteUtilizado + limiteFuncionarioEdit) {
                limiteFuncionarioEdit = 0;
                //this.ui.inputLimiteMensal.val('0,00')
                this.ui.spanLimiteDisponivel.text('Limite Disponível R$ ' + parseFloat(centroCusto.valorLimite - limiteUtilizado).toFixed(2));
                util.showMessage('error', 'O limite mensal ultrapassa o limite disponível do centro de custo ' + centroCusto.descricao + '!');
                return false;
            }
            this.ui.spanLimiteDisponivel.text('Limite Disponível R$ ' + parseFloat(centroCusto.valorLimite - (limiteUtilizado + limiteFuncionarioEdit)).toFixed(2));
            return true;
        },
        acionaLimiteDisponivelCC: function() {
            var centroCusto = this.model.get('centroCusto') == null && this.modalCentroCusto.getJsonValue() != null ? this.modalCentroCusto.getJsonValue() : this.model.get('centroCusto');
            if (centroCusto != null) this.limiteDisponivelCC(centroCusto);
        },
        uniqueCpf: function() {
            var that = this;
            util.validateUnique({
                element: that.ui.inputCpf,
                fieldName: 'cpf',
                fieldDisplayName: 'CPF',
                view: that,
                collection: FuncionarioCollection,
            })
        },
        uniqueTelefone: function() {
            var that = this;
            util.validateUnique({
                element: that.ui.inputTelefone,
                fieldName: 'telefone',
                fieldDisplayName: 'Telefone',
                view: that,
                collection: FuncionarioCollection,
            })
        },
        // vitoriano : chunk
        checkButtonAuthority: function() {
            var newButton = this.ui.newButton;
            var saveButton = this.ui.saveButton;
            var saveContinueButton = this.ui.saveContinueButton;
            $.grep(roles, function(e) {
                if (e.authority == 'ROLE_FUNCIONARIO_CRIAR' || e.authority == 'ROLE_ADMIN') {
                    newButton.show();
                    saveButton.show();
                    saveContinueButton.show();
                }
                if (e.authority == 'ROLE_FUNCIONARIO_EDITAR' || e.authority == 'ROLE_ADMIN') {
                    saveButton.show();
                }
            })
        },
    });
    return FormFuncionarios;
});