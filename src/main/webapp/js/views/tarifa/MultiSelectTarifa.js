/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectTarifa = require('views/tarifa/ModalMultiSelectTarifa');
	var MultiSelectTarifaTemplate = require('text!views/tarifa/tpl/MultiSelectTarifaTemplate.html');

	var MultiSelectTarifa = Marionette.LayoutView.extend({
		template : _.template(MultiSelectTarifaTemplate),

		regions : {
			modalMultiSelectTarifaRegion : '#modalMultiSelectTarifas',
			gridTarifasModalRegion : '#gridMultiselectTarifas',
		},

		initialize : function() {
			var that = this;

			this.tarifas = this.collection;

			this.gridTarifas = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.tarifas,
			});

			this.modalMultiSelectTarifa = new ModalMultiSelectTarifa({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectTarifaRegion.show(that.modalMultiSelectTarifa);
				that.gridTarifasModalRegion.show(that.gridTarifas);
			});
		},
		clear : function(){
			this.modalMultiSelectTarifa.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "bairroOrigem",
				editable : false,
				sortable : false,
				label 	 : "Bairro origem",
				cell 	 : "string",
			}, 
			{
				name : "bairroDestino",
				editable : false,
				sortable : false,
				label 	 : "Bairro destino",
				cell 	 : "string",
			}, 
			{
				name : "valor",
				editable : false,
				sortable : false,
				label 	 : "Valor",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return MultiSelectTarifa
});
