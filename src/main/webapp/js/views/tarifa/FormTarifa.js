/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var TemplateFormTarifas = require('text!views/tarifa/tpl/FormTarifaTemplate.html');
	var TarifaModel = require('models/TarifaModel');
	var TarifaCollection = require('collections/TarifaCollection');
	var FormTarifas = Marionette.LayoutView.extend({
		template: _.template(TemplateFormTarifas),
		regions: {},
		events: {
			'click 	.save': 'save',
			'click 	.saveAndContinue': 'saveAndContinue',
		},
		ui: {
			inputId: '#inputId',
			inputBairroOrigem: '#inputBairroOrigem',
			inputBairroDestino: '#inputBairroDestino',
			inputValor: '#inputValor',
			form: '#formTarifa',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		initialize: function() {
			var that = this;
			this.on('show', function() {
				this.ui.inputValor.mask('###.###.###.###,##', {
					reverse: true
				});
				this.ui.inputValor.val(util.printFormatNumber(this.model.get('valor')));
				this.ui.form.validationEngine('attach', {
					promptPosition: "topLeft",
					isOverflown: false,
					validationEventTrigger: "change"
				});
				this.checkButtonAuthority();
			});
		},
		saveAndContinue: function() {
			this.save(true)
		},
		save: function(continua) {
			var that = this;
			var tarifa = that.getModel();
			if (this.isValid()) {
				tarifa.save({}, {
					success: function(_model, _resp, _options) {
						util.showSuccessMessage('Tarifa salvo com sucesso!');
						that.clearForm();
						if (continua != true) {
							util.goPage('app/tarifas');
						}
					},
					error: function(_model, _resp, _options) {
						if(_resp.status == '403'){
							util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
						} else {
							util.showErrorMessage('Problema ao salvar registro', _resp);
						}
					}
				});
			}
			else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},
		clearForm: function() {
			util.clear('inputId');
			util.clear('inputBairroOrigem');
			util.clear('inputBairroDestino');
			util.clear('inputValor');
		},
		isValid: function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition: "topLeft",
				isOverflown: false,
				validationEventTrigger: "change"
			});
		},
		getModel: function() {
			var that = this;
			var tarifa = that.model;
			tarifa.set({
				id: util.escapeById('inputId') || null,
				bairroOrigem: util.escapeById('inputBairroOrigem'),
				bairroDestino: util.escapeById('inputBairroDestino'),
				valor: util.escapeById('inputValor', true),
			});
			return tarifa;
		},
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_TARIFA_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
				if(e.authority == 'ROLE_TARIFA_EDITAR' || e.authority == 'ROLE_ADMIN') {
					saveButton.show();
				}
			})
		},
	});
	return FormTarifas;
});