/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var TarifaPageCollection = require('collections/TarifaPageCollection');
	var ModalMultiSelectTarifaTemplate = require('text!views/tarifa/tpl/ModalMultiSelectTarifaTemplate.html');
	// End of "Import´s" definition

	var ModalTarifas = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectTarifaTemplate),

		regions : {
			gridRegion : '#grid-tarifas-modal',
			paginatorRegion : '#paginator-tarifas-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoTarifas = this.collection;
			
			this.tarifas = new TarifaPageCollection();
			this.tarifas.on('fetched', this.endFetch, this);
			this.tarifas.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.tarifas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.tarifas,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.tarifas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid tarifa');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoTarifas.add(model)
			else
				this.projetoTarifas.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.tarifas.each(function(model) {
				if (that.projetoTarifas.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "bairroOrigem",
				editable : false,
				sortable : false,
				label 	 : "Bairro origem",
				cell 	 : "string",
			}, 
			{
				name : "bairroDestino",
				editable : false,
				sortable : false,
				label 	 : "Bairro destino",
				cell 	 : "string",
			}, 
			{
				name : "valor",
				editable : false,
				sortable : false,
				label 	 : "Valor",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return ModalTarifas;
});
