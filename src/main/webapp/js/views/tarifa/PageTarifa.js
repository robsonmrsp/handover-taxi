/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');
	var CustomNumberCell = require('views/components/CustomNumberCell');
	var TarifaModel = require('models/TarifaModel');
	var TarifaCollection = require('collections/TarifaCollection');
	var TarifaPageCollection = require('collections/TarifaPageCollection');
	var PageTarifaTemplate = require('text!views/tarifa/tpl/PageTarifaTemplate.html');
	//Filter import
	// End of "Import´s" definition
	var PageTarifa = Marionette.LayoutView.extend({
		template: _.template(PageTarifaTemplate),
		regions: {
			gridRegion: '#grid',
			counterRegion: '#counter',
			paginatorRegion: '#paginator',
		},
		events: {
			'click 	#reset': 'resetTarifa',
			'keypress': 'treatKeypress',
			'click 	.search-button': 'searchTarifa',
			'click .show-advanced-search-button': 'toggleAdvancedForm',
		},
		ui: {
			inputBairroOrigem: '#inputBairroOrigem',
			inputBairroDestino: '#inputBairroDestino',
			inputValor: '#inputValor',
			form: '#formTarifaFilter',
			advancedSearchForm: '.advanced-search-form',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		toggleAdvancedForm: function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},
		treatKeypress: function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchTarifa();
			}
		},
		initialize: function() {
			var that = this;
			this.tarifas = new TarifaPageCollection();
			this.grid = new Backgrid.Grid({
				className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns: this.getColumns(),
				emptyText: "Sem registros",
				collection: this.tarifas
			});
			this.counter = new Counter({
				collection: this.tarifas,
			});
			this.paginator = new Backgrid.Extension.Paginator({
				columns: this.getColumns(),
				collection: this.tarifas,
				className: ' paging_simple_numbers',
				uiClassName: 'pagination',
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				this.searchTarifa();
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.ui.inputValor.mask('###.###.###.###,##', {
					reverse : true
				});
				this.ui.inputValor.formatNumber(2);
				this.checkButtonAuthority();
			});
		},
		searchTarifa: function() {
			var that = this;
			if (!util.escapeById('inputBairroOrigem')) {
				this.tarifas.filterQueryParams = {
					bairroOrigem: util.escapeById('inputAvancadoBairroOrigem'),
					bairroDestino: util.escapeById('inputBairroDestino'),
					valor: util.escapeById('inputValor'),
				}
			}
			else {
				this.tarifas.filterQueryParams = {
					bairroOrigem: util.escapeById('inputBairroOrigem'),
				}
			}
			this.tarifas.fetch({
				success: function(_coll, _resp, _opt) {
					console.info('Consulta para o grid tarifa');
				},
				error: function(_coll, _resp, _opt) {
					if(_resp.status == '403'){
						util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
					}
				},
				complete: function() {},
			})
		},
		resetTarifa: function() {
			this.ui.form.get(0).reset();
			this.tarifas.reset();
		},
		getColumns: function() {
			var that = this;
			var columns = [
			{
				name : "bairroOrigem",
				editable : false,
				sortable : true,
				label 	 : "Bairro origem",
				cell 	 : "string",
			}, 
			{
				name : "bairroDestino",
				editable : false,
				sortable : true,
				label 	 : "Bairro destino",
				cell 	 : "string",
			}, 
			{
				name : "valor",
				editable : false,
				sortable : true,
				label 	 : "Valor",
				cell : 'number',
				formatter : _.extend({}, Backgrid.CellFormatter.prototype, {
					fromRaw : function(rawValue, model) {
						return 'R$ ' + util.printFormatNumber(rawValue);
					}
				})
			}, 
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			}];
			return columns;
		},
		getCellButtons: function() {
			var that = this;
			var buttons = this.checkGridButtonAuthority();
			return buttons;
		},
		deleteModel: function(model) {
			var that = this;
			var modelTipo = new TarifaModel({
				id: model.id,
			});
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success: function() {
							that.tarifas.remove(model);
							util.showSuccessMessage('Tarifa removido com sucesso!');
						},
						error: function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro', _resp);
						}
					});
				}
			});
		},
		editModel: function(model) {
			util.goPage("app/editTarifa/" + model.get('id'));
		},
		// vitoriano : chunck : check grid buttons authority
		checkGridButtonAuthority: function() {
			var that = this;
			var buttons = [];
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_TARIFA_EDITAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'edita_ficha_button',
						type: 'primary',
						icon: 'icon-pencil fa-pencil',
						hint: 'Editar Tarifa',
						onClick: that.editModel,
					});
				}
				if (e.authority == 'ROLE_TARIFA_DELETAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'delete_button',
						type: 'danger',
						icon: 'icon-trash fa-trash',
						hint: 'Remover Tarifa',
						onClick: that.deleteModel,
					});
				}
			})
			return buttons;
		},
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_TARIFA_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
	});
	return PageTarifa;
});