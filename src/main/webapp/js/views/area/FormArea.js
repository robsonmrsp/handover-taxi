/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormAreas = require('text!views/area/tpl/FormAreaTemplate.html');
	var AreaModel = require('models/AreaModel');
	var AreaCollection = require('collections/AreaCollection');

	var GMap = require('views/components/GMap');

	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
	// BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormAreas = Marionette.LayoutView.extend({
		template : _.template(TemplateFormAreas),

		regions : {},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
		},

		ui : {
			inputId : '#inputId',
			inputDescricao : '#inputDescricao',
			inputCoordenadas : '#inputCoordenadas',

			mapa : '#map',
			form : '#formArea',
		},

		initialize : function() {
			var that = this;
			this.on('show', function() {

				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});

				this.gMap = new GMap({
					mapElement : this.ui.mapa,

					showDrawTools : true,
				})

				if (this.model.get('id')) {
					this.gMap.addPolygon(JSON.parse(this.model.get('coordenadas')));
				}
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var area = that.getModel();

			if (this.isValid()) {
				area.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Area salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/areas');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro', _resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		clearForm : function() {
			util.clear('inputId');
			util.clear('inputDescricao');
			util.clear('inputCoordenadas');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var area = that.model;
			area.set({
				id : util.escapeById('inputId') || null,
				descricao : util.escapeById('inputDescricao'),
				coordenadas : that.getAreaCoordenadas(),
			});
			return area;
		},

		getAreaCoordenadas : function() {
			var overlays = this.gMap.getOverlays();
			var coordenadasString = ''
			if (overlays.length > 0) {
				coordenadasString = JSON.stringify(overlays[0].getPath().getArray())
			}
			return coordenadasString;
		}

	});

	return FormAreas;
});