/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectArea = require('views/area/ModalMultiSelectArea');
	var MultiSelectAreaTemplate = require('text!views/area/tpl/MultiSelectAreaTemplate.html');

	var MultiSelectArea = Marionette.LayoutView.extend({
		template : _.template(MultiSelectAreaTemplate),

		regions : {
			modalMultiSelectAreaRegion : '#modalMultiSelectAreas',
			gridAreasModalRegion : '#gridMultiselectAreas',
		},

		initialize : function() {
			var that = this;

			this.areas = this.collection;

			this.gridAreas = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.areas,
			});

			this.modalMultiSelectArea = new ModalMultiSelectArea({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectAreaRegion.show(that.modalMultiSelectArea);
				that.gridAreasModalRegion.show(that.gridAreas);
			});
		},
		clear : function(){
			this.modalMultiSelectArea.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descrição",
				cell 	 : "string",
			}, 
			{
				name : "coordenadas",
				editable : false,
				sortable : false,
				label 	 : "Coordenadas",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectArea
});
