/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var AreaPageCollection = require('collections/AreaPageCollection');
	var ModalMultiSelectAreaTemplate = require('text!views/area/tpl/ModalMultiSelectAreaTemplate.html');
	// End of "Import´s" definition

	var ModalAreas = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectAreaTemplate),

		regions : {
			gridRegion : '#grid-areas-modal',
			paginatorRegion : '#paginator-areas-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoAreas = this.collection;
			
			this.areas = new AreaPageCollection();
			this.areas.on('fetched', this.endFetch, this);
			this.areas.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.areas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.areas,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.areas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid area');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoAreas.add(model)
			else
				this.projetoAreas.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.areas.each(function(model) {
				if (that.projetoAreas.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descrição",
				cell 	 : "string",
			}, 
			{
				name : "coordenadas",
				editable : false,
				sortable : false,
				label 	 : "Coordenadas",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalAreas;
});
