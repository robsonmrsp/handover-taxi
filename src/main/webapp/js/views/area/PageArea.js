/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var AreaModel = require('models/AreaModel');
	var AreaCollection = require('collections/AreaCollection');
	var AreaPageCollection = require('collections/AreaPageCollection');
	var PageAreaTemplate = require('text!views/area/tpl/PageAreaTemplate.html');

	// Filter import

	// End of "Import´s" definition

	var PageArea = Marionette.LayoutView.extend({
		template : _.template(PageAreaTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
		},

		events : {
			'click 	#reset' : 'resetArea',
			'keypress' : 'treatKeypress',

			'click 	.search-button' : 'searchArea',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},

		ui : {
			inputDescricao : '#inputDescricao',
			inputCoordenadas : '#inputCoordenadas',

			form : '#formAreaFilter',
			advancedSearchForm : '.advanced-search-form',
		},

		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		treatKeypress : function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchArea();
			}
		},

		initialize : function() {
			var that = this;

			this.areas = new AreaPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.areas
			});

			this.counter = new Counter({
				collection : this.areas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.areas,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.areas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid area');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		searchArea : function() {
			var that = this;

			this.areas.filterQueryParams = {
				descricao : util.escapeById('inputDescricao'),
				coordenadas : util.escapeById('inputCoordenadas'),
			}
			this.areas.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid area');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {

				},
			})
		},
		resetArea : function() {
			this.ui.form.get(0).reset();
			this.areas.reset();
		},

		getColumns : function() {
			var that = this;
			var columns = [ {
				name : "descricao",
				editable : false,
				sortable : true,
				label : "Descrição",
				cell : "string",
			}, {
				name : "ordem",
				editable : false,
				sortable : true,
				label : "Ordem",
				cell : "string",
			}, {
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},

		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Endereço',
				onClick : that.editModel,
			}, {
				id : 'subir_button',
				type : 'primary',
				icon : 'fa-sort-asc',
				hint : 'subir',
				onClick : that.subirModel,
			}, {
				id : 'descrer_button',
				type : 'primary',
				icon : 'fa-sort-desc',
				hint : 'Descer',
				onClick : that.descerModel,
			},

			{
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Corrida',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		descerModel : function(model) {
			this.areas.movePraBaixo(model);
		},
		subirModel : function(model) {
			this.areas.movePraCima(model);
		},

		getCellButtons_ : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Area',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Area',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;

			var modelTipo = new AreaModel({
				id : model.id,
			});

			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.areas.remove(model);
							util.showSuccessMessage('Area removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro', _resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editArea/" + model.get('id'));
		},

	});

	return PageArea;
});
