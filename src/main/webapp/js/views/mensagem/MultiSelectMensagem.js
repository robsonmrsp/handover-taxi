/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectMensagem = require('views/mensagem/ModalMultiSelectMensagem');
	var MultiSelectMensagemTemplate = require('text!views/mensagem/tpl/MultiSelectMensagemTemplate.html');

	var MultiSelectMensagem = Marionette.LayoutView.extend({
		template : _.template(MultiSelectMensagemTemplate),

		regions : {
			modalMultiSelectMensagemRegion : '#modalMultiSelectMensagems',
			gridMensagemsModalRegion : '#gridMultiselectMensagems',
		},

		initialize : function() {
			var that = this;

			this.mensagems = this.collection;

			this.gridMensagems = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.mensagems,
			});

			this.modalMultiSelectMensagem = new ModalMultiSelectMensagem({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectMensagemRegion.show(that.modalMultiSelectMensagem);
				that.gridMensagemsModalRegion.show(that.gridMensagems);
			});
		},
		clear : function(){
			this.modalMultiSelectMensagem.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descrição",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectMensagem
});
