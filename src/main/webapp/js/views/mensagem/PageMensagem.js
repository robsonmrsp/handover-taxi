/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var MensagemModel = require('models/MensagemModel');
	var MensagemCollection = require('collections/MensagemCollection');
	var MensagemPageCollection = require('collections/MensagemPageCollection');
	var PageMensagemTemplate = require('text!views/mensagem/tpl/PageMensagemTemplate.html');
	
	//Filter import
	
	// End of "Import´s" definition

	var PageMensagem = Marionette.LayoutView.extend({
		template : _.template(PageMensagemTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
		},
		
		events : {
			'click 	#reset' : 'resetMensagem',			
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchMensagem',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
			inputDescricao : '#inputDescricao',
		
			form : '#formMensagemFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchMensagem();
	    	}
		},

		initialize : function() {
			var that = this;

			this.mensagems = new MensagemPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.mensagems
			});

			this.counter = new Counter({
				collection : this.mensagems,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.mensagems,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.mensagems.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid mensagem');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
				}
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
		
			});
		},
		 
		searchMensagem : function(){
			var that = this;

			this.mensagems.filterQueryParams = {
	    		descricao : util.escapeById('inputDescricao'),
			}
			this.mensagems.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid mensagem');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetMensagem : function(){
			this.ui.form.get(0).reset();
			this.mensagems.reset();
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "descricao",
				editable : false,
				sortable : true,
				label 	 : "Descrição",
				cell 	 : "string",
			}, 
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Mensagem',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Mensagem',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new MensagemModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.mensagems.remove(model);
							util.showSuccessMessage('Mensagem removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editMensagem/" + model.get('id'));
		},

		

	});

	return PageMensagem;
});
