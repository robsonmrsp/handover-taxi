/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormMensagems = require('text!views/mensagem/tpl/FormMensagemTemplate.html');
	var MensagemModel = require('models/MensagemModel');
	var MensagemCollection = require('collections/MensagemCollection');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormMensagems = Marionette.LayoutView.extend({
		template : _.template(TemplateFormMensagems),

		regions : {
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
		},
		
		ui : {
			inputId : '#inputId',
			inputDescricao : '#inputDescricao',
		
			form : '#formMensagem',
		},

		initialize : function() {
			var that = this;
			this.on('show', function() {
		
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var mensagem = that.getModel();

			if (this.isValid()) {
				mensagem.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Mensagem salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/mensagems');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputDescricao'); 
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var mensagem = that.model; 
			mensagem.set({
				id: util.escapeById('inputId') || null,
		    	descricao : util.escapeById('inputDescricao'), 
			});
			return mensagem;
		},
		 		

				
		
	});

	return FormMensagems;
});