/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var MensagemPageCollection = require('collections/MensagemPageCollection');
	var ModalMultiSelectMensagemTemplate = require('text!views/mensagem/tpl/ModalMultiSelectMensagemTemplate.html');
	// End of "Import´s" definition

	var ModalMensagems = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectMensagemTemplate),

		regions : {
			gridRegion : '#grid-mensagems-modal',
			paginatorRegion : '#paginator-mensagems-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoMensagems = this.collection;
			
			this.mensagems = new MensagemPageCollection();
			this.mensagems.on('fetched', this.endFetch, this);
			this.mensagems.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.mensagems,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.mensagems,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.mensagems.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid mensagem');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoMensagems.add(model)
			else
				this.projetoMensagems.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.mensagems.each(function(model) {
				if (that.projetoMensagems.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descrição",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalMensagems;
});
