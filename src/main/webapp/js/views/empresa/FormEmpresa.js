/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var DadosUtils = require('utilities/DadosUtils');
	var Combobox = require('views/components/Combobox');
	var SuggestGmapBox = require('views/components/SuggestGmapBox');
	var TemplateFormEmpresas = require('text!views/empresa/tpl/FormEmpresaTemplate.html');
	var EmpresaModel = require('models/EmpresaModel');
	var EmpresaCollection = require('collections/EmpresaCollection');
	// End of "Import´s" definition
	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
	// BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################
	var FormEmpresas = Marionette.LayoutView.extend({
		template: _.template(TemplateFormEmpresas),
		regions: {},
		events: {
			'click 	.save': 'save',
			'click 	.saveAndContinue': 'saveAndContinue',
			'change #inputContrato': 'uniqueContrato',
		},
		ui: {
			inputId: '#inputId',
			inputEnderecoCombo: '.inputEnderecoCombo',
			inputNomeFantasia: '#inputNomeFantasia',
			inputCnpj: '#inputCnpj',
			inputContrato: '#inputContrato',
			inputRazaoSocial: '#inputRazaoSocial',
			inputEndereco: '#inputEndereco',
			inputNumeroEndereco: '#inputNumeroEndereco',
			inputComplemento: '#inputComplemento',
			inputUf: '#inputUf',
			inputCidade: '#inputCidade',
			inputBairro: '#inputBairro',
			inputCep: '#inputCep',
			inputReferenciaEndereco: '#inputReferenciaEndereco',
			inputDdd: '#inputDdd',
			inputFone: '#inputFone',
			inputUtilizaVoucher: '#inputUtilizaVoucher',
			inputUtilizaEticket: '#inputUtilizaEticket',
			inputObservacaoTaxista: '#inputObservacaoTaxista',
			inputObservacaoCentral: '#inputObservacaoCentral',
			inputObservacaoFinanceiro: '#inputObservacaoFinanceiro',
			inputPercentualDesconto: '#inputPercentualDesconto',
			inputInscricaoMunicipal: '#inputInscricaoMunicipal',
			inputInscricaoEstadual: '#inputInscricaoEstadual',
			inputEmiteNf: '#inputEmiteNf',
			inputPercentualIss: '#inputPercentualIss',
			inputPercentualIrf: '#inputPercentualIrf',
			inputPercentualInss: '#inputPercentualInss',
			inputDiaVencimento: '#inputDiaVencimento',
			inputPercentualMotorista: '#inputPercentualMotorista',
			inputBanco: '#inputBanco',
			inputAgencia: '#inputAgencia',
			inputConta: '#inputConta',
			inputEmail: '#inputEmail',
			inputUsuarioCadastro: '#inputUsuarioCadastro',
			inputDataCadastro: '#inputDataCadastro',
			groupInputDataCadastro: '#groupInputDataCadastro',
			inputStatusEmpresa: '#inputStatusEmpresa',
			inputNomeCliente1: '#inputNomeCliente1',
			inputNomeCliente2: '#inputNomeCliente2',
			inputFoneCliente1: '#inputFoneCliente1',
			inputFoneCliente2: '#inputFoneCliente2',
			inputNomeFornecedor1: '#inputNomeFornecedor1',
			inputNomeFornecedor2: '#inputNomeFornecedor2',
			inputFoneFornecedor1: '#inputFoneFornecedor1',
			inputFoneFornecedor2: '#inputFoneFornecedor2',
			form: '#formEmpresa',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		initialize: function() {
			var that = this;
			this.on('show', function() {
				this.enderecoPartida = new SuggestGmapBox({
					el: that.ui.inputEnderecoCombo,
					onSelect: function(endereco) {
						that.jsonValue = endereco;
						that.model.set(endereco);
						that.populaEndereco(endereco);
					},
				});
				this.ui.inputFone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.ui.inputFoneCliente1.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.ui.inputFoneCliente2.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.ui.inputFoneFornecedor1.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.ui.inputFoneFornecedor2.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.ui.inputCnpj.mask('99.999.999/9999-99');
				this.ui.inputCep.mask('99.999-999');
				this.ui.inputInscricaoMunicipal.mask('##############');
				this.ui.inputInscricaoEstadual.mask('##############');
				this.ui.inputPercentualDesconto.formatNumber(2);
				this.ui.inputPercentualIss.formatNumber(2);
				this.ui.inputPercentualIrf.formatNumber(2);
				this.ui.inputPercentualInss.formatNumber(2);
				this.ui.inputDiaVencimento.formatNumber(2);
				this.ui.inputPercentualMotorista.formatNumber(2);
				this.ui.inputUsuarioCadastro.formatNumber(2);
				this.ui.groupInputDataCadastro.datetimepicker({
					pickTime: true,
					language: 'pt_BR',
				});
				this.ui.inputDataCadastro.datetimepicker({
					pickTime: true,
					language: 'pt_BR',
				});
				this.ui.inputDataCadastro.mask('99/99/9999 99:99');
				this.ui.form.validationEngine('attach', {
					promptPosition: "topLeft",
					isOverflown: false,
					validationEventTrigger: "change"
				});
				this.comboBancos = new Combobox({
					el: this.ui.inputBanco,
					values: DadosUtils.BANCOS,
					comboId: 'codigo',
					comboVal: 'nome'
				})
				this.comboBancos.setValue(this.model.get('banco'));
				this.checkButtonAuthority();
			});
		},
		saveAndContinue: function() {
			this.save(true)
		},
		save: function(continua) {
			var that = this;
			var empresa = that.getModel();
			if (this.isValid()) {
				empresa.save({}, {
					success: function(_model, _resp, _options) {
						util.showSuccessMessage('Empresa salvo com sucesso!');
						that.clearForm();
						if (continua != true) {
							util.goPage('app/empresas');
						}
					},
					error: function(_model, _resp, _options) {
						if(_resp.status == '403'){
							util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
						} else {
							util.showErrorMessage('Problema ao salvar registro', _resp);
						}
					}
				});
			}
			else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},
		clearForm: function() {
			util.clear('inputId');
			util.clear('inputNomeFantasia');
			util.clear('inputCnpj');
			util.clear('inputContrato');
			util.clear('inputRazaoSocial');
			util.clear('inputEndereco');
			util.clear('inputNumeroEndereco');
			util.clear('inputComplemento');
			util.clear('inputUf');
			util.clear('inputCidade');
			util.clear('inputBairro');
			util.clear('inputCep');
			util.clear('inputReferenciaEndereco');
			util.clear('inputDdd');
			util.clear('inputFone');
			util.clear('inputUtilizaVoucher');
			util.clear('inputUtilizaEticket');
			util.clear('inputObservacaoTaxista');
			util.clear('inputObservacaoCentral');
			util.clear('inputObservacaoFinanceiro');
			util.clear('inputPercentualDesconto');
			util.clear('inputInscricaoMunicipal');
			util.clear('inputInscricaoEstadual');
			util.clear('inputEmiteNf');
			util.clear('inputPercentualIss');
			util.clear('inputPercentualIrf');
			util.clear('inputPercentualInss');
			util.clear('inputDiaVencimento');
			util.clear('inputPercentualMotorista');
			util.clear('inputBanco');
			util.clear('inputAgencia');
			util.clear('inputConta');
			util.clear('inputEmail');
			util.clear('inputUsuarioCadastro');
			util.clear('inputDataCadastro');
			util.clear('inputStatusEmpresa');
			util.clear('inputNomeCliente1');
			util.clear('inputNomeCliente2');
			util.clear('inputFoneCliente1');
			util.clear('inputFoneCliente2');
			util.clear('inputNomeFornecedor1');
			util.clear('inputNomeFornecedor2');
			util.clear('inputFoneFornecedor1');
			util.clear('inputFoneFornecedor2');
		},
		isValid: function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition: "topLeft",
				isOverflown: false,
				validationEventTrigger: "change"
			});
		},
		getModel: function() {
			var that = this;
			var empresa = that.model;
			empresa.set({
				id: util.escapeById('inputId') || null,
				nomeFantasia: util.escapeById('inputNomeFantasia'),
				cnpj: util.escapeById('inputCnpj'),
				contrato: util.escapeById('inputContrato'),
				razaoSocial: util.escapeById('inputRazaoSocial'),
				endereco: util.escapeById('inputEndereco'),
				numeroEndereco: util.escapeById('inputNumeroEndereco'),
				complemento: util.escapeById('inputComplemento'),
				uf: util.escapeById('inputUf'),
				cidade: util.escapeById('inputCidade'),
				bairro: util.escapeById('inputBairro'),
				cep: util.onlyNumber(util.escapeById('inputCep')),
				referenciaEndereco: util.escapeById('inputReferenciaEndereco'),
				ddd: util.escapeById('inputDdd'),
				fone: util.escapeById('inputFone'),
				utilizaVoucher: util.escapeById('inputUtilizaVoucher'),
				utilizaEticket: util.escapeById('inputUtilizaEticket'),
				observacaoTaxista: util.escapeById('inputObservacaoTaxista'),
				observacaoCentral: util.escapeById('inputObservacaoCentral'),
				observacaoFinanceiro: util.escapeById('inputObservacaoFinanceiro'),
				percentualDesconto: util.escapeById('inputPercentualDesconto', true),
				inscricaoMunicipal: util.escapeById('inputInscricaoMunicipal'),
				inscricaoEstadual: util.escapeById('inputInscricaoEstadual'),
				emiteNf: util.escapeById('inputEmiteNf'),
				percentualIss: util.escapeById('inputPercentualIss', true),
				percentualIrf: util.escapeById('inputPercentualIrf', true),
				percentualInss: util.escapeById('inputPercentualInss', true),
				diaVencimento: util.escapeById('inputDiaVencimento', true),
				percentualMotorista: util.escapeById('inputPercentualMotorista', true),
				banco: util.escapeById('inputBanco'),
				agencia: util.escapeById('inputAgencia'),
				conta: util.escapeById('inputConta'),
				email: util.escapeById('inputEmail'),
				usuarioCadastro: util.escapeById('inputUsuarioCadastro', true),
				dataCadastro: util.escapeById('inputDataCadastro'),
				statusEmpresa: util.escapeById('inputStatusEmpresa'),
				nomeCliente1: util.escapeById('inputNomeCliente1'),
				nomeCliente2: util.escapeById('inputNomeCliente2'),
				foneCliente1: util.escapeById('inputFoneCliente1'),
				foneCliente2: util.escapeById('inputFoneCliente2'),
				nomeFornecedor1: util.escapeById('inputNomeFornecedor1'),
				nomeFornecedor2: util.escapeById('inputNomeFornecedor2'),
				foneFornecedor1: util.escapeById('inputFoneFornecedor1'),
				foneFornecedor2: util.escapeById('inputFoneFornecedor2'),
			});
			return empresa;
		},
		populaEndereco: function(endereco) {
			this.ui.inputEndereco.val(endereco.logradouro);
			this.ui.inputNumeroEndereco.val(endereco.numero);
			this.ui.inputCep.val(endereco.cep);
			this.ui.inputBairro.val(endereco.bairro);
			this.ui.inputCidade.val(endereco.cidade);
			this.ui.inputUf.val(endereco.uf);
		},
		uniqueContrato: function() {
			var that = this;
			util.validateUnique({
				element: that.ui.inputContrato,
				fieldName: 'contrato',
				fieldDisplayName: 'Contrato',
				view: that,
				collection: EmpresaCollection,
			})
		},
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_EMPRESA_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
				if(e.authority == 'ROLE_EMPRESA_EDITAR' || e.authority == 'ROLE_ADMIN') {
					saveButton.show();
				}
			})
		},
	});
	return FormEmpresas;
});