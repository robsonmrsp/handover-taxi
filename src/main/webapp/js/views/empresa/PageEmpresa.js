/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var client = require('adapters/auth-adapter');
	var roles = client.roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');
	var CustomNumberCell = require('views/components/CustomNumberCell');
	var EmpresaModel = require('models/EmpresaModel');
	var EmpresaCollection = require('collections/EmpresaCollection');
	var EmpresaPageCollection = require('collections/EmpresaPageCollection');
	var PageEmpresaTemplate = require('text!views/empresa/tpl/PageEmpresaTemplate.html');;
	// End of "Import´s" definition
	var PageEmpresa = Marionette.LayoutView.extend({
		template: _.template(PageEmpresaTemplate),
		regions: {
			gridRegion: '#grid',
			counterRegion: '#counter',
			paginatorRegion: '#paginator',
		},
		events: {
			'click 	#reset': 'resetEmpresa',
			'keypress': 'treatKeypress',
			'click 	.search-button': 'searchEmpresa',
			'click .show-advanced-search-button': 'toggleAdvancedForm',
		},
		ui: {
			inputNomeFantasia: '#inputNomeFantasia',
			inputAvancadoNomeFantasia: '#inputAvancadoNomeFantasia',
			inputCnpj: '#inputCnpj',
			inputContrato: '#inputContrato',
			inputRazaoSocial: '#inputRazaoSocial',
			form: '#formEmpresaFilter',
			advancedSearchForm: '.advanced-search-form',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue', 
		},
		toggleAdvancedForm: function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},
		treatKeypress: function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchEmpresa();
			}
		},
		initialize: function() {
			var that = this;
			this.empresas = new EmpresaPageCollection();
			this.grid = new Backgrid.Grid({
				className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns: this.getColumns(),
				emptyText: "Sem registros",
				collection: this.empresas
			});
			this.counter = new Counter({
				collection: this.empresas,
			});
			this.paginator = new Backgrid.Extension.Paginator({
				columns: this.getColumns(this.roles),
				collection: this.empresas,
				className: ' paging_simple_numbers',
				uiClassName: 'pagination',
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				this.searchEmpresa();
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.ui.inputCnpj.mask('99.999.999/9999-99');
				this.checkButtonAuthority();
			});
		},
		searchEmpresa: function() {
			var that = this;
			if(client.empresa !== null){
				if (!util.escapeById('inputNomeFantasia')) {
					this.empresas.filterQueryParams = {
						id: client.empresa.id,
						nomeFantasia: util.escapeById('inputAvancadoNomeFantasia'),
						cnpj: util.escapeById('inputCnpj'),
						contrato: util.escapeById('inputContrato'),
						razaoSocial: util.escapeById('inputRazaoSocial'),
					}
				} else {
					this.empresas.filterQueryParams = {
						id: client.empresa.id,
						nomeFantasia: util.escapeById('inputNomeFantasia'),
					}
				} 
				this.empresas.fetch({
					data:{id:  client.empresa.id,},
					dataProcess: true,
					success: function(_coll, _resp, _opt) {
						console.info('Consulta para o grid empresa');
					},
					error: function(_coll, _resp, _opt) {
						if(_resp.status == '403'){
							util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
						} 
					}
				})
			} else {
				if (!util.escapeById('inputNomeFantasia')) {
					this.empresas.filterQueryParams = {
						nomeFantasia: util.escapeById('inputAvancadoNomeFantasia'),
						cnpj: util.escapeById('inputCnpj'),
						contrato: util.escapeById('inputContrato'),
						razaoSocial: util.escapeById('inputRazaoSocial'),
					}
				}
				else {
					this.empresas.filterQueryParams = {
						nomeFantasia: util.escapeById('inputNomeFantasia'),
					}
				} 
				this.empresas.fetch({
					success: function(_coll, _resp, _opt) {
						console.info('Consulta para o grid empresa');
					},
					error: function(_coll, _resp, _opt) {
						if(_resp.status == '403'){
							util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
						} 
					}
				})
			}
		},
		resetEmpresa: function() {
			this.ui.form.get(0).reset();
			this.empresas.reset();
		},
		getColumns: function() {
			var that = this;
			var columns = [{
				name: "nomeFantasia",
				editable: false,
				sortable: true,
				label: "Nome Fantasia",
				cell: "string",
			}, {
				name: "cnpj",
				editable: false,
				sortable: true,
				label: "CNPJ",
				cell: "string",
			}, {
				name: "contrato",
				editable: false,
				sortable: true,
				label: "Contrato",
				cell: "string",
			}, {
				name: "razaoSocial",
				editable: false,
				sortable: true,
				label: "Razão social",
				cell: "string",
			}, {
				name: "multipleActions",
				label: "Ações(Editar, Deletar)",
				sortable: false,
				cell: GeneralActionsCell.extend({
					className: 'multiple-actions',
					buttons: this.getCellButtons(),
					context: that,
				})
			}];
			return columns;
		},
		getCellButtons: function() {
			var that = this;
			var buttons = this.checkGridButtonAuthority();
			return buttons;
		},
		verFuncionarios: function(model) {
			util.goPage("app/empresa/" + model.get('id') + '/funcionarios');
		},
		verVouchers: function(model) {
			util.goPage("app/empresa/" + model.get('id') + '/faixaVouchers');
		},
		verCentroCustos: function(model) {
			util.goPage("app/empresa/" + model.get('id') + '/centroCustos');
		},
		verContatos: function(model) {
			util.goPage("app/empresa/" + model.get('id') + '/contatos');
		},
		verEnderecos: function(model) {
			util.goPage("app/empresa/" + model.get('id') + '/enderecos');
		},
		verMotoristasBloqueados: function(model) {
			util.goPage("app/empresa/" + model.get('id') + '/motoristaBloqueados');
		},
		verTelefones: function(model) {
			util.goPage("app/empresa/" + model.get('id') + '/telefoneFavoritos');
		},
		verSocios: function(model) {
			util.goPage("app/empresa/" + model.get('id') + '/socios');
		},
		deleteModel: function(model) {
			var that = this;
			var modelTipo = new EmpresaModel({
				id: model.id,
			});
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success: function() {
							that.empresas.remove(model);
							util.showSuccessMessage('Empresa removido com sucesso!');
						},
						error: function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro', _resp);
						}
					});
				}
			});
		},
		editModel: function(model) {
			util.goPage("app/editEmpresa/" + model.get('id'));
		},
		// vitoriano : chunck : check grid buttons authority
		checkGridButtonAuthority: function() {
			var that = this;
			var buttons = [];
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_CENTROCUSTO' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'centroCusto_button',
						type: 'primary',
						icon: 'fa-cc',
						hint: 'Centro de Custo',
						onClick: that.verCentroCustos,
					});
				}
				if (e.authority == 'ROLE_FUNCIONARIO' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'funcionarios_button',
						type: 'primary',
						icon: 'fa-users',
						hint: 'Funcionarios',
						onClick: that.verFuncionarios,
					});
				}if (e.authority == 'ROLE_CONTATO' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'contatos_button',
						type: 'primary',
						icon: 'fa-book',
						hint: 'Contatos',
						onClick: that.verContatos,
					});
				}
				if (e.authority == 'ROLE_SOCIO' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'socios_button',
						type: 'primary',
						icon: 'fa-user',
						hint: 'Socios',
						onClick: that.verSocios,
					});
				}
				if (e.authority == 'ROLE_VOUCHER' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'voucher_button',
						type: 'primary',
						icon: 'fa-ticket',
						hint: 'Vouchers',
						onClick: that.verVouchers,
					});
				}
				if (e.authority == 'ROLE_EMPRESA_EDITAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'edita_ficha_button',
						type: 'primary',
						icon: ' fa-pencil',
						hint: 'Editar Empresa',
						onClick: that.editModel,
					});
				}
				if (e.authority == 'ROLE_EMPRESA_DELETAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'delete_button',
						type: 'danger',
						icon: 'fa-trash',
						hint: 'Remover Empresa',
						onClick: that.deleteModel,
					});
				}
			})
			return buttons;
		},
		// vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_EMPRESA_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
	});
	return PageEmpresa;
});