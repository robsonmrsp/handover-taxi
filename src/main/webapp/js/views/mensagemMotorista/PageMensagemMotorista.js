/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var MensagemMotoristaModel = require('models/MensagemMotoristaModel');
	var MensagemMotoristaCollection = require('collections/MensagemMotoristaCollection');
	var MensagemMotoristaPageCollection = require('collections/MensagemMotoristaPageCollection');
	var PageMensagemMotoristaTemplate = require('text!views/mensagemMotorista/tpl/PageMensagemMotoristaTemplate.html');
	
	//Filter import
	var ModalMotorista = require('views/modalComponents/MotoristaModal');
	var ModalMensagem = require('views/modalComponents/MensagemModal');
	
	// End of "Import´s" definition

	var PageMensagemMotorista = Marionette.LayoutView.extend({
		template : _.template(PageMensagemMotoristaTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			modalMotoristaRegion : '#motoristaModal',
			modalMensagemRegion : '#mensagemModal',
		},
		
		events : {
			'click 	#reset' : 'resetMensagemMotorista',			
			'click #searchMotoristaModal' : 'showModalMotorista',
			'click #searchMensagemModal' : 'showModalMensagem',
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchMensagemMotorista',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
			inputDataEnvio : '#inputDataEnvio',
			groupInputDataEnvio : '#groupInputDataEnvio',
			inputDataLeitura : '#inputDataLeitura',
			groupInputDataLeitura : '#groupInputDataLeitura',
		
			inputMotoristaId : '#inputMotoristaId',
			inputMotoristaNome : '#inputMotoristaNome',
			inputMensagemId : '#inputMensagemId',
			inputMensagemNome : '#inputMensagemNome',
			form : '#formMensagemMotoristaFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchMensagemMotorista();
	    	}
		},

		initialize : function() {
			var that = this;

			this.mensagemMotoristas = new MensagemMotoristaPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.mensagemMotoristas
			});

			this.counter = new Counter({
				collection : this.mensagemMotoristas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.mensagemMotoristas,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.mensagemMotoristas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid mensagemMotorista');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
				}
			});
			this.modalMotorista = new ModalMotorista({
				onSelectModel : function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.modalMensagem = new ModalMensagem({
				onSelectModel : function(model) {
					that.onSelectMensagem(model);
				},
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.modalMotoristaRegion.show(this.modalMotorista);		
				this.modalMensagemRegion.show(this.modalMensagem);		
				this.ui.groupInputDataEnvio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataEnvio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataEnvio.mask('99/99/9999 99:99');
				this.ui.groupInputDataLeitura.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataLeitura.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataLeitura.mask('99/99/9999 99:99');
		
			});
		},
		 
		searchMensagemMotorista : function(){
			var that = this;

			this.mensagemMotoristas.filterQueryParams = {
	    		dataEnvio : util.escapeById('inputDataEnvio'),
	    		dataLeitura : util.escapeById('inputDataLeitura'),
			    motorista : util.escapeById('inputMotoristaId'), 
			    mensagem : util.escapeById('inputMensagemId'), 
			}
			this.mensagemMotoristas.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid mensagemMotorista');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetMensagemMotorista : function(){
			this.ui.form.get(0).reset();
			this.mensagemMotoristas.reset();
			util.clear('inputMotoristaId');
			util.clear('inputMensagemId');
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "dataEnvio",
				editable : false,
				sortable : true,
				label 	 : "Data envio",
				cell 	 : "string",
			}, 
			/*
			{
				name : "dataLeitura",
				editable : false,
				sortable : true,
				label 	 : "Data leitura",
				cell 	 : "string",
			},*/ 
			{
				name : "motorista.nome",
				editable : false,
				sortable : true,  
				label : "Motorista",
				cell : CustomStringCell.extend({
					fieldName : 'motorista.nome',
				}),
			},	
			{
				name : "mensagem.nome",
				editable : false,
				sortable : true,  
				label : "Mensagem",
				cell : CustomStringCell.extend({
					fieldName : 'mensagem.descricao',
				}),
			},/*	
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			}*/ ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Mensagem motorista',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Mensagem motorista',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new MensagemMotoristaModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.mensagemMotoristas.remove(model);
							util.showSuccessMessage('Mensagem motorista removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editMensagemMotorista/" + model.get('id'));
		},

		showModalMotorista : function() {
			this.modalMotorista.showPage();
		},
			
		onSelectMotorista : function(motorista) {
			this.modalMotorista.hidePage();	
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));		
		},
		showModalMensagem : function() {
			this.modalMensagem.showPage();
		},
			
		onSelectMensagem : function(mensagem) {
			this.modalMensagem.hidePage();	
			this.ui.inputMensagemId.val(mensagem.get('id'));
			this.ui.inputMensagemNome.val(mensagem.get('nome'));		
		},
		

	});

	return PageMensagemMotorista;
});
