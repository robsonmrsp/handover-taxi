/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectMensagemMotorista = require('views/mensagemMotorista/ModalMultiSelectMensagemMotorista');
	var MultiSelectMensagemMotoristaTemplate = require('text!views/mensagemMotorista/tpl/MultiSelectMensagemMotoristaTemplate.html');

	var MultiSelectMensagemMotorista = Marionette.LayoutView.extend({
		template : _.template(MultiSelectMensagemMotoristaTemplate),

		regions : {
			modalMultiSelectMensagemMotoristaRegion : '#modalMultiSelectMensagemMotoristas',
			gridMensagemMotoristasModalRegion : '#gridMultiselectMensagemMotoristas',
		},

		initialize : function() {
			var that = this;

			this.mensagemMotoristas = this.collection;

			this.gridMensagemMotoristas = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.mensagemMotoristas,
			});

			this.modalMultiSelectMensagemMotorista = new ModalMultiSelectMensagemMotorista({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectMensagemMotoristaRegion.show(that.modalMultiSelectMensagemMotorista);
				that.gridMensagemMotoristasModalRegion.show(that.gridMensagemMotoristas);
			});
		},
		clear : function(){
			this.modalMultiSelectMensagemMotorista.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "dataEnvio",
				editable : false,
				sortable : false,
				label 	 : "Data envio",
				cell 	 : "string",
			}, 
			{
				name : "dataLeitura",
				editable : false,
				sortable : false,
				label 	 : "Data leitura",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectMensagemMotorista
});
