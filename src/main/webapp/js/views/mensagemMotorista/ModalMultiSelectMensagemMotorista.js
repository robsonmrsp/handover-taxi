/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var MensagemMotoristaPageCollection = require('collections/MensagemMotoristaPageCollection');
	var ModalMultiSelectMensagemMotoristaTemplate = require('text!views/mensagemMotorista/tpl/ModalMultiSelectMensagemMotoristaTemplate.html');
	// End of "Import´s" definition

	var ModalMensagemMotoristas = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectMensagemMotoristaTemplate),

		regions : {
			gridRegion : '#grid-mensagemMotoristas-modal',
			paginatorRegion : '#paginator-mensagemMotoristas-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoMensagemMotoristas = this.collection;
			
			this.mensagemMotoristas = new MensagemMotoristaPageCollection();
			this.mensagemMotoristas.on('fetched', this.endFetch, this);
			this.mensagemMotoristas.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.mensagemMotoristas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.mensagemMotoristas,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.mensagemMotoristas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid mensagemMotorista');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoMensagemMotoristas.add(model)
			else
				this.projetoMensagemMotoristas.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.mensagemMotoristas.each(function(model) {
				if (that.projetoMensagemMotoristas.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "dataEnvio",
				editable : false,
				sortable : false,
				label 	 : "Data envio",
				cell 	 : "string",
			}, 
			{
				name : "dataLeitura",
				editable : false,
				sortable : false,
				label 	 : "Data leitura",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalMensagemMotoristas;
});
