/* generated: 16/10/2016 15:27:08 */
define(function(require) {
    // Start "Import´s" Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var TemplateFormMensagemMotoristas = require('text!views/mensagemMotorista/tpl/FormMensagemMotoristaTemplate.html');
    var MensagemMotoristaModel = require('models/MensagemMotoristaModel');
    var MensagemMotoristaCollection = require('collections/MensagemMotoristaCollection');
    var ModalMotorista = require('views/modalComponents/MotoristaModal');
    var ModalMensagem = require('views/modalComponents/MensagemModal');
    var ItemMotoristaTemplate = require('text!views/mensagemMotorista/tpl/ItemMotoristaTemplate.html');
    var ItemMensagemTemplate = require('text!views/mensagemMotorista/tpl/ItemMensagemTemplate.html');
    var MotoristaCollection = require('collections/MotoristaCollection');
    // End of "Import´s" definition

    var motoqueiroKey = null;
    var centralKey = 'CENTRAL00001';
    var firebaseRef = new Firebase("https://chat-eeb48.firebaseio.com/");

    // #####################################################################################################
    // ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
    // BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
    // #####################################################################################################


    var CollectionViewMotoristas = Marionette.CollectionView.extend({

        childView: Marionette.ItemView.extend({
            template: _.template(ItemMotoristaTemplate),
            ui: {
                motoqueiro: '.motoqueiro'
            },
            events: {
                'click': 'onClickMotorista',
            },

            initialize: function(options) {
                this.onSelectMotorista = options.onSelectMotorista;
                this.parent = options.parent;
            },
            
            onClickMotorista: function(e) {
                $('.memberdiv').css('background-color', '');
                $(e.target).closest('.memberdiv').css('background-color', '#f1f5f9');
                // $('.sendMensagem').prop("disabled", false);
                //this.parent.ui.inputTextoMensagem.prop('disabled', false);

                // atualiza o id do motorista no form de envio de msg
                var motoqueiro_id = this.ui.motoqueiro.attr('data-motoqueiro-id');
                this.parent.ui.inputMotoristaId.val(motoqueiro_id);

                if (this.onSelectMotorista) {
                    this.onSelectMotorista(this.model);
                }
            }
        }),

        // opções que serão passadas para cada um dos clientes criados.
        childViewOptions: function(model, index) {
            // do some calculations based on the model
            return {
                onSelectMotorista: this.onSelectMotorista,
                parent: this.parent
            }
        },

        initialize: function(options) {
            this.onSelectMotorista = options.onSelectMotorista;
            this.parent = options.parent;
        }

    });

    // mensagem model
    var Mensagem = Backbone.Model.extend({});

    // mensagem collection view
    var CollectionViewMensagens = Marionette.CollectionView.extend({

        childView: Marionette.ItemView.extend({
            template: _.template(ItemMensagemTemplate),
            ui: {

            },
            events: {

            },

            initialize: function(options) {

            }
        }),

        childViewOptions: function(model, index) {
            return {};
        },
        initialize: function(options) {

        }

    });

    // message collection : nao esta sendo referenciado
    var MensagemCollection = Backbone.Firebase.Collection.extend({
        url: firebaseRef.child("messages/4:CENTRAL00001"),
        model: Mensagem
    });

    // aqui começa a definição do formulario propriamente dito
    var FormMensagemMotoristas = Marionette.LayoutView.extend({
        template: _.template(TemplateFormMensagemMotoristas),

        regions: {
            modalMotoristaRegion: '#motoristaModal',
            modalMensagemRegion: '#mensagemModal',
            motoristasRegion: '.js-motoqueiros',
            mensagensRegion: '.js-mensagens',
            formEnviarMensagem: '#formEnviarMensagem'
        },

        events: {
            'click 	.save': 'save',
            'click 	.reset-motorista' : 'resetMotoqueiros',
            'click 	.saveAndContinue': 'saveAndContinue',
            'click #searchMotoristaModal': 'showModalMotorista',
            'click #searchMensagemModal': 'showModalMensagem',
            'click .sendMensagem': 'sendMensagem',
            'click .search-motorista': 'searchMotorista',
            'keypress #inputTextoMensagem': 'sendMensagemOnEnter',
            'click .show-advanced-search-button': 'toggleAdvancedForm',
        },

        ui: {
            inputId: '#inputId',
            inputDataEnvio: '#inputDataEnvio',
            groupInputDataEnvio: '#groupInputDataEnvio',
            inputDataLeitura: '#inputDataLeitura',
            groupInputDataLeitura: '#groupInputDataLeitura',

            inputMotoristaId: '#inputMotoristaId',
            inputMotoristaNome: '#inputMotoristaNome',
            inputMensagemId: '#inputMensagemId',
            inputMensagemNome: '#inputMensagemNome',
            form: '#formMensagemMotorista',
            advancedSearchForm: '.advanced-search-form',
            inputTextoMensagem: '#inputTextoMensagem',
            sendMensagemBtn: '.sendMensagem',
            inputNomeMotorista: '#inputNomeMotorista',
        },

        treatKeypress: function(e) {
            if (util.enterPressed(e)) {
                e.preventDefault();
                this.searchMotorista();
            }
        },

        searchMotorista: function() {
            var that = this;
            this.motoristas = new MotoristaCollection();
            this.motoristas.filter({   
            	success : function ( coll, resp, _opt )  {
                    console.info('Consulta para o grid motorista') ;
                },
                error : function ( coll, resp, _opt) {
                 console.error(_resp.responseText || (resp.getResponseHeader && resp.getResponseHeader('exception' ) )  )  ;
                },
                data : {
                	nome : util.escapeById('inputMotoristaNome'),
                 },
               })
            
              this.collectionViewMotoristas = new CollectionViewMotoristas({
                collection: this.motoristas,
                onSelectMotorista: function(motoristaSelecionado) {
                    // remove a referência ao motoqueiro anterior
                    if (motoqueiroKey != null) {
                        var messageKey = motoqueiroKey + ':' + centralKey;
                        firebaseRef.child('messages/' + messageKey).off("child_added");
                    }

                    motoqueiroKey = motoristaSelecionado.get('id');
                    var messageKey = motoqueiroKey + ':' + centralKey;


                    showConversa(messageKey);
                },
                parent: this
            });   
            this.motoristasRegion.show(this.collectionViewMotoristas);
            this.mensagensRegion.show(new CollectionViewMensagens());
            // mostra conversa
            var showConversa = function(message_key) {
                var mensagemMotoristaCollection = new MensagemMotoristaCollection;
                mensagemMotoristaCollection.url = 'rs/crud/mensagemMotoristas/motorista/' + that.ui.inputMotoristaId.val();
                mensagemMotoristaCollection.fetch({
                    success: function(collection, response, options) {
                        var count = 1;
                        var lastMessageId = null;
                        var $mensagens = $('.js-mensagens');
                        $mensagens.empty();

                        collection.each(function(mensagemMotorista) {
                            if (count == collection.size()) {
                                lastMessageId = mensagemMotorista.get('id');
                                return;
                            }

                            var message = {
                                id: mensagemMotorista.get('id'),
                                timestamp: mensagemMotorista.get('dataEnvio'),
                                from: 'central',
                                text: mensagemMotorista.get('mensagem').descricao
                            };

                            $mensagens.append(_.template(ItemMensagemTemplate, message));

                            count += 1;
                        });

                        $('.js-mensagens-scroll').animate({
                            scrollTop: $mensagens.height()
                        }, 'fast');

                        if (lastMessageId != null) {
                            firebaseRef.child('messages/' + message_key).orderByKey().startAt(lastMessageId.toString()).on('child_added', function(snapshot) {
                                var message = snapshot.val();
                                message.timestamp = moment.unix(message.timestamp).format("DD/MM/YYYY h:mm:ss");
                                $mensagens.append(_.template(ItemMensagemTemplate, message));

                                $('.js-mensagens-scroll').animate({
                                    scrollTop: $mensagens.height()
                                }, 'fast');
                            });
                        } else {
                            firebaseRef.child('messages/' + message_key).on('child_added', function(snapshot) {
                                var message = snapshot.val();
                                message.timestamp = moment.unix(message.timestamp).format("DD/MM/YYYY hh:mm:ss");
                                $mensagens.append(_.template(ItemMensagemTemplate, message));

                                $('.js-mensagens-scroll').animate({
                                    scrollTop: $mensagens.height()
                                }, 'fast');
                            });
                        }
                    },
                    error: function(collection, response, option) {
                        console.log('Erro ao retorna as mensagens do motorista');
                    }
                });
            };
            
        },

        initialize: function() {
            var that = this;

            this.motoristas = new MotoristaCollection();

            this.motoristas.fetch();

            this.collectionViewMotoristas = new CollectionViewMotoristas({
                collection: this.motoristas,
                onSelectMotorista: function(motoristaSelecionado) {
                    // remove a referência ao motoqueiro anterior
                    if (motoqueiroKey != null) {
                        var messageKey = motoqueiroKey + ':' + centralKey;
                        firebaseRef.child('messages/' + messageKey).off("child_added");
                    }

                    motoqueiroKey = motoristaSelecionado.get('id');
                    var messageKey = motoqueiroKey + ':' + centralKey;


                    showConversa(messageKey);
                },
                parent: this
            });


            this.modalMotorista = new ModalMotorista({
                onSelectModel: function(model) {
                    that.onSelectMotorista(model);
                },
            });
            
            this.modalMensagem = new ModalMensagem({
                onSelectModel: function(model) {
                    that.onSelectMensagem(model);
                },
            });
            
            this.on('show', function() {
                this.modalMotoristaRegion.show(this.modalMotorista);
                this.motoristasRegion.show(this.collectionViewMotoristas);
                this.modalMensagemRegion.show(this.modalMensagem);

                this.ui.groupInputDataEnvio.datetimepicker({
                    pickTime: true,
                    language: 'pt_BR',
                });
                
                this.ui.inputDataEnvio.datetimepicker({
                    pickTime: true,
                    language: 'pt_BR',
                });
                
                this.ui.inputDataEnvio.mask('99/99/9999 99:99');
                
                this.ui.groupInputDataLeitura.datetimepicker({
                    pickTime: true,
                    language: 'pt_BR',
                });
                
                this.ui.inputDataLeitura.datetimepicker({
                    pickTime: true,
                    language: 'pt_BR',
                });
                
                this.ui.inputDataLeitura.mask('99/99/9999 99:99');

                this.ui.form.validationEngine('attach', {
                    promptPosition: "topLeft",
                    isOverflown: false,
                    validationEventTrigger: "change"
                });
            });

            // mostra conversa
            var showConversa = function(message_key) {
                var mensagemMotoristaCollection = new MensagemMotoristaCollection;
                mensagemMotoristaCollection.url = 'rs/crud/mensagemMotoristas/motorista/' + that.ui.inputMotoristaId.val();
                mensagemMotoristaCollection.fetch({
                    success: function(collection, response, options) {
                        var count = 1;
                        var lastMessageId = null;
                        var $mensagens = $('.js-mensagens');
                        $mensagens.empty();

                        collection.each(function(mensagemMotorista) {
                            if (count == collection.size()) {
                                lastMessageId = mensagemMotorista.get('id');
                                return;
                            }

                            var message = {
                                id: mensagemMotorista.get('id'),
                                timestamp: mensagemMotorista.get('dataEnvio'),
                                from: 'central',
                                text: mensagemMotorista.get('mensagem').descricao
                            };

                            $mensagens.append(_.template(ItemMensagemTemplate, message));

                            count += 1;
                        });

                        $('.js-mensagens-scroll').animate({
                            scrollTop: $mensagens.height()
                        }, 'fast');

                        if (lastMessageId != null) {
                            firebaseRef.child('messages/' + message_key).orderByKey().startAt(lastMessageId.toString()).on('child_added', function(snapshot) {
                                var message = snapshot.val();
                                message.timestamp = moment.unix(message.timestamp).format("DD/MM/YYYY h:mm:ss");
                                $mensagens.append(_.template(ItemMensagemTemplate, message));

                                $('.js-mensagens-scroll').animate({
                                    scrollTop: $mensagens.height()
                                }, 'fast');
                            });
                        } else {
                            firebaseRef.child('messages/' + message_key).on('child_added', function(snapshot) {
                                var message = snapshot.val();
                                message.timestamp = moment.unix(message.timestamp).format("DD/MM/YYYY hh:mm:ss");
                                $mensagens.append(_.template(ItemMensagemTemplate, message));

                                $('.js-mensagens-scroll').animate({
                                    scrollTop: $mensagens.height()
                                }, 'fast');
                            });
                        }
                    },
                    error: function(collection, response, option) {
                        console.log('Erro ao retorna as mensagens do motorista');
                    }
                });
            };
        },

        toggleAdvancedForm: function() {
            this.ui.advancedSearchForm.slideToggle("slow");
        },

        saveAndContinue: function() {
            this.save(true)
        },

        save: function(continua) {
            // dataEnvio, dataLeitura, motorista, mensagem
            var that = this;
            var mensagemMotorista = that.getModel();

            if (this.isValid()) {
                mensagemMotorista.save({}, {
                    success: function(_model, _resp, _options) {
                        util.showSuccessMessage('Mensagem motorista salvo com sucesso!');
                        that.clearForm();

                        if (continua != true) {
                            util.goPage('app/mensagemMotoristas');
                        }
                    },

                    error: function(_model, _resp, _options) {
                        util.showErrorMessage('Problema ao salvar registro', _resp);
                    }
                });
            } else {
                util.showMessage('error', 'Verifique campos em destaque!');
            }
        },

        // vitoriano : envia mensagem para todos os motoqueiros em collectionViewMotoristas
        sendMensagem: function() {
            var that = this;
        	if (that.ui.inputTextoMensagem.val().trim() == ""){
            	return false;
            }
        	
            var collection = this.collectionViewMotoristas.collection;
            var mensagem = that.ui.inputTextoMensagem.val();
            
            collection.each(function(model){
                mensagemMotorista = that.getModel(model.id);
            
                mensagemMotorista.save({}, {
                    success: function(_model, _resp, _options) {
                        // envia a mensagem
                        var messagesRef = firebaseRef.child("messages");
                        var messageKey = model.id + ':' + centralKey;

                        var message = {
                            'id': _model.get('id'),
                            'text': mensagem,
                            'timestamp': moment(_model.get('dataEnvio'), "DD/MM/YYYY hh:mm:ss").unix()
                        };
                        messagesRef.child(messageKey).child(_model.get('id')).set(message);
                    },

                    error: function(_model, _resp, _options) {
                        console.log('erro', _resp);
                    }
                });
            });
            that.ui.inputTextoMensagem.val('');
        },

        sendMensagemOnEnter: function(e) {
            if (e.which == 13) {
                e.preventDefault();
                this.ui.sendMensagemBtn.click();
            }
        },

        clearForm: function() {
            util.clear('inputId');
            util.clear('inputDataEnvio');
            util.clear('inputDataLeitura');
            util.clear('inputMotoristaId');
            util.clear('inputMotoristaNome');
            util.clear('inputMensagemId');
            util.clear('inputMensagemNome');
        },

        isValid: function() {
            return this.ui.form.validationEngine('validate', {
                promptPosition: "topLeft",
                isOverflown: false,
                validationEventTrigger: "change"
            });
        },

        getModel: function(idMotoqueiro) {
            var that = this;
            var mensagemMotorista = that.model;

            mensagemMotorista.set({
                id: null,
                dataEnvio: null,
                dataLeitura: null,
                motorista: {
                    id: idMotoqueiro,
                },
                mensagem: {
                    id: null,
                    descricao: that.ui.inputTextoMensagem.val()
                },
            });

            return mensagemMotorista;
        },

        // vitoriano : refatorar
        resetMotoqueiros : function() {
        	 this.motoristas = new MotoristaCollection();
        	 var that = this;
             this.motoristas.fetch();
             
             this.collectionViewMotoristas = new CollectionViewMotoristas({
                 collection: this.motoristas,
                 onSelectMotorista: function(motoristaSelecionado) {
                     // remove a referência ao motoqueiro anterior
                     if (motoqueiroKey != null) {
                         var messageKey = motoqueiroKey + ':' + centralKey;
                         firebaseRef.child('messages/' + messageKey).off("child_added");
                     }

                     motoqueiroKey = motoristaSelecionado.get('id');
                     var messageKey = motoqueiroKey + ':' + centralKey;


                     showConversa(messageKey);
                 },
                 parent: this
             });
             this.motoristasRegion.show(this.collectionViewMotoristas);
             this.mensagensRegion.show(new CollectionViewMensagens());
             
             // mostra conversa
             var showConversa = function(message_key) {
                 var mensagemMotoristaCollection = new MensagemMotoristaCollection;
                 mensagemMotoristaCollection.url = 'rs/crud/mensagemMotoristas/motorista/' + that.ui.inputMotoristaId.val();
                 mensagemMotoristaCollection.fetch({
                     success: function(collection, response, options) {
                         var count = 1;
                         var lastMessageId = null;
                         var $mensagens = $('.js-mensagens');
                         $mensagens.empty();

                         collection.each(function(mensagemMotorista) {
                             if (count == collection.size()) {
                                 lastMessageId = mensagemMotorista.get('id');
                                 return;
                             }

                             var message = {
                                 id: mensagemMotorista.get('id'),
                                 timestamp: mensagemMotorista.get('dataEnvio'),
                                 from: 'central',
                                 text: mensagemMotorista.get('mensagem').descricao
                             };

                             $mensagens.append(_.template(ItemMensagemTemplate, message));

                             count += 1;
                         });

                         $('.js-mensagens-scroll').animate({
                             scrollTop: $mensagens.height()
                         }, 'fast');

                         if (lastMessageId != null) {
                             firebaseRef.child('messages/' + message_key).orderByKey().startAt(lastMessageId.toString()).on('child_added', function(snapshot) {
                                 var message = snapshot.val();
                                 message.timestamp = moment.unix(message.timestamp).format("DD/MM/YYYY h:mm:ss");
                                 $mensagens.append(_.template(ItemMensagemTemplate, message));

                                 $('.js-mensagens-scroll').animate({
                                     scrollTop: $mensagens.height()
                                 }, 'fast');
                             });
                         } else {
                             firebaseRef.child('messages/' + message_key).on('child_added', function(snapshot) {
                                 var message = snapshot.val();
                                 message.timestamp = moment.unix(message.timestamp).format("DD/MM/YYYY hh:mm:ss");
                                 $mensagens.append(_.template(ItemMensagemTemplate, message));

                                 $('.js-mensagens-scroll').animate({
                                     scrollTop: $mensagens.height()
                                 }, 'fast');
                             });
                         }
                     },
                     error: function(collection, response, option) {
                         console.log('Erro ao retorna as mensagens do motorista');
                     }
                 });
             };
		},
        
        showModalMotorista: function() {
            // add more before the modal is open
            this.modalMotorista.showPage();
        },
    
        showModalMensagem: function() {
            // add more before the modal is open
            this.modalMensagem.showPage();
        },

        onSelectMotorista: function(motorista) {
            this.modalMotorista.hidePage();
            this.ui.inputMotoristaId.val(motorista.get('id'));
            this.ui.inputMotoristaNome.val(motorista.get('nome'));
            this.searchMotorista();
           
        },
        
        onSelectMensagem: function(mensagem) {
            this.modalMensagem.hidePage();
            this.ui.inputMensagemId.val(mensagem.get('id'));
            this.ui.inputMensagemNome.val(mensagem.get('nome'));
        },

    });

    return FormMensagemMotoristas;
});