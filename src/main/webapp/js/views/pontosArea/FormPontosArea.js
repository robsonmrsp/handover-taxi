/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormPontosAreas = require('text!views/pontosArea/tpl/FormPontosAreaTemplate.html');
	var PontosAreaModel = require('models/PontosAreaModel');
	var PontosAreaCollection = require('collections/PontosAreaCollection');
	var ModalArea = require('views/modalComponents/AreaModal');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormPontosAreas = Marionette.LayoutView.extend({
		template : _.template(TemplateFormPontosAreas),

		regions : {
			modalAreaRegion : '#areaModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click #searchAreaModal' : 'showModalArea',
		},
		
		ui : {
			inputId : '#inputId',
			inputLatitude : '#inputLatitude',
			inputLongitude : '#inputLongitude',
		
			inputAreaId : '#inputAreaId',
			inputAreaNome : '#inputAreaNome',
			form : '#formPontosArea',
		},

		initialize : function() {
			var that = this;
			this.modalArea = new ModalArea({
				onSelectModel : function(model) {
					that.onSelectArea(model);
				},
			});
			this.on('show', function() {
				this.modalAreaRegion.show(this.modalArea);		
		
				this.ui.inputLatitude.formatNumber(2);
				this.ui.inputLongitude.formatNumber(2);
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var pontosArea = that.getModel();

			if (this.isValid()) {
				pontosArea.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Pontos area salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/pontosAreas');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputLatitude'); 
			util.clear('inputLongitude'); 
			util.clear('inputAreaId');
			util.clear('inputAreaNome');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var pontosArea = that.model; 
			pontosArea.set({
				id: util.escapeById('inputId') || null,
		    	latitude : util.escapeById('inputLatitude', true), 
		    	longitude : util.escapeById('inputLongitude', true), 
				area : that.modalArea.getJsonValue(),
			});
			return pontosArea;
		},
		 		
		showModalArea : function() {
			// add more before the modal is open
			this.modalArea.showPage();
		},

		onSelectArea : function(area) {
			this.modalArea.hidePage();	
			this.ui.inputAreaId.val(area.get('id'));
			this.ui.inputAreaNome.val(area.get('nome'));		
		},
				
		
	});

	return FormPontosAreas;
});