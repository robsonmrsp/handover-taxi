/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var PontosAreaModel = require('models/PontosAreaModel');
	var PontosAreaCollection = require('collections/PontosAreaCollection');
	var PontosAreaPageCollection = require('collections/PontosAreaPageCollection');
	var PagePontosAreaTemplate = require('text!views/pontosArea/tpl/PagePontosAreaTemplate.html');
	
	//Filter import
	var ModalArea = require('views/modalComponents/AreaModal');
	
	// End of "Import´s" definition

	var PagePontosArea = Marionette.LayoutView.extend({
		template : _.template(PagePontosAreaTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			modalAreaRegion : '#areaModal',
		},
		
		events : {
			'click 	#reset' : 'resetPontosArea',			
			'click #searchAreaModal' : 'showModalArea',
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchPontosArea',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
			inputLatitude : '#inputLatitude',
			inputLongitude : '#inputLongitude',
		
			inputAreaId : '#inputAreaId',
			inputAreaNome : '#inputAreaNome',
			form : '#formPontosAreaFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchPontosArea();
	    	}
		},

		initialize : function() {
			var that = this;

			this.pontosAreas = new PontosAreaPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.pontosAreas
			});

			this.counter = new Counter({
				collection : this.pontosAreas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.pontosAreas,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.pontosAreas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid pontosArea');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
				}
			});
			this.modalArea = new ModalArea({
				onSelectModel : function(model) {
					that.onSelectArea(model);
				},
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.modalAreaRegion.show(this.modalArea);		
				this.ui.inputLatitude.formatNumber(2);
				this.ui.inputLongitude.formatNumber(2);
		
			});
		},
		 
		searchPontosArea : function(){
			var that = this;

			this.pontosAreas.filterQueryParams = {
	    		latitude : util.escapeById('inputLatitude'),
	    		longitude : util.escapeById('inputLongitude'),
			    area : util.escapeById('inputAreaId'), 
			}
			this.pontosAreas.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid pontosArea');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetPontosArea : function(){
			this.ui.form.get(0).reset();
			this.pontosAreas.reset();
			util.clear('inputAreaId');
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "latitude",
				editable : false,
				sortable : true,
				label 	 : "Latitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "longitude",
				editable : false,
				sortable : true,
				label 	 : "Longitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "area.nome",
				editable : false,
				sortable : true,  
				label : "Area",
				cell : CustomStringCell.extend({
					fieldName : 'area.nome',
				}),
			},	
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Pontos area',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Pontos area',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new PontosAreaModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.pontosAreas.remove(model);
							util.showSuccessMessage('Pontos area removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editPontosArea/" + model.get('id'));
		},

		showModalArea : function() {
			this.modalArea.showPage();
		},
			
		onSelectArea : function(area) {
			this.modalArea.hidePage();	
			this.ui.inputAreaId.val(area.get('id'));
			this.ui.inputAreaNome.val(area.get('nome'));		
		},
		

	});

	return PagePontosArea;
});
