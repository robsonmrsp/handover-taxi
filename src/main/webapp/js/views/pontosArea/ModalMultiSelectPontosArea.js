/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var PontosAreaPageCollection = require('collections/PontosAreaPageCollection');
	var ModalMultiSelectPontosAreaTemplate = require('text!views/pontosArea/tpl/ModalMultiSelectPontosAreaTemplate.html');
	// End of "Import´s" definition

	var ModalPontosAreas = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectPontosAreaTemplate),

		regions : {
			gridRegion : '#grid-pontosAreas-modal',
			paginatorRegion : '#paginator-pontosAreas-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoPontosAreas = this.collection;
			
			this.pontosAreas = new PontosAreaPageCollection();
			this.pontosAreas.on('fetched', this.endFetch, this);
			this.pontosAreas.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.pontosAreas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.pontosAreas,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.pontosAreas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid pontosArea');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoPontosAreas.add(model)
			else
				this.projetoPontosAreas.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.pontosAreas.each(function(model) {
				if (that.projetoPontosAreas.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "latitude",
				editable : false,
				sortable : false,
				label 	 : "Latitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "longitude",
				editable : false,
				sortable : false,
				label 	 : "Longitude",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return ModalPontosAreas;
});
