/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectPontosArea = require('views/pontosArea/ModalMultiSelectPontosArea');
	var MultiSelectPontosAreaTemplate = require('text!views/pontosArea/tpl/MultiSelectPontosAreaTemplate.html');

	var MultiSelectPontosArea = Marionette.LayoutView.extend({
		template : _.template(MultiSelectPontosAreaTemplate),

		regions : {
			modalMultiSelectPontosAreaRegion : '#modalMultiSelectPontosAreas',
			gridPontosAreasModalRegion : '#gridMultiselectPontosAreas',
		},

		initialize : function() {
			var that = this;

			this.pontosAreas = this.collection;

			this.gridPontosAreas = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.pontosAreas,
			});

			this.modalMultiSelectPontosArea = new ModalMultiSelectPontosArea({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectPontosAreaRegion.show(that.modalMultiSelectPontosArea);
				that.gridPontosAreasModalRegion.show(that.gridPontosAreas);
			});
		},
		clear : function(){
			this.modalMultiSelectPontosArea.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "latitude",
				editable : false,
				sortable : false,
				label 	 : "Latitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "longitude",
				editable : false,
				sortable : false,
				label 	 : "Longitude",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return MultiSelectPontosArea
});
