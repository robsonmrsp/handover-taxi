/* generated: 15/09/2016 17:18:58 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalTemplate = require('text!views/user/tpl/ChangePasswordModalTemplate.html');
	var UserPageCollection = require('collections/UserPageCollection');
	var UserModel = require('models/UserModel');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var UserModal = Marionette.LayoutView.extend({
		template : _.template(ModalTemplate),

		events : {
			'click .save-password' : 'savePassword',
			'blur .password' : 'confirmPassword',

		},

		regions : {

		},

		savePassword : function() {
			if (this.ui.inputModalNewPassword.val() == this.ui.inputModalConfirmPassword.val()) {
				this.currentUser.set('password', util.escapeById('inputModalConfirmPassword'));
				this.currentUser.changePassword({
					success : function(model) {
						util.showMessage('info', 'Senha alterada com sucesso!', 'message-modal-user');
					},
					error : function(x, y, z) {
						util.showMessage('error', 'Erro ao salvar usuário!', 'message-modal-user');
					}
				});
			} else {
				util.showMessage('error', 'Senha e confirmação não conferem!', 'message-modal-user');
			}
		},

		confirmPassword : function() {

			if (this.ui.inputModalNewPassword.val() !== this.ui.inputModalConfirmPassword.val()) {
				this.ui.inputModalNewPassword.addClass('btn-white').addClass('btn-danger');
				this.ui.inputModalConfirmPassword.addClass('btn-white').addClass('btn-danger');
				// util.showMessage('error', 'Senhas nova e antiga não //
				// conferem!','message-modal-user');
			} else {
				this.ui.inputModalNewPassword.removeClass('btn-white').removeClass('btn-danger')
				this.ui.inputModalConfirmPassword.removeClass('btn-white').removeClass('btn-danger')
			}
		},

		initIn : function(container) {
			container.$el.append(this.render().el);
			Marionette.triggerMethod.call(this, "show");
		},

		ui : {
			inputModalName : '#inputModalName',

			inputModalOldPassword : '#inputModalOldPassword',
			inputModalNewPassword : '#inputModalNewPassword',
			inputModalConfirmPassword : '#inputModalConfirmPassword',

			form : '#formSearchUser',
			modalScreen : '.modal',
		},

		initialize : function(opt) {
			var that = this;
			this.currentUser = opt.user;

			this.on('show', function() {

			});
		},

		clearModal : function() {

			this.ui.inputModalOldPassword.val('');
			this.ui.inputModalNewPassword.val('');
			this.ui.inputModalConfirmPassword.val('');

		},

		showPage : function() {
			this.clearModal();
			this.ui.modalScreen.modal('show');

		},
	});

	return UserModal;
});
