/* generated: 18/03/2015 12:38:56 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var TemplateFormUsers = require('text!views/user/tpl/FormUserTemplate.html');
	var UserModel = require('models/UserModel');
	var UserCollection = require('collections/UserCollection');
	var RoleCollection = require('collections/RoleCollection');
	var MultiSelectRole = require('views/role/MultiSelectRole');
	var ModalEmpresa = require('views/modalComponents/EmpresaModal');
	// End of "Import´s" definition
	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################
	var FormUsers = Marionette.LayoutView.extend({
		template: _.template(TemplateFormUsers),
		regions: {
			rolesRegion: ".roles-container",
			modalEmpresaRegion: '#empresaModal',
		},
		events: {
			'change  #inputUploadImage': 'startUpload',
			'click  #inputImage': 'uploadFile',
			'click 	.save': 'save',
			'click 	.saveAndContinue': 'saveAndContinue',
			'click #searchEmpresaModal': 'showModalEmpresa',
			'change #inputTipo': 'showHideEmpresa',
			'change #inputUsername': 'changeUsername',
			'change #inputUsername': 'hasWhitespace',
		},
		ui: {
			inputId: '#inputId',
			inputName: '#inputName',
			inputImage: '#inputImage',
			saveButton: '.save',
			saveAndContinueButton: '.saveAndContinue ',
			returnToListButton: '.go-back-link',
			inputPerfil: '#inputPerfil',
			inputUsername: '#inputUsername',
			inputPassword: '#inputPassword',
			inputConfirmPassword: '#inputConfirmPassword',
			inputEnable: '#inputEnable',
			inputUploadImage: '#inputUploadImage',
			groupInputEmpresaContainer: '#groupInputEmpresaContainer',
			inputEmail: '#inputEmail',
			inputTipo: '#inputTipo',
			inputStatusUsuario: '#inputStatusUsuario',
			inputEmpresaId: '#inputEmpresaId',
			inputEmpresaNome: '#inputEmpresaNome',
			form: '#formUser',
		},
		initialize: function() {
			var that = this;
			that.roles = new RoleCollection();
			that.roles.add(this.model.get('roles'));
			this.multiSelectRole = new MultiSelectRole({
				collection: that.roles,
			});
			this.modalEmpresa = new ModalEmpresa({
				onSelectModel: function(model) {
					that.onSelectEmpresa(model);
				},
			});
			this.modalEmpresa.setValue(this.model.get('empresa'));
			this.on('show', function() {
				this.rolesRegion.show(this.multiSelectRole);
				this.modalEmpresaRegion.show(this.modalEmpresa);
				this.showHideEmpresa();
				this.ui.form.validationEngine('attach', {
					promptPosition: "topLeft",
					isOverflown: false,
					validationEventTrigger: "change"
				});
				if (this.model.get('id'))
					this.ui.inputPassword.removeClass('validate[required]')
			});
		},
		showHideEmpresa: function() {
			if (this.ui.inputTipo.val() == 'Empresa') {
				this.ui.groupInputEmpresaContainer.show();
				this.ui.inputEmpresaNome.addClass('validate[required]');
			}
			else {
				this.ui.groupInputEmpresaContainer.hide();
				this.ui.inputEmpresaNome.removeClass('validate[required]');
				this.modalEmpresa.setValue(null);
				this.ui.inputEmpresaId.val(null);
				this.ui.inputEmpresaNome.val(null);
			}
		},
		uploadFile: function() {
			this.ui.inputUploadImage.trigger('click');
		},
		startUpload: function(e) {
			var that = this;
			if (!this.ui.inputUploadImage.val()) return;
			this.ui.saveButton.addClass('disabled');
			this.ui.saveAndContinueButton.addClass('disabled');
			this.ui.returnToListButton.addClass('disabled');
			this.ui.inputImage.attr('src', 'images/loading.gif');
			$('#formUser').ajaxSubmit({
				success: function(responseText) {
					that.ui.inputImage.attr('src', responseText.dataUrl)
					that.ui.saveButton.removeClass('disabled');
					that.ui.saveAndContinueButton.removeClass('disabled');
					that.ui.returnToListButton.removeClass('disabled');
				},
				error: function(response, paran, paran2) {
					console.log(response);
					console.log(paran);
					console.log(paran2);
					that.ui.saveButton.removeClass('disabled');
					that.ui.saveAndContinueButton.removeClass('disabled');
					that.ui.returnToListButton.removeClass('disabled');
				},
			});
		},
		saveAndContinue: function() {
			this.save(true)
		},
		save: function(continua) {
			var that = this;
			var user = that._getModel();
			if (this._isValid()) {
				if (!this._senhaConfirmada()) {
					util.showMessage('error', 'Senhas não conferem!', 'messages_password');
					return;
				}
				if (!this._escolheuPapeis()) {
					util.showMessage('error', 'Voce deve escolher pelo menos um papel!', 'role-msg');
					return;
				}
				if (!this.hasWhitespace()) {
					return;
				}
				user.save({}, {
					success: function(_model, _resp, _options) {
						//util.showMessage('success', 'User salvo com sucesso!');
						//that.clearForm();
						// needs to work on what is passive of refreshing
						if (continua != true) {
							//
							$.get('j_spring_security_logout', util.goPage('/'));
							location.reload();
							//util.goPage('app/users');
						}
						$.get('j_spring_security_logout', util.goPage('/'));
						location.reload();
					},
					error: function(_model, _resp, _options) {
						util.showMessage('error', 'Problema ao salvar User');
						console.error(_resp.responseText || _resp.getResponseHeader('exception'));
					}
				});
			}
			else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},
		_senhaConfirmada: function() {
			if (!this.ui.inputId.val() && (this.ui.inputPassword.val() !== this.ui.inputConfirmPassword.val())) {
				return false;
			}
			return true;
		},
		// vitoriano : nao salva espacos em branco : refactor
		hasWhitespace: function() {
			var r = new RegExp('( )+');
			var result = r.test(this.ui.inputUsername.val());
			if (result) {
				util.clear('inputUsername');
				util.showMessage('error', 'Username não pode conter espaços em branco!');
				return false;
			}
			return true;
		},
		clearForm: function() {
			util.clear('inputId');
			util.clear('inputName');
			util.clear('inputUsername');
			util.clear('inputPassword');
			util.clear('inputConfirmPassword');
			util.clear('inputEnable');
			util.clear('inputImage');
			util.clear('inputEmail');
			util.clear('inputTipo');
			util.clear('inputStatusUsuario');
			this.roles.reset();
			this.multiSelectRole.clear();
			this.ui.inputImage.attr('src', "images/no_photo.jpg");
		},
		possuiCamposInvalidos: function() {
			return util.hasInvalidFields(this.validateFields);
		},
		_escolheuPapeis: function() {
			if (this.roles.length > 0) {
				return true;
			}
			else {
				return false;
			}
		},
		_isValid: function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition: "topLeft",
				isOverflown: false,
				validationEventTrigger: "change"
			});
		},
		_getModel: function() {
			var that = this;
			var user = that.model;
			user.set({
				id: util.escapeById('inputId') || null,
				name: util.escapeById('inputName'),
				username: util.escapeById('inputUsername'),
				password: util.escapeById('inputPassword'),
				enable: util.escapeById('inputEnable'),
				perfil: util.escapeById('inputPerfil'),
				email: util.escapeById('inputEmail'),
				tipo: util.escapeById('inputTipo'),
				statusUsuario: util.escapeById('inputStatusUsuario'),
				empresa: that.modalEmpresa.getJsonValue(),
				image: that.ui.inputImage.attr('src'),
				roles: that.roles.toJSON(),
			});
			return user;
		},
		showModalEmpresa: function() {
			// add more before the modal is open
			this.modalEmpresa.showPage();
		},
		onSelectEmpresa: function(empresa) {
			this.modalEmpresa.hidePage();
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNome.val(empresa.get('nomeFantasia'));
		},
		changeUsername: function() {
			var that = this;
			util.validateUnique({
				element: that.ui.inputUsername,
				fieldName: 'username',
				fieldDisplayName: 'Username',
				// onlyNumber : true, //caso queira que as mascaras sejam
				// removidas e somente NUMEROS sejam enviados na consulta.
				view: that,
				collection: UserCollection,
			})
		},
	});
	return FormUsers;
});