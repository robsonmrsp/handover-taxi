/* generated: 18/03/2015 12:38:56 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var PageUserProfileTemplate = require('text!views/user/tpl/PageUserProfileTemplate.html');

	var ChangePasswordModal = require('views/user/ChangePasswordModal');

	var UserModel = require('models/UserModel');
	var UserCollection = require('collections/UserCollection');

	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormUsers = Marionette.LayoutView.extend({
		template : _.template(PageUserProfileTemplate),

		regions : {},

		events : {
			'change  #inputUploadImage' : 'startUpload',
			'click  #inputImage' : 'uploadFile',
			'click 	.save' : 'save',
			'click 	.change-password' : 'changePassword',
			'change #inputUsername' : 'changeUsername',
		},

		ui : {
			inputId : '#inputId',
			inputName : '#inputName',
			inputImage : '#inputImage',

			saveButton : '.save',
			inputUsername : '#inputUsername',
			inputUploadImage : '#inputUploadImage',

			form : '#formUser',
		},

		changePassword : function() {
			this.changePasswordModal.showPage();
		},
		initialize : function() {
			var that = this;

			if (!this.model) {
				this.model.getCurrentUser({
					success : function(model) {
						console.log('currentuser' + model);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				});
			}

			this.changePasswordModal = new ChangePasswordModal({
				user : that.model,
			});

			this.on('show', function() {
				this.changePasswordModal.initIn(this);
			});
		},

		uploadFile : function() {
			this.ui.inputUploadImage.trigger('click');
		},

		startUpload : function(e) {
			var that = this;
			if (!this.ui.inputUploadImage.val())
				return;

			this.ui.saveButton.addClass('disabled');

			this.ui.inputImage.attr('src', 'images/loading.gif');

			$('#formUser').ajaxSubmit({
				success : function(responseText) {
					that.ui.inputImage.attr('src', responseText.dataUrl)

					that.ui.saveButton.removeClass('disabled');
					that.ui.saveAndContinueButton.removeClass('disabled');
					that.ui.returnToListButton.removeClass('disabled');
				},

				error : function(response, paran, paran2) {
					console.log(response);
					console.log(paran);
					console.log(paran2);

					that.ui.saveButton.removeClass('disabled');
					that.ui.saveAndContinueButton.removeClass('disabled');
					that.ui.returnToListButton.removeClass('disabled');
				},

			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var user = that._getModel();

			if (this._isValid()) {
				user.changeUser({
					success : function(_model, _resp, _options) {
						util.showMessage('success', 'User salvo com sucesso!');
					},

					error : function(_model, _resp, _options) {
						util.showMessage('error', 'Problema ao salvar User');
						console.error(_resp.responseText || _resp.getResponseHeader('exception'));
					}
				});

			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		_isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		_getModel : function() {
			var that = this;
			var user = new UserModel(that.model.attributes);
			user.set({
				id : util.escapeById('inputId') || null,
				name : util.escapeById('inputName'),
				username : util.escapeById('inputUsername'),
				enable : util.escapeById('inputEnable'),
				image : that.ui.inputImage.attr('src'),
			});
			return user;
		},
	});

	return FormUsers;
});
