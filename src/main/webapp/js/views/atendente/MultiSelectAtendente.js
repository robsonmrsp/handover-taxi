/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectAtendente = require('views/atendente/ModalMultiSelectAtendente');
	var MultiSelectAtendenteTemplate = require('text!views/atendente/tpl/MultiSelectAtendenteTemplate.html');

	var MultiSelectAtendente = Marionette.LayoutView.extend({
		template : _.template(MultiSelectAtendenteTemplate),

		regions : {
			modalMultiSelectAtendenteRegion : '#modalMultiSelectAtendentes',
			gridAtendentesModalRegion : '#gridMultiselectAtendentes',
		},

		initialize : function() {
			var that = this;

			this.atendentes = this.collection;

			this.gridAtendentes = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.atendentes,
			});

			this.modalMultiSelectAtendente = new ModalMultiSelectAtendente({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectAtendenteRegion.show(that.modalMultiSelectAtendente);
				that.gridAtendentesModalRegion.show(that.gridAtendentes);
			});
		},
		clear : function(){
			this.modalMultiSelectAtendente.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "nome",
				editable : false,
				sortable : false,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "situacao",
				editable : false,
				sortable : false,
				label 	 : "Situação",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectAtendente
});
