/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormAtendentes = require('text!views/atendente/tpl/FormAtendenteTemplate.html');
	var AtendenteModel = require('models/AtendenteModel');
	var AtendenteCollection = require('collections/AtendenteCollection');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormAtendentes = Marionette.LayoutView.extend({
		template : _.template(TemplateFormAtendentes),

		regions : {
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
		},
		
		ui : {
			inputId : '#inputId',
			inputNome : '#inputNome',
			inputSituacao : '#inputSituacao',
		
			form : '#formAtendente',
		},

		initialize : function() {
			var that = this;
			this.on('show', function() {
		
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var atendente = that.getModel();

			if (this.isValid()) {
				atendente.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Atendente salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/atendentes');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputNome'); 
			util.clear('inputSituacao'); 
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var atendente = that.model; 
			atendente.set({
				id: util.escapeById('inputId') || null,
		    	nome : util.escapeById('inputNome'), 
		    	situacao : util.escapeById('inputSituacao'), 
			});
			return atendente;
		},
		 		

				
		
	});

	return FormAtendentes;
});