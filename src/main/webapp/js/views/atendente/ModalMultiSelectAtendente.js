/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var AtendentePageCollection = require('collections/AtendentePageCollection');
	var ModalMultiSelectAtendenteTemplate = require('text!views/atendente/tpl/ModalMultiSelectAtendenteTemplate.html');
	// End of "Import´s" definition

	var ModalAtendentes = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectAtendenteTemplate),

		regions : {
			gridRegion : '#grid-atendentes-modal',
			paginatorRegion : '#paginator-atendentes-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoAtendentes = this.collection;
			
			this.atendentes = new AtendentePageCollection();
			this.atendentes.on('fetched', this.endFetch, this);
			this.atendentes.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.atendentes,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.atendentes,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.atendentes.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid atendente');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoAtendentes.add(model)
			else
				this.projetoAtendentes.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.atendentes.each(function(model) {
				if (that.projetoAtendentes.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "nome",
				editable : false,
				sortable : false,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "situacao",
				editable : false,
				sortable : false,
				label 	 : "Situação",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalAtendentes;
});
