/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var DistanciaMotoCorridaModel = require('models/DistanciaMotoCorridaModel');
	var DistanciaMotoCorridaCollection = require('collections/DistanciaMotoCorridaCollection');
	var DistanciaMotoCorridaPageCollection = require('collections/DistanciaMotoCorridaPageCollection');
	var PageDistanciaMotoCorridaTemplate = require('text!views/distanciaMotoCorrida/tpl/PageDistanciaMotoCorridaTemplate.html');
	
	//Filter import
	var ModalMotorista = require('views/modalComponents/MotoristaModal');
	var ModalCorrida = require('views/modalComponents/CorridaModal');
	
	// End of "Import´s" definition

	var PageDistanciaMotoCorrida = Marionette.LayoutView.extend({
		template : _.template(PageDistanciaMotoCorridaTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			modalMotoristaRegion : '#motoristaModal',
			modalCorridaRegion : '#corridaModal',
		},
		
		events : {
			'click 	#reset' : 'resetDistanciaMotoCorrida',			
			'click #searchMotoristaModal' : 'showModalMotorista',
			'click #searchCorridaModal' : 'showModalCorrida',
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchDistanciaMotoCorrida',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
		
			inputMotoristaId : '#inputMotoristaId',
			inputMotoristaNome : '#inputMotoristaNome',
			inputCorridaId : '#inputCorridaId',
			inputCorridaNome : '#inputCorridaNome',
			form : '#formDistanciaMotoCorridaFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchDistanciaMotoCorrida();
	    	}
		},

		initialize : function() {
			var that = this;

			this.distanciaMotoCorridas = new DistanciaMotoCorridaPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.distanciaMotoCorridas
			});

			this.counter = new Counter({
				collection : this.distanciaMotoCorridas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.distanciaMotoCorridas,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.distanciaMotoCorridas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid distanciaMotoCorrida');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
				}
			});
			this.modalMotorista = new ModalMotorista({
				onSelectModel : function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.modalCorrida = new ModalCorrida({
				onSelectModel : function(model) {
					that.onSelectCorrida(model);
				},
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.modalMotoristaRegion.show(this.modalMotorista);		
				this.modalCorridaRegion.show(this.modalCorrida);		
		
			});
		},
		 
		searchDistanciaMotoCorrida : function(){
			var that = this;

			this.distanciaMotoCorridas.filterQueryParams = {
			    motorista : util.escapeById('inputMotoristaId'), 
			    corrida : util.escapeById('inputCorridaId'), 
			}
			this.distanciaMotoCorridas.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid distanciaMotoCorrida');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetDistanciaMotoCorrida : function(){
			this.ui.form.get(0).reset();
			this.distanciaMotoCorridas.reset();
			util.clear('inputMotoristaId');
			util.clear('inputCorridaId');
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "motorista.nome",
				editable : false,
				sortable : true,  
				label : "Motorista",
				cell : CustomStringCell.extend({
					fieldName : 'motorista.nome',
				}),
			},	
			{
				name : "corrida.nome",
				editable : false,
				sortable : true,  
				label : "Corrida",
				cell : CustomStringCell.extend({
					fieldName : 'corrida.nome',
				}),
			},	
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Distancia moto corrida',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Distancia moto corrida',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new DistanciaMotoCorridaModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.distanciaMotoCorridas.remove(model);
							util.showSuccessMessage('Distancia moto corrida removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editDistanciaMotoCorrida/" + model.get('id'));
		},

		showModalMotorista : function() {
			this.modalMotorista.showPage();
		},
			
		onSelectMotorista : function(motorista) {
			this.modalMotorista.hidePage();	
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));		
		},
		showModalCorrida : function() {
			this.modalCorrida.showPage();
		},
			
		onSelectCorrida : function(corrida) {
			this.modalCorrida.hidePage();	
			this.ui.inputCorridaId.val(corrida.get('id'));
			this.ui.inputCorridaNome.val(corrida.get('nome'));		
		},
		

	});

	return PageDistanciaMotoCorrida;
});
