/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormDistanciaMotoCorridas = require('text!views/distanciaMotoCorrida/tpl/FormDistanciaMotoCorridaTemplate.html');
	var DistanciaMotoCorridaModel = require('models/DistanciaMotoCorridaModel');
	var DistanciaMotoCorridaCollection = require('collections/DistanciaMotoCorridaCollection');
	var ModalMotorista = require('views/modalComponents/MotoristaModal');
	var ModalCorrida = require('views/modalComponents/CorridaModal');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormDistanciaMotoCorridas = Marionette.LayoutView.extend({
		template : _.template(TemplateFormDistanciaMotoCorridas),

		regions : {
			modalMotoristaRegion : '#motoristaModal',
			modalCorridaRegion : '#corridaModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click #searchMotoristaModal' : 'showModalMotorista',
			'click #searchCorridaModal' : 'showModalCorrida',
		},
		
		ui : {
			inputId : '#inputId',
			inputDistancia : '#inputDistancia',
			inputSequencia : '#inputSequencia',
		
			inputMotoristaId : '#inputMotoristaId',
			inputMotoristaNome : '#inputMotoristaNome',
			inputCorridaId : '#inputCorridaId',
			inputCorridaNome : '#inputCorridaNome',
			form : '#formDistanciaMotoCorrida',
		},

		initialize : function() {
			var that = this;
			this.modalMotorista = new ModalMotorista({
				onSelectModel : function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.modalCorrida = new ModalCorrida({
				onSelectModel : function(model) {
					that.onSelectCorrida(model);
				},
			});
			this.on('show', function() {
				this.modalMotoristaRegion.show(this.modalMotorista);		
				this.modalCorridaRegion.show(this.modalCorrida);		
		
				this.ui.inputDistancia.formatNumber(2);
				this.ui.inputSequencia.formatNumber(2);
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var distanciaMotoCorrida = that.getModel();

			if (this.isValid()) {
				distanciaMotoCorrida.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Distancia moto corrida salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/distanciaMotoCorridas');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputDistancia'); 
			util.clear('inputSequencia'); 
			util.clear('inputMotoristaId');
			util.clear('inputMotoristaNome');
			util.clear('inputCorridaId');
			util.clear('inputCorridaNome');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var distanciaMotoCorrida = that.model; 
			distanciaMotoCorrida.set({
				id: util.escapeById('inputId') || null,
		    	distancia : util.escapeById('inputDistancia', true), 
		    	sequencia : util.escapeById('inputSequencia', true), 
				motorista : that.modalMotorista.getJsonValue(),
				corrida : that.modalCorrida.getJsonValue(),
			});
			return distanciaMotoCorrida;
		},
		 		
		showModalMotorista : function() {
			// add more before the modal is open
			this.modalMotorista.showPage();
		},
		showModalCorrida : function() {
			// add more before the modal is open
			this.modalCorrida.showPage();
		},

		onSelectMotorista : function(motorista) {
			this.modalMotorista.hidePage();	
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));		
		},
		onSelectCorrida : function(corrida) {
			this.modalCorrida.hidePage();	
			this.ui.inputCorridaId.val(corrida.get('id'));
			this.ui.inputCorridaNome.val(corrida.get('nome'));		
		},
				
		
	});

	return FormDistanciaMotoCorridas;
});