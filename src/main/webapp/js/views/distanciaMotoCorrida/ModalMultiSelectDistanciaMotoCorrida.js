/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var DistanciaMotoCorridaPageCollection = require('collections/DistanciaMotoCorridaPageCollection');
	var ModalMultiSelectDistanciaMotoCorridaTemplate = require('text!views/distanciaMotoCorrida/tpl/ModalMultiSelectDistanciaMotoCorridaTemplate.html');
	// End of "Import´s" definition

	var ModalDistanciaMotoCorridas = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectDistanciaMotoCorridaTemplate),

		regions : {
			gridRegion : '#grid-distanciaMotoCorridas-modal',
			paginatorRegion : '#paginator-distanciaMotoCorridas-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoDistanciaMotoCorridas = this.collection;
			
			this.distanciaMotoCorridas = new DistanciaMotoCorridaPageCollection();
			this.distanciaMotoCorridas.on('fetched', this.endFetch, this);
			this.distanciaMotoCorridas.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.distanciaMotoCorridas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.distanciaMotoCorridas,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.distanciaMotoCorridas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid distanciaMotoCorrida');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoDistanciaMotoCorridas.add(model)
			else
				this.projetoDistanciaMotoCorridas.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.distanciaMotoCorridas.each(function(model) {
				if (that.projetoDistanciaMotoCorridas.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "distancia",
				editable : false,
				sortable : false,
				label 	 : "Distancia",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "sequencia",
				editable : false,
				sortable : false,
				label 	 : "Sequencia",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return ModalDistanciaMotoCorridas;
});
