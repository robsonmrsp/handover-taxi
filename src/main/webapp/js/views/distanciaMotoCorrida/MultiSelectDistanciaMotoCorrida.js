/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectDistanciaMotoCorrida = require('views/distanciaMotoCorrida/ModalMultiSelectDistanciaMotoCorrida');
	var MultiSelectDistanciaMotoCorridaTemplate = require('text!views/distanciaMotoCorrida/tpl/MultiSelectDistanciaMotoCorridaTemplate.html');

	var MultiSelectDistanciaMotoCorrida = Marionette.LayoutView.extend({
		template : _.template(MultiSelectDistanciaMotoCorridaTemplate),

		regions : {
			modalMultiSelectDistanciaMotoCorridaRegion : '#modalMultiSelectDistanciaMotoCorridas',
			gridDistanciaMotoCorridasModalRegion : '#gridMultiselectDistanciaMotoCorridas',
		},

		initialize : function() {
			var that = this;

			this.distanciaMotoCorridas = this.collection;

			this.gridDistanciaMotoCorridas = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.distanciaMotoCorridas,
			});

			this.modalMultiSelectDistanciaMotoCorrida = new ModalMultiSelectDistanciaMotoCorrida({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectDistanciaMotoCorridaRegion.show(that.modalMultiSelectDistanciaMotoCorrida);
				that.gridDistanciaMotoCorridasModalRegion.show(that.gridDistanciaMotoCorridas);
			});
		},
		clear : function(){
			this.modalMultiSelectDistanciaMotoCorrida.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "distancia",
				editable : false,
				sortable : false,
				label 	 : "Distancia",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "sequencia",
				editable : false,
				sortable : false,
				label 	 : "Sequencia",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return MultiSelectDistanciaMotoCorrida
});
