/* generated: 28/11/2016 23:13:05 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var MotivoPunicaoPageCollection = require('collections/MotivoPunicaoPageCollection');
	var ModalMultiSelectMotivoPunicaoTemplate = require('text!views/motivoPunicao/tpl/ModalMultiSelectMotivoPunicaoTemplate.html');
	// End of "Import´s" definition

	var ModalMotivoPunicaos = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectMotivoPunicaoTemplate),

		regions : {
			gridRegion : '#grid-motivoPunicaos-modal',
			paginatorRegion : '#paginator-motivoPunicaos-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoMotivoPunicaos = this.collection;
			
			this.motivoPunicaos = new MotivoPunicaoPageCollection();
			this.motivoPunicaos.on('fetched', this.endFetch, this);
			this.motivoPunicaos.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.motivoPunicaos,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.motivoPunicaos,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.motivoPunicaos.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid motivoPunicao');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoMotivoPunicaos.add(model)
			else
				this.projetoMotivoPunicaos.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.motivoPunicaos.each(function(model) {
				if (that.projetoMotivoPunicaos.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descrição",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalMotivoPunicaos;
});
