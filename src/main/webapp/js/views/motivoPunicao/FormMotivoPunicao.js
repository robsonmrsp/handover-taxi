/* generated: 28/11/2016 23:13:05 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var TemplateFormMotivoPunicaos = require('text!views/motivoPunicao/tpl/FormMotivoPunicaoTemplate.html');
	var MotivoPunicaoModel = require('models/MotivoPunicaoModel');
	var MotivoPunicaoCollection = require('collections/MotivoPunicaoCollection');
	// End of "Import´s" definition
	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################
	var FormMotivoPunicaos = Marionette.LayoutView.extend({
		template: _.template(TemplateFormMotivoPunicaos),
		regions: {},
		events: {
			'click 	.save': 'save',
			'click 	.saveAndContinue': 'saveAndContinue',
		},
		ui: {
			inputId: '#inputId',
			inputDescricao: '#inputDescricao',
			form: '#formMotivoPunicao',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		initialize: function() {
			var that = this;
			this.on('show', function() {
				this.ui.form.validationEngine('attach', {
					promptPosition: "topLeft",
					isOverflown: false,
					validationEventTrigger: "change"
				});
				this.checkButtonAuthority();
			});
		},
		saveAndContinue: function() {
			this.save(true)
		},
		save: function(continua) {
			var that = this;
			var motivoPunicao = that.getModel();
			if (this.isValid()) {
				motivoPunicao.save({}, {
					success: function(_model, _resp, _options) {
						util.showSuccessMessage('Motivo da Punição salvo com sucesso!');
						that.clearForm();
						if (continua != true) {
							util.goPage('app/motivoPunicaos');
						}
					},
					error: function(_model, _resp, _options) {
						if(_resp.status == '403'){
							util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
						} else {
							util.showErrorMessage('Problema ao salvar registro', _resp);
						}
					}
				});
			}
			else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},
		clearForm: function() {
			util.clear('inputId');
			util.clear('inputDescricao');
		},
		isValid: function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition: "topLeft",
				isOverflown: false,
				validationEventTrigger: "change"
			});
		},
		getModel: function() {
			var that = this;
			var motivoPunicao = that.model;
			motivoPunicao.set({
				id: util.escapeById('inputId') || null,
				descricao: util.escapeById('inputDescricao'),
			});
			return motivoPunicao;
		},
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_MOTIVOPUNICAO_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
				if(e.authority == 'ROLE_MOTIVOPUNICAO_EDITAR' || e.authority == 'ROLE_ADMIN') {
					saveButton.show();
				}
			})
		},
	});
	return FormMotivoPunicaos;
});