/* generated: 28/11/2016 23:13:05 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectMotivoPunicao = require('views/motivoPunicao/ModalMultiSelectMotivoPunicao');
	var MultiSelectMotivoPunicaoTemplate = require('text!views/motivoPunicao/tpl/MultiSelectMotivoPunicaoTemplate.html');

	var MultiSelectMotivoPunicao = Marionette.LayoutView.extend({
		template : _.template(MultiSelectMotivoPunicaoTemplate),

		regions : {
			modalMultiSelectMotivoPunicaoRegion : '#modalMultiSelectMotivoPunicaos',
			gridMotivoPunicaosModalRegion : '#gridMultiselectMotivoPunicaos',
		},

		initialize : function() {
			var that = this;

			this.motivoPunicaos = this.collection;

			this.gridMotivoPunicaos = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.motivoPunicaos,
			});

			this.modalMultiSelectMotivoPunicao = new ModalMultiSelectMotivoPunicao({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectMotivoPunicaoRegion.show(that.modalMultiSelectMotivoPunicao);
				that.gridMotivoPunicaosModalRegion.show(that.gridMotivoPunicaos);
			});
		},
		clear : function(){
			this.modalMultiSelectMotivoPunicao.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descrição",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectMotivoPunicao
});
