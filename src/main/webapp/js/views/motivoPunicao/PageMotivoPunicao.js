/* generated: 28/11/2016 23:13:05 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');
	var CustomNumberCell = require('views/components/CustomNumberCell');
	var MotivoPunicaoModel = require('models/MotivoPunicaoModel');
	var MotivoPunicaoCollection = require('collections/MotivoPunicaoCollection');
	var MotivoPunicaoPageCollection = require('collections/MotivoPunicaoPageCollection');
	var PageMotivoPunicaoTemplate = require('text!views/motivoPunicao/tpl/PageMotivoPunicaoTemplate.html');
	// End of "Import´s" definition
	var PageMotivoPunicao = Marionette.LayoutView.extend({
		template: _.template(PageMotivoPunicaoTemplate),
		regions: {
			gridRegion: '#grid',
			counterRegion: '#counter',
			paginatorRegion: '#paginator',
		},
		events: {
			'click 	#reset': 'resetMotivoPunicao',
			'keypress': 'treatKeypress',
			'click 	.search-button': 'searchMotivoPunicao',
			'click .show-advanced-search-button': 'toggleAdvancedForm',
		},
		ui: {
			form: '#formMotivoPunicaoFilter',
			advancedSearchForm: '.advanced-search-form',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		toggleAdvancedForm: function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},
		treatKeypress: function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchMotivoPunicao();
			}
		},
		initialize: function() {
			var that = this;
			this.motivoPunicaos = new MotivoPunicaoPageCollection();
			this.grid = new Backgrid.Grid({
				className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns: this.getColumns(),
				emptyText: "Sem registros",
				collection: this.motivoPunicaos
			});
			this.counter = new Counter({
				collection: this.motivoPunicaos,
			});
			this.paginator = new Backgrid.Extension.Paginator({
				columns: this.getColumns(),
				collection: this.motivoPunicaos,
				className: ' paging_simple_numbers',
				uiClassName: 'pagination',
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				this.searchMotivoPunicao();
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.checkButtonAuthority();
			});
		},
		searchMotivoPunicao: function() {
			var that = this;
			this.motivoPunicaos.filterQueryParams = {
				descricao: util.escapeById('inputDescricao'),
			}
			this.motivoPunicaos.fetch({
				success: function(_coll, _resp, _opt) {
					console.info('Consulta para o grid motivoPunicao');
				},
				error: function(_coll, _resp, _opt) {
					if(_resp.status == '403'){
						util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
					}
				},
				complete: function() {},
			})
		},
		resetMotivoPunicao: function() {
			this.ui.form.get(0).reset();
			this.motivoPunicaos.reset();
		},
		getColumns: function() {
			var that = this;
			var columns = [{
				name: "descricao",
				editable: false,
				sortable: true,
				label: "Motivo",
				cell: "string",
			}, {
				name: "acoes",
				label: "Ações(Editar, Deletar)",
				sortable: false,
				cell: GeneralActionsCell.extend({
					buttons: that.getCellButtons(),
					context: that,
				})
			}];
			return columns;
		},
		getCellButtons: function() {
			var that = this;
			var buttons = this.checkGridButtonAuthority();
			return buttons;
		},
		deleteModel: function(model) {
			var that = this;
			var modelTipo = new MotivoPunicaoModel({
				id: model.id,
			});
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success: function() {
							that.motivoPunicaos.remove(model);
							util.showSuccessMessage('Motivo da Punição removido com sucesso!');
						},
						error: function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro', _resp);
						}
					});
				}
			});
		},
		editModel: function(model) {
			util.goPage("app/editMotivoPunicao/" + model.get('id'));
		},
		// vitoriano : chunck : check grid buttons authority
		checkGridButtonAuthority: function() {
			var that = this;
			var buttons = [];
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_MOTIVOPUNICAO_EDITAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'edita_ficha_button',
						type: 'primary',
						icon: 'icon-pencil fa-pencil',
						hint: 'Editar Motivo da Punição',
						onClick: that.editModel,
					});
				}
				if (e.authority == 'ROLE_MOTIVOPUNICAO_DELETAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'delete_button',
						type: 'danger',
						icon: 'icon-trash fa-trash',
						hint: 'Remover Motivo da Punição',
						onClick: that.deleteModel,
					});
				}
			})
			return buttons;
		},
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_MOTIVOPUNICAO_CRIAR' || 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
	});
	return PageMotivoPunicao;
});