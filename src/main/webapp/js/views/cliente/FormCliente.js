/* generated: 16/10/2016 15:27:07 */
define(function(require) {
    // Start "Import´s" Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var roles = require('adapters/auth-adapter').roles;
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var TemplateFormClientes = require('text!views/cliente/tpl/FormClienteTemplate.html');
    var ClienteModel = require('models/ClienteModel');
    var ClienteCollection = require('collections/ClienteCollection');
    var ModalEmpresa = require('views/modalComponents/EmpresaModal');
    var ModalCentroCusto = require('views/modalComponents/CentroCustoModal');
    // End of "Import´s" definition
    // #####################################################################################################
    // ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
    // BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
    // #####################################################################################################
    var FormClientes = Marionette.LayoutView.extend({
        template: _.template(TemplateFormClientes),
        regions: {
            modalEmpresaRegion: '#empresaModal',
            modalCentroCustoRegion: '#centroCustoModal',
        },
        events: {
            'click 	.save': 'save',
            'click 	.saveAndContinue': 'saveAndContinue',
            'click #searchEmpresaModal': 'showModalEmpresa',
            'change #inputTipo': 'showHideEmpresa',
            'click #inputTipo': 'showHideEmpresa',
            'click #searchCentroCustoModal': 'showModalCentroCusto',
            'change  #inputCpf': 'uniqueCpf',
            'change  #inputTelefone': 'uniqueTelefone',
            'change #inputLimiteMensal': 'acionaLimiteDisponivelCC',
        },
        ui: {
            inputId: '#inputId',
            inputNome: '#inputNome',
            inputEmail: '#inputEmail',
            inputUsername: '#inputUsername',
            inputSenha: '#inputSenha',
            inputTelefone: '#inputTelefone',
            inputTipo: '#inputTipo',
            inputInativo: '#inputInativo',
            inputUsuarioCadastro: '#inputUsuarioCadastro',
            inputDataCadastro: '#inputDataCadastro',
            groupInputDataCadastro: '#groupInputDataCadastro',
            inputMatricula: '#inputMatricula',
            inputLimiteMensal: '#inputLimiteMensal',
            inputCnpjCpf: '#inputCnpjCpf',
            inputCnpj: '#inputCnpj',
            inputCpf: '#inputCpf',
            inputAutorizaEticket: '#inputAutorizaEticket',
            inputObservacao: '#inputObservacao',
            groupInputEmpresaContainer: '#groupInputEmpresaContainer',
            groupInputCentroCustoContainer: '#groupInputCentroCustoContainer',
            groupInputLimiteMensal: '#groupInputLimiteMensal',
            groupInputAutorizaEticket: '#groupInputAutorizaEticket',
            inputEmpresaId: '#inputEmpresaId',
            inputEmpresaNome: '#inputEmpresaNome',
            inputCentroCustoId: '#inputCentroCustoId',
            inputCentroCustoDescricao: '#inputCentroCustoDescricao',
            spanLimiteDisponivel: '#limiteDisponivel',
            inputLimiteDisponivel: '#inputLimiteDisponivel',
            groupInputLimiteDisponivel: '#groupInputLimiteDisponivel',
            form: '#formCliente',
            newButton: 'a#new',
            saveButton: 'a#save',
            saveContinueButton: 'a#saveContinue',
        },
        initialize: function() {
            var that = this;
            this.modalEmpresa = new ModalEmpresa({
                onSelectModel: function(model) {
                    that.onSelectEmpresa(model);
                },
            });
            this.modalEmpresa.setValue(this.model.get('empresa'));
            this.modalCentroCusto = new ModalCentroCusto({
                onSelectModel: function(model) {
                    that.onSelectCentroCusto(model);
                },
            });
            this.modalCentroCusto.setValue(this.model.get('centroCusto'));
            this.on('show', function() {
                this.ui.inputTelefone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
                this.modalEmpresaRegion.show(this.modalEmpresa);
                this.modalCentroCustoRegion.show(this.modalCentroCusto)
                this.ui.inputUsuarioCadastro.formatNumber(2);
                this.ui.groupInputDataCadastro.datetimepicker({
                    pickTime: false,
                    language: 'pt_BR',
                });
                this.ui.inputDataCadastro.datetimepicker({
                    pickTime: false,
                    language: 'pt_BR',
                });
                this.showHideEmpresa();
                this.ui.inputDataCadastro.mask('99/99/9999');
                this.ui.inputMatricula.formatNumber(2);
                this.ui.inputLimiteMensal.mask('###.###.###.###,##', {
                    reverse: true
                });
                this.ui.inputLimiteMensal.val(util.printFormatNumber(this.model.get('limiteMensal')));
                this.ui.inputCnpj.mask('99.999.999/9999-99');
                this.ui.inputCpf.mask('999.999.999-99');
                this.ui.form.validationEngine('attach', {
                    promptPosition: "topLeft",
                    isOverflown: false,
                    validationEventTrigger: "change"
                });
                if (this.model.get('id') && this.model.get('centroCusto')) {
                    this.limiteDisponivelCC(this.model.get('centroCusto'));
                }
                this.checkButtonAuthority();
            });
        },
        saveAndContinue: function() {
            this.save(true)
        },
        save: function(continua) {
            var that = this;
            var cliente = that.getModel();
            if (this.isValid()) {
                if (cliente.get('centroCusto') && !this.limiteDisponivelCC(cliente.get('centroCusto'))) return;
                cliente.save({}, {
                    success: function(_model, _resp, _options) {
                        util.showSuccessMessage('Cliente salvo com sucesso!');
                        that.clearForm();
                        if (continua != true) {
                            util.goPage('app/clientes');
                        }
                    },
                    error: function(_model, _resp, _options) {
                        if (_resp.status == '403') {
                            util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
                        }
                        else {
                            util.showErrorMessage('Problema ao salvar registro', _resp);
                        }
                    }
                });
            }
            else {
                util.showMessage('error', 'Verifique campos em destaque!');
            }
        },
        clearForm: function() {
            util.clear('inputId');
            util.clear('inputNome');
            util.clear('inputEmail');
            util.clear('inputUsername');
            util.clear('inputSenha');
            util.clear('inputTelefone');
            util.clear('inputTipo');
            util.clear('inputInativo');
            util.clear('inputUsuarioCadastro');
            util.clear('inputDataCadastro');
            util.clear('inputMatricula');
            util.clear('inputLimiteMensal');
            util.clear('inputCnpjCpf');
            util.clear('inputCnpj');
            util.clear('inputCpf');
            util.clear('inputAutorizaEticket');
            util.clear('inputObservacao');
            util.clear('inputEmpresaId');
            util.clear('inputEmpresaNome');
            util.clear('inputCentroCustoId');
            util.clear('inputCentroCustoNome');
        },
        isValid: function() {
            return this.ui.form.validationEngine('validate', {
                promptPosition: "topLeft",
                isOverflown: false,
                validationEventTrigger: "change"
            });
        },
        getModel: function() {
            var that = this;
            var cliente = that.model;
            cliente.set({
                id: util.escapeById('inputId') || null,
                nome: util.escapeById('inputNome'),
                email: util.escapeById('inputEmail'),
                username: util.escapeById('inputUsername'),
                senha: util.escapeById('inputSenha'),
                telefone: util.escapeById('inputTelefone'),
                tipo: util.escapeById('inputTipo'),
                inativo: util.escapeById('inputInativo'),
                usuarioCadastro: util.escapeById('inputUsuarioCadastro', true),
                dataCadastro: util.escapeById('inputDataCadastro'),
                matricula: util.escapeById('inputMatricula', true),
                limiteMensal: util.escapeById('inputLimiteMensal', true),
                cnpjCpf: util.escapeById('inputCnpjCpf'),
                cnpj: util.escapeById('inputCnpj'),
                cpf: util.escapeById('inputCpf'),
                autorizaEticket: util.escapeById('inputAutorizaEticket'),
                observacao: util.escapeById('inputObservacao'),
                empresa: that.modalEmpresa.getJsonValue(),
                centroCusto: that.modalCentroCusto.getJsonValue(),
            });
            return cliente;
        },
        showModalEmpresa: function() {
            // add more before the modal is open
            this.modalEmpresa.showPage();
        },
        onSelectEmpresa: function(empresa) {
            this.modalEmpresa.hidePage();
            this.ui.inputEmpresaId.val(empresa.get('id'));
            this.ui.inputEmpresaNome.val(empresa.get('nomeFantasia'));
            this.modalCentroCusto.setEmpresa(this.ui.inputEmpresaId.val());;
        },
        showModalCentroCusto: function() {
            // add more before the modal is open
            this.modalCentroCusto.showPage();
        },
        onSelectCentroCusto: function(centroCusto) {
            this.modalCentroCusto.hidePage();
            this.ui.inputCentroCustoId.val(centroCusto.get('id'));
            this.ui.inputCentroCustoDescricao.val(centroCusto.get('descricao'));
            this.limiteDisponivelCC(centroCusto.toJSON());
        },
        //TODO: Refatorar função utilizando variáveis globais e, se for o caso, dividir a função
        limiteDisponivelCC: function(centroCusto) {
            if (this.ui.inputTipo.val() != 'FUNCIONARIO') return;
        	var limiteUtilizado = 0;
            //Monta valor de limiteUtilizado com os funcionários do CC menos o funcionário corrente
            for (var index in centroCusto.clientes)
                if (this.model.get('id') != centroCusto.clientes[index].id) limiteUtilizado += centroCusto.clientes[index].limiteMensal;
            //Levar tratamento para a máscara
            //Se o valor inputado for inteiro, ele é convertido para float pq a máscara apaga valores inteiros
            if (this.ui.inputLimiteMensal.val() % 1 == 0) this.ui.inputLimiteMensal.val(util.formatFinalNumber(this.ui.inputLimiteMensal.val()));
            //Substitui a virgula do inputLimiteMensal e transforma para float para operações.
            var limiteFuncionarioEdit = parseFloat(util.strToNum(this.ui.inputLimiteMensal.val()));
            //TODO: Criar variável global para limite Utilizado
            this.ui.inputLimiteDisponivel.val(limiteUtilizado);
            //Se o limite do funcionário + o limiteUtilizado (limiteFuncionarioEdit) for NaN ou o limite do CC for estourado, o cálculo é resetado
            if (isNaN(limiteFuncionarioEdit) || centroCusto.valorLimite < limiteUtilizado + limiteFuncionarioEdit) {
                limiteFuncionarioEdit = 0;
                //this.ui.inputLimiteMensal.val('0,00')
                this.ui.spanLimiteDisponivel.text('Limite Disponível R$ ' + parseFloat(centroCusto.valorLimite - limiteUtilizado).toFixed(2));
                util.showMessage('error', 'O limite mensal ultrapassa o limite disponível do centro de custo ' + centroCusto.descricao + '!');
                return false;
            }
            this.ui.spanLimiteDisponivel.text('Limite Disponível R$ ' + parseFloat(centroCusto.valorLimite - (limiteUtilizado + limiteFuncionarioEdit)).toFixed(2));
            return true;
        },
        acionaLimiteDisponivelCC: function() {
            var centroCusto = this.model.get('centroCusto') == null && this.modalCentroCusto.getJsonValue() != null ? this.modalCentroCusto.getJsonValue() : this.model.get('centroCusto');
            if (centroCusto != null) this.limiteDisponivelCC(centroCusto);
        },
        showHideEmpresa: function() {
            if (this.ui.inputTipo.val() == 'FUNCIONARIO') {
                this.ui.groupInputEmpresaContainer.show('slow');
                this.ui.groupInputCentroCustoContainer.show('slow');
                this.ui.groupInputLimiteMensal.show('slow');
                this.ui.groupInputAutorizaEticket.show('slow');
                this.ui.inputEmpresaNome.addClass('validate[required]');
                this.ui.inputCentroCustoId.addClass('validate[required]');
                this.ui.inputLimiteMensal.addClass('validate[required]');
                this.ui.inputCentroCustoId.addClass('validate[required]');
            }
            else {
                this.ui.groupInputEmpresaContainer.hide('slow');
                this.ui.groupInputCentroCustoContainer.hide('slow');
                this.ui.groupInputLimiteMensal.hide('slow');
                this.ui.groupInputAutorizaEticket.hide('slow');
                this.ui.inputEmpresaNome.removeClass('validate[required]');
                this.ui.inputCentroCustoId.removeClass('validate[required]');
                this.ui.inputLimiteMensal.removeClass('validate[required]');
                this.ui.inputCentroCustoId.removeClass('validate[required]');
                // Limpa dados da empresa
                this.modalEmpresa.setValue(null);
                this.ui.inputEmpresaId.val(null);
                this.ui.inputEmpresaNome.val(null);
                // Limpa dados do centroCusto
                this.modalCentroCusto.setValue(null);
				this.ui.inputCentroCustoId.val(null);
				this.ui.inputCentroCustoDescricao.val(null);
                // Limpa dados do limite disponivel
				this.ui.spanLimiteDisponivel.text(null);
				this.ui.inputLimiteDisponivel.val(null);
                this.ui.inputLimiteMensal.val(null);
            }
        },
        uniqueCpf: function() {
            var that = this;
            util.validateUnique({
                element: that.ui.inputCpf,
                fieldName: 'cpf',
                fieldDisplayName: 'CPF',
                view: that,
                collection: ClienteCollection,
            })
        },
        uniqueTelefone: function() {
            var that = this;
            util.validateUnique({
                element: that.ui.inputTelefone,
                fieldName: 'telefone',
                fieldDisplayName: 'Telefone',
                view: that,
                collection: ClienteCollection,
            })
        },
        checkButtonAuthority: function() {
            var newButton = this.ui.newButton;
            var saveButton = this.ui.saveButton;
            var saveContinueButton = this.ui.saveContinueButton;
            $.grep(roles, function(e) {
                if (e.authority == 'ROLE_CLIENTE_CRIAR' || e.authority == 'ROLE_ADMIN') {
                    newButton.show();
                    saveButton.show();
                    saveContinueButton.show();
                }
                if (e.authority == 'ROLE_CLIENTE_EDITAR' || e.authority == 'ROLE_ADMIN') {
                    saveButton.show();
                }
            })
        },
    });
    return FormClientes;
});