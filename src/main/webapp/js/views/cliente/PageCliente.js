/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var client = require('adapters/auth-adapter');
	var roles = client.roles
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');
	var CustomNumberCell = require('views/components/CustomNumberCell');
	var ClienteModel = require('models/ClienteModel');
	var ClienteCollection = require('collections/ClienteCollection');
	var EmpresaCollection = require('collections/EmpresaCollection');
	var ClientePageCollection = require('collections/ClientePageCollection');
	var ModalEmpresa = require('views/modalComponents/EmpresaModal');
	var ModalCentroCusto = require('views/modalComponents/CentroCustoModal');
	var PageClienteTemplate = require('text!views/cliente/tpl/PageClienteTemplate.html');
	// End of "Import´s" definition
	var PageCliente = Marionette.LayoutView.extend({
		template: _.template(PageClienteTemplate),
		regions: {
			gridRegion: '#grid',
			counterRegion: '#counter',
			paginatorRegion: '#paginator',
			modalEmpresaRegion: '#empresaModal',
			modalCentroCustoRegion: '#centroCustoModal',
		},
		events: {
			'click 	#reset': 'resetCliente',
			'click #searchEmpresaModal': 'showModalEmpresa',
			'click #searchCentroCustoModal': 'showModalCentroCusto',
			'keypress': 'treatKeypress',
			'click 	.search-button': 'searchCliente',
			'click .show-advanced-search-button': 'toggleAdvancedForm',
		},
		ui: {
			inputNome: '#inputNome',
			inputTelefoneFavorito : '#inputTelefoneFavorito',
			inputEmail: '#inputEmail',
			inputSenha: '#inputSenha',
			inputTipo: '#inputTipo',
			inputInativo: '#inputInativo',
			inputEmpresaContrato: '#inputEmpresaContrato',
			inputEmpresaId: '#inputEmpresaId',
			inputEmpresaNome: '#inputEmpresaNome',
			inputCentroCustoId: '#inputCentroCustoId',
			inputCentroCustoNome: '#inputCentroCustoNome',
			form: '#formClienteFilter',
			advancedSearchForm: '.advanced-search-form',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		toggleAdvancedForm: function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},
		treatKeypress: function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchCliente();
			}
		},
		initialize: function() {
			var that = this;
			this.clientes = new ClientePageCollection();
			this.grid = new Backgrid.Grid({
				className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns: this.getColumns(),
				emptyText: "Sem registros",
				collection: this.clientes
			});
			this.grid.render().sort('id', 'descending');
			this.counter = new Counter({
				collection: this.clientes,
			});
			this.paginator = new Backgrid.Extension.Paginator({
				columns: this.getColumns(),
				collection: this.clientes,
				className: ' paging_simple_numbers',
				uiClassName: 'pagination',
			});
			this.modalEmpresa = new ModalEmpresa({
				onSelectModel: function(model) {
					that.onSelectEmpresa(model);
				},
			});
			this.modalCentroCusto = new ModalCentroCusto({
				onSelectModel: function(model) {
					that.onSelectCentroCusto(model);
				},
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				this.searchCliente();
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.ui.inputTelefoneFavorito.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.modalEmpresaRegion.show(this.modalEmpresa);
				this.modalCentroCustoRegion.show(this.modalCentroCusto);
				this.checkButtonAuthority();
			});
		},
		searchCliente: function() {
			var that = this;
			if(client.empresa !== null){
				if (!util.escapeById('inputNome')) {
					this.clientes.filterQueryParams = {
						empresa: client.empresa.id,
						nome: util.escapeById('inputAvancadoNome'),
						telefonesFavoritos: util.escapeById('inputTelefoneFavorito'),
						email: util.escapeById('inputEmail'),
						senha: util.escapeById('inputSenha'),
						tipo: util.escapeById('inputTipo'),
						tipo: util.escapeById('inputTipo'),
						inativo: util.escapeById('inputInativo'),
					}
				}
				else {
					this.clientes.filterQueryParams = {
						empresa: client.empresa.id,
						nome: util.escapeById('inputNome'),
					}
				}
				console.log('tem empresa');
				this.clientes.fetch({
					data:{empresa:  client.empresa.id,},
					dataProcess: true,
					success: function(_coll, _resp, _opt) {
						console.info('Consulta para o grid cliente');
					},
					error: function(_coll, _resp, _opt) {
						if(_resp.status == '403'){
							util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
						} 
					},
					complete: function() {},
				})
			} else {
				if (!util.escapeById('inputNome')) {
					this.clientes.filterQueryParams = {
						nome: util.escapeById('inputAvancadoNome'),
						telefonesFavoritos: util.escapeById('inputTelefoneFavorito'),
						email: util.escapeById('inputEmail'),
						senha: util.escapeById('inputSenha'),
						tipo: util.escapeById('inputTipo'),
						tipo: util.escapeById('inputTipo'),
						inativo: util.escapeById('inputInativo'),
						contrato : util.escapeById('inputContrato'),
					}
				} else {
					this.clientes.filterQueryParams = {
						nome: util.escapeById('inputNome'),
					}
				}
				this.clientes.fetch({
					success: function(_coll, _resp, _opt) {
						console.info('Consulta para o grid cliente');
					},
					error: function(_coll, _resp, _opt) {
						if(_resp.status == '403'){
							util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
						} 
					},
					complete: function() {},
				})
			}
			
		},
		resetCliente: function() {
			this.ui.form.get(0).reset();
			this.clientes.reset();
			util.clear('inputEmpresaId');
			util.clear('inputCentroCustoId');
		},
		getColumns: function() {
			var that = this;
			var columns = [{
				name : "id",
				editable : false,
				sortable : true,
				label : "N° Cliente",
				cell : "string",
			}, {
				name : "nome",
				editable : false,
				sortable : true,
				label : "Nome",
				cell : "string",
			}, {
				name: "email",
				editable: false,
				sortable: true,
				label: "E-mail",
				cell: "string",
			}, {
				name: "inativo",
				editable: false,
				sortable: true,
				label: "Status",
				cell: "string",
				formatter: _.extend({}, Backgrid.CellFormatter.prototype, {
					fromRaw: function(rawValue, model) {
						return (rawValue ? "Inativo" : "Ativo");
					}
				})
			}, {
				name: "tipo",
				editable: false,
				sortable: true,
				label: "Tipo",
				cell: "string",
				formatter: _.extend({}, Backgrid.CellFormatter.prototype, {
					fromRaw: function(rawValue, model) {
						if (rawValue == 'CADASTRO_CENTRAL') return 'Cadastro Central';
						if (rawValue == 'APP') return 'App';
						if (rawValue == 'FUNCIONARIO') return 'Funcionário';
						if (rawValue == 'ESTABELECIMENTO') return 'Estabelecimento';
					}
				})
			}, {
				name: "empresa.contrato",
				editable: false,
				sortable: true,
				label: "Contrato",
				cell: CustomStringCell.extend({
					fieldName: 'empresa.contrato',
				}),
			}, {
				name: "acoes",
				label: "Ações(Editar, Deletar)",
				sortable: false,
				cell: GeneralActionsCell.extend({
					buttons: that.getCellButtons(),
					context: that,
				})
			}];
			return columns;
		},
		getCellButtons: function() {
			var that = this;
			var buttons = this.checkGridButtonAuthority();
			return buttons;
		},
		verTelefones: function(model) {
			util.goPage("app/cliente/" + model.get('id') + '/clienteFones');
		},
		verEnderecos: function(model) {
			util.goPage("app/cliente/" + model.get('id') + '/enderecos');
		},
		verMotoristasBloqueados: function(model) {
			util.goPage("app/cliente/" + model.get('id') + '/motoristaBloqueados');
		},
		deleteModel: function(model) {
			var that = this;
			var modelTipo = new ClienteModel({
				id: model.id,
			});
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success: function() {
							that.clientes.remove(model);
							util.showSuccessMessage('Cliente removido com sucesso!');
						},
						error: function(_model, _resp) {
							util.showErrorMessage('O cliente possui outros registro dependentes e não pode ser removido', _resp);
						}
					});
				}
			});
		},
		editModel: function(model) {
			util.goPage("app/editCliente/" + model.get('id'));
		},
		showModalEmpresa: function() {
			this.modalEmpresa.showPage();
		},
		onSelectEmpresa: function(empresa) {
			this.modalEmpresa.hidePage();
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNome.val(empresa.get('nome'));
		},
		showModalCentroCusto: function() {
			this.modalCentroCusto.showPage();
		},
		onSelectCentroCusto: function(centroCusto) {
			this.modalCentroCusto.hidePage();
			this.ui.inputCentroCustoId.val(centroCusto.get('id'));
			this.ui.inputCentroCustoNome.val(centroCusto.get('nome'));
		},
		// vitoriano : chunck : check grid buttons authority
		checkGridButtonAuthority: function() {
			var that = this;
			var buttons = [];
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_TELEFONE' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'telefone_button',
						type: 'primary',
						icon: 'fa-phone',
						hint: 'Telefones',
						onClick: that.verTelefones,
					});
				}
				if (e.authority == 'ROLE_ENDERECO' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'endereco_button',
						type: 'primary',
						icon: 'fa-map-marker',
						hint: 'Endereços',
						onClick: that.verEnderecos,
					});
				}
				if (e.authority == 'ROLE_MOTOQUEIROBLOQUEADO' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'motorista_bloqueados_button',
						type: 'primary',
						icon: 'fa-ban',
						hint: 'Motoqueiros Bloqueados',
						onClick: that.verMotoristasBloqueados,
					});
				}
				if (e.authority == 'ROLE_CLIENTE_EDITAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'edita_ficha_button',
						type: 'primary',
						icon: 'icon-pencil fa-pencil',
						hint: 'Editar Cliente',
						onClick: that.editModel,
					});
				}
				if (e.authority == 'ROLE_CLIENTE_DELETAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'delete_button',
						type: 'danger',
						icon: 'icon-trash fa-trash',
						hint: 'Remover Cliente',
						onClick: that.deleteModel,
					});
				}
			})
			return buttons;
		},
		// vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_CLIENTE_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
	});
	return PageCliente;
});