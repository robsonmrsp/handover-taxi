/* generated: 25/10/2016 10:24:45 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	// views/components/SuggestGmapBox
	var SuggestGmapBox = require('views/components/SuggestGmapBox');

	var TemplateFormCorridas = require('text!views/corrida/tpl/FormCorridaTemplate.html');
	var CorridaModel = require('models/CorridaModel');
	var CorridaEnderecoModel = require('models/CorridaEnderecoModel');
	var CorridaCollection = require('collections/CorridaCollection');
	var ModalAtendente = require('views/modalComponents/AtendenteModal');

	var ModalMotorista = require('views/modalComponents/MotoristaModal');

	var CheckGroup = require('views/components/CheckGroup');
	var RadioGroup = require('views/components/RadioGroup');

	var GMap = require('views/components/GMap');

	var BaseCollection = require('collections/BaseCollection');
	var ClienteCollection = require('collections/ClienteCollection');

	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
	// BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormCorridas = Marionette.LayoutView.extend({
		template : _.template(TemplateFormCorridas),

		regions : {
			modalAtendenteRegion : '#atendenteModal',
			modalMotoristaRegion : '#motoristaModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'change	#inputFone' : 'buscaCliente',
			'click #searchAtendenteModal' : 'showModalAtendente',
			'click #searchMotoristaModal' : 'showModalMotorista',
		},

		ui : {
			inputId : '#inputId',
			inputDataCorrida : '#inputDataCorrida',
			groupInputDataCorrida : '#groupInputDataCorrida',
			inputNome : '#inputNome',
			inputFone : '#inputFone',
			inputComplemento : '#inputComplemento',
			inputReferenciaAceite : '#inputReferenciaAceite',
			inputFoneRetorno : '#inputFoneRetorno',
			inputTipoPagamento : '#inputTipoPagamento',

			inputEnderecoDestino : '#inputEnderecoDestino',
			inputEnderecoPartida : '#inputEnderecoPartida',

			inputValor : '#inputValor',
			inputObservacao : '#inputObservacao',
			inputStatusCorrida : '#inputStatusCorrida',
			groupInputTipoPagamento : '.groupInputTipoPagamento',

			groupExigencias : '.groupExigencias',

			inputEnderecoPartida : '#inputEnderecoPartida',
			inputEnderecoDestino : '#inputEnderecoDestino',

			inputAtendenteId : '#inputAtendenteId',
			inputAtendenteNome : '#inputAtendenteNome',
			inputMotoristaId : '#inputMotoristaId',
			inputMotoristaNome : '#inputMotoristaNome',

			mapa : '#map',
			form : '#formCorrida',
		},
		buscaCliente : function() {
			var that = this;
			var clientes = new ClienteCollection();

			clientes.filter({
				success : function(clientes, res, opt) {
					if (clientes.at(0)) {
						var cliente = clientes.at(0)
						that.ui.inputNome.val(cliente.get('nome'));
					}
					console.debug('Sucesso na busca do cliente');
				},

				error : function(model, resp, opt) {
					console.error('Error na busca do cliente');
				},

				data : {
					telefone : this.ui.inputFone.val(),
				}
			})
		},

		initialize : function() {
			var that = this;
			this.modalAtendente = new ModalAtendente({
				onSelectModel : function(model) {
					that.onSelectAtendente(model);
				},
			});
			this.modalMotorista = new ModalMotorista({
				onSelectModel : function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.modalMotorista.setValue(this.model.get('motorista'));

			this.on('show', function() {
				// adição das regioes
				this.modalAtendenteRegion.show(this.modalAtendente);
				this.modalMotoristaRegion.show(this.modalMotorista);

				// Formatação dos telefones
				this.ui.inputFone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.ui.inputFoneRetorno.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);

				// formatação dos campos de data
				this.ui.groupInputDataCorrida.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataCorrida.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataCorrida.mask('99/99/9999 99:99');
				this.ui.inputTipoPagamento.formatNumber(2);
				this.ui.inputValor.formatNumber(2);

				// Radios e checkgroups
				this.tipoPagamento = new RadioGroup({
					container : this.ui.groupInputTipoPagamento,
				});

				this.tipoPagamento.setValue(this.model.get('tipoPagamento'));

				this.exigencias = new CheckGroup({
					container : this.ui.groupExigencias,
				});

				this.exigencias.setValue(this.model.get('exigencias'));

				// Manipuações que envolvem mapa
				this.gMap = new GMap({
					mapElement : this.ui.mapa,
				})

				this.enderecoPartida = new SuggestGmapBox({
					el : this.ui.inputEnderecoPartida,
					onSelect : function(jsonEndereco) {
						that.gMap.addMarker(jsonEndereco.latlong, jsonEndereco.endereco);
					}
				});

				// this.enderecoPartida.setValue('R. Cento e Treze, 40 - Timbo,
				// Maracanaú - CE, 61936-130, Brasil');
				// this.ui.inputEnderecoPartida.val('R. Cento e Treze, 40 -
				// Timbo, Maracanaú - CE, 61936-130, Brasil');
				// this.enderecoPartida.setValue(this.model.get('enderecoPartida'));
				this.enderecoDestino = new SuggestGmapBox({
					el : this.ui.inputEnderecoDestino,
					onSelect : function(jsonEndereco) {
						that.gMap.addMarker(jsonEndereco.latlong, jsonEndereco.endereco);
					}
				});
				// this.enderecoDestino.setValue(this.model.get('enderecoDestino'));
				//
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});

			});
		},

		saveAndContinue : function() {
			this.save(true)
		},
		search : function geocodeAddress() {

		},
		save : function(continua) {
			var that = this;
			var corrida = that.getModel();

			if (this.isValid()) {
				corrida.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Corrida salvo com sucesso!');
//						that.clearForm();

//						if (continua != true) {
//							util.goPage('app/corridas');
//						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro', _resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		clearForm : function() {
			util.clear('inputId');
			util.clear('inputDataCorrida');
			util.clear('inputTipoPagamento');
			util.clear('inputValor');
			util.clear('inputFone');
			util.clear('inputFoneRetorno');
			util.clear('inputComplemento');
			util.clear('inputReferenciaAceite');
			util.clear('inputObservacao');
			util.clear('inputStatusCorrida');
			util.clear('inputAtendenteId');
			util.clear('inputAtendenteNome');
			util.clear('inputMotoristaId');
			util.clear('inputMotoristaNome');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var corrida = that.model;
			corrida.set({
				id : util.escapeById('inputId') || null,
				dataCorrida : util.escapeById('inputDataCorrida'),
				tipoPagamento : that.tipoPagamento.getValue(),
				exigencias : that.exigencias.getValue(),
				valor : util.escapeById('inputValor', true),

				enderecoPartida : this.enderecoPartida.getTexto(),
				enderecoDestino : this.enderecoDestino.getTexto(),

				nome : util.escapeById('inputNome'),
				fone : util.escapeById('inputFone'),
				foneRetorno : util.escapeById('inputFoneRetorno'),
				complemento : util.escapeById('inputComplemento'),
				referenciaAceite : util.escapeById('inputReferenciaAceite'),

				observacao : util.escapeById('inputObservacao'),

				statusCorrida : util.escapeById('inputStatusCorrida'),

				atendente : that.modalAtendente.getJsonValue(),

				motorista : that.modalMotorista.getJsonValue(),

			// corridaEnderecos : that.getEnderecosCorrida(),
			});
			return corrida;
		},

		getEnderecosCorrida : function() {
			var enderecos = [];

			var partida = this.enderecoPartida.getValue();
			var chegada = this.enderecoDestino.getValue();

			var enderecoCorrida = new CorridaEnderecoModel();

			if (partida) {
				enderecoCorrida.set('enderecoOrigem', partida.get('endereco'));
				enderecoCorrida.set('referenciaOrigem', partida.get(''));
			}
			if (chegada) {
				enderecoCorrida.set('enderecoDestino', chegada.get('endereco'));
				enderecoCorrida.set('referenciaDestino', chegada.get(''));
			}

			// enderecoCorrida.set('numeroOrigem', partida.get('') ) ;
			// enderecoCorrida.set('complementoOrigem', partida.get('') ) ;
			// enderecoCorrida.set('bairroOrigem', partida.get('') ) ;
			// enderecoCorrida.set('ufOrigem', partida.get('') ) ;
			// enderecoCorrida.set('cidadeOrigem', partida.get('') ) ;
			// enderecoCorrida.set('cepOrigem', partida.get('') ) ;
			// enderecoCorrida.set('numeroDestino', chegada.get('') ) ;
			// enderecoCorrida.set('complementoDestino', chegada.get('') );
			// enderecoCorrida.set('bairroDestino', chegada.get('') ) ;
			// enderecoCorrida.set('ufDestino', chegada.get('') ) ;
			// enderecoCorrida.set('cidadeDestino', chegada.get('') ) ;
			// enderecoCorrida.set('cepDestino', chegada.get('') ) ;
			// enderecoCorrida.set('descricao', chegada.get('') ) ;

			return enderecoCorrida.toJSON();
		},
		showModalAtendente : function() {
			// add more before the modal is open
			this.modalAtendente.showPage();
		},
		showModalMotorista : function() {
			// add more before the modal is open
			this.modalMotorista.showPage();
		},

		onSelectAtendente : function(atendente) {
			this.modalAtendente.hidePage();
			this.ui.inputAtendenteId.val(atendente.get('id'));
			this.ui.inputAtendenteNome.val(atendente.get('nome'));
		},
		onSelectMotorista : function(motorista) {
			this.modalMotorista.hidePage();
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));
		},

	});

	return FormCorridas;
});