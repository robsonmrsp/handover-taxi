/* generated: 25/10/2016 10:24:45 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var CorridaModel = require('models/CorridaModel');
	var CorridaCollection = require('collections/CorridaCollection');
	var CorridaPageCollection = require('collections/CorridaPageCollection');
	var PageCorridaTemplate = require('text!views/corrida/tpl/PageCorridaTemplate.html');
	
	//Filter import
	var ModalAtendente = require('views/modalComponents/AtendenteModal');
	var ModalMotorista = require('views/modalComponents/MotoristaModal');
	
	// End of "Import´s" definition

	var PageCorrida = Marionette.LayoutView.extend({
		template : _.template(PageCorridaTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			modalAtendenteRegion : '#atendenteModal',
			modalMotoristaRegion : '#motoristaModal',
		},
		
		events : {
			'click 	#reset' : 'resetCorrida',			
			'click #searchAtendenteModal' : 'showModalAtendente',
			'click #searchMotoristaModal' : 'showModalMotorista',
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchCorrida',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
		
			inputAtendenteId : '#inputAtendenteId',
			inputAtendenteNome : '#inputAtendenteNome',
			inputMotoristaId : '#inputMotoristaId',
			inputMotoristaNome : '#inputMotoristaNome',
			form : '#formCorridaFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchCorrida();
	    	}
		},

		initialize : function() {
			var that = this;

			this.corridas = new CorridaPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.corridas
			});

			this.counter = new Counter({
				collection : this.corridas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.corridas,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.corridas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid corrida');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
				}
			});
			this.modalAtendente = new ModalAtendente({
				onSelectModel : function(model) {
					that.onSelectAtendente(model);
				},
			});
			this.modalMotorista = new ModalMotorista({
				onSelectModel : function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.modalAtendenteRegion.show(this.modalAtendente);		
				this.modalMotoristaRegion.show(this.modalMotorista);		
		
			});
		},
		 
		searchCorrida : function(){
			var that = this;

			this.corridas.filterQueryParams = {
			    atendente : util.escapeById('inputAtendenteId'), 
			    motorista : util.escapeById('inputMotoristaId'), 
			}
			this.corridas.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid corrida');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetCorrida : function(){
			this.ui.form.get(0).reset();
			this.corridas.reset();
			util.clear('inputAtendenteId');
			util.clear('inputMotoristaId');
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "atendente.nome",
				editable : false,
				sortable : true,  
				label : "Atendente",
				cell : CustomStringCell.extend({
					fieldName : 'atendente.nome',
				}),
			},	
			{
				name : "motorista.nome",
				editable : false,
				sortable : true,  
				label : "Motorista",
				cell : CustomStringCell.extend({
					fieldName : 'motorista.nome',
				}),
			},	
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Corrida',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Corrida',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new CorridaModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.corridas.remove(model);
							util.showSuccessMessage('Corrida removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editCorrida/" + model.get('id'));
		},

		showModalAtendente : function() {
			this.modalAtendente.showPage();
		},
			
		onSelectAtendente : function(atendente) {
			this.modalAtendente.hidePage();	
			this.ui.inputAtendenteId.val(atendente.get('id'));
			this.ui.inputAtendenteNome.val(atendente.get('nome'));		
		},
		showModalMotorista : function() {
			this.modalMotorista.showPage();
		},
			
		onSelectMotorista : function(motorista) {
			this.modalMotorista.hidePage();	
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));		
		},
		

	});

	return PageCorrida;
});
