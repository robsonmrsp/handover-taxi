/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectMotoristaBloqueado = require('views/motoristaBloqueado/ModalMultiSelectMotoristaBloqueado');
	var MultiSelectMotoristaBloqueadoTemplate = require('text!views/motoristaBloqueado/tpl/MultiSelectMotoristaBloqueadoTemplate.html');

	var MultiSelectMotoristaBloqueado = Marionette.LayoutView.extend({
		template : _.template(MultiSelectMotoristaBloqueadoTemplate),

		regions : {
			modalMultiSelectMotoristaBloqueadoRegion : '#modalMultiSelectMotoristaBloqueados',
			gridMotoristaBloqueadosModalRegion : '#gridMultiselectMotoristaBloqueados',
		},

		initialize : function() {
			var that = this;

			this.motoristaBloqueados = this.collection;

			this.gridMotoristaBloqueados = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.motoristaBloqueados,
			});

			this.modalMultiSelectMotoristaBloqueado = new ModalMultiSelectMotoristaBloqueado({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectMotoristaBloqueadoRegion.show(that.modalMultiSelectMotoristaBloqueado);
				that.gridMotoristaBloqueadosModalRegion.show(that.gridMotoristaBloqueados);
			});
		},
		clear : function(){
			this.modalMultiSelectMotoristaBloqueado.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "dataBloqueio",
				editable : false,
				sortable : false,
				label 	 : "Data bloqueio",
				cell 	 : "string",
			}, 
			{
				name : "usuarioBloqueio",
				editable : false,
				sortable : false,
				label 	 : "Usuario bloqueio",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return MultiSelectMotoristaBloqueado
});
