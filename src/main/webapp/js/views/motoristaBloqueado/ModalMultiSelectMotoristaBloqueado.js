/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var MotoristaBloqueadoPageCollection = require('collections/MotoristaBloqueadoPageCollection');
	var ModalMultiSelectMotoristaBloqueadoTemplate = require('text!views/motoristaBloqueado/tpl/ModalMultiSelectMotoristaBloqueadoTemplate.html');
	// End of "Import´s" definition

	var ModalMotoristaBloqueados = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectMotoristaBloqueadoTemplate),

		regions : {
			gridRegion : '#grid-motoristaBloqueados-modal',
			paginatorRegion : '#paginator-motoristaBloqueados-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoMotoristaBloqueados = this.collection;
			
			this.motoristaBloqueados = new MotoristaBloqueadoPageCollection();
			this.motoristaBloqueados.on('fetched', this.endFetch, this);
			this.motoristaBloqueados.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.motoristaBloqueados,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.motoristaBloqueados,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.motoristaBloqueados.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid motoristaBloqueado');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoMotoristaBloqueados.add(model)
			else
				this.projetoMotoristaBloqueados.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.motoristaBloqueados.each(function(model) {
				if (that.projetoMotoristaBloqueados.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "dataBloqueio",
				editable : false,
				sortable : false,
				label 	 : "Data bloqueio",
				cell 	 : "string",
			}, 
			{
				name : "usuarioBloqueio",
				editable : false,
				sortable : false,
				label 	 : "Usuario bloqueio",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return ModalMotoristaBloqueados;
});
