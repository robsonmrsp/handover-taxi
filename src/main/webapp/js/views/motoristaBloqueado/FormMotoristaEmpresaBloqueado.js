/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormMotoristaBloqueados = require('text!views/motoristaBloqueado/tpl/FormMotoristaEmpresaBloqueadoTemplate.html');
	var MotoristaBloqueadoModel = require('models/MotoristaBloqueadoModel');
	var MotoristaBloqueadoCollection = require('collections/MotoristaBloqueadoCollection');
	var ModalEmpresa = require('views/modalComponents/EmpresaModal');
	var ModalMotorista = require('views/modalComponents/MotoristaModal');

	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
	// BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormMotoristaBloqueados = Marionette.LayoutView.extend({
		template : _.template(TemplateFormMotoristaBloqueados),

		regions : {
			modalEmpresaRegion : '#empresaModal',
			modalMotoristaRegion : '#motoristaModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click #searchEmpresaModal' : 'showModalEmpresa',
			'click 	.go-back-link' : 'goBack',
			'click #searchMotoristaModal' : 'showModalMotorista',
		},

		ui : {
			inputEmpresaId : '#inputEmpresaId',
			inputNomeFantasia : '#inputNomeFantasia',
			inputCnpj : '#inputCnpj',
			inputContrato : '#inputContrato',
			inputRazaoSocial : '#inputRazaoSocial',
			
			inputId : '#inputId',
			inputDataBloqueio : '#inputDataBloqueio',
			groupInputDataBloqueio : '#groupInputDataBloqueio',
			inputUsuarioBloqueio : '#inputUsuarioBloqueio',

			inputMotoristaId : '#inputMotoristaId',
			inputMotoristaNome : '#inputMotoristaNome',
			form : '#formMotoristaBloqueado',
		},

		initialize : function(options) {
			var that = this;
			this.empresa = options.empresa

			this.modalEmpresa = new ModalEmpresa({
				onSelectModel : function(model) {
					that.onSelectEmpresa(model);
				},
			});

			this.modalMotorista = new ModalMotorista({
				onSelectModel : function(model) {
					that.onSelectMotorista(model);
				},
			});

			this.modalMotorista.setValue(this.model.get('motorista'));

			this.on('show', function() {
				this.modalEmpresaRegion.show(this.modalEmpresa);
				this.modalMotoristaRegion.show(this.modalMotorista);

				this.ui.groupInputDataBloqueio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataBloqueio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataBloqueio.mask('99/99/9999 99:99');
				this.ui.inputUsuarioBloqueio.formatNumber(2);

				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
				this.carregaEmpresa(this.empresa);
			});
		},

		carregaEmpresa : function(empresa) {
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputNomeFantasia.val(empresa.get('nomeFantasia'));
			this.ui.inputCnpj.val(empresa.get('cnpj'));
			this.ui.inputContrato.val(empresa.get('contrato'));
			this.ui.inputRazaoSocial.val(empresa.get('razaoSocial'));
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var motoristaBloqueado = that.getModel();
			console.log(motoristaBloqueado);
			console.log("EMPRESA");
			if (this.isValid()) {
				motoristaBloqueado.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Motoqueiro bloqueado salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/empresa/' + that.empresa.get('id') + '/motoristaBloqueados');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro', _resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		goBack : function() {
			util.goPage('app/empresa/' + this.empresa.get('id') + '/motoristaBloqueados');
		},

		clearForm : function() {
			util.clear('inputId');
			util.clear('inputDataBloqueio');
			util.clear('inputUsuarioBloqueio');
			util.clear('inputEmpresaId');
			util.clear('inputEmpresaNome');
			util.clear('inputMotoristaId');
			util.clear('inputMotoristaNome');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var motoristaBloqueado = that.model;
			motoristaBloqueado.set({
				id : util.escapeById('inputId') || null,
				dataBloqueio : moment().format('DD/MM/YYYY HH:mm'),
				usuarioBloqueio : util.escapeById('inputUsuarioBloqueio', true),
				empresa : that.empresa.toJSON(),
				motorista : that.modalMotorista.getJsonValue(),
			});
			return motoristaBloqueado;
		},

		showModalEmpresa : function() {
			// add more before the modal is open
			this.modalEmpresa.showPage();
		},
		showModalMotorista : function() {
			// add more before the modal is open
			this.modalMotorista.showPage();
		},

		onSelectEmpresa : function(empresa) {
			this.modalEmpresa.hidePage();
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNome.val(empresa.get('nomeFantasia'));
		},
		onSelectMotorista : function(motorista) {
			this.modalMotorista.hidePage();
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));
		},

	});

	return FormMotoristaBloqueados;
});