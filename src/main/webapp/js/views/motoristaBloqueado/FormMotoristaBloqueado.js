/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var TemplateFormMotoristaBloqueados = require('text!views/motoristaBloqueado/tpl/FormMotoristaBloqueadoTemplate.html');
	var MotoristaBloqueadoModel = require('models/MotoristaBloqueadoModel');
	var MotoristaBloqueadoCollection = require('collections/MotoristaBloqueadoCollection');
	var ModalCliente = require('views/modalComponents/ClienteModal');
	var ModalMotorista = require('views/modalComponents/MotoristaModal');
	// End of "Import´s" definition
	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
	// BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################
	var FormMotoristaBloqueados = Marionette.LayoutView.extend({
		template: _.template(TemplateFormMotoristaBloqueados),
		regions: {
			modalClienteRegion: '#clienteModal',
			modalMotoristaRegion: '#motoristaModal',
		},
		events: {
			'click 	.save': 'save',
			'click 	.saveAndContinue': 'saveAndContinue',
			'click #searchClienteModal': 'showModalCliente',
			'click 	.go-back-link': 'goBack',
			'click #searchMotoristaModal': 'showModalMotorista',
		},
		ui: {
			inputClienteId: '#inputClienteId',
			inputClienteNome: '#inputClienteNome',
			inputClienteEmail: '#inputClienteEmail',
			inputClienteTipo: '#inputClienteTipo',
			inputId: '#inputId',
			inputDataBloqueio: '#inputDataBloqueio',
			groupInputDataBloqueio: '#groupInputDataBloqueio',
			inputUsuarioBloqueio: '#inputUsuarioBloqueio',
			inputMotoristaId: '#inputMotoristaId',
			inputMotoristaNome: '#inputMotoristaNome',
			form: '#formMotoristaBloqueado',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		initialize: function(options) {
			var that = this;
			this.cliente = options.cliente
			this.modalCliente = new ModalCliente({
				onSelectModel: function(model) {
					that.onSelectCliente(model);
				},
			});
			this.modalMotorista = new ModalMotorista({
				onSelectModel: function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.modalMotorista.setValue(this.model.get('motorista'));
			this.on('show', function() {
				this.modalClienteRegion.show(this.modalCliente);
				this.modalMotoristaRegion.show(this.modalMotorista);
				this.ui.groupInputDataBloqueio.datetimepicker({
					pickTime: true,
					language: 'pt_BR',
				});
				this.ui.inputDataBloqueio.datetimepicker({
					pickTime: true,
					language: 'pt_BR',
				});
				this.ui.inputDataBloqueio.mask('99/99/9999 99:99');
				this.ui.inputUsuarioBloqueio.formatNumber(2);
				this.ui.form.validationEngine('attach', {
					promptPosition: "topLeft",
					isOverflown: false,
					validationEventTrigger: "change"
				});
				this.carregaCliente(this.cliente);
				this.checkButtonAuthority();
			});
		},
		carregaCliente: function(cliente) {
			this.ui.inputClienteNome.val(cliente.get('nome'));
			this.ui.inputClienteEmail.val(cliente.get('email'));
			this.ui.inputClienteTipo.val(cliente.get('tipo'));
		},
		saveAndContinue: function() {
			this.save(true)
		},
		save: function(continua) {
			var that = this;
			var motoristaBloqueado = that.getModel();
			console.log(motoristaBloqueado);
			console.log("CLIENTE");
			if (this.isValid()) {
				motoristaBloqueado.save({}, {
					success: function(_model, _resp, _options) {
						util.showSuccessMessage('Motoqueiro bloqueado salvo com sucesso!');
						that.clearForm();
						if (continua != true) {
							util.goPage('app/cliente/' + that.cliente.get('id') + '/motoristaBloqueados');
						}
					},
					error: function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro', _resp);
					}
				});
			}
			else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},
		goBack: function() {
			util.goPage('app/cliente/' + this.cliente.get('id') + '/motoristaBloqueados');
		},
		clearForm: function() {
			util.clear('inputId');
			util.clear('inputDataBloqueio');
			util.clear('inputUsuarioBloqueio');
			util.clear('inputClienteId');
			util.clear('inputClienteNome');
			util.clear('inputMotoristaId');
			util.clear('inputMotoristaNome');
		},
		isValid: function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition: "topLeft",
				isOverflown: false,
				validationEventTrigger: "change"
			});
		},
		getModel: function() {
			var that = this;
			var motoristaBloqueado = that.model;
			motoristaBloqueado.set({
				id: util.escapeById('inputId') || null,
				dataBloqueio: moment().format('DD/MM/YYYY HH:mm'),
				usuarioBloqueio: util.escapeById('inputUsuarioBloqueio', true),
				cliente: that.cliente.toJSON(),
				motorista: that.modalMotorista.getJsonValue(),
			});
			return motoristaBloqueado;
		},
		showModalCliente: function() {
			// add more before the modal is open
			this.modalCliente.showPage();
		},
		showModalMotorista: function() {
			// add more before the modal is open
			this.modalMotorista.showPage();
		},
		onSelectCliente: function(cliente) {
			this.modalCliente.hidePage();
			this.ui.inputClienteId.val(cliente.get('id'));
			this.ui.inputClienteNome.val(cliente.get('nome'));
		},
		onSelectMotorista: function(motorista) {
			this.modalMotorista.hidePage();
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));
		},
		// vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_MOTOQUEIROBLOQUEADO_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
				if(e.authority == 'ROLE_MOTOQUEIROBLOQUEADO_EDITAR' || e.authority == 'ROLE_ADMIN') {
					saveButton.show();
				}
			})
		},
	});
	return FormMotoristaBloqueados;
});