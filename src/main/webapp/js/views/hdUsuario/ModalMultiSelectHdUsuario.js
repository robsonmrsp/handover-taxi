/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var HdUsuarioPageCollection = require('collections/HdUsuarioPageCollection');
	var ModalMultiSelectHdUsuarioTemplate = require('text!views/hdUsuario/tpl/ModalMultiSelectHdUsuarioTemplate.html');
	// End of "Import´s" definition

	var ModalHdUsuarios = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectHdUsuarioTemplate),

		regions : {
			gridRegion : '#grid-hdUsuarios-modal',
			paginatorRegion : '#paginator-hdUsuarios-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoHdUsuarios = this.collection;
			
			this.hdUsuarios = new HdUsuarioPageCollection();
			this.hdUsuarios.on('fetched', this.endFetch, this);
			this.hdUsuarios.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.hdUsuarios,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.hdUsuarios,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.hdUsuarios.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid hdUsuario');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoHdUsuarios.add(model)
			else
				this.projetoHdUsuarios.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.hdUsuarios.each(function(model) {
				if (that.projetoHdUsuarios.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "nome",
				editable : false,
				sortable : false,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : false,
				label 	 : "Email",
				cell 	 : "string",
			}, 
			{
				name : "senha",
				editable : false,
				sortable : false,
				label 	 : "Senha",
				cell 	 : "string",
			}, 
			{
				name : "tipo",
				editable : false,
				sortable : false,
				label 	 : "Tipo",
				cell 	 : "string",
			}, 
			{
				name : "perfil",
				editable : false,
				sortable : false,
				label 	 : "Perfil",
				cell 	 : "string",
			}, 
			{
				name : "statusUsuario",
				editable : false,
				sortable : false,
				label 	 : "Status usuario",
				cell 	 : "string",
			}, 
			{
				name : "datainclusao",
				editable : false,
				sortable : false,
				label 	 : "Datainclusao",
				cell 	 : "string",
			}, 
			{
				name : "usuarioinclusao",
				editable : false,
				sortable : false,
				label 	 : "Usuarioinclusao",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return ModalHdUsuarios;
});
