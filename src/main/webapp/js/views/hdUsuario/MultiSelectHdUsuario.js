/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectHdUsuario = require('views/hdUsuario/ModalMultiSelectHdUsuario');
	var MultiSelectHdUsuarioTemplate = require('text!views/hdUsuario/tpl/MultiSelectHdUsuarioTemplate.html');

	var MultiSelectHdUsuario = Marionette.LayoutView.extend({
		template : _.template(MultiSelectHdUsuarioTemplate),

		regions : {
			modalMultiSelectHdUsuarioRegion : '#modalMultiSelectHdUsuarios',
			gridHdUsuariosModalRegion : '#gridMultiselectHdUsuarios',
		},

		initialize : function() {
			var that = this;

			this.hdUsuarios = this.collection;

			this.gridHdUsuarios = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.hdUsuarios,
			});

			this.modalMultiSelectHdUsuario = new ModalMultiSelectHdUsuario({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectHdUsuarioRegion.show(that.modalMultiSelectHdUsuario);
				that.gridHdUsuariosModalRegion.show(that.gridHdUsuarios);
			});
		},
		clear : function(){
			this.modalMultiSelectHdUsuario.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "nome",
				editable : false,
				sortable : false,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : false,
				label 	 : "Email",
				cell 	 : "string",
			}, 
			{
				name : "senha",
				editable : false,
				sortable : false,
				label 	 : "Senha",
				cell 	 : "string",
			}, 
			{
				name : "tipo",
				editable : false,
				sortable : false,
				label 	 : "Tipo",
				cell 	 : "string",
			}, 
			{
				name : "perfil",
				editable : false,
				sortable : false,
				label 	 : "Perfil",
				cell 	 : "string",
			}, 
			{
				name : "statusUsuario",
				editable : false,
				sortable : false,
				label 	 : "Status usuario",
				cell 	 : "string",
			}, 
			{
				name : "datainclusao",
				editable : false,
				sortable : false,
				label 	 : "Datainclusao",
				cell 	 : "string",
			}, 
			{
				name : "usuarioinclusao",
				editable : false,
				sortable : false,
				label 	 : "Usuarioinclusao",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return MultiSelectHdUsuario
});
