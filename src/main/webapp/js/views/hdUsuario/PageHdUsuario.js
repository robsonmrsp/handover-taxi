/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var HdUsuarioModel = require('models/HdUsuarioModel');
	var HdUsuarioCollection = require('collections/HdUsuarioCollection');
	var HdUsuarioPageCollection = require('collections/HdUsuarioPageCollection');
	var PageHdUsuarioTemplate = require('text!views/hdUsuario/tpl/PageHdUsuarioTemplate.html');
	
	//Filter import
	var ModalEmpresa = require('views/modalComponents/EmpresaModal');
	
	// End of "Import´s" definition

	var PageHdUsuario = Marionette.LayoutView.extend({
		template : _.template(PageHdUsuarioTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			modalEmpresaRegion : '#empresaModal',
		},
		
		events : {
			'click 	#reset' : 'resetHdUsuario',			
			'click #searchEmpresaModal' : 'showModalEmpresa',
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchHdUsuario',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
			inputNome : '#inputNome',
			inputEmail : '#inputEmail',
			inputSenha : '#inputSenha',
			inputTipo : '#inputTipo',
			inputPerfil : '#inputPerfil',
			inputStatusUsuario : '#inputStatusUsuario',
			inputDatainclusao : '#inputDatainclusao',
			groupInputDatainclusao : '#groupInputDatainclusao',
			inputUsuarioinclusao : '#inputUsuarioinclusao',
		
			inputEmpresaId : '#inputEmpresaId',
			inputEmpresaNome : '#inputEmpresaNome',
			form : '#formHdUsuarioFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchHdUsuario();
	    	}
		},

		initialize : function() {
			var that = this;

			this.hdUsuarios = new HdUsuarioPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.hdUsuarios
			});

			this.counter = new Counter({
				collection : this.hdUsuarios,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.hdUsuarios,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.hdUsuarios.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid hdUsuario');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
				}
			});
			this.modalEmpresa = new ModalEmpresa({
				onSelectModel : function(model) {
					that.onSelectEmpresa(model);
				},
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.modalEmpresaRegion.show(this.modalEmpresa);		
				this.ui.groupInputDatainclusao.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDatainclusao.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDatainclusao.mask('99/99/9999 99:99');
				this.ui.inputUsuarioinclusao.formatNumber(2);
		
			});
		},
		 
		searchHdUsuario : function(){
			var that = this;

			this.hdUsuarios.filterQueryParams = {
	    		nome : util.escapeById('inputNome'),
	    		email : util.escapeById('inputEmail'),
	    		senha : util.escapeById('inputSenha'),
	    		tipo : util.escapeById('inputTipo'),
	    		perfil : util.escapeById('inputPerfil'),
	    		statusUsuario : util.escapeById('inputStatusUsuario'),
	    		datainclusao : util.escapeById('inputDatainclusao'),
	    		usuarioinclusao : util.escapeById('inputUsuarioinclusao'),
			    empresa : util.escapeById('inputEmpresaId'), 
			}
			this.hdUsuarios.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid hdUsuario');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetHdUsuario : function(){
			this.ui.form.get(0).reset();
			this.hdUsuarios.reset();
			util.clear('inputEmpresaId');
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "nome",
				editable : false,
				sortable : true,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : true,
				label 	 : "Email",
				cell 	 : "string",
			}, 
			{
//				name : "senha",
//				editable : false,
//				sortable : true,
//				label 	 : "Senha",
//				cell 	 : "string",
//			}, 
//			{
				name : "tipo",
				editable : false,
				sortable : true,
				label 	 : "Tipo",
				cell 	 : "string",
			}, 
			{
				name : "perfil",
				editable : false,
				sortable : true,
				label 	 : "Perfil",
				cell 	 : "string",
			}, 
			{
				name : "statusUsuario",
				editable : false,
				sortable : true,
				label 	 : "Status",
				cell 	 : "string",
			}, 
			{
//				name : "datainclusao",
//				editable : false,
//				sortable : true,
//				label 	 : "Datainclusao",
//				cell 	 : "string",
//			}, 
//			{
//				name : "usuarioinclusao",
//				editable : false,
//				sortable : true,
//				label 	 : "Usuarioinclusao",
//				cell : CustomNumberCell.extend({}),
//			}, 
//			{
//				name : "empresa.nome",
//				editable : false,
//				sortable : true,  
//				label : "Empresa",
//				cell : CustomStringCell.extend({
//					fieldName : 'empresa.nome',
//				}),
//			},	
//			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Hd usuario',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Hd usuario',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new HdUsuarioModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.hdUsuarios.remove(model);
							util.showSuccessMessage('Hd usuario removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editHdUsuario/" + model.get('id'));
		},

		showModalEmpresa : function() {
			this.modalEmpresa.showPage();
		},
			
		onSelectEmpresa : function(empresa) {
			this.modalEmpresa.hidePage();	
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNome.val(empresa.get('nome'));		
		},
		

	});

	return PageHdUsuario;
});
