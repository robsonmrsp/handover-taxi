/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormHdUsuarios = require('text!views/hdUsuario/tpl/FormHdUsuarioTemplate.html');
	var HdUsuarioModel = require('models/HdUsuarioModel');
	var HdUsuarioCollection = require('collections/HdUsuarioCollection');
	var ModalEmpresa = require('views/modalComponents/EmpresaModal');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
	// BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormHdUsuarios = Marionette.LayoutView.extend({
		template : _.template(TemplateFormHdUsuarios),

		regions : {
			modalEmpresaRegion : '#empresaModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click #searchEmpresaModal' : 'showModalEmpresa',
			'change #inputTipo' : 'showHideEmpresa',
			'change #inputEmail' : 'uniqueEmail'
		},
		
		ui : {
			inputId : '#inputId',
			inputNome : '#inputNome',
			inputEmail : '#inputEmail',
			inputSenha : '#inputSenha',
			inputTipo : '#inputTipo',
			inputPerfil : '#inputPerfil',
			inputStatusUsuario : '#inputStatusUsuario',
			inputDatainclusao : '#inputDatainclusao',
			groupInputDatainclusao : '#groupInputDatainclusao',
			inputUsuarioinclusao : '#inputUsuarioinclusao',
			groupInputEmpresaContainer : '#groupInputEmpresaContainer',
		
			inputEmpresaId : '#inputEmpresaId',
			inputEmpresaNome : '#inputEmpresaNome',
			form : '#formHdUsuario',
		},

		initialize : function() {
			var that = this;
			this.modalEmpresa = new ModalEmpresa({
				onSelectModel : function(model) {
					that.onSelectEmpresa(model);
				},
			});
			
			this.modalEmpresa.setValue(this.model.get('empresa'));
			
			this.on('show', function() {
				
				this.modalEmpresaRegion.show(this.modalEmpresa);		
		
				this.ui.groupInputDatainclusao.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDatainclusao.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDatainclusao.mask('99/99/9999 99:99');
				this.ui.inputUsuarioinclusao.formatNumber(2);

				this.showHideEmpresa();
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},
		
		showHideEmpresa : function() {
			if(this.ui.inputTipo.val() == 'Empresa'){
				this.ui.groupInputEmpresaContainer.show();
				this.ui.inputEmpresaNome.addClass('validate[required]');
			}
			else{
				this.ui.groupInputEmpresaContainer.hide();
				this.ui.inputEmpresaNome.removeClass('validate[required]');
			}
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var hdUsuario = that.getModel();

			if (this.isValid()) {
				hdUsuario.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Hd usuario salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/hdUsuarios');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputNome'); 
			util.clear('inputEmail'); 
			util.clear('inputSenha'); 
			util.clear('inputTipo'); 
			util.clear('inputPerfil'); 
			util.clear('inputStatusUsuario'); 
			util.clear('inputDatainclusao'); 
			util.clear('inputUsuarioinclusao'); 
			util.clear('inputEmpresaId');
			util.clear('inputEmpresaNome');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var hdUsuario = that.model; 
			hdUsuario.set({
				id: util.escapeById('inputId') || null,
		    	nome : util.escapeById('inputNome'), 
		    	email : util.escapeById('inputEmail'), 
		    	senha : util.escapeById('inputSenha'), 
		    	tipo : util.escapeById('inputTipo'), 
		    	perfil : util.escapeById('inputPerfil'), 
		    	statusUsuario : util.escapeById('inputStatusUsuario'), 
		    	datainclusao : util.escapeById('inputDatainclusao'), 
		    	usuarioinclusao : util.escapeById('inputUsuarioinclusao', true), 
				empresa : that.modalEmpresa.getJsonValue(),
			});
			return hdUsuario;
		},
		 		
		showModalEmpresa : function() {
			// add more before the modal is open
			this.modalEmpresa.showPage();
		},

		onSelectEmpresa : function(empresa) {
			this.modalEmpresa.hidePage();	
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNome.val(empresa.get('nomeFantasia'));		
		},
				
		uniqueEmail : function() {
			var that = this;
			util.validateUnique({
				element : that.ui.inputEmail,
				fieldName : 'email',
				fieldDisplayName : 'Email',
				view : that,
				collection : HdUsuarioCollection,
			})
		},
		
	});

	return FormHdUsuarios;
});