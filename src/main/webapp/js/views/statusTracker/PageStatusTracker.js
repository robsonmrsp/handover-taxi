/* generated: 04/11/2016 11:17:04 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var StatusTrackerModel = require('models/StatusTrackerModel');
	var StatusTrackerCollection = require('collections/StatusTrackerCollection');
	var StatusTrackerPageCollection = require('collections/StatusTrackerPageCollection');
	var PageStatusTrackerTemplate = require('text!views/statusTracker/tpl/PageStatusTrackerTemplate.html');
	
	//Filter import
	var ModalMotorista = require('views/modalComponents/MotoristaModal');
	
	// End of "Import´s" definition

	var PageStatusTracker = Marionette.LayoutView.extend({
		template : _.template(PageStatusTrackerTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			modalMotoristaRegion : '#motoristaModal',
		},
		
		events : {
			'click 	#reset' : 'resetStatusTracker',			
			'click #searchMotoristaModal' : 'showModalMotorista',
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchStatusTracker',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
		
			inputMotoristaId : '#inputMotoristaId',
			inputMotoristaNome : '#inputMotoristaNome',
			form : '#formStatusTrackerFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchStatusTracker();
	    	}
		},

		initialize : function() {
			var that = this;

			this.statusTrackers = new StatusTrackerPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.statusTrackers
			});

			this.counter = new Counter({
				collection : this.statusTrackers,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.statusTrackers,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.statusTrackers.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid statusTracker');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
				}
			});
			this.modalMotorista = new ModalMotorista({
				onSelectModel : function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.modalMotoristaRegion.show(this.modalMotorista);		
		
			});
		},
		 
		searchStatusTracker : function(){
			var that = this;

			this.statusTrackers.filterQueryParams = {
			    motorista : util.escapeById('inputMotoristaId'), 
			}
			this.statusTrackers.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid statusTracker');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetStatusTracker : function(){
			this.ui.form.get(0).reset();
			this.statusTrackers.reset();
			util.clear('inputMotoristaId');
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "motorista.nome",
				editable : false,
				sortable : true,  
				label : "Motorista",
				cell : CustomStringCell.extend({
					fieldName : 'motorista.nome',
				}),
			},	
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Status tracker',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Status tracker',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new StatusTrackerModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.statusTrackers.remove(model);
							util.showSuccessMessage('Status tracker removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editStatusTracker/" + model.get('id'));
		},

		showModalMotorista : function() {
			this.modalMotorista.showPage();
		},
			
		onSelectMotorista : function(motorista) {
			this.modalMotorista.hidePage();	
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));		
		},
		

	});

	return PageStatusTracker;
});
