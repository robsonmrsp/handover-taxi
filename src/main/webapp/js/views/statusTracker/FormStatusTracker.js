/* generated: 04/11/2016 11:17:04 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormStatusTrackers = require('text!views/statusTracker/tpl/FormStatusTrackerTemplate.html');
	var StatusTrackerModel = require('models/StatusTrackerModel');
	var StatusTrackerCollection = require('collections/StatusTrackerCollection');
	var ModalMotorista = require('views/modalComponents/MotoristaModal');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormStatusTrackers = Marionette.LayoutView.extend({
		template : _.template(TemplateFormStatusTrackers),

		regions : {
			modalMotoristaRegion : '#motoristaModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click #searchMotoristaModal' : 'showModalMotorista',
		},
		
		ui : {
			inputId : '#inputId',
			inputLatitude : '#inputLatitude',
			inputLongitude : '#inputLongitude',
			inputTimestamp : '#inputTimestamp',
			inputSpeed : '#inputSpeed',
			inputAccuracy : '#inputAccuracy',
			inputDirection : '#inputDirection',
			inputAltitude : '#inputAltitude',
			inputBateryLevel : '#inputBateryLevel',
			inputGpsEnabled : '#inputGpsEnabled',
			inputWifiEnabled : '#inputWifiEnabled',
			inputMobileEnabled : '#inputMobileEnabled',
		
			inputMotoristaId : '#inputMotoristaId',
			inputMotoristaNome : '#inputMotoristaNome',
			form : '#formStatusTracker',
		},

		initialize : function() {
			var that = this;
			this.modalMotorista = new ModalMotorista({
				onSelectModel : function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.on('show', function() {
				this.modalMotoristaRegion.show(this.modalMotorista);		
		
				this.ui.inputLatitude.formatNumber(2);
				this.ui.inputLongitude.formatNumber(2);
				this.ui.inputTimestamp.formatNumber(2);
				this.ui.inputSpeed.formatNumber(2);
				this.ui.inputAccuracy.formatNumber(2);
				this.ui.inputDirection.formatNumber(2);
				this.ui.inputAltitude.formatNumber(2);
				this.ui.inputBateryLevel.formatNumber(2);
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var statusTracker = that.getModel();

			if (this.isValid()) {
				statusTracker.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Status tracker salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/statusTrackers');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputLatitude'); 
			util.clear('inputLongitude'); 
			util.clear('inputTimestamp'); 
			util.clear('inputSpeed'); 
			util.clear('inputAccuracy'); 
			util.clear('inputDirection'); 
			util.clear('inputAltitude'); 
			util.clear('inputBateryLevel'); 
			util.clear('inputGpsEnabled'); 
			util.clear('inputWifiEnabled'); 
			util.clear('inputMobileEnabled'); 
			util.clear('inputMotoristaId');
			util.clear('inputMotoristaNome');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var statusTracker = that.model; 
			statusTracker.set({
				id: util.escapeById('inputId') || null,
		    	latitude : util.escapeById('inputLatitude', true), 
		    	longitude : util.escapeById('inputLongitude', true), 
		    	timestamp : util.escapeById('inputTimestamp', true), 
		    	speed : util.escapeById('inputSpeed', true), 
		    	accuracy : util.escapeById('inputAccuracy', true), 
		    	direction : util.escapeById('inputDirection', true), 
		    	altitude : util.escapeById('inputAltitude', true), 
		    	bateryLevel : util.escapeById('inputBateryLevel', true), 
		    	gpsEnabled : util.escapeById('inputGpsEnabled'), 
		    	wifiEnabled : util.escapeById('inputWifiEnabled'), 
		    	mobileEnabled : util.escapeById('inputMobileEnabled'), 
				motorista : that.modalMotorista.getJsonValue(),
			});
			return statusTracker;
		},
		 		
		showModalMotorista : function() {
			// add more before the modal is open
			this.modalMotorista.showPage();
		},

		onSelectMotorista : function(motorista) {
			this.modalMotorista.hidePage();	
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));		
		},
				
		
	});

	return FormStatusTrackers;
});