/* generated: 04/11/2016 11:17:04 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var StatusTrackerPageCollection = require('collections/StatusTrackerPageCollection');
	var ModalMultiSelectStatusTrackerTemplate = require('text!views/statusTracker/tpl/ModalMultiSelectStatusTrackerTemplate.html');
	// End of "Import´s" definition

	var ModalStatusTrackers = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectStatusTrackerTemplate),

		regions : {
			gridRegion : '#grid-statusTrackers-modal',
			paginatorRegion : '#paginator-statusTrackers-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoStatusTrackers = this.collection;
			
			this.statusTrackers = new StatusTrackerPageCollection();
			this.statusTrackers.on('fetched', this.endFetch, this);
			this.statusTrackers.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.statusTrackers,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.statusTrackers,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.statusTrackers.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid statusTracker');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoStatusTrackers.add(model)
			else
				this.projetoStatusTrackers.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.statusTrackers.each(function(model) {
				if (that.projetoStatusTrackers.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "latitude",
				editable : false,
				sortable : false,
				label 	 : "Latitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "longitude",
				editable : false,
				sortable : false,
				label 	 : "Longitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "timestamp",
				editable : false,
				sortable : false,
				label 	 : "Timestamp",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "speed",
				editable : false,
				sortable : false,
				label 	 : "Speed",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "accuracy",
				editable : false,
				sortable : false,
				label 	 : "Accuracy",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "direction",
				editable : false,
				sortable : false,
				label 	 : "Direction",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "altitude",
				editable : false,
				sortable : false,
				label 	 : "Altitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "bateryLevel",
				editable : false,
				sortable : false,
				label 	 : "Batery level",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "gpsEnabled",
				editable : false,
				sortable : false,
				label 	 : "Gps enabled",
				cell 	 : "string",
			}, 
			{
				name : "wifiEnabled",
				editable : false,
				sortable : false,
				label 	 : "Wifi enabled",
				cell 	 : "string",
			}, 
			{
				name : "mobileEnabled",
				editable : false,
				sortable : false,
				label 	 : "Mobile enabled",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalStatusTrackers;
});
