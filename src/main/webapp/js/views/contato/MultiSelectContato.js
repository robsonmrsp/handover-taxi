/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectContato = require('views/contato/ModalMultiSelectContato');
	var MultiSelectContatoTemplate = require('text!views/contato/tpl/MultiSelectContatoTemplate.html');

	var MultiSelectContato = Marionette.LayoutView.extend({
		template : _.template(MultiSelectContatoTemplate),

		regions : {
			modalMultiSelectContatoRegion : '#modalMultiSelectContatos',
			gridContatosModalRegion : '#gridMultiselectContatos',
		},

		initialize : function() {
			var that = this;

			this.contatos = this.collection;

			this.gridContatos = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.contatos,
			});

			this.modalMultiSelectContato = new ModalMultiSelectContato({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectContatoRegion.show(that.modalMultiSelectContato);
				that.gridContatosModalRegion.show(that.gridContatos);
			});
		},
		clear : function(){
			this.modalMultiSelectContato.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "nome",
				editable : false,
				sortable : false,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "ddd",
				editable : false,
				sortable : false,
				label 	 : "DDD",
				cell 	 : "string",
			}, 
			{
				name : "fone",
				editable : false,
				sortable : false,
				label 	 : "Fone",
				cell 	 : "string",
			}, 
			{
				name : "setor",
				editable : false,
				sortable : false,
				label 	 : "Setor",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectContato
});
