/* generated: 16/10/2016 15:27:08 */
define(function(require) {
    // Start "Import´s Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var roles = require('adapters/auth-adapter').roles;
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var CustomStringCell = require('views/components/CustomStringCell');
    var Counter = require('views/components/Counter');
    var ActionsCell = require('views/components/ActionsCell');
    var GeneralActionsCell = require('views/components/GeneralActionsCell');
    var CustomNumberCell = require('views/components/CustomNumberCell');
    var ContatoModel = require('models/ContatoModel');
    var ContatoCollection = require('collections/ContatoCollection');
    var ContatoPageCollection = require('collections/ContatoPageCollection');
    var PageContatoTemplate = require('text!views/contato/tpl/PageContatoTemplate.html');
    // End of "Import´s" definition
    var PageContato = Marionette.LayoutView.extend({
        template: _.template(PageContatoTemplate),
        regions: {
            gridRegion: '#grid',
            counterRegion: '#counter',
            paginatorRegion: '#paginator',
            modalEmpresaRegion: '#empresaModal',
        },
        events: {
            'click 	#reset': 'resetContato',
            'click 	.novo-contato': 'novoContato',
            'click 	.voltar-empresa': 'voltarEmpresa',
            'click 	#reset': 'resetContato',
            'keypress': 'treatKeypress',
            'click 	.search-button': 'searchContato',
            'click .show-advanced-search-button': 'toggleAdvancedForm',
        },
        ui: {
            inputEmpresaNomeFantasia: '#inputEmpresaNomeFantasia',
            inputEmpresaCnpj: '#inputEmpresaCnpj',
            inputEmpresaContrato: '#inputEmpresaContrato',
            inputEmpresaRazaoSocial: '#inputEmpresaRazaoSocial',
            inputNome: '#inputNome',
            inputDdd: '#inputDdd',
            inputFone: '#inputFone',
            inputSetor: '#inputSetor',
            inputEmpresaId: '#inputEmpresaId',
            inputEmpresaNome: '#inputEmpresaNome',
            form: '#formContatoFilter',
            advancedSearchForm: '.advanced-search-form',
            newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
        },
        toggleAdvancedForm: function() {
            this.ui.advancedSearchForm.slideToggle("slow");
        },
        treatKeypress: function(e) {
            if (util.enterPressed(e)) {
                e.preventDefault();
                this.searchContato();
            }
        },
        initialize: function(options) {
            var that = this;
            this.empresa = options.empresa;
            this.contatos = new ContatoPageCollection();
            this.grid = new Backgrid.Grid({
                className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
                columns: this.getColumns(),
                emptyText: "Sem registros",
                collection: this.contatos
            });
            this.counter = new Counter({
                collection: this.contatos,
            });
            this.paginator = new Backgrid.Extension.Paginator({
                columns: this.getColumns(),
                collection: this.contatos,
                className: ' paging_simple_numbers',
                uiClassName: 'pagination',
            });
            this.on('show', function() {
                this.searchContato();
                this.ui.inputFone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
                that.gridRegion.show(that.grid);
                that.counterRegion.show(that.counter);
                that.paginatorRegion.show(that.paginator);
                this.modalEmpresaRegion.show(this.modalEmpresa);
                this.carregaEmpresa(this.empresa);
                this.checkButtonAuthority();
            });
        },
        carregaEmpresa: function(empresa) {
            this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));
            this.ui.inputEmpresaCnpj.val(empresa.get('cnpj'));
            this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
            this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));
        },
        searchContato: function() {
            var that = this;
            if (!util.escapeById('inputNome')) {
                this.contatos.filterQueryParams = {
                    nome: util.escapeById('inputNome'),
                    fone: util.escapeById('inputFone'),
                    setor: util.escapeById('inputSetor'),
                    empresa: that.empresa.get('id'),
                }
            }
            else {
                this.contatos.filterQueryParams = {
                    nome: util.escapeById('inputNome'),
                    empresa: that.empresa.get('id'),
                }
            }
            this.contatos.fetch({
                success: function(_coll, _resp, _opt) {
                    console.info('Consulta para o grid contato');
                },
                error: function(_coll, _resp, _opt) {
                    console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
                },
                complete: function() {},
            })
        },
        resetContato: function() {
            this.ui.form.get(0).reset();
            this.contatos.reset();
        },
        getColumns: function() {
            var that = this;
            var columns = [{
                name: "nome",
                editable: false,
                sortable: true,
                label: "Nome",
                cell: "string",
            }, {
                name: "setor",
                editable: false,
                sortable: true,
                label: "Setor",
                cell: "string",
            }, {
                name: "fone",
                editable: false,
                sortable: true,
                label: "Telefone",
                cell: "string",
            }, {
                name: "acoes",
                label: "Ações(Editar, Deletar)",
                sortable: false,
                cell: GeneralActionsCell.extend({
                    buttons: that.getCellButtons(),
                    context: that,
                })
            }];
            return columns;
        },
        getCellButtons: function() {
            var that = this;
            var buttons = this.checkGridButtonAuthority();
            return buttons;
        },
        deleteModel: function(model) {
            var that = this;
            var modelTipo = new ContatoModel({
                id: model.id,
            });
            util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
                if (yes) {
                    modelTipo.destroy({
                        success: function() {
                            that.contatos.remove(model);
                            util.showSuccessMessage('Contato removido com sucesso!');
                        },
                        error: function(_model, _resp) {
                            util.showErrorMessage('Problema ao remover o registro', _resp);
                        }
                    });
                }
            });
        },
        novoContato: function() {
            util.goPage("app/empresa/" + this.empresa.get('id') + "/newContato");
        },
        voltarEmpresa: function() {
            util.goPage("app/empresas");
        },
        editModel: function(model) {
            util.goPage("app/empresa/" + this.empresa.get('id') + "/editContato/" + model.get('id'));
        },
        // vitoriano : chunck : check grid buttons authority
        checkGridButtonAuthority: function() {
            var that = this;
            var buttons = [];
            $.grep(roles, function(e) {
                if (e.authority == 'ROLE_CONTATO_EDITAR' || e.authority == 'ROLE_ADMIN') {
                    buttons.push({
                        id: 'edita_ficha_button',
                        type: 'primary',
                        icon: 'icon-pencil fa-pencil',
                        hint: 'Editar Contato',
                        onClick: that.editModel,
                    });
                }
                if (e.authority == 'ROLE_CONTATO_DELETAR' || e.authority == 'ROLE_ADMIN') {
                    buttons.push({
                        id: 'delete_button',
                        type: 'danger',
                        icon: 'icon-trash fa-trash',
                        hint: 'Remover Contato',
                        onClick: that.deleteModel,
                    });
                }
            })
            return buttons;
        },
     // vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_CONTATO_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
    });
    return PageContato;
});