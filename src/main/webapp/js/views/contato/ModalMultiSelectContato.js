/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var ContatoPageCollection = require('collections/ContatoPageCollection');
	var ModalMultiSelectContatoTemplate = require('text!views/contato/tpl/ModalMultiSelectContatoTemplate.html');
	// End of "Import´s" definition

	var ModalContatos = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectContatoTemplate),

		regions : {
			gridRegion : '#grid-contatos-modal',
			paginatorRegion : '#paginator-contatos-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoContatos = this.collection;
			
			this.contatos = new ContatoPageCollection();
			this.contatos.on('fetched', this.endFetch, this);
			this.contatos.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.contatos,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.contatos,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.contatos.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid contato');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoContatos.add(model)
			else
				this.projetoContatos.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.contatos.each(function(model) {
				if (that.projetoContatos.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "nome",
				editable : false,
				sortable : false,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "ddd",
				editable : false,
				sortable : false,
				label 	 : "DDD",
				cell 	 : "string",
			}, 
			{
				name : "fone",
				editable : false,
				sortable : false,
				label 	 : "Fone",
				cell 	 : "string",
			}, 
			{
				name : "setor",
				editable : false,
				sortable : false,
				label 	 : "Setor",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalContatos;
});
