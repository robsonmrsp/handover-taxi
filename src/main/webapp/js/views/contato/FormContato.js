/* generated: 16/10/2016 15:27:08 */
define(function(require) {
    // Start "Import´s" Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var roles = require('adapters/auth-adapter').roles;
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var TemplateFormContatos = require('text!views/contato/tpl/FormContatoTemplate.html');
    var ContatoModel = require('models/ContatoModel');
    var ContatoCollection = require('collections/ContatoCollection');
    var ModalEmpresa = require('views/modalComponents/EmpresaModal');
    var EmpresaModel = require('models/EmpresaModel');
    // End of "Import´s" definition
    // #####################################################################################################
    // ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
    // BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
    // #####################################################################################################
    var FormContatos = Marionette.LayoutView.extend({
        template: _.template(TemplateFormContatos),
        regions: {
            modalEmpresaRegion: '#empresaModal',
        },
        events: {
            'click 	.save': 'save',
            'click 	.go-back-link': 'goBack',
            'click 	.saveAndContinue': 'saveAndContinue',
            'click #searchEmpresaModal': 'showModalEmpresa',
            'change #inputFone': 'uniqueTelefone',
        },
        ui: {
            inputEmpresaNomeFantasia: '#inputEmpresaNomeFantasia',
            inputEmpresaCnpj: '#inputEmpresaCnpj',
            inputEmpresaContrato: '#inputEmpresaContrato',
            inputEmpresaRazaoSocial: '#inputEmpresaRazaoSocial',
            inputId: '#inputId',
            inputNome: '#inputNome',
            inputDdd: '#inputDdd',
            inputFone: '#inputFone',
            inputSetor: '#inputSetor',
            inputEmpresaId: '#inputEmpresaId',
            inputEmpresaNome: '#inputEmpresaNome',
            form: '#formContato',
            newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
        },
        initialize: function(options) {
            var that = this;
            this.empresa = options.empresa || new EmpresaModel(this.model.get('empresa'));
            console.log(this.empresa);
            this.on('show', function() {
                this.ui.inputFone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
                this.ui.form.validationEngine('attach', {
                    promptPosition: "topLeft",
                    isOverflown: false,
                    validationEventTrigger: "change"
                });
                this.carregaEmpresa(this.empresa);
                this.checkButtonAuthority();
            });
        },
        carregaEmpresa: function(empresa) {
            this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));
            this.ui.inputEmpresaCnpj.val(empresa.get('cnpj'));
            this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
            this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));
        },
        saveAndContinue: function() {
            this.save(true)
        },
        save: function(continua) {
            var that = this;
            var contato = that.getModel();
            if (this.isValid()) {
                contato.save({}, {
                    success: function(_model, _resp, _options) {
                        util.showSuccessMessage('Contato salvo com sucesso!');
                        that.clearForm();
                        if (continua != true) {
                            util.goPage('app/empresa/' + that.empresa.get('id') + '/contatos');
                        }
                    },
                    error: function(_model, _resp, _options) {
                        util.showErrorMessage('Problema ao salvar registro! ' + _resp.responseJSON.legalMessage, _resp.responseJSON.legalMessage);
                    }
                });
            }
            else {
                util.showMessage('error', 'Verifique campos em destaque!');
            }
        },
        goBack: function() {
            util.goPage('app/empresa/' + this.empresa.get('id') + '/contatos');
        },
        clearForm: function() {
            util.clear('inputId');
            util.clear('inputNome');
            util.clear('inputDdd');
            util.clear('inputFone');
            util.clear('inputSetor');
        },
        isValid: function() {
            return this.ui.form.validationEngine('validate', {
                promptPosition: "topLeft",
                isOverflown: false,
                validationEventTrigger: "change"
            });
        },
        getModel: function() {
            var that = this;
            var contato = that.model;
            contato.set({
                id: util.escapeById('inputId') || null,
                nome: util.escapeById('inputNome'),
                ddd: util.escapeById('inputDdd'),
                fone: util.escapeById('inputFone'),
                setor: util.escapeById('inputSetor'),
                empresa: that.empresa.toJSON(),
            });
            return contato;
        },
        uniqueTelefone: function() {
            var that = this;
            util.validateUnique({
                element: that.ui.inputFone,
                fieldName: 'fone',
                fieldDisplayName: 'Telefone',
                view: that,
                collection: ContatoCollection,
            })
        },
        // vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_CONTATO_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
				if(e.authority == 'ROLE_CONTATO_EDITAR' || e.authority == 'ROLE_ADMIN') {
					saveButton.show();
				}
			})
		},
    });
    return FormContatos;
});