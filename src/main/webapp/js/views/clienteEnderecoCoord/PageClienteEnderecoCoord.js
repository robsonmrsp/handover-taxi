/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ClienteEnderecoCoordModel = require('models/ClienteEnderecoCoordModel');
	var ClienteEnderecoCoordCollection = require('collections/ClienteEnderecoCoordCollection');
	var ClienteEnderecoCoordPageCollection = require('collections/ClienteEnderecoCoordPageCollection');
	var PageClienteEnderecoCoordTemplate = require('text!views/clienteEnderecoCoord/tpl/PageClienteEnderecoCoordTemplate.html');
	
	//Filter import
	var ModalClienteEndereco = require('views/modalComponents/ClienteEnderecoModal');
	
	// End of "Import´s" definition

	var PageClienteEnderecoCoord = Marionette.LayoutView.extend({
		template : _.template(PageClienteEnderecoCoordTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			modalClienteEnderecoRegion : '#clienteEnderecoModal',
		},
		
		events : {
			'click 	#reset' : 'resetClienteEnderecoCoord',			
			'click #searchClienteEnderecoModal' : 'showModalClienteEndereco',
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchClienteEnderecoCoord',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
			inputLatitude : '#inputLatitude',
			inputLongitude : '#inputLongitude',
			inputEnderecoFormatado : '#inputEnderecoFormatado',
			inputValido : '#inputValido',
		
			inputClienteEnderecoId : '#inputClienteEnderecoId',
			inputClienteEnderecoNome : '#inputClienteEnderecoNome',
			form : '#formClienteEnderecoCoordFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchClienteEnderecoCoord();
	    	}
		},

		initialize : function() {
			var that = this;

			this.clienteEnderecoCoords = new ClienteEnderecoCoordPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.clienteEnderecoCoords
			});

			this.counter = new Counter({
				collection : this.clienteEnderecoCoords,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.clienteEnderecoCoords,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.clienteEnderecoCoords.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid clienteEnderecoCoord');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
				}
			});
			this.modalClienteEndereco = new ModalClienteEndereco({
				onSelectModel : function(model) {
					that.onSelectClienteEndereco(model);
				},
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.modalClienteEnderecoRegion.show(this.modalClienteEndereco);		
				this.ui.inputLatitude.formatNumber(2);
				this.ui.inputLongitude.formatNumber(2);
		
			});
		},
		 
		searchClienteEnderecoCoord : function(){
			var that = this;

			this.clienteEnderecoCoords.filterQueryParams = {
	    		latitude : util.escapeById('inputLatitude'),
	    		longitude : util.escapeById('inputLongitude'),
	    		enderecoFormatado : util.escapeById('inputEnderecoFormatado'),
	    		valido : util.escapeById('inputValido'),
			    clienteEndereco : util.escapeById('inputClienteEnderecoId'), 
			}
			this.clienteEnderecoCoords.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid clienteEnderecoCoord');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetClienteEnderecoCoord : function(){
			this.ui.form.get(0).reset();
			this.clienteEnderecoCoords.reset();
			util.clear('inputClienteEnderecoId');
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "latitude",
				editable : false,
				sortable : true,
				label 	 : "Latitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "longitude",
				editable : false,
				sortable : true,
				label 	 : "Longitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "enderecoFormatado",
				editable : false,
				sortable : true,
				label 	 : "Endereco formatado",
				cell 	 : "string",
			}, 
			{
				name : "valido",
				editable : false,
				sortable : true,
				label 	 : "Válido",
				cell 	 : "string",
			}, 
			{
				name : "clienteEndereco.nome",
				editable : false,
				sortable : true,  
				label : "Cliente endereco",
				cell : CustomStringCell.extend({
					fieldName : 'clienteEndereco.nome',
				}),
			},	
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Cliente endereco coord',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Cliente endereco coord',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new ClienteEnderecoCoordModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.clienteEnderecoCoords.remove(model);
							util.showSuccessMessage('Cliente endereco coord removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editClienteEnderecoCoord/" + model.get('id'));
		},

		showModalClienteEndereco : function() {
			this.modalClienteEndereco.showPage();
		},
			
		onSelectClienteEndereco : function(clienteEndereco) {
			this.modalClienteEndereco.hidePage();	
			this.ui.inputClienteEnderecoId.val(clienteEndereco.get('id'));
			this.ui.inputClienteEnderecoNome.val(clienteEndereco.get('nome'));		
		},
		

	});

	return PageClienteEnderecoCoord;
});
