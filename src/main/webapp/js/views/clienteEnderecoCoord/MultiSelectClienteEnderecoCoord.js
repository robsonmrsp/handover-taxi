/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectClienteEnderecoCoord = require('views/clienteEnderecoCoord/ModalMultiSelectClienteEnderecoCoord');
	var MultiSelectClienteEnderecoCoordTemplate = require('text!views/clienteEnderecoCoord/tpl/MultiSelectClienteEnderecoCoordTemplate.html');

	var MultiSelectClienteEnderecoCoord = Marionette.LayoutView.extend({
		template : _.template(MultiSelectClienteEnderecoCoordTemplate),

		regions : {
			modalMultiSelectClienteEnderecoCoordRegion : '#modalMultiSelectClienteEnderecoCoords',
			gridClienteEnderecoCoordsModalRegion : '#gridMultiselectClienteEnderecoCoords',
		},

		initialize : function() {
			var that = this;

			this.clienteEnderecoCoords = this.collection;

			this.gridClienteEnderecoCoords = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.clienteEnderecoCoords,
			});

			this.modalMultiSelectClienteEnderecoCoord = new ModalMultiSelectClienteEnderecoCoord({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectClienteEnderecoCoordRegion.show(that.modalMultiSelectClienteEnderecoCoord);
				that.gridClienteEnderecoCoordsModalRegion.show(that.gridClienteEnderecoCoords);
			});
		},
		clear : function(){
			this.modalMultiSelectClienteEnderecoCoord.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "latitude",
				editable : false,
				sortable : false,
				label 	 : "Latitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "longitude",
				editable : false,
				sortable : false,
				label 	 : "Longitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "enderecoFormatado",
				editable : false,
				sortable : false,
				label 	 : "Endereco formatado",
				cell 	 : "string",
			}, 
			{
				name : "valido",
				editable : false,
				sortable : false,
				label 	 : "Válido",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectClienteEnderecoCoord
});
