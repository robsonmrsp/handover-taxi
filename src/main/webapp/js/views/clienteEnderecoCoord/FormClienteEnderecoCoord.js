/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormClienteEnderecoCoords = require('text!views/clienteEnderecoCoord/tpl/FormClienteEnderecoCoordTemplate.html');
	var ClienteEnderecoCoordModel = require('models/ClienteEnderecoCoordModel');
	var ClienteEnderecoCoordCollection = require('collections/ClienteEnderecoCoordCollection');
	var ModalClienteEndereco = require('views/modalComponents/ClienteEnderecoModal');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormClienteEnderecoCoords = Marionette.LayoutView.extend({
		template : _.template(TemplateFormClienteEnderecoCoords),

		regions : {
			modalClienteEnderecoRegion : '#clienteEnderecoModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click #searchClienteEnderecoModal' : 'showModalClienteEndereco',
		},
		
		ui : {
			inputId : '#inputId',
			inputLatitude : '#inputLatitude',
			inputLongitude : '#inputLongitude',
			inputEnderecoFormatado : '#inputEnderecoFormatado',
			inputValido : '#inputValido',
		
			inputClienteEnderecoId : '#inputClienteEnderecoId',
			inputClienteEnderecoNome : '#inputClienteEnderecoNome',
			form : '#formClienteEnderecoCoord',
		},

		initialize : function() {
			var that = this;
			this.modalClienteEndereco = new ModalClienteEndereco({
				onSelectModel : function(model) {
					that.onSelectClienteEndereco(model);
				},
			});
			this.on('show', function() {
				this.modalClienteEnderecoRegion.show(this.modalClienteEndereco);		
		
				this.ui.inputLatitude.formatNumber(2);
				this.ui.inputLongitude.formatNumber(2);
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var clienteEnderecoCoord = that.getModel();

			if (this.isValid()) {
				clienteEnderecoCoord.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Cliente endereco coord salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/clienteEnderecoCoords');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputLatitude'); 
			util.clear('inputLongitude'); 
			util.clear('inputEnderecoFormatado'); 
			util.clear('inputValido'); 
			util.clear('inputClienteEnderecoId');
			util.clear('inputClienteEnderecoNome');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var clienteEnderecoCoord = that.model; 
			clienteEnderecoCoord.set({
				id: util.escapeById('inputId') || null,
		    	latitude : util.escapeById('inputLatitude', true), 
		    	longitude : util.escapeById('inputLongitude', true), 
		    	enderecoFormatado : util.escapeById('inputEnderecoFormatado'), 
		    	valido : util.escapeById('inputValido'), 
				clienteEndereco : that.modalClienteEndereco.getJsonValue(),
			});
			return clienteEnderecoCoord;
		},
		 		
		showModalClienteEndereco : function() {
			// add more before the modal is open
			this.modalClienteEndereco.showPage();
		},

		onSelectClienteEndereco : function(clienteEndereco) {
			this.modalClienteEndereco.hidePage();	
			this.ui.inputClienteEnderecoId.val(clienteEndereco.get('id'));
			this.ui.inputClienteEnderecoNome.val(clienteEndereco.get('nome'));		
		},
				
		
	});

	return FormClienteEnderecoCoords;
});