/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');
	var CustomNumberCell = require('views/components/CustomNumberCell');
	var TarifaAdicionalModel = require('models/TarifaAdicionalModel');
	var TarifaAdicionalCollection = require('collections/TarifaAdicionalCollection');
	var TarifaAdicionalPageCollection = require('collections/TarifaAdicionalPageCollection');
	var PageTarifaAdicionalTemplate = require('text!views/tarifaAdicional/tpl/PageTarifaAdicionalTemplate.html');
	// End of "Import´s" definition
	var PageTarifaAdicional = Marionette.LayoutView.extend({
		template: _.template(PageTarifaAdicionalTemplate),
		regions: {
			gridRegion: '#grid',
			counterRegion: '#counter',
			paginatorRegion: '#paginator',
		},
		events: {
			'click 	#reset': 'resetTarifaAdicional',
			'keypress': 'treatKeypress',
			'click 	.search-button': 'searchTarifaAdicional',
			'click .show-advanced-search-button': 'toggleAdvancedForm',
		},
		ui: {
			inputDescricao: '#inputDescricao',
			inputValor: '#inputValor',
			form: '#formTarifaAdicionalFilter',
			advancedSearchForm: '.advanced-search-form',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		toggleAdvancedForm: function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},
		treatKeypress: function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchTarifaAdicional();
			}
		},
		initialize: function() {
			var that = this;
			this.tarifaAdicionals = new TarifaAdicionalPageCollection();
			this.grid = new Backgrid.Grid({
				className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns: this.getColumns(),
				emptyText: "Sem registros",
				collection: this.tarifaAdicionals
			});
			this.counter = new Counter({
				collection: this.tarifaAdicionals,
			});
			this.paginator = new Backgrid.Extension.Paginator({
				columns: this.getColumns(),
				collection: this.tarifaAdicionals,
				className: ' paging_simple_numbers',
				uiClassName: 'pagination',
			});
			this.on('show', function() {
				this.ui.inputValor.mask('###.###.###.###,##', {
					reverse: true
				});
				that.gridRegion.show(that.grid);
				this.searchTarifaAdicional();
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.ui.inputValor.formatNumber(2);
				this.checkButtonAuthority();
			});
		},
		searchTarifaAdicional: function() {
			var that = this;
			if (!util.escapeById('inputDescricao')) {
				this.tarifaAdicionals.filterQueryParams = {
					descricao: util.escapeById('inputAvancadoDescricao'),
					valor: util.escapeById('inputValor', true),
				}
			}
			else {
				this.tarifaAdicionals.filterQueryParams = {
					descricao: util.escapeById('inputDescricao'),
				}
			}
			this.tarifaAdicionals.fetch({
				success: function(_coll, _resp, _opt) {
					console.info('Consulta para o grid tarifaAdicional');
				},
				error: function(_coll, _resp, _opt) {
					if(_resp.status == '403'){
						util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
					}
				},
				complete: function() {},
			})
		},
		resetTarifaAdicional: function() {
			this.ui.form.get(0).reset();
			this.tarifaAdicionals.reset();
		},
		getColumns: function() {
			var that = this;
			var columns = [{
				name: "descricao",
				editable: false,
				sortable: true,
				label: "Descrição",
				cell: "string",
			}, {
				name : "valor",
				editable : false,
				sortable : true,
				label : "Valor",
				cell : 'number',
				formatter : _.extend({}, Backgrid.CellFormatter.prototype, {
					fromRaw : function(rawValue, model) {
						return 'R$ ' + util.printFormatNumber(rawValue);
					}
				})
			}, {
				name: "acoes",
				label: "Ações(Editar, Deletar)",
				sortable: false,
				cell: GeneralActionsCell.extend({
					buttons: that.getCellButtons(),
					context: that,
				})
			}];
			return columns;
		},
		getCellButtons: function() {
			var that = this;
			var buttons = this.checkGridButtonAuthority();
			return buttons;
		},
		deleteModel: function(model) {
			var that = this;
			var modelTipo = new TarifaAdicionalModel({
				id: model.id,
			});
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success: function() {
							that.tarifaAdicionals.remove(model);
							util.showSuccessMessage('Tarifa adicional removido com sucesso!');
						},
						error: function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro', _resp);
						}
					});
				}
			});
		},
		editModel: function(model) {
			util.goPage("app/editTarifaAdicional/" + model.get('id'));
		},
		// vitoriano : chunck : check grid buttons authority
		checkGridButtonAuthority: function() {
			var that = this;
			var buttons = [];
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_TARIFAADICIONAL_EDITAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'edita_ficha_button',
						type: 'primary',
						icon: 'icon-pencil fa-pencil',
						hint: 'Editar Tarifa adicional',
						onClick: that.editModel,
					});
				}
				if (e.authority == 'ROLE_TARIFAADICIONAL_DELETAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'delete_button',
						type: 'danger',
						icon: 'icon-trash fa-trash',
						hint: 'Remover Tarifa adicional',
						onClick: that.deleteModel,
					});
				}
			})
			return buttons;
		},
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_TARIFAADICIONAL_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
	});
	return PageTarifaAdicional;
});