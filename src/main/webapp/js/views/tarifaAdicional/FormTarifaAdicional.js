/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var TemplateFormTarifaAdicionals = require('text!views/tarifaAdicional/tpl/FormTarifaAdicionalTemplate.html');
	var TarifaAdicionalModel = require('models/TarifaAdicionalModel');
	var TarifaAdicionalCollection = require('collections/TarifaAdicionalCollection');
	// End of "Import´s" definition
	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################
	var FormTarifaAdicionals = Marionette.LayoutView.extend({
		template: _.template(TemplateFormTarifaAdicionals),
		regions: {},
		events: {
			'click 	.save': 'save',
			'click 	.saveAndContinue': 'saveAndContinue',
		},
		ui: {
			inputId: '#inputId',
			inputDescricao: '#inputDescricao',
			inputValor: '#inputValor',
			form: '#formTarifaAdicional',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		initialize: function() {
			var that = this;
			this.on('show', function() {
				this.ui.inputValor.formatNumber(2);
				this.ui.inputValor.mask('###.###.###.###,##', {
					reverse: true
				});
				// this.ui.inputValor.val(util.printFormatNumber(this.model.get('valor')));
				this.ui.form.validationEngine('attach', {
					promptPosition: "topLeft",
					isOverflown: false,
					validationEventTrigger: "change"
				});
				this.checkButtonAuthority();
			});
		},
		saveAndContinue: function() {
			this.save(true)
		},
		save: function(continua) {
			var that = this;
			var tarifaAdicional = that.getModel();
			if (this.isValid()) {
				tarifaAdicional.save({}, {
					success: function(_model, _resp, _options) {
						util.showSuccessMessage('Tarifa adicional salvo com sucesso!');
						that.clearForm();
						if (continua != true) {
							util.goPage('app/tarifaAdicionals');
						}
					},
					error: function(_model, _resp, _options) {
						if(_resp.status == '403'){
							util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
						} else {
							util.showErrorMessage('Problema ao salvar registro', _resp);
						}
					}
				});
			}
			else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},
		clearForm: function() {
			util.clear('inputId');
			util.clear('inputDescricao');
			util.clear('inputValor');
		},
		isValid: function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition: "topLeft",
				isOverflown: false,
				validationEventTrigger: "change"
			});
		},
		getModel: function() {
			var that = this;
			var tarifaAdicional = that.model;
			tarifaAdicional.set({
				id: util.escapeById('inputId') || null,
				descricao: util.escapeById('inputDescricao'),
				valor: util.escapeById('inputValor', true),
			});
			return tarifaAdicional;
		},
		// vitoriano : chunk : checks html button authority
		checkButtonAuthority: function() {
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_TARIFAADICIONAL_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
				if(e.authority == 'ROLE_TARIFAADICIONAL_EDITAR' || e.authority == 'ROLE_ADMIN') {
					saveButton.show();
				}
			})
		},
		// vitoriano : chunk
	});
	return FormTarifaAdicionals;
});