/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var TarifaAdicionalPageCollection = require('collections/TarifaAdicionalPageCollection');
	var ModalMultiSelectTarifaAdicionalTemplate = require('text!views/tarifaAdicional/tpl/ModalMultiSelectTarifaAdicionalTemplate.html');
	// End of "Import´s" definition

	var ModalTarifaAdicionals = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectTarifaAdicionalTemplate),

		regions : {
			gridRegion : '#grid-tarifaAdicionals-modal',
			paginatorRegion : '#paginator-tarifaAdicionals-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoTarifaAdicionals = this.collection;
			
			this.tarifaAdicionals = new TarifaAdicionalPageCollection();
			this.tarifaAdicionals.on('fetched', this.endFetch, this);
			this.tarifaAdicionals.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.tarifaAdicionals,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.tarifaAdicionals,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.tarifaAdicionals.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid tarifaAdicional');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoTarifaAdicionals.add(model)
			else
				this.projetoTarifaAdicionals.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.tarifaAdicionals.each(function(model) {
				if (that.projetoTarifaAdicionals.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descrição",
				cell 	 : "string",
			}, 
			{
				name : "valor",
				editable : false,
				sortable : false,
				label 	 : "Valor",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return ModalTarifaAdicionals;
});
