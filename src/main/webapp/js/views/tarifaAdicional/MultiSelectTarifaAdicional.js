/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectTarifaAdicional = require('views/tarifaAdicional/ModalMultiSelectTarifaAdicional');
	var MultiSelectTarifaAdicionalTemplate = require('text!views/tarifaAdicional/tpl/MultiSelectTarifaAdicionalTemplate.html');

	var MultiSelectTarifaAdicional = Marionette.LayoutView.extend({
		template : _.template(MultiSelectTarifaAdicionalTemplate),

		regions : {
			modalMultiSelectTarifaAdicionalRegion : '#modalMultiSelectTarifaAdicionals',
			gridTarifaAdicionalsModalRegion : '#gridMultiselectTarifaAdicionals',
		},

		initialize : function() {
			var that = this;

			this.tarifaAdicionals = this.collection;

			this.gridTarifaAdicionals = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.tarifaAdicionals,
			});

			this.modalMultiSelectTarifaAdicional = new ModalMultiSelectTarifaAdicional({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectTarifaAdicionalRegion.show(that.modalMultiSelectTarifaAdicional);
				that.gridTarifaAdicionalsModalRegion.show(that.gridTarifaAdicionals);
			});
		},
		clear : function(){
			this.modalMultiSelectTarifaAdicional.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descrição",
				cell 	 : "string",
			}, 
			{
				name : "valor",
				editable : false,
				sortable : false,
				label 	 : "Valor",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return MultiSelectTarifaAdicional
});
