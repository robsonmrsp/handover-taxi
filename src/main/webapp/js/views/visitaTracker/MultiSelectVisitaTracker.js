/* generated: 04/11/2016 11:17:04 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectVisitaTracker = require('views/visitaTracker/ModalMultiSelectVisitaTracker');
	var MultiSelectVisitaTrackerTemplate = require('text!views/visitaTracker/tpl/MultiSelectVisitaTrackerTemplate.html');

	var MultiSelectVisitaTracker = Marionette.LayoutView.extend({
		template : _.template(MultiSelectVisitaTrackerTemplate),

		regions : {
			modalMultiSelectVisitaTrackerRegion : '#modalMultiSelectVisitaTrackers',
			gridVisitaTrackersModalRegion : '#gridMultiselectVisitaTrackers',
		},

		initialize : function() {
			var that = this;

			this.visitaTrackers = this.collection;

			this.gridVisitaTrackers = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.visitaTrackers,
			});

			this.modalMultiSelectVisitaTracker = new ModalMultiSelectVisitaTracker({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectVisitaTrackerRegion.show(that.modalMultiSelectVisitaTracker);
				that.gridVisitaTrackersModalRegion.show(that.gridVisitaTrackers);
			});
		},
		clear : function(){
			this.modalMultiSelectVisitaTracker.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "latitude",
				editable : false,
				sortable : false,
				label 	 : "Latitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "longitude",
				editable : false,
				sortable : false,
				label 	 : "Longitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "timestamp",
				editable : false,
				sortable : false,
				label 	 : "Timestamp",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "speed",
				editable : false,
				sortable : false,
				label 	 : "Speed",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "accuracy",
				editable : false,
				sortable : false,
				label 	 : "Accuracy",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "direction",
				editable : false,
				sortable : false,
				label 	 : "Direction",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "altitude",
				editable : false,
				sortable : false,
				label 	 : "Altitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "baterryLevel",
				editable : false,
				sortable : false,
				label 	 : "Baterry level",
				cell 	 : "string",
			}, 
			{
				name : "gpsEnabled",
				editable : false,
				sortable : false,
				label 	 : "Gps enabled",
				cell 	 : "string",
			}, 
			{
				name : "wifiEnabled",
				editable : false,
				sortable : false,
				label 	 : "Wifi enabled",
				cell 	 : "string",
			}, 
			{
				name : "mobileEnabled",
				editable : false,
				sortable : false,
				label 	 : "Mobile enabled",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectVisitaTracker
});
