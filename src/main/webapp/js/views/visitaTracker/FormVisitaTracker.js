/* generated: 04/11/2016 11:17:04 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormVisitaTrackers = require('text!views/visitaTracker/tpl/FormVisitaTrackerTemplate.html');
	var VisitaTrackerModel = require('models/VisitaTrackerModel');
	var VisitaTrackerCollection = require('collections/VisitaTrackerCollection');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormVisitaTrackers = Marionette.LayoutView.extend({
		template : _.template(TemplateFormVisitaTrackers),

		regions : {
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
		},
		
		ui : {
			inputId : '#inputId',
			inputLatitude : '#inputLatitude',
			inputLongitude : '#inputLongitude',
			inputTimestamp : '#inputTimestamp',
			inputSpeed : '#inputSpeed',
			inputAccuracy : '#inputAccuracy',
			inputDirection : '#inputDirection',
			inputAltitude : '#inputAltitude',
			inputBaterryLevel : '#inputBaterryLevel',
			inputGpsEnabled : '#inputGpsEnabled',
			inputWifiEnabled : '#inputWifiEnabled',
			inputMobileEnabled : '#inputMobileEnabled',
		
			form : '#formVisitaTracker',
		},

		initialize : function() {
			var that = this;
			this.on('show', function() {
		
				this.ui.inputLatitude.formatNumber(2);
				this.ui.inputLongitude.formatNumber(2);
				this.ui.inputTimestamp.formatNumber(2);
				this.ui.inputSpeed.formatNumber(2);
				this.ui.inputAccuracy.formatNumber(2);
				this.ui.inputDirection.formatNumber(2);
				this.ui.inputAltitude.formatNumber(2);
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var visitaTracker = that.getModel();

			if (this.isValid()) {
				visitaTracker.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Visita tracker salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/visitaTrackers');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputLatitude'); 
			util.clear('inputLongitude'); 
			util.clear('inputTimestamp'); 
			util.clear('inputSpeed'); 
			util.clear('inputAccuracy'); 
			util.clear('inputDirection'); 
			util.clear('inputAltitude'); 
			util.clear('inputBaterryLevel'); 
			util.clear('inputGpsEnabled'); 
			util.clear('inputWifiEnabled'); 
			util.clear('inputMobileEnabled'); 
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var visitaTracker = that.model; 
			visitaTracker.set({
				id: util.escapeById('inputId') || null,
		    	latitude : util.escapeById('inputLatitude', true), 
		    	longitude : util.escapeById('inputLongitude', true), 
		    	timestamp : util.escapeById('inputTimestamp', true), 
		    	speed : util.escapeById('inputSpeed', true), 
		    	accuracy : util.escapeById('inputAccuracy', true), 
		    	direction : util.escapeById('inputDirection', true), 
		    	altitude : util.escapeById('inputAltitude', true), 
		    	baterryLevel : util.escapeById('inputBaterryLevel'), 
		    	gpsEnabled : util.escapeById('inputGpsEnabled'), 
		    	wifiEnabled : util.escapeById('inputWifiEnabled'), 
		    	mobileEnabled : util.escapeById('inputMobileEnabled'), 
			});
			return visitaTracker;
		},
		 		

				
		
	});

	return FormVisitaTrackers;
});