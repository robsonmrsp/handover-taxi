/* generated: 04/11/2016 11:17:04 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var VisitaTrackerModel = require('models/VisitaTrackerModel');
	var VisitaTrackerCollection = require('collections/VisitaTrackerCollection');
	var VisitaTrackerPageCollection = require('collections/VisitaTrackerPageCollection');
	var PageVisitaTrackerTemplate = require('text!views/visitaTracker/tpl/PageVisitaTrackerTemplate.html');
	
	//Filter import
	
	// End of "Import´s" definition

	var PageVisitaTracker = Marionette.LayoutView.extend({
		template : _.template(PageVisitaTrackerTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
		},
		
		events : {
			'click 	#reset' : 'resetVisitaTracker',			
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchVisitaTracker',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
		
			form : '#formVisitaTrackerFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchVisitaTracker();
	    	}
		},

		initialize : function() {
			var that = this;

			this.visitaTrackers = new VisitaTrackerPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.visitaTrackers
			});

			this.counter = new Counter({
				collection : this.visitaTrackers,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.visitaTrackers,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.visitaTrackers.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid visitaTracker');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
				}
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
		
			});
		},
		 
		searchVisitaTracker : function(){
			var that = this;

			this.visitaTrackers.filterQueryParams = {
			}
			this.visitaTrackers.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid visitaTracker');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetVisitaTracker : function(){
			this.ui.form.get(0).reset();
			this.visitaTrackers.reset();
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Visita tracker',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Visita tracker',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new VisitaTrackerModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.visitaTrackers.remove(model);
							util.showSuccessMessage('Visita tracker removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editVisitaTracker/" + model.get('id'));
		},

		

	});

	return PageVisitaTracker;
});
