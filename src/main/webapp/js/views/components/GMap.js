/* generated: 22/09/2014 11:14:50 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');

	// options.drawing.iconMarker

	var GMap = Marionette.ItemView.extend({
		initialize : function(options) {
			this.directionsService = new google.maps.DirectionsService;

			this.directionsDisplay = new google.maps.DirectionsRenderer;

			this.showDrawTools = options.showDrawTools;

			if (!options.mapElement) {
				throw new TypeError("Voce deve indicar qual o container do map.");
			}

			this.criaMapa(options)
		},

		criaMapa : function(options) {
			this.overlays = [];
			this.markers = [];

			this.markersMap = new Col.Map();

			var that = this;

			this.googlemap = new google.maps.Map(options.mapElement[0], {
				center : (options.mapOptions && options.mapOptions.center) || new google.maps.LatLng(-3.806443, -38.455302),
				zoom : (options.mapOptions && options.mapOptions.zoom) || 11,
				mapTypeId : (options.mapOptions && options.mapOptions.mapTypeId) || google.maps.MapTypeId.ROADMAP,
			});

			if (this.showDrawTools) {
				this.drawingManager = new google.maps.drawing.DrawingManager({
					drawingControl : true,
					drawingControlOptions : {

						style : google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
						position : google.maps.ControlPosition.TOP_CENTER,
						drawingModes : [ google.maps.drawing.OverlayType.POLYGON ]
					},
					polygonOptions : {
						fillColor : '#ddff00',
						fillOpacity : 0.5,
						strokeWeight : 3,
						clickable : true,
						editable : true,
						zIndex : 1
					},

				});

				google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function(event) {
					if (event.type == google.maps.drawing.OverlayType.CIRCLE) {
						var radius = event.overlay.getRadius();
					}
					google.maps.event.addListener(event.overlay, 'rightclick', that.mostraAlgumaCoisa);
					that.overlays.push(event.overlay);
				});
				this.drawingManager.setMap(this.googlemap);
			}
		},

		desenhaRota : function(arrayLatlongs) {
			var that = this;
			var dirOpts = {}
			var len = arrayLatlongs.length;
			if (len < 2) {
				that.directionsDisplay.setMap(null);
				return false;
			}
			if (len == 2) {
				dirOpts = {
					origin : arrayLatlongs[0],
					destination : arrayLatlongs[len - 1],
					travelMode : google.maps.TravelMode.DRIVING
				}
			}
			if (len > 2) {
				var waypoints = []
				_.each(arrayLatlongs.slice(1, len - 1), function(ponto) {
					waypoints.push({
						location : ponto,
						stopover : true
					})
				});

				dirOpts = {
					origin : arrayLatlongs[0],
					destination : arrayLatlongs[len - 1],
					waypoints : waypoints,
					optimizeWaypoints : true,
					travelMode : google.maps.TravelMode.DRIVING
				}
			}

			this.directionsService.route(dirOpts, function(response, status) {
				if (status === google.maps.DirectionsStatus.OK) {
					that.directionsDisplay.setMap(that.googlemap);
					that.directionsDisplay.setDirections(response);
					that.googlemap.setZoom(11);

				} else {
					console.warn('Directions request failed due to ' + status);
				}
			});

		},
		mostraAlgumaCoisa : function(evt) {
			console.log(evt)
		},

		getOverlays : function() {
			return this.overlays;
		},

		addMarker : function(jsonLatLng, identify) {
			var point = new google.maps.LatLng(jsonLatLng.lat, jsonLatLng.lng);
			var marker = new google.maps.Marker({
				position : point,
				map : this.googlemap,
				title : identify
			});

			this.markers.push(marker);
			this.markersMap.put(identify, marker);
			this.googlemap.setCenter(point);
		},

		addMotos : function(collection) {
			var that = this;
			_.each(collection, function(element) {
				var point = new google.maps.LatLng(element.latitude, element.longitude);
				var marker = new google.maps.Marker({
					position : point,
					map : that.googlemap,
					title : element.viatura,
					icon : 'images/moto-marker.png'
				});
				that.markers.push(marker);
			})
		},

		clearMarkers : function() {
			var markers = this.markersMap.values()
			_.each(markers, function(marker) {
				marker.setMap(null);
			})
			
		},
		removeMarker : function(jsonLatLng, identify) {
			var marker = this.markersMap.get(identify)
			marker.setMap(null);
		},

		addPolygon : function(array) {
			var polygon = new google.maps.Polygon({
				paths : array,
				fillColor : '#ddff00',
				fillOpacity : 0.5,
				strokeWeight : 3,
				clickable : true,
				editable : true,
				zIndex : 1
			});
			polygon.setMap(this.googlemap);

			this.overlays.push(polygon);
		},

		clear : function() {
			_.each(this.markers, function(marker) {
				marker.setMap(null)
			})
			this.directionsDisplay.setMap(null)
			this.markers = [];
		}
	});

	return GMap;
})
