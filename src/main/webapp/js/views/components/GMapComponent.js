/* generated: 22/09/2014 11:14:50 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');

	// options.drawing.iconMarker

	var GMap = Marionette.ItemView.extend({
		initialize : function(options) {

			this.showDrawTools = options.showDrawTools;

			if (!options.mapElement) {
				throw new TypeError("Voce deve indicar qual o container do map.");
			}

		},

	});

	return GMap;
})

/**
 * Context Menu for google maps
 */
(function(window, undefined) {

	// Use the correct document accordingly with window argument (sandbox)
	var document = window.document,

	// shorthand some stuff
	$ = jQuery, g = google.maps;

	/**
	 * Create the context menu
	 * 
	 * @param array
	 *            opts Array of options.
	 * @todo Menu id needs to be unique, in case of multiple maps and context
	 *       menus
	 */
	function contextMenu(opts) {
		// A way to access 'this' object from inside functions
		var self = this;

		if (opts.map !== undefined) {
			this.theMap = opts.map;
			this.clickedLatLng = null;
			this.theMenu = $(document.createElement('div')).attr('class', 'contextMenu').bind('contextmenu', function() {
				return false;
			})

			.append($(document.createElement('ul'))).appendTo(this.theMap.getDiv());
			g.event.addListener(this.theMap, 'rightclick', function(e) {
				var mapDiv = $(self.theMap.getDiv()), menu = self.theMenu, x = e.pixel.x, y = e.pixel.y;
				menu.hide();
				self.clickedLatLng = e.latLng;
				if (x > mapDiv.width() - menu.width())
					x -= menu.width();

				if (y > mapDiv.height() - menu.height())
					y -= menu.height();
				menu.css({
					top : y,
					left : x
				}).fadeIn(200);
			});

			// Hide context menu on several events
			$.each('click dragstart zoom_changed maptypeid_changed center_changed'.split(' '), function(i, name) {
				g.event.addListener(self.theMap, name, function() {
					self.theMenu.hide();
				});
			});
		}
	}

	/**
	 * Add new items to the context menu
	 * 
	 * @param string
	 *            name Name of the list item.
	 * @param function
	 *            callback Function to run when you click the list item
	 * @return jQuery The list item that is created.
	 */
	contextMenu.prototype.addItem = function(name, loc, callback) {
		// If no loc was provided
		if (typeof loc === 'function') {
			callback = loc;
			loc = undefined;
		}

		// A way to access 'this' object from inside functions
		var self = this,

		// The name turned into camelCase for use in the li id, and anchor href
		idName = name.toCamel(),

		// The li element
		li = $(document.createElement('li')).attr('id', idName);

		// the anchor element
		$(document.createElement('a')).attr('href', '#' + idName).html(name).appendTo(li)

		// Add some nice hover effects
		.hover(function() {
			$(this).parent().toggleClass('hover');
		})

		// Set the click event
		.click(function() {

			// fade out the menu
			self.theMenu.hide();

			// call the callback function - 'this' would refer back to the
			// jQuery object of the item element
			callback.call(this, self.theMap, self.clickedLatLng);

			// make sure the click doesnt take us anywhere
			return false;
		});

		// If `loc` is a number put it at that location
		if (typeof loc === 'number' && loc < this.theMenu.find('li').length)
			this.theMenu.find('li').eq(loc).before(li);

		// .. else appened it to the end of the menu
		else
			this.theMenu.find('ul').append(li);

		// Return the whole list item
		return li;
	};

	/**
	 * Add a seperators
	 * 
	 * @return jQuery The list item that is created.
	 */
	contextMenu.prototype.addSep = function(loc) {
		// Create the li element
		var li = $(document.createElement('li')).addClass('separator')

		// .. add a div child
		.append($(document.createElement('div')))

		// If loc is a number put the li at that location
		if (typeof loc === 'number')
			this.theMenu.find('li').eq(loc).before(li)

			// .. else appened it to the end
		else
			this.theMenu.find('ul').append(li);

		// Return the li element
		return li
	};

	/**
	 * Remove a menu list item.
	 * 
	 * @param string
	 *            name The string used to create the list item.
	 * @param number
	 *            name The index value of the list item.
	 * @param jQuery
	 *            name The jQuery object that is returned by addItem() or
	 *            addSep()
	 */
	contextMenu.prototype.remove = function(item) {
		// No need to search for name if its a jquery object
		if (item instanceof $)
			item.remove();

		// Find all the elements and remove the one at the specified index
		else if (typeof item === 'number')
			this.theMenu.find('li').eq(item).remove()

			// Find all the items by the id name and remove them
		else if (typeof item === 'string') {
			// Find and remove the element
			this.theMenu.find('#' + item.toCamel()).remove()
		}
	};

	// Expose this to the global object
	window.contextMenu = contextMenu;
	/**
	 * Convert a string into a 'camelCase' string
	 * 
	 * @example 'Camel case string'.toCamel() -> 'camelCaseString'
	 */
	String.prototype.toCamel = function() {
		return this.toLowerCase().replace(/(\s)([a-z])/gi, function(match, group1, group2) {
			return group2.toUpperCase().replace(group1, '');
		});
	}
})(window);
