define(function(require) {
    // Start "ImportÂ´s" Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var util = require('utilities/utils');
    var BaseCollection = require('collections/BaseCollection');
    var estados = {
        'Acre': 'AC',
        'Alagoas': 'AL',
        'Amapá': 'AP',
        'Amazonas': 'AM',
        'Bahia ': 'BA',
        'Ceará': 'CE',
        'Distrito Federal ': 'DF',
        'Espírito Santo': 'ES',
        'Goiás': 'GO',
        'Maranhão': 'MA',
        'Mato Grosso': 'MT',
        'Mato Grosso do Sul': 'MS',
        'Minas Gerais': 'MG',
        'Pará': 'PA',
        'Paraíba': 'PB',
        'Paraná': 'PR',
        'Pernambuco': 'PE',
        'Piauí': 'PI',
        'Rio de Janeiro': 'RJ',
        'Rio Grande do Norte': 'RN',
        'Rio Grande do Sul': 'RS',
        'Rondônia': 'RO',
        'Roraima': 'RR',
        'Santa Catarina': 'SC',
        'São Paulo': 'SP',
        'Sergipe': 'SE',
        'Tocantins': 'TO',
    }
    var Suggestbox = Backbone.View.extend({
        getValue: function() {
            this.fieldVal;
        },
        events: {
            'click #botao': 'selectBotao'
        },
        getTexto: function() {
            if (this.fieldVal) {
                return this.fieldVal.get('endereco');
            }
            return '';
        },
        getJsonValue: function() {
            if (this.fieldVal) {
                return this.fieldVal.toJSON();
            }
            return null;
        },
        clear: function() {
            this.$el.val('');
        },
        setValue: function(endereco) {},
        setParam: function(jsonParam) {
            this.param = jsonParam
        },
        initialize: function(options) {
            var that = this;
            this.fieldVal = null;
            this.onSelect = options.onSelect;
            var input = /** @type {!HTMLInputElement} */ (that.$el.get(0));
            this.autocomplete = new google.maps.places.Autocomplete(input);
            this.autocomplete.addListener('place_changed', function() {
                var place = that.autocomplete.getPlace();
                if (place) {
                    var jsonEndereco = util.addressComponentsToJson(place.address_components, place.formatted_address);
                    var endereco = {
                        id: place.id,
                        latlong: place.geometry.location.toJSON(),
                        endereco: place.formatted_address,
                        enderecoFormatado: place.formatted_address,
                        latitude: place.geometry.location.lat(),
                        longitude: place.geometry.location.lng(),
                        logradouro: jsonEndereco.endereco,
                        numero: jsonEndereco.numero,
                        complemento: '',
                        uf: estados[jsonEndereco.uf],
                        cidade: jsonEndereco.cidade,
                        bairro: jsonEndereco.bairro,
                        cep: jsonEndereco.cep,
                    }
                    console.log(endereco)
                    that.fieldVal = endereco;
                    if (that.onSelect) {
                        that.onSelect(endereco);
                    }
                }
            });
        },
    });
    return Suggestbox;
});