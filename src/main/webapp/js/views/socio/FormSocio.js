/* generated: 16/10/2016 15:27:08 */
define(function(require) {
    // Start "Import´s" Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var roles = require('adapters/auth-adapter').roles;
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var TemplateFormSocios = require('text!views/socio/tpl/FormSocioTemplate.html');
    var SocioModel = require('models/SocioModel');
    var SocioCollection = require('collections/SocioCollection');
    // End of "Import´s" definition
    // #####################################################################################################
    // ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
    // BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
    // #####################################################################################################
    var FormSocios = Marionette.LayoutView.extend({
        template: _.template(TemplateFormSocios),
        regions: {
            modalEmpresaRegion: '#empresaModal',
        },
        events: {
            'click 	.save': 'save',
            'click 	.go-back-link': 'goBack',
            'click 	.saveAndContinue': 'saveAndContinue',
            'change #inputCpf': 'uniqueCpf',
        },
        ui: {
            inputEmpresaNomeFantasia: '#inputEmpresaNomeFantasia',
            inputEmpresaCnpj: '#inputEmpresaCnpj',
            inputEmpresaContrato: '#inputEmpresaContrato',
            inputEmpresaRazaoSocial: '#inputEmpresaRazaoSocial',
            inputId: '#inputId',
            inputNome: '#inputNome',
            inputCpf: '#inputCpf',
            inputEmpresaId: '#inputEmpresaId',
            inputEmpresaNome: '#inputEmpresaNome',
            form: '#formSocio',
            newButton: 'a#new',
            saveButton: 'a#save',
            saveContinueButton: 'a#saveContinue',
        },
        initialize: function(options) {
            this.empresa = options.empresa || new EmpresaModel(this.model.get('empresa'));
            var that = this;
            this.on('show', function() {
                this.ui.inputCpf.mask('999.999.999-99');
                this.ui.form.validationEngine('attach', {
                    promptPosition: "topLeft",
                    isOverflown: false,
                    validationEventTrigger: "change"
                });
                this.carregaEmpresa(this.empresa);
                this.checkButtonAuthority();
            });
        },
        carregaEmpresa: function(empresa) {
            this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));
            this.ui.inputEmpresaCnpj.val(empresa.get('cnpj'));
            this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
            this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));
        },
        saveAndContinue: function() {
            this.save(true)
        },
        save: function(continua) {
            var that = this;
            var socio = that.getModel();
            if (this.isValid()) {
                socio.save({}, {
                    success: function(_model, _resp, _options) {
                        util.showSuccessMessage('Sócio salvo com sucesso!');
                        that.clearForm();
                        if (continua != true) {
                            util.goPage('app/empresa/' + that.empresa.get('id') + '/socios');
                        }
                    },
                    error: function(_model, _resp, _options) {
                        util.showErrorMessage('Problema ao salvar registro', _resp);
                    }
                });
            }
            else {
                util.showMessage('error', 'Verifique campos em destaque!');
            }
        },
        goBack: function() {
            util.goPage('app/empresa/' + this.empresa.get('id') + '/socios');
        },
        clearForm: function() {
            util.clear('inputId');
            util.clear('inputNome');
            util.clear('inputCpf');
            util.clear('inputEmpresaId');
            util.clear('inputEmpresaNome');
        },
        isValid: function() {
            return this.ui.form.validationEngine('validate', {
                promptPosition: "topLeft",
                isOverflown: false,
                validationEventTrigger: "change"
            });
        },
        getModel: function() {
            var that = this;
            var socio = that.model;
            socio.set({
                id: util.escapeById('inputId') || null,
                nome: util.escapeById('inputNome'),
                cpf: util.escapeById('inputCpf'),
                empresa: that.empresa.toJSON(),
            });
            return socio;
        },
        //        uniqueCpf: function() {
        //            var that = this;
        //            util.validateUnique({
        //                element: that.ui.inputCpf,
        //                fieldName: 'cpf',
        //                fieldDisplayName: 'CPF',
        //                view: that,
        //                collection: SocioCollection,
        //            })
        //        },
        // vitoriano : chunk
        checkButtonAuthority: function() {
            var newButton = this.ui.newButton;
            var saveButton = this.ui.saveButton;
            var saveContinueButton = this.ui.saveContinueButton;
            $.grep(roles, function(e) {
                if (e.authority == 'ROLE_SOCIO_CRIAR' || e.authority == e.authority == 'ROLE_ADMIN') {
                    newButton.show();
                    saveButton.show();
                    saveContinueButton.show();
                }
                if (e.authority == 'ROLE_SOCIO_EDITAR' || e.authority == e.authority == 'ROLE_ADMIN') {
                    saveButton.show();
                }
            })
        },
    });
    return FormSocios;
});