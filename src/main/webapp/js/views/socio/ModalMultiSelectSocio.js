/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var SocioPageCollection = require('collections/SocioPageCollection');
	var ModalMultiSelectSocioTemplate = require('text!views/socio/tpl/ModalMultiSelectSocioTemplate.html');
	// End of "Import´s" definition

	var ModalSocios = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectSocioTemplate),

		regions : {
			gridRegion : '#grid-socios-modal',
			paginatorRegion : '#paginator-socios-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoSocios = this.collection;
			
			this.socios = new SocioPageCollection();
			this.socios.on('fetched', this.endFetch, this);
			this.socios.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.socios,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.socios,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.socios.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid socio');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoSocios.add(model)
			else
				this.projetoSocios.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.socios.each(function(model) {
				if (that.projetoSocios.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "nome",
				editable : false,
				sortable : false,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "cpf",
				editable : false,
				sortable : false,
				label 	 : "Cpf",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalSocios;
});
