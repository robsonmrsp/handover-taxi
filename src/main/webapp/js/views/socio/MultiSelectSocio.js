/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectSocio = require('views/socio/ModalMultiSelectSocio');
	var MultiSelectSocioTemplate = require('text!views/socio/tpl/MultiSelectSocioTemplate.html');

	var MultiSelectSocio = Marionette.LayoutView.extend({
		template : _.template(MultiSelectSocioTemplate),

		regions : {
			modalMultiSelectSocioRegion : '#modalMultiSelectSocios',
			gridSociosModalRegion : '#gridMultiselectSocios',
		},

		initialize : function() {
			var that = this;

			this.socios = this.collection;

			this.gridSocios = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.socios,
			});

			this.modalMultiSelectSocio = new ModalMultiSelectSocio({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectSocioRegion.show(that.modalMultiSelectSocio);
				that.gridSociosModalRegion.show(that.gridSocios);
			});
		},
		clear : function(){
			this.modalMultiSelectSocio.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "nome",
				editable : false,
				sortable : false,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "cpf",
				editable : false,
				sortable : false,
				label 	 : "Cpf",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectSocio
});
