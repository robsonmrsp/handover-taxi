/* generated: 16/10/2016 15:27:08 */
define(function(require) {
    // Start "Import´s Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var roles = require('adapters/auth-adapter').roles;
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var CustomStringCell = require('views/components/CustomStringCell');
    var Counter = require('views/components/Counter');
    var ActionsCell = require('views/components/ActionsCell');
    var GeneralActionsCell = require('views/components/GeneralActionsCell');
    var CustomNumberCell = require('views/components/CustomNumberCell');
    var SocioModel = require('models/SocioModel');
    var SocioCollection = require('collections/SocioCollection');
    var SocioPageCollection = require('collections/SocioPageCollection');
    var PageSocioTemplate = require('text!views/socio/tpl/PageSocioTemplate.html');
    var ModalEmpresa = require('views/modalComponents/EmpresaModal');
    // End of "Import´s" definition
    var PageSocio = Marionette.LayoutView.extend({
        template: _.template(PageSocioTemplate),
        regions: {
            gridRegion: '#grid',
            counterRegion: '#counter',
            paginatorRegion: '#paginator',
            modalEmpresaRegion: '#empresaModal',
        },
        events: {
            'click 	#reset': 'resetSocio',
            'click 	.novo-socio': 'novoSocio',
            'click 	.voltar-empresa': 'voltarEmpresa',
            'keypress': 'treatKeypress',
            'click 	.search-button': 'searchSocio',
            'click .show-advanced-search-button': 'toggleAdvancedForm',
        },
        ui: {
            inputEmpresaNomeFantasia: '#inputEmpresaNomeFantasia',
            inputEmpresaCnpj: '#inputEmpresaCnpj',
            inputEmpresaContrato: '#inputEmpresaContrato',
            inputEmpresaRazaoSocial: '#inputEmpresaRazaoSocial',
            inputNome: '#inputNome',
            inputCpf: '#inputCpf',
            inputEmpresaId: '#inputEmpresaId',
            inputEmpresaNome: '#inputEmpresaNome',
            form: '#formSocioFilter',
            advancedSearchForm: '.advanced-search-form',
            newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
        },
        toggleAdvancedForm: function() {
            this.ui.advancedSearchForm.slideToggle("slow");
        },
        treatKeypress: function(e) {
            if (util.enterPressed(e)) {
                e.preventDefault();
                this.searchSocio();
            }
        },
        initialize: function(options) {
            var that = this;
            this.empresa = options.empresa;
            this.socios = new SocioPageCollection();
            this.grid = new Backgrid.Grid({
                className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
                columns: this.getColumns(),
                emptyText: "Sem registros",
                collection: this.socios
            });
            this.counter = new Counter({
                collection: this.socios,
            });
            this.paginator = new Backgrid.Extension.Paginator({
                columns: this.getColumns(),
                collection: this.socios,
                className: ' paging_simple_numbers',
                uiClassName: 'pagination',
            });
            this.on('show', function() {
                this.searchSocio();
                that.ui.inputCpf.mask('999.999.999-99');
                that.gridRegion.show(that.grid);
                that.counterRegion.show(that.counter);
                that.paginatorRegion.show(that.paginator);
                this.carregaEmpresa(this.empresa);
                this.checkButtonAuthority();
            });
        },
        carregaEmpresa: function(empresa) {
            this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));
            this.ui.inputEmpresaCnpj.val(empresa.get('cnpj'));
            this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
            this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));
        },
        searchSocio: function() {
            var that = this;
            this.socios.filterQueryParams = {
                nome: util.escapeById('inputNome'),
                cpf: util.escapeById('inputCpf'),
                empresa: that.empresa.get('id'),
            }
            this.socios.fetch({
                success: function(_coll, _resp, _opt) {
                    console.info('Consulta para o grid socio');
                },
                error: function(_coll, _resp, _opt) {
                    console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
                },
                complete: function() {},
            })
        },
        resetSocio: function() {
            this.ui.form.get(0).reset();
            this.socios.reset();
        },
        getColumns: function() {
            var that = this;
            var columns = [{
                name: "nome",
                editable: false,
                sortable: true,
                label: "Nome",
                cell: "string",
            }, {
                name: "cpf",
                editable: false,
                sortable: true,
                label: "CPF",
                cell: "string",
            }, {
                name: "acoes",
                label: "Ações(Editar, Deletar)",
                sortable: false,
                cell: GeneralActionsCell.extend({
                    buttons: that.getCellButtons(),
                    context: that,
                })
            }];
            return columns;
        },
        getCellButtons: function() {
            var that = this;
            var buttons = this.checkGridButtonAuthority();
            return buttons;
        },
        deleteModel: function(model) {
            var that = this;
            var modelTipo = new SocioModel({
                id: model.id,
            });
            util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
                if (yes) {
                    modelTipo.destroy({
                        success: function() {
                            that.socios.remove(model);
                            util.showSuccessMessage('Sócio removido com sucesso!');
                        },
                        error: function(_model, _resp) {
                            util.showErrorMessage('Problema ao remover o registro', _resp);
                        }
                    });
                }
            });
        },
        novoSocio: function() {
            util.goPage("app/empresa/" + this.empresa.get('id') + "/newSocio");
        },
        voltarEmpresa: function() {
            util.goPage("app/empresas");
        },
        editModel: function(model) {
            util.goPage("app/empresa/" + this.empresa.get('id') + "/editSocio/" + model.get('id'));
        },
        // vitoriano : chunck : check grid buttons authority
        checkGridButtonAuthority: function() {
            var that = this;
            var buttons = [];
            $.grep(roles, function(e) {
                if (e.authority == 'ROLE_SOCIO_EDITAR' || e.authority == 'ROLE_ADMIN') {
                    buttons.push({
                        id: 'edita_ficha_button',
                        type: 'primary',
                        icon: 'icon-pencil fa-pencil',
                        hint: 'Editar Sócio',
                        onClick: that.editModel,
                    });
                }
                if (e.authority == 'ROLE_SOCIO_DELETAR' || e.authority == 'ROLE_ADMIN') {
                    buttons.push({
                        id: 'delete_button',
                        type: 'danger',
                        icon: 'icon-trash fa-trash',
                        hint: 'Remover Sócio',
                        onClick: that.deleteModel,
                    });
                }
            })
            return buttons;
        },
     // vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_SOCIO_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
    });
    return PageSocio;
});