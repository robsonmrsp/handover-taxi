/* generated: 25/10/2016 10:24:45 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var util = require('utilities/utils');

	var SuggestGmapBox = require('views/components/SuggestGmapBox');

	var FrameEnderecoPartidaTemplate = require('text!views/corrida/tpl/FrameEnderecoPartidaTemplate.html');

	var ModalEnderecoFavorito = require('views/modalComponents/EnderecoFavoritoModal');

	var FrameEnderecoPartida = Marionette.LayoutView.extend({
		template : _.template(FrameEnderecoPartidaTemplate),

		regions : {
			modalEnderecoFavoritoRegion : '.endereco-favorito-modal'
		},

		events : {
			'click .marcar-favorito' : 'marcarFavorito',
			'click .mostrar-modal-favoritos' : 'showModalEnderecoFavorito'
		},

		ui : {

			inputEnderecoCombo : '.inputEnderecoCombo',
			inputEnderecoLogradouro : '.inputEnderecoLogradouro',
			inputEnderecoNumero : '.inputEnderecoNumero',
			inputEnderecoCep : '.inputEnderecoCep',
			inputEnderecoBairro : '.inputEnderecoBairro',
			inputEnderecoCidade : '.inputEnderecoCidade',
			inputEnderecoUf : '.inputEnderecoUf',
			inputEnderecoReferencia : '.inputReferencia',
			inputEnderecoComplemento : '.inputComplemento',
			inputObservacao : '.inputObservacao',
			inputContato : '.inputContato',
			inputTarefa : '.inputTarefa',
			form : '.form-endereco-partida',
		},

		initialize : function(options) {
			var that = this;
			this.onSelectEndereco = options.onSelectEndereco;
			this.onSelectComoFavorito = options.onSelectComoFavorito;

			this.modalEnderecoFavorito = new ModalEnderecoFavorito({
				onSelectModel : function(model) {
					that.onSelectEnderecoFavorito(model);
				},

			});

			this.on('show', function() {

				this.modalEnderecoFavoritoRegion.show(this.modalEnderecoFavorito);
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});

				this.ui.inputEnderecoCep.mask('#####-###');
				this.enderecoPartida = new SuggestGmapBox({
					el : that.ui.inputEnderecoCombo,

					onSelect : function(endereco) {
						that.jsonValue = endereco;

						that.onSelectEndereco(endereco);
						that.populaEndereco(endereco);
					},
				});
			});
		},

		selectEndereco : function(endereco) {

		},

		populaEndereco : function(endereco) {
			this.ui.inputEnderecoLogradouro.val(endereco.logradouro || endereco.endereco);
			this.ui.inputEnderecoNumero.val(endereco.numero);
			this.ui.inputEnderecoCep.val(endereco.cep);
			this.ui.inputEnderecoCombo.val(endereco.enderecoFormatado);
			this.ui.inputEnderecoBairro.val(endereco.bairro);
			this.ui.inputEnderecoCidade.val(endereco.cidade);
			this.ui.inputEnderecoUf.val(endereco.uf);
			this.ui.inputEnderecoComplemento.val(endereco.complemento);
			this.ui.inputEnderecoReferencia.val(endereco.referencia);
		},

		clearForm : function() {
			this.ui.inputEnderecoCombo.val('');
			this.ui.inputEnderecoLogradouro.val('');
			this.ui.inputEnderecoNumero.val('');
			this.ui.inputEnderecoCep.val('');
			this.ui.inputEnderecoUf.val('');
			this.ui.inputEnderecoBairro.val('');
			this.ui.inputEnderecoCidade.val('');
			this.ui.inputEnderecoComplemento.val('');
			this.ui.inputObservacao.val('');
			this.ui.inputEnderecoReferencia.val('');
			this.ui.inputContato.val('')
			this.ui.inputTarefa.val('')
			this.enderecoPartida.clear();
		},

		getJsonValue : function() {
			if (this.jsonValue) {
				this.jsonValue.origem = true;
				this.jsonValue.ordem = 0;
				this.jsonValue.complemento = this.ui.inputEnderecoComplemento.val();
				this.jsonValue.referencia = this.ui.inputEnderecoReferencia.val();

				this.jsonValue.contato = this.ui.inputContato.val();
				this.jsonValue.tarefa = this.ui.inputTarefa.val();
			} else {
				return this.model && this.model.toJSON();
			}
			return this.jsonValue;
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		showModalEnderecoFavorito : function() {
			// add more before the modal is open
			if (this.cliente) {
				this.modalEnderecoFavorito.setCliente(this.cliente);
				this.modalEnderecoFavorito.showPage();
			}
		},

		onSelectEnderecoFavorito : function(endereco) {
			// somente para o mapa poder apontar esse endereço.
			endereco.set('latlong', {
				lat : endereco.get('latitude'),
				lng : endereco.get('longitude')
			});

			var json = endereco.toJSON();

			this.jsonValue = json;

			this.populaEndereco(json);
			this.onSelectEndereco(json);

			this.modalEnderecoFavorito.hidePage();
		},
		marcarFavorito : function() {
			var jsonEndereco = this.getJsonValue();
			if (this.onSelectComoFavorito) {
				if (jsonEndereco) {
					this.onSelectComoFavorito(jsonEndereco);
				}
			}
		},
		setEmpresa : function(empresaJson) {
			this.empresa = empresaJson;
			this.cliente = null;
		},

		setCliente : function(clienteJson) {
			this.cliente = clienteJson;
			this.empresa = null;

		},
	});

	return FrameEnderecoPartida;
});