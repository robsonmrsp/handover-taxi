/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var CorridaPageCollection = require('collections/CorridaPageCollection');
	var ModalMultiSelectCorridaTemplate = require('text!views/corrida/tpl/ModalMultiSelectCorridaTemplate.html');
	// End of "Import´s" definition

	var ModalCorridas = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectCorridaTemplate),

		regions : {
			gridRegion : '#grid-corridas-modal',
			paginatorRegion : '#paginator-corridas-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoCorridas = this.collection;
			
			this.corridas = new CorridaPageCollection();
			this.corridas.on('fetched', this.endFetch, this);
			this.corridas.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.corridas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.corridas,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.corridas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid corrida');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoCorridas.add(model)
			else
				this.projetoCorridas.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.corridas.each(function(model) {
				if (that.projetoCorridas.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "dataCorrida",
				editable : false,
				sortable : false,
				label 	 : "Data corrida",
				cell 	 : "string",
			}, 
			{
				name : "tipoPagamento",
				editable : false,
				sortable : false,
				label 	 : "Tipo pagamento",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "valor",
				editable : false,
				sortable : false,
				label 	 : "Valor",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "observacao",
				editable : false,
				sortable : false,
				label 	 : "Observacao",
				cell 	 : "string",
			}, 
			{
				name : "statusCorrida",
				editable : false,
				sortable : false,
				label 	 : "Status corrida",
				cell 	 : "string",
			}, 
			{
				name : "seqEmAtendimento",
				editable : false,
				sortable : false,
				label 	 : "Seq em atendimento",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "motoGrande",
				editable : false,
				sortable : false,
				label 	 : "Moto grande",
				cell 	 : "string",
			}, 
			{
				name : "bau",
				editable : false,
				sortable : false,
				label 	 : "Bau",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalCorridas;
});
