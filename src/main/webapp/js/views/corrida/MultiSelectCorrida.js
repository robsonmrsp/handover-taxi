/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectCorrida = require('views/corrida/ModalMultiSelectCorrida');
	var MultiSelectCorridaTemplate = require('text!views/corrida/tpl/MultiSelectCorridaTemplate.html');

	var MultiSelectCorrida = Marionette.LayoutView.extend({
		template : _.template(MultiSelectCorridaTemplate),

		regions : {
			modalMultiSelectCorridaRegion : '#modalMultiSelectCorridas',
			gridCorridasModalRegion : '#gridMultiselectCorridas',
		},

		initialize : function() {
			var that = this;

			this.corridas = this.collection;

			this.gridCorridas = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.corridas,
			});

			this.modalMultiSelectCorrida = new ModalMultiSelectCorrida({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectCorridaRegion.show(that.modalMultiSelectCorrida);
				that.gridCorridasModalRegion.show(that.gridCorridas);
			});
		},
		clear : function() {
			this.modalMultiSelectCorrida.clear();
		},

		_getColumns : function() {
			var columns = [

				{
					name : "dataCorrida",
					editable : false,
					sortable : false,
					label : "Data corrida",
					cell : "string",
				},
				{
					name : "tipoPagamento",
					editable : false,
					sortable : false,
					label : "Tipo pagamento",
					cell : CustomNumberCell.extend({}),
				},
				{
					name : "valor",
					editable : false,
					sortable : false,
					label : "Valor",
					cell : CustomNumberCell.extend({}),
				},
				{
					name : "observacao",
					editable : false,
					sortable : false,
					label : "Observacao",
					cell : "string",
				},
				{
					name : "statusCorrida",
					editable : false,
					sortable : false,
					label : "Status corrida",
					cell : "string",
				},
				{
					name : "seqEmAtendimento",
					editable : false,
					sortable : false,
					label : "Seq em atendimento",
					cell : CustomNumberCell.extend({}),
				},
				{
					name : "motoGrande",
					editable : false,
					sortable : false,
					label : "Moto grande",
					cell : "string",
				},
				{
					name : "bau",
					editable : false,
					sortable : false,
					label : "Bau",
					cell : "string",
					
				},
			];
			return columns;
		},
	});

	return MultiSelectCorrida
});