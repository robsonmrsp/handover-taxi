/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateModalCorrida = require('text!views/corrida/tpl/ModalConfirmacaoCorridaTemplate.html');

	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
	// BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormCorridas = Marionette.LayoutView.extend({
		template : _.template(TemplateModalCorrida),

		regions : {},

		events : {},

		ui : {
			form : '#formCorrida',

			enderecoPartida : ".endereco-partida",
			enderecosDestino : ".enderecos-destino",
			dataCorrida : ".data-corrida",
			tipoPagamento : ".tipo-pagamento",
			motoGrande : ".moto-grande",
			cliente : ".cliente",
			empresa : ".empresa",
			valor : ".valor",
			bau : ".bau",

			modalScreen : '.modal'
		},

		initialize : function() {
			var that = this;

			this.on('show', function() {

			});
		},

		showInPage : function(modelCorrida) {
			this.model = modelCorrida;

			this.ui.dataCorrida.text(modelCorrida.get('dataCorrida') || 'dd/MM/yyyy ');
			this.ui.valor.text('R$' + modelCorrida.get('valor'));

			this.ui.tipoPagamento.text('pagamento');

			if (modelCorrida.get('tipoPagamento') === 0) {
				this.ui.tipoPagamento.text('Dinheiro');
			}

			if (modelCorrida.get('tipoPagamento') === 1) {
				this.ui.tipoPagamento.text('Crédito');
			}

			if (modelCorrida.get('tipoPagamento') === 2) {
				this.ui.tipoPagamento.text('Débito');
			}
			if (modelCorrida.get('tipoPagamento') === 3) {
				this.ui.tipoPagamento.text('Voucher');
			}
			if (modelCorrida.get('tipoPagamento') === 4) {
				this.ui.tipoPagamento.text('E-Ticket');
			}
			this.ui.motoGrande.text(modelCorrida.get('motoGrande') ? 'Sim' : 'Não');
			this.ui.bau.text(modelCorrida.get('bau') ? 'Sim' : 'Não');

			var partida = '';
			var destinos = '';

			_.each(modelCorrida.get('enderecoCorridas'), function(endereco) {
				if (endereco.origem === true) {
					partida = endereco.enderecoFormatado;
				} else {
					destinos += endereco.enderecoFormatado + ', ';
				}
			})

			this.ui.cliente.text(modelCorrida.get('cliente') && modelCorrida.get('cliente').nome || ' ');
			this.ui.empresa.text(modelCorrida.get('empresa') && modelCorrida.get('empresa').nomeFantasia || '  ');
			this.ui.enderecoPartida.text(partida);
			this.ui.enderecosDestino.text(destinos);

			this.ui.modalScreen.modal({
				show : true,
				backdrop : 'static'
			});
		}
	});

	return FormCorridas;
});