/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');
	var CustomNumberCell = require('views/components/CustomNumberCell');
	var TarifaModel = require('models/TarifaModel');
	var TarifaAdicionalCollection = require('collections/TarifaAdicionalCollection');
	var InformacaoTarifaAdicionalCollection = require('collections/InformacaoTarifaAdicionalCollection');
	var EnderecoCorridaCollection = require('collections/EnderecoCorridaCollection');
	var FrameTarifasAdicionaisCorridaTemplate = require('text!views/corrida/tpl/FrameTarifasAdicionaisCorridaTemplate.html');
	var ModalTarifaAdicional = require('views/modalComponents/TarifaAdicionalModal');
	var TarifaAdicionalModel = require('models/TarifaAdicionalModel');
	var InformacaoTarifaAdicionalModel = require('models/InformacaoTarifaAdicionalModel')
	var FrameTarifasAdicionaisCorrida = Marionette.LayoutView.extend({
		template : _.template(FrameTarifasAdicionaisCorridaTemplate),

		regions : {
			gridRegion : '.grid-tarifas-adicionais',
			modalTarifaAdicionalRegion : '.modal-tarifa-adicional',
		},

		events : {
			'click .escolher-tarifa-adicional' : 'abrirModalTarifasAdicionais',
			'change  .valor-maior-trajeto' : 'changeValorMaiorTrajeto',
			'change .input-valor-corrida' : 'changeValorCorrida'
		},

		ui : {
			valorCorridaMaiorTrajeto : '.valor-maior-trajeto',
			valorCorrida : '.valor-corrida',
			inputValorCorrida : '.input-valor-corrida',
			maiorMrajeto : '.maior-trajeto'
		},

		initialize : function(options) {

			var that = this;

			this.enderecoPartidaJson = options.enderecoPartida;
			this.enderecos = options.enderecos;

			this.informacaoTarifas = options.informacaoTarifas || [];

			this.onDefinePrecos = options.onDefinePrecos;

			this.enderecos.on('add', this.onAddEnderecos, this);
			this.enderecos.on('sort', this.onAddEnderecos, this);
			this.enderecos.on('remove', this.onAddEnderecos, this);

			this.tarifaAdicionals = new InformacaoTarifaAdicionalCollection(this.informacaoTarifas);

			this.tarifaAdicionals.on('backgrid:edited', this.editTarifaAdicional, this);

			this.tarifaAdicionals.on('add', this.calculaTotal, this);
			this.tarifaAdicionals.on('remove', this.calculaTotal, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table table-striped table-bordered',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.tarifaAdicionals
			});
			this.modalTarifaAdicional = new ModalTarifaAdicional({
				onSelectModel : function(model) {
					that.onSelectTarifaAdicional(model);
				},
			})

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				if (this.tarifaAdicionals.size() > 0) {
					that.ui.valorCorridaMaiorTrajeto.val(this.tarifaAdicionals.at(0).get('valorTotal'));
				}
				this.ui.valorCorridaMaiorTrajeto.mask('###.###.###.###,00', {
					reverse : true
				});
				that.modalTarifaAdicionalRegion.show(this.modalTarifaAdicional);
			});
		},
		onSelectTarifaAdicional : function(model) {

			var informacaoTarifaAdicional = new InformacaoTarifaAdicionalModel();

			informacaoTarifaAdicional.set('tarifaAdicional', model.toJSON());

			informacaoTarifaAdicional.set('valorTotal', model.get('valor') * 1 * model.get('quantidade'));

			this.tarifaAdicionals.add(informacaoTarifaAdicional.toJSON());
		},

		abrirModalTarifasAdicionais : function() {
			this.modalTarifaAdicional.showPage();
		},
		calculaTotal : function() {
			var total = 0;
			this.tarifaAdicionals.each(function(model) {

				total = (total + 0) + new Number(util.strToNum(model.get('valorTotal')));
			})

			this.ui.inputValorCorrida.text(util.formatMoney(total));
			if (this.onDefinePrecos) {
				this.onDefinePrecos(util.formatMoney(total));
			}
		},

		editTarifaAdicional : function(model) {
			if (model.get('tarifaAdicional')) {
				model.set('valorTotal', model.get('tarifaAdicional').valor * 1 * model.get('quantidade'));
				this.calculaTotal();
			}
		},

		getColumns : function() {
			var that = this;
			var columns = [ {
				name : "tarifaAdicional",
				editable : false,
				sortable : false,
				label : "Descrição",
				cell : "string",
				formatter : _.extend({}, Backgrid.CellFormatter.prototype, {
					fromRaw : function(tarifaAdicional, model) {
						var descricao = (tarifaAdicional && tarifaAdicional.get && tarifaAdicional.get('descricao')) || (tarifaAdicional && tarifaAdicional.descricao) || model.get('descricao');
						return descricao;
					}
				})

			}, {
				name : "quantidade",
				editable : true,
				sortable : false,
				label : "Quantidade",
				cell : 'string',
			}, {
				name : "tarifaAdicional",
				editable : false,
				sortable : false,
				label : "Valor unitário",
				cell : 'string',
				formatter : _.extend({}, Backgrid.CellFormatter.prototype, {
					fromRaw : function(tarifaAdicional, model) {

						var valor = (tarifaAdicional && tarifaAdicional.get && tarifaAdicional.get('valor')) || (tarifaAdicional && tarifaAdicional.valor) || model.get('valorTotal');
						return 'R$ ' + util.printFormatNumber(valor);
					}
				})

			}, {
				name : "valorTotal",
				editable : false,
				sortable : false,
				label : "Valor Total",
				cell : 'string',
				formatter : _.extend({}, Backgrid.CellFormatter.prototype, {
					fromRaw : function(rawValue, model) {
						return 'R$ ' + util.printFormatNumber(rawValue);
					}
				})
			}, {
				name : "acoes",
				editable : false,
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},

		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover ',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;

			this.tarifaAdicionals.remove(model);
		},
		changeValorMaiorTrajeto : function() {
			// var model = {
			// descricao : "Valor da corrida para maior trajeto",
			// valor : this.ui.valorCorridaMaiorTrajeto.val(),
			// valorTotal : this.ui.valorCorridaMaiorTrajeto.val() || 0,
			// };
			var informacaoTarifaAdicional = new InformacaoTarifaAdicionalModel();

			// informacaoTarifaAdicional.set('id', -1);
			// informacaoTarifaAdicional.set('tarifaAdicional', model);
			informacaoTarifaAdicional.set('descricao', 'Valor da corrida para maior trajeto');
			// informacaoTarifaAdicional.set('tarifaAdicional',
			// this.ui.valorCorridaMaiorTrajeto.val() || 0);
			informacaoTarifaAdicional.set('valorTotal', this.ui.valorCorridaMaiorTrajeto.val() || 0);

			if (this.tarifaAdicionals.at(0)) {
				this.tarifaAdicionals.at(0).set(informacaoTarifaAdicional.toJSON());
				this.tarifaAdicionals.at(0).trigger('change:tarifaAdicional');
			} else {
				this.tarifaAdicionals.unshift(informacaoTarifaAdicional.toJSON());
			}
			this.calculaTotal();
		},

		onAddEnderecos : function(model, destino_collection) {
			this.buscaMaiorTrajeto();
			this.addTarifasDestinoExtra();
		},

		addTarifasDestinoExtra : function(model, destino_collection) {
			var total = 0;
		},

		buscaMaiorTrajeto : function() {
			var that = this;
			var collection = new EnderecoCorridaCollection();

			if (this.enderecoPartidaJson)
				collection.unshift(this.enderecoPartidaJson);
			collection.add(this.enderecos.toJSON());

			var newTextMaiorTrajeto = '';
			var newValorMaiorTrajeto = 0;

			var bairro0 = '';
			var bairro1 = '';
			this.inicioMaiorTrajeto = null;
			this.finalMaiorTrajeto = null;

			if (collection.size() == 2) {
				var endereco0 = collection.at(0);
				var endereco1 = collection.at(1);

				var latLong0 = new google.maps.LatLng(endereco0.get('latitude'), endereco0.get('longitude'));
				var latLong1 = new google.maps.LatLng(endereco1.get('latitude'), endereco1.get('longitude'));

				this.valorMaiorTrajeto = google.maps.geometry.spherical.computeDistanceBetween(latLong0, latLong1)
				this.textMaiorTrajeto = endereco0.get('enderecoFormatado') + ' --> ' + endereco1.get('enderecoFormatado')

				bairro0 = endereco0.get('bairro');
				bairro1 = endereco1.get('bairro');

				console.log(this.textMaiorTrajeto);

				this.buscaSugestaoValorMaiorTrajeto(bairro0, bairro1);
			} else if (collection.size() > 2) {
				that.valorMaiorTrajeto = 0;
				var endereco0 = null; // collection.at(collection.size() - 2);
				var endereco1 = null; // collection.at(collection.size() - 1);

				collection.each(function(endereco, index) {
					if (index < collection.size() - 1) {
						endereco0 = endereco;
						endereco1 = collection.at(index + 1);

						bairro0 = endereco0.get('bairro');
						bairro1 = endereco1.get('bairro');

						var latLong0 = new google.maps.LatLng(endereco0.get('latitude'), endereco0.get('longitude'));
						var latLong1 = new google.maps.LatLng(endereco1.get('latitude'), endereco1.get('longitude'));

						newValorMaiorTrajeto = google.maps.geometry.spherical.computeDistanceBetween(latLong0, latLong1)
						console.log("Distancia do trajeto entre os pontos/bairros" + bairro0 + " " + bairro1 + " " + newValorMaiorTrajeto);
						newTextMaiorTrajeto = endereco0.get('enderecoFormatado') + ' --> ' + endereco1.get('enderecoFormatado')

						if (newValorMaiorTrajeto >= that.valorMaiorTrajeto) {
							console.log("Maior trajeto antes : ", that.textMaiorTrajeto)
							console.log("Maior trajeto depois : ", that.newTextMaiorTrajeto)
							that.valorMaiorTrajeto = newValorMaiorTrajeto;
							that.textMaiorTrajeto = newTextMaiorTrajeto;
							that.buscaSugestaoValorMaiorTrajeto(bairro0, bairro1);
						}
					}
				})
			}

			this.ui.maiorMrajeto.text(this.textMaiorTrajeto);
		},
		// Busca o Valor da tarifa baseado nos bairros
		buscaSugestaoValorMaiorTrajeto : function(bairroOrigem, bairroDestino) {
			var that = this;
			var tarifa = new TarifaModel();
			this.ui.valorCorridaMaiorTrajeto.val("");

			if (bairroOrigem && bairroDestino) {
				tarifa.fetch({
					success : function(col, resp, xhr) {
						if (col && col.get('itens') && col.get('itens')[0]) {
							var tarifa = col.get('itens')[0];
							that.ui.valorCorridaMaiorTrajeto.val(util.formatMoney(tarifa.valor));
							if (that.onDefinePrecos) {
								that.onDefinePrecos(util.formatMoney(tarifa.valor));
								that.changeValorMaiorTrajeto();
							}

						}
					},
					error : function(model, resp, xhr) {
						console.error(model, resp, xhr);
					},
					data : {
						bairroDestino : bairroDestino,
						bairroOrigem : bairroOrigem
					}
				});
				if(this.ui.valorCorridaMaiorTrajeto.val() == ""){
					tarifa.fetch({
						success : function(col, resp, xhr) {
							if (col && col.get('itens') && col.get('itens')[0]) {
								var tarifa = col.get('itens')[0];
								that.ui.valorCorridaMaiorTrajeto.val(util.formatMoney(tarifa.valor));
								if (that.onDefinePrecos) {
									that.onDefinePrecos(util.formatMoney(tarifa.valor));
									that.changeValorMaiorTrajeto();
								}

							}
						},
						error : function(model, resp, xhr) {
							console.error(model, resp, xhr);
						},
						data : {
							bairroDestino : bairroOrigem,
							bairroOrigem : bairroDestino
						}
					});
				}
			}
		},

		getTarifas : function() {
			return this.tarifaAdicionals;
		},

		setEnderecoPartida : function(enderecoJson) {
			this.enderecoPartidaJson = enderecoJson;
		},

		clear : function() {
			this.ui.valorCorridaMaiorTrajeto.val('');
			this.ui.maiorMrajeto.text('');
			this.ui.valorCorrida.text('');
			this.tarifaAdicionals.reset();
			this.enderecos.reset();
		},
	});

	return FrameTarifasAdicionaisCorrida;
});