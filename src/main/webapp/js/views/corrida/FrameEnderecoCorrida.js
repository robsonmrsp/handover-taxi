/* generated: 25/10/2016 10:24:45 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var util = require('utilities/utils');

	var EnderecoCorridaModel = require('models/EnderecoCorridaModel');
	var SuggestGmapBox = require('views/components/SuggestGmapBox');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var FrameEnderecoCorridaTemplate = require('text!views/corrida/tpl/FrameEnderecoCorridaTemplate.html');

	var FormCorridas = Marionette.LayoutView.extend({
		template : _.template(FrameEnderecoCorridaTemplate),

		regions : {
			gridEndereosEscolhidosRegion : '.grid-enderecos-escolhidos-container',
		},

		events : {
			'click .adicionar-endereco-corrida' : 'escolherEndereco',
			'click .usar-este-endereco-continuar' : 'usarEsteEnderecoEContinuar',
			'click .usar-este-endereco' : 'usarEsteEndereco',
			'click .salvar-endereco' : 'salvarEndereco',
		},

		initialize : function(options) {
			var that = this;
			this.onSelectEndereco = options.onSelectEndereco;

			this.enderecoCorridas = options.enderecos;
			this.onAddFavorito = options.onAddFavorito;

			this.gridEnderecosEscolhidos = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getEndereosEscolhidosColumns(),
				emptyText : "Não há endereços para a corrida",
				collection : this.enderecoCorridas
			});

			this.on('show', function() {
				that.gridEndereosEscolhidosRegion.show(that.gridEnderecosEscolhidos);

				this.ui.inputEnderecoCep.mask('#####-###');
				this.enderecoPartida = new SuggestGmapBox({
					el : that.ui.inputEnderecoCombo,

					onSelect : function(endereco) {
						that.jsonValue = endereco;
						that.model.set(endereco);
						that.populaEndereco(endereco);
					},
				});
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});

				var fixHelperModified = function(e, tr) {
					var $originals = tr.children();
					var $helper = $('<span>');

					$helper.css("background-color", "#ccc");
					$helper.css("text-align", "left");
					$helper.text($($originals.get(1)).text())
					$helper.width(tr.width());

					return $helper;

				},
				// acontece depois da recolocação dos itens da tabela.
				updateIndex = function(e, ui) {

					var novaListaLinhas = ui.item.parent().find('tr');
					_.each(novaListaLinhas, function(tr, index) {
						// pega o indice guardado na tela.
						if (tr.cells.length > 1) {

							var cels = $(tr).children()
							var ordem = parseInt($(cels.get(0)).text());
							var model = that.enderecoCorridas.findWhere({
								ordem : ordem
							});
							model.set('ordem', index + 1);
							that.enderecoCorridas.add(model.toJSON(), {
								merge : true
							});
							// busca na coleção pela ordem. e substitue pelo
							// indice.
							// that.areas.sort()
						}
					})
					that.enderecoCorridas.sort();
				};

				$(".grid-enderecos-escolhidos-container tbody").sortable({

					helper : fixHelperModified,
					stop : updateIndex
				}).disableSelection();

				// this.ui.inputEnderecoCombo.val(this.model.get('enderecoFormatado'))
			});
		},

		escolherEndereco : function() {
			this.ui.modalScreen.modal({
				backdrop : 'static',
				show : true,
			});
		},

		usarEsteEnderecoEContinuar : function() {

			if (this.jsonValue != null) {
				if (this.isValid()) {
					var model = new EnderecoCorridaModel(this.jsonValue);

					model.set('complemento', this.ui.inputComplemento.val());
					model.set('ordem', this.enderecoCorridas.size() + 1);
					model.set('contato', this.ui.inputContato.val());
					model.set('tarefa', this.ui.inputTarefa.val());
					model.set('referencia', this.ui.inputEnderecoReferencia.val());

					this.enderecoCorridas.add(model.toJSON(), {
						merge : false
					});

					this.clearForm()

					this.enderecoPartida.clear();
				} else {
					util.showMessage('warning', 'Voce deve preencher os campos com (*)!', 'mensagem-endereco-destino-modal');
				}
			}
		},
		usarMarcarFavorito : function() {

			if (this.jsonValue != null) {
				if (this.isValid()) {
					var model = new EnderecoCorridaModel(this.jsonValue);

					model.set('complemento', this.ui.inputComplemento.val());
					model.set('ordem', this.enderecoCorridas.size() + 1);
					model.set('contato', this.ui.inputContato.val());
					model.set('tarefa', this.ui.inputTarefa.val());
					model.set('referencia', this.ui.inputEnderecoReferencia.val());

					this.enderecoCorridas.add(model.toJSON(), {
						merge : true
					});

					if (this.onAddFavorito) {
						this.onAddFavorito(model.toJSON());
					}

					this.clearForm()

					this.enderecoPartida.clear();
				} else {
					util.showMessage('warning', 'Voce deve preencher os campos com (*)!', 'mensagem-endereco-destino-modal');
				}
			}
		},
		usarEsteEndereco : function() {
			if (this.jsonValue != null) {
				if (this.isValid()) {
					var model = new EnderecoCorridaModel(this.jsonValue);

					model.set('ordem', this.enderecoCorridas.size() + 1);
					model.set('complemento', this.ui.inputComplemento.val());
					model.set('contato', this.ui.inputContato.val());
					model.set('tarefa', this.ui.inputTarefa.val());
					model.set('referencia', this.ui.inputEnderecoReferencia.val());

					this.enderecoCorridas.add(model.toJSON(), {
						merge : false,
					});

					this.enderecoPartida.clear();
					this.clearForm()
					this.ui.modalScreen.modal('hide');
				} else {
					util.showMessage('warning', 'Campos com (*) são obrigórios !', 'mensagem-endereco-destino-modal');
				}
			}
		},

		ui : {

			inputContato : '.inputContato',
			inputTarefa : '.inputTarefa',
			inputComplemento : '.inputComplemento',

			inputEnderecoCombo : '.inputEnderecoCombo',
			inputEnderecoLogradouro : '.inputEnderecoLogradouro',
			inputEnderecoNumero : '.inputEnderecoNumero',
			inputEnderecoCep : '.inputEnderecoCep',
			inputEnderecoBairro : '.inputEnderecoBairro',
			inputEnderecoCidade : '.inputEnderecoCidade',
			inputEnderecoUf : '.inputEnderecoUf',
			modalScreen : '.modal',
			inputEnderecoReferencia : '.inputReferencia',
			inputEnderecoComplemento : '.inputComplemento',
			camposNaoEditaveis : '.endereco-partida input',
			botaoUsarEsteEnderecoContinuar : '.usar-este-endereco-continuar',
			botaoUsarEsteEndereco : '.usar-este-endereco',
			botaoSalvarEndereco : '.salvar-endereco',
			form : '.form-endereco-escolhido',
		},

		selectEndereco : function(endereco) {

		},
		getEndereosEscolhidosColumns : function() {
			var that = this;

			// Observação IMPORTANTE, por enquanto a coluna ordem é importante
			// para a reorganização pelo metodo arrastar e soltar
			var columns = [ { // 
				name : "ordem",
				sortable : false,
				editable : false,
				label : "Ordem",
				cell : 'string'
			}, {
				name : "enderecoFormatado",
				sortable : false,
				editable : false,
				label : "Endereço completo",
				cell : 'string'
			}, {
				// name : "referencia",
				// sortable : false,
				// editable : false,
				// label : "Referência",
				// cell : 'string'
				// }, {
				// name : "contato",
				// sortable : false,
				// editable : false,
				// label : "Contato",
				// cell : 'string'
				// }, {
				// name : "tarefa",
				// sortable : false,
				// editable : false,
				// label : "Tarefa",
				// cell : 'string'
				// }, {
				name : "acoes",
				label : "Ações",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			}

			];
			return columns;
		},

		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Endereço',
				onClick : that.editModel,
			},
			// {
			// id : 'edita_ficha_button',
			// type : 'primary',
			// icon : 'fa-sort-asc',
			// hint : 'subir',
			// onClick : that.subirModel,
			// },
			// {
			// id : 'edita_ficha_button',
			// type : 'primary',
			// icon : 'fa-sort-desc',
			// hint : 'Descer',
			// onClick : that.descerModel,
			// },

			{
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Corrida',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		// descerModel : function(model) {
		// this.enderecoCorridas.movePraBaixo(model);
		// },
		// subirModel : function(model) {
		// this.enderecoCorridas.movePraCima(model);
		// },
		//		
		editModel : function(model) {
			this.populaEndereco(model.toJSON(), true);
			this.jsonValue = model.toJSON();
			this.ui.modalScreen.modal({
				backdrop : 'static',
				show : true,
			});
		},

		deleteModel : function(model) {
			var that = this;

			var modelTipo = new EnderecoCorridaModel({
				id : model.id,
			});

			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('enderecoFormatado') + " ] ?", function(yes) {
				if (yes) {
					if (_.isNumber(model.id)) {
						modelTipo.destroy({
							success : function() {
								that.enderecoCorridas.remove(model);
								util.showSuccessMessage('Corrida removido com sucesso!');
							},
							error : function(_model, _resp) {
								util.showErrorMessage('Problema ao remover o registro', _resp);
							}
						});
					} else {
						that.enderecoCorridas.remove(model);
					}

				}
			});
		},

		populaEndereco : function(endereco, edit) {
			this.ui.inputEnderecoLogradouro.val(endereco.logradouro);
			this.ui.inputEnderecoNumero.val(endereco.numero);
			this.ui.inputEnderecoCep.val(endereco.cep);
			this.ui.inputEnderecoBairro.val(endereco.bairro);
			this.ui.inputEnderecoCidade.val(endereco.cidade);
			this.ui.inputEnderecoUf.val(endereco.uf);
			this.ui.inputEnderecoUf.val(endereco.uf);
			this.ui.inputEnderecoComplemento.val(endereco.complemento);
			this.ui.inputEnderecoReferencia.val(endereco.referencia);
			this.ui.inputContato.val(endereco.contato);
			this.ui.inputTarefa.val(endereco.tarefa);

			if (edit) {
				this.ui.botaoSalvarEndereco.show()
				this.ui.botaoUsarEsteEnderecoContinuar.hide()
				this.ui.botaoUsarEsteEndereco.hide()
				this.ui.camposNaoEditaveis.prop('disabled', true);
			} else {
				this.ui.botaoSalvarEndereco.hide()
				this.ui.botaoUsarEsteEnderecoContinuar.show()
				this.ui.botaoUsarEsteEndereco.show();
				this.ui.camposNaoEditaveis.prop('disabled', false);
			}
		},
		// somente será usado para editar o endereço em questão nos campos que
		// são passivels de edição
		salvarEndereco : function() {
			var model = new EnderecoCorridaModel(this.jsonValue);

			//model.set('ordem', this.enderecoCorridas.size() + 1);
			model.set('complemento', this.ui.inputComplemento.val());
			model.set('contato', this.ui.inputContato.val());
			model.set('tarefa', this.ui.inputTarefa.val());
			model.set('referencia', this.ui.inputEnderecoReferencia.val());

			this.enderecoCorridas.add(model.toJSON(), {
				merge : true
			});

			this.clearForm();
		},

		clearForm : function() {
			this.ui.inputEnderecoLogradouro.val('');
			this.ui.inputEnderecoNumero.val('');
			this.ui.inputEnderecoCep.val('');
			this.ui.inputEnderecoBairro.val('');
			this.ui.inputEnderecoCidade.val('');
			this.ui.inputEnderecoCidade.val('');
			this.ui.inputEnderecoComplemento.val('');
			this.ui.inputEnderecoReferencia.val('');
			this.ui.inputContato.val('');
			this.ui.inputTarefa.val('');
			this.ui.camposNaoEditaveis.prop('disabled', false);

			this.ui.botaoSalvarEndereco.hide()
			this.ui.botaoUsarEsteEnderecoContinuar.show()
			this.ui.botaoUsarEsteEndereco.show();

			this.enderecoPartida.clear();

			this.jsonValue = null;
		},

		getJsonValue : function() {
			if (this.jsonValue) {
				this.jsonValue.complemento = this.ui.inputEnderecoComplemento.val();
				this.jsonValue.contato = this.ui.inputContato.val();
				this.jsonValue.referencia = this.ui.inputEnderecoReferencia.val();
			}
			return this.jsonValue;
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

	});

	return FormCorridas;
});