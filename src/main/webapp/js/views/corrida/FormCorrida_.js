/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormCorridas = require('text!views/corrida/tpl/FormCorridaTemplate.html');
	var CorridaModel = require('models/CorridaModel');
	var CorridaCollection = require('collections/CorridaCollection');
	var ModalAtendente = require('views/modalComponents/AtendenteModal');
	var ModalMotorista = require('views/modalComponents/MotoristaModal');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormCorridas = Marionette.LayoutView.extend({
		template : _.template(TemplateFormCorridas),

		regions : {
			modalAtendenteRegion : '#atendenteModal',
			modalMotoristaRegion : '#motoristaModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click #searchAtendenteModal' : 'showModalAtendente',
			'click #searchMotoristaModal' : 'showModalMotorista',
		},
		
		ui : {
			inputId : '#inputId',
			inputDataCorrida : '#inputDataCorrida',
			groupInputDataCorrida : '#groupInputDataCorrida',
			inputTipoPagamento : '#inputTipoPagamento',
			inputValor : '#inputValor',
			inputObservacao : '#inputObservacao',
			inputStatusCorrida : '#inputStatusCorrida',
			inputSeqEmAtendimento : '#inputSeqEmAtendimento',
			inputMotoGrande : '#inputMotoGrande',
			inputBau : '#inputBau',
		
			inputAtendenteId : '#inputAtendenteId',
			inputAtendenteNome : '#inputAtendenteNome',
			inputMotoristaId : '#inputMotoristaId',
			inputMotoristaNome : '#inputMotoristaNome',
			form : '#formCorrida',
		},

		initialize : function() {
			var that = this;
			this.modalAtendente = new ModalAtendente({
				onSelectModel : function(model) {
					that.onSelectAtendente(model);
				},
			});
			this.modalMotorista = new ModalMotorista({
				onSelectModel : function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.on('show', function() {
				this.modalAtendenteRegion.show(this.modalAtendente);		
				this.modalMotoristaRegion.show(this.modalMotorista);		
		
				this.ui.groupInputDataCorrida.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataCorrida.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataCorrida.mask('99/99/9999 99:99');
				this.ui.inputTipoPagamento.formatNumber(2);
				this.ui.inputValor.formatNumber(2);
				this.ui.inputSeqEmAtendimento.formatNumber(2);
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var corrida = that.getModel();

			if (this.isValid()) {
				corrida.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Corrida salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/corridas');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputDataCorrida'); 
			util.clear('inputTipoPagamento'); 
			util.clear('inputValor'); 
			util.clear('inputObservacao'); 
			util.clear('inputStatusCorrida'); 
			util.clear('inputSeqEmAtendimento'); 
			util.clear('inputMotoGrande'); 
			util.clear('inputBau'); 
			util.clear('inputAtendenteId');
			util.clear('inputAtendenteNome');
			util.clear('inputMotoristaId');
			util.clear('inputMotoristaNome');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var corrida = that.model; 
			corrida.set({
				id: util.escapeById('inputId') || null,
		    	dataCorrida : util.escapeById('inputDataCorrida'), 
		    	tipoPagamento : util.escapeById('inputTipoPagamento', true), 
		    	valor : util.escapeById('inputValor', true), 
		    	observacao : util.escapeById('inputObservacao'), 
		    	statusCorrida : util.escapeById('inputStatusCorrida'), 
		    	seqEmAtendimento : util.escapeById('inputSeqEmAtendimento', true), 
		    	motoGrande : util.escapeById('inputMotoGrande'), 
		    	bau : util.escapeById('inputBau'), 
				atendente : that.modalAtendente.getJsonValue(),
				motorista : that.modalMotorista.getJsonValue(),
			});
			return corrida;
		},
		 		
		showModalAtendente : function() {
			// add more before the modal is open
			this.modalAtendente.showPage();
		},
		showModalMotorista : function() {
			// add more before the modal is open
			this.modalMotorista.showPage();
		},

		onSelectAtendente : function(atendente) {
			this.modalAtendente.hidePage();	
			this.ui.inputAtendenteId.val(atendente.get('id'));
			this.ui.inputAtendenteNome.val(atendente.get('nome'));		
		},
		onSelectMotorista : function(motorista) {
			this.modalMotorista.hidePage();	
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));		
		},
				
		
	});

	return FormCorridas;
});