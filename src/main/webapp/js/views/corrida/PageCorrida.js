/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var client = require('adapters/auth-adapter');
	var roles = client.roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');
	var CustomNumberCell = require('views/components/CustomNumberCell');
	var CorridaModel = require('models/CorridaModel');
	var CorridaCollection = require('collections/CorridaCollection');
	var CorridaPageCollection = require('collections/CorridaPageCollection');
	var PageCorridaTemplate = require('text!views/corrida/tpl/PageCorridaTemplate.html');

	// Filter import
	var ModalAtendente = require('views/modalComponents/AtendenteModal');
	var ModalMotorista = require('views/modalComponents/MotoristaModal');
	var ModalEmpresa = require('views/modalComponents/EmpresaModal');
	var ModalCliente = require('views/modalComponents/ClienteModal');
	// End of "Import´s" definition
	var PageCorrida = Marionette.LayoutView.extend({
		template: _.template(PageCorridaTemplate),
		regions: {
			gridRegion: '#grid',
			counterRegion: '#counter',
			paginatorRegion: '#paginator',
			modalAtendenteRegion: '#atendenteModal',
			modalMotoristaRegion: '#motoristaModal',
			modalEmpresaRegion: '#empresaModal',
			modalClienteRegion: '#clienteModal',
		},
		events: {
			'click 	#reset': 'resetCorrida',
			'click #searchAtendenteModal': 'showModalAtendente',
			'click #searchMotoristaModal': 'showModalMotorista',
			'click #searchEmpresaModal': 'showModalEmpresa',
			'click #searchClienteModal': 'showModalCliente',
			'keypress': 'treatKeypress',
			'click 	.search-button': 'searchCorrida',
			'click .show-advanced-search-button': 'toggleAdvancedForm',
		},
		ui: {
			inputAtendenteId: '#inputAtendenteId',
			inputAtendenteNome: '#inputAtendenteNome',
			inputMotoristaId: '#inputMotoristaId',
			inputMotoristaNome: '#inputMotoristaNome',
			inputEmpresaId: '#inputEmpresaId',
			inputEmpresaContrato: '#inputEmpresaContrato',
			inputClienteId: '#inputClienteId',
			inputClienteNome: '#inputClienteNome',
			inputTelefone: '#inputTelefone',
			form: '#formCorridaFilter',
			advancedSearchForm: '.advanced-search-form',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue', 
		},
		toggleAdvancedForm: function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},
		treatKeypress: function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchCorrida();
			}
		},
		initialize: function() {
			var that = this;
			this.corridas = new CorridaPageCollection();
			this.grid = new Backgrid.Grid({
				className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns: this.getColumns(),
				emptyText: "Sem registros",
				collection: this.corridas
			});
			this.grid.render().sort('id', 'descending');
			this.counter = new Counter({
				collection: this.corridas,
			});
			this.paginator = new Backgrid.Extension.Paginator({
				columns: this.getColumns(),
				collection: this.corridas,
				className: ' paging_simple_numbers',
				uiClassName: 'pagination',
			});
			this.corridas.getFirstPage({
				success: function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid corrida');
				},
				error: function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
			this.modalAtendente = new ModalAtendente({
				onSelectModel: function(model) {
					that.onSelectAtendente(model);
				},
			});
			this.modalMotorista = new ModalMotorista({
				onSelectModel: function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.modalEmpresa = new ModalEmpresa({
				onSelectModel: function(model) {
					that.onSelectEmpresa(model);
				},
			});
			this.modalCliente = new ModalCliente({
				onSelectModel: function(model) {
					that.onSelectCliente(model);
				},
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.ui.inputTelefone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.modalAtendenteRegion.show(this.modalAtendente);
				this.modalMotoristaRegion.show(this.modalMotorista);
				this.modalEmpresaRegion.show(this.modalEmpresa);
				this.modalClienteRegion.show(this.modalCliente);
				this.checkButtonAuthority();
			});
		},
		searchCorrida: function() {
			var that = this;
			this.corridas.filterQueryParams = {
				id: util.escapeById('inputId'),
				cliente: util.escapeById('inputClienteId'),
				motorista: util.escapeById('inputMotoristaId'),
				empresa: util.escapeById('inputEmpresaId'),
				statusCorrida: util.escapeById('inputStatusCorrida'),
				atendente: util.escapeById('inputAtendenteId'),
				dataCorrida: util.escapeById('inputDataCorrida'),
				telefone: util.escapeById('inputTelefone'),
			}
			this.corridas.fetch({
				success: function(_coll, _resp, _opt) {
					console.info('Consulta para o grid corrida');
				},
				error: function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete: function() {},
			})
		},
		resetCorrida: function() {
			this.ui.form.get(0).reset();
			this.corridas.reset();
			util.clear('inputAtendenteId');
			util.clear('inputMotoristaId');
			util.clear('inputEmpresaId');
			util.clear('inputClienteId');
		},
		getColumns: function() {
			var that = this;
			var columns = [{
				name: "id",
				label: "N° OS",
				accordion: false,
				editable: false,
				sortable: true,
				cell : Backgrid.ExpandableCell.extend({
					className : 'detail-col',
					fieldName: 'id',
				}),
			    expand: function (el, model) {
			      that.showDetails(el, model);
			    }
			}, {
				name: "cliente.nome",
				editable: false,
				sortable: true,
				label: "Cliente",
				cell: CustomStringCell.extend({
					fieldName: 'cliente.nome',
				}),
			}, {
				name: "telefone",
				editable: false,
				sortable: true,
				label: "Telefone",
				cell: "string",
			}, {
				name: "motorista.viatura",
				editable: false,
				sortable: true,
				label: "Motorista",
				cell: CustomStringCell.extend({
					fieldName: 'motorista.viatura',
				}),
			}, {
				name: "empresa.contrato",
				editable: false,
				sortable: true,
				label: "Contrato",
				cell: CustomStringCell.extend({
					fieldName: 'empresa.contrato',
				}),
			}, {
				name: "statusCorrida",
				editable: false,
				sortable: true,
				label: "Status",
				cell: "string",
				formatter: _.extend({}, Backgrid.CellFormatter.prototype, {
					fromRaw: function(rawValue, model) {
						if (rawValue == 'A') return "Agendada";
						if (rawValue == 'C') return "A caminho";
						if (rawValue == 'D') return "Disponível";
						if (rawValue == 'E') return "Em andamento";
						if (rawValue == 'F') return "Finalizada";
						if (rawValue == 'K') return "Cancelada";
						if (rawValue == 'P') return "Pendente";
						if (rawValue == 'R') return "Reenviada";
					}
				})
			}, {
				name: "dataCorrida",
				editable: false,
				sortable: true,
				label: "Data Solicitação",
				cell: CustomStringCell.extend({
					fieldName: 'dataCorrida',
				}),
			}, {
				name: "acoes",
				label: "Ações(Editar, Deletar)",
				sortable: false,
				cell: GeneralActionsCell.extend({
					buttons: that.getCellButtons(),
					context: that,
				})
			}];
			return columns;
		},
		getCellButtons: function() {
			var that = this;
			var buttons = [];
			buttons.push({
				id: 'reenvio_button',
				type: 'primary',
				icon: 'fa-repeat',
				hint: 'Reenviar Corrida',
				onClick: that.reenvioModel,
			}, {
				id: 'edita_ficha_button',
				type: 'primary',
				icon: 'icon-pencil fa-pencil',
				hint: 'Editar Corrida',
				onClick: that.editModel,
			}, { 
				id: 'cancel_button',
				type: 'danger',
				icon: 'fa-times',
				hint: 'Cancelar Corrida',
				onClick: that.cancelaModel,
//			}, {
//				id: 'delete_button',
//				type: 'danger',
//				icon: 'icon-trash fa-trash',
//				hint: 'Remover Corrida',
//				onClick: that.deleteModel,
			});
			return buttons;
		},
		deleteModel: function(model) {
			var that = this;
			var modelTipo = new CorridaModel({
				id: model.id,
			});
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success: function() {
							that.corridas.remove(model);
							util.showSuccessMessage('Corrida removido com sucesso!');
						},
						error: function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro', _resp);
						}
					});
				}
			});
		},
		cancelaModel: function(model) {
			var corrida = model;
			console.log(corrida.get('statusCorrida'));
			if(corrida.get('statusCorrida') != 'K' && corrida.get('statusCorrida') != 'F'){
				util.Bootbox.confirm("Tem certeza que deseja CANCELAR a corrida N° OS [ " + model.get('id') + " ] ?", function(yes) {
					if (yes) {
						corrida.set('statusCorrida', 'K');
						corrida.save({}, {
							success: function(_model, _resp, _options) {
								util.showSuccessMessage('Corrida cancelada com sucesso!');
								util.goPage('app/corridas');
							},
							error: function(_model, _resp, _options) {
								util.showErrorMessage('Problema ao cancelar corrida! ' + _resp.responseJSON.legalMessage, _resp.responseJSON.legalMessage);
							}
						});
					}
				});
			}else{
				util.showMessage('error', 'A corrida não pode ser cancelada caso esteja finalizada ou cancelada!');
			}
		},

		editModel: function(model) {
			util.goPage("app/editCorrida/" + model.get('id'));
		},
		
		reenvioModel: function(model) {
			var corrida = model;
			if(corrida.get('statusCorrida') == 'D' || corrida.get('statusCorrida') == 'A'){
				util.Bootbox.confirm("Tem certeza que deseja REENVIAR a corrida N° OS [ " + model.get('id') + " ] ?", function(yes) {
					if (yes) {
						corrida.set('statusCorrida', 'R');
						corrida.save({}, {
							success: function(_model, _resp, _options) {
								util.showSuccessMessage('Corrida reenviada com sucesso!');
								util.goPage('app/corridas');
							},
							error: function(_model, _resp, _options) {
								util.showErrorMessage('Problema ao reenviar corrida! ' + _resp.responseJSON.legalMessage, _resp.responseJSON.legalMessage);
							}
						});
					}
				});
			}else{
				util.showMessage('error', 'A corrida só pode ser reenviada caso esteja disponível ou agendada!');
			}
		},
		
		showModalAtendente: function() {
			this.modalAtendente.showPage();
		},
		onSelectAtendente: function(atendente) {
			this.modalAtendente.hidePage();
			this.ui.inputAtendenteId.val(atendente.get('id'));
			this.ui.inputAtendenteNome.val(atendente.get('nome'));
		},
		showModalMotorista: function() {
			this.modalMotorista.showPage();
		},
		onSelectMotorista: function(motorista) {
			this.modalMotorista.hidePage();
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));
		},
		showModalEmpresa: function() {
			this.modalEmpresa.showPage();
		},
		onSelectEmpresa: function(empresa) {
			this.modalEmpresa.hidePage();
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
		},
		showModalCliente: function() {
			this.modalCliente.showPage();
		},
		onSelectCliente: function(cliente) {
			this.modalCliente.hidePage();
			this.ui.inputClienteId.val(cliente.get('id'));
			this.ui.inputClienteNome.val(cliente.get('nome'));
		},
		// vitoriano : chunck : check grid buttons authority
		checkGridButtonAuthority: function() {
			var that = this;
			var buttons = [];
			$.grep(roles, function(e) {
				
				if (e.authority == 'ROLE_CORRIDA_EDITAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'reenvio_button',
						type: 'primary',
						icon: 'fa-repeat',
						hint: 'Reenviar Corrida',
						onClick: that.reenvioModel,
					}, {
						id: 'edita_ficha_button',
						type: 'primary',
						icon: 'icon-pencil fa-pencil',
						hint: 'Editar Corrida',
						onClick: that.editModel,
					}, { 
						id: 'cancel_button',
						type: 'danger',
						icon: 'fa-times',
						hint: 'Cancelar Corrida',
						onClick: that.cancelaModel,
					});
				}
//				if (e.authority == 'ROLE_CORRIDA_DELETAR' || e.authority == 'ROLE_ADMIN') {
//					buttons.push({
//						id: 'delete_button',
//						type: 'danger',
//						icon: 'fa-trash',
//						hint: 'Remover Corrida',
//						onClick: that.deleteModel,
//					});
//				}
			})
			return buttons;
		},
		
		showDetails : function(el, model) {
			var corrida = model.toJSON();
			
			var statusCliente = corrida.cliente.inativo ? "Inativo" : "Ativo";
			console.log(corrida);

			var html = "<div class='well'>" +
				"<h4><b>Cliente</b></h4>" +
				"<b>Nome</b>: " + corrida.cliente.nome + "<br>" +
				"<b>Telefone</b>: " + corrida.telefone + "<br>" +
				"<b>Status</b>: " + statusCliente + "<br>";
			
			if(corrida.cliente.observacao)
				html += "<b>Observação</b>: " + corrida.cliente.observacao + "<br>"; 
			
			html += "<b>Tipo</b>: " + corrida.cliente.tipo + "<br><br>"; 
			
			if(corrida.empresa){
				html += "<h4><b>Empresa</b></h4>" +
				"<b>Empresa</b>: " + corrida.empresa.nomeFantasia + "<br>" +
				"<b>Contrato</b>: " + corrida.empresa.contrato + "<br><br>";
			}
				
			html += "<h4><b>Corrida</b></h4>" +
				"<b>Endereço de Partida</b>: " + corrida.enderecoCorridas[0].enderecoFormatado +  "<br>";

			for (var int = 1; int < corrida.enderecoCorridas.length; int++) {
				html += "<b>Endereço de Destino</b>: " + corrida.enderecoCorridas[int].enderecoFormatado +  "<br>";
			}
			html += "<b>Valor</b>: R$ " + corrida.valor + "<br>" + 
				"<b>Forma de Pagamento</b>: " + corrida.tipoPagamento + "<br>"; 

			if(corrida.observacao != " ")
				html += "<b>Observação da corrida</b>: " + corrida.observacao + "<br>";
			
			if(corrida.motoqueiro){
				html += "<b>Motoqueiro</b>: " + corrida.motoqueiro.nome + "<br>"; 
					"<b>Viatura</b>: " + corrida.motoqueiro.viatura + "<br>";
			}
			html += "</div>";
			$(el).append($('<div>').html(html));
		},
		
		// vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_CORRIDA_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
	});
	return PageCorrida;
});