/* generated: 25/10/2016 10:24:45 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var SuggestGmapBox = require('views/components/SuggestGmapBox');
	var TemplateFormCorridas = require('text!views/corrida/tpl/FormCorridaTemplate.html');
	var CorridaModel = require('models/CorridaModel');
	var EnderecoCorridaModel = require('models/EnderecoCorridaModel');
	var EnderecoFavoritoModel = require('models/EnderecoFavoritoModel');
	var CorridaEnderecoModel = require('models/CorridaEnderecoModel');
	var CorridaCollection = require('collections/CorridaCollection');
	var MotoristaCollection = require('collections/MotoristaCollection');
	var ModalAtendente = require('views/modalComponents/AtendenteModal');
	var ModalMotorista = require('views/modalComponents/MotoristaModal');
	var CheckGroup = require('views/components/CheckGroup');
	var RadioGroup = require('views/components/RadioGroup');
	var GMap = require('views/components/GMap');
	var BaseCollection = require('collections/BaseCollection');
	var ClienteCollection = require('collections/ClienteCollection');
	var ClienteEnderecoCollection = require('collections/ClienteEnderecoCollection');
	var EnderecoCorridaCollection = require('collections/EnderecoCorridaCollection');
	var FrameEnderecoCorrida = require('views/corrida/FrameEnderecoCorrida');
	var FrameEnderecoPartida = require('views/corrida/FrameEnderecoPartida');
	var FrameTarifasAdicionaisCorrida = require('views/corrida/FrameTarifasAdicionaisCorrida');
	var ModalConfirmacaoCorrida = require('views/corrida/ModalConfirmacaoCorrida');
	// End of "Import´s" definition
	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
	// BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################
	var FormCorridas = Marionette.LayoutView.extend({
		template : _.template(TemplateFormCorridas),
		regions : {
			modalAtendenteRegion : '#atendenteModal',
			modalConfirmacaoCorridaRegion : '#modalConfirmacaoCorrida',
			modalMotoristaRegion : '#motoristaModal',
			enderecoPartidaRegion : '.endereco-partida-container',
			enderecoDestinoRegion : '.endereco-destino-container',
			tarifasAdicionaisRegion : '.tarifas-adicionais-container',
		},
		events : {
			'click 	.save' : 'save',
			'click 	.adiciona-destino' : 'adicionaDestino',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click .mostrar-motoqueiros' : 'mostraMotoqueiros',
			'change	#inputFone' : 'buscaCliente',
			'click #searchAtendenteModal' : 'showModalAtendente',
			'click #searchMotoristaModal' : 'showModalMotorista',
		},
		ui : {
			inputId : '#inputId',
			inputDataCorrida : '#inputDataCorrida',
			groupInputDataCorrida : '#groupInputDataCorrida',
			inputDataInicio : '#inputDataInicio',
			groupInputDataInicio : '#groupInputDataInicio',
			inputNome : '#inputNome',
			consultaContainer : '.consulta-container',
			clienteContainer : '.cliente-container',
			inputFone : '#inputFone',
			inputObservacaoCliente : '#inputObservacaoCliente',
			inputComplemento : '#inputComplemento',
			inputFoneRetorno : '#inputFoneRetorno',
			inputTipoPagamento : '#inputTipoPagamento',
			inputEnderecoDestino : '#inputEnderecoDestino',
			inputEnderecoPartida : '#inputEnderecoPartida',
			inputValor : '#inputValor',
			inputStatusCorrida : '#inputStatusCorrida',
			inputStatusCorridaExibicao : '#inputStatusCorridaExibicao',
			inputDataAgendamento : '#inputDataAgendamento',
			groupInputTipoPagamento : '.groupInputTipoPagamento',
			groupExigencias : '.groupExigencias',
			inputEnderecoPartida : '#inputEnderecoPartida',
			inputEnderecoDestino : '#inputEnderecoDestino',
			inputAtendenteId : '#inputAtendenteId',
			inputAtendenteNome : '#inputAtendenteNome',
			inputMotoristaId : '#inputMotoristaId',
			inputMotoristaViatura : '#inputMotoristaViatura',
			empresaContainer : '.empresa-container',
			empresaInativaContainer : '.empresa-container-inativa',
			labelEmpresaFuncionario : '.label-Empresa-funcionario',
			inputEmpresaNome : '#inputEmpresaNome',
			inputEmpresaNomeFuncionario : '#inputEmpresaNomeFuncionario',
			inputEmpresaContrato : '#inputEmpresaContrato',
			mapa : '#map',
			form : '#formCorrida',
			newButton : 'a#new',
			saveButton : 'a#save',
			saveContinueButton : 'a#saveContinue',
		},
		initialize : function() {
			var that = this;
			this.motorista = this.model.get('motorista');
			this.clienteEnderecos = new ClienteEnderecoCollection();
			this.enderecoCorridas = new EnderecoCorridaCollection(this.model.get('enderecoCorridas'));
			this.enderecoDestinoCorridaCollection = new EnderecoCorridaCollection(this.enderecoCorridas.slice(1));
			this.tarifasAdicionais = new FrameTarifasAdicionaisCorrida({
				enderecos : that.enderecoDestinoCorridaCollection,
				informacaoTarifas : that.model.get('informacaoTarifas'),
				onDefinePrecos : function(valorCorrida) {
					that.ui.inputValor.val(valorCorrida);
				}
			});
			this.modalConfirmacaoCorrida = new ModalConfirmacaoCorrida({
				model : this.model,
			});
			var enderecoOrigem = that.enderecoCorridas.at(0) && that.enderecoCorridas.at(0).toJSON() || {};
			enderecoOrigem.corrida = this.model.toJSON();
			this.enderecoPartida = new FrameEnderecoPartida({
				enderecosFavoritos : that.clienteEnderecos,
				model : new EnderecoCorridaModel(enderecoOrigem),
				onSelectEndereco : function(endereco) {
					that.gMap.clearMarkers();
					that.gMap.addMarker(endereco.latlong, endereco.endereco);
					that.desenhaRota();
					// Somente para que o frame de tarifas saiba qual o endereço
					// partida para calcular a maior perna
					that.tarifasAdicionais.setEnderecoPartida(endereco);
				},
				onSelectComoFavorito : function(jsonEndereco) {
					var model = new EnderecoFavoritoModel(jsonEndereco);
					if (that.empresa || that.cliente) {
						model.set('id', null);
						model.set('empresa', that.empresa);
						model.set('endereco', that.empresa);
						model.set('cliente', that.cliente);
						model.set('favorito', true);
						// Pequeno arranjo para corrigir a nomenclatura do
						// endereço/logradouro
						model.set('endereco', jsonEndereco.logradouro);
						model.save({}, {
							success : function(model, resp, xhr) {
								console.log(model, resp, xhr);
							},
							error : function(model, resp, xhr) {
								console.error(model, resp, xhr);
							}
						})
					}
				},
			})
			this.enderecosDestino = new FrameEnderecoCorrida({
				enderecosFavoritos : that.clienteEnderecos,
				model : new EnderecoCorridaModel(),
				enderecos : that.enderecoDestinoCorridaCollection,
			})
			this.modalMotorista = new ModalMotorista({
				onSelectModel : function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.enderecoDestinoCorridaCollection.on('add', this.desenhaRota, this);
			this.enderecoDestinoCorridaCollection.on('sort', this.desenhaRota, this);
			this.enderecoDestinoCorridaCollection.on('remove', this.removeDestino, this);
			this.on('show', function() {
				// adição das regioes
				this.empresa = this.model.get('empresa');
				this.cliente = this.model.get('cliente');
				this.enderecoPartidaRegion.show(this.enderecoPartida);
				this.modalConfirmacaoCorridaRegion.show(this.modalConfirmacaoCorrida);
				this.enderecoDestinoRegion.show(this.enderecosDestino);
				this.tarifasAdicionaisRegion.show(this.tarifasAdicionais);
				this.modalMotoristaRegion.show(this.modalMotorista);
				// this.enderecosDestinoRegion.show(this.enderecosCorridaDestino);
				// Formatação dos telefones
				this.ui.inputFone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.ui.inputFoneRetorno.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				// formatação dos campos de data
				this.ui.groupInputDataCorrida.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataCorrida.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataCorrida.mask('99/99/9999 99:99');
				this.ui.groupInputDataInicio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataInicio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataInicio.mask('99/99/9999 99:99');
				this.ui.inputTipoPagamento.formatNumber(2);
				this.ui.inputValor.formatNumber(2);
				// Radios e checkgroups
				this.tipoPagamento = new RadioGroup({
					container : this.ui.groupInputTipoPagamento,
				});
				this.tipoPagamento.setValue(this.model.get('tipoPagamento'));
				// Manipuações que envolvem mapa
				this.gMap = new GMap({
					mapElement : this.ui.mapa,
				})
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
				this.checkButtonAuthority();
				// this.mostraMotoqueiros();
				if (this.model.get('id'))
					this.consultaCorrida();
				this.statusCorrida();
			});
		},
		/*
		 * Implementar tratamento para os outros status de acordo com a demanda.
		 */
		statusCorrida : function(status) {
			var that = this;
			if (!that.model.get('id'))
				that.model.set('statusCorrida', 'P');
			status = that.model.get('statusCorrida');
			switch (status) {
			case 'A':
				status = 'Agendada'
				break;
			case 'C':
				status = 'A caminho'
				break;
			case 'D':
				status = 'Disponível'
				break;
			case 'E':
				status = 'Em andamento'
				break;
			case 'F':
				status = 'Finalizada'
				break;
			case 'K':
				status = 'Cancelada'
				break;
			case 'P':
				status = 'Pendente'
				break;
			case 'R':
				status = 'Reenviada'
				break;
			}
			that.ui.inputStatusCorridaExibicao.val(status);
		},
		/**
		 * Solução temporária para consultar as corridas criadas.
		 */
		consultaCorrida : function() {
			if (this.model.get('empresa')) {
				this.ui.empresaContainer.show("slow")
				this.ui.labelEmpresaFuncionario.text("Funcionário");
			} else {
				this.ui.clienteContainer.show("slow")
				this.ui.labelEmpresaFuncionario.text("Cliente");
			}
			var jsonModel = this.model.toJSON();

			var status = this.model.get('statusCorrida');
			if (status == 'C' || status == 'E' || status == 'F' || status == 'K' || status == 'P' || status == 'R') {
				this.ui.saveButton.hide();
				this.ui.saveContinueButton.hide();
			}
			this.ui.inputFone.val(jsonModel.telefone);
			this.ui.consultaContainer.show("slow")
			// this.ui.inputDataInicio.attr('readonly', true);
			this.ui.inputFone.attr('readonly', true);
			this.desenhaRota();
		},
		mostraMotoqueiros : function() {
			var that = this;
			var motoristas = new MotoristaCollection();
			motoristas.motoristasOnline({
				success : function(col, resp, xhr) {
					that.gMap.clear();
					that.gMap.addMotos(col.toJSON());
					that.desenhaRota();
				},
			})
		},
		saveAndContinue : function() {
			this.save(true)
		},
		search : function geocodeAddress() {
		},
		save : function(continua) {
			var that = this;
			var corrida = that.getModel();
			if (this.isValid() && that.enderecoPartida.isValid()) {
				if (corrida.get('enderecoCorridas').length > 1) {
					corrida.save({}, {
						success : function(_model, _resp, _options) {
							util.showSuccessMessage('Corrida salva com sucesso!');
							// that.modalConfirmacaoCorrida.showInPage(_model);
							that.clearForm();
							if (continua != true)
								util.goPage('app/corridas');
							// else that.mostraMotoqueiros();
						},
						error : function(_model, _resp, _options) {
							util.showErrorMessage('Problema ao salvar registro! ' + _resp.responseJSON.legalMessage, _resp.responseJSON.legalMessage);
						}
					});
				} else {
					util.showMessage('error', 'A corrida deve ter no mínimo uma partida e um destino!');
				}
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputDataCorrida');
			util.clear('inputDataInicio');
			util.clear('inputTipoPagamento');
			util.clear('inputValor');
			util.clear('inputFone');
			util.clear('inputFoneRetorno');
			util.clear('inputComplemento');
			util.clear('inputStatusCorrida');
			util.clear('inputStatusCorridaExibicao');
			util.clear('inputAtendenteId');
			util.clear('inputAtendenteNome');
			util.clear('inputMotoristaId');
			util.clear('inputBau');
			util.clear('inputPadronizada');
			util.clear('inputMotoGrande');
			util.clear('inputMotoristaId');
			util.clear('inputMotoristaViatura');
			util.clear('inputEmpresaNomeFuncionario');
			util.clear('inputEmpresaNome');
			util.clear('inputEmpresaContrato');
			this.tipoPagamento.clear();
			this.ui.inputFone.val('(85)');
			this.enderecosDestino.clearForm();
			this.enderecoPartida.clearForm();
			this.enderecoDestinoCorridaCollection.reset();
			this.tarifasAdicionais.clear();
			this.gMap.clear();
		},
		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},
		getModel : function() {
			var that = this;
			var corrida = that.model;
			corrida.set({
				id : util.escapeById('inputId') || null,
				bau : util.escapeById('inputBau'),
				padronizada : util.escapeById('inputPadronizada'),
				motoGrande : util.escapeById('inputMotoGrande'),
				dataCorrida : util.escapeById('inputDataInicio'),
				dataAgendamento : util.escapeById('inputDataInicio'),
				nome : util.escapeById('inputNome'),
				telefone : util.escapeById('inputFone'),
				foneRetorno : util.escapeById('inputFoneRetorno'),
				complemento : util.escapeById('inputComplemento'),
				observacao : this.enderecoPartida.ui.inputObservacao.val(),
				tipoPagamento : that.tipoPagamento.getValue(),
				motorista : that.motorista,
				empresa : that.empresa,
				cliente : that.cliente,
				enderecoCorridas : that.getEnderecos(),
				informacaoTarifas : that.tarifasAdicionais.getTarifas(),
				valor : util.escapeById('inputValor', true),
			});
			return corrida;
		},
		getEnderecos : function() {
			var enderecoPartida = this.enderecoPartida.getJsonValue();
			// Criar uma cópia para garantir a integridade da coleção que
			// renderiza o grid de endereços destino
			var copiaEnderecos = new EnderecoCorridaCollection();
			copiaEnderecos.add(enderecoPartida);
			copiaEnderecos.add(this.enderecoDestinoCorridaCollection.toJSON());
			return copiaEnderecos.toJSON();
		},
		adicionaDestino : function() {
			this.enderecoDestinoCorridaCollection.add(new EnderecoCorridaModel());
		},
		onSelectEnderecoPartida : function(jsonEndereco) {
			this.gMap.addMarker(jsonEndereco.latlong, jsonEndereco.endereco);
		},
		showModalMotorista : function(motorista) {
			this.modalMotorista.showPage();
		},
		onSelectMotorista : function(motorista) {
			this.motorista = motorista.toJSON();
			this.modalMotorista.hidePage();
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaViatura.val(motorista.get('viatura'));
		},
		buscaCliente : function() {
			var that = this;
			var consumidorCorrida = new CorridaModel();
			that.empresa = null;
			that.cliente = null;
			that.ui.inputEmpresaNomeFuncionario.val('');
			that.ui.inputEmpresaNome.val('');
			that.ui.inputEmpresaContrato.val('');
			that.ui.inputNome.val('');
			that.ui.inputObservacaoCliente.val('');
			that.ui.clienteContainer.hide()
			that.ui.empresaContainer.hide()
			consumidorCorrida.getClientCorridaPorTelefone({
				success : function(_model, resp, opt) {
					if (consumidorCorrida.get('tipo') === 'FUNCIONARIO') {
						var empresa = consumidorCorrida.get('empresa');
						var funcionario = consumidorCorrida.get('cliente');

						that.ui.inputEmpresaNomeFuncionario.val(funcionario.nome);
						that.ui.inputEmpresaNome.val(empresa.nomeFantasia);
						that.ui.inputEmpresaContrato.val(empresa.contrato);
						that.empresa = empresa;
						that.cliente = funcionario;
						that.ui.labelEmpresaFuncionario.text("Funcionário");
						that.ui.clienteContainer.hide("slow")
						that.ui.empresaContainer.show("slow")
						that.enderecoPartida.setCliente(funcionario);
						if (empresa.statusEmpresa == 'Inativa')
							that.ui.empresaInativaContainer.hide("slow")
						if (empresa.observacaoCentral)
							util.alert(empresa.observacaoCentral);
					} else if (consumidorCorrida.get('tipo') === 'CLIENTE') {
						var cliente = consumidorCorrida.get('cliente');
						that.ui.inputNome.val(cliente.nome);
						that.ui.inputObservacaoCliente.val(cliente.observacao);
						that.cliente = cliente;
						that.ui.empresaContainer.hide("slow")
						that.ui.clienteContainer.show("slow");
						that.enderecoPartida.setCliente(cliente);
					} else {
						util.showErrorMessage(that.ui.inputFone.val() + ' não encontrado', null, 'mensagem-busca');
						that.ui.inputFone.val('(85)');
						that.ui.inputFone.focus();
					}
				},
				error : function(model, resp, opt) {
					console.error('Error na busca do cliente');
				},
				data : {
					telefone : this.ui.inputFone.val(),
				}
			})
		},
		addMarker : function(modelEndereco) {
			// this.gMap.addMarker(modelEndereco.get('latlong'),
			// modelEndereco.get('endereco'));
		},
		desenhaRota : function(modelEndereco) {
			// this.gMap.addMarker(modelEndereco.get('latlong'),
			// modelEndereco.get('endereco'));
			var enderecos = this.getEnderecos()
			var latlongs = []
			_.each(enderecos, function(endereco) {
				latlongs.push(new google.maps.LatLng(endereco.latitude, endereco.longitude))
			})
			this.gMap.desenhaRota(latlongs);
		},
		removeMarker : function(modelEndereco) {
			this.gMap.removeMarker(modelEndereco.get('latlong'), modelEndereco.get('endereco'));
		},
		removeDestino : function(modelEndereco) {
			this.desenhaRota();
		},
		checkButtonAuthority : function() {
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton;
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_EMPRESA_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
				if (e.authority == 'ROLE_EMPRESA_EDITAR' || e.authority == 'ROLE_ADMIN') {
					saveButton.show();
				}
			})
		},
	});
	return FormCorridas;
});