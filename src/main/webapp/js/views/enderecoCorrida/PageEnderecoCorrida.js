/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var EnderecoCorridaModel = require('models/EnderecoCorridaModel');
	var EnderecoCorridaCollection = require('collections/EnderecoCorridaCollection');
	var EnderecoCorridaPageCollection = require('collections/EnderecoCorridaPageCollection');
	var PageEnderecoCorridaTemplate = require('text!views/enderecoCorrida/tpl/PageEnderecoCorridaTemplate.html');

	// Filter import
	var ModalCorrida = require('views/modalComponents/CorridaModal');

	// End of "Import´s" definition

	var PageEnderecoCorrida = Marionette.LayoutView.extend({
		template : _.template(PageEnderecoCorridaTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			modalCorridaRegion : '#corridaModal',
		},

		events : {
			'click 	#reset' : 'resetEnderecoCorrida',
			'click #searchCorridaModal' : 'showModalCorrida',
			'keypress' : 'treatKeypress',

			'click 	.search-button' : 'searchEnderecoCorrida',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},

		ui : {
			inputCorridaId : '#inputCorridaId',
			inputCorridaNome : '#inputCorridaNome',
			form : '#formEnderecoCorridaFilter',
			advancedSearchForm : '.advanced-search-form',
		},

		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		treatKeypress : function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchEnderecoCorrida();
			}
		},

		initialize : function() {
			var that = this;

			this.enderecoCorridas = new EnderecoCorridaPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.enderecoCorridas
			});

			this.counter = new Counter({
				collection : this.enderecoCorridas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.enderecoCorridas,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.enderecoCorridas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid enderecoCorrida');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
			this.modalCorrida = new ModalCorrida({
				onSelectModel : function(model) {
					that.onSelectCorrida(model);
				},
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.modalCorridaRegion.show(this.modalCorrida);

			});
		},

		searchEnderecoCorrida : function() {
			var that = this;

			this.enderecoCorridas.filterQueryParams = {
				corrida : util.escapeById('inputCorridaId'),
			}
			this.enderecoCorridas.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid enderecoCorrida');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {

				},
			})
		},
		resetEnderecoCorrida : function() {
			this.ui.form.get(0).reset();
			this.enderecoCorridas.reset();
			util.clear('inputCorridaId');
		},

		getColumns : function() {
			var that = this;
			var columns = [ {
				name : "corrida.nome",
				editable : false,
				sortable : true,
				label : "Corrida",
				cell : CustomStringCell.extend({
					fieldName : 'corrida.nome',
				}),
			}, {
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},

		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Endereco corrida',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Endereco corrida',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;

			var modelTipo = new EnderecoCorridaModel({
				id : model.id,
			});

			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.enderecoCorridas.remove(model);
							util.showSuccessMessage('Endereco corrida removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro', _resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editEnderecoCorrida/" + model.get('id'));
		},

		showModalCorrida : function() {
			this.modalCorrida.showPage();
		},

		onSelectCorrida : function(corrida) {
			this.modalCorrida.hidePage();
			this.ui.inputCorridaId.val(corrida.get('id'));
			this.ui.inputCorridaNome.val(corrida.get('nome'));
		},

	});

	return PageEnderecoCorrida;
});
