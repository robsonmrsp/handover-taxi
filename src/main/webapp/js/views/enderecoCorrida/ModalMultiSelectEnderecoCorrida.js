/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var EnderecoCorridaPageCollection = require('collections/EnderecoCorridaPageCollection');
	var ModalMultiSelectEnderecoCorridaTemplate = require('text!views/enderecoCorrida/tpl/ModalMultiSelectEnderecoCorridaTemplate.html');
	// End of "Import´s" definition

	var ModalEnderecoCorridas = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectEnderecoCorridaTemplate),

		regions : {
			gridRegion : '#grid-enderecoCorridas-modal',
			paginatorRegion : '#paginator-enderecoCorridas-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoEnderecoCorridas = this.collection;
			
			this.enderecoCorridas = new EnderecoCorridaPageCollection();
			this.enderecoCorridas.on('fetched', this.endFetch, this);
			this.enderecoCorridas.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.enderecoCorridas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.enderecoCorridas,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.enderecoCorridas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid enderecoCorrida');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoEnderecoCorridas.add(model)
			else
				this.projetoEnderecoCorridas.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.enderecoCorridas.each(function(model) {
				if (that.projetoEnderecoCorridas.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "endereco",
				editable : false,
				sortable : false,
				label 	 : "Endereco",
				cell 	 : "string",
			}, 
			{
				name : "numero",
				editable : false,
				sortable : false,
				label 	 : "Numero",
				cell 	 : "string",
			}, 
			{
				name : "complemento",
				editable : false,
				sortable : false,
				label 	 : "Complemento",
				cell 	 : "string",
			}, 
			{
				name : "bairro",
				editable : false,
				sortable : false,
				label 	 : "Bairro",
				cell 	 : "string",
			}, 
			{
				name : "uf",
				editable : false,
				sortable : false,
				label 	 : "Uf",
				cell 	 : "string",
			}, 
			{
				name : "cidade",
				editable : false,
				sortable : false,
				label 	 : "Cidade",
				cell 	 : "string",
			}, 
			{
				name : "cep",
				editable : false,
				sortable : false,
				label 	 : "Cep",
				cell 	 : "string",
			}, 
			{
				name : "referencia",
				editable : false,
				sortable : false,
				label 	 : "Referencia",
				cell 	 : "string",
			}, 
			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descricao",
				cell 	 : "string",
			}, 
			{
				name : "ordem",
				editable : false,
				sortable : false,
				label 	 : "Ordem",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "latitude",
				editable : false,
				sortable : false,
				label 	 : "Latitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "longitude",
				editable : false,
				sortable : false,
				label 	 : "Longitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "enderecoFormatado",
				editable : false,
				sortable : false,
				label 	 : "Endereco formatado",
				cell 	 : "string",
			}, 
			{
				name : "origem",
				editable : false,
				sortable : false,
				label 	 : "Origem",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalEnderecoCorridas;
});
