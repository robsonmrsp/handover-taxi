/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormEnderecoCorridas = require('text!views/enderecoCorrida/tpl/FormEnderecoCorridaTemplate.html');
	var EnderecoCorridaModel = require('models/EnderecoCorridaModel');
	var EnderecoCorridaCollection = require('collections/EnderecoCorridaCollection');
	var ModalCorrida = require('views/modalComponents/CorridaModal');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormEnderecoCorridas = Marionette.LayoutView.extend({
		template : _.template(TemplateFormEnderecoCorridas),

		regions : {
			modalCorridaRegion : '#corridaModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click #searchCorridaModal' : 'showModalCorrida',
		},
		
		ui : {
			inputId : '#inputId',
			inputEndereco : '#inputEndereco',
			inputNumero : '#inputNumero',
			inputComplemento : '#inputComplemento',
			inputBairro : '#inputBairro',
			inputUf : '#inputUf',
			inputCidade : '#inputCidade',
			inputCep : '#inputCep',
			inputReferencia : '#inputReferencia',
			inputDescricao : '#inputDescricao',
			inputOrdem : '#inputOrdem',
			inputLatitude : '#inputLatitude',
			inputLongitude : '#inputLongitude',
			inputEnderecoFormatado : '#inputEnderecoFormatado',
			inputOrigem : '#inputOrigem',
		
			inputCorridaId : '#inputCorridaId',
			inputCorridaNome : '#inputCorridaNome',
			form : '#formEnderecoCorrida',
		},

		initialize : function() {
			var that = this;
			this.modalCorrida = new ModalCorrida({
				onSelectModel : function(model) {
					that.onSelectCorrida(model);
				},
			});
			this.on('show', function() {
				this.modalCorridaRegion.show(this.modalCorrida);		
		
				this.ui.inputOrdem.formatNumber(2);
				this.ui.inputLatitude.formatNumber(2);
				this.ui.inputLongitude.formatNumber(2);
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var enderecoCorrida = that.getModel();

			if (this.isValid()) {
				enderecoCorrida.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Endereco corrida salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/enderecoCorridas');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputEndereco'); 
			util.clear('inputNumero'); 
			util.clear('inputComplemento'); 
			util.clear('inputBairro'); 
			util.clear('inputUf'); 
			util.clear('inputCidade'); 
			util.clear('inputCep'); 
			util.clear('inputReferencia'); 
			util.clear('inputDescricao'); 
			util.clear('inputOrdem'); 
			util.clear('inputLatitude'); 
			util.clear('inputLongitude'); 
			util.clear('inputEnderecoFormatado'); 
			util.clear('inputOrigem'); 
			util.clear('inputCorridaId');
			util.clear('inputCorridaNome');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var enderecoCorrida = that.model; 
			enderecoCorrida.set({
				id: util.escapeById('inputId') || null,
		    	endereco : util.escapeById('inputEndereco'), 
		    	numero : util.escapeById('inputNumero'), 
		    	complemento : util.escapeById('inputComplemento'), 
		    	bairro : util.escapeById('inputBairro'), 
		    	uf : util.escapeById('inputUf'), 
		    	cidade : util.escapeById('inputCidade'), 
		    	cep : util.escapeById('inputCep'), 
		    	referencia : util.escapeById('inputReferencia'), 
		    	descricao : util.escapeById('inputDescricao'), 
		    	ordem : util.escapeById('inputOrdem', true), 
		    	latitude : util.escapeById('inputLatitude', true), 
		    	longitude : util.escapeById('inputLongitude', true), 
		    	enderecoFormatado : util.escapeById('inputEnderecoFormatado'), 
		    	origem : util.escapeById('inputOrigem'), 
				corrida : that.modalCorrida.getJsonValue(),
			});
			return enderecoCorrida;
		},
		 		
		showModalCorrida : function() {
			// add more before the modal is open
			this.modalCorrida.showPage();
		},

		onSelectCorrida : function(corrida) {
			this.modalCorrida.hidePage();	
			this.ui.inputCorridaId.val(corrida.get('id'));
			this.ui.inputCorridaNome.val(corrida.get('nome'));		
		},
				
		
	});

	return FormEnderecoCorridas;
});