/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectEnderecoCorrida = require('views/enderecoCorrida/ModalMultiSelectEnderecoCorrida');
	var MultiSelectEnderecoCorridaTemplate = require('text!views/enderecoCorrida/tpl/MultiSelectEnderecoCorridaTemplate.html');

	var MultiSelectEnderecoCorrida = Marionette.LayoutView.extend({
		template : _.template(MultiSelectEnderecoCorridaTemplate),

		regions : {
			modalMultiSelectEnderecoCorridaRegion : '#modalMultiSelectEnderecoCorridas',
			gridEnderecoCorridasModalRegion : '#gridMultiselectEnderecoCorridas',
		},

		initialize : function() {
			var that = this;

			this.enderecoCorridas = this.collection;

			this.gridEnderecoCorridas = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.enderecoCorridas,
			});

			this.modalMultiSelectEnderecoCorrida = new ModalMultiSelectEnderecoCorrida({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectEnderecoCorridaRegion.show(that.modalMultiSelectEnderecoCorrida);
				that.gridEnderecoCorridasModalRegion.show(that.gridEnderecoCorridas);
			});
		},
		clear : function(){
			this.modalMultiSelectEnderecoCorrida.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "endereco",
				editable : false,
				sortable : false,
				label 	 : "Endereco",
				cell 	 : "string",
			}, 
			{
				name : "numero",
				editable : false,
				sortable : false,
				label 	 : "Numero",
				cell 	 : "string",
			}, 
			{
				name : "complemento",
				editable : false,
				sortable : false,
				label 	 : "Complemento",
				cell 	 : "string",
			}, 
			{
				name : "bairro",
				editable : false,
				sortable : false,
				label 	 : "Bairro",
				cell 	 : "string",
			}, 
			{
				name : "uf",
				editable : false,
				sortable : false,
				label 	 : "Uf",
				cell 	 : "string",
			}, 
			{
				name : "cidade",
				editable : false,
				sortable : false,
				label 	 : "Cidade",
				cell 	 : "string",
			}, 
			{
				name : "cep",
				editable : false,
				sortable : false,
				label 	 : "Cep",
				cell 	 : "string",
			}, 
			{
				name : "referencia",
				editable : false,
				sortable : false,
				label 	 : "Referencia",
				cell 	 : "string",
			}, 
			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descricao",
				cell 	 : "string",
			}, 
			{
				name : "ordem",
				editable : false,
				sortable : false,
				label 	 : "Ordem",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "latitude",
				editable : false,
				sortable : false,
				label 	 : "Latitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "longitude",
				editable : false,
				sortable : false,
				label 	 : "Longitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "enderecoFormatado",
				editable : false,
				sortable : false,
				label 	 : "Endereco formatado",
				cell 	 : "string",
			}, 
			{
				name : "origem",
				editable : false,
				sortable : false,
				label 	 : "Origem",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectEnderecoCorrida
});
