/* generated: 26/10/2016 13:30:46 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var FuncionarioModal = require('text!views/modalComponents/tpl/FuncionarioModalTemplate.html');
	var FuncionarioPageCollection = require('collections/FuncionarioPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var FuncionarioModal = Marionette.LayoutView.extend({
		template : _.template(FuncionarioModal),

		events : {
			'click #btnSearchFuncionario' : 'searchFuncionario',
			'click #btnClearFuncionario' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-funcionario',
			gridRegion : 		'#grid-funcionario',
			paginatorRegion : 	'#paginator-funcionario',
		},

		ui : {
    		inputModalNome : '#inputModalNome',
    		inputModalTelefone : '#inputModalTelefone',
    		inputModalTelefone2 : '#inputModalTelefone2',
    		inputModalEmail : '#inputModalEmail',
    		inputModalSenha : '#inputModalSenha',
    		inputModalTipo : '#inputModalTipo',
    		inputModalInativo : '#inputModalInativo',
    		inputModalUsuarioCadastro : '#inputModalUsuarioCadastro',
    		inputModalDataCadastro : '#inputModalDataCadastro',
			groupInputModalDataCadastro : '#groupInputModalDataCadastro',
    		inputModalMatricula : '#inputModalMatricula',
    		inputModalLimiteMensal : '#inputModalLimiteMensal',
    		inputModalCnpjCpf : '#inputModalCnpjCpf',
    		inputModalCnpj : '#inputModalCnpj',
    		inputModalCpf : '#inputModalCpf',
    		inputModalAutorizaEticket : '#inputModalAutorizaEticket',
    		inputModalObservacao : '#inputModalObservacao',
		
			form : '#formSearchFuncionario',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchFuncionario();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.funcionarioCollection = new FuncionarioPageCollection();
			this.funcionarioCollection.state.pageSize = 5;
			this.funcionarioCollection.on('fetching', this.startFetch, this);
			this.funcionarioCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.funcionarioCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.funcionarioCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.funcionarioCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectRow : function(e) {
			var modelFuncionario = util.getWrappedModel(e);
			if (modelFuncionario){
				this.modelSelect = modelFuncionario; 
				this.onSelectModel(modelFuncionario);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

			{
				name : "nome",
				editable : false,
				sortable : true,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : true,
				label 	 : "E-mail",
				cell 	 : "string",
			}, 
			{
				name : "senha",
				editable : false,
				sortable : true,
				label 	 : "Senha",
				cell 	 : "string",
			}, 
			{
				name : "tipo",
				editable : false,
				sortable : true,
				label 	 : "Tipo",
				cell 	 : "string",
			}, 
			{
				name : "inativo",
				editable : false,
				sortable : true,
				label 	 : "Inativo",
				cell 	 : "string",
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalNome'); 
			util.clear('inputModalEmail'); 
			util.clear('inputModalSenha'); 
			util.clear('inputModalTipo'); 
			util.clear('inputModalInativo'); 
			util.scrollUpModal();
		},

		searchFuncionario : function() {
			this.funcionarioCollection.filterQueryParams = {
	    		nome : util.escapeById('inputModalNome'),
	    		email : util.escapeById('inputModalEmail'),
	    		senha : util.escapeById('inputModalSenha'),
	    		tipo : util.escapeById('inputModalTipo'),
	    		inativo : util.escapeById('inputModalInativo'),
			};

			this.funcionarioCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.funcionarioCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.funcionarioCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinFuncionario');
		},
	});

	return FuncionarioModal;
});
