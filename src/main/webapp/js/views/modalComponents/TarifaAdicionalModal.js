/* generated: 26/10/2016 13:30:46 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var TarifaAdicionalModal = require('text!views/modalComponents/tpl/TarifaAdicionalModalTemplate.html');
	var TarifaAdicionalPageCollection = require('collections/TarifaAdicionalPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var TarifaAdicionalModal = Marionette.LayoutView.extend({
		template : _.template(TarifaAdicionalModal),

		events : {
			'click #btnSearchTarifaAdicional' : 'searchTarifaAdicional',
			'click #btnClearTarifaAdicional' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : '#counter-tarifaAdicional',
			gridRegion : '#grid-tarifaAdicional',
			paginatorRegion : '#paginator-tarifaAdicional',
		},

		ui : {
			inputModalDescricao : '#inputModalDescricao',
			inputModalValor : '#inputModalValor',

			form : '#formSearchTarifaAdicional',
			modalScreen : '.modal',
		},
		treatKeypress : function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchTarifaAdicional();
			}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.tarifaAdicionalCollection = new TarifaAdicionalPageCollection();
			this.tarifaAdicionalCollection.state.pageSize = 5;
			this.tarifaAdicionalCollection.on('fetching', this.startFetch, this);
			this.tarifaAdicionalCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.tarifaAdicionalCollection,
				emptyText : "Sem registros para exibir."

			});

			this.counter = new Counter({
				collection : this.tarifaAdicionalCollection,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.tarifaAdicionalCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectRow : function(e) {
			var modelTarifaAdicional = util.getWrappedModel(e);
			util.showSuccessMessage(e.target.textContent + ' adicionada com sucesso!', 'messages_modal_div');
			if (modelTarifaAdicional) {
				this.modelSelect = modelTarifaAdicional;
				this.onSelectModel(modelTarifaAdicional);
			}
		},
		
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [

			{
				name : "descricao",
				editable : false,
				sortable : true,
				label : "Descrição",
				cell : "string",
			}, {
				name : "valor",
				editable : false,
				sortable : true,
				label : "Valor",
				cell : 'string',

				formatter : _.extend({}, Backgrid.CellFormatter.prototype, {
					fromRaw : function(rawValue, model) {
						return 'R$ ' + util.printFormatNumber(rawValue);
					}
				})
			}, ];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalDescricao');
			util.clear('inputModalValor');
			util.scrollUpModal();
		},

		searchTarifaAdicional : function() {
			this.tarifaAdicionalCollection.filterQueryParams = {
				descricao : util.escapeById('inputModalDescricao'),
				valor : util.escapeById('inputModalValor'),
			};

			this.tarifaAdicionalCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					// caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.tarifaAdicionalCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					// caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.tarifaAdicionalCollection.reset();
		},

		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},

		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinTarifaAdicional');
		},
	});

	return TarifaAdicionalModal;
});
