/* generated: 26/10/2016 13:30:46 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var MensagemMotoristaModal = require('text!views/modalComponents/tpl/MensagemMotoristaModalTemplate.html');
	var MensagemMotoristaPageCollection = require('collections/MensagemMotoristaPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var MensagemMotoristaModal = Marionette.LayoutView.extend({
		template : _.template(MensagemMotoristaModal),

		events : {
			'click #btnSearchMensagemMotorista' : 'searchMensagemMotorista',
			'click #btnClearMensagemMotorista' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-mensagemMotorista',
			gridRegion : 		'#grid-mensagemMotorista',
			paginatorRegion : 	'#paginator-mensagemMotorista',
		},

		ui : {
    		inputModalDataEnvio : '#inputModalDataEnvio',
			groupInputModalDataEnvio : '#groupInputModalDataEnvio',
    		inputModalDataLeitura : '#inputModalDataLeitura',
			groupInputModalDataLeitura : '#groupInputModalDataLeitura',
		
			form : '#formSearchMensagemMotorista',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchMensagemMotorista();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.mensagemMotoristaCollection = new MensagemMotoristaPageCollection();
			this.mensagemMotoristaCollection.state.pageSize = 5;
			this.mensagemMotoristaCollection.on('fetching', this.startFetch, this);
			this.mensagemMotoristaCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.mensagemMotoristaCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.mensagemMotoristaCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.mensagemMotoristaCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.ui.groupInputModalDataEnvio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputModalDataEnvio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputModalDataEnvio.mask('99/99/9999 99:99');
				this.ui.groupInputModalDataLeitura.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputModalDataLeitura.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputModalDataLeitura.mask('99/99/9999 99:99');
			});
		},

		selectRow : function(e) {
			var modelMensagemMotorista = util.getWrappedModel(e);
			if (modelMensagemMotorista){
				this.modelSelect = modelMensagemMotorista; 
				this.onSelectModel(modelMensagemMotorista);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

			{
				name : "dataEnvio",
				editable : false,
				sortable : true,
				label 	 : "Data envio",
				cell 	 : "string",
			}, 
			{
				name : "dataLeitura",
				editable : false,
				sortable : true,
				label 	 : "Data leitura",
				cell 	 : "string",
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalDataEnvio'); 
			util.clear('inputModalDataLeitura'); 
			util.scrollUpModal();
		},

		searchMensagemMotorista : function() {
			this.mensagemMotoristaCollection.filterQueryParams = {
	    		dataEnvio : util.escapeById('inputModalDataEnvio'),
	    		dataLeitura : util.escapeById('inputModalDataLeitura'),
			};

			this.mensagemMotoristaCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.mensagemMotoristaCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.mensagemMotoristaCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinMensagemMotorista');
		},
	});

	return MensagemMotoristaModal;
});
