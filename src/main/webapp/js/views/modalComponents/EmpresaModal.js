/* generated: 26/10/2016 13:30:46 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var EmpresaModal = require('text!views/modalComponents/tpl/EmpresaModalTemplate.html');
	var EmpresaPageCollection = require('collections/EmpresaPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var EmpresaModal = Marionette.LayoutView.extend({
		template : _.template(EmpresaModal),

		events : {
			'click #btnSearchEmpresa' : 'searchEmpresa',
			'click #btnClearEmpresa' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-empresa',
			gridRegion : 		'#grid-empresa',
			paginatorRegion : 	'#paginator-empresa',
		},

		ui : {
    		inputModalNomeFantasia : '#inputModalNomeFantasia',
    		inputModalCnpj : '#inputModalCnpj',
    		inputModalContrato : '#inputModalContrato',
    		inputModalRazaoSocial : '#inputModalRazaoSocial',
    		inputModalEndereco : '#inputModalEndereco',
    		inputModalNumeroEndereco : '#inputModalNumeroEndereco',
    		inputModalComplemento : '#inputModalComplemento',
    		inputModalUf : '#inputModalUf',
    		inputModalCidade : '#inputModalCidade',
    		inputModalBairro : '#inputModalBairro',
    		inputModalCep : '#inputModalCep',
    		inputModalReferenciaEndereco : '#inputModalReferenciaEndereco',
    		inputModalDdd : '#inputModalDdd',
    		inputModalFone : '#inputModalFone',
    		inputModalUtilizaVoucher : '#inputModalUtilizaVoucher',
    		inputModalUtilizaEticket : '#inputModalUtilizaEticket',
    		inputModalObservacaoTaxista : '#inputModalObservacaoTaxista',
    		inputModalObservacaoCentral : '#inputModalObservacaoCentral',
    		inputModalObservacaoFinanceiro : '#inputModalObservacaoFinanceiro',
    		inputModalPercentualDesconto : '#inputModalPercentualDesconto',
    		inputModalInscricaoMunicipal : '#inputModalInscricaoMunicipal',
    		inputModalInscricaoEstadual : '#inputModalInscricaoEstadual',
    		inputModalEmiteNf : '#inputModalEmiteNf',
    		inputModalPercentualIss : '#inputModalPercentualIss',
    		inputModalPercentualIrf : '#inputModalPercentualIrf',
    		inputModalPercentualInss : '#inputModalPercentualInss',
    		inputModalDiaVencimento : '#inputModalDiaVencimento',
    		inputModalPercentualMotorista : '#inputModalPercentualMotorista',
    		inputModalBanco : '#inputModalBanco',
    		inputModalAgencia : '#inputModalAgencia',
    		inputModalConta : '#inputModalConta',
    		inputModalEmail : '#inputModalEmail',
    		inputModalUsuarioCadastro : '#inputModalUsuarioCadastro',
    		inputModalDataCadastro : '#inputModalDataCadastro',
			groupInputModalDataCadastro : '#groupInputModalDataCadastro',
    		inputModalStatusEmpresa : '#inputModalStatusEmpresa',
		
			form : '#formSearchEmpresa',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchEmpresa();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.empresaCollection = new EmpresaPageCollection();
			this.empresaCollection.state.pageSize = 5;
			this.empresaCollection.on('fetching', this.startFetch, this);
			this.empresaCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.empresaCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.empresaCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.empresaCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.ui.inputModalCnpj.mask('99.999.999/9999-99');
			});
		},

		selectRow : function(e) {
			var modelEmpresa = util.getWrappedModel(e);
			if (modelEmpresa){
				this.modelSelect = modelEmpresa; 
				this.onSelectModel(modelEmpresa);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

			{
				name : "nomeFantasia",
				editable : false,
				sortable : true,
				label 	 : "Nome Fantasia",
				cell 	 : "string",
			}, 
			{
				name : "cnpj",
				editable : false,
				sortable : true,
				label 	 : "CNPJ",
				cell 	 : "string",
			}, 
			{
				name : "contrato",
				editable : false,
				sortable : true,
				label 	 : "Contrato",
				cell 	 : "string",
			}, 
			{
				name : "razaoSocial",
				editable : false,
				sortable : true,
				label 	 : "Razão social",
				cell 	 : "string",
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalNomeFantasia'); 
			util.clear('inputModalCnpj'); 
			util.clear('inputModalContrato'); 
			util.clear('inputModalRazaoSocial'); 
			util.scrollUpModal();
		},

		searchEmpresa : function() {
			this.empresaCollection.filterQueryParams = {
	    		nomeFantasia : util.escapeById('inputModalNomeFantasia'),
	    		cnpj : util.escapeById('inputModalCnpj'),
	    		contrato : util.escapeById('inputModalContrato'),
	    		razaoSocial : util.escapeById('inputModalRazaoSocial'),
			};

			this.empresaCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.empresaCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.empresaCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinEmpresa');
		},
	});

	return EmpresaModal;
});
