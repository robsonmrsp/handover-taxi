/* generated: 04/11/2016 11:29:27 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var StatusTrackerModal = require('text!views/modalComponents/tpl/StatusTrackerModalTemplate.html');
	var StatusTrackerPageCollection = require('collections/StatusTrackerPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var StatusTrackerModal = Marionette.LayoutView.extend({
		template : _.template(StatusTrackerModal),

		events : {
			'click #btnSearchStatusTracker' : 'searchStatusTracker',
			'click #btnClearStatusTracker' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-statusTracker',
			gridRegion : 		'#grid-statusTracker',
			paginatorRegion : 	'#paginator-statusTracker',
		},

		ui : {
    		inputModalLatitude : '#inputModalLatitude',
    		inputModalLongitude : '#inputModalLongitude',
    		inputModalTimestamp : '#inputModalTimestamp',
    		inputModalSpeed : '#inputModalSpeed',
    		inputModalAccuracy : '#inputModalAccuracy',
    		inputModalDirection : '#inputModalDirection',
    		inputModalAltitude : '#inputModalAltitude',
    		inputModalBateryLevel : '#inputModalBateryLevel',
    		inputModalGpsEnabled : '#inputModalGpsEnabled',
    		inputModalWifiEnabled : '#inputModalWifiEnabled',
    		inputModalMobileEnabled : '#inputModalMobileEnabled',
		
			form : '#formSearchStatusTracker',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchStatusTracker();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.statusTrackerCollection = new StatusTrackerPageCollection();
			this.statusTrackerCollection.state.pageSize = 5;
			this.statusTrackerCollection.on('fetching', this.startFetch, this);
			this.statusTrackerCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.statusTrackerCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.statusTrackerCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.statusTrackerCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectRow : function(e) {
			var modelStatusTracker = util.getWrappedModel(e);
			if (modelStatusTracker){
				this.modelSelect = modelStatusTracker; 
				this.onSelectModel(modelStatusTracker);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.scrollUpModal();
		},

		searchStatusTracker : function() {
			this.statusTrackerCollection.filterQueryParams = {
			};

			this.statusTrackerCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.statusTrackerCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.statusTrackerCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinStatusTracker');
		},
	});

	return StatusTrackerModal;
});
