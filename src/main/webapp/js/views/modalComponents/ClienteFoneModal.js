/* generated: 26/10/2016 13:30:46 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ClienteFoneModal = require('text!views/modalComponents/tpl/ClienteFoneModalTemplate.html');
	var ClienteFonePageCollection = require('collections/ClienteFonePageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var ClienteFoneModal = Marionette.LayoutView.extend({
		template : _.template(ClienteFoneModal),

		events : {
			'click #btnSearchClienteFone' : 'searchClienteFone',
			'click #btnClearClienteFone' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-clienteFone',
			gridRegion : 		'#grid-clienteFone',
			paginatorRegion : 	'#paginator-clienteFone',
		},

		ui : {
    		inputModalDdd : '#inputModalDdd',
    		inputModalFone : '#inputModalFone',
    		inputModalFavorito : '#inputModalFavorito',
		
			form : '#formSearchClienteFone',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchClienteFone();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.clienteFoneCollection = new ClienteFonePageCollection();
			this.clienteFoneCollection.state.pageSize = 5;
			this.clienteFoneCollection.on('fetching', this.startFetch, this);
			this.clienteFoneCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.clienteFoneCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.clienteFoneCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.clienteFoneCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectRow : function(e) {
			var modelClienteFone = util.getWrappedModel(e);
			if (modelClienteFone){
				this.modelSelect = modelClienteFone; 
				this.onSelectModel(modelClienteFone);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

			{
				name : "ddd",
				editable : false,
				sortable : true,
				label 	 : "Ddd",
				cell 	 : "string",
			}, 
			{
				name : "fone",
				editable : false,
				sortable : true,
				label 	 : "Fone",
				cell 	 : "string",
			}, 
			{
				name : "favorito",
				editable : false,
				sortable : true,
				label 	 : "Favorito",
				cell 	 : "string",
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalDdd'); 
			util.clear('inputModalFone'); 
			util.clear('inputModalFavorito'); 
			util.scrollUpModal();
		},

		searchClienteFone : function() {
			this.clienteFoneCollection.filterQueryParams = {
	    		ddd : util.escapeById('inputModalDdd'),
	    		fone : util.escapeById('inputModalFone'),
	    		favorito : util.escapeById('inputModalFavorito'),
			};

			this.clienteFoneCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.clienteFoneCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.clienteFoneCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinClienteFone');
		},
	});

	return ClienteFoneModal;
});
