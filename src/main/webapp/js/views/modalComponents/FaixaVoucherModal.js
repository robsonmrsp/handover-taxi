/* generated: 26/10/2016 13:30:46 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var FaixaVoucherModal = require('text!views/modalComponents/tpl/FaixaVoucherModalTemplate.html');
	var FaixaVoucherPageCollection = require('collections/FaixaVoucherPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var FaixaVoucherModal = Marionette.LayoutView.extend({
		template : _.template(FaixaVoucherModal),

		events : {
			'click #btnSearchFaixaVoucher' : 'searchFaixaVoucher',
			'click #btnClearFaixaVoucher' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-faixaVoucher',
			gridRegion : 		'#grid-faixaVoucher',
			paginatorRegion : 	'#paginator-faixaVoucher',
		},

		ui : {
    		inputModalSerie : '#inputModalSerie',
    		inputModalNumeroInicial : '#inputModalNumeroInicial',
    		inputModalNumeroFinal : '#inputModalNumeroFinal',
    		inputModalInativo : '#inputModalInativo',
		
			form : '#formSearchFaixaVoucher',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchFaixaVoucher();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.faixaVoucherCollection = new FaixaVoucherPageCollection();
			this.faixaVoucherCollection.state.pageSize = 5;
			this.faixaVoucherCollection.on('fetching', this.startFetch, this);
			this.faixaVoucherCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.faixaVoucherCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.faixaVoucherCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.faixaVoucherCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectRow : function(e) {
			var modelFaixaVoucher = util.getWrappedModel(e);
			if (modelFaixaVoucher){
				this.modelSelect = modelFaixaVoucher; 
				this.onSelectModel(modelFaixaVoucher);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

			{
				name : "serie",
				editable : false,
				sortable : true,
				label 	 : "Serie",
				cell 	 : "string",
			}, 
			{
				name : "numeroInicial",
				editable : false,
				sortable : true,
				label 	 : "Número inicial",
				cell 	 : "string",
			}, 
			{
				name : "numeroFinal",
				editable : false,
				sortable : true,
				label 	 : "Número final",
				cell 	 : "string",
			}, 
			{
				name : "inativo",
				editable : false,
				sortable : true,
				label 	 : "Inativo",
				cell 	 : "string",
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalSerie'); 
			util.clear('inputModalNumeroInicial'); 
			util.clear('inputModalNumeroFinal'); 
			util.clear('inputModalInativo'); 
			util.scrollUpModal();
		},

		searchFaixaVoucher : function() {
			this.faixaVoucherCollection.filterQueryParams = {
	    		serie : util.escapeById('inputModalSerie'),
	    		numeroInicial : util.escapeById('inputModalNumeroInicial'),
	    		numeroFinal : util.escapeById('inputModalNumeroFinal'),
	    		inativo : util.escapeById('inputModalInativo'),
			};

			this.faixaVoucherCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.faixaVoucherCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.faixaVoucherCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinFaixaVoucher');
		},
	});

	return FaixaVoucherModal;
});
