/* generated: 26/10/2016 13:30:46 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var MotoristaBloqueadoModal = require('text!views/modalComponents/tpl/MotoristaBloqueadoModalTemplate.html');
	var MotoristaBloqueadoPageCollection = require('collections/MotoristaBloqueadoPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var MotoristaBloqueadoModal = Marionette.LayoutView.extend({
		template : _.template(MotoristaBloqueadoModal),

		events : {
			'click #btnSearchMotoristaBloqueado' : 'searchMotoristaBloqueado',
			'click #btnClearMotoristaBloqueado' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-motoristaBloqueado',
			gridRegion : 		'#grid-motoristaBloqueado',
			paginatorRegion : 	'#paginator-motoristaBloqueado',
		},

		ui : {
    		inputModalDataBloqueio : '#inputModalDataBloqueio',
			groupInputModalDataBloqueio : '#groupInputModalDataBloqueio',
    		inputModalUsuarioBloqueio : '#inputModalUsuarioBloqueio',
		
			form : '#formSearchMotoristaBloqueado',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchMotoristaBloqueado();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.motoristaBloqueadoCollection = new MotoristaBloqueadoPageCollection();
			this.motoristaBloqueadoCollection.state.pageSize = 5;
			this.motoristaBloqueadoCollection.on('fetching', this.startFetch, this);
			this.motoristaBloqueadoCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.motoristaBloqueadoCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.motoristaBloqueadoCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.motoristaBloqueadoCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.ui.groupInputModalDataBloqueio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputModalDataBloqueio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputModalDataBloqueio.mask('99/99/9999 99:99');
			});
		},

		selectRow : function(e) {
			var modelMotoristaBloqueado = util.getWrappedModel(e);
			if (modelMotoristaBloqueado){
				this.modelSelect = modelMotoristaBloqueado; 
				this.onSelectModel(modelMotoristaBloqueado);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

			{
				name : "dataBloqueio",
				editable : false,
				sortable : true,
				label 	 : "Data bloqueio",
				cell 	 : "string",
			}, 
			{
				name : "usuarioBloqueio",
				editable : false,
				sortable : true,
				label 	 : "Usuario bloqueio",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalDataBloqueio'); 
			util.clear('inputModalUsuarioBloqueio'); 
			util.scrollUpModal();
		},

		searchMotoristaBloqueado : function() {
			this.motoristaBloqueadoCollection.filterQueryParams = {
	    		dataBloqueio : util.escapeById('inputModalDataBloqueio'),
	    		usuarioBloqueio : util.escapeById('inputModalUsuarioBloqueio'),
			};

			this.motoristaBloqueadoCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.motoristaBloqueadoCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.motoristaBloqueadoCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinMotoristaBloqueado');
		},
	});

	return MotoristaBloqueadoModal;
});
