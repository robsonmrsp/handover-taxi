/* generated: 26/10/2016 13:30:46 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var EnderecoFavoritoModal = require('text!views/modalComponents/tpl/EnderecoFavoritoModalTemplate.html');
	var EnderecoFavoritoPageCollection = require('collections/EnderecoFavoritoPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var EnderecoFavoritoModal = Marionette.LayoutView.extend({
		template : _.template(EnderecoFavoritoModal),

		events : {
			'click #btnSearchEnderecoFavorito' : 'searchEnderecoFavorito',
			'click #btnClearEnderecoFavorito' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : '#counter-enderecoFavorito',
			gridRegion : '#grid-enderecoFavorito',
			paginatorRegion : '#paginator-enderecoFavorito',
		},

		ui : {
			inputModalLatitude : '#inputModalLatitude',
			inputModalLongitude : '#inputModalLongitude',
			inputModalEnderecoFormatado : '#inputModalEnderecoFormatado',
			inputModalValido : '#inputModalValido',

			form : '#formSearchEnderecoFavorito',
			modalScreen : '.modal',
		},
		treatKeypress : function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchEnderecoFavorito();
			}
		},

		setCliente : function(clienteJson) {
			this.cliente = clienteJson;
		},
		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.enderecoFavoritoCollection = new EnderecoFavoritoPageCollection();
			this.enderecoFavoritoCollection.state.pageSize = 5;
			this.enderecoFavoritoCollection.on('fetching', this.startFetch, this);
			this.enderecoFavoritoCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.enderecoFavoritoCollection,
				emptyText : "Sem registros para exibir."

			});

			this.counter = new Counter({
				collection : this.enderecoFavoritoCollection,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.enderecoFavoritoCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectRow : function(e) {
			var modelEnderecoFavorito = util.getWrappedModel(e);
			if (modelEnderecoFavorito) {
				modelEnderecoFavorito.set('id', null);
				modelEnderecoFavorito.id = null;
				this.modelSelect = modelEnderecoFavorito;
				this.onSelectModel(modelEnderecoFavorito);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [

			// {
			// name : "latitude",
			// editable : false,
			// sortable : true,
			// label : "Latitude",
			// cell : CustomNumberCell.extend({}),
			// },
			// {
			// name : "longitude",
			// editable : false,
			// sortable : true,
			// label : "Longitude",
			// cell : CustomNumberCell.extend({}),
			// },
			{
				name : "enderecoFormatado",
				editable : false,
				sortable : true,
				label : "Endereco formatado",
				cell : "string",
			},
			// {
			// name : "valido",
			// editable : false,
			// sortable : true,
			// label : "Válido",
			// cell : "string",
			// },
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalLatitude');
			util.clear('inputModalLongitude');
			util.clear('inputModalEnderecoFormatado');
			util.clear('inputModalValido');
			util.scrollUpModal();
		},

		searchEnderecoFavorito : function() {
			var that = this;
			this.enderecoFavoritoCollection.filterQueryParams = {
				enderecoFormatado : util.escapeById('inputModalEnderecoFormatado'),
				cliente : that.cliente && that.cliente.id,
			};

			this.enderecoFavoritoCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					// caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');

			this.searchEnderecoFavorito()
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.enderecoFavoritoCollection.reset();
		},

		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},

		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinEnderecoFavorito');
		},
	});

	return EnderecoFavoritoModal;
});
