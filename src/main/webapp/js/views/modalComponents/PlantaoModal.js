/* generated: 26/10/2016 13:30:46 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var PlantaoModal = require('text!views/modalComponents/tpl/PlantaoModalTemplate.html');
	var PlantaoPageCollection = require('collections/PlantaoPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var PlantaoModal = Marionette.LayoutView.extend({
		template : _.template(PlantaoModal),

		events : {
			'click #btnSearchPlantao' : 'searchPlantao',
			'click #btnClearPlantao' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-plantao',
			gridRegion : 		'#grid-plantao',
			paginatorRegion : 	'#paginator-plantao',
		},

		ui : {
    		inputModalDataInicio : '#inputModalDataInicio',
			groupInputModalDataInicio : '#groupInputModalDataInicio',
    		inputModalDataFim : '#inputModalDataFim',
			groupInputModalDataFim : '#groupInputModalDataFim',
		
			form : '#formSearchPlantao',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchPlantao();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.plantaoCollection = new PlantaoPageCollection();
			this.plantaoCollection.state.pageSize = 5;
			this.plantaoCollection.on('fetching', this.startFetch, this);
			this.plantaoCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.plantaoCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.plantaoCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.plantaoCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.ui.groupInputModalDataInicio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputModalDataInicio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputModalDataInicio.mask('99/99/9999 99:99');
				this.ui.groupInputModalDataFim.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputModalDataFim.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputModalDataFim.mask('99/99/9999 99:99');
			});
		},

		selectRow : function(e) {
			var modelPlantao = util.getWrappedModel(e);
			if (modelPlantao){
				this.modelSelect = modelPlantao; 
				this.onSelectModel(modelPlantao);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

			{
				name : "dataInicio",
				editable : false,
				sortable : true,
				label 	 : "Data inicio",
				cell 	 : "string",
			}, 
			{
				name : "dataFim",
				editable : false,
				sortable : true,
				label 	 : "Data fim",
				cell 	 : "string",
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalDataInicio'); 
			util.clear('inputModalDataFim'); 
			util.scrollUpModal();
		},

		searchPlantao : function() {
			this.plantaoCollection.filterQueryParams = {
	    		dataInicio : util.escapeById('inputModalDataInicio'),
	    		dataFim : util.escapeById('inputModalDataFim'),
			};

			this.plantaoCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.plantaoCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.plantaoCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinPlantao');
		},
	});

	return PlantaoModal;
});
