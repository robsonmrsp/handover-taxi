/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var MotoristaHasPlantaoModal = require('text!views/modalComponents/tpl/MotoristaHasPlantaoModalTemplate.html');
	var MotoristaHasPlantaoPageCollection = require('collections/MotoristaHasPlantaoPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var MotoristaHasPlantaoModal = Marionette.LayoutView.extend({
		template : _.template(MotoristaHasPlantaoModal),

		events : {
			'click #btnSearchMotoristaHasPlantao' : 'searchMotoristaHasPlantao',
			'click #btnClearMotoristaHasPlantao' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-motoristaHasPlantao',
			gridRegion : 		'#grid-motoristaHasPlantao',
			paginatorRegion : 	'#paginator-motoristaHasPlantao',
		},

		ui : {
		
			form : '#formSearchMotoristaHasPlantao',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchMotoristaHasPlantao();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.motoristaHasPlantaoCollection = new MotoristaHasPlantaoPageCollection();
			this.motoristaHasPlantaoCollection.state.pageSize = 5;
			this.motoristaHasPlantaoCollection.on('fetching', this.startFetch, this);
			this.motoristaHasPlantaoCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.motoristaHasPlantaoCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.motoristaHasPlantaoCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.motoristaHasPlantaoCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectRow : function(e) {
			var modelMotoristaHasPlantao = util.getWrappedModel(e);
			if (modelMotoristaHasPlantao){
				this.modelSelect = modelMotoristaHasPlantao; 
				this.onSelectModel(modelMotoristaHasPlantao);
			}
		},
		getJsonValue : function() {
			if(this.modelSelect){
				return this.modelSelect.toJSON();
			}
			return null;
		},
		
		getValue : function() {
			return this.modelSelect;
		},
		
		getColumns : function() {
			var columns = [	

			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.scrollUpModal();
		},

		searchMotoristaHasPlantao : function() {
			this.motoristaHasPlantaoCollection.filterQueryParams = {
			};

			this.motoristaHasPlantaoCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.motoristaHasPlantaoCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.motoristaHasPlantaoCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinMotoristaHasPlantao');
		},
	});

	return MotoristaHasPlantaoModal;
});
