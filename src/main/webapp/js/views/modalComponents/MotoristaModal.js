/* generated: 26/10/2016 13:30:46 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var MotoristaModal = require('text!views/modalComponents/tpl/MotoristaModalTemplate.html');
	var MotoristaPageCollection = require('collections/MotoristaPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var MotoristaModal = Marionette.LayoutView.extend({
		template : _.template(MotoristaModal),

		events : {
			'click #btnSearchMotorista' : 'searchMotorista',
			'click #btnClearMotorista' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-motorista',
			gridRegion : 		'#grid-motorista',
			paginatorRegion : 	'#paginator-motorista',
		},

		ui : {
			inputModalNomeMotoqueiro : '#inputModalNomeMotoqueiro',
    		inputModalNomeReduzido : '#inputModalNomeReduzido',
    		inputModalViatura : '#inputModalViatura',
    		inputModalEmail : '#inputModalEmail',
    		inputModalDdd : '#inputModalDdd',
    		inputModalFone : '#inputModalFone',
    		inputModalStatusMotorista : '#inputModalStatusMotorista',
    		inputModalDdd2 : '#inputModalDdd2',
    		inputModalFone2 : '#inputModalFone2',
    		inputModalDddWhatsapp : '#inputModalDddWhatsapp',
    		inputModalWhatsapp : '#inputModalWhatsapp',
    		inputModalDataNascimento : '#inputModalDataNascimento',
			groupInputModalDataNascimento : '#groupInputModalDataNascimento',
    		inputModalRg : '#inputModalRg',
    		inputModalOrgaoExpedidor : '#inputModalOrgaoExpedidor',
    		inputModalDataExpedicao : '#inputModalDataExpedicao',
			groupInputModalDataExpedicao : '#groupInputModalDataExpedicao',
    		inputModalNaturalidade : '#inputModalNaturalidade',
    		inputModalCpf : '#inputModalCpf',
    		inputModalCnh : '#inputModalCnh',
    		inputModalEmissaoCnh : '#inputModalEmissaoCnh',
			groupInputModalEmissaoCnh : '#groupInputModalEmissaoCnh',
    		inputModalCategoriaCnh : '#inputModalCategoriaCnh',
    		inputModalValidadeCnh : '#inputModalValidadeCnh',
			groupInputModalValidadeCnh : '#groupInputModalValidadeCnh',
    		inputModalFoto1 : '#inputModalFoto1',
    		inputModalFoto2 : '#inputModalFoto2',
    		inputModalFoto3 : '#inputModalFoto3',
    		inputModalEndereco : '#inputModalEndereco',
    		inputModalNumeroEndereco : '#inputModalNumeroEndereco',
    		inputModalComplemento : '#inputModalComplemento',
    		inputModalUf : '#inputModalUf',
    		inputModalCidade : '#inputModalCidade',
    		inputModalBairro : '#inputModalBairro',
    		inputModalCep : '#inputModalCep',
    		inputModalReferenciaEndereco : '#inputModalReferenciaEndereco',
    		inputModalMae : '#inputModalMae',
    		inputModalPai : '#inputModalPai',
    		inputModalConjuge : '#inputModalConjuge',
    		inputModalReferenciaPessoal1 : '#inputModalReferenciaPessoal1',
    		inputModalFoneReferencia1 : '#inputModalFoneReferencia1',
    		inputModalParentescoReferencia1 : '#inputModalParentescoReferencia1',
    		inputModalReferenciaPessoal2 : '#inputModalReferenciaPessoal2',
    		inputModalFoneReferencia2 : '#inputModalFoneReferencia2',
    		inputModalParentescoReferencia2 : '#inputModalParentescoReferencia2',
    		inputModalMarcaVeiculo : '#inputModalMarcaVeiculo',
    		inputModalModeloVeiculo : '#inputModalModeloVeiculo',
    		inputModalPlacaVeiculo : '#inputModalPlacaVeiculo',
    		inputModalCorVeiculo : '#inputModalCorVeiculo',
    		inputModalAnoVeiculo : '#inputModalAnoVeiculo',
    		inputModalRenavam : '#inputModalRenavam',
    		inputModalChassi : '#inputModalChassi',
    		inputModalCategoria : '#inputModalCategoria',
    		inputModalImei : '#inputModalImei',
    		inputModalCartaoCredito : '#inputModalCartaoCredito',
    		inputModalCartaoDebito : '#inputModalCartaoDebito',
    		inputModalVoucher : '#inputModalVoucher',
    		inputModalEticket : '#inputModalEticket',
    		inputModalSenha : '#inputModalSenha',
    		inputModalPadronizada : '#inputModalPadronizada',
    		inputModalBau : '#inputModalBau',
    		inputModalMotoGrande : '#inputModalMotoGrande',
		
			form : '#formSearchMotorista',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchMotorista();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.motoristaCollection = new MotoristaPageCollection();
			this.motoristaCollection.state.pageSize = 5;
			this.motoristaCollection.on('fetching', this.startFetch, this);
			this.motoristaCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.motoristaCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.motoristaCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.motoristaCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectRow : function(e) {
			var modelMotorista = util.getWrappedModel(e);
			if (modelMotorista){
				this.modelSelect = modelMotorista; 
				this.onSelectModel(modelMotorista);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

			{
				name : "viatura",
				editable : false,
				sortable : true,
				label 	 : "Viatura",
				cell 	 : "string",
			}, 
			{
				name : "nome",
				editable : false,
				sortable : true,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "nomeReduzido",
				editable : false,
				sortable : true,
				label 	 : "Apelido",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : true,
				label 	 : "E-mail",
				cell 	 : "string",
			}, 
			{
				name : "fone",
				editable : false,
				sortable : true,
				label 	 : "Telefone",
				cell 	 : "string",
			}, 
			{
				name : "statusMotorista",
				editable : false,
				sortable : true,
				label 	 : "Status",
				cell 	 : "string",
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalNomeMotoqueiro'); 
			util.clear('inputModalNomeReduzido'); 
			util.clear('inputModalViatura'); 
			util.clear('inputModalEmail'); 
			util.clear('inputModalDdd'); 
			util.clear('inputModalFone'); 
			util.clear('inputModalStatusMotorista'); 
			util.scrollUpModal();
		},

		searchMotorista : function() {
			
			console.log(util.escapeById('inputModalNomeMotoqueiro'));
			console.log(this.ui.inputModalNomeMotoqueiro);
			
			this.motoristaCollection.filterQueryParams = {
				nome : util.escapeById('inputModalNomeMotoqueiro'),
	    		nomeReduzido : util.escapeById('inputModalNomeReduzido'),
	    		viatura : util.escapeById('inputModalViatura'),
	    		email : util.escapeById('inputModalEmail'),
	    		ddd : util.escapeById('inputModalDdd'),
	    		fone : util.escapeById('inputModalFone'),
	    		statusMotorista : util.escapeById('inputModalStatusMotorista'),
			};

			this.motoristaCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.motoristaCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
//			this.ui.form.get(0).reset();
			this.motoristaCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinMotorista');
		},
	});

	return MotoristaModal;
});
