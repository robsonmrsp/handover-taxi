/* generated: 26/10/2016 13:30:46 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ClienteEnderecoModal = require('text!views/modalComponents/tpl/ClienteEnderecoModalTemplate.html');
	var ClienteEnderecoPageCollection = require('collections/ClienteEnderecoPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var ClienteEnderecoModal = Marionette.LayoutView.extend({
		template : _.template(ClienteEnderecoModal),

		events : {
			'click #btnSearchClienteEndereco' : 'searchClienteEndereco',
			'click #btnClearClienteEndereco' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-clienteEndereco',
			gridRegion : 		'#grid-clienteEndereco',
			paginatorRegion : 	'#paginator-clienteEndereco',
		},

		ui : {
    		inputModalEndereco : '#inputModalEndereco',
    		inputModalNumero : '#inputModalNumero',
    		inputModalComplemento : '#inputModalComplemento',
    		inputModalUf : '#inputModalUf',
    		inputModalCidade : '#inputModalCidade',
    		inputModalBairro : '#inputModalBairro',
    		inputModalCep : '#inputModalCep',
    		inputModalReferencia : '#inputModalReferencia',
    		inputModalTipo : '#inputModalTipo',
    		inputModalFavorito : '#inputModalFavorito',
		
			form : '#formSearchClienteEndereco',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchClienteEndereco();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.clienteEnderecoCollection = new ClienteEnderecoPageCollection();
			this.clienteEnderecoCollection.state.pageSize = 5;
			this.clienteEnderecoCollection.on('fetching', this.startFetch, this);
			this.clienteEnderecoCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.clienteEnderecoCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.clienteEnderecoCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.clienteEnderecoCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectRow : function(e) {
			var modelClienteEndereco = util.getWrappedModel(e);
			if (modelClienteEndereco){
				this.modelSelect = modelClienteEndereco; 
				this.onSelectModel(modelClienteEndereco);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

			{
				name : "endereco",
				editable : false,
				sortable : true,
				label 	 : "Endereco",
				cell 	 : "string",
			}, 
			{
				name : "numero",
				editable : false,
				sortable : true,
				label 	 : "Úmero",
				cell 	 : "string",
			}, 
			{
				name : "complemento",
				editable : false,
				sortable : true,
				label 	 : "Complemento",
				cell 	 : "string",
			}, 
			{
				name : "uf",
				editable : false,
				sortable : true,
				label 	 : "Uf",
				cell 	 : "string",
			}, 
			{
				name : "cidade",
				editable : false,
				sortable : true,
				label 	 : "Cidade",
				cell 	 : "string",
			}, 
			{
				name : "bairro",
				editable : false,
				sortable : true,
				label 	 : "Bairro",
				cell 	 : "string",
			}, 
			{
				name : "cep",
				editable : false,
				sortable : true,
				label 	 : "Cep",
				cell 	 : "string",
			}, 
			{
				name : "referencia",
				editable : false,
				sortable : true,
				label 	 : "Referência",
				cell 	 : "string",
			}, 
			{
				name : "tipo",
				editable : false,
				sortable : true,
				label 	 : "Tipo",
				cell 	 : "string",
			}, 
			{
				name : "favorito",
				editable : false,
				sortable : true,
				label 	 : "Favorito",
				cell 	 : "string",
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalEndereco'); 
			util.clear('inputModalNumero'); 
			util.clear('inputModalComplemento'); 
			util.clear('inputModalUf'); 
			util.clear('inputModalCidade'); 
			util.clear('inputModalBairro'); 
			util.clear('inputModalCep'); 
			util.clear('inputModalReferencia'); 
			util.clear('inputModalTipo'); 
			util.clear('inputModalFavorito'); 
			util.scrollUpModal();
		},

		searchClienteEndereco : function() {
			this.clienteEnderecoCollection.filterQueryParams = {
	    		endereco : util.escapeById('inputModalEndereco'),
	    		numero : util.escapeById('inputModalNumero'),
	    		complemento : util.escapeById('inputModalComplemento'),
	    		uf : util.escapeById('inputModalUf'),
	    		cidade : util.escapeById('inputModalCidade'),
	    		bairro : util.escapeById('inputModalBairro'),
	    		cep : util.escapeById('inputModalCep'),
	    		referencia : util.escapeById('inputModalReferencia'),
	    		tipo : util.escapeById('inputModalTipo'),
	    		favorito : util.escapeById('inputModalFavorito'),
			};

			this.clienteEnderecoCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.clienteEnderecoCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.clienteEnderecoCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinClienteEndereco');
		},
	});

	return ClienteEnderecoModal;
});
