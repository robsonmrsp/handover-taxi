/* generated: 26/10/2016 13:30:46 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var HdUsuarioModal = require('text!views/modalComponents/tpl/HdUsuarioModalTemplate.html');
	var HdUsuarioPageCollection = require('collections/HdUsuarioPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var HdUsuarioModal = Marionette.LayoutView.extend({
		template : _.template(HdUsuarioModal),

		events : {
			'click #btnSearchHdUsuario' : 'searchHdUsuario',
			'click #btnClearHdUsuario' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-hdUsuario',
			gridRegion : 		'#grid-hdUsuario',
			paginatorRegion : 	'#paginator-hdUsuario',
		},

		ui : {
    		inputModalNome : '#inputModalNome',
    		inputModalEmail : '#inputModalEmail',
    		inputModalSenha : '#inputModalSenha',
    		inputModalTipo : '#inputModalTipo',
    		inputModalPerfil : '#inputModalPerfil',
    		inputModalStatusUsuario : '#inputModalStatusUsuario',
    		inputModalDatainclusao : '#inputModalDatainclusao',
			groupInputModalDatainclusao : '#groupInputModalDatainclusao',
    		inputModalUsuarioinclusao : '#inputModalUsuarioinclusao',
		
			form : '#formSearchHdUsuario',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchHdUsuario();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.hdUsuarioCollection = new HdUsuarioPageCollection();
			this.hdUsuarioCollection.state.pageSize = 5;
			this.hdUsuarioCollection.on('fetching', this.startFetch, this);
			this.hdUsuarioCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.hdUsuarioCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.hdUsuarioCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.hdUsuarioCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.ui.groupInputModalDatainclusao.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputModalDatainclusao.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputModalDatainclusao.mask('99/99/9999 99:99');
			});
		},

		selectRow : function(e) {
			var modelHdUsuario = util.getWrappedModel(e);
			if (modelHdUsuario){
				this.modelSelect = modelHdUsuario; 
				this.onSelectModel(modelHdUsuario);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

			{
				name : "nome",
				editable : false,
				sortable : true,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : true,
				label 	 : "Email",
				cell 	 : "string",
			}, 
			{
				name : "senha",
				editable : false,
				sortable : true,
				label 	 : "Senha",
				cell 	 : "string",
			}, 
			{
				name : "tipo",
				editable : false,
				sortable : true,
				label 	 : "Tipo",
				cell 	 : "string",
			}, 
			{
				name : "perfil",
				editable : false,
				sortable : true,
				label 	 : "Perfil",
				cell 	 : "string",
			}, 
			{
				name : "statusUsuario",
				editable : false,
				sortable : true,
				label 	 : "Status usuario",
				cell 	 : "string",
			}, 
			{
				name : "datainclusao",
				editable : false,
				sortable : true,
				label 	 : "Datainclusao",
				cell 	 : "string",
			}, 
			{
				name : "usuarioinclusao",
				editable : false,
				sortable : true,
				label 	 : "Usuarioinclusao",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalNome'); 
			util.clear('inputModalEmail'); 
			util.clear('inputModalSenha'); 
			util.clear('inputModalTipo'); 
			util.clear('inputModalPerfil'); 
			util.clear('inputModalStatusUsuario'); 
			util.clear('inputModalDatainclusao'); 
			util.clear('inputModalUsuarioinclusao'); 
			util.scrollUpModal();
		},

		searchHdUsuario : function() {
			this.hdUsuarioCollection.filterQueryParams = {
	    		nome : util.escapeById('inputModalNome'),
	    		email : util.escapeById('inputModalEmail'),
	    		senha : util.escapeById('inputModalSenha'),
	    		tipo : util.escapeById('inputModalTipo'),
	    		perfil : util.escapeById('inputModalPerfil'),
	    		statusUsuario : util.escapeById('inputModalStatusUsuario'),
	    		datainclusao : util.escapeById('inputModalDatainclusao'),
	    		usuarioinclusao : util.escapeById('inputModalUsuarioinclusao'),
			};

			this.hdUsuarioCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.hdUsuarioCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.hdUsuarioCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinHdUsuario');
		},
	});

	return HdUsuarioModal;
});
