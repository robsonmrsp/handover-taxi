/* generated: 04/11/2016 11:29:26 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var DistanciaMotoCorridaModal = require('text!views/modalComponents/tpl/DistanciaMotoCorridaModalTemplate.html');
	var DistanciaMotoCorridaPageCollection = require('collections/DistanciaMotoCorridaPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var DistanciaMotoCorridaModal = Marionette.LayoutView.extend({
		template : _.template(DistanciaMotoCorridaModal),

		events : {
			'click #btnSearchDistanciaMotoCorrida' : 'searchDistanciaMotoCorrida',
			'click #btnClearDistanciaMotoCorrida' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-distanciaMotoCorrida',
			gridRegion : 		'#grid-distanciaMotoCorrida',
			paginatorRegion : 	'#paginator-distanciaMotoCorrida',
		},

		ui : {
    		inputModalDistancia : '#inputModalDistancia',
    		inputModalSequencia : '#inputModalSequencia',
		
			form : '#formSearchDistanciaMotoCorrida',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchDistanciaMotoCorrida();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.distanciaMotoCorridaCollection = new DistanciaMotoCorridaPageCollection();
			this.distanciaMotoCorridaCollection.state.pageSize = 5;
			this.distanciaMotoCorridaCollection.on('fetching', this.startFetch, this);
			this.distanciaMotoCorridaCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.distanciaMotoCorridaCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.distanciaMotoCorridaCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.distanciaMotoCorridaCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectRow : function(e) {
			var modelDistanciaMotoCorrida = util.getWrappedModel(e);
			if (modelDistanciaMotoCorrida){
				this.modelSelect = modelDistanciaMotoCorrida; 
				this.onSelectModel(modelDistanciaMotoCorrida);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.scrollUpModal();
		},

		searchDistanciaMotoCorrida : function() {
			this.distanciaMotoCorridaCollection.filterQueryParams = {
			};

			this.distanciaMotoCorridaCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.distanciaMotoCorridaCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.distanciaMotoCorridaCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinDistanciaMotoCorrida');
		},
	});

	return DistanciaMotoCorridaModal;
});
