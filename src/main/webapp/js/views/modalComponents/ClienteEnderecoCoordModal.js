/* generated: 26/10/2016 13:30:46 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ClienteEnderecoCoordModal = require('text!views/modalComponents/tpl/ClienteEnderecoCoordModalTemplate.html');
	var ClienteEnderecoCoordPageCollection = require('collections/ClienteEnderecoCoordPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var ClienteEnderecoCoordModal = Marionette.LayoutView.extend({
		template : _.template(ClienteEnderecoCoordModal),

		events : {
			'click #btnSearchClienteEnderecoCoord' : 'searchClienteEnderecoCoord',
			'click #btnClearClienteEnderecoCoord' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-clienteEnderecoCoord',
			gridRegion : 		'#grid-clienteEnderecoCoord',
			paginatorRegion : 	'#paginator-clienteEnderecoCoord',
		},

		ui : {
    		inputModalLatitude : '#inputModalLatitude',
    		inputModalLongitude : '#inputModalLongitude',
    		inputModalEnderecoFormatado : '#inputModalEnderecoFormatado',
    		inputModalValido : '#inputModalValido',
		
			form : '#formSearchClienteEnderecoCoord',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchClienteEnderecoCoord();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.clienteEnderecoCoordCollection = new ClienteEnderecoCoordPageCollection();
			this.clienteEnderecoCoordCollection.state.pageSize = 5;
			this.clienteEnderecoCoordCollection.on('fetching', this.startFetch, this);
			this.clienteEnderecoCoordCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.clienteEnderecoCoordCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.clienteEnderecoCoordCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.clienteEnderecoCoordCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectRow : function(e) {
			var modelClienteEnderecoCoord = util.getWrappedModel(e);
			if (modelClienteEnderecoCoord){
				this.modelSelect = modelClienteEnderecoCoord; 
				this.onSelectModel(modelClienteEnderecoCoord);
			}
		},
		getJsonValue : function() {
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
			return null;
		},

		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

			{
				name : "latitude",
				editable : false,
				sortable : true,
				label 	 : "Latitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "longitude",
				editable : false,
				sortable : true,
				label 	 : "Longitude",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "enderecoFormatado",
				editable : false,
				sortable : true,
				label 	 : "Endereco formatado",
				cell 	 : "string",
			}, 
			{
				name : "valido",
				editable : false,
				sortable : true,
				label 	 : "Válido",
				cell 	 : "string",
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalLatitude'); 
			util.clear('inputModalLongitude'); 
			util.clear('inputModalEnderecoFormatado'); 
			util.clear('inputModalValido'); 
			util.scrollUpModal();
		},

		searchClienteEnderecoCoord : function() {
			this.clienteEnderecoCoordCollection.filterQueryParams = {
	    		latitude : util.escapeById('inputModalLatitude'),
	    		longitude : util.escapeById('inputModalLongitude'),
	    		enderecoFormatado : util.escapeById('inputModalEnderecoFormatado'),
	    		valido : util.escapeById('inputModalValido'),
			};

			this.clienteEnderecoCoordCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.clienteEnderecoCoordCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.clienteEnderecoCoordCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinClienteEnderecoCoord');
		},
	});

	return ClienteEnderecoCoordModal;
});
