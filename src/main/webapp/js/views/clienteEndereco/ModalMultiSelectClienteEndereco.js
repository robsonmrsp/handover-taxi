/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var ClienteEnderecoPageCollection = require('collections/ClienteEnderecoPageCollection');
	var ModalMultiSelectClienteEnderecoTemplate = require('text!views/clienteEndereco/tpl/ModalMultiSelectClienteEnderecoTemplate.html');
	// End of "Import´s" definition

	var ModalClienteEnderecos = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectClienteEnderecoTemplate),

		regions : {
			gridRegion : '#grid-clienteEnderecos-modal',
			paginatorRegion : '#paginator-clienteEnderecos-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoClienteEnderecos = this.collection;
			
			this.clienteEnderecos = new ClienteEnderecoPageCollection();
			this.clienteEnderecos.on('fetched', this.endFetch, this);
			this.clienteEnderecos.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.clienteEnderecos,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.clienteEnderecos,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.clienteEnderecos.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid clienteEndereco');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoClienteEnderecos.add(model)
			else
				this.projetoClienteEnderecos.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.clienteEnderecos.each(function(model) {
				if (that.projetoClienteEnderecos.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "endereco",
				editable : false,
				sortable : false,
				label 	 : "Endereco",
				cell 	 : "string",
			}, 
			{
				name : "numero",
				editable : false,
				sortable : false,
				label 	 : "Úmero",
				cell 	 : "string",
			}, 
			{
				name : "complemento",
				editable : false,
				sortable : false,
				label 	 : "Complemento",
				cell 	 : "string",
			}, 
			{
				name : "uf",
				editable : false,
				sortable : false,
				label 	 : "Uf",
				cell 	 : "string",
			}, 
			{
				name : "cidade",
				editable : false,
				sortable : false,
				label 	 : "Cidade",
				cell 	 : "string",
			}, 
			{
				name : "bairro",
				editable : false,
				sortable : false,
				label 	 : "Bairro",
				cell 	 : "string",
			}, 
			{
				name : "cep",
				editable : false,
				sortable : false,
				label 	 : "Cep",
				cell 	 : "string",
			}, 
			{
				name : "referencia",
				editable : false,
				sortable : false,
				label 	 : "Referência",
				cell 	 : "string",
			}, 
			{
				name : "tipo",
				editable : false,
				sortable : false,
				label 	 : "Tipo",
				cell 	 : "string",
			}, 
			{
				name : "favorito",
				editable : false,
				sortable : false,
				label 	 : "Favorito",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalClienteEnderecos;
});
