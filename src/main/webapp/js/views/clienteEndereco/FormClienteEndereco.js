/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var TemplateFormClienteEnderecos = require('text!views/clienteEndereco/tpl/FormClienteEnderecoTemplate.html');
	var ClienteEnderecoModel = require('models/ClienteEnderecoModel');
	var ClienteEnderecoCollection = require('collections/ClienteEnderecoCollection');
	var ModalCliente = require('views/modalComponents/ClienteModal');
	// End of "Import´s" definition
	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################
	var FormClienteEnderecos = Marionette.LayoutView.extend({
		template: _.template(TemplateFormClienteEnderecos),
		regions: {
			modalClienteRegion: '#clienteModal',
		},
		events: {
			'click 	.save': 'save',
			'click 	.saveAndContinue': 'saveAndContinue',
			'click 	.go-back-link': 'goBack',
			'click #searchClienteModal': 'showModalCliente',
		},
		ui: {
			inputClienteNome: '#inputClienteNome',
			inputClienteEmail: '#inputClienteEmail',
			inputClienteTipo: '#inputClienteTipo',
			inputId: '#inputId',
			inputEndereco: '#inputEndereco',
			inputNumero: '#inputNumero',
			inputComplemento: '#inputComplemento',
			inputUf: '#inputUf',
			inputCidade: '#inputCidade',
			inputBairro: '#inputBairro',
			inputCep: '#inputCep',
			inputReferencia: '#inputReferencia',
			inputTipo: '#inputTipo',
			inputFavorito: '#inputFavorito',
			inputClienteId: '#inputClienteId',
			inputClienteNome: '#inputClienteNome',
			form: '#formClienteEndereco',
		},
		initialize: function(options) {
			var that = this;
			this.cliente = options.cliente
			this.modalCliente = new ModalCliente({
				onSelectModel: function(model) {
					that.onSelectCliente(model);
				},
			});
			this.on('show', function() {
				this.modalClienteRegion.show(this.modalCliente);
				this.ui.inputCep.mask('99.999-999');
				this.ui.form.validationEngine('attach', {
					promptPosition: "topLeft",
					isOverflown: false,
					validationEventTrigger: "change"
				});
				this.carregaCliente(this.cliente);
			});
			this.checkButtonAuthority();
		},
		carregaCliente: function(cliente) {
			this.ui.inputClienteNome.val(cliente.get('nome'));
			this.ui.inputClienteEmail.val(cliente.get('email'));
			this.ui.inputClienteTipo.val(cliente.get('tipo'));
		},
		goBack: function() {
			util.goPage('app/cliente/' + this.cliente.get('id') + '/clienteEnderecos');
		},
		saveAndContinue: function() {
			this.save(true)
		},
		save: function(continua) {
			var that = this;
			var clienteEndereco = that.getModel();
			if (this.isValid()) {
				clienteEndereco.save({}, {
					success: function(_model, _resp, _options) {
						util.showSuccessMessage('Cliente endereco salvo com sucesso!');
						that.clearForm();
						if (continua != true) {
							util.goPage('app/cliente/' + that.cliente.get('id') + '/clienteEnderecos');
						}
					},
					error: function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro', _resp);
					}
				});
			}
			else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},
		clearForm: function() {
			util.clear('inputId');
			util.clear('inputEndereco');
			util.clear('inputNumero');
			util.clear('inputComplemento');
			util.clear('inputUf');
			util.clear('inputCidade');
			util.clear('inputBairro');
			util.clear('inputCep');
			util.clear('inputReferencia');
			util.clear('inputTipo');
			util.clear('inputFavorito');
			util.clear('inputClienteId');
			util.clear('inputClienteNome');
		},
		isValid: function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition: "topLeft",
				isOverflown: false,
				validationEventTrigger: "change"
			});
		},
		getModel: function() {
			var that = this;
			var clienteEndereco = that.model;
			clienteEndereco.set({
				id: util.escapeById('inputId') || null,
				endereco: util.escapeById('inputEndereco'),
				numero: util.escapeById('inputNumero'),
				complemento: util.escapeById('inputComplemento'),
				uf: util.escapeById('inputUf'),
				cidade: util.escapeById('inputCidade'),
				bairro: util.escapeById('inputBairro'),
				cep: util.onlyNumber(util.escapeById('inputCep')),
				referencia: util.escapeById('inputReferencia'),
				tipo: util.escapeById('inputTipo'),
				favorito: util.escapeById('inputFavorito'),
				cliente: that.cliente.toJSON(),
			});
			return clienteEndereco;
		},
		showModalCliente: function() {
			// add more before the modal is open
			this.modalCliente.showPage();
		},
		onSelectCliente: function(cliente) {
			this.modalCliente.hidePage();
			this.ui.inputClienteId.val(cliente.get('id'));
			this.ui.inputClienteNome.val(cliente.get('nome'));
		},
		// vitoriano : chunk : checks html button authority
		checkButtonAuthority: function() {
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_CRIAR') {
					$('#new').show();
					$('#save').show();
					$('#saveContinue').show();
				}
			})
		},
		// vitoriano : chunk
	});
	return FormClienteEnderecos;
});