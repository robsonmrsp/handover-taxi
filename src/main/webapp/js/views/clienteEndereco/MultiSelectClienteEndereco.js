/* generated: 16/10/2016 15:27:07 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectClienteEndereco = require('views/clienteEndereco/ModalMultiSelectClienteEndereco');
	var MultiSelectClienteEnderecoTemplate = require('text!views/clienteEndereco/tpl/MultiSelectClienteEnderecoTemplate.html');

	var MultiSelectClienteEndereco = Marionette.LayoutView.extend({
		template : _.template(MultiSelectClienteEnderecoTemplate),

		regions : {
			modalMultiSelectClienteEnderecoRegion : '#modalMultiSelectClienteEnderecos',
			gridClienteEnderecosModalRegion : '#gridMultiselectClienteEnderecos',
		},

		initialize : function() {
			var that = this;

			this.clienteEnderecos = this.collection;

			this.gridClienteEnderecos = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.clienteEnderecos,
			});

			this.modalMultiSelectClienteEndereco = new ModalMultiSelectClienteEndereco({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectClienteEnderecoRegion.show(that.modalMultiSelectClienteEndereco);
				that.gridClienteEnderecosModalRegion.show(that.gridClienteEnderecos);
			});
		},
		clear : function(){
			this.modalMultiSelectClienteEndereco.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "endereco",
				editable : false,
				sortable : false,
				label 	 : "Endereco",
				cell 	 : "string",
			}, 
			{
				name : "numero",
				editable : false,
				sortable : false,
				label 	 : "Úmero",
				cell 	 : "string",
			}, 
			{
				name : "complemento",
				editable : false,
				sortable : false,
				label 	 : "Complemento",
				cell 	 : "string",
			}, 
			{
				name : "uf",
				editable : false,
				sortable : false,
				label 	 : "Uf",
				cell 	 : "string",
			}, 
			{
				name : "cidade",
				editable : false,
				sortable : false,
				label 	 : "Cidade",
				cell 	 : "string",
			}, 
			{
				name : "bairro",
				editable : false,
				sortable : false,
				label 	 : "Bairro",
				cell 	 : "string",
			}, 
			{
				name : "cep",
				editable : false,
				sortable : false,
				label 	 : "Cep",
				cell 	 : "string",
			}, 
			{
				name : "referencia",
				editable : false,
				sortable : false,
				label 	 : "Referência",
				cell 	 : "string",
			}, 
			{
				name : "tipo",
				editable : false,
				sortable : false,
				label 	 : "Tipo",
				cell 	 : "string",
			}, 
			{
				name : "favorito",
				editable : false,
				sortable : false,
				label 	 : "Favorito",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectClienteEndereco
});
