/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormCidades = require('text!views/cidade/tpl/FormCidadeTemplate.html');
	var CidadeModel = require('models/CidadeModel');
	var CidadeCollection = require('collections/CidadeCollection');
	var ModalEstado = require('views/modalComponents/EstadoModal');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormCidades = Marionette.LayoutView.extend({
		template : _.template(TemplateFormCidades),

		regions : {
			modalEstadoRegion : '#estadoModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click #searchEstadoModal' : 'showModalEstado',
		},
		
		ui : {
			inputId : '#inputId',
			inputNome : '#inputNome',
			inputCep : '#inputCep',
		
			inputEstadoId : '#inputEstadoId',
			inputEstadoNome : '#inputEstadoNome',
			form : '#formCidade',
		},

		initialize : function() {
			var that = this;
			this.modalEstado = new ModalEstado({
				onSelectModel : function(model) {
					that.onSelectEstado(model);
				},
			});
			this.on('show', function() {
				this.modalEstadoRegion.show(this.modalEstado);		
		
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var cidade = that.getModel();

			if (this.isValid()) {
				cidade.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Cidade salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/cidades');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputNome'); 
			util.clear('inputCep'); 
			util.clear('inputEstadoId');
			util.clear('inputEstadoNome');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var cidade = that.model; 
			cidade.set({
				id: util.escapeById('inputId') || null,
		    	nome : util.escapeById('inputNome'), 
		    	cep : util.escapeById('inputCep'), 
				estado : that.modalEstado.getJsonValue(),
			});
			return cidade;
		},
		 		
		showModalEstado : function() {
			// add more before the modal is open
			this.modalEstado.showPage();
		},

		onSelectEstado : function(estado) {
			this.modalEstado.hidePage();	
			this.ui.inputEstadoId.val(estado.get('id'));
			this.ui.inputEstadoNome.val(estado.get('nome'));		
		},
				
		
	});

	return FormCidades;
});