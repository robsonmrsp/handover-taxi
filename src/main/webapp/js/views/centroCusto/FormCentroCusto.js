/* generated: 16/10/2016 15:27:07 */
define(function(require) {
    // Start "Import´s" Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var roles = require('adapters/auth-adapter').roles;
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var TemplateFormCentroCustos = require('text!views/centroCusto/tpl/FormCentroCustoTemplate.html');
    var CentroCustoModel = require('models/CentroCustoModel');
    var CentroCustoCollection = require('collections/CentroCustoCollection');
    // End of "Import´s" definition
    // #####################################################################################################
    // ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
    // BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
    // #####################################################################################################
    var FormCentroCustos = Marionette.LayoutView.extend({
        template: _.template(TemplateFormCentroCustos),
        regions: {
            modalEmpresaRegion: '#empresaModal',
        },
        events: {
            'click 	.save' : 'save',
			'click 	.go-back-link' : 'goBack',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click #searchEmpresaModal' : 'showModalEmpresa',
			'change #inputValorLimite' : 'tratarValorInteiro',
        },
        ui: {
            inputEmpresaNomeFantasia: '#inputEmpresaNomeFantasia',
            inputEmpresaCnpj: '#inputEmpresaCnpj',
            inputEmpresaContrato: '#inputEmpresaContrato',
            inputEmpresaRazaoSocial: '#inputEmpresaRazaoSocial',
            inputId: '#inputId',
            inputDescricao: '#inputDescricao',
            inputValorLimite: '#inputValorLimite',
            inputObservacao: '#inputObservacao',
            inputInativo: '#inputInativo',
            inputEmpresaId: '#inputEmpresaId',
            inputEmpresaNome: '#inputEmpresaNome',
            form: '#formCentroCusto',
            newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
        },
        initialize: function(options) {
            this.empresa = options.empresa || new EmpresaModel(this.model.get('empresa'));
            var that = this;
            this.on('show', function() {
                this.modalEmpresaRegion.show(this.modalEmpresa);
                // this.ui.inputValorLimite.formatNumber(2);
                this.ui.inputValorLimite.mask('###.###.###.###,00', {
                    reverse: true
                });
                // arranjo para poder fazer a formatação cocordante com o plugin
                // de mascara
                this.ui.inputValorLimite.val(util.printFormatNumber(this.model.get('valorLimite')));
                this.ui.form.validationEngine('attach', {
                    promptPosition: "topLeft",
                    isOverflown: false,
                    validationEventTrigger: "change"
                });
                this.carregaEmpresa(this.empresa);
                this.checkButtonAuthority();
            });
        },
        carregaEmpresa: function(empresa) {
            this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));
            this.ui.inputEmpresaCnpj.val(empresa.get('cnpj'));
            this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
            this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));
        },
        goBack: function() {
            util.goPage('app/empresa/' + this.empresa.get('id') + '/centroCustos');
        },
        saveAndContinue: function() {
            this.save(true)
        },
        save: function(continua) {
            var that = this;
            var centroCusto = that.getModel();
            if (this.isValid()) {
                centroCusto.save({}, {
                    success: function(_model, _resp, _options) {
                        util.showSuccessMessage('Centro custo salvo com sucesso!');
                        that.clearForm();
                        if (continua != true) {
                            util.goPage('app/empresa/' + that.empresa.get('id') + '/centroCustos');
                        }
                    },
                    error: function(_model, _resp, _options) {
                        util.showErrorMessage('Problema ao salvar registro', _resp);
                    }
                });
            }
            else {
                util.showMessage('error', 'Verifique campos em destaque!');
            }
        },
        clearForm: function() {
            util.clear('inputId');
            util.clear('inputDescricao');
            util.clear('inputValorLimite');
            util.clear('inputObservacao');
            util.clear('inputInativo');
            util.clear('inputEmpresaId');
            util.clear('inputEmpresaNome');
        },
        isValid: function() {
            return this.ui.form.validationEngine('validate', {
                promptPosition: "topLeft",
                isOverflown: false,
                validationEventTrigger: "change"
            });
        },
        // Gambiarra necessária para validação de valor inteiro que é apagado
		// Tratar na máscara
		tratarValorInteiro : function() {
			if(this.ui.inputValorLimite.val() % 1 == 0 )
				this.ui.inputValorLimite.val(util.formatFinalNumber(this.ui.inputValorLimite.val()));
		},
        getModel: function() {
            var that = this;
            var centroCusto = that.model;
            centroCusto.set({
                id: util.escapeById('inputId') || null,
                descricao: util.escapeById('inputDescricao'),
                valorLimite: util.escapeById('inputValorLimite', true),
                observacao: util.escapeById('inputObservacao'),
                inativo: util.escapeById('inputInativo'),
                empresa: that.empresa.toJSON(),
            });
            return centroCusto;
        },
        // vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_CENTROCUSTO_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
				if(e.authority == 'ROLE_CENTROCUSTO_EDITAR' || e.authority == 'ROLE_ADMIN') {
					saveButton.show();
				}
			})
		},
	});
	return FormCentroCustos;
});