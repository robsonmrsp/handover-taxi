/* generated: 16/10/2016 15:27:07 */
define(function(require) {
    // Start "Import´s Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var roles = require('adapters/auth-adapter').roles;
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var CustomStringCell = require('views/components/CustomStringCell');
    var Counter = require('views/components/Counter');
    var ActionsCell = require('views/components/ActionsCell');
    var GeneralActionsCell = require('views/components/GeneralActionsCell');
    var CustomNumberCell = require('views/components/CustomNumberCell');
    var CentroCustoModel = require('models/CentroCustoModel');
    var CentroCustoCollection = require('collections/CentroCustoCollection');
    var CentroCustoPageCollection = require('collections/CentroCustoPageCollection');
    var PageCentroCustoTemplate = require('text!views/centroCusto/tpl/PageCentroCustoTemplate.html');
    var ModalEmpresa = require('views/modalComponents/EmpresaModal');
    // End of "Import´s" definition
    var PageCentroCusto = Marionette.LayoutView.extend({
        template: _.template(PageCentroCustoTemplate),
        regions: {
            gridRegion: '#grid',
            counterRegion: '#counter',
            paginatorRegion: '#paginator',
            modalEmpresaRegion: '#empresaModal',
        },
        events: {
            'click 	#reset': 'resetCentroCusto',
            'click 	.novo-centroCusto': 'novoCentroCusto',
            'click 	.voltar-empresa': 'voltarEmpresa',
            'click #searchEmpresaModal': 'showModalEmpresa',
            'keypress': 'treatKeypress',
            'click 	.search-button': 'searchCentroCusto',
            'click .show-advanced-search-button': 'toggleAdvancedForm',
        },
        ui: {
            inputEmpresaNomeFantasia: '#inputEmpresaNomeFantasia',
            inputEmpresaCnpj: '#inputEmpresaCnpj',
            inputEmpresaContrato: '#inputEmpresaContrato',
            inputEmpresaRazaoSocial: '#inputEmpresaRazaoSocial',
            inputDescricao: '#inputDescricao',
            inputValorLimite: '#inputValorLimite',
            inputObservacao: '#inputObservacao',
            inputInativo: '#inputInativo',
            inputEmpresaId: '#inputEmpresaId',
            inputEmpresaNome: '#inputEmpresaNome',
            form: '#formCentroCustoFilter',
            advancedSearchForm: '.advanced-search-form',
            newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
        },
        toggleAdvancedForm: function() {
            this.ui.advancedSearchForm.slideToggle("slow");
        },
        treatKeypress: function(e) {
            if (util.enterPressed(e)) {
                e.preventDefault();
                this.searchCentroCusto();
            }
        },
        initialize: function(options) {
            var that = this;
            this.empresa = options.empresa;
            this.centroCustos = new CentroCustoPageCollection();
            this.grid = new Backgrid.Grid({
                className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
                columns: this.getColumns(),
                emptyText: "Sem registros",
                collection: this.centroCustos
            });
            this.counter = new Counter({
                collection: this.centroCustos,
            });
            this.paginator = new Backgrid.Extension.Paginator({
                columns: this.getColumns(),
                collection: this.centroCustos,
                className: ' paging_simple_numbers',
                uiClassName: 'pagination',
            });
            this.modalEmpresa = new ModalEmpresa({
                onSelectModel: function(model) {
                    that.onSelectEmpresa(model);
                },
            });
            this.on('show', function() {
                this.searchCentroCusto();
                that.gridRegion.show(that.grid);
                that.counterRegion.show(that.counter);
                that.paginatorRegion.show(that.paginator);
                this.ui.inputValorLimite.formatNumber(2);
                this.carregaEmpresa(this.empresa);
                this.checkButtonAuthority();
            });
        },
        carregaEmpresa: function(empresa) {
            this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));
            this.ui.inputEmpresaCnpj.val(empresa.get('cnpj'));
            this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
            this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));
        },
        searchCentroCusto: function() {
            var that = this;
            var inativo_;
            var value = this.ui.inputInativo.val()
            // vitoriano : refactor
            if (value == -1) {
                inativo_ = null;
            }
            else if (value == 0) {
                inativo_ = false;
            }
            else if (value == 1) {
                inativo_ = true;
            }
            else {
                // do nothing
            }
            // vitoriano 
            this.centroCustos.filterQueryParams = {
                descricao: util.escapeById('inputDescricao'),
                valorLimite: util.escapeById('inputValorLimite'),
                observacao: util.escapeById('inputObservacao'),
                inativo: inativo_,
                empresa: that.empresa.get('id'),
            }
            this.centroCustos.fetch({
                success: function(_coll, _resp, _opt) {
                    console.info('Consulta para o grid centroCusto');
                },
                error: function(_coll, _resp, _opt) {
                    console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
                },
                complete: function() {},
            })
        },
        resetCentroCusto: function() {
            this.ui.form.get(0).reset();
            this.centroCustos.reset();
            util.clear('inputEmpresaId');
        },
        getColumns: function() {
            var that = this;
            var columns = [{
                name: "descricao",
                editable: false,
                sortable: true,
                label: "Descrição",
                cell: "string",
            }, {
                name: "valorLimite",
                editable: false,
                sortable: true,
                label: "Valor limite",
                cell: 'number',
                formatter: _.extend({}, Backgrid.CellFormatter.prototype, {
                    fromRaw: function(rawValue, model) {
                        return 'R$ ' + util.printFormatNumber(rawValue);
                    }
                })
            }, {
                name: "observacao",
                editable: false,
                sortable: true,
                label: "Observação",
                cell: "string",
            }, {
                name: "inativo",
                editable: false,
                sortable: true,
                label: "Status",
                cell: "string",
                formatter: _.extend({}, Backgrid.CellFormatter.prototype, {
                    fromRaw: function(rawValue, model) {
                        return (rawValue ? "Inativo" : "Ativo");
                    }
                })
            }, {
                name: "acoes",
                label: "Ações(Editar, Deletar)",
                sortable: false,
                cell: GeneralActionsCell.extend({
                    buttons: that.getCellButtons(),
                    context: that,
                })
            }];
            return columns;
        },
        getCellButtons: function() {
            var that = this;
            var buttons = this.checkGridButtonAuthority();
            return buttons;
        },
        deleteModel: function(model) {
            var that = this;
            var modelTipo = new CentroCustoModel({
                id: model.id,
            });
            util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
                if (yes) {
                    modelTipo.destroy({
                        success: function() {
                            that.centroCustos.remove(model);
                            util.showSuccessMessage('Centro custo removido com sucesso!');
                        },
                        error: function(_model, _resp) {
                            util.showErrorMessage('Problema ao remover o registro', _resp);
                        }
                    });
                }
            });
        },
        novoCentroCusto: function() {
            util.goPage("app/empresa/" + this.empresa.get('id') + "/newCentroCusto");
        },
        voltarEmpresa: function() {
            util.goPage("app/empresas");
        },
        editModel: function(model) {
            util.goPage("app/empresa/" + this.empresa.get('id') + "/editCentroCusto/" + model.get('id'));
        },
        // vitoriano : chunck : check grid buttons authority
        checkGridButtonAuthority: function() {
            var that = this;
            var buttons = [];
            $.grep(roles, function(e) {
                if (e.authority == 'ROLE_CENTROCUSTO_EDITAR' || e.authority == 'ROLE_ADMIN') {
                    buttons.push({
                        id: 'edita_ficha_button',
                        type: 'primary',
                        icon: 'icon-pencil fa-pencil',
                        hint: 'Editar Centro custo',
                        onClick: that.editModel,
                    });
                }
                if (e.authority == 'ROLE_CENTROCUSTO_DELETAR' || e.authority == 'ROLE_ADMIN') {
                    buttons.push({
                        id: 'delete_button',
                        type: 'danger',
                        icon: 'icon-trash fa-trash',
                        hint: 'Remover Centro custo',
                        onClick: that.deleteModel,
                    });
                }
            })
            return buttons;
        },
        // vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_CENTROCUSTO_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
    });
    return PageCentroCusto;
});