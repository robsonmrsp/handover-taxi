/* generated: 18/10/2016 23:48:44 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectPlantao = require('views/plantao/ModalMultiSelectPlantao');
	var MultiSelectPlantaoTemplate = require('text!views/plantao/tpl/MultiSelectPlantaoTemplate.html');

	var MultiSelectPlantao = Marionette.LayoutView.extend({
		template : _.template(MultiSelectPlantaoTemplate),

		regions : {
			modalMultiSelectPlantaoRegion : '#modalMultiSelectPlantaos',
			gridPlantaosModalRegion : '#gridMultiselectPlantaos',
		},

		initialize : function() {
			var that = this;

			this.plantaos = this.collection;

			this.gridPlantaos = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.plantaos,
			});

			this.modalMultiSelectPlantao = new ModalMultiSelectPlantao({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectPlantaoRegion.show(that.modalMultiSelectPlantao);
				that.gridPlantaosModalRegion.show(that.gridPlantaos);
			});
		},
		clear : function(){
			this.modalMultiSelectPlantao.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "dataInicio",
				editable : false,
				sortable : false,
				label 	 : "Data inicio",
				cell 	 : "string",
			}, 
			{
				name : "dataFim",
				editable : false,
				sortable : false,
				label 	 : "Data fim",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectPlantao
});
