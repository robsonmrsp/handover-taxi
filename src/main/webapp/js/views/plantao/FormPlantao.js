/* generated: 18/10/2016 23:48:44 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormPlantaos = require('text!views/plantao/tpl/FormPlantaoTemplate.html');
	var PlantaoModel = require('models/PlantaoModel');
	var PlantaoCollection = require('collections/PlantaoCollection');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormPlantaos = Marionette.LayoutView.extend({
		template : _.template(TemplateFormPlantaos),

		regions : {
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
		},
		
		ui : {
			inputId : '#inputId',
			inputDataInicio : '#inputDataInicio',
			groupInputDataInicio : '#groupInputDataInicio',
			inputDataFim : '#inputDataFim',
			groupInputDataFim : '#groupInputDataFim',
		
			form : '#formPlantao',
		},

		initialize : function() {
			var that = this;
			this.on('show', function() {
		
				this.ui.groupInputDataInicio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataInicio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataInicio.mask('99/99/9999 99:99');
				this.ui.groupInputDataFim.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataFim.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataFim.mask('99/99/9999 99:99');
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var plantao = that.getModel();

			if (this.isValid()) {
				plantao.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Plantao salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/plantaos');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputDataInicio'); 
			util.clear('inputDataFim'); 
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var plantao = that.model; 
			plantao.set({
				id: util.escapeById('inputId') || null,
		    	dataInicio : util.escapeById('inputDataInicio'), 
		    	dataFim : util.escapeById('inputDataFim'), 
			});
			return plantao;
		},
		 		

				
		
	});

	return FormPlantaos;
});