/* generated: 18/10/2016 23:48:44 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var PlantaoPageCollection = require('collections/PlantaoPageCollection');
	var ModalMultiSelectPlantaoTemplate = require('text!views/plantao/tpl/ModalMultiSelectPlantaoTemplate.html');
	// End of "Import´s" definition

	var ModalPlantaos = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectPlantaoTemplate),

		regions : {
			gridRegion : '#grid-plantaos-modal',
			paginatorRegion : '#paginator-plantaos-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoPlantaos = this.collection;
			
			this.plantaos = new PlantaoPageCollection();
			this.plantaos.on('fetched', this.endFetch, this);
			this.plantaos.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.plantaos,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.plantaos,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.plantaos.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid plantao');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoPlantaos.add(model)
			else
				this.projetoPlantaos.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.plantaos.each(function(model) {
				if (that.projetoPlantaos.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "dataInicio",
				editable : false,
				sortable : false,
				label 	 : "Data inicio",
				cell 	 : "string",
			}, 
			{
				name : "dataFim",
				editable : false,
				sortable : false,
				label 	 : "Data fim",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalPlantaos;
});
