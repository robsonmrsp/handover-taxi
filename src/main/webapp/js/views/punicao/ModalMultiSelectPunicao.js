/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var PunicaoPageCollection = require('collections/PunicaoPageCollection');
	var ModalMultiSelectPunicaoTemplate = require('text!views/punicao/tpl/ModalMultiSelectPunicaoTemplate.html');
	// End of "Import´s" definition

	var ModalPunicaos = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectPunicaoTemplate),

		regions : {
			gridRegion : '#grid-punicaos-modal',
			paginatorRegion : '#paginator-punicaos-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoPunicaos = this.collection;
			
			this.punicaos = new PunicaoPageCollection();
			this.punicaos.on('fetched', this.endFetch, this);
			this.punicaos.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.punicaos,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.punicaos,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.punicaos.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid punicao');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoPunicaos.add(model)
			else
				this.projetoPunicaos.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.punicaos.each(function(model) {
				if (that.projetoPunicaos.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "motivo",
				editable : false,
				sortable : false,
				label 	 : "Motivo",
				cell 	 : "string",
			}, 
			{
				name : "dataInicio",
				editable : false,
				sortable : false,
				label 	 : "Data inicio",
				cell 	 : "string",
			}, 
			{
				name : "dataFim",
				editable : false,
				sortable : false,
				label 	 : "Data fim",
				cell 	 : "string",
			}, 
			{
				name : "quantidadetempo",
				editable : false,
				sortable : false,
				label 	 : "QuantidadeTempo",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return ModalPunicaos;
});
