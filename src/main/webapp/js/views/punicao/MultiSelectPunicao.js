/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectPunicao = require('views/punicao/ModalMultiSelectPunicao');
	var MultiSelectPunicaoTemplate = require('text!views/punicao/tpl/MultiSelectPunicaoTemplate.html');

	var MultiSelectPunicao = Marionette.LayoutView.extend({
		template : _.template(MultiSelectPunicaoTemplate),

		regions : {
			modalMultiSelectPunicaoRegion : '#modalMultiSelectPunicaos',
			gridPunicaosModalRegion : '#gridMultiselectPunicaos',
		},

		initialize : function() {
			var that = this;

			this.punicaos = this.collection;

			this.gridPunicaos = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.punicaos,
			});

			this.modalMultiSelectPunicao = new ModalMultiSelectPunicao({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectPunicaoRegion.show(that.modalMultiSelectPunicao);
				that.gridPunicaosModalRegion.show(that.gridPunicaos);
			});
		},
		clear : function(){
			this.modalMultiSelectPunicao.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "motivo",
				editable : false,
				sortable : false,
				label 	 : "Motivo",
				cell 	 : "string",
			}, 
			{
				name : "dataInicio",
				editable : false,
				sortable : false,
				label 	 : "Data inicio",
				cell 	 : "string",
			}, 
			{
				name : "dataFim",
				editable : false,
				sortable : false,
				label 	 : "Data fim",
				cell 	 : "string",
			}, 
			{
				name : "quantidadetempo",
				editable : false,
				sortable : false,
				label 	 : "QuantidadeTempo",
				cell : CustomNumberCell.extend({}),
			}, 
			];
			return columns;
		},
	});

	return MultiSelectPunicao
});
