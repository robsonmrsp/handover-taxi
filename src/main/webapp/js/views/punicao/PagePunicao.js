/* generated: 16/10/2016 15:27:08 */
define(function(require) {
    // Start "Import´s Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var roles = require('adapters/auth-adapter').roles;
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var CustomStringCell = require('views/components/CustomStringCell');
    var Counter = require('views/components/Counter');
    var ActionsCell = require('views/components/ActionsCell');
    var GeneralActionsCell = require('views/components/GeneralActionsCell');
    var CustomNumberCell = require('views/components/CustomNumberCell');
    var PunicaoModel = require('models/PunicaoModel');
    var PunicaoCollection = require('collections/PunicaoCollection');
    var PunicaoPageCollection = require('collections/PunicaoPageCollection');
    var PagePunicaoTemplate = require('text!views/punicao/tpl/PagePunicaoTemplate.html');
    var ModalMotorista = require('views/modalComponents/MotoristaModal');
    // End of "Import´s" definition
    var PagePunicao = Marionette.LayoutView.extend({
        template: _.template(PagePunicaoTemplate),
        regions: {
            gridRegion: '#grid',
            counterRegion: '#counter',
            paginatorRegion: '#paginator',
            modalMotoristaRegion: '#motoristaModal',
        },
        events: {
            'click 	#reset': 'resetPunicao',
            'click #searchMotoristaModal': 'showModalMotorista',
            'keypress': 'treatKeypress',
            'click 	.nova-punicao': 'novaPunicao',
            'click 	.voltar-motorista': 'voltarMotorista',
            'click 	.search-button': 'searchPunicao',
            'click .show-advanced-search-button': 'toggleAdvancedForm',
        },
        ui: {
            inputMotoristaNome: '#inputMotoristaNome',
            inputMotoristaViatura: '#inputMotoristaViatura',
            inputMotoristaEmail: '#inputMotoristaEmail',
            inputMotivo: '#inputMotivo',
            inputDataInicio: '#inputDataInicio',
            groupInputDataInicio: '#groupInputDataInicio',
            inputDataFim: '#inputDataFim',
            groupInputDataFim: '#groupInputDataFim',
            inputQuantidadetempo: '#inputQuantidadetempo',
            inputMotoristaId: '#inputMotoristaId',
            inputMotoristaNome: '#inputMotoristaNome',
            form: '#formPunicaoFilter',
            advancedSearchForm: '.advanced-search-form',
            newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
        },
        toggleAdvancedForm: function() {
            this.ui.advancedSearchForm.slideToggle("slow");
        },
        treatKeypress: function(e) {
            if (util.enterPressed(e)) {
                e.preventDefault();
                this.searchPunicao();
            }
        },
        initialize: function(options) {
            var that = this;
            this.motorista = options.motorista;
            this.punicaos = new PunicaoPageCollection();
            this.grid = new Backgrid.Grid({
                className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
                columns: this.getColumns(),
                emptyText: "Sem registros",
                collection: this.punicaos
            });
            this.counter = new Counter({
                collection: this.punicaos,
            });
            this.paginator = new Backgrid.Extension.Paginator({
                columns: this.getColumns(),
                collection: this.punicaos,
                className: ' paging_simple_numbers',
                uiClassName: 'pagination',
            });
            this.punicaos.getFirstPage({
                success: function(_col, _resp, _opts) {
                    console.info('Primeira pagina do grid punicao');
                },
                error: function(_col, _resp, _opts) {
                    console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
                },
                data: {
                    motorista: that.motorista.get('id'),
                }
            });
            this.modalMotorista = new ModalMotorista({
                onSelectModel: function(model) {
                    that.onSelectMotorista(model);
                },
            });
            this.on('show', function() {
                that.gridRegion.show(that.grid);
                that.counterRegion.show(that.counter);
                that.paginatorRegion.show(that.paginator);
                this.modalMotoristaRegion.show(this.modalMotorista);
                this.ui.groupInputDataInicio.datetimepicker({
                    pickTime: true,
                    language: 'pt_BR',
                });
                this.ui.inputDataInicio.datetimepicker({
                    pickTime: true,
                    language: 'pt_BR',
                });
                this.ui.inputDataInicio.mask('99/99/9999 99:99');
                this.ui.groupInputDataFim.datetimepicker({
                    pickTime: true,
                    language: 'pt_BR',
                });
                this.ui.inputDataFim.datetimepicker({
                    pickTime: true,
                    language: 'pt_BR',
                });
                this.ui.inputDataFim.mask('99/99/9999 99:99');
                this.ui.inputQuantidadetempo.formatNumber(2);
                this.carregaMotorista(this.motorista);
                this.checkButtonAuthority();
            });
        },
        carregaMotorista: function(motorista) {
            this.ui.inputMotoristaNome.val(motorista.get('nome'));
            this.ui.inputMotoristaViatura.val(motorista.get('viatura'));
            this.ui.inputMotoristaEmail.val(motorista.get('email'));
        },
        searchPunicao: function() {
            var that = this;
            this.punicaos.filterQueryParams = {
                motivo: util.escapeById('inputMotivo'),
                dataInicio: util.escapeById('inputDataInicio'),
                dataFim: util.escapeById('inputDataFim'),
                quantidadetempo: util.escapeById('inputQuantidadetempo'),
            }
            this.punicaos.fetch({
                success: function(_coll, _resp, _opt) {
                    console.info('Consulta para o grid punicao');
                },
                error: function(_coll, _resp, _opt) {
                    console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
                },
                complete: function() {},
            })
        },
        resetPunicao: function() {
            this.ui.form.get(0).reset();
            this.punicaos.reset();
            util.clear('inputMotoristaId');
        },
        getColumns: function() {
            var that = this;
            var columns = [{
                name: "motivo",
                editable: false,
                sortable: false,
                label: "Motivo",
                cell: "string",
                formatter: _.extend({}, Backgrid.CellFormatter.prototype, {
                    fromRaw: function(rawValue, model) {
                        return rawValue.descricao;
                    }
                })
            }, {
                name: "dataInicio",
                editable: false,
                sortable: true,
                label: "Data inicio",
                cell: "string",
            }, {
                name: "dataFim",
                editable: false,
                sortable: true,
                label: "Data fim",
                cell: "string",
            }, {
                name: "quantidadetempo",
                editable: false,
                sortable: true,
                label: "Duração (Horas)",
                cell: Backgrid.IntegerCell.extend({
                    quantidadetempo: ''
                })
            }, {
                name: "acoes",
                label: "Ações(Editar, Deletar)",
                sortable: false,
                cell: GeneralActionsCell.extend({
                    buttons: that.getCellButtons(),
                    context: that,
                })
            }];
            return columns;
        },
        getCellButtons: function() {
            var that = this;
            var buttons = this.checkGridButtonAuthority();
            return buttons;
        },
        deleteModel: function(model) {
            var that = this;
            var modelTipo = new PunicaoModel({
                id: model.id,
            });
            util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
                if (yes) {
                    modelTipo.destroy({
                        success: function() {
                            that.punicaos.remove(model);
                            util.showSuccessMessage('Punicao removido com sucesso!');
                        },
                        error: function(_model, _resp) {
                            util.showErrorMessage('Problema ao remover o registro', _resp);
                        }
                    });
                }
            });
        },
        editModel: function(model) {
            util.goPage("app/motorista/" + this.motorista.get('id') + "/editPunicao/" + model.get('id'));
        },
        showModalMotorista: function() {
            this.modalMotorista.showPage();
        },
        onSelectMotorista: function(motorista) {
            this.modalMotorista.hidePage();
            this.ui.inputMotoristaId.val(motorista.get('id'));
            this.ui.inputMotoristaNome.val(motorista.get('nome'));
        },
        novaPunicao: function() {
            util.goPage("app/motorista/" + this.motorista.get('id') + "/newPunicao");
        },
        voltarMotorista: function() {
            util.goPage("app/motoristas");
        },
        // vitoriano : chunck : check grid buttons authority
        checkGridButtonAuthority: function() {
            var that = this;
            var buttons = [];
            $.grep(roles, function(e) {
                if (e.authority == 'ROLE_PUNICAO_EDITAR' || e.authority == 'ROLE_ADMIN') {
                    buttons.push({
                        id: 'edita_ficha_button',
                        type: 'primary',
                        icon: 'icon-pencil fa-pencil',
                        hint: 'Editar Punicao',
                        onClick: that.editModel,
                    });
                }
                if (e.authority == 'ROLE_PUNICAO_DELETAR' || e.authority == 'ROLE_ADMIN') {
                    buttons.push({
                        id: 'delete_button',
                        type: 'danger',
                        icon: 'icon-trash fa-trash',
                        hint: 'Remover Punicao',
                        onClick: that.deleteModel,
                    });
                }
            })
            return buttons;
        },
        // vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_PUNICAO_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
    });
    return PagePunicao;
});