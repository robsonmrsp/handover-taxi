/* generated: 16/10/2016 15:27:08 */
define(function(require) {
    // Start "Import´s" Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var roles = require('adapters/auth-adapter').roles;
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var TemplateFormPunicaos = require('text!views/punicao/tpl/FormPunicaoTemplate.html');
    var PunicaoModel = require('models/PunicaoModel');
    var PunicaoCollection = require('collections/PunicaoCollection');
    var MensagemMotoristaModel = require('models/MensagemMotoristaModel');
    var MotivoPunicaoCollection = require('collections/MotivoPunicaoCollection');
    var ModalMotorista = require('views/modalComponents/MotoristaModal');
    var FormPunicaos = Marionette.LayoutView.extend({
        template: _.template(TemplateFormPunicaos),
        regions: {
            modalMotoristaRegion: '#motoristaModal',
        },
        events: {
            'click 	.save': 'save',
            'click 	.go-back-link': 'goBack',
            'click 	.saveAndContinue': 'saveAndContinue',
            'click #searchMotoristaModal': 'showModalMotorista',
            'blur #groupInputDataInicio': 'isDateValid',
            'blur #groupInputDataFim': 'isDateValid',
        },
        ui: {
            inputMotoristaNome: '#inputMotoristaNome',
            inputMotoristaViatura: '#inputMotoristaViatura',
            inputMotoristaEmail: '#inputMotoristaEmail',
            inputId: '#inputId',
            inputMotivo: '#inputMotivo',
            inputDataInicio: '#inputDataInicio',
            groupInputDataInicio: '#groupInputDataInicio',
            inputDataFim: '#inputDataFim',
            groupInputDataFim: '#groupInputDataFim',
            inputQuantidadetempo: '#inputQuantidadetempo',
            inputMotoristaId: '#inputMotoristaId',
            inputMotoristaNome: '#inputMotoristaNome',
            form: '#formPunicao',
            newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
        },
        initialize: function(options) {
            var that = this;
            this.motorista = options.motorista;
            this.modalMotorista = new ModalMotorista({
                onSelectModel: function(model) {
                    that.onSelectMotorista(model);
                },
            });
            this.on('show', function() {
                this.comboPunicao = new Combobox({
                    el: this.ui.inputMotivo,
                    comboId: 'id',
                    comboVal: 'descricao',
                    collectionEntity: MotivoPunicaoCollection
                })
                this.comboPunicao.setValue(this.model.get('motivo'));
                this.modalMotoristaRegion.show(this.modalMotorista);
                this.ui.groupInputDataInicio.datetimepicker({
                    pickTime: true,
                    language: 'pt_BR',
                });
                this.ui.inputDataInicio.datetimepicker({
                    pickTime: true,
                    language: 'pt_BR',
                });
                this.ui.inputDataInicio.mask('99/99/9999 99:99');
                this.ui.groupInputDataFim.datetimepicker({
                    pickTime: true,
                    language: 'pt_BR',
                });
                this.ui.inputDataFim.datetimepicker({
                    pickTime: true,
                    language: 'pt_BR',
                });
                this.ui.inputDataFim.mask('99/99/9999 99:99');
                this.ui.inputQuantidadetempo.formatNumber(2);
                this.ui.form.validationEngine('attach', {
                    promptPosition: "topLeft",
                    isOverflown: false,
                    validationEventTrigger: "change"
                });
                this.carregaMotorista(this.motorista);
                this.checkButtonAuthority();
            });
        },
        carregaMotorista: function(motorista) {
            this.ui.inputMotoristaNome.val(motorista.get('nome'));
            this.ui.inputMotoristaId.val(motorista.get('id'));
            this.ui.inputMotoristaViatura.val(motorista.get('viatura'));
            this.ui.inputMotoristaEmail.val(motorista.get('email'));
        },
        saveAndContinue: function() {
            this.save(true)
        },
        goBack: function() {
            util.goPage('app/motorista/' + this.motorista.get('id') + '/punicaos');
        },
        save: function(continua) {
            var that = this;
            var punicao = that.getModel();
            if (this.isValid()) {
                punicao.save({}, {
                    success: function(_model, _resp, _options) {
                        util.showSuccessMessage('Punicao salvo com sucesso!');
                        that.clearForm();
                        // vitoriano : nem tudo na vida e bonito :-) : refactor
                        var dataInicio = _model.get('dataInicio');
                        var dataFim = _model.get('dataFim');
                        var mensagemMotorista = new MensagemMotoristaModel();
                        mensagemMotorista.set({
                            id: null,
                            dataEnvio: null,
                            dataLeitura: null,
                            motorista: {
                                id: _model.get('motorista').id,
                            },
                            mensagem: {
                                id: null,
                                descricao: 'Você foi punido de ' + dataInicio + ' a ' + dataFim,
                            },
                        });
                        mensagemMotorista.save({}, {
                            success: function(_model, _resp, _options) {
                                var ref = new Firebase("https://chat-eeb48.firebaseio.com/").child("messages");
                                var key = _model.get('motorista').id + ":" + 'CENTRAL00001';
                                var message = {
                                    'id': _model.get('id'),
                                    'text': 'Você foi punido de ' + dataInicio + ' a ' + dataFim,
                                    'timestamp': moment(_model.get('dataEnvio'), "DD/MM/YYYY hh:mm:ss").unix(),
                                };
                                ref.child(key).child(_model.get('id')).set(message);
                            },
                            error: function(_model, _resp, _options) {
                                console.log('erro', _resp);
                            }
                        });
                        if (continua != true) {
                            util.goPage('app/motorista/' + that.motorista.get('id') + '/punicaos');
                        }
                    },
                    error: function(_model, _resp, _options) {
                        util.showErrorMessage('Problema ao salvar registro', _resp);
                    }
                });
            }
            else {
                util.showMessage('error', 'Verifique campos em destaque!');
            }
        },
        clearForm: function() {
            util.clear('inputId');
            util.clear('inputMotivo');
            util.clear('inputDataInicio');
            util.clear('inputDataFim');
            util.clear('inputQuantidadetempo');
            util.clear('inputMotoristaId');
            util.clear('inputMotoristaNome');
        },
        isValid: function() {
            return this.ui.form.validationEngine('validate', {
                promptPosition: "topLeft",
                isOverflown: false,
                validationEventTrigger: "change"
            });
        },
        getModel: function() {
            var that = this;
            var punicao = that.model;
            punicao.set({
                id: util.escapeById('inputId') || null,
                motivo: this.comboPunicao.getJsonValue(),
                dataInicio: util.escapeById('inputDataInicio'),
                dataFim: util.escapeById('inputDataFim'),
                quantidadetempo: util.escapeById('inputQuantidadetempo'),
                motorista: that.motorista.toJSON(),
            });
            return punicao;
        },
        isDateValid: function() {
            // input : none
            // output : checks to see if dates are valid and sets field inputQuantidadetempo to its diff
            var sd = this.parseDate(util.escapeById('inputDataInicio')); // start date
            var ed = this.parseDate(util.escapeById('inputDataFim')); // end date
            if (sd && ed) {
                var td = new Date(ed[2], ed[1], ed[0], ed[3], ed[4]) - new Date(sd[2], sd[1], sd[0], sd[3], sd[4]); // diff date
            }
            if (typeof td != 'undefined') {
                this.ui.inputQuantidadetempo.val(Math.ceil(td / (60 * 60 * 1000))); // sets diff date normalized to hours	
            }
        },
        parseDate: function(date) {
            // input : 28-12-2016 10:37 format
            // output : ['28', '12', '2016', '10', '37']
            if (date) {
                var date_ = date.split(" ");
                return date_[0].split("/").concat(date_[1].split(":"));
            }
            return null;
        },
        sendNotification: function(punicao, message) {
            // input : obj motoqueiro, punicao and message
            // output : saves a tuple in Firebase related to motoqueiro, punicao and message
            var ref = new Firebase("https://chat-eeb48.firebaseio.com/").child("notifications");
            var key = punicao.get('motorista').id + ":" + 'CENTRAL00001';
            var message = 'Você está punido!';
            var notification = {
                'id': punicao.get('id'),
                'text': message,
                'inicio': _model.get('dataInicio'),
                'fim': punicao.get('dataFim'),
                'timestamp': moment(punicao.get('createDatetime'), "DD/MM/YYYY hh:mm:ss").unix(),
            }
            ref.child(key).child(punicao.get('id')).set(notification);
            //
        },
        showModalMotorista: function() {
            // add more before the modal is open
            this.modalMotorista.showPage();
        },
        onSelectMotorista: function(motorista) {
            this.modalMotorista.hidePage();
            this.ui.inputMotoristaId.val(motorista.get('id'));
            this.ui.inputMotoristaNome.val(motorista.get('nome'));
        },
        // vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_PUNICAO_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
				if(e.authority == 'ROLE_PUNICAO_EDITAR' || e.authority == 'ROLE_ADMIN') {
					saveButton.show();
				}			
			})
		},
    });
    return FormPunicaos;
});