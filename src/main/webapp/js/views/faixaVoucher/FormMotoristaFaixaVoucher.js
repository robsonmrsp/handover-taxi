/* generated: 16/10/2016 15:27:08 */
define(function(require) {
    // Start "Import´s" Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var roles = require('adapters/auth-adapter').roles;
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var TemplateFormFaixaVouchers = require('text!views/faixaVoucher/tpl/FormMotoristaFaixaVoucherTemplate.html');
    var FaixaVoucherModel = require('models/FaixaVoucherModel');
    var FaixaVoucherCollection = require('collections/FaixaVoucherCollection');
    var ModalMotorista = require('views/modalComponents/MotoristaModal');
    // End of "Import´s" definition
    // #####################################################################################################
    // ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
    // BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
    // #####################################################################################################
    var FormMotoristaFaixaVouchers = Marionette.LayoutView.extend({
        template: _.template(TemplateFormFaixaVouchers),
        regions: {
            modalMotoristaRegion: '#motoristaModal',
        },
        events: {
            'click 	.save': 'save',
            'click 	.go-back-link': 'goBack',
            'click 	.saveAndContinue': 'saveAndContinue',
            'click #searchMotoristaModal': 'showModalMotorista',
        },
        ui: {
            inputMotoristaNome: '#inputMotoristaNome',
            inputMotoristaViatura: '#inputMotoristaViatura',
            inputMotoristaEmail: '#inputMotoristaEmail',
            inputId: '#inputId',
            inputSerie: '#inputSerie',
            inputNumeroInicial: '#inputNumeroInicial',
            inputNumeroFinal: '#inputNumeroFinal',
            inputInativo: '#inputInativo',
            // inputEmpresaId : '#inputEmpresaId',
            // inputEmpresaNome : '#inputEmpresaNome',
            form: '#formFaixaVoucher',
            newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
        },
        initialize: function(options) {
            var that = this;
            this.motorista = options.motorista || new MotoristaModel(this.model.get('motorista'));
            var that = this;
            this.on('show', function() {
                this.ui.inputNumeroInicial.mask('#########');
                this.ui.inputNumeroFinal.mask('#########');
                this.ui.form.validationEngine('attach', {
                    promptPosition: "topLeft",
                    isOverflown: false,
                    validationEventTrigger: "change"
                });
                this.carregaMotorista(this.motorista);
                this.checkButtonAuthority();
            });
        },
        carregaMotorista: function(empresa) {
            this.ui.inputMotoristaNome.val(empresa.get('nome'));
            this.ui.inputMotoristaViatura.val(empresa.get('viatura'));
            this.ui.inputMotoristaEmail.val(empresa.get('email'));
        },
        faixaVoucherValido: function() {
            if (parseInt(this.ui.inputNumeroInicial.val()) > parseInt(this.ui.inputNumeroFinal.val())) return false;
            return true;
        },
        saveAndContinue: function() {
            this.save(true)
        },
        save: function(continua) {
            var that = this;
            var faixaVoucher = that.getModel();
            if (this.isValid() && this.faixaVoucherValido()) {
                faixaVoucher.save({}, {
                    success: function(_model, _resp, _options) {
                        util.showSuccessMessage('Faixa voucher salvo com sucesso!');
                        that.clearForm();
                        if (continua != true) {
                            util.goPage('app/motorista/' + that.motorista.get('id') + '/faixaVouchers');
                        }
                    },
                    error: function(_model, _resp, _options) {
                        util.showErrorMessage('Problema ao salvar registro! ' + _resp.responseJSON.legalMessage, _resp.responseJSON.legalMessage);
                    }
                });
            }
            else {
                if (!this.faixaVoucherValido()) util.showErrorMessage('O número incial não pode ser maior que o número final!');
                else util.showMessage('error', 'Verifique campos em destaque!');
            }
        },
        goBack: function() {
            util.goPage('app/motorista/' + this.motorista.get('id') + '/faixaVouchers');
        },
        clearForm: function() {
            util.clear('inputId');
            util.clear('inputMotoristaNome');
            util.clear('inputMotoristaViatura');
            util.clear('inputMotoristaEmail');
        },
        isValid: function() {
            return this.ui.form.validationEngine('validate', {
                promptPosition: "topLeft",
                isOverflown: false,
                validationEventTrigger: "change"
            });
        },
        getModel: function() {
            var that = this;
            var faixaVoucher = that.model;
            faixaVoucher.set({
                id: util.escapeById('inputId') || null,
                serie: util.escapeById('inputSerie'),
                numeroInicial: util.escapeById('inputNumeroInicial', true),
                numeroFinal: util.escapeById('inputNumeroFinal', true),
                inativo: util.escapeById('inputInativo'),
                motorista: that.motorista.toJSON(),
            });
            return faixaVoucher;
        },
        // vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_MOTOQUEIROVOUCHER_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
				if(e.authority == 'ROLE_MOTOQUEIROVOUCHER_EDITAR' || e.authority == 'ROLE_ADMIN') {
					saveButton.show();
				}
			})
		},
    });
    return FormMotoristaFaixaVouchers;
});