/* generated: 16/10/2016 15:27:08 */
define(function(require) {
    // Start "Import´s Definition"
    var _ = require('adapters/underscore-adapter');
    var $ = require('adapters/jquery-adapter');
    var Col = require('adapters/col-adapter');
    var Backbone = require('adapters/backbone-adapter');
    var Marionette = require('marionette');
    var Backgrid = require('adapters/backgrid-adapter');
    var roles = require('adapters/auth-adapter').roles;
    var util = require('utilities/utils');
    var Combobox = require('views/components/Combobox');
    var CustomStringCell = require('views/components/CustomStringCell');
    var Counter = require('views/components/Counter');
    var ActionsCell = require('views/components/ActionsCell');
    var GeneralActionsCell = require('views/components/GeneralActionsCell');
    var CustomNumberCell = require('views/components/CustomNumberCell');
    var FaixaVoucherModel = require('models/FaixaVoucherModel');
    var FaixaVoucherCollection = require('collections/FaixaVoucherCollection');
    var FaixaVoucherPageCollection = require('collections/FaixaVoucherPageCollection');
    var PageFaixaVoucherTemplate = require('text!views/faixaVoucher/tpl/PageFaixaVoucherTemplate.html');
    var ModalEmpresa = require('views/modalComponents/EmpresaModal');
    // End of "Import´s" definition
    var PageFaixaVoucher = Marionette.LayoutView.extend({
        template: _.template(PageFaixaVoucherTemplate),
        regions: {
            gridRegion: '#grid',
            counterRegion: '#counter',
            paginatorRegion: '#paginator',
            modalEmpresaRegion: '#empresaModal',
        },
        events: {
            'click 	#reset': 'resetFaixaVoucher',
            'click 	.novo-faixaVoucher': 'novoFaixaVoucher',
            'click 	.voltar-empresa': 'voltarEmpresa',
            'keypress': 'treatKeypress',
            'click 	.search-button': 'searchFaixaVoucher',
            'click .show-advanced-search-button': 'toggleAdvancedForm',
        },
        ui: {
            inputEmpresaNomeFantasia: '#inputEmpresaNomeFantasia',
            inputEmpresaCnpj: '#inputEmpresaCnpj',
            inputEmpresaContrato: '#inputEmpresaContrato',
            inputEmpresaRazaoSocial: '#inputEmpresaRazaoSocial',
            inputSerie: '#inputSerie',
            inputNumeroInicial: '#inputNumeroInicial',
            inputNumeroFinal: '#inputNumeroFinal',
            inputInativo: '#inputInativo',
            inputEmpresaId: '#inputEmpresaId',
            inputEmpresaNome: '#inputEmpresaNome',
            form: '#formFaixaVoucherFilter',
            advancedSearchForm: '.advanced-search-form',
            newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
        },
        toggleAdvancedForm: function() {
            this.ui.advancedSearchForm.slideToggle("slow");
        },
        treatKeypress: function(e) {
            if (util.enterPressed(e)) {
                e.preventDefault();
                this.searchFaixaVoucher();
            }
        },
        initialize: function(options) {
            var that = this;
            this.empresa = options.empresa || new EmpresaModel(this.model.get('empresa'));
            var that = this;
            this.faixaVouchers = new FaixaVoucherPageCollection();
            this.grid = new Backgrid.Grid({
                className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
                columns: this.getColumns(),
                emptyText: "Sem registros",
                collection: this.faixaVouchers
            });
            this.counter = new Counter({
                collection: this.faixaVouchers,
            });
            this.paginator = new Backgrid.Extension.Paginator({
                columns: this.getColumns(),
                collection: this.faixaVouchers,
                className: ' paging_simple_numbers',
                uiClassName: 'pagination',
            });
            this.modalEmpresa = new ModalEmpresa({
                onSelectModel: function(model) {
                    that.onSelectEmpresa(model);
                },
            });
            this.on('show', function() {
                this.searchFaixaVoucher();
                that.gridRegion.show(that.grid);
                that.counterRegion.show(that.counter);
                that.paginatorRegion.show(that.paginator);
                this.modalEmpresaRegion.show(this.modalEmpresa);
                this.ui.inputNumeroInicial;
                this.ui.inputNumeroFinal;
                this.carregaEmpresa(this.empresa);
                this.checkButtonAuthority();
            });
        },
        carregaEmpresa: function(empresa) {
            this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));
            this.ui.inputEmpresaCnpj.val(empresa.get('cnpj'));
            this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
            this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));
        },
        searchFaixaVoucher: function() {
            var that = this;
            this.faixaVouchers.filterQueryParams = {
                serie: util.escapeById('inputSerie'),
                numeroInicial: util.escapeById('inputNumeroInicial'),
                numeroFinal: util.escapeById('inputNumeroFinal'),
                inativo: util.escapeById('inputInativo'),
                empresa: that.empresa.get('id'),
            }
            this.faixaVouchers.fetch({
                success: function(_coll, _resp, _opt) {
                    console.info('Consulta para o grid faixaVoucher');
                },
                error: function(_coll, _resp, _opt) {
                    console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
                },
                complete: function() {},
            })
        },
        resetFaixaVoucher: function() {
            this.ui.form.get(0).reset();
            this.faixaVouchers.reset();
            util.clear('inputEmpresaId');
        },
        getColumns: function() {
            var that = this;
            var columns = [{
                //				name : "serie",
                //				editable : false,
                //				sortable : true,
                //				label : "Serie",
                //				cell : "string",
                //			}, {
                name: "numeroInicial",
                editable: false,
                sortable: true,
                label: "Número inicial",
                cell: 'string',
            }, {
                name: "numeroFinal",
                editable: false,
                sortable: true,
                label: "Número final",
                cell: 'string',
            }, {
                name: "inativo",
                editable: false,
                sortable: true,
                label: "Status",
                cell: "string",
                formatter: _.extend({}, Backgrid.CellFormatter.prototype, {
                    fromRaw: function(rawValue, model) {
                        return (rawValue ? "Inativo" : "Ativo");
                    }
                })
            }, {
                name: "acoes",
                label: "Ações(Editar, Deletar)",
                sortable: false,
                cell: GeneralActionsCell.extend({
                    buttons: that.getCellButtons(),
                    context: that,
                })
            }];
            return columns;
        },
        getCellButtons: function() {
            var that = this;
            var buttons = this.checkGridButtonAuthority();
            return buttons;
        },
        deleteModel: function(model) {
            var that = this;
            var modelTipo = new FaixaVoucherModel({
                id: model.id,
            });
            util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
                if (yes) {
                    modelTipo.destroy({
                        success: function() {
                            that.faixaVouchers.remove(model);
                            util.showSuccessMessage('Faixa voucher removido com sucesso!');
                        },
                        error: function(_model, _resp) {
                            util.showErrorMessage('Problema ao remover o registro', _resp);
                        }
                    });
                }
            });
        },
        novoFaixaVoucher: function() {
            util.goPage("app/empresa/" + this.empresa.get('id') + "/newFaixaVoucher");
        },
        voltarEmpresa: function() {
            util.goPage("app/empresas");
        },
        editModel: function(model) {
            util.goPage("app/empresa/" + this.empresa.get('id') + "/editFaixaVoucher/" + model.get('id'));
        },
        // vitoriano : chunck : check grid buttons authority
        checkGridButtonAuthority: function() {
            var that = this;
            var buttons = [];
            $.grep(roles, function(e) {
                if (e.authority == 'ROLE_VOUCHER_EDITAR' || e.authority == 'ROLE_ADMIN') {
                    buttons.push({
                        id: 'edita_ficha_button',
                        type: 'primary',
                        icon: 'icon-pencil fa-pencil',
                        hint: 'Editar Faixa voucher',
                        onClick: that.editModel,
                    });
                }
                if (e.authority == 'ROLE_VOUCHER_DELETAR' || e.authority == 'ROLE_ADMIN') {
                    buttons.push({
                        id: 'delete_button',
                        type: 'danger',
                        icon: 'icon-trash fa-trash',
                        hint: 'Remover Faixa voucher',
                        onClick: that.deleteModel,
                    });
                }
            })
            return buttons;
        },
     // vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_VOUCHER_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
    });
    return PageFaixaVoucher;
});