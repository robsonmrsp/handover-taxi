/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectFaixaVoucher = require('views/faixaVoucher/ModalMultiSelectFaixaVoucher');
	var MultiSelectFaixaVoucherTemplate = require('text!views/faixaVoucher/tpl/MultiSelectFaixaVoucherTemplate.html');

	var MultiSelectFaixaVoucher = Marionette.LayoutView.extend({
		template : _.template(MultiSelectFaixaVoucherTemplate),

		regions : {
			modalMultiSelectFaixaVoucherRegion : '#modalMultiSelectFaixaVouchers',
			gridFaixaVouchersModalRegion : '#gridMultiselectFaixaVouchers',
		},

		initialize : function() {
			var that = this;

			this.faixaVouchers = this.collection;

			this.gridFaixaVouchers = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.faixaVouchers,
			});

			this.modalMultiSelectFaixaVoucher = new ModalMultiSelectFaixaVoucher({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectFaixaVoucherRegion.show(that.modalMultiSelectFaixaVoucher);
				that.gridFaixaVouchersModalRegion.show(that.gridFaixaVouchers);
			});
		},
		clear : function(){
			this.modalMultiSelectFaixaVoucher.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "serie",
				editable : false,
				sortable : false,
				label 	 : "Serie",
				cell 	 : "string",
			}, 
			{
				name : "numeroInicial",
				editable : false,
				sortable : false,
				label 	 : "Número inicial",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "numeroFinal",
				editable : false,
				sortable : false,
				label 	 : "Número final",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "inativo",
				editable : false,
				sortable : false,
				label 	 : "Inativo",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectFaixaVoucher
});
