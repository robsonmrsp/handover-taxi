/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var FaixaVoucherPageCollection = require('collections/FaixaVoucherPageCollection');
	var ModalMultiSelectFaixaVoucherTemplate = require('text!views/faixaVoucher/tpl/ModalMultiSelectFaixaVoucherTemplate.html');
	// End of "Import´s" definition

	var ModalFaixaVouchers = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectFaixaVoucherTemplate),

		regions : {
			gridRegion : '#grid-faixaVouchers-modal',
			paginatorRegion : '#paginator-faixaVouchers-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoFaixaVouchers = this.collection;
			
			this.faixaVouchers = new FaixaVoucherPageCollection();
			this.faixaVouchers.on('fetched', this.endFetch, this);
			this.faixaVouchers.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.faixaVouchers,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.faixaVouchers,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.faixaVouchers.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid faixaVoucher');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoFaixaVouchers.add(model)
			else
				this.projetoFaixaVouchers.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.faixaVouchers.each(function(model) {
				if (that.projetoFaixaVouchers.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "serie",
				editable : false,
				sortable : false,
				label 	 : "Serie",
				cell 	 : "string",
			}, 
			{
				name : "numeroInicial",
				editable : false,
				sortable : false,
				label 	 : "Número inicial",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "numeroFinal",
				editable : false,
				sortable : false,
				label 	 : "Número final",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "inativo",
				editable : false,
				sortable : false,
				label 	 : "Inativo",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalFaixaVouchers;
});
