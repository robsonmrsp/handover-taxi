/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var FormTelefoneFavoritoEmpresaTemplate = require('text!views/clienteFone/tpl/FormTelefoneFavoritoEmpresaTemplate.html');
	var TelefoneFavoritoEmpresaModel = require('models/ClienteFoneModel');
	var TelefoneFavoritoEmpresaCollection = require('collections/ClienteFoneCollection');
	var TelefoneFavoritoEmpresaPageCollection = require('collections/ClienteFonePageCollection');

//	var ClienteFoneModel = require('models/ClienteFoneModel');
//	var ClienteFoneCollection = require('collections/ClienteFoneCollection');
//	var ModalCliente = require('views/modalComponents/ClienteModal');
//	
	// End of "Import´s" definition
	var EmpresaModal = require('views/modalComponents/EmpresaModal');

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormTelefoneFavoritoEmpresa = Marionette.LayoutView.extend({
		template : _.template(FormTelefoneFavoritoEmpresaTemplate),

		regions : {
			empresaModalRegion : '#empresaModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click 	.go-back-link' : 'goBack',
			'click #searchEmpresaModal' : 'showEmpresaModal',
			//'change #inputFone' : 'uniqueTelefone',
		},
		
		ui : {
			// empresa
			inputEmpresaId : '#inputEmpresaId',
			inputEmpresaNomeFantasia : '#inputEmpresaNomeFantasia',
			inputEmpresaCnpj : '#inputEmpresaCnpj',
			inputEmpresaContrato : '#inputEmpresaContrato',
			inputEmpresaRazaoSocial : '#inputEmpresaRazaoSocial',
			// telefone favorito
			inputId : '#inputId',
			inputDdd : '#inputDdd',
			inputFone : '#inputFone',
			inputFavorito : '#inputFavorito',
			// so demiurge sabe
			form : '#formClienteFone',
		},

		initialize : function(options) {
			var that = this;
			this.empresa = options.empresa

			this.empresaModal= new EmpresaModal({
				onSelectModel : function(model) {
					that.onSelectEmpresa(model);
				},
			});
			this.on('show', function() {
				this.empresaModalRegion.show(this.empresaModal);		
		
				this.ui.inputFone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			
				this.carregaEmpresa(this.empresa);
			});
		},

		carregaEmpresa : function(empresa) {
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));
			this.ui.inputEmpresaCnpj.val(empresa.get('cnpj'));
			this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
			this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));
		},
		
		saveAndContinue : function() {
			this.save(true)
		},

		goBack : function() {
			util.goPage('app/empresa/' + this.empresa.get('id') + '/telefoneFavoritos'); // vitoriano
		},

		save : function(continua) {
			var that = this;
			var clienteFone = that.getModel();
			console.log(clienteFone);
			if (this.isValid()) {
				clienteFone.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Telefone salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/empresa/' + that.empresa.get('id') + '/telefoneFavoritos');
						}
					},
					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro! ' + _resp.responseJSON.legalMessage, _resp.responseJSON.legalMessage);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		clearForm : function() {
			util.clear('inputId');
			util.clear('inputDdd'); 
			util.clear('inputFone'); 
			util.clear('inputFavorito'); 
			util.clear('inputClienteId');
			util.clear('inputClienteNome');
			util.clear('inputClienteId');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var telefoneFavorito = that.model; 
			telefoneFavorito.set({
				id: util.escapeById('inputId') || null,
		    	fone : util.escapeById('inputFone'), 
				empresa : that.empresa.toJSON(),
			});
			return telefoneFavorito;
		},
		 		
		showModalEmpresa : function() {
			// add more before the modal is open
			this.empresaModal.showPage();
		},

		onSelectCliente : function(empresa) {
			this.empresaModal.hidePage();	
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));		
		},
				
		uniqueTelefone : function() {
			var that = this;
			util.validateUnique({
				element : that.ui.inputFone,
				fieldName : 'fone',
				fieldDisplayName : 'Telefone',
				view : that,
				collection : ClienteFoneCollection,
			})
		},
		
	});

	return FormTelefoneFavoritoEmpresa;
});