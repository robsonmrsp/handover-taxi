/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var ClienteFonePageCollection = require('collections/ClienteFonePageCollection');
	var ModalMultiSelectClienteFoneTemplate = require('text!views/clienteFone/tpl/ModalMultiSelectClienteFoneTemplate.html');
	// End of "Import´s" definition

	var ModalClienteFones = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectClienteFoneTemplate),

		regions : {
			gridRegion : '#grid-clienteFones-modal',
			paginatorRegion : '#paginator-clienteFones-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoClienteFones = this.collection;
			
			this.clienteFones = new ClienteFonePageCollection();
			this.clienteFones.on('fetched', this.endFetch, this);
			this.clienteFones.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.clienteFones,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.clienteFones,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.clienteFones.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid clienteFone');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoClienteFones.add(model)
			else
				this.projetoClienteFones.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.clienteFones.each(function(model) {
				if (that.projetoClienteFones.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "ddd",
				editable : false,
				sortable : false,
				label 	 : "Ddd",
				cell 	 : "string",
			}, 
			{
				name : "fone",
				editable : false,
				sortable : false,
				label 	 : "Fone",
				cell 	 : "string",
			}, 
			{
				name : "favorito",
				editable : false,
				sortable : false,
				label 	 : "Favorito",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalClienteFones;
});
