/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectClienteFone = require('views/clienteFone/ModalMultiSelectClienteFone');
	var MultiSelectClienteFoneTemplate = require('text!views/clienteFone/tpl/MultiSelectClienteFoneTemplate.html');

	var MultiSelectClienteFone = Marionette.LayoutView.extend({
		template : _.template(MultiSelectClienteFoneTemplate),

		regions : {
			modalMultiSelectClienteFoneRegion : '#modalMultiSelectClienteFones',
			gridClienteFonesModalRegion : '#gridMultiselectClienteFones',
		},

		initialize : function() {
			var that = this;

			this.clienteFones = this.collection;

			this.gridClienteFones = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.clienteFones,
			});

			this.modalMultiSelectClienteFone = new ModalMultiSelectClienteFone({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectClienteFoneRegion.show(that.modalMultiSelectClienteFone);
				that.gridClienteFonesModalRegion.show(that.gridClienteFones);
			});
		},
		clear : function(){
			this.modalMultiSelectClienteFone.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "ddd",
				editable : false,
				sortable : false,
				label 	 : "Ddd",
				cell 	 : "string",
			}, 
			{
				name : "fone",
				editable : false,
				sortable : false,
				label 	 : "Fone",
				cell 	 : "string",
			}, 
			{
				name : "favorito",
				editable : false,
				sortable : false,
				label 	 : "Favorito",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectClienteFone
});
