/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');
	var CustomNumberCell = require('views/components/CustomNumberCell');
	var ClienteFoneModel = require('models/ClienteFoneModel');
	var ClienteFoneCollection = require('collections/ClienteFoneCollection');
	var ClienteFonePageCollection = require('collections/ClienteFonePageCollection');
	var PageClienteFoneTemplate = require('text!views/clienteFone/tpl/PageClienteFoneTemplate.html');
	//Filter import
	var ModalCliente = require('views/modalComponents/ClienteModal');
	var PageClienteFone = Marionette.LayoutView.extend({
		template: _.template(PageClienteFoneTemplate),
		regions: {
			gridRegion: '#grid',
			counterRegion: '#counter',
			paginatorRegion: '#paginator',
			modalClienteRegion: '#clienteModal',
		},
		events: {
			'click 	#reset': 'resetClienteFone',
			'click #searchClienteModal': 'showModalCliente',
			'keypress': 'treatKeypress',
			'click 	.novo-telefone': 'novoTelefone',
			'click 	.voltar-cliente': 'voltarCliente',
			'click 	.search-button': 'searchClienteFone',
			'click .show-advanced-search-button': 'toggleAdvancedForm',
		},
		ui: {
			inputClienteNome: '#inputClienteNome',
			inputClienteEmail: '#inputClienteEmail',
			inputClienteTipo: '#inputClienteTipo',
			inputDdd: '#inputDdd',
			inputFone: '#inputFone',
			inputFavorito: '#inputFavorito',
			inputClienteId: '#inputClienteId',
			inputClienteNome: '#inputClienteNome',
			form: '#formClienteFoneFilter',
			advancedSearchForm: '.advanced-search-form',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		toggleAdvancedForm: function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},
		treatKeypress: function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchClienteFone();
			}
		},
		initialize: function(options) {
			var that = this;
			this.cliente = options.cliente;
			this.clienteFones = new ClienteFonePageCollection();
			this.grid = new Backgrid.Grid({
				className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns: this.getColumns(),
				emptyText: "Sem registros",
				collection: this.clienteFones
			});
			this.counter = new Counter({
				collection: this.clienteFones,
			});
			this.paginator = new Backgrid.Extension.Paginator({
				columns: this.getColumns(),
				collection: this.clienteFones,
				className: ' paging_simple_numbers',
				uiClassName: 'pagination',
			});
			this.modalCliente = new ModalCliente({
				onSelectModel: function(model) {
					that.onSelectCliente(model);
				},
			});
			this.on('show', function() {
				this.ui.inputFone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.searchClienteFone();
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.modalClienteRegion.show(this.modalCliente);
				this.carregaCliente(this.cliente);
				this.checkButtonAuthority();
			});
		},
		carregaCliente: function(cliente) {
			this.ui.inputClienteId.val(cliente.get('id'));
			this.ui.inputClienteNome.val(cliente.get('nome'));
			this.ui.inputClienteEmail.val(cliente.get('email'));
			this.ui.inputClienteTipo.val(cliente.get('tipo'));
		},
		searchClienteFone: function() {
			var that = this;
			this.clienteFones.filterQueryParams = {
				fone: util.escapeById('inputFone'),
				favorito: util.escapeById('inputFavorito'),
				// cliente : util.escapeById('inputClienteId'), 
				cliente: that.cliente.get('id'),
			}
			this.clienteFones.fetch({
				success: function(_coll, _resp, _opt) {
					console.info('Consulta para o grid clienteFone');
				},
				error: function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete: function() {},
			})
		},
		resetClienteFone: function() {
			this.ui.form.get(0).reset();
			this.clienteFones.reset();
			util.clear('inputClienteId');
		},
		getColumns: function() {
			var that = this;
			var columns = [{
					name: "fone",
					editable: false,
					sortable: true,
					label: "Fone",
					cell: "string",
				},
				/*{
					name : "favorito",
					editable : false,
					sortable : true,
					label 	 : "Favorito",
					cell 	 : "string",
				},*/
				{
					name: "acoes",
					label: "Ações(Editar, Deletar)",
					sortable: false,
					cell: GeneralActionsCell.extend({
						buttons: that.getCellButtons(),
						context: that,
					})
				}
			];
			return columns;
		},
		getCellButtons: function() {
			var that = this;
			var buttons = this.checkGridButtonAuthority();
			return buttons;
		},
		deleteModel: function(model) {
			var that = this;
			var modelTipo = new ClienteFoneModel({
				id: model.id,
			});
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success: function() {
							that.clienteFones.remove(model);
							util.showSuccessMessage('Telefone removido com sucesso!');
						},
						error: function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro', _resp);
						}
					});
				}
			});
		},
		novoTelefone: function() {
			util.goPage("app/cliente/" + this.cliente.get('id') + "/newClienteFone");
		},
		editModel: function(model) {
			util.goPage("app/cliente/" + this.cliente.get('id') + "/editClienteFone/" + model.get('id'));
		},
		voltarCliente: function() {
			util.goPage("app/clientes");
		},
		showModalCliente: function() {
			this.modalCliente.showPage();
		},
		onSelectCliente: function(cliente) {
			this.modalCliente.hidePage();
			this.ui.inputClienteId.val(cliente.get('id'));
			this.ui.inputClienteNome.val(cliente.get('nome'));
		},
		// vitoriano : chunck : check grid buttons authority
		checkGridButtonAuthority: function() {
			var that = this;
			var buttons = [];
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_TELEFONE_EDITAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'edita_ficha_button',
						type: 'primary',
						icon: 'icon-pencil fa-pencil',
						hint: 'Editar telefone',
						onClick: that.editModel,
					});
				}
				if (e.authority == 'ROLE_TELEFONE_DELETAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'delete_button',
						type: 'danger',
						icon: 'icon-trash fa-trash',
						hint: 'Remover telefone',
						onClick: that.deleteModel,
					});
				}
			})
			return buttons;
		},
		// vitoriano : chunk
		// vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_TELEFONE_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
	});
	return PageClienteFone;
});