/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');
	var CustomNumberCell = require('views/components/CustomNumberCell');
	var TelefoneFavoritoEmpresaModel = require('models/ClienteFoneModel');
	var TelefoneFavoritoEmpresaCollection = require('collections/ClienteFoneCollection');
	var TelefoneFavoritoEmpresaPageCollection = require('collections/ClienteFonePageCollection');
	var PageTelefoneFavoritoEmpresaTemplate = require('text!views/clienteFone/tpl/PageTelefoneFavoritoEmpresaTemplate.html');
	
	//Filter import
	var EmpresaModal = require('views/modalComponents/EmpresaModal');
	
	// End of "Import´s" definition

	var PageTelefoneFavoritoEmpresa = Marionette.LayoutView.extend({
		template : _.template(PageTelefoneFavoritoEmpresaTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			empresaModalRegion : '#empresaModal',
		},
		
		events : {
			'click 	#reset' : 'resetEmpresaFone',			
			'click #searchEmpresaModal' : 'showModalEmpresa',
			'keypress' : 'treatKeypress',
			'click 	.novo-telefone' : 'novoTelefone',
			'click 	.voltar-empresa' : 'voltarEmpresa',

			'click 	.search-button' : 'searchEmpresaFone',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
			// empresa
			inputEmpresaId : '#inputEmpresaId',
			inputEmpresaNomeFantasia : '#inputEmpresaNomeFantasia',
			inputEmpresaCnpj : '#inputEmpresaCnpj',
			inputEmpresaContrato : '#inputEmpresaContrato',
			inputEmpresaRazaoSocial : '#inputEmpresaRazaoSocial',
			// telefone favorito
			inputDdd : '#inputDdd',
			inputFone : '#inputFone',
			inputFavorito : '#inputFavorito',
			// so demiurge sabe
			form : '#formClienteFoneFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchEmpresaFone();
	    	}
		},

		initialize : function(options) {
			var that = this;
			this.empresa = options.empresa;
			
			this.telefoneFavoritosEmpresa = new TelefoneFavoritoEmpresaPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.telefoneFavoritosEmpresa
			});

			this.counter = new Counter({
				collection : this.telefoneFavoritosEmpresa,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.telefoneFavoritosEmpresa,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.empresaModal = new EmpresaModal({
				onSelectModel : function(model) {
					that.onSelectEmpresa(model);
				},
			});
			this.on('show', function() {
				this.ui.inputFone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.searchEmpresaFone();
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.empresaModalRegion.show(this.empresaModal);		
				this.carregaEmpresa(this.empresa);
			});
		},

		carregaEmpresa : function(empresa) {
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNomeFantasia.val(empresa.get('nomeFantasia'));
			this.ui.inputEmpresaCnpj.val(empresa.get('cnpj'));
			this.ui.inputEmpresaContrato.val(empresa.get('contrato'));
			this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));
		},
		
		searchEmpresaFone : function(){
			var that = this;

			this.telefoneFavoritosEmpresa.filterQueryParams = {
	    		fone : util.escapeById('inputFone'),
	    		favorito : util.escapeById('inputFavorito'),
			    empresa : that.empresa.get('id'),
			}
			this.telefoneFavoritosEmpresa.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid telefone favorito de Empresa');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetEmpresaFone : function(){
			this.ui.form.get(0).reset();
			this.telefoneFavoritosEmpresa.reset();
			util.clear('inputEmpresaId');
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "fone",
				editable : false,
				sortable : true,
				label 	 : "Fone",
				cell 	 : "string",
			}, 
			/*{
				name : "favorito",
				editable : false,
				sortable : true,
				label 	 : "Favorito",
				cell 	 : "string",
			},*/ 
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar telefone',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover telefone',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new TelefoneFavoritoEmpresaModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.telefoneFavoritosEmpresa.remove(model);
							util.showSuccessMessage('Telefone removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		novoTelefone : function() {
			util.goPage("app/empresa/" + this.empresa.get('id') + "/newTelefoneFavorito");
		},

		editModel : function(model) {  
			util.goPage("app/empresa/" + this.empresa.get('id') + "/editTelefoneFavorito/" + model.get('id'));
		},

		voltarEmpresa: function() {
			util.goPage("app/empresas");
		},

		showModalEmpresa : function() {
			this.modalCliente.showPage();
		},
			
		onSelectCliente : function(empresa) {
			this.empresaModal.hidePage();	
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNome.val(empresa.get('nome'));		
		},
		

	});

	return PageTelefoneFavoritoEmpresa;
});
