/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var TemplateFormClienteFones = require('text!views/clienteFone/tpl/FormClienteFoneTemplate.html');
	var ClienteFoneModel = require('models/ClienteFoneModel');
	var ClienteFoneCollection = require('collections/ClienteFoneCollection');
	var ModalCliente = require('views/modalComponents/ClienteModal');
	// End of "Import´s" definition
	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################
	var FormClienteFones = Marionette.LayoutView.extend({
		template: _.template(TemplateFormClienteFones),
		regions: {
			modalClienteRegion: '#clienteModal',
		},
		events: {
			'click 	.save': 'save',
			'click 	.saveAndContinue': 'saveAndContinue',
			'click 	.go-back-link': 'goBack',
			'click #searchClienteModal': 'showModalCliente',
			'change #inputFone': 'uniqueTelefone',
		},
		ui: {
			inputClienteId: '#inputClienteId',
			inputClienteNome: '#inputClienteNome',
			inputClienteEmail: '#inputClienteEmail',
			inputClienteTipo: '#inputClienteTipo',
			inputId: '#inputId',
			inputDdd: '#inputDdd',
			inputFone: '#inputFone',
			inputFavorito: '#inputFavorito',
			inputClienteId: '#inputClienteId',
			form: '#formClienteFone',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		initialize: function(options) {
			var that = this;
			this.cliente = options.cliente
			this.modalCliente = new ModalCliente({
				onSelectModel: function(model) {
					that.onSelectCliente(model);
				},
			});
			this.on('show', function() {
				this.modalClienteRegion.show(this.modalCliente);
				this.ui.inputFone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.ui.form.validationEngine('attach', {
					promptPosition: "topLeft",
					isOverflown: false,
					validationEventTrigger: "change"
				});
				this.carregaCliente(this.cliente);
				this.checkButtonAuthority();
			});
		},
		carregaCliente: function(cliente) {
			this.ui.inputClienteNome.val(cliente.get('nome'));
			this.ui.inputClienteEmail.val(cliente.get('email'));
			this.ui.inputClienteTipo.val(cliente.get('tipo'));
		},
		saveAndContinue: function() {
			this.save(true)
		},
		goBack: function() {
			util.goPage('app/cliente/' + this.cliente.get('id') + '/clienteFones');
		},
		save: function(continua) {
			var that = this;
			var clienteFone = that.getModel();
			console.log(clienteFone);
			if (this.isValid()) {
				clienteFone.save({}, {
					success: function(_model, _resp, _options) {
						util.showSuccessMessage('Telefone salvo com sucesso!');
						that.clearForm();
						if (continua != true) {
							util.goPage('app/cliente/' + that.cliente.get('id') + '/clienteFones');
						}
					},
					error: function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro! ' + _resp.responseJSON.legalMessage, _resp.responseJSON.legalMessage);
					}
				});
			}
			else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},
		clearForm: function() {
			util.clear('inputId');
			util.clear('inputDdd');
			util.clear('inputFone');
			util.clear('inputFavorito');
			util.clear('inputClienteId');
			util.clear('inputClienteNome');
			util.clear('inputClienteId');
		},
		isValid: function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition: "topLeft",
				isOverflown: false,
				validationEventTrigger: "change"
			});
		},
		getModel: function() {
			var that = this;
			var clienteFone = that.model;
			clienteFone.set({
				id: util.escapeById('inputId') || null,
				ddd: util.escapeById('inputDdd'),
				fone: util.escapeById('inputFone'),
				favorito: util.escapeById('inputFavorito'),
				cliente: that.cliente.toJSON(),
			});
			return clienteFone;
		},
		showModalCliente: function() {
			// add more before the modal is open
			this.modalCliente.showPage();
		},
		onSelectCliente: function(cliente) {
			this.modalCliente.hidePage();
			this.ui.inputClienteId.val(cliente.get('id'));
			this.ui.inputClienteNome.val(cliente.get('nome'));
		},
		uniqueTelefone: function() {
			var that = this;
			util.validateUnique({
				element: that.ui.inputFone,
				fieldName: 'fone',
				fieldDisplayName: 'Telefone',
				view: that,
				collection: ClienteFoneCollection,
			})
		},
		// vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_TELEFONE_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
				if(e.authority == 'ROLE_TELEFONE_EDITAR' || e.authority == 'ROLE_ADMIN') {
					saveButton.show();
				}
			})
		},
	});
	return FormClienteFones;
});