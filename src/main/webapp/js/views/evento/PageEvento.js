/* generated: 04/11/2016 11:17:03 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var EventoModel = require('models/EventoModel');
	var EventoCollection = require('collections/EventoCollection');
	var EventoPageCollection = require('collections/EventoPageCollection');
	var PageEventoTemplate = require('text!views/evento/tpl/PageEventoTemplate.html');
	
	//Filter import
	var ModalEnderecoCorrida = require('views/modalComponents/EnderecoCorridaModal');
	var ModalMotorista = require('views/modalComponents/MotoristaModal');
	var ModalVisitaTracker = require('views/modalComponents/VisitaTrackerModal');
	var ModalVisitaTracker = require('views/modalComponents/VisitaTrackerModal');
	
	// End of "Import´s" definition

	var PageEvento = Marionette.LayoutView.extend({
		template : _.template(PageEventoTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			modalEnderecoCorridaRegion : '#enderecoCorridaModal',
			modalMotoristaRegion : '#motoristaModal',
			modalVisitaTrackerRegion : '#visitaTrackerModal',
			modalVisitaTrackerRegion : '#visitaTrackerModal',
		},
		
		events : {
			'click 	#reset' : 'resetEvento',			
			'click #searchEnderecoCorridaModal' : 'showModalEnderecoCorrida',
			'click #searchMotoristaModal' : 'showModalMotorista',
			'click #searchVisitaTrackerModal' : 'showModalVisitaTracker',
			'click #searchVisitaTrackerModal' : 'showModalVisitaTracker',
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchEvento',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
		
			inputEnderecoCorridaId : '#inputEnderecoCorridaId',
			inputEnderecoCorridaNome : '#inputEnderecoCorridaNome',
			inputMotoristaId : '#inputMotoristaId',
			inputMotoristaNome : '#inputMotoristaNome',
			inputVisitaTrackerId : '#inputVisitaTrackerId',
			inputVisitaTrackerNome : '#inputVisitaTrackerNome',
			inputVisitaTrackerId : '#inputVisitaTrackerId',
			inputVisitaTrackerNome : '#inputVisitaTrackerNome',
			form : '#formEventoFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchEvento();
	    	}
		},

		initialize : function() {
			var that = this;

			this.eventos = new EventoPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.eventos
			});

			this.counter = new Counter({
				collection : this.eventos,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.eventos,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.eventos.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid evento');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
				}
			});
			this.modalEnderecoCorrida = new ModalEnderecoCorrida({
				onSelectModel : function(model) {
					that.onSelectEnderecoCorrida(model);
				},
			});
			this.modalMotorista = new ModalMotorista({
				onSelectModel : function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.modalVisitaTracker = new ModalVisitaTracker({
				onSelectModel : function(model) {
					that.onSelectVisitaTracker(model);
				},
			});
			this.modalVisitaTracker = new ModalVisitaTracker({
				onSelectModel : function(model) {
					that.onSelectVisitaTracker(model);
				},
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.modalEnderecoCorridaRegion.show(this.modalEnderecoCorrida);		
				this.modalMotoristaRegion.show(this.modalMotorista);		
				this.modalVisitaTrackerRegion.show(this.modalVisitaTracker);		
				this.modalVisitaTrackerRegion.show(this.modalVisitaTracker);		
		
			});
		},
		 
		searchEvento : function(){
			var that = this;

			this.eventos.filterQueryParams = {
			    enderecoCorrida : util.escapeById('inputEnderecoCorridaId'), 
			    motorista : util.escapeById('inputMotoristaId'), 
			    visitaTracker : util.escapeById('inputVisitaTrackerId'), 
			    visitaTracker : util.escapeById('inputVisitaTrackerId'), 
			}
			this.eventos.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid evento');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetEvento : function(){
			this.ui.form.get(0).reset();
			this.eventos.reset();
			util.clear('inputEnderecoCorridaId');
			util.clear('inputMotoristaId');
			util.clear('inputVisitaTrackerId');
			util.clear('inputVisitaTrackerId');
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "enderecoCorrida.nome",
				editable : false,
				sortable : true,  
				label : "Endereco corrida",
				cell : CustomStringCell.extend({
					fieldName : 'enderecoCorrida.nome',
				}),
			},	
			{
				name : "motorista.nome",
				editable : false,
				sortable : true,  
				label : "Motorista",
				cell : CustomStringCell.extend({
					fieldName : 'motorista.nome',
				}),
			},	
			{
				name : "visitaTracker.nome",
				editable : false,
				sortable : true,  
				label : "Visita tracker",
				cell : CustomStringCell.extend({
					fieldName : 'visitaTracker.nome',
				}),
			},	
			{
				name : "visitaTracker.nome",
				editable : false,
				sortable : true,  
				label : "Visita tracker",
				cell : CustomStringCell.extend({
					fieldName : 'visitaTracker.nome',
				}),
			},	
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Evento',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Evento',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new EventoModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.eventos.remove(model);
							util.showSuccessMessage('Evento removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editEvento/" + model.get('id'));
		},

		showModalEnderecoCorrida : function() {
			this.modalEnderecoCorrida.showPage();
		},
			
		onSelectEnderecoCorrida : function(enderecoCorrida) {
			this.modalEnderecoCorrida.hidePage();	
			this.ui.inputEnderecoCorridaId.val(enderecoCorrida.get('id'));
			this.ui.inputEnderecoCorridaNome.val(enderecoCorrida.get('nome'));		
		},
		showModalMotorista : function() {
			this.modalMotorista.showPage();
		},
			
		onSelectMotorista : function(motorista) {
			this.modalMotorista.hidePage();	
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));		
		},
		showModalVisitaTracker : function() {
			this.modalVisitaTracker.showPage();
		},
			
		onSelectVisitaTracker : function(visitaTracker) {
			this.modalVisitaTracker.hidePage();	
			this.ui.inputVisitaTrackerId.val(visitaTracker.get('id'));
			this.ui.inputVisitaTrackerNome.val(visitaTracker.get('nome'));		
		},
		showModalVisitaTracker : function() {
			this.modalVisitaTracker.showPage();
		},
			
		onSelectVisitaTracker : function(visitaTracker) {
			this.modalVisitaTracker.hidePage();	
			this.ui.inputVisitaTrackerId.val(visitaTracker.get('id'));
			this.ui.inputVisitaTrackerNome.val(visitaTracker.get('nome'));		
		},
		

	});

	return PageEvento;
});
