/* generated: 04/11/2016 11:17:03 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var EventoPageCollection = require('collections/EventoPageCollection');
	var ModalMultiSelectEventoTemplate = require('text!views/evento/tpl/ModalMultiSelectEventoTemplate.html');
	// End of "Import´s" definition

	var ModalEventos = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectEventoTemplate),

		regions : {
			gridRegion : '#grid-eventos-modal',
			paginatorRegion : '#paginator-eventos-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoEventos = this.collection;
			
			this.eventos = new EventoPageCollection();
			this.eventos.on('fetched', this.endFetch, this);
			this.eventos.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.eventos,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.eventos,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.eventos.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid evento');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoEventos.add(model)
			else
				this.projetoEventos.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.eventos.each(function(model) {
				if (that.projetoEventos.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descricao",
				cell 	 : "string",
			}, 
			{
				name : "dataInicio",
				editable : false,
				sortable : false,
				label 	 : "Data inicio",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "dataFim",
				editable : false,
				sortable : false,
				label 	 : "Data fim",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "descricaoCurta",
				editable : false,
				sortable : false,
				label 	 : "Descricao curta",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalEventos;
});
