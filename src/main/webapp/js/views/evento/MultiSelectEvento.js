/* generated: 04/11/2016 11:17:03 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectEvento = require('views/evento/ModalMultiSelectEvento');
	var MultiSelectEventoTemplate = require('text!views/evento/tpl/MultiSelectEventoTemplate.html');

	var MultiSelectEvento = Marionette.LayoutView.extend({
		template : _.template(MultiSelectEventoTemplate),

		regions : {
			modalMultiSelectEventoRegion : '#modalMultiSelectEventos',
			gridEventosModalRegion : '#gridMultiselectEventos',
		},

		initialize : function() {
			var that = this;

			this.eventos = this.collection;

			this.gridEventos = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.eventos,
			});

			this.modalMultiSelectEvento = new ModalMultiSelectEvento({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectEventoRegion.show(that.modalMultiSelectEvento);
				that.gridEventosModalRegion.show(that.gridEventos);
			});
		},
		clear : function(){
			this.modalMultiSelectEvento.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descricao",
				cell 	 : "string",
			}, 
			{
				name : "dataInicio",
				editable : false,
				sortable : false,
				label 	 : "Data inicio",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "dataFim",
				editable : false,
				sortable : false,
				label 	 : "Data fim",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "descricaoCurta",
				editable : false,
				sortable : false,
				label 	 : "Descricao curta",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectEvento
});
