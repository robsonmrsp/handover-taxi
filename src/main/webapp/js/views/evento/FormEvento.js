/* generated: 04/11/2016 11:17:03 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormEventos = require('text!views/evento/tpl/FormEventoTemplate.html');
	var EventoModel = require('models/EventoModel');
	var EventoCollection = require('collections/EventoCollection');
	var ModalEnderecoCorrida = require('views/modalComponents/EnderecoCorridaModal');
	var ModalMotorista = require('views/modalComponents/MotoristaModal');
	var ModalVisitaTracker = require('views/modalComponents/VisitaTrackerModal');
	var ModalVisitaTracker = require('views/modalComponents/VisitaTrackerModal');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormEventos = Marionette.LayoutView.extend({
		template : _.template(TemplateFormEventos),

		regions : {
			modalEnderecoCorridaRegion : '#enderecoCorridaModal',
			modalMotoristaRegion : '#motoristaModal',
			modalVisitaTrackerRegion : '#visitaTrackerModal',
			modalVisitaTrackerRegion : '#visitaTrackerModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click #searchEnderecoCorridaModal' : 'showModalEnderecoCorrida',
			'click #searchMotoristaModal' : 'showModalMotorista',
			'click #searchVisitaTrackerModal' : 'showModalVisitaTracker',
			'click #searchVisitaTrackerModal' : 'showModalVisitaTracker',
		},
		
		ui : {
			inputId : '#inputId',
			inputDescricao : '#inputDescricao',
			inputDataInicio : '#inputDataInicio',
			inputDataFim : '#inputDataFim',
			inputDescricaoCurta : '#inputDescricaoCurta',
		
			inputEnderecoCorridaId : '#inputEnderecoCorridaId',
			inputEnderecoCorridaNome : '#inputEnderecoCorridaNome',
			inputMotoristaId : '#inputMotoristaId',
			inputMotoristaNome : '#inputMotoristaNome',
			inputVisitaTrackerId : '#inputVisitaTrackerId',
			inputVisitaTrackerNome : '#inputVisitaTrackerNome',
			inputVisitaTrackerId : '#inputVisitaTrackerId',
			inputVisitaTrackerNome : '#inputVisitaTrackerNome',
			form : '#formEvento',
		},

		initialize : function() {
			var that = this;
			this.modalEnderecoCorrida = new ModalEnderecoCorrida({
				onSelectModel : function(model) {
					that.onSelectEnderecoCorrida(model);
				},
			});
			this.modalMotorista = new ModalMotorista({
				onSelectModel : function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.modalVisitaTracker = new ModalVisitaTracker({
				onSelectModel : function(model) {
					that.onSelectVisitaTracker(model);
				},
			});
			this.modalVisitaTracker = new ModalVisitaTracker({
				onSelectModel : function(model) {
					that.onSelectVisitaTracker(model);
				},
			});
			this.on('show', function() {
				this.modalEnderecoCorridaRegion.show(this.modalEnderecoCorrida);		
				this.modalMotoristaRegion.show(this.modalMotorista);		
				this.modalVisitaTrackerRegion.show(this.modalVisitaTracker);		
				this.modalVisitaTrackerRegion.show(this.modalVisitaTracker);		
		
				this.ui.inputDataInicio.formatNumber(2);
				this.ui.inputDataFim.formatNumber(2);
			
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var evento = that.getModel();

			if (this.isValid()) {
				evento.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Evento salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/eventos');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputDescricao'); 
			util.clear('inputDataInicio'); 
			util.clear('inputDataFim'); 
			util.clear('inputDescricaoCurta'); 
			util.clear('inputEnderecoCorridaId');
			util.clear('inputEnderecoCorridaNome');
			util.clear('inputMotoristaId');
			util.clear('inputMotoristaNome');
			util.clear('inputVisitaTrackerId');
			util.clear('inputVisitaTrackerNome');
			util.clear('inputVisitaTrackerId');
			util.clear('inputVisitaTrackerNome');
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var evento = that.model; 
			evento.set({
				id: util.escapeById('inputId') || null,
		    	descricao : util.escapeById('inputDescricao'), 
		    	dataInicio : util.escapeById('inputDataInicio', true), 
		    	dataFim : util.escapeById('inputDataFim', true), 
		    	descricaoCurta : util.escapeById('inputDescricaoCurta'), 
				enderecoCorrida : that.modalEnderecoCorrida.getJsonValue(),
				motorista : that.modalMotorista.getJsonValue(),
				visitaTracker : that.modalVisitaTracker.getJsonValue(),
				visitaTracker : that.modalVisitaTracker.getJsonValue(),
			});
			return evento;
		},
		 		
		showModalEnderecoCorrida : function() {
			// add more before the modal is open
			this.modalEnderecoCorrida.showPage();
		},
		showModalMotorista : function() {
			// add more before the modal is open
			this.modalMotorista.showPage();
		},
		showModalVisitaTracker : function() {
			// add more before the modal is open
			this.modalVisitaTracker.showPage();
		},
		showModalVisitaTracker : function() {
			// add more before the modal is open
			this.modalVisitaTracker.showPage();
		},

		onSelectEnderecoCorrida : function(enderecoCorrida) {
			this.modalEnderecoCorrida.hidePage();	
			this.ui.inputEnderecoCorridaId.val(enderecoCorrida.get('id'));
			this.ui.inputEnderecoCorridaNome.val(enderecoCorrida.get('nome'));		
		},
		onSelectMotorista : function(motorista) {
			this.modalMotorista.hidePage();	
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));		
		},
		onSelectVisitaTracker : function(visitaTracker) {
			this.modalVisitaTracker.hidePage();	
			this.ui.inputVisitaTrackerId.val(visitaTracker.get('id'));
			this.ui.inputVisitaTrackerNome.val(visitaTracker.get('nome'));		
		},
		onSelectVisitaTracker : function(visitaTracker) {
			this.modalVisitaTracker.hidePage();	
			this.ui.inputVisitaTrackerId.val(visitaTracker.get('id'));
			this.ui.inputVisitaTrackerNome.val(visitaTracker.get('nome'));		
		},
				
		
	});

	return FormEventos;
});