/* generated: 18/10/2016 23:48:44 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var MotoristaPageCollection = require('collections/MotoristaPageCollection');
	var ModalMultiSelectMotoristaTemplate = require('text!views/motorista/tpl/ModalMultiSelectMotoristaTemplate.html');
	// End of "Import´s" definition

	var ModalMotoristas = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectMotoristaTemplate),

		regions : {
			gridRegion : '#grid-motoristas-modal',
			paginatorRegion : '#paginator-motoristas-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoMotoristas = this.collection;
			
			this.motoristas = new MotoristaPageCollection();
			this.motoristas.on('fetched', this.endFetch, this);
			this.motoristas.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.motoristas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.motoristas,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.motoristas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid motorista');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoMotoristas.add(model)
			else
				this.projetoMotoristas.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.motoristas.each(function(model) {
				if (that.projetoMotoristas.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "nome",
				editable : false,
				sortable : false,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "nomeReduzido",
				editable : false,
				sortable : false,
				label 	 : "Nome reduzido",
				cell 	 : "string",
			}, 
			{
				name : "viatura",
				editable : false,
				sortable : false,
				label 	 : "Viatura",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : false,
				label 	 : "E-mail",
				cell 	 : "string",
			}, 
			{
				name : "ddd",
				editable : false,
				sortable : false,
				label 	 : "DDD",
				cell 	 : "string",
			}, 
			{
				name : "fone",
				editable : false,
				sortable : false,
				label 	 : "Fone",
				cell 	 : "string",
			}, 
			{
				name : "statusMotorista",
				editable : false,
				sortable : false,
				label 	 : "Status motorista",
				cell 	 : "string",
			}, 
			{
				name : "ddd2",
				editable : false,
				sortable : false,
				label 	 : "DDD2",
				cell 	 : "string",
			}, 
			{
				name : "fone2",
				editable : false,
				sortable : false,
				label 	 : "Fone2",
				cell 	 : "string",
			}, 
			{
				name : "dddWhatsapp",
				editable : false,
				sortable : false,
				label 	 : "Ddd whatsapp",
				cell 	 : "string",
			}, 
			{
				name : "whatsapp",
				editable : false,
				sortable : false,
				label 	 : "Whatsapp",
				cell 	 : "string",
			}, 
			{
				name : "dataNascimento",
				editable : false,
				sortable : false,
				label 	 : "Data nascimento",
				cell 	 : "string",
			}, 
			{
				name : "rg",
				editable : false,
				sortable : false,
				label 	 : "RG",
				cell 	 : "string",
			}, 
			{
				name : "orgaoExpedidor",
				editable : false,
				sortable : false,
				label 	 : "Órgao expedidor",
				cell 	 : "string",
			}, 
			{
				name : "dataExpedicao",
				editable : false,
				sortable : false,
				label 	 : "Data expedicao",
				cell 	 : "string",
			}, 
			{
				name : "naturalidade",
				editable : false,
				sortable : false,
				label 	 : "Naturalidade",
				cell 	 : "string",
			}, 
			{
				name : "cpf",
				editable : false,
				sortable : false,
				label 	 : "CPF",
				cell 	 : "string",
			}, 
			{
				name : "cnh",
				editable : false,
				sortable : false,
				label 	 : "CNH",
				cell 	 : "string",
			}, 
			{
				name : "emissaoCnh",
				editable : false,
				sortable : false,
				label 	 : "Emissão CNH",
				cell 	 : "string",
			}, 
			{
				name : "categoriaCnh",
				editable : false,
				sortable : false,
				label 	 : "Categoria CNH",
				cell 	 : "string",
			}, 
			{
				name : "validadeCnh",
				editable : false,
				sortable : false,
				label 	 : "Validade CNH",
				cell 	 : "string",
			}, 
			{
				name : "foto1",
				editable : false,
				sortable : false,
				label 	 : "Foto1",
				cell 	 : "string",
			}, 
			{
				name : "foto2",
				editable : false,
				sortable : false,
				label 	 : "Foto2",
				cell 	 : "string",
			}, 
			{
				name : "foto3",
				editable : false,
				sortable : false,
				label 	 : "Foto3",
				cell 	 : "string",
			}, 
			{
				name : "endereco",
				editable : false,
				sortable : false,
				label 	 : "Endereco",
				cell 	 : "string",
			}, 
			{
				name : "numeroEndereco",
				editable : false,
				sortable : false,
				label 	 : "Número endereco",
				cell 	 : "string",
			}, 
			{
				name : "complemento",
				editable : false,
				sortable : false,
				label 	 : "Complemento",
				cell 	 : "string",
			}, 
			{
				name : "uf",
				editable : false,
				sortable : false,
				label 	 : "Uf",
				cell 	 : "string",
			}, 
			{
				name : "cidade",
				editable : false,
				sortable : false,
				label 	 : "Cidade",
				cell 	 : "string",
			}, 
			{
				name : "bairro",
				editable : false,
				sortable : false,
				label 	 : "Bairro",
				cell 	 : "string",
			}, 
			{
				name : "cep",
				editable : false,
				sortable : false,
				label 	 : "Cep",
				cell 	 : "string",
			}, 
			{
				name : "referenciaEndereco",
				editable : false,
				sortable : false,
				label 	 : "Referencia endereco",
				cell 	 : "string",
			}, 
			{
				name : "mae",
				editable : false,
				sortable : false,
				label 	 : "Nome da Mãe",
				cell 	 : "string",
			}, 
			{
				name : "pai",
				editable : false,
				sortable : false,
				label 	 : "Nome do Pai",
				cell 	 : "string",
			}, 
			{
				name : "conjuge",
				editable : false,
				sortable : false,
				label 	 : "Cônjuge",
				cell 	 : "string",
			}, 
			{
				name : "referenciaPessoal1",
				editable : false,
				sortable : false,
				label 	 : "Referencia pessoal1",
				cell 	 : "string",
			}, 
			{
				name : "foneReferencia1",
				editable : false,
				sortable : false,
				label 	 : "Fone referência 1",
				cell 	 : "string",
			}, 
			{
				name : "parentescoReferencia1",
				editable : false,
				sortable : false,
				label 	 : "Parentesco referencia1",
				cell 	 : "string",
			}, 
			{
				name : "referenciaPessoal2",
				editable : false,
				sortable : false,
				label 	 : "Referencia pessoal2",
				cell 	 : "string",
			}, 
			{
				name : "foneReferencia2",
				editable : false,
				sortable : false,
				label 	 : "Fone referencia2",
				cell 	 : "string",
			}, 
			{
				name : "parentescoReferencia2",
				editable : false,
				sortable : false,
				label 	 : "Parentesco referencia2",
				cell 	 : "string",
			}, 
			{
				name : "marcaVeiculo",
				editable : false,
				sortable : false,
				label 	 : "Marca veiculo",
				cell 	 : "string",
			}, 
			{
				name : "modeloVeiculo",
				editable : false,
				sortable : false,
				label 	 : "Modelo veiculo",
				cell 	 : "string",
			}, 
			{
				name : "placaVeiculo",
				editable : false,
				sortable : false,
				label 	 : "Placa veiculo",
				cell 	 : "string",
			}, 
			{
				name : "corVeiculo",
				editable : false,
				sortable : false,
				label 	 : "Cor veiculo",
				cell 	 : "string",
			}, 
			{
				name : "anoVeiculo",
				editable : false,
				sortable : false,
				label 	 : "Ano veiculo",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "renavam",
				editable : false,
				sortable : false,
				label 	 : "Renavam",
				cell 	 : "string",
			}, 
			{
				name : "chassi",
				editable : false,
				sortable : false,
				label 	 : "Chassi",
				cell 	 : "string",
			}, 
			{
				name : "categoria",
				editable : false,
				sortable : false,
				label 	 : "Categoria",
				cell 	 : "string",
			}, 
			{
				name : "imei",
				editable : false,
				sortable : false,
				label 	 : "Imei",
				cell 	 : "string",
			}, 
			{
				name : "cartaoCredito",
				editable : false,
				sortable : false,
				label 	 : "Cartao credito",
				cell 	 : "string",
			}, 
			{
				name : "cartaoDebito",
				editable : false,
				sortable : false,
				label 	 : "Cartao débito",
				cell 	 : "string",
			}, 
			{
				name : "voucher",
				editable : false,
				sortable : false,
				label 	 : "Voucher",
				cell 	 : "string",
			}, 
			{
				name : "eticket",
				editable : false,
				sortable : false,
				label 	 : "Eticket",
				cell 	 : "string",
			}, 
			{
				name : "senha",
				editable : false,
				sortable : false,
				label 	 : "Senha",
				cell 	 : "string",
			}, 
			{
				name : "padronizada",
				editable : false,
				sortable : false,
				label 	 : "Padronizada",
				cell 	 : "string",
			}, 
			{
				name : "bau",
				editable : false,
				sortable : false,
				label 	 : "Bau",
				cell 	 : "string",
			}, 
			{
				name : "motoGrande",
				editable : false,
				sortable : false,
				label 	 : "Moto grande",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalMotoristas;
});
