/* generated: 18/10/2016 23:48:44 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var UploadView = require('views/components/UploadView');
	var SuggestGmapBox = require('views/components/SuggestGmapBox');
	var TemplateFormMotoristas = require('text!views/motorista/tpl/FormMotoristaTemplate.html');
	var MotoristaModel = require('models/MotoristaModel');
	var MotoristaCollection = require('collections/MotoristaCollection');
	// End of "Import´s" definition
	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN
	// BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################
	var FormMotoristas = Marionette.LayoutView.extend({
		template: _.template(TemplateFormMotoristas),
		regions: {
			foto1Region: '.foto1',
			foto2Region: '.foto2',
			foto3Region: '.foto3',
		},
		events: {
			'click 	.save': 'save',
			'click 	.saveAndContinue': 'saveAndContinue',
			'change #inputViatura': 'uniqueViatura',
		},
		ui: {
			inputId: '#inputId',
			inputEnderecoCombo: '.inputEnderecoCombo',
			inputNome: '#inputNome',
			inputNomeReduzido: '#inputNomeReduzido',
			inputViatura: '#inputViatura',
			inputEmail: '#inputEmail',
			inputDdd: '#inputDdd',
			inputFone: '#inputFone',
			inputStatusMotorista: '#inputStatusMotorista',
			inputDdd2: '#inputDdd2',
			inputFone2: '#inputFone2',
			inputDddWhatsapp: '#inputDddWhatsapp',
			inputWhatsapp: '#inputWhatsapp',
			inputDataNascimento: '#inputDataNascimento',
			groupInputDataNascimento: '#groupInputDataNascimento',
			inputRg: '#inputRg',
			inputOrgaoExpedidor: '#inputOrgaoExpedidor',
			inputDataExpedicao: '#inputDataExpedicao',
			groupInputDataExpedicao: '#groupInputDataExpedicao',
			inputNaturalidade: '#inputNaturalidade',
			inputCpf: '#inputCpf',
			inputCnh: '#inputCnh',
			inputEmissaoCnh: '#inputEmissaoCnh',
			groupInputEmissaoCnh: '#groupInputEmissaoCnh',
			inputCategoriaCnh: '#inputCategoriaCnh',
			inputValidadeCnh: '#inputValidadeCnh',
			groupInputValidadeCnh: '#groupInputValidadeCnh',
			inputFoto1: '#inputFoto1',
			groupInputFoto1: '#groupInputFoto1',
			inputFoto2: '#inputFoto2',
			inputFoto3: '#inputFoto3',
			inputEndereco: '#inputEndereco',
			inputNumeroEndereco: '#inputNumeroEndereco',
			inputComplemento: '#inputComplemento',
			inputUf: '#inputUf',
			inputCidade: '#inputCidade',
			inputBairro: '#inputBairro',
			inputCep: '#inputCep',
			inputObservacao: '#inputObservacao',
			inputReferenciaEndereco: '#inputReferenciaEndereco',
			inputMae: '#inputMae',
			inputPai: '#inputPai',
			inputConjuge: '#inputConjuge',
			inputReferenciaPessoal1: '#inputReferenciaPessoal1',
			inputFoneReferencia1: '#inputFoneReferencia1',
			inputParentescoReferencia1: '#inputParentescoReferencia1',
			inputReferenciaPessoal2: '#inputReferenciaPessoal2',
			inputFoneReferencia2: '#inputFoneReferencia2',
			inputParentescoReferencia2: '#inputParentescoReferencia2',
			inputMarcaVeiculo: '#inputMarcaVeiculo',
			inputModeloVeiculo: '#inputModeloVeiculo',
			inputPlacaVeiculo: '#inputPlacaVeiculo',
			inputCorVeiculo: '#inputCorVeiculo',
			inputAnoVeiculo: '#inputAnoVeiculo',
			inputRenavam: '#inputRenavam',
			inputChassi: '#inputChassi',
			inputCategoria: '#inputCategoria',
			inputImei: '#inputImei',
			inputCartaoCredito: '#inputCartaoCredito',
			inputCartaoDebito: '#inputCartaoDebito',
			inputVoucher: '#inputVoucher',
			inputEticket: '#inputEticket',
			inputSenha: '#inputSenha',
			inputPadronizada: '#inputPadronizada',
			inputBau: '#inputBau',
			inputMotoGrande: '#inputMotoGrande',
			form: '#formMotorista',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		initialize: function() {
			var that = this;
			this.uploadViewFoto1 = new UploadView({
				folder: "motoristas",
				bindElement: that.ui.inputFoto1,
				onSuccess: function(resp, options) {
					console.info('Upload da foto1 concluido...[ ' + resp + ' ]')
				},
				onError: function(resp, options) {
					console.error('Problemas ao uppar foto1')
				}
			});
			this.uploadViewFoto2 = new UploadView({
				folder: "motoristas",
				bindElement: that.ui.inputFoto2,
				onSuccess: function(resp, options) {
					console.info('Upload da foto2 concluido...[ ' + resp + ' ]')
				},
				onError: function(resp, options) {
					console.error('Problemas ao uppar foto2')
				}
			});
			this.uploadViewFoto3 = new UploadView({
				folder: "motoristas",
				bindElement: that.ui.inputFoto3,
				onSuccess: function(resp, options) {
					console.info('Upload da foto3 concluido...[ ' + resp + ' ]')
				},
				onError: function(resp, options) {
					console.error('Problemas ao uppar foto3')
				}
			});
			this.on('show', function() {
				this.foto1Region.show(this.uploadViewFoto1);
				this.foto2Region.show(this.uploadViewFoto2);
				this.foto3Region.show(this.uploadViewFoto3);
				this.ui.inputFone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.ui.inputFone2.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.ui.inputWhatsapp.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.ui.inputFoneReferencia1.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.ui.inputFoneReferencia2.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);
				this.ui.inputCep.mask('99.999-999');
				this.enderecoPartida = new SuggestGmapBox({
					el: that.ui.inputEnderecoCombo,
					onSelect: function(endereco) {
						that.jsonValue = endereco;
						that.model.set(endereco);
						that.populaEndereco(endereco);
					},
				});
				this.ui.groupInputDataNascimento.datetimepicker({
					pickTime: false,
					language: 'pt_BR',
				});
				this.ui.inputDataNascimento.datetimepicker({
					pickTime: false,
					language: 'pt_BR',
				});
				this.ui.inputDataNascimento.mask('99/99/9999');
				this.ui.groupInputDataExpedicao.datetimepicker({
					pickTime: false,
					language: 'pt_BR',
				});
				this.ui.inputDataExpedicao.datetimepicker({
					pickTime: false,
					language: 'pt_BR',
				});
				this.ui.inputDataExpedicao.mask('99/99/9999');
				this.ui.inputCpf.mask('999.999.999-99');
				this.ui.groupInputEmissaoCnh.datetimepicker({
					pickTime: false,
					language: 'pt_BR',
				});
				this.ui.inputEmissaoCnh.datetimepicker({
					pickTime: false,
					language: 'pt_BR',
				});
				this.ui.inputEmissaoCnh.mask('99/99/9999');
				this.ui.groupInputValidadeCnh.datetimepicker({
					pickTime: false,
					language: 'pt_BR',
				});
				this.ui.inputValidadeCnh.datetimepicker({
					pickTime: false,
					language: 'pt_BR',
				});
				this.ui.inputValidadeCnh.mask('99/99/9999');
				this.ui.inputValidadeCnh.mask('99/99/9999');
				this.ui.inputAnoVeiculo.mask('9999');
				this.ui.form.validationEngine('attach', {
					promptPosition: "topLeft",
					isOverflown: false,
					validationEventTrigger: "change"
				});
				this.checkButtonAuthority();
			});
		},
		saveAndContinue: function() {
			this.save(true)
		},
		save: function(continua) {
			var that = this;
			var motorista = that.getModel();
			if (this.isValid()) {
				motorista.save({}, {
					success: function(_model, _resp, _options) {
						util.showSuccessMessage('Motoqueiro salvo com sucesso!');
						that.clearForm();
						if (continua != true) {
							util.goPage('app/motoristas');
						}
					},
					error: function(_model, _resp, _options) {
						if(_resp.status == '403'){
							util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
						} else {
							util.showErrorMessage('Problema ao salvar registro! ' + _resp.responseJSON.legalMessage, _resp.responseJSON.legalMessage);
						}
					}
				});
			}
			else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},
		clearForm: function() {
			util.clear('inputId');
			util.clear('inputNome');
			util.clear('inputNomeReduzido');
			util.clear('inputViatura');
			util.clear('inputEmail');
			util.clear('inputDdd');
			util.clear('inputFone');
			util.clear('inputStatusMotorista');
			util.clear('inputDdd2');
			util.clear('inputFone2');
			util.clear('inputDddWhatsapp');
			util.clear('inputWhatsapp');
			util.clear('inputDataNascimento');
			util.clear('inputRg');
			util.clear('inputOrgaoExpedidor');
			util.clear('inputDataExpedicao');
			util.clear('inputNaturalidade');
			util.clear('inputCpf');
			util.clear('inputCnh');
			util.clear('inputEmissaoCnh');
			util.clear('inputCategoriaCnh');
			util.clear('inputValidadeCnh');
			util.clear('inputFoto1');
			util.clear('inputFoto2');
			util.clear('inputFoto3');
			util.clear('inputEndereco');
			util.clear('inputNumeroEndereco');
			util.clear('inputComplemento');
			util.clear('inputUf');
			util.clear('inputCidade');
			util.clear('inputBairro');
			util.clear('inputCep');
			util.clear('inputReferenciaEndereco');
			util.clear('inputMae');
			util.clear('inputPai');
			util.clear('inputConjuge');
			util.clear('inputReferenciaPessoal1');
			util.clear('inputFoneReferencia1');
			util.clear('inputParentescoReferencia1');
			util.clear('inputReferenciaPessoal2');
			util.clear('inputFoneReferencia2');
			util.clear('inputParentescoReferencia2');
			util.clear('inputMarcaVeiculo');
			util.clear('inputModeloVeiculo');
			util.clear('inputPlacaVeiculo');
			util.clear('inputCorVeiculo');
			util.clear('inputAnoVeiculo');
			util.clear('inputRenavam');
			util.clear('inputChassi');
			util.clear('inputCategoria');
			util.clear('inputImei');
			util.clear('inputCartaoCredito');
			util.clear('inputCartaoDebito');
			util.clear('inputVoucher');
			util.clear('inputEticket');
			util.clear('inputObservacao');
			util.clear('inputSenha');
			util.clear('inputPadronizada');
			util.clear('inputBau');
			util.clear('inputMotoGrande');
		},
		isValid: function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition: "topLeft",
				isOverflown: false,
				validationEventTrigger: "change"
			});
		},
		getModel: function() {
			var that = this;
			var motorista = that.model;
			motorista.set({
				id: util.escapeById('inputId') || null,
				nome: util.escapeById('inputNome'),
				nomeReduzido: util.escapeById('inputNomeReduzido'),
				viatura: util.escapeById('inputViatura'),
				email: util.escapeById('inputEmail'),
				ddd: util.escapeById('inputDdd'),
				fone: util.escapeById('inputFone'),
				statusMotorista: util.escapeById('inputStatusMotorista'),
				ddd2: util.escapeById('inputDdd2'),
				fone2: util.escapeById('inputFone2'),
				dddWhatsapp: util.escapeById('inputDddWhatsapp'),
				whatsapp: util.escapeById('inputWhatsapp'),
				dataNascimento: util.escapeById('inputDataNascimento'),
				observacao: util.escapeById('inputObservacao'),
				rg: util.escapeById('inputRg'),
				orgaoExpedidor: util.escapeById('inputOrgaoExpedidor'),
				dataExpedicao: util.escapeById('inputDataExpedicao'),
				naturalidade: util.escapeById('inputNaturalidade'),
				cpf: util.onlyNumber(util.escapeById('inputCpf')),
				cnh: util.escapeById('inputCnh'),
				emissaoCnh: util.escapeById('inputEmissaoCnh'),
				categoriaCnh: util.escapeById('inputCategoriaCnh'),
				validadeCnh: util.escapeById('inputValidadeCnh'),
				foto1: util.escapeById('inputFoto1'),
				foto2: util.escapeById('inputFoto2'),
				foto3: util.escapeById('inputFoto3'),
				endereco: util.escapeById('inputEndereco'),
				numeroEndereco: util.escapeById('inputNumeroEndereco'),
				complemento: util.escapeById('inputComplemento'),
				uf: util.escapeById('inputUf'),
				cidade: util.escapeById('inputCidade'),
				bairro: util.escapeById('inputBairro'),
				cep: util.onlyNumber(util.escapeById('inputCep')),
				referenciaEndereco: util.escapeById('inputReferenciaEndereco'),
				mae: util.escapeById('inputMae'),
				pai: util.escapeById('inputPai'),
				conjuge: util.escapeById('inputConjuge'),
				referenciaPessoal1: util.escapeById('inputReferenciaPessoal1'),
				foneReferencia1: util.escapeById('inputFoneReferencia1'),
				parentescoReferencia1: util.escapeById('inputParentescoReferencia1'),
				referenciaPessoal2: util.escapeById('inputReferenciaPessoal2'),
				foneReferencia2: util.escapeById('inputFoneReferencia2'),
				parentescoReferencia2: util.escapeById('inputParentescoReferencia2'),
				marcaVeiculo: util.escapeById('inputMarcaVeiculo'),
				modeloVeiculo: util.escapeById('inputModeloVeiculo'),
				placaVeiculo: util.escapeById('inputPlacaVeiculo'),
				corVeiculo: util.escapeById('inputCorVeiculo'),
				anoVeiculo: util.escapeById('inputAnoVeiculo', true),
				renavam: util.escapeById('inputRenavam'),
				chassi: util.escapeById('inputChassi'),
				categoria: util.escapeById('inputCategoria'),
				imei: util.escapeById('inputImei'),
				cartaoCredito: util.escapeById('inputCartaoCredito'),
				cartaoDebito: util.escapeById('inputCartaoDebito'),
				voucher: util.escapeById('inputVoucher'),
				eticket: util.escapeById('inputEticket'),
				senha: util.escapeById('inputSenha'),
				padronizada: util.escapeById('inputPadronizada'),
				bau: util.escapeById('inputBau'),
				motoGrande: util.escapeById('inputMotoGrande'),
			});
			return motorista;
		},
		populaEndereco: function(endereco) {
			this.ui.inputEndereco.val(endereco.logradouro);
			this.ui.inputNumeroEndereco.val(endereco.numero);
			this.ui.inputCep.val(endereco.cep);
			this.ui.inputBairro.val(endereco.bairro);
			this.ui.inputCidade.val(endereco.cidade);
			this.ui.inputUf.val(endereco.uf);
		},
		uniqueViatura: function() {
			var that = this;
			util.validateUnique({
				element: that.ui.inputViatura,
				fieldName: 'viatura',
				fieldDisplayName: 'Viatura',
				view: that,
				collection: MotoristaCollection,
			})
		},
		// vitoriano : chunk
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_MOTORISTA_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
				if(e.authority == 'ROLE_MOTORISTA_EDITAR' || e.authority == 'ROLE_ADMIN') {
					saveButton.show();
				}
			})
		},
		
	});
	return FormMotoristas;
});