/* generated: 16/10/2016 15:27:08 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var MotoristaBloqueadoModel = require('models/MotoristaBloqueadoModel');
	var MotoristaBloqueadoCollection = require('collections/MotoristaBloqueadoCollection');
	var MotoristaBloqueadoPageCollection = require('collections/MotoristaBloqueadoPageCollection');
	var PageMotoristaBloqueadoTemplate = require('text!views/motorista/tpl/PageMotoristaBloqueiosTemplate.html');	
	
	//Filter import
	var ModalCliente = require('views/modalComponents/ClienteModal');
	var ModalMotorista = require('views/modalComponents/MotoristaModal');
	
	// End of "Import´s" definition

	var PageMotoristaBloqueado = Marionette.LayoutView.extend({
		template : _.template(PageMotoristaBloqueadoTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			modalClienteRegion : '#clienteModal',
			modalMotoristaRegion : '#motoristaModal',
		},
		
		events : {
			'click 	#reset' : 'resetMotoristaBloqueado',			
			'click #searchClienteModal' : 'showModalCliente',
			'click #searchMotoristaModal' : 'showModalMotorista',
			'keypress' : 'treatKeypress',
			'click 	.novo-bloqueio' : 'novoBloqueio',
			'click 	.voltar-motorista' : 'voltarMotorista',
			
			'click 	.search-button' : 'searchMotoristaBloqueado',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
			inputMotoristaNome : '#inputMotoristaNome',
			inputMotoristaViatura : '#inputMotoristaViatura',
			inputMotoristaEmail : '#inputMotoristaEmail',

			inputDataBloqueio : '#inputDataBloqueio',
			groupInputDataBloqueio : '#groupInputDataBloqueio',
			inputUsuarioBloqueio : '#inputUsuarioBloqueio',
		
			inputClienteId : '#inputClienteId',
			inputClienteNome : '#inputClienteNome',
			inputMotoristaId : '#inputMotoristaId',
			inputMotoristaNome : '#inputMotoristaNome',
			form : '#formMotoristaBloqueadoFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchMotoristaBloqueado();
	    	}
		},

		initialize : function(options) {
			var that = this;
			this.motorista = options.motorista;
			this.motoristaBloqueados = new MotoristaBloqueadoPageCollection();
			console.log(this.motoristaBloqueados);
			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.motoristaBloqueados
			});

			this.counter = new Counter({
				collection : this.motoristaBloqueados,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.motoristaBloqueados,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

//			this.motoristaBloqueados.getFirstPage({
//				success : function(_col, _resp, _opts) {
//					console.info('Primeira pagina do grid motoristaBloqueado');
//				},
//				error : function(_col, _resp, _opts) {
//					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
//				},
//				data : {
//					motorista : that.motorista.get('id'),
//				}
//			});
			this.modalCliente = new ModalCliente({
				onSelectModel : function(model) {
					that.onSelectCliente(model);
				},
			});
			this.modalMotorista = new ModalMotorista({
				onSelectModel : function(model) {
					that.onSelectMotorista(model);
				},
			});
			this.on('show', function() {
				this.searchMotoristaBloqueado();
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.modalClienteRegion.show(this.modalCliente);		
				this.modalMotoristaRegion.show(this.modalMotorista);		
				this.ui.groupInputDataBloqueio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataBloqueio.datetimepicker({
					pickTime : true,
					language : 'pt_BR',
				});
				this.ui.inputDataBloqueio.mask('99/99/9999 99:99');
				this.ui.inputUsuarioBloqueio.formatNumber(2);
				//this.carregaCliente(this.cliente);
				this.carregaMotorista(this.motorista);
			});
		},

		carregaMotorista : function(motorista) {
			this.ui.inputMotoristaNome.val(motorista.get('nome'));
			this.ui.inputMotoristaViatura.val(motorista.get('viatura'));
			this.ui.inputMotoristaEmail.val(motorista.get('email'));
		},
		 
		searchMotoristaBloqueado : function(){
			var that = this;

			this.motoristaBloqueados.filterQueryParams = {
	    		dataBloqueio : util.escapeById('inputDataBloqueio'),
	    		// usuarioBloqueio : util.escapeById('inputUsuarioBloqueio'),
			    cliente : util.escapeById('inputClienteId'), 
			    motorista : that.motorista.get('id'), 
			}
			this.motoristaBloqueados.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid motoristaBloqueado');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetMotoristaBloqueado : function(){
			this.ui.form.get(0).reset();
			this.motoristaBloqueados.reset();
			//util.clear('inputClienteId');
			util.clear('inputMotoristaId');
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "cliente.nome",
				editable : false,
				sortable : true,  
				label : "Cliente",
				cell : CustomStringCell.extend({
					fieldName : 'cliente.nome',
				}),
			},	
			{
				name : "motorista.nome",
				editable : false,
				sortable : true,  
				label : "Motorista",
				cell : CustomStringCell.extend({
					fieldName : 'motorista.nome',
				}),
			},	
			{
				name : "dataBloqueio",
				editable : false,
				sortable : true,
				label 	 : "Data bloqueio",
				cell 	 : "string",
			}];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Motoqueiro bloqueado',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Motoqueiro bloqueado',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new MotoristaBloqueadoModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.motoristaBloqueados.remove(model);
							util.showSuccessMessage('Motoqueiro bloqueado removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

//		novoBloqueio : function() {
//			util.goPage("app/cliente/" + this.cliente.get('id') + "/newMotoristaBloqueado");
//		},
//
//		editModel : function(model) {
//			util.goPage("app/cliente/" + this.cliente.get('id') + "/editMotoristaBloqueado/" + model.get('id'));
//		},

		voltarMotorista : function() {
			util.goPage("app/motoristas");
		},

		showModalCliente : function() {
			this.modalCliente.showPage();
		},
			
		onSelectCliente : function(cliente) {
			this.modalCliente.hidePage();	
			this.ui.inputClienteId.val(cliente.get('id'));
			this.ui.inputClienteNome.val(cliente.get('nome'));		
		},
		showModalMotorista : function() {
			this.modalMotorista.showPage();
		},
			
		onSelectMotorista : function(motorista) {
			this.modalMotorista.hidePage();	
			this.ui.inputMotoristaId.val(motorista.get('id'));
			this.ui.inputMotoristaNome.val(motorista.get('nome'));		
		},
		

	});

	return PageMotoristaBloqueado;
});
