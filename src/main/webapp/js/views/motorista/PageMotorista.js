/* generated: 18/10/2016 23:48:44 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var roles = require('adapters/auth-adapter').roles;
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');
	var CustomNumberCell = require('views/components/CustomNumberCell');
	var MotoristaModel = require('models/MotoristaModel');
	var MotoristaCollection = require('collections/MotoristaCollection');
	var MotoristaPageCollection = require('collections/MotoristaPageCollection');
	var PageMotoristaTemplate = require('text!views/motorista/tpl/PageMotoristaTemplate.html');
	// End of "Import´s" definition
	var PageMotorista = Marionette.LayoutView.extend({
		template: _.template(PageMotoristaTemplate),
		regions: {
			gridRegion: '#grid',
			counterRegion: '#counter',
			paginatorRegion: '#paginator',
		},
		events: {
			'click 	#reset': 'resetMotorista',
			'keypress': 'treatKeypress',
			'click 	.search-button': 'searchMotorista',
			'click .show-advanced-search-button': 'toggleAdvancedForm',
		},
		ui: {
			inputNome: '#inputNome',
			inputAvancadoNome: 'inputAvancadoNome',
			inputNomeReduzido: '#inputNomeReduzido',
			inputViatura: '#inputViatura',
			inputEmail: '#inputEmail',
			inputDdd: '#inputDdd',
			inputFone: '#inputFone',
			inputStatusMotorista: '#inputStatusMotorista',
			form: '#formMotoristaFilter',
			advancedSearchForm: '.advanced-search-form',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		toggleAdvancedForm: function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},
		treatKeypress: function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchMotorista();
			}
		},
		initialize: function() {
			var that = this;
			this.motoristas = new MotoristaPageCollection();
			this.grid = new Backgrid.Grid({
				className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns: this.getColumns(),
				emptyText: "Sem registros",
				collection: this.motoristas
			});
			this.counter = new Counter({
				collection: this.motoristas,
			});
			this.paginator = new Backgrid.Extension.Paginator({
				columns: this.getColumns(),
				collection: this.motoristas,
				className: ' paging_simple_numbers',
				uiClassName: 'pagination',
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				this.searchMotorista();
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.checkButtonAuthority();
			});
		},
		searchMotorista: function() {
			var that = this;
			if (!util.escapeById('inputNome')) {
				this.motoristas.filterQueryParams = {
					nome: util.escapeById('inputAvancadoNomeMotorista'),
					nomeReduzido: util.escapeById('inputNomeReduzido'),
					viatura: util.escapeById('inputViatura'),
					email: util.escapeById('inputEmail'),
					ddd: util.escapeById('inputDdd'),
					fone: util.escapeById('inputFone'),
					statusMotorista: this.ui.inputStatusMotorista.val(),
				}
			}
			else {
				this.motoristas.filterQueryParams = {
					nome: util.escapeById('inputNome'),
				}
			}
			this.motoristas.fetch({
				success: function(_coll, _resp, _opt) {
					console.info('Consulta para o grid motorista');
				},
				error: function(_coll, _resp, _opt) {
					console.log(_resp);
					if(_resp.status == '403'){
						util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
					}
				},
				complete: function() {},
			})
		},
		resetMotorista: function() {
			this.ui.form.get(0).reset();
			this.motoristas.reset();
		},
		getColumns: function() {
			var that = this;
			var columns = [{
				name: "nome",
				editable: false,
				sortable: true,
				label: "Nome",
				cell: "string",
			}, {
				name: "viatura",
				editable: false,
				sortable: true,
				label: "Viatura",
				cell: "string",
			}, {
				name: "email",
				editable: false,
				sortable: true,
				label: "E-mail",
				cell: "string",
			}, {
				name: "fone",
				editable: false,
				sortable: true,
				label: "Telefone",
				cell: "string",
			}, {
				name: "statusMotorista",
				editable: false,
				sortable: true,
				label: "Status",
				cell: "string",
			}, {
				name: "acoes",
				label: "Ações(Editar, Deletar)",
				sortable: false,
				cell: GeneralActionsCell.extend({
					buttons: that.getCellButtons(),
					context: that,
				})
			}];
			return columns;
		},
		getCellButtons: function() {
			var that = this;
			var buttons = this.checkGridButtonAuthority();
			return buttons;
		},
		// vitoriano :
		verBloqueios: function(model) {
			util.goPage("app/motorista/" + model.get('id') + '/bloqueios');
		},
		//
		verPunicoes: function(model) {
			util.goPage("app/motorista/" + model.get('id') + '/punicaos');
		},
		// vitoriano :
		verVouchers: function(model) {
			util.goPage("app/motorista/" + model.get('id') + '/faixaVouchers');
		},
		//
		deleteModel: function(model) {
			var that = this;
			var modelTipo = new MotoristaModel({
				id: model.id,
			});
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success: function() {
							that.motoristas.remove(model);
							util.showSuccessMessage('Motoqueiro removido com sucesso!');
						},
						error: function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro', _resp);
						}
					});
				}
			});
		},
		editModel: function(model) {
			util.goPage("app/editMotorista/" + model.get('id'));
		},
		// vitoriano : chunck : check grid buttons authority
		checkGridButtonAuthority: function() {
			var that = this;
			var buttons = [];
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_BLOQUEIOS' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'bloqueios_button',
						type: 'primary',
						icon: 'fa-ban',
						hint: 'Bloqueios',
						onClick: that.verBloqueios,
					});
				}
				if (e.authority == 'ROLE_PUNICOES' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'punicao_button',
						type: 'primary',
						icon: 'fa-chain-broken',
						hint: 'Punições',
						onClick: that.verPunicoes,
					});
				}
				if (e.authority == 'ROLE_MOTOQUEIROVOUCHER' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'voucher_button',
						type: 'primary',
						icon: 'fa-ticket',
						hint: 'Vouchers',
						onClick: that.verVouchers,
					});
				}
				if (e.authority == 'ROLE_MOTORISTA_EDITAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'edita_ficha_button',
						type: 'primary',
						icon: 'icon-pencil fa-pencil',
						hint: 'Editar Motoqueiro',
						onClick: that.editModel,
					});
				}
				if (e.authority == 'ROLE_MOTORISTA_DELETAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'delete_button',
						type: 'danger',
						icon: 'icon-trash fa-trash',
						hint: 'Remover Motoqueiro',
						onClick: that.deleteModel,
					});
				}
			})
			return buttons;
		},
		checkButtonAuthority: function(){
			var newButton = this.ui.newButton;
			var saveButton = this.ui.saveButton;
			var saveContinueButton = this.ui.saveContinueButton; 
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_MOTORISTA_CRIAR' || e.authority == 'ROLE_ADMIN') {
					newButton.show();
					saveButton.show();
					saveContinueButton.show();
				}
			})
		},
	});
	return PageMotorista;
});