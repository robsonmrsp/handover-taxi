define(function(require) {
	var Router = require('Router');
	describe("Rotas", function() {

		beforeEach(function() {
			try {
				Backbone.history.stop();
			} catch (e) {
				console.error(e);
			}
		});
		
		afterEach(function() {
			// Reset URL
			var router = new Router();
			router.navigate("");
		});
				it("Rota de \"Areas\"", function() {
			spyOn(Router.prototype, "Areas")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Areas', true);
			expect(Router.prototype.Areas).toHaveBeenCalled();
		});

		it("Rota de \"newarea\"", function() {
			spyOn(Router.prototype, "newarea")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newarea', true);
			expect(Router.prototype.newarea).toHaveBeenCalled();
		});
		
		it("Rota de \"editarea\"", function() {
			spyOn(Router.prototype, "editarea")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editarea/1', true);
			expect(Router.prototype.editarea).toHaveBeenCalled();
		});
		it("Rota de \"Atendentes\"", function() {
			spyOn(Router.prototype, "Atendentes")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Atendentes', true);
			expect(Router.prototype.Atendentes).toHaveBeenCalled();
		});

		it("Rota de \"newatendente\"", function() {
			spyOn(Router.prototype, "newatendente")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newatendente', true);
			expect(Router.prototype.newatendente).toHaveBeenCalled();
		});
		
		it("Rota de \"editatendente\"", function() {
			spyOn(Router.prototype, "editatendente")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editatendente/1', true);
			expect(Router.prototype.editatendente).toHaveBeenCalled();
		});
		it("Rota de \"CentroCustos\"", function() {
			spyOn(Router.prototype, "CentroCustos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/CentroCustos', true);
			expect(Router.prototype.CentroCustos).toHaveBeenCalled();
		});

		it("Rota de \"newcentroCusto\"", function() {
			spyOn(Router.prototype, "newcentroCusto")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newcentroCusto', true);
			expect(Router.prototype.newcentroCusto).toHaveBeenCalled();
		});
		
		it("Rota de \"editcentroCusto\"", function() {
			spyOn(Router.prototype, "editcentroCusto")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editcentroCusto/1', true);
			expect(Router.prototype.editcentroCusto).toHaveBeenCalled();
		});
		it("Rota de \"Clientes\"", function() {
			spyOn(Router.prototype, "Clientes")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Clientes', true);
			expect(Router.prototype.Clientes).toHaveBeenCalled();
		});

		it("Rota de \"newcliente\"", function() {
			spyOn(Router.prototype, "newcliente")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newcliente', true);
			expect(Router.prototype.newcliente).toHaveBeenCalled();
		});
		
		it("Rota de \"editcliente\"", function() {
			spyOn(Router.prototype, "editcliente")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editcliente/1', true);
			expect(Router.prototype.editcliente).toHaveBeenCalled();
		});
		it("Rota de \"ClienteEnderecos\"", function() {
			spyOn(Router.prototype, "ClienteEnderecos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/ClienteEnderecos', true);
			expect(Router.prototype.ClienteEnderecos).toHaveBeenCalled();
		});

		it("Rota de \"newclienteEndereco\"", function() {
			spyOn(Router.prototype, "newclienteEndereco")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newclienteEndereco', true);
			expect(Router.prototype.newclienteEndereco).toHaveBeenCalled();
		});
		
		it("Rota de \"editclienteEndereco\"", function() {
			spyOn(Router.prototype, "editclienteEndereco")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editclienteEndereco/1', true);
			expect(Router.prototype.editclienteEndereco).toHaveBeenCalled();
		});
		it("Rota de \"ClienteEnderecoCoords\"", function() {
			spyOn(Router.prototype, "ClienteEnderecoCoords")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/ClienteEnderecoCoords', true);
			expect(Router.prototype.ClienteEnderecoCoords).toHaveBeenCalled();
		});

		it("Rota de \"newclienteEnderecoCoord\"", function() {
			spyOn(Router.prototype, "newclienteEnderecoCoord")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newclienteEnderecoCoord', true);
			expect(Router.prototype.newclienteEnderecoCoord).toHaveBeenCalled();
		});
		
		it("Rota de \"editclienteEnderecoCoord\"", function() {
			spyOn(Router.prototype, "editclienteEnderecoCoord")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editclienteEnderecoCoord/1', true);
			expect(Router.prototype.editclienteEnderecoCoord).toHaveBeenCalled();
		});
		it("Rota de \"ClienteFones\"", function() {
			spyOn(Router.prototype, "ClienteFones")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/ClienteFones', true);
			expect(Router.prototype.ClienteFones).toHaveBeenCalled();
		});

		it("Rota de \"newclienteFone\"", function() {
			spyOn(Router.prototype, "newclienteFone")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newclienteFone', true);
			expect(Router.prototype.newclienteFone).toHaveBeenCalled();
		});
		
		it("Rota de \"editclienteFone\"", function() {
			spyOn(Router.prototype, "editclienteFone")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editclienteFone/1', true);
			expect(Router.prototype.editclienteFone).toHaveBeenCalled();
		});
		it("Rota de \"Contatos\"", function() {
			spyOn(Router.prototype, "Contatos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Contatos', true);
			expect(Router.prototype.Contatos).toHaveBeenCalled();
		});

		it("Rota de \"newcontato\"", function() {
			spyOn(Router.prototype, "newcontato")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newcontato', true);
			expect(Router.prototype.newcontato).toHaveBeenCalled();
		});
		
		it("Rota de \"editcontato\"", function() {
			spyOn(Router.prototype, "editcontato")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editcontato/1', true);
			expect(Router.prototype.editcontato).toHaveBeenCalled();
		});
		it("Rota de \"Corridas\"", function() {
			spyOn(Router.prototype, "Corridas")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Corridas', true);
			expect(Router.prototype.Corridas).toHaveBeenCalled();
		});

		it("Rota de \"newcorrida\"", function() {
			spyOn(Router.prototype, "newcorrida")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newcorrida', true);
			expect(Router.prototype.newcorrida).toHaveBeenCalled();
		});
		
		it("Rota de \"editcorrida\"", function() {
			spyOn(Router.prototype, "editcorrida")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editcorrida/1', true);
			expect(Router.prototype.editcorrida).toHaveBeenCalled();
		});
		it("Rota de \"CorridaEnderecos\"", function() {
			spyOn(Router.prototype, "CorridaEnderecos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/CorridaEnderecos', true);
			expect(Router.prototype.CorridaEnderecos).toHaveBeenCalled();
		});

		it("Rota de \"newcorridaEndereco\"", function() {
			spyOn(Router.prototype, "newcorridaEndereco")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newcorridaEndereco', true);
			expect(Router.prototype.newcorridaEndereco).toHaveBeenCalled();
		});
		
		it("Rota de \"editcorridaEndereco\"", function() {
			spyOn(Router.prototype, "editcorridaEndereco")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editcorridaEndereco/1', true);
			expect(Router.prototype.editcorridaEndereco).toHaveBeenCalled();
		});
		it("Rota de \"Empresas\"", function() {
			spyOn(Router.prototype, "Empresas")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Empresas', true);
			expect(Router.prototype.Empresas).toHaveBeenCalled();
		});

		it("Rota de \"newempresa\"", function() {
			spyOn(Router.prototype, "newempresa")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newempresa', true);
			expect(Router.prototype.newempresa).toHaveBeenCalled();
		});
		
		it("Rota de \"editempresa\"", function() {
			spyOn(Router.prototype, "editempresa")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editempresa/1', true);
			expect(Router.prototype.editempresa).toHaveBeenCalled();
		});
		it("Rota de \"FaixaVouchers\"", function() {
			spyOn(Router.prototype, "FaixaVouchers")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/FaixaVouchers', true);
			expect(Router.prototype.FaixaVouchers).toHaveBeenCalled();
		});

		it("Rota de \"newfaixaVoucher\"", function() {
			spyOn(Router.prototype, "newfaixaVoucher")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newfaixaVoucher', true);
			expect(Router.prototype.newfaixaVoucher).toHaveBeenCalled();
		});
		
		it("Rota de \"editfaixaVoucher\"", function() {
			spyOn(Router.prototype, "editfaixaVoucher")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editfaixaVoucher/1', true);
			expect(Router.prototype.editfaixaVoucher).toHaveBeenCalled();
		});
		it("Rota de \"HdUsuarios\"", function() {
			spyOn(Router.prototype, "HdUsuarios")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/HdUsuarios', true);
			expect(Router.prototype.HdUsuarios).toHaveBeenCalled();
		});

		it("Rota de \"newhdUsuario\"", function() {
			spyOn(Router.prototype, "newhdUsuario")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newhdUsuario', true);
			expect(Router.prototype.newhdUsuario).toHaveBeenCalled();
		});
		
		it("Rota de \"edithdUsuario\"", function() {
			spyOn(Router.prototype, "edithdUsuario")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/edithdUsuario/1', true);
			expect(Router.prototype.edithdUsuario).toHaveBeenCalled();
		});
		it("Rota de \"Mensagems\"", function() {
			spyOn(Router.prototype, "Mensagems")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Mensagems', true);
			expect(Router.prototype.Mensagems).toHaveBeenCalled();
		});

		it("Rota de \"newmensagem\"", function() {
			spyOn(Router.prototype, "newmensagem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newmensagem', true);
			expect(Router.prototype.newmensagem).toHaveBeenCalled();
		});
		
		it("Rota de \"editmensagem\"", function() {
			spyOn(Router.prototype, "editmensagem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editmensagem/1', true);
			expect(Router.prototype.editmensagem).toHaveBeenCalled();
		});
		it("Rota de \"MensagemMotoristas\"", function() {
			spyOn(Router.prototype, "MensagemMotoristas")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/MensagemMotoristas', true);
			expect(Router.prototype.MensagemMotoristas).toHaveBeenCalled();
		});

		it("Rota de \"newmensagemMotorista\"", function() {
			spyOn(Router.prototype, "newmensagemMotorista")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newmensagemMotorista', true);
			expect(Router.prototype.newmensagemMotorista).toHaveBeenCalled();
		});
		
		it("Rota de \"editmensagemMotorista\"", function() {
			spyOn(Router.prototype, "editmensagemMotorista")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editmensagemMotorista/1', true);
			expect(Router.prototype.editmensagemMotorista).toHaveBeenCalled();
		});
		it("Rota de \"Motoristas\"", function() {
			spyOn(Router.prototype, "Motoristas")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Motoristas', true);
			expect(Router.prototype.Motoristas).toHaveBeenCalled();
		});

		it("Rota de \"newmotorista\"", function() {
			spyOn(Router.prototype, "newmotorista")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newmotorista', true);
			expect(Router.prototype.newmotorista).toHaveBeenCalled();
		});
		
		it("Rota de \"editmotorista\"", function() {
			spyOn(Router.prototype, "editmotorista")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editmotorista/1', true);
			expect(Router.prototype.editmotorista).toHaveBeenCalled();
		});
		it("Rota de \"MotoristaBloqueados\"", function() {
			spyOn(Router.prototype, "MotoristaBloqueados")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/MotoristaBloqueados', true);
			expect(Router.prototype.MotoristaBloqueados).toHaveBeenCalled();
		});

		it("Rota de \"newmotoristaBloqueado\"", function() {
			spyOn(Router.prototype, "newmotoristaBloqueado")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newmotoristaBloqueado', true);
			expect(Router.prototype.newmotoristaBloqueado).toHaveBeenCalled();
		});
		
		it("Rota de \"editmotoristaBloqueado\"", function() {
			spyOn(Router.prototype, "editmotoristaBloqueado")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editmotoristaBloqueado/1', true);
			expect(Router.prototype.editmotoristaBloqueado).toHaveBeenCalled();
		});
		it("Rota de \"MotoristaHasPlantaos\"", function() {
			spyOn(Router.prototype, "MotoristaHasPlantaos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/MotoristaHasPlantaos', true);
			expect(Router.prototype.MotoristaHasPlantaos).toHaveBeenCalled();
		});

		it("Rota de \"newmotoristaHasPlantao\"", function() {
			spyOn(Router.prototype, "newmotoristaHasPlantao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newmotoristaHasPlantao', true);
			expect(Router.prototype.newmotoristaHasPlantao).toHaveBeenCalled();
		});
		
		it("Rota de \"editmotoristaHasPlantao\"", function() {
			spyOn(Router.prototype, "editmotoristaHasPlantao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editmotoristaHasPlantao/1', true);
			expect(Router.prototype.editmotoristaHasPlantao).toHaveBeenCalled();
		});
		it("Rota de \"Plantaos\"", function() {
			spyOn(Router.prototype, "Plantaos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Plantaos', true);
			expect(Router.prototype.Plantaos).toHaveBeenCalled();
		});

		it("Rota de \"newplantao\"", function() {
			spyOn(Router.prototype, "newplantao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newplantao', true);
			expect(Router.prototype.newplantao).toHaveBeenCalled();
		});
		
		it("Rota de \"editplantao\"", function() {
			spyOn(Router.prototype, "editplantao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editplantao/1', true);
			expect(Router.prototype.editplantao).toHaveBeenCalled();
		});
		it("Rota de \"PontosAreas\"", function() {
			spyOn(Router.prototype, "PontosAreas")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/PontosAreas', true);
			expect(Router.prototype.PontosAreas).toHaveBeenCalled();
		});

		it("Rota de \"newpontosArea\"", function() {
			spyOn(Router.prototype, "newpontosArea")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newpontosArea', true);
			expect(Router.prototype.newpontosArea).toHaveBeenCalled();
		});
		
		it("Rota de \"editpontosArea\"", function() {
			spyOn(Router.prototype, "editpontosArea")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editpontosArea/1', true);
			expect(Router.prototype.editpontosArea).toHaveBeenCalled();
		});
		it("Rota de \"Punicaos\"", function() {
			spyOn(Router.prototype, "Punicaos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Punicaos', true);
			expect(Router.prototype.Punicaos).toHaveBeenCalled();
		});

		it("Rota de \"newpunicao\"", function() {
			spyOn(Router.prototype, "newpunicao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newpunicao', true);
			expect(Router.prototype.newpunicao).toHaveBeenCalled();
		});
		
		it("Rota de \"editpunicao\"", function() {
			spyOn(Router.prototype, "editpunicao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editpunicao/1', true);
			expect(Router.prototype.editpunicao).toHaveBeenCalled();
		});
		it("Rota de \"Socios\"", function() {
			spyOn(Router.prototype, "Socios")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Socios', true);
			expect(Router.prototype.Socios).toHaveBeenCalled();
		});

		it("Rota de \"newsocio\"", function() {
			spyOn(Router.prototype, "newsocio")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newsocio', true);
			expect(Router.prototype.newsocio).toHaveBeenCalled();
		});
		
		it("Rota de \"editsocio\"", function() {
			spyOn(Router.prototype, "editsocio")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editsocio/1', true);
			expect(Router.prototype.editsocio).toHaveBeenCalled();
		});
		it("Rota de \"Tarifas\"", function() {
			spyOn(Router.prototype, "Tarifas")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Tarifas', true);
			expect(Router.prototype.Tarifas).toHaveBeenCalled();
		});

		it("Rota de \"newtarifa\"", function() {
			spyOn(Router.prototype, "newtarifa")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newtarifa', true);
			expect(Router.prototype.newtarifa).toHaveBeenCalled();
		});
		
		it("Rota de \"edittarifa\"", function() {
			spyOn(Router.prototype, "edittarifa")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/edittarifa/1', true);
			expect(Router.prototype.edittarifa).toHaveBeenCalled();
		});
		it("Rota de \"TarifaAdicionals\"", function() {
			spyOn(Router.prototype, "TarifaAdicionals")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/TarifaAdicionals', true);
			expect(Router.prototype.TarifaAdicionals).toHaveBeenCalled();
		});

		it("Rota de \"newtarifaAdicional\"", function() {
			spyOn(Router.prototype, "newtarifaAdicional")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newtarifaAdicional', true);
			expect(Router.prototype.newtarifaAdicional).toHaveBeenCalled();
		});
		
		it("Rota de \"edittarifaAdicional\"", function() {
			spyOn(Router.prototype, "edittarifaAdicional")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/edittarifaAdicional/1', true);
			expect(Router.prototype.edittarifaAdicional).toHaveBeenCalled();
		});
		it("Rota de \"Bairros\"", function() {
			spyOn(Router.prototype, "Bairros")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Bairros', true);
			expect(Router.prototype.Bairros).toHaveBeenCalled();
		});

		it("Rota de \"newbairro\"", function() {
			spyOn(Router.prototype, "newbairro")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newbairro', true);
			expect(Router.prototype.newbairro).toHaveBeenCalled();
		});
		
		it("Rota de \"editbairro\"", function() {
			spyOn(Router.prototype, "editbairro")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editbairro/1', true);
			expect(Router.prototype.editbairro).toHaveBeenCalled();
		});
		it("Rota de \"Ceps\"", function() {
			spyOn(Router.prototype, "Ceps")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Ceps', true);
			expect(Router.prototype.Ceps).toHaveBeenCalled();
		});

		it("Rota de \"newcep\"", function() {
			spyOn(Router.prototype, "newcep")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newcep', true);
			expect(Router.prototype.newcep).toHaveBeenCalled();
		});
		
		it("Rota de \"editcep\"", function() {
			spyOn(Router.prototype, "editcep")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editcep/1', true);
			expect(Router.prototype.editcep).toHaveBeenCalled();
		});
		it("Rota de \"Cidades\"", function() {
			spyOn(Router.prototype, "Cidades")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Cidades', true);
			expect(Router.prototype.Cidades).toHaveBeenCalled();
		});

		it("Rota de \"newcidade\"", function() {
			spyOn(Router.prototype, "newcidade")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newcidade', true);
			expect(Router.prototype.newcidade).toHaveBeenCalled();
		});
		
		it("Rota de \"editcidade\"", function() {
			spyOn(Router.prototype, "editcidade")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editcidade/1', true);
			expect(Router.prototype.editcidade).toHaveBeenCalled();
		});
		it("Rota de \"Estados\"", function() {
			spyOn(Router.prototype, "Estados")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Estados', true);
			expect(Router.prototype.Estados).toHaveBeenCalled();
		});

		it("Rota de \"newestado\"", function() {
			spyOn(Router.prototype, "newestado")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newestado', true);
			expect(Router.prototype.newestado).toHaveBeenCalled();
		});
		
		it("Rota de \"editestado\"", function() {
			spyOn(Router.prototype, "editestado")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editestado/1', true);
			expect(Router.prototype.editestado).toHaveBeenCalled();
		});
		it("Rota de \"Paiss\"", function() {
			spyOn(Router.prototype, "Paiss")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Paiss', true);
			expect(Router.prototype.Paiss).toHaveBeenCalled();
		});

		it("Rota de \"newpais\"", function() {
			spyOn(Router.prototype, "newpais")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newpais', true);
			expect(Router.prototype.newpais).toHaveBeenCalled();
		});
		
		it("Rota de \"editpais\"", function() {
			spyOn(Router.prototype, "editpais")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editpais/1', true);
			expect(Router.prototype.editpais).toHaveBeenCalled();
		});
		it("Rota de \"Items\"", function() {
			spyOn(Router.prototype, "Items")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Items', true);
			expect(Router.prototype.Items).toHaveBeenCalled();
		});

		it("Rota de \"newitem\"", function() {
			spyOn(Router.prototype, "newitem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newitem', true);
			expect(Router.prototype.newitem).toHaveBeenCalled();
		});
		
		it("Rota de \"edititem\"", function() {
			spyOn(Router.prototype, "edititem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/edititem/1', true);
			expect(Router.prototype.edititem).toHaveBeenCalled();
		});
		it("Rota de \"ItemTypes\"", function() {
			spyOn(Router.prototype, "ItemTypes")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/ItemTypes', true);
			expect(Router.prototype.ItemTypes).toHaveBeenCalled();
		});

		it("Rota de \"newitemType\"", function() {
			spyOn(Router.prototype, "newitemType")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newitemType', true);
			expect(Router.prototype.newitemType).toHaveBeenCalled();
		});
		
		it("Rota de \"edititemType\"", function() {
			spyOn(Router.prototype, "edititemType")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/edititemType/1', true);
			expect(Router.prototype.edititemType).toHaveBeenCalled();
		});
		it("Rota de \"Operations\"", function() {
			spyOn(Router.prototype, "Operations")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Operations', true);
			expect(Router.prototype.Operations).toHaveBeenCalled();
		});

		it("Rota de \"newoperation\"", function() {
			spyOn(Router.prototype, "newoperation")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newoperation', true);
			expect(Router.prototype.newoperation).toHaveBeenCalled();
		});
		
		it("Rota de \"editoperation\"", function() {
			spyOn(Router.prototype, "editoperation")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editoperation/1', true);
			expect(Router.prototype.editoperation).toHaveBeenCalled();
		});
		it("Rota de \"Permissions\"", function() {
			spyOn(Router.prototype, "Permissions")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Permissions', true);
			expect(Router.prototype.Permissions).toHaveBeenCalled();
		});

		it("Rota de \"newpermission\"", function() {
			spyOn(Router.prototype, "newpermission")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newpermission', true);
			expect(Router.prototype.newpermission).toHaveBeenCalled();
		});
		
		it("Rota de \"editpermission\"", function() {
			spyOn(Router.prototype, "editpermission")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editpermission/1', true);
			expect(Router.prototype.editpermission).toHaveBeenCalled();
		});
		it("Rota de \"Roles\"", function() {
			spyOn(Router.prototype, "Roles")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Roles', true);
			expect(Router.prototype.Roles).toHaveBeenCalled();
		});

		it("Rota de \"newrole\"", function() {
			spyOn(Router.prototype, "newrole")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newrole', true);
			expect(Router.prototype.newrole).toHaveBeenCalled();
		});
		
		it("Rota de \"editrole\"", function() {
			spyOn(Router.prototype, "editrole")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editrole/1', true);
			expect(Router.prototype.editrole).toHaveBeenCalled();
		});
		it("Rota de \"Sessions\"", function() {
			spyOn(Router.prototype, "Sessions")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Sessions', true);
			expect(Router.prototype.Sessions).toHaveBeenCalled();
		});

		it("Rota de \"newsession\"", function() {
			spyOn(Router.prototype, "newsession")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newsession', true);
			expect(Router.prototype.newsession).toHaveBeenCalled();
		});
		
		it("Rota de \"editsession\"", function() {
			spyOn(Router.prototype, "editsession")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editsession/1', true);
			expect(Router.prototype.editsession).toHaveBeenCalled();
		});
		it("Rota de \"Users\"", function() {
			spyOn(Router.prototype, "Users")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Users', true);
			expect(Router.prototype.Users).toHaveBeenCalled();
		});

		it("Rota de \"newuser\"", function() {
			spyOn(Router.prototype, "newuser")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newuser', true);
			expect(Router.prototype.newuser).toHaveBeenCalled();
		});
		
		it("Rota de \"edituser\"", function() {
			spyOn(Router.prototype, "edituser")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/edituser/1', true);
			expect(Router.prototype.edituser).toHaveBeenCalled();
		});
	});
})
