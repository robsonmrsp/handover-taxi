define(function(require) {
	var $ = require('adapters/jquery-adapter');
	var _ = require('adapters/underscore-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
		
	var PageArea = require('views/area/PageArea');
	var FormArea = require('views/area/FormArea');
	var AreaModel = require('models/AreaModel');
	
	var PageAtendente = require('views/atendente/PageAtendente');
	var FormAtendente = require('views/atendente/FormAtendente');
	var AtendenteModel = require('models/AtendenteModel');
	
	var PageCentrocusto = require('views/centrocusto/PageCentrocusto');
	var FormCentrocusto = require('views/centrocusto/FormCentrocusto');
	var CentrocustoModel = require('models/CentrocustoModel');
	
	var PageCliente = require('views/cliente/PageCliente');
	var FormCliente = require('views/cliente/FormCliente');
	var ClienteModel = require('models/ClienteModel');
	
	var PageClienteEndereco = require('views/clienteEndereco/PageClienteEndereco');
	var FormClienteEndereco = require('views/clienteEndereco/FormClienteEndereco');
	var ClienteEnderecoModel = require('models/ClienteEnderecoModel');
	
	var PageClienteEnderecoCoord = require('views/clienteEnderecoCoord/PageClienteEnderecoCoord');
	var FormClienteEnderecoCoord = require('views/clienteEnderecoCoord/FormClienteEnderecoCoord');
	var ClienteEnderecoCoordModel = require('models/ClienteEnderecoCoordModel');
	
	var PageClienteFone = require('views/clienteFone/PageClienteFone');
	var FormClienteFone = require('views/clienteFone/FormClienteFone');
	var ClienteFoneModel = require('models/ClienteFoneModel');
	
	var PageContato = require('views/contato/PageContato');
	var FormContato = require('views/contato/FormContato');
	var ContatoModel = require('models/ContatoModel');
	
	var PageCorrida = require('views/corrida/PageCorrida');
	var FormCorrida = require('views/corrida/FormCorrida');
	var CorridaModel = require('models/CorridaModel');
	
	var PageDistanciaMotoCorrida = require('views/distanciaMotoCorrida/PageDistanciaMotoCorrida');
	var FormDistanciaMotoCorrida = require('views/distanciaMotoCorrida/FormDistanciaMotoCorrida');
	var DistanciaMotoCorridaModel = require('models/DistanciaMotoCorridaModel');
	
	var PageEmpresa = require('views/empresa/PageEmpresa');
	var FormEmpresa = require('views/empresa/FormEmpresa');
	var EmpresaModel = require('models/EmpresaModel');
	
	var PageEnderecoCorrida = require('views/enderecoCorrida/PageEnderecoCorrida');
	var FormEnderecoCorrida = require('views/enderecoCorrida/FormEnderecoCorrida');
	var EnderecoCorridaModel = require('models/EnderecoCorridaModel');
	
	var PageEvento = require('views/evento/PageEvento');
	var FormEvento = require('views/evento/FormEvento');
	var EventoModel = require('models/EventoModel');
	
	var PageFaixaVoucher = require('views/faixaVoucher/PageFaixaVoucher');
	var FormFaixaVoucher = require('views/faixaVoucher/FormFaixaVoucher');
	var FaixaVoucherModel = require('models/FaixaVoucherModel');
	
	var PageHdUsuario = require('views/hdUsuario/PageHdUsuario');
	var FormHdUsuario = require('views/hdUsuario/FormHdUsuario');
	var HdUsuarioModel = require('models/HdUsuarioModel');
	
	var PageMensagem = require('views/mensagem/PageMensagem');
	var FormMensagem = require('views/mensagem/FormMensagem');
	var MensagemModel = require('models/MensagemModel');
	
	var PageMensagemMotorista = require('views/mensagemMotorista/PageMensagemMotorista');
	var FormMensagemMotorista = require('views/mensagemMotorista/FormMensagemMotorista');
	var MensagemMotoristaModel = require('models/MensagemMotoristaModel');
	
	var PageMotorista = require('views/motorista/PageMotorista');
	var FormMotorista = require('views/motorista/FormMotorista');
	var MotoristaModel = require('models/MotoristaModel');
	
	var PageMotoristaBloqueado = require('views/motoristaBloqueado/PageMotoristaBloqueado');
	var FormMotoristaBloqueado = require('views/motoristaBloqueado/FormMotoristaBloqueado');
	var MotoristaBloqueadoModel = require('models/MotoristaBloqueadoModel');
	
	var PageMotoristaHasPlantao = require('views/motoristaHasPlantao/PageMotoristaHasPlantao');
	var FormMotoristaHasPlantao = require('views/motoristaHasPlantao/FormMotoristaHasPlantao');
	var MotoristaHasPlantaoModel = require('models/MotoristaHasPlantaoModel');
	
	var PagePlantao = require('views/plantao/PagePlantao');
	var FormPlantao = require('views/plantao/FormPlantao');
	var PlantaoModel = require('models/PlantaoModel');
	
	var PagePontoArea = require('views/pontoArea/PagePontoArea');
	var FormPontoArea = require('views/pontoArea/FormPontoArea');
	var PontoAreaModel = require('models/PontoAreaModel');
	
	var PagePunicao = require('views/punicao/PagePunicao');
	var FormPunicao = require('views/punicao/FormPunicao');
	var PunicaoModel = require('models/PunicaoModel');
	
	var PageSocio = require('views/socio/PageSocio');
	var FormSocio = require('views/socio/FormSocio');
	var SocioModel = require('models/SocioModel');
	
	var PageStatusTracker = require('views/statusTracker/PageStatusTracker');
	var FormStatusTracker = require('views/statusTracker/FormStatusTracker');
	var StatusTrackerModel = require('models/StatusTrackerModel');
	
	var PageTarifa = require('views/tarifa/PageTarifa');
	var FormTarifa = require('views/tarifa/FormTarifa');
	var TarifaModel = require('models/TarifaModel');
	
	var PageTarifaAdicional = require('views/tarifaAdicional/PageTarifaAdicional');
	var FormTarifaAdicional = require('views/tarifaAdicional/FormTarifaAdicional');
	var TarifaAdicionalModel = require('models/TarifaAdicionalModel');
	
	var PageVisitaTracker = require('views/visitaTracker/PageVisitaTracker');
	var FormVisitaTracker = require('views/visitaTracker/FormVisitaTracker');
	var VisitaTrackerModel = require('models/VisitaTrackerModel');
	
	var PageBairro = require('views/bairro/PageBairro');
	var FormBairro = require('views/bairro/FormBairro');
	var BairroModel = require('models/BairroModel');
	
	var PageCep = require('views/cep/PageCep');
	var FormCep = require('views/cep/FormCep');
	var CepModel = require('models/CepModel');
	
	var PageCidade = require('views/cidade/PageCidade');
	var FormCidade = require('views/cidade/FormCidade');
	var CidadeModel = require('models/CidadeModel');
	
	var PageEstado = require('views/estado/PageEstado');
	var FormEstado = require('views/estado/FormEstado');
	var EstadoModel = require('models/EstadoModel');
	
	var PagePais = require('views/pais/PagePais');
	var FormPais = require('views/pais/FormPais');
	var PaisModel = require('models/PaisModel');
	
	var PageItem = require('views/item/PageItem');
	var FormItem = require('views/item/FormItem');
	var ItemModel = require('models/ItemModel');
	
	var PageItemType = require('views/itemType/PageItemType');
	var FormItemType = require('views/itemType/FormItemType');
	var ItemTypeModel = require('models/ItemTypeModel');
	
	var PageOperation = require('views/operation/PageOperation');
	var FormOperation = require('views/operation/FormOperation');
	var OperationModel = require('models/OperationModel');
	
	var PagePermission = require('views/permission/PagePermission');
	var FormPermission = require('views/permission/FormPermission');
	var PermissionModel = require('models/PermissionModel');
	
	var PageRole = require('views/role/PageRole');
	var FormRole = require('views/role/FormRole');
	var RoleModel = require('models/RoleModel');
	
	var PageSession = require('views/session/PageSession');
	var FormSession = require('views/session/FormSession');
	var SessionModel = require('models/SessionModel');
	
	var PageUser = require('views/user/PageUser');
	var FormUser = require('views/user/FormUser');
	var UserModel = require('models/UserModel');
	
	util.NProgress.setBlockerPanel('block_panel');
	
	var CustomRegion = Marionette.Region.extend({
		el : ".main-content",

		attachHtml : function(view) {
			this.$el.hide();
			this.$el.html(view.el);
			//this.$el.slideDown(300);
			//this.$el.show("slide", { direction: "up" }, 300);
			util.scrollTop();
			this.$el.fadeIn(300);
			view.listenTo(view, 'show', function() {
				setTimeout(function() {
					// ver tambem backbone-adapter
					util.NProgress.done(false, true);
					// uma pequena espera para garantir que o componente foi
					// renderizado antes de mandar remove-lo.
				}, 100);
			});
		},
	});

	var AppRouter = Backbone.Router.extend({
		routes : {
			'' : 'index',
			// hashs de Area
			'app/areas' : 'areas',
			'app/newArea' : 'newArea',
			'app/editArea/:id' : 'editArea',
			// hashs de Atendente
			'app/atendentes' : 'atendentes',
			'app/newAtendente' : 'newAtendente',
			'app/editAtendente/:id' : 'editAtendente',
			// hashs de Centrocusto
			'app/centrocustos' : 'centrocustos',
			'app/newCentrocusto' : 'newCentrocusto',
			'app/editCentrocusto/:id' : 'editCentrocusto',
			// hashs de Cliente
			'app/clientes' : 'clientes',
			'app/newCliente' : 'newCliente',
			'app/editCliente/:id' : 'editCliente',
			// hashs de ClienteEndereco
			'app/clienteEnderecos' : 'clienteEnderecos',
			'app/newClienteEndereco' : 'newClienteEndereco',
			'app/editClienteEndereco/:id' : 'editClienteEndereco',
			// hashs de ClienteEnderecoCoord
			'app/clienteEnderecoCoords' : 'clienteEnderecoCoords',
			'app/newClienteEnderecoCoord' : 'newClienteEnderecoCoord',
			'app/editClienteEnderecoCoord/:id' : 'editClienteEnderecoCoord',
			// hashs de ClienteFone
			'app/clienteFones' : 'clienteFones',
			'app/newClienteFone' : 'newClienteFone',
			'app/editClienteFone/:id' : 'editClienteFone',
			// hashs de Contato
			'app/contatos' : 'contatos',
			'app/newContato' : 'newContato',
			'app/editContato/:id' : 'editContato',
			// hashs de Corrida
			'app/corridas' : 'corridas',
			'app/newCorrida' : 'newCorrida',
			'app/editCorrida/:id' : 'editCorrida',
			// hashs de DistanciaMotoCorrida
			'app/distanciaMotoCorridas' : 'distanciaMotoCorridas',
			'app/newDistanciaMotoCorrida' : 'newDistanciaMotoCorrida',
			'app/editDistanciaMotoCorrida/:id' : 'editDistanciaMotoCorrida',
			// hashs de Empresa
			'app/empresas' : 'empresas',
			'app/newEmpresa' : 'newEmpresa',
			'app/editEmpresa/:id' : 'editEmpresa',
			// hashs de EnderecoCorrida
			'app/enderecoCorridas' : 'enderecoCorridas',
			'app/newEnderecoCorrida' : 'newEnderecoCorrida',
			'app/editEnderecoCorrida/:id' : 'editEnderecoCorrida',
			// hashs de Evento
			'app/eventos' : 'eventos',
			'app/newEvento' : 'newEvento',
			'app/editEvento/:id' : 'editEvento',
			// hashs de FaixaVoucher
			'app/faixaVouchers' : 'faixaVouchers',
			'app/newFaixaVoucher' : 'newFaixaVoucher',
			'app/editFaixaVoucher/:id' : 'editFaixaVoucher',
			// hashs de HdUsuario
			'app/hdUsuarios' : 'hdUsuarios',
			'app/newHdUsuario' : 'newHdUsuario',
			'app/editHdUsuario/:id' : 'editHdUsuario',
			// hashs de Mensagem
			'app/mensagems' : 'mensagems',
			'app/newMensagem' : 'newMensagem',
			'app/editMensagem/:id' : 'editMensagem',
			// hashs de MensagemMotorista
			'app/mensagemMotoristas' : 'mensagemMotoristas',
			'app/newMensagemMotorista' : 'newMensagemMotorista',
			'app/editMensagemMotorista/:id' : 'editMensagemMotorista',
			// hashs de Motorista
			'app/motoristas' : 'motoristas',
			'app/newMotorista' : 'newMotorista',
			'app/editMotorista/:id' : 'editMotorista',
			// hashs de MotoristaBloqueado
			'app/motoristaBloqueados' : 'motoristaBloqueados',
			'app/newMotoristaBloqueado' : 'newMotoristaBloqueado',
			'app/editMotoristaBloqueado/:id' : 'editMotoristaBloqueado',
			// hashs de MotoristaHasPlantao
			'app/motoristaHasPlantaos' : 'motoristaHasPlantaos',
			'app/newMotoristaHasPlantao' : 'newMotoristaHasPlantao',
			'app/editMotoristaHasPlantao/:id' : 'editMotoristaHasPlantao',
			// hashs de Plantao
			'app/plantaos' : 'plantaos',
			'app/newPlantao' : 'newPlantao',
			'app/editPlantao/:id' : 'editPlantao',
			// hashs de PontoArea
			'app/pontoAreas' : 'pontoAreas',
			'app/newPontoArea' : 'newPontoArea',
			'app/editPontoArea/:id' : 'editPontoArea',
			// hashs de Punicao
			'app/punicaos' : 'punicaos',
			'app/newPunicao' : 'newPunicao',
			'app/editPunicao/:id' : 'editPunicao',
			// hashs de Socio
			'app/socios' : 'socios',
			'app/newSocio' : 'newSocio',
			'app/editSocio/:id' : 'editSocio',
			// hashs de StatusTracker
			'app/statusTrackers' : 'statusTrackers',
			'app/newStatusTracker' : 'newStatusTracker',
			'app/editStatusTracker/:id' : 'editStatusTracker',
			// hashs de Tarifa
			'app/tarifas' : 'tarifas',
			'app/newTarifa' : 'newTarifa',
			'app/editTarifa/:id' : 'editTarifa',
			// hashs de TarifaAdicional
			'app/tarifaAdicionals' : 'tarifaAdicionals',
			'app/newTarifaAdicional' : 'newTarifaAdicional',
			'app/editTarifaAdicional/:id' : 'editTarifaAdicional',
			// hashs de VisitaTracker
			'app/visitaTrackers' : 'visitaTrackers',
			'app/newVisitaTracker' : 'newVisitaTracker',
			'app/editVisitaTracker/:id' : 'editVisitaTracker',
			// hashs de Bairro
			'app/bairros' : 'bairros',
			'app/newBairro' : 'newBairro',
			'app/editBairro/:id' : 'editBairro',
			// hashs de Cep
			'app/ceps' : 'ceps',
			'app/newCep' : 'newCep',
			'app/editCep/:id' : 'editCep',
			// hashs de Cidade
			'app/cidades' : 'cidades',
			'app/newCidade' : 'newCidade',
			'app/editCidade/:id' : 'editCidade',
			// hashs de Estado
			'app/estados' : 'estados',
			'app/newEstado' : 'newEstado',
			'app/editEstado/:id' : 'editEstado',
			// hashs de Pais
			'app/paiss' : 'paiss',
			'app/newPais' : 'newPais',
			'app/editPais/:id' : 'editPais',
			// hashs de Item
			'app/items' : 'items',
			'app/newItem' : 'newItem',
			'app/editItem/:id' : 'editItem',
			// hashs de ItemType
			'app/itemTypes' : 'itemTypes',
			'app/newItemType' : 'newItemType',
			'app/editItemType/:id' : 'editItemType',
			// hashs de Operation
			'app/operations' : 'operations',
			'app/newOperation' : 'newOperation',
			'app/editOperation/:id' : 'editOperation',
			// hashs de Permission
			'app/permissions' : 'permissions',
			'app/newPermission' : 'newPermission',
			'app/editPermission/:id' : 'editPermission',
			// hashs de Role
			'app/roles' : 'roles',
			'app/newRole' : 'newRole',
			'app/editRole/:id' : 'editRole',
			// hashs de Session
			'app/sessions' : 'sessions',
			'app/newSession' : 'newSession',
			'app/editSession/:id' : 'editSession',
			// hashs de User
			'app/users' : 'users',
			'app/newUser' : 'newUser',
			'app/editUser/:id' : 'editUser',
		},
		initialize : function() {
			this.App = new Marionette.Application();
			this.App.addRegions({
				mainRegion : CustomRegion
			});
			this.on('route', function(abc) {
				util.NProgress.start(true);
			});
		},

		index : function(path) {
			util.markActiveItem('dashboard');
			setTimeout(function() {
				util.NProgress.done(false, true);
			}, 500);
		},
		
		//configuração das rotas de Area
		areas: function() {
			util.markActiveItem('areas');
			this.pageArea = new PageArea();
			this.App.mainRegion.show(this.pageArea);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Area',
				itemSubFolderName : 'Listagem',
				url : 'app/areas'
			});
		},

		newArea: function() {
			util.markActiveItem('areas');
			var formArea = new FormArea({
				model : new AreaModel(),
			});
			this.App.mainRegion.show(formArea);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Area',
				itemSubFolderName : 'Formulário de cadastro de Area',
				url : 'app/areas'
			});
		},
		
		editArea: function(idArea) {
			var that = this;
			util.markActiveItem('areas');
			var formArea = null;
			if (this.pageArea) {
				formArea = new FormArea({
					model : this.pageArea.areas.get(idArea),
				});
				that.App.mainRegion.show(formArea);
			} else {
				var model = new AreaModel({
					id : idArea,
				})
				model.fetch({
					success : function(model) {
						formArea = new FormArea({
							model : model,
						});
						that.App.mainRegion.show(formArea);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Areas',
					itemSubFolderName : 'Formulário de atualização de Area',
					url : 'app/areas'
				});
			}
		},
		
		//configuração das rotas de Atendente
		atendentes: function() {
			util.markActiveItem('atendentes');
			this.pageAtendente = new PageAtendente();
			this.App.mainRegion.show(this.pageAtendente);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Atendente',
				itemSubFolderName : 'Listagem',
				url : 'app/atendentes'
			});
		},

		newAtendente: function() {
			util.markActiveItem('atendentes');
			var formAtendente = new FormAtendente({
				model : new AtendenteModel(),
			});
			this.App.mainRegion.show(formAtendente);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Atendente',
				itemSubFolderName : 'Formulário de cadastro de Atendente',
				url : 'app/atendentes'
			});
		},
		
		editAtendente: function(idAtendente) {
			var that = this;
			util.markActiveItem('atendentes');
			var formAtendente = null;
			if (this.pageAtendente) {
				formAtendente = new FormAtendente({
					model : this.pageAtendente.atendentes.get(idAtendente),
				});
				that.App.mainRegion.show(formAtendente);
			} else {
				var model = new AtendenteModel({
					id : idAtendente,
				})
				model.fetch({
					success : function(model) {
						formAtendente = new FormAtendente({
							model : model,
						});
						that.App.mainRegion.show(formAtendente);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Atendentes',
					itemSubFolderName : 'Formulário de atualização de Atendente',
					url : 'app/atendentes'
				});
			}
		},
		
		//configuração das rotas de Centrocusto
		centrocustos: function() {
			util.markActiveItem('centrocustos');
			this.pageCentrocusto = new PageCentrocusto();
			this.App.mainRegion.show(this.pageCentrocusto);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Centrocusto',
				itemSubFolderName : 'Listagem',
				url : 'app/centrocustos'
			});
		},

		newCentrocusto: function() {
			util.markActiveItem('centrocustos');
			var formCentrocusto = new FormCentrocusto({
				model : new CentrocustoModel(),
			});
			this.App.mainRegion.show(formCentrocusto);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Centrocusto',
				itemSubFolderName : 'Formulário de cadastro de Centrocusto',
				url : 'app/centrocustos'
			});
		},
		
		editCentrocusto: function(idCentrocusto) {
			var that = this;
			util.markActiveItem('centrocustos');
			var formCentrocusto = null;
			if (this.pageCentrocusto) {
				formCentrocusto = new FormCentrocusto({
					model : this.pageCentrocusto.centrocustos.get(idCentrocusto),
				});
				that.App.mainRegion.show(formCentrocusto);
			} else {
				var model = new CentrocustoModel({
					id : idCentrocusto,
				})
				model.fetch({
					success : function(model) {
						formCentrocusto = new FormCentrocusto({
							model : model,
						});
						that.App.mainRegion.show(formCentrocusto);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Centrocustos',
					itemSubFolderName : 'Formulário de atualização de Centrocusto',
					url : 'app/centrocustos'
				});
			}
		},
		
		//configuração das rotas de Cliente
		clientes: function() {
			util.markActiveItem('clientes');
			this.pageCliente = new PageCliente();
			this.App.mainRegion.show(this.pageCliente);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cliente',
				itemSubFolderName : 'Listagem',
				url : 'app/clientes'
			});
		},

		newCliente: function() {
			util.markActiveItem('clientes');
			var formCliente = new FormCliente({
				model : new ClienteModel(),
			});
			this.App.mainRegion.show(formCliente);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cliente',
				itemSubFolderName : 'Formulário de cadastro de Cliente',
				url : 'app/clientes'
			});
		},
		
		editCliente: function(idCliente) {
			var that = this;
			util.markActiveItem('clientes');
			var formCliente = null;
			if (this.pageCliente) {
				formCliente = new FormCliente({
					model : this.pageCliente.clientes.get(idCliente),
				});
				that.App.mainRegion.show(formCliente);
			} else {
				var model = new ClienteModel({
					id : idCliente,
				})
				model.fetch({
					success : function(model) {
						formCliente = new FormCliente({
							model : model,
						});
						that.App.mainRegion.show(formCliente);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Clientes',
					itemSubFolderName : 'Formulário de atualização de Cliente',
					url : 'app/clientes'
				});
			}
		},
		
		//configuração das rotas de ClienteEndereco
		clienteEnderecos: function() {
			util.markActiveItem('clienteEnderecos');
			this.pageClienteEndereco = new PageClienteEndereco();
			this.App.mainRegion.show(this.pageClienteEndereco);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cliente endereco',
				itemSubFolderName : 'Listagem',
				url : 'app/clienteEnderecos'
			});
		},

		newClienteEndereco: function() {
			util.markActiveItem('clienteEnderecos');
			var formClienteEndereco = new FormClienteEndereco({
				model : new ClienteEnderecoModel(),
			});
			this.App.mainRegion.show(formClienteEndereco);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cliente endereco',
				itemSubFolderName : 'Formulário de cadastro de Cliente endereco',
				url : 'app/clienteEnderecos'
			});
		},
		
		editClienteEndereco: function(idClienteEndereco) {
			var that = this;
			util.markActiveItem('clienteEnderecos');
			var formClienteEndereco = null;
			if (this.pageClienteEndereco) {
				formClienteEndereco = new FormClienteEndereco({
					model : this.pageClienteEndereco.clienteEnderecos.get(idClienteEndereco),
				});
				that.App.mainRegion.show(formClienteEndereco);
			} else {
				var model = new ClienteEnderecoModel({
					id : idClienteEndereco,
				})
				model.fetch({
					success : function(model) {
						formClienteEndereco = new FormClienteEndereco({
							model : model,
						});
						that.App.mainRegion.show(formClienteEndereco);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'ClienteEnderecos',
					itemSubFolderName : 'Formulário de atualização de Cliente endereco',
					url : 'app/clienteEnderecos'
				});
			}
		},
		
		//configuração das rotas de ClienteEnderecoCoord
		clienteEnderecoCoords: function() {
			util.markActiveItem('clienteEnderecoCoords');
			this.pageClienteEnderecoCoord = new PageClienteEnderecoCoord();
			this.App.mainRegion.show(this.pageClienteEnderecoCoord);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cliente endereco coord',
				itemSubFolderName : 'Listagem',
				url : 'app/clienteEnderecoCoords'
			});
		},

		newClienteEnderecoCoord: function() {
			util.markActiveItem('clienteEnderecoCoords');
			var formClienteEnderecoCoord = new FormClienteEnderecoCoord({
				model : new ClienteEnderecoCoordModel(),
			});
			this.App.mainRegion.show(formClienteEnderecoCoord);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cliente endereco coord',
				itemSubFolderName : 'Formulário de cadastro de Cliente endereco coord',
				url : 'app/clienteEnderecoCoords'
			});
		},
		
		editClienteEnderecoCoord: function(idClienteEnderecoCoord) {
			var that = this;
			util.markActiveItem('clienteEnderecoCoords');
			var formClienteEnderecoCoord = null;
			if (this.pageClienteEnderecoCoord) {
				formClienteEnderecoCoord = new FormClienteEnderecoCoord({
					model : this.pageClienteEnderecoCoord.clienteEnderecoCoords.get(idClienteEnderecoCoord),
				});
				that.App.mainRegion.show(formClienteEnderecoCoord);
			} else {
				var model = new ClienteEnderecoCoordModel({
					id : idClienteEnderecoCoord,
				})
				model.fetch({
					success : function(model) {
						formClienteEnderecoCoord = new FormClienteEnderecoCoord({
							model : model,
						});
						that.App.mainRegion.show(formClienteEnderecoCoord);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'ClienteEnderecoCoords',
					itemSubFolderName : 'Formulário de atualização de Cliente endereco coord',
					url : 'app/clienteEnderecoCoords'
				});
			}
		},
		
		//configuração das rotas de ClienteFone
		clienteFones: function() {
			util.markActiveItem('clienteFones');
			this.pageClienteFone = new PageClienteFone();
			this.App.mainRegion.show(this.pageClienteFone);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cliente fone',
				itemSubFolderName : 'Listagem',
				url : 'app/clienteFones'
			});
		},

		newClienteFone: function() {
			util.markActiveItem('clienteFones');
			var formClienteFone = new FormClienteFone({
				model : new ClienteFoneModel(),
			});
			this.App.mainRegion.show(formClienteFone);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cliente fone',
				itemSubFolderName : 'Formulário de cadastro de Cliente fone',
				url : 'app/clienteFones'
			});
		},
		
		editClienteFone: function(idClienteFone) {
			var that = this;
			util.markActiveItem('clienteFones');
			var formClienteFone = null;
			if (this.pageClienteFone) {
				formClienteFone = new FormClienteFone({
					model : this.pageClienteFone.clienteFones.get(idClienteFone),
				});
				that.App.mainRegion.show(formClienteFone);
			} else {
				var model = new ClienteFoneModel({
					id : idClienteFone,
				})
				model.fetch({
					success : function(model) {
						formClienteFone = new FormClienteFone({
							model : model,
						});
						that.App.mainRegion.show(formClienteFone);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'ClienteFones',
					itemSubFolderName : 'Formulário de atualização de Cliente fone',
					url : 'app/clienteFones'
				});
			}
		},
		
		//configuração das rotas de Contato
		contatos: function() {
			util.markActiveItem('contatos');
			this.pageContato = new PageContato();
			this.App.mainRegion.show(this.pageContato);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Contato',
				itemSubFolderName : 'Listagem',
				url : 'app/contatos'
			});
		},

		newContato: function() {
			util.markActiveItem('contatos');
			var formContato = new FormContato({
				model : new ContatoModel(),
			});
			this.App.mainRegion.show(formContato);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Contato',
				itemSubFolderName : 'Formulário de cadastro de Contato',
				url : 'app/contatos'
			});
		},
		
		editContato: function(idContato) {
			var that = this;
			util.markActiveItem('contatos');
			var formContato = null;
			if (this.pageContato) {
				formContato = new FormContato({
					model : this.pageContato.contatos.get(idContato),
				});
				that.App.mainRegion.show(formContato);
			} else {
				var model = new ContatoModel({
					id : idContato,
				})
				model.fetch({
					success : function(model) {
						formContato = new FormContato({
							model : model,
						});
						that.App.mainRegion.show(formContato);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Contatos',
					itemSubFolderName : 'Formulário de atualização de Contato',
					url : 'app/contatos'
				});
			}
		},
		
		//configuração das rotas de Corrida
		corridas: function() {
			util.markActiveItem('corridas');
			this.pageCorrida = new PageCorrida();
			this.App.mainRegion.show(this.pageCorrida);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Corrida',
				itemSubFolderName : 'Listagem',
				url : 'app/corridas'
			});
		},

		newCorrida: function() {
			util.markActiveItem('corridas');
			var formCorrida = new FormCorrida({
				model : new CorridaModel(),
			});
			this.App.mainRegion.show(formCorrida);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Corrida',
				itemSubFolderName : 'Formulário de cadastro de Corrida',
				url : 'app/corridas'
			});
		},
		
		editCorrida: function(idCorrida) {
			var that = this;
			util.markActiveItem('corridas');
			var formCorrida = null;
			if (this.pageCorrida) {
				formCorrida = new FormCorrida({
					model : this.pageCorrida.corridas.get(idCorrida),
				});
				that.App.mainRegion.show(formCorrida);
			} else {
				var model = new CorridaModel({
					id : idCorrida,
				})
				model.fetch({
					success : function(model) {
						formCorrida = new FormCorrida({
							model : model,
						});
						that.App.mainRegion.show(formCorrida);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Corridas',
					itemSubFolderName : 'Formulário de atualização de Corrida',
					url : 'app/corridas'
				});
			}
		},
		
		//configuração das rotas de DistanciaMotoCorrida
		distanciaMotoCorridas: function() {
			util.markActiveItem('distanciaMotoCorridas');
			this.pageDistanciaMotoCorrida = new PageDistanciaMotoCorrida();
			this.App.mainRegion.show(this.pageDistanciaMotoCorrida);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Distancia moto corrida',
				itemSubFolderName : 'Listagem',
				url : 'app/distanciaMotoCorridas'
			});
		},

		newDistanciaMotoCorrida: function() {
			util.markActiveItem('distanciaMotoCorridas');
			var formDistanciaMotoCorrida = new FormDistanciaMotoCorrida({
				model : new DistanciaMotoCorridaModel(),
			});
			this.App.mainRegion.show(formDistanciaMotoCorrida);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Distancia moto corrida',
				itemSubFolderName : 'Formulário de cadastro de Distancia moto corrida',
				url : 'app/distanciaMotoCorridas'
			});
		},
		
		editDistanciaMotoCorrida: function(idDistanciaMotoCorrida) {
			var that = this;
			util.markActiveItem('distanciaMotoCorridas');
			var formDistanciaMotoCorrida = null;
			if (this.pageDistanciaMotoCorrida) {
				formDistanciaMotoCorrida = new FormDistanciaMotoCorrida({
					model : this.pageDistanciaMotoCorrida.distanciaMotoCorridas.get(idDistanciaMotoCorrida),
				});
				that.App.mainRegion.show(formDistanciaMotoCorrida);
			} else {
				var model = new DistanciaMotoCorridaModel({
					id : idDistanciaMotoCorrida,
				})
				model.fetch({
					success : function(model) {
						formDistanciaMotoCorrida = new FormDistanciaMotoCorrida({
							model : model,
						});
						that.App.mainRegion.show(formDistanciaMotoCorrida);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'DistanciaMotoCorridas',
					itemSubFolderName : 'Formulário de atualização de Distancia moto corrida',
					url : 'app/distanciaMotoCorridas'
				});
			}
		},
		
		//configuração das rotas de Empresa
		empresas: function() {
			util.markActiveItem('empresas');
			this.pageEmpresa = new PageEmpresa();
			this.App.mainRegion.show(this.pageEmpresa);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Empresa',
				itemSubFolderName : 'Listagem',
				url : 'app/empresas'
			});
		},

		newEmpresa: function() {
			util.markActiveItem('empresas');
			var formEmpresa = new FormEmpresa({
				model : new EmpresaModel(),
			});
			this.App.mainRegion.show(formEmpresa);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Empresa',
				itemSubFolderName : 'Formulário de cadastro de Empresa',
				url : 'app/empresas'
			});
		},
		
		editEmpresa: function(idEmpresa) {
			var that = this;
			util.markActiveItem('empresas');
			var formEmpresa = null;
			if (this.pageEmpresa) {
				formEmpresa = new FormEmpresa({
					model : this.pageEmpresa.empresas.get(idEmpresa),
				});
				that.App.mainRegion.show(formEmpresa);
			} else {
				var model = new EmpresaModel({
					id : idEmpresa,
				})
				model.fetch({
					success : function(model) {
						formEmpresa = new FormEmpresa({
							model : model,
						});
						that.App.mainRegion.show(formEmpresa);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Empresas',
					itemSubFolderName : 'Formulário de atualização de Empresa',
					url : 'app/empresas'
				});
			}
		},
		
		//configuração das rotas de EnderecoCorrida
		enderecoCorridas: function() {
			util.markActiveItem('enderecoCorridas');
			this.pageEnderecoCorrida = new PageEnderecoCorrida();
			this.App.mainRegion.show(this.pageEnderecoCorrida);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Endereco corrida',
				itemSubFolderName : 'Listagem',
				url : 'app/enderecoCorridas'
			});
		},

		newEnderecoCorrida: function() {
			util.markActiveItem('enderecoCorridas');
			var formEnderecoCorrida = new FormEnderecoCorrida({
				model : new EnderecoCorridaModel(),
			});
			this.App.mainRegion.show(formEnderecoCorrida);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Endereco corrida',
				itemSubFolderName : 'Formulário de cadastro de Endereco corrida',
				url : 'app/enderecoCorridas'
			});
		},
		
		editEnderecoCorrida: function(idEnderecoCorrida) {
			var that = this;
			util.markActiveItem('enderecoCorridas');
			var formEnderecoCorrida = null;
			if (this.pageEnderecoCorrida) {
				formEnderecoCorrida = new FormEnderecoCorrida({
					model : this.pageEnderecoCorrida.enderecoCorridas.get(idEnderecoCorrida),
				});
				that.App.mainRegion.show(formEnderecoCorrida);
			} else {
				var model = new EnderecoCorridaModel({
					id : idEnderecoCorrida,
				})
				model.fetch({
					success : function(model) {
						formEnderecoCorrida = new FormEnderecoCorrida({
							model : model,
						});
						that.App.mainRegion.show(formEnderecoCorrida);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'EnderecoCorridas',
					itemSubFolderName : 'Formulário de atualização de Endereco corrida',
					url : 'app/enderecoCorridas'
				});
			}
		},
		
		//configuração das rotas de Evento
		eventos: function() {
			util.markActiveItem('eventos');
			this.pageEvento = new PageEvento();
			this.App.mainRegion.show(this.pageEvento);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Evento',
				itemSubFolderName : 'Listagem',
				url : 'app/eventos'
			});
		},

		newEvento: function() {
			util.markActiveItem('eventos');
			var formEvento = new FormEvento({
				model : new EventoModel(),
			});
			this.App.mainRegion.show(formEvento);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Evento',
				itemSubFolderName : 'Formulário de cadastro de Evento',
				url : 'app/eventos'
			});
		},
		
		editEvento: function(idEvento) {
			var that = this;
			util.markActiveItem('eventos');
			var formEvento = null;
			if (this.pageEvento) {
				formEvento = new FormEvento({
					model : this.pageEvento.eventos.get(idEvento),
				});
				that.App.mainRegion.show(formEvento);
			} else {
				var model = new EventoModel({
					id : idEvento,
				})
				model.fetch({
					success : function(model) {
						formEvento = new FormEvento({
							model : model,
						});
						that.App.mainRegion.show(formEvento);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Eventos',
					itemSubFolderName : 'Formulário de atualização de Evento',
					url : 'app/eventos'
				});
			}
		},
		
		//configuração das rotas de FaixaVoucher
		faixaVouchers: function() {
			util.markActiveItem('faixaVouchers');
			this.pageFaixaVoucher = new PageFaixaVoucher();
			this.App.mainRegion.show(this.pageFaixaVoucher);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Faixa voucher',
				itemSubFolderName : 'Listagem',
				url : 'app/faixaVouchers'
			});
		},

		newFaixaVoucher: function() {
			util.markActiveItem('faixaVouchers');
			var formFaixaVoucher = new FormFaixaVoucher({
				model : new FaixaVoucherModel(),
			});
			this.App.mainRegion.show(formFaixaVoucher);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Faixa voucher',
				itemSubFolderName : 'Formulário de cadastro de Faixa voucher',
				url : 'app/faixaVouchers'
			});
		},
		
		editFaixaVoucher: function(idFaixaVoucher) {
			var that = this;
			util.markActiveItem('faixaVouchers');
			var formFaixaVoucher = null;
			if (this.pageFaixaVoucher) {
				formFaixaVoucher = new FormFaixaVoucher({
					model : this.pageFaixaVoucher.faixaVouchers.get(idFaixaVoucher),
				});
				that.App.mainRegion.show(formFaixaVoucher);
			} else {
				var model = new FaixaVoucherModel({
					id : idFaixaVoucher,
				})
				model.fetch({
					success : function(model) {
						formFaixaVoucher = new FormFaixaVoucher({
							model : model,
						});
						that.App.mainRegion.show(formFaixaVoucher);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'FaixaVouchers',
					itemSubFolderName : 'Formulário de atualização de Faixa voucher',
					url : 'app/faixaVouchers'
				});
			}
		},
		
		//configuração das rotas de HdUsuario
		hdUsuarios: function() {
			util.markActiveItem('hdUsuarios');
			this.pageHdUsuario = new PageHdUsuario();
			this.App.mainRegion.show(this.pageHdUsuario);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Hd usuario',
				itemSubFolderName : 'Listagem',
				url : 'app/hdUsuarios'
			});
		},

		newHdUsuario: function() {
			util.markActiveItem('hdUsuarios');
			var formHdUsuario = new FormHdUsuario({
				model : new HdUsuarioModel(),
			});
			this.App.mainRegion.show(formHdUsuario);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Hd usuario',
				itemSubFolderName : 'Formulário de cadastro de Hd usuario',
				url : 'app/hdUsuarios'
			});
		},
		
		editHdUsuario: function(idHdUsuario) {
			var that = this;
			util.markActiveItem('hdUsuarios');
			var formHdUsuario = null;
			if (this.pageHdUsuario) {
				formHdUsuario = new FormHdUsuario({
					model : this.pageHdUsuario.hdUsuarios.get(idHdUsuario),
				});
				that.App.mainRegion.show(formHdUsuario);
			} else {
				var model = new HdUsuarioModel({
					id : idHdUsuario,
				})
				model.fetch({
					success : function(model) {
						formHdUsuario = new FormHdUsuario({
							model : model,
						});
						that.App.mainRegion.show(formHdUsuario);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'HdUsuarios',
					itemSubFolderName : 'Formulário de atualização de Hd usuario',
					url : 'app/hdUsuarios'
				});
			}
		},
		
		//configuração das rotas de Mensagem
		mensagems: function() {
			util.markActiveItem('mensagems');
			this.pageMensagem = new PageMensagem();
			this.App.mainRegion.show(this.pageMensagem);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Mensagem',
				itemSubFolderName : 'Listagem',
				url : 'app/mensagems'
			});
		},

		newMensagem: function() {
			util.markActiveItem('mensagems');
			var formMensagem = new FormMensagem({
				model : new MensagemModel(),
			});
			this.App.mainRegion.show(formMensagem);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Mensagem',
				itemSubFolderName : 'Formulário de cadastro de Mensagem',
				url : 'app/mensagems'
			});
		},
		
		editMensagem: function(idMensagem) {
			var that = this;
			util.markActiveItem('mensagems');
			var formMensagem = null;
			if (this.pageMensagem) {
				formMensagem = new FormMensagem({
					model : this.pageMensagem.mensagems.get(idMensagem),
				});
				that.App.mainRegion.show(formMensagem);
			} else {
				var model = new MensagemModel({
					id : idMensagem,
				})
				model.fetch({
					success : function(model) {
						formMensagem = new FormMensagem({
							model : model,
						});
						that.App.mainRegion.show(formMensagem);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Mensagems',
					itemSubFolderName : 'Formulário de atualização de Mensagem',
					url : 'app/mensagems'
				});
			}
		},
		
		//configuração das rotas de MensagemMotorista
		mensagemMotoristas: function() {
			util.markActiveItem('mensagemMotoristas');
			this.pageMensagemMotorista = new PageMensagemMotorista();
			this.App.mainRegion.show(this.pageMensagemMotorista);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Mensagem motorista',
				itemSubFolderName : 'Listagem',
				url : 'app/mensagemMotoristas'
			});
		},

		newMensagemMotorista: function() {
			util.markActiveItem('mensagemMotoristas');
			var formMensagemMotorista = new FormMensagemMotorista({
				model : new MensagemMotoristaModel(),
			});
			this.App.mainRegion.show(formMensagemMotorista);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Mensagem motorista',
				itemSubFolderName : 'Formulário de cadastro de Mensagem motorista',
				url : 'app/mensagemMotoristas'
			});
		},
		
		editMensagemMotorista: function(idMensagemMotorista) {
			var that = this;
			util.markActiveItem('mensagemMotoristas');
			var formMensagemMotorista = null;
			if (this.pageMensagemMotorista) {
				formMensagemMotorista = new FormMensagemMotorista({
					model : this.pageMensagemMotorista.mensagemMotoristas.get(idMensagemMotorista),
				});
				that.App.mainRegion.show(formMensagemMotorista);
			} else {
				var model = new MensagemMotoristaModel({
					id : idMensagemMotorista,
				})
				model.fetch({
					success : function(model) {
						formMensagemMotorista = new FormMensagemMotorista({
							model : model,
						});
						that.App.mainRegion.show(formMensagemMotorista);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'MensagemMotoristas',
					itemSubFolderName : 'Formulário de atualização de Mensagem motorista',
					url : 'app/mensagemMotoristas'
				});
			}
		},
		
		//configuração das rotas de Motorista
		motoristas: function() {
			util.markActiveItem('motoristas');
			this.pageMotorista = new PageMotorista();
			this.App.mainRegion.show(this.pageMotorista);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Motoqueiro',
				itemSubFolderName : 'Listagem',
				url : 'app/motoristas'
			});
		},

		newMotorista: function() {
			util.markActiveItem('motoristas');
			var formMotorista = new FormMotorista({
				model : new MotoristaModel(),
			});
			this.App.mainRegion.show(formMotorista);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Motoqueiro',
				itemSubFolderName : 'Formulário de cadastro de Motoqueiro',
				url : 'app/motoristas'
			});
		},
		
		editMotorista: function(idMotorista) {
			var that = this;
			util.markActiveItem('motoristas');
			var formMotorista = null;
			if (this.pageMotorista) {
				formMotorista = new FormMotorista({
					model : this.pageMotorista.motoristas.get(idMotorista),
				});
				that.App.mainRegion.show(formMotorista);
			} else {
				var model = new MotoristaModel({
					id : idMotorista,
				})
				model.fetch({
					success : function(model) {
						formMotorista = new FormMotorista({
							model : model,
						});
						that.App.mainRegion.show(formMotorista);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Motoristas',
					itemSubFolderName : 'Formulário de atualização de Motoqueiro',
					url : 'app/motoristas'
				});
			}
		},
		
		//configuração das rotas de MotoristaBloqueado
		motoristaBloqueados: function() {
			util.markActiveItem('motoristaBloqueados');
			this.pageMotoristaBloqueado = new PageMotoristaBloqueado();
			this.App.mainRegion.show(this.pageMotoristaBloqueado);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Motorista bloqueado',
				itemSubFolderName : 'Listagem',
				url : 'app/motoristaBloqueados'
			});
		},

		newMotoristaBloqueado: function() {
			util.markActiveItem('motoristaBloqueados');
			var formMotoristaBloqueado = new FormMotoristaBloqueado({
				model : new MotoristaBloqueadoModel(),
			});
			this.App.mainRegion.show(formMotoristaBloqueado);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Motorista bloqueado',
				itemSubFolderName : 'Formulário de cadastro de Motorista bloqueado',
				url : 'app/motoristaBloqueados'
			});
		},
		
		editMotoristaBloqueado: function(idMotoristaBloqueado) {
			var that = this;
			util.markActiveItem('motoristaBloqueados');
			var formMotoristaBloqueado = null;
			if (this.pageMotoristaBloqueado) {
				formMotoristaBloqueado = new FormMotoristaBloqueado({
					model : this.pageMotoristaBloqueado.motoristaBloqueados.get(idMotoristaBloqueado),
				});
				that.App.mainRegion.show(formMotoristaBloqueado);
			} else {
				var model = new MotoristaBloqueadoModel({
					id : idMotoristaBloqueado,
				})
				model.fetch({
					success : function(model) {
						formMotoristaBloqueado = new FormMotoristaBloqueado({
							model : model,
						});
						that.App.mainRegion.show(formMotoristaBloqueado);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'MotoristaBloqueados',
					itemSubFolderName : 'Formulário de atualização de Motorista bloqueado',
					url : 'app/motoristaBloqueados'
				});
			}
		},
		
		//configuração das rotas de MotoristaHasPlantao
		motoristaHasPlantaos: function() {
			util.markActiveItem('motoristaHasPlantaos');
			this.pageMotoristaHasPlantao = new PageMotoristaHasPlantao();
			this.App.mainRegion.show(this.pageMotoristaHasPlantao);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Motorista has plantao',
				itemSubFolderName : 'Listagem',
				url : 'app/motoristaHasPlantaos'
			});
		},

		newMotoristaHasPlantao: function() {
			util.markActiveItem('motoristaHasPlantaos');
			var formMotoristaHasPlantao = new FormMotoristaHasPlantao({
				model : new MotoristaHasPlantaoModel(),
			});
			this.App.mainRegion.show(formMotoristaHasPlantao);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Motorista has plantao',
				itemSubFolderName : 'Formulário de cadastro de Motorista has plantao',
				url : 'app/motoristaHasPlantaos'
			});
		},
		
		editMotoristaHasPlantao: function(idMotoristaHasPlantao) {
			var that = this;
			util.markActiveItem('motoristaHasPlantaos');
			var formMotoristaHasPlantao = null;
			if (this.pageMotoristaHasPlantao) {
				formMotoristaHasPlantao = new FormMotoristaHasPlantao({
					model : this.pageMotoristaHasPlantao.motoristaHasPlantaos.get(idMotoristaHasPlantao),
				});
				that.App.mainRegion.show(formMotoristaHasPlantao);
			} else {
				var model = new MotoristaHasPlantaoModel({
					id : idMotoristaHasPlantao,
				})
				model.fetch({
					success : function(model) {
						formMotoristaHasPlantao = new FormMotoristaHasPlantao({
							model : model,
						});
						that.App.mainRegion.show(formMotoristaHasPlantao);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'MotoristaHasPlantaos',
					itemSubFolderName : 'Formulário de atualização de Motorista has plantao',
					url : 'app/motoristaHasPlantaos'
				});
			}
		},
		
		//configuração das rotas de Plantao
		plantaos: function() {
			util.markActiveItem('plantaos');
			this.pagePlantao = new PagePlantao();
			this.App.mainRegion.show(this.pagePlantao);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Plantao',
				itemSubFolderName : 'Listagem',
				url : 'app/plantaos'
			});
		},

		newPlantao: function() {
			util.markActiveItem('plantaos');
			var formPlantao = new FormPlantao({
				model : new PlantaoModel(),
			});
			this.App.mainRegion.show(formPlantao);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Plantao',
				itemSubFolderName : 'Formulário de cadastro de Plantao',
				url : 'app/plantaos'
			});
		},
		
		editPlantao: function(idPlantao) {
			var that = this;
			util.markActiveItem('plantaos');
			var formPlantao = null;
			if (this.pagePlantao) {
				formPlantao = new FormPlantao({
					model : this.pagePlantao.plantaos.get(idPlantao),
				});
				that.App.mainRegion.show(formPlantao);
			} else {
				var model = new PlantaoModel({
					id : idPlantao,
				})
				model.fetch({
					success : function(model) {
						formPlantao = new FormPlantao({
							model : model,
						});
						that.App.mainRegion.show(formPlantao);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Plantaos',
					itemSubFolderName : 'Formulário de atualização de Plantao',
					url : 'app/plantaos'
				});
			}
		},
		
		//configuração das rotas de PontoArea
		pontoAreas: function() {
			util.markActiveItem('pontoAreas');
			this.pagePontoArea = new PagePontoArea();
			this.App.mainRegion.show(this.pagePontoArea);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Pontos area',
				itemSubFolderName : 'Listagem',
				url : 'app/pontoAreas'
			});
		},

		newPontoArea: function() {
			util.markActiveItem('pontoAreas');
			var formPontoArea = new FormPontoArea({
				model : new PontoAreaModel(),
			});
			this.App.mainRegion.show(formPontoArea);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Pontos area',
				itemSubFolderName : 'Formulário de cadastro de Pontos area',
				url : 'app/pontoAreas'
			});
		},
		
		editPontoArea: function(idPontoArea) {
			var that = this;
			util.markActiveItem('pontoAreas');
			var formPontoArea = null;
			if (this.pagePontoArea) {
				formPontoArea = new FormPontoArea({
					model : this.pagePontoArea.pontoAreas.get(idPontoArea),
				});
				that.App.mainRegion.show(formPontoArea);
			} else {
				var model = new PontoAreaModel({
					id : idPontoArea,
				})
				model.fetch({
					success : function(model) {
						formPontoArea = new FormPontoArea({
							model : model,
						});
						that.App.mainRegion.show(formPontoArea);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'PontoAreas',
					itemSubFolderName : 'Formulário de atualização de Pontos area',
					url : 'app/pontoAreas'
				});
			}
		},
		
		//configuração das rotas de Punicao
		punicaos: function() {
			util.markActiveItem('punicaos');
			this.pagePunicao = new PagePunicao();
			this.App.mainRegion.show(this.pagePunicao);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Punicao',
				itemSubFolderName : 'Listagem',
				url : 'app/punicaos'
			});
		},

		newPunicao: function() {
			util.markActiveItem('punicaos');
			var formPunicao = new FormPunicao({
				model : new PunicaoModel(),
			});
			this.App.mainRegion.show(formPunicao);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Punicao',
				itemSubFolderName : 'Formulário de cadastro de Punicao',
				url : 'app/punicaos'
			});
		},
		
		editPunicao: function(idPunicao) {
			var that = this;
			util.markActiveItem('punicaos');
			var formPunicao = null;
			if (this.pagePunicao) {
				formPunicao = new FormPunicao({
					model : this.pagePunicao.punicaos.get(idPunicao),
				});
				that.App.mainRegion.show(formPunicao);
			} else {
				var model = new PunicaoModel({
					id : idPunicao,
				})
				model.fetch({
					success : function(model) {
						formPunicao = new FormPunicao({
							model : model,
						});
						that.App.mainRegion.show(formPunicao);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Punicaos',
					itemSubFolderName : 'Formulário de atualização de Punicao',
					url : 'app/punicaos'
				});
			}
		},
		
		//configuração das rotas de Socio
		socios: function() {
			util.markActiveItem('socios');
			this.pageSocio = new PageSocio();
			this.App.mainRegion.show(this.pageSocio);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Socio',
				itemSubFolderName : 'Listagem',
				url : 'app/socios'
			});
		},

		newSocio: function() {
			util.markActiveItem('socios');
			var formSocio = new FormSocio({
				model : new SocioModel(),
			});
			this.App.mainRegion.show(formSocio);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Socio',
				itemSubFolderName : 'Formulário de cadastro de Socio',
				url : 'app/socios'
			});
		},
		
		editSocio: function(idSocio) {
			var that = this;
			util.markActiveItem('socios');
			var formSocio = null;
			if (this.pageSocio) {
				formSocio = new FormSocio({
					model : this.pageSocio.socios.get(idSocio),
				});
				that.App.mainRegion.show(formSocio);
			} else {
				var model = new SocioModel({
					id : idSocio,
				})
				model.fetch({
					success : function(model) {
						formSocio = new FormSocio({
							model : model,
						});
						that.App.mainRegion.show(formSocio);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Socios',
					itemSubFolderName : 'Formulário de atualização de Socio',
					url : 'app/socios'
				});
			}
		},
		
		//configuração das rotas de StatusTracker
		statusTrackers: function() {
			util.markActiveItem('statusTrackers');
			this.pageStatusTracker = new PageStatusTracker();
			this.App.mainRegion.show(this.pageStatusTracker);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Status tracker',
				itemSubFolderName : 'Listagem',
				url : 'app/statusTrackers'
			});
		},

		newStatusTracker: function() {
			util.markActiveItem('statusTrackers');
			var formStatusTracker = new FormStatusTracker({
				model : new StatusTrackerModel(),
			});
			this.App.mainRegion.show(formStatusTracker);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Status tracker',
				itemSubFolderName : 'Formulário de cadastro de Status tracker',
				url : 'app/statusTrackers'
			});
		},
		
		editStatusTracker: function(idStatusTracker) {
			var that = this;
			util.markActiveItem('statusTrackers');
			var formStatusTracker = null;
			if (this.pageStatusTracker) {
				formStatusTracker = new FormStatusTracker({
					model : this.pageStatusTracker.statusTrackers.get(idStatusTracker),
				});
				that.App.mainRegion.show(formStatusTracker);
			} else {
				var model = new StatusTrackerModel({
					id : idStatusTracker,
				})
				model.fetch({
					success : function(model) {
						formStatusTracker = new FormStatusTracker({
							model : model,
						});
						that.App.mainRegion.show(formStatusTracker);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'StatusTrackers',
					itemSubFolderName : 'Formulário de atualização de Status tracker',
					url : 'app/statusTrackers'
				});
			}
		},
		
		//configuração das rotas de Tarifa
		tarifas: function() {
			util.markActiveItem('tarifas');
			this.pageTarifa = new PageTarifa();
			this.App.mainRegion.show(this.pageTarifa);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Tarifa',
				itemSubFolderName : 'Listagem',
				url : 'app/tarifas'
			});
		},

		newTarifa: function() {
			util.markActiveItem('tarifas');
			var formTarifa = new FormTarifa({
				model : new TarifaModel(),
			});
			this.App.mainRegion.show(formTarifa);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Tarifa',
				itemSubFolderName : 'Formulário de cadastro de Tarifa',
				url : 'app/tarifas'
			});
		},
		
		editTarifa: function(idTarifa) {
			var that = this;
			util.markActiveItem('tarifas');
			var formTarifa = null;
			if (this.pageTarifa) {
				formTarifa = new FormTarifa({
					model : this.pageTarifa.tarifas.get(idTarifa),
				});
				that.App.mainRegion.show(formTarifa);
			} else {
				var model = new TarifaModel({
					id : idTarifa,
				})
				model.fetch({
					success : function(model) {
						formTarifa = new FormTarifa({
							model : model,
						});
						that.App.mainRegion.show(formTarifa);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Tarifas',
					itemSubFolderName : 'Formulário de atualização de Tarifa',
					url : 'app/tarifas'
				});
			}
		},
		
		//configuração das rotas de TarifaAdicional
		tarifaAdicionals: function() {
			util.markActiveItem('tarifaAdicionals');
			this.pageTarifaAdicional = new PageTarifaAdicional();
			this.App.mainRegion.show(this.pageTarifaAdicional);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Tarifa adicional',
				itemSubFolderName : 'Listagem',
				url : 'app/tarifaAdicionals'
			});
		},

		newTarifaAdicional: function() {
			util.markActiveItem('tarifaAdicionals');
			var formTarifaAdicional = new FormTarifaAdicional({
				model : new TarifaAdicionalModel(),
			});
			this.App.mainRegion.show(formTarifaAdicional);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Tarifa adicional',
				itemSubFolderName : 'Formulário de cadastro de Tarifa adicional',
				url : 'app/tarifaAdicionals'
			});
		},
		
		editTarifaAdicional: function(idTarifaAdicional) {
			var that = this;
			util.markActiveItem('tarifaAdicionals');
			var formTarifaAdicional = null;
			if (this.pageTarifaAdicional) {
				formTarifaAdicional = new FormTarifaAdicional({
					model : this.pageTarifaAdicional.tarifaAdicionals.get(idTarifaAdicional),
				});
				that.App.mainRegion.show(formTarifaAdicional);
			} else {
				var model = new TarifaAdicionalModel({
					id : idTarifaAdicional,
				})
				model.fetch({
					success : function(model) {
						formTarifaAdicional = new FormTarifaAdicional({
							model : model,
						});
						that.App.mainRegion.show(formTarifaAdicional);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'TarifaAdicionals',
					itemSubFolderName : 'Formulário de atualização de Tarifa adicional',
					url : 'app/tarifaAdicionals'
				});
			}
		},
		
		//configuração das rotas de VisitaTracker
		visitaTrackers: function() {
			util.markActiveItem('visitaTrackers');
			this.pageVisitaTracker = new PageVisitaTracker();
			this.App.mainRegion.show(this.pageVisitaTracker);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Visita tracker',
				itemSubFolderName : 'Listagem',
				url : 'app/visitaTrackers'
			});
		},

		newVisitaTracker: function() {
			util.markActiveItem('visitaTrackers');
			var formVisitaTracker = new FormVisitaTracker({
				model : new VisitaTrackerModel(),
			});
			this.App.mainRegion.show(formVisitaTracker);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Visita tracker',
				itemSubFolderName : 'Formulário de cadastro de Visita tracker',
				url : 'app/visitaTrackers'
			});
		},
		
		editVisitaTracker: function(idVisitaTracker) {
			var that = this;
			util.markActiveItem('visitaTrackers');
			var formVisitaTracker = null;
			if (this.pageVisitaTracker) {
				formVisitaTracker = new FormVisitaTracker({
					model : this.pageVisitaTracker.visitaTrackers.get(idVisitaTracker),
				});
				that.App.mainRegion.show(formVisitaTracker);
			} else {
				var model = new VisitaTrackerModel({
					id : idVisitaTracker,
				})
				model.fetch({
					success : function(model) {
						formVisitaTracker = new FormVisitaTracker({
							model : model,
						});
						that.App.mainRegion.show(formVisitaTracker);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'VisitaTrackers',
					itemSubFolderName : 'Formulário de atualização de Visita tracker',
					url : 'app/visitaTrackers'
				});
			}
		},
		
		//configuração das rotas de Bairro
		bairros: function() {
			util.markActiveItem('bairros');
			this.pageBairro = new PageBairro();
			this.App.mainRegion.show(this.pageBairro);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Bairro',
				itemSubFolderName : 'Listagem',
				url : 'app/bairros'
			});
		},

		newBairro: function() {
			util.markActiveItem('bairros');
			var formBairro = new FormBairro({
				model : new BairroModel(),
			});
			this.App.mainRegion.show(formBairro);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Bairro',
				itemSubFolderName : 'Formulário de cadastro de Bairro',
				url : 'app/bairros'
			});
		},
		
		editBairro: function(idBairro) {
			var that = this;
			util.markActiveItem('bairros');
			var formBairro = null;
			if (this.pageBairro) {
				formBairro = new FormBairro({
					model : this.pageBairro.bairros.get(idBairro),
				});
				that.App.mainRegion.show(formBairro);
			} else {
				var model = new BairroModel({
					id : idBairro,
				})
				model.fetch({
					success : function(model) {
						formBairro = new FormBairro({
							model : model,
						});
						that.App.mainRegion.show(formBairro);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Bairros',
					itemSubFolderName : 'Formulário de atualização de Bairro',
					url : 'app/bairros'
				});
			}
		},
		
		//configuração das rotas de Cep
		ceps: function() {
			util.markActiveItem('ceps');
			this.pageCep = new PageCep();
			this.App.mainRegion.show(this.pageCep);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cep',
				itemSubFolderName : 'Listagem',
				url : 'app/ceps'
			});
		},

		newCep: function() {
			util.markActiveItem('ceps');
			var formCep = new FormCep({
				model : new CepModel(),
			});
			this.App.mainRegion.show(formCep);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cep',
				itemSubFolderName : 'Formulário de cadastro de Cep',
				url : 'app/ceps'
			});
		},
		
		editCep: function(idCep) {
			var that = this;
			util.markActiveItem('ceps');
			var formCep = null;
			if (this.pageCep) {
				formCep = new FormCep({
					model : this.pageCep.ceps.get(idCep),
				});
				that.App.mainRegion.show(formCep);
			} else {
				var model = new CepModel({
					id : idCep,
				})
				model.fetch({
					success : function(model) {
						formCep = new FormCep({
							model : model,
						});
						that.App.mainRegion.show(formCep);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Ceps',
					itemSubFolderName : 'Formulário de atualização de Cep',
					url : 'app/ceps'
				});
			}
		},
		
		//configuração das rotas de Cidade
		cidades: function() {
			util.markActiveItem('cidades');
			this.pageCidade = new PageCidade();
			this.App.mainRegion.show(this.pageCidade);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cidade',
				itemSubFolderName : 'Listagem',
				url : 'app/cidades'
			});
		},

		newCidade: function() {
			util.markActiveItem('cidades');
			var formCidade = new FormCidade({
				model : new CidadeModel(),
			});
			this.App.mainRegion.show(formCidade);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cidade',
				itemSubFolderName : 'Formulário de cadastro de Cidade',
				url : 'app/cidades'
			});
		},
		
		editCidade: function(idCidade) {
			var that = this;
			util.markActiveItem('cidades');
			var formCidade = null;
			if (this.pageCidade) {
				formCidade = new FormCidade({
					model : this.pageCidade.cidades.get(idCidade),
				});
				that.App.mainRegion.show(formCidade);
			} else {
				var model = new CidadeModel({
					id : idCidade,
				})
				model.fetch({
					success : function(model) {
						formCidade = new FormCidade({
							model : model,
						});
						that.App.mainRegion.show(formCidade);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Cidades',
					itemSubFolderName : 'Formulário de atualização de Cidade',
					url : 'app/cidades'
				});
			}
		},
		
		//configuração das rotas de Estado
		estados: function() {
			util.markActiveItem('estados');
			this.pageEstado = new PageEstado();
			this.App.mainRegion.show(this.pageEstado);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Estado',
				itemSubFolderName : 'Listagem',
				url : 'app/estados'
			});
		},

		newEstado: function() {
			util.markActiveItem('estados');
			var formEstado = new FormEstado({
				model : new EstadoModel(),
			});
			this.App.mainRegion.show(formEstado);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Estado',
				itemSubFolderName : 'Formulário de cadastro de Estado',
				url : 'app/estados'
			});
		},
		
		editEstado: function(idEstado) {
			var that = this;
			util.markActiveItem('estados');
			var formEstado = null;
			if (this.pageEstado) {
				formEstado = new FormEstado({
					model : this.pageEstado.estados.get(idEstado),
				});
				that.App.mainRegion.show(formEstado);
			} else {
				var model = new EstadoModel({
					id : idEstado,
				})
				model.fetch({
					success : function(model) {
						formEstado = new FormEstado({
							model : model,
						});
						that.App.mainRegion.show(formEstado);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Estados',
					itemSubFolderName : 'Formulário de atualização de Estado',
					url : 'app/estados'
				});
			}
		},
		
		//configuração das rotas de Pais
		paiss: function() {
			util.markActiveItem('paiss');
			this.pagePais = new PagePais();
			this.App.mainRegion.show(this.pagePais);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Pais',
				itemSubFolderName : 'Listagem',
				url : 'app/paiss'
			});
		},

		newPais: function() {
			util.markActiveItem('paiss');
			var formPais = new FormPais({
				model : new PaisModel(),
			});
			this.App.mainRegion.show(formPais);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Pais',
				itemSubFolderName : 'Formulário de cadastro de Pais',
				url : 'app/paiss'
			});
		},
		
		editPais: function(idPais) {
			var that = this;
			util.markActiveItem('paiss');
			var formPais = null;
			if (this.pagePais) {
				formPais = new FormPais({
					model : this.pagePais.paiss.get(idPais),
				});
				that.App.mainRegion.show(formPais);
			} else {
				var model = new PaisModel({
					id : idPais,
				})
				model.fetch({
					success : function(model) {
						formPais = new FormPais({
							model : model,
						});
						that.App.mainRegion.show(formPais);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Paiss',
					itemSubFolderName : 'Formulário de atualização de Pais',
					url : 'app/paiss'
				});
			}
		},
		
		//configuração das rotas de Item
		items: function() {
			util.markActiveItem('items');
			this.pageItem = new PageItem();
			this.App.mainRegion.show(this.pageItem);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Item',
				itemSubFolderName : 'Listagem',
				url : 'app/items'
			});
		},

		newItem: function() {
			util.markActiveItem('items');
			var formItem = new FormItem({
				model : new ItemModel(),
			});
			this.App.mainRegion.show(formItem);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Item',
				itemSubFolderName : 'Formulário de cadastro de Item',
				url : 'app/items'
			});
		},
		
		editItem: function(idItem) {
			var that = this;
			util.markActiveItem('items');
			var formItem = null;
			if (this.pageItem) {
				formItem = new FormItem({
					model : this.pageItem.items.get(idItem),
				});
				that.App.mainRegion.show(formItem);
			} else {
				var model = new ItemModel({
					id : idItem,
				})
				model.fetch({
					success : function(model) {
						formItem = new FormItem({
							model : model,
						});
						that.App.mainRegion.show(formItem);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Items',
					itemSubFolderName : 'Formulário de atualização de Item',
					url : 'app/items'
				});
			}
		},
		
		//configuração das rotas de ItemType
		itemTypes: function() {
			util.markActiveItem('itemTypes');
			this.pageItemType = new PageItemType();
			this.App.mainRegion.show(this.pageItemType);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Tipo de Item',
				itemSubFolderName : 'Listagem',
				url : 'app/itemTypes'
			});
		},

		newItemType: function() {
			util.markActiveItem('itemTypes');
			var formItemType = new FormItemType({
				model : new ItemTypeModel(),
			});
			this.App.mainRegion.show(formItemType);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Tipo de Item',
				itemSubFolderName : 'Formulário de cadastro de Tipo de Item',
				url : 'app/itemTypes'
			});
		},
		
		editItemType: function(idItemType) {
			var that = this;
			util.markActiveItem('itemTypes');
			var formItemType = null;
			if (this.pageItemType) {
				formItemType = new FormItemType({
					model : this.pageItemType.itemTypes.get(idItemType),
				});
				that.App.mainRegion.show(formItemType);
			} else {
				var model = new ItemTypeModel({
					id : idItemType,
				})
				model.fetch({
					success : function(model) {
						formItemType = new FormItemType({
							model : model,
						});
						that.App.mainRegion.show(formItemType);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'ItemTypes',
					itemSubFolderName : 'Formulário de atualização de Tipo de Item',
					url : 'app/itemTypes'
				});
			}
		},
		
		//configuração das rotas de Operation
		operations: function() {
			util.markActiveItem('operations');
			this.pageOperation = new PageOperation();
			this.App.mainRegion.show(this.pageOperation);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Operação',
				itemSubFolderName : 'Listagem',
				url : 'app/operations'
			});
		},

		newOperation: function() {
			util.markActiveItem('operations');
			var formOperation = new FormOperation({
				model : new OperationModel(),
			});
			this.App.mainRegion.show(formOperation);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Operação',
				itemSubFolderName : 'Formulário de cadastro de Operação',
				url : 'app/operations'
			});
		},
		
		editOperation: function(idOperation) {
			var that = this;
			util.markActiveItem('operations');
			var formOperation = null;
			if (this.pageOperation) {
				formOperation = new FormOperation({
					model : this.pageOperation.operations.get(idOperation),
				});
				that.App.mainRegion.show(formOperation);
			} else {
				var model = new OperationModel({
					id : idOperation,
				})
				model.fetch({
					success : function(model) {
						formOperation = new FormOperation({
							model : model,
						});
						that.App.mainRegion.show(formOperation);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Operations',
					itemSubFolderName : 'Formulário de atualização de Operação',
					url : 'app/operations'
				});
			}
		},
		
		//configuração das rotas de Permission
		permissions: function() {
			util.markActiveItem('permissions');
			this.pagePermission = new PagePermission();
			this.App.mainRegion.show(this.pagePermission);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Permissão',
				itemSubFolderName : 'Listagem',
				url : 'app/permissions'
			});
		},

		newPermission: function() {
			util.markActiveItem('permissions');
			var formPermission = new FormPermission({
				model : new PermissionModel(),
			});
			this.App.mainRegion.show(formPermission);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Permissão',
				itemSubFolderName : 'Formulário de cadastro de Permissão',
				url : 'app/permissions'
			});
		},
		
		editPermission: function(idPermission) {
			var that = this;
			util.markActiveItem('permissions');
			var formPermission = null;
			if (this.pagePermission) {
				formPermission = new FormPermission({
					model : this.pagePermission.permissions.get(idPermission),
				});
				that.App.mainRegion.show(formPermission);
			} else {
				var model = new PermissionModel({
					id : idPermission,
				})
				model.fetch({
					success : function(model) {
						formPermission = new FormPermission({
							model : model,
						});
						that.App.mainRegion.show(formPermission);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Permissions',
					itemSubFolderName : 'Formulário de atualização de Permissão',
					url : 'app/permissions'
				});
			}
		},
		
		//configuração das rotas de Role
		roles: function() {
			util.markActiveItem('roles');
			this.pageRole = new PageRole();
			this.App.mainRegion.show(this.pageRole);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Papel',
				itemSubFolderName : 'Listagem',
				url : 'app/roles'
			});
		},

		newRole: function() {
			util.markActiveItem('roles');
			var formRole = new FormRole({
				model : new RoleModel(),
			});
			this.App.mainRegion.show(formRole);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Papel',
				itemSubFolderName : 'Formulário de cadastro de Papel',
				url : 'app/roles'
			});
		},
		
		editRole: function(idRole) {
			var that = this;
			util.markActiveItem('roles');
			var formRole = null;
			if (this.pageRole) {
				formRole = new FormRole({
					model : this.pageRole.roles.get(idRole),
				});
				that.App.mainRegion.show(formRole);
			} else {
				var model = new RoleModel({
					id : idRole,
				})
				model.fetch({
					success : function(model) {
						formRole = new FormRole({
							model : model,
						});
						that.App.mainRegion.show(formRole);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Roles',
					itemSubFolderName : 'Formulário de atualização de Papel',
					url : 'app/roles'
				});
			}
		},
		
		//configuração das rotas de Session
		sessions: function() {
			util.markActiveItem('sessions');
			this.pageSession = new PageSession();
			this.App.mainRegion.show(this.pageSession);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Sessão',
				itemSubFolderName : 'Listagem',
				url : 'app/sessions'
			});
		},

		newSession: function() {
			util.markActiveItem('sessions');
			var formSession = new FormSession({
				model : new SessionModel(),
			});
			this.App.mainRegion.show(formSession);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Sessão',
				itemSubFolderName : 'Formulário de cadastro de Sessão',
				url : 'app/sessions'
			});
		},
		
		editSession: function(idSession) {
			var that = this;
			util.markActiveItem('sessions');
			var formSession = null;
			if (this.pageSession) {
				formSession = new FormSession({
					model : this.pageSession.sessions.get(idSession),
				});
				that.App.mainRegion.show(formSession);
			} else {
				var model = new SessionModel({
					id : idSession,
				})
				model.fetch({
					success : function(model) {
						formSession = new FormSession({
							model : model,
						});
						that.App.mainRegion.show(formSession);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Sessions',
					itemSubFolderName : 'Formulário de atualização de Sessão',
					url : 'app/sessions'
				});
			}
		},
		
		//configuração das rotas de User
		users: function() {
			util.markActiveItem('users');
			this.pageUser = new PageUser();
			this.App.mainRegion.show(this.pageUser);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Usuário',
				itemSubFolderName : 'Listagem',
				url : 'app/users'
			});
		},

		newUser: function() {
			util.markActiveItem('users');
			var formUser = new FormUser({
				model : new UserModel(),
			});
			this.App.mainRegion.show(formUser);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Usuário',
				itemSubFolderName : 'Formulário de cadastro de Usuário',
				url : 'app/users'
			});
		},
		
		editUser: function(idUser) {
			var that = this;
			util.markActiveItem('users');
			var formUser = null;
			if (this.pageUser) {
				formUser = new FormUser({
					model : this.pageUser.users.get(idUser),
				});
				that.App.mainRegion.show(formUser);
			} else {
				var model = new UserModel({
					id : idUser,
				})
				model.fetch({
					success : function(model) {
						formUser = new FormUser({
							model : model,
						});
						that.App.mainRegion.show(formUser);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Users',
					itemSubFolderName : 'Formulário de atualização de Usuário',
					url : 'app/users'
				});
			}
		},
		
		start : function() {
			Backbone.history.start();
		}
	});
	return AppRouter;
});
