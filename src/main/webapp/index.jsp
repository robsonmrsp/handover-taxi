<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='security' uri='http://www.springframework.org/security/tags'%>
<c:set var="authenticated" value="false" />
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="icon" href="images/ico/favicon.png" type="image/png">
<title>HandoverTaxi</title>
<link rel="stylesheet" href=css/all.css " />
<!--[if lt IE 9]><script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<script src="https://cdn.firebase.com/js/client/2.4.2/firebase.js"></script>
<!-- LigMotoTaxi Produção -->
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0yWaBY53ZihT4EcKCiBDSMoGq0fx8zGQ&libraries=geometry,places"></script> -->
<!-- Handover -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxu_kxyx7MNpiAYUIAzNhl123nY2RbkQk&libraries=geometry,places"></script>
<security:authorize access="isAuthenticated()">
	<c:set var="authenticated" value="true" />
	<c:set var="userName">
		<security:authentication property="principal.username" />
	</c:set>
</security:authorize>
<body class="no-skin">
	<div class="splash" id="loadInitialPanel" class="fader" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 99999999999; opacity: 0.99;">
		<div class="color-line"></div>
		<div class="splash-title">
			<h1>Aguarde, carregando...</h1>
			<img src="images/loading-bars.svg" width="64" height="64">
		</div>
	</div>
	<div id="navbar" class="navbar navbar-default navbar-fixed-top">
		<div class="navbar-container" id="navbar-container">
			<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
				<span class="sr-only">Toggle sidebar</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="navbar-header pull-left">
				<a href="#" class="navbar-brand">
					<small>
						<i class="fa fa-desktop"></i>
						&nbsp;&nbsp;HandoverTaxi
					</small>
				</a>
			</div>
			<div class="navbar-buttons navbar-header pull-right" role="navigation">
				<ul class="nav ace-nav">
					<li class="light-blue">
						<a data-toggle="dropdown" href="#" class="dropdown-toggle">
							<img class="nav-user-photo" src="images/avatar2.png" alt="" width="40" height="40" />
							<span id="specialUserName" class="user-info">
								<small ">Bem vindo,</small>
								${userName}
							</span>
							<i class="ace-icon fa fa-caret-down"></i>
						</a>
						<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
							<li>
								<a href="javascript:void(-1)">
									<i class="ace-icon fa fa-cogs"></i>
									Configurações
								</a>
							</li>
							<li>
								<a href="#app/userProfile">
									<i class="ace-icon fa fa-user"></i>
									Perfil do usuário
								</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="j_spring_security_logout">
									<i class="ace-icon fa fa-power-off"></i>
									Sair
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="main-container" id="main-container">
		<div id="sidebar" class="sidebar responsive ">
			<ul class="nav nav-list" id='nav-accordion'>
				<li>
					<a href="#">
						<i class="menu-icon fa fa-home"></i>
						<span class="menu-text"> Home </span>
					</a>
					<b class="arrow"></b>
				</li>
				<security:authorize access="hasAnyRole('ROLE_EMPRESA', 'ROLE_CLIENTE', 'ROLE_MOTOQUEIRO', 'ROLE_TARIFA', 'ROLE_TARIFAADICIONAL', 'ROLE_MOTIVOPUNICAO', 'ROLE_ADMIN')">
				<li class="">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fa fa-file-text-o"></i>
						<span class="menu-text"> Cadastros </span>
						<b class="arrow fa fa-angle-down"></b>
					</a>
					<b class="arrow"></b>
					<ul class="submenu">
						<!-- <li id="hdUsuarios" class="">
							<a href="#app/hdUsuarios">
								<i class="menu-icon fa fa-caret-right"></i>
								Usuário
							</a>
							<b class="arrow"></b>
						</li> -->
						<security:authorize access="hasAnyRole('ROLE_EMPRESA', 'ROLE_ADMIN')">
						<li id="empresas" class="">
							<a href="#app/empresas">
								<i class="menu-icon fa fa-caret-right"></i>
								Empresa
							</a>
							<b class="arrow"></b>
						</li>
						</security:authorize>
						<security:authorize access="hasAnyRole('ROLE_CLIENTE', 'ROLE_ADMIN')">
						<li id="clientes" class="">
							<a href="#app/clientes">
								<i class="menu-icon fa fa-caret-right"></i>
								Cliente
							</a>
							<b class="arrow"></b>
						</li>
						</security:authorize>
						<security:authorize access="hasAnyRole('ROLE_MOTOQUEIRO', 'ROLE_ADMIN')">
						<li id="motoristas" class="">
							<a href="#app/motoristas">
								<i class="menu-icon fa fa-caret-right"></i>
								Motoqueiro
							</a>
							<b class="arrow"></b>
						</li>
						</security:authorize>
						<!-- <li id="areas" class="">
							<a href="#app/areas">
								<i class="menu-icon fa fa-caret-right"></i>
								Área
							</a>
							<b class="arrow"></b>
						</li> -->
						<security:authorize access="hasAnyRole('ROLE_TARIFA', 'ROLE_ADMIN')">
						<li id="tarifas" class="">
							<a href="#app/tarifas">
								<i class="menu-icon fa fa-caret-right"></i>
								Tarifa
							</a>
							<b class="arrow"></b>
						</li>
						</security:authorize>
						<security:authorize access="hasAnyRole('ROLE_TARIFAADICIONAL', 'ROLE_ADMIN')">
						<li id="tarifaAdicionals" class="">
							<a href="#app/tarifaAdicionals">
								<i class="menu-icon fa fa-caret-right"></i>
								Tarifa adicional
							</a>
							<b class="arrow"></b>
						</li>
						</security:authorize>
						<security:authorize access="hasAnyRole('ROLE_MOTIVOPUNICAO', 'ROLE_ADMIN')">
						<li id="motivoPunicaos" class="">
							<a href="#app/motivoPunicaos">
								<i class="menu-icon fa fa-caret-right"></i>
								Motivo da Punição
							</a>
							<b class="arrow"></b>
						</li>
						</security:authorize>
						<!-- <li id="atendentes" class="">
							<a href="#app/atendentes">
								<i class="menu-icon fa fa-caret-right"></i>
								Atendente
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="centroCustos" class="">
							<a href="#app/centroCustos">
								<i class="menu-icon fa fa-caret-right"></i>
								Centro custo
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="funcionarios" class="">
							<a href="#app/funcionarios">
								<i class="menu-icon fa fa-caret-right"></i>
								Funcionário
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="clienteEnderecos" class=""> 
							<a href="#app/clienteEnderecos">
								<i class="menu-icon fa fa-caret-right"></i>
								Cliente endereco
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="clienteEnderecoCoords" class="">
							<a href="#app/clienteEnderecoCoords">
								<i class="menu-icon fa fa-caret-right"></i>
								Cliente endereco coord
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="clienteFones" class="">
							<a href="#app/clienteFones">
								<i class="menu-icon fa fa-caret-right"></i>
								Cliente fone
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="contatos" class="">
							<a href="#app/contatos">
								<i class="menu-icon fa fa-caret-right"></i>
								Contato
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="corridaEnderecos" class="">
							<a href="#app/corridaEnderecos">
								<i class="menu-icon fa fa-caret-right"></i>
								Corrida endereco
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="faixaVouchers" class="">
							<a href="#app/faixaVouchers">
								<i class="menu-icon fa fa-caret-right"></i>
								Faixa voucher
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="mensagems" class="">
							<a href="#app/mensagems">
								<i class="menu-icon fa fa-caret-right"></i>
								Mensagem
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="mensagemMotoristas" class="">
							<a href="#app/mensagemMotoristas">
								<i class="menu-icon fa fa-caret-right"></i>
								Mensagens
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="motoristaBloqueados" class="">
							<a href="#app/motoristaBloqueados">
								<i class="menu-icon fa fa-caret-right"></i>
								Motorista bloqueado
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="motoristaHasPlantaos" class="">
							<a href="#app/motoristaHasPlantaos">
								<i class="menu-icon fa fa-caret-right"></i>
								Motorista has plantao
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="plantaos" class="">
							<a href="#app/plantaos">
								<i class="menu-icon fa fa-caret-right"></i>
								Plantao
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="pontosAreas" class="">
							<a href="#app/pontosAreas">
								<i class="menu-icon fa fa-caret-right"></i>
								Pontos area
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="punicaos" class="">
							<a href="#app/punicaos">
								<i class="menu-icon fa fa-caret-right"></i>
								Punicao
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="socios" class="">
							<a href="#app/socios">
								<i class="menu-icon fa fa-caret-right"></i>
								Sócio
							</a>
							<b class="arrow"></b>
						</li> -->
						<!-- <li id="roles" class="">
							<a href="#app/roles">
								<i class="menu-icon fa fa-caret-right"></i>
								Papel
							</a>
							<b class="arrow"></b>
						</li>
						<li id="users" class="">
							<a href="#app/users">
								<i class="menu-icon fa fa-caret-right"></i>
								Usuário
							</a>
							<b class="arrow"></b>
						</li> -->
					</ul>
				</li>
				</security:authorize>
				<security:authorize access="hasAnyRole('ROLE_CORRIDA', 'ROLE_ADMIN')">
				<li class="">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fa fa-motorcycle"></i>
						<span class="menu-text"> Corrida </span>
						<b class="arrow fa fa-angle-down"></b>
					</a>
					<b class="arrow"></b>
					<ul class="submenu">
						<security:authorize access="hasRole('ROLE_CORRIDA') AND hasRole('ROLE_CORRIDA_CRIAR') OR hasRole('ROLE_ADMIN')">
						<li id="solicitarCorridas" class="">
							<a href="#app/newCorrida">
								<i class="menu-icon fa fa-caret-right"></i>
								Solicitar
							</a>
							<b class="arrow"></b>
						</li>
						</security:authorize>
						<security:authorize access="hasRole('ROLE_CORRIDA') OR hasRole('ROLE_ADMIN')">
						<li id="consultarCcorridas" class="">
							<a href="#app/corridas">
								<i class="menu-icon fa fa-caret-right"></i>
								Consulta
							</a>
							<b class="arrow"></b>
						</li>
						</security:authorize>
					</ul>
				</li>
				</security:authorize>
				<li class="">
				<security:authorize access="hasRole('ROLE_MENSAGEM') OR hasRole('ROLE_ADMIN')">
				<li id="mensagem" class="">
					<a href="#app/newMensagemMotorista">
						<i class="menu-icon fa fa-comments-o"></i>
						Mensagem
					</a>
					<b class="arrow"></b>
				</li>
				</security:authorize>
				</li>
				<c:if test="${authenticated}">
					<security:authorize access="hasAnyRole('ROLE_ADMIN')">
						<li class="">
						<li class="main-submenu">
							<a href="#" class="dropdown-toggle">
								<i class="menu-icon fa fa fa-users"></i>
								<span class="menu-text"> Controle de Acesso </span>
								<b class="arrow fa fa-angle-down"></b>
							</a>
							<b class="arrow"></b>
							<ul class="submenu primary-submenu">
								<li id="users" class="">
									<a href="#app/users">
										<i class="menu-icon fa fa-caret-right"></i>
										Usuário
									</a>
									<b class="arrow"></b>
								</li>
								<!-- <li id="roles" class="item-menu">
									<a href="#app/roles">
										<i class="menu-icon fa fa-caret-right"></i>
										Papel
									</a>
									<b class="arrow"></b>
								</li> -->
								<!-- <li id="permissions" class="item-menu">
									<a href="#app/permissions">
										<i class="menu-icon fa fa-caret-right"></i>
										Permissões
									</a>
									<b class="arrow"></b>
								</li>
								<li id="operations" class="item-menu">
									<a href="#app/operations">
										<i class="menu-icon fa fa-caret-right"></i>
										Operações
									</a>
									<b class="arrow"></b>
								</li>
								<li id="items" class="item-menu">
									<a href="#app/items">
										<i class="menu-icon fa fa-caret-right"></i>
										Itens
									</a>
									<b class="arrow"></b>
								</li>
								<li id="itemTypes" class="item-menu">
									<a href="#app/itemTypes">
										<i class="menu-icon fa fa-caret-right"></i>
										Tipos de Itens
									</a>
									<b class="arrow"></b>
								</li> -->
							</ul>
						</li>
					</security:authorize>
				</c:if>
			</ul>
			<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
				<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
			</div>
		</div>
		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="#">Home</a>
						</li>
						<li class="active">Cadastrar Pessoa</li>
					</ul>
				</div>
				<div class="page-content">
					<div class="page-header">
						<h1>Home</h1>
					</div>
					<div class="main-principal">
						<!-- AQUI DENTRO SERÃ INSERIDO VIA ENGINE DE TEMPLATES OS FORMS E PAGES-->
					</div>
				</div>
			</div>
		</div>
		<div class="footer">
			<div class="footer-inner">
				<div class="footer-content">
					<span class="bigger-120">
						<span class="blue bolder">Handover</span>
						Taxi &copy; 2015-2016
					</span>
				</div>
			</div>
		</div>
		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
			<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
		</a>
	</div>
	<div id="toTop">
		<img src="images/backtop.png" style="width: 48px; display: inline;">
	</div>
	<script data-main="js/main" src="vendor/require/require.js"></script>
</body>
</html>