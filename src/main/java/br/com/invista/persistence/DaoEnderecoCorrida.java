package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.EnderecoCorrida;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterEnderecoCorrida;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.EnderecoCorrida;
/**
*  generated: 04/11/2016 11:29:26
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoEnderecoCorrida extends AccessibleHibernateDao<EnderecoCorrida> {
	private static final Logger LOGGER = Logger.getLogger(DaoEnderecoCorrida.class);

	public DaoEnderecoCorrida() {
		super(EnderecoCorrida.class);
	}

	@Override
	public Pagination<EnderecoCorrida> getAll(PaginationParams paginationParams) {
		FilterEnderecoCorrida filterEnderecoCorrida = (FilterEnderecoCorrida) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterEnderecoCorrida.getEndereco() != null) {
			searchCriteria.add(Restrictions.ilike("endereco", filterEnderecoCorrida.getEndereco(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("endereco", filterEnderecoCorrida.getEndereco(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoCorrida.getLogradouro() != null) {
			searchCriteria.add(Restrictions.ilike("logradouro", filterEnderecoCorrida.getLogradouro(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("logradouro", filterEnderecoCorrida.getLogradouro(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoCorrida.getNumero() != null) {
			searchCriteria.add(Restrictions.ilike("numero", filterEnderecoCorrida.getNumero(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("numero", filterEnderecoCorrida.getNumero(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoCorrida.getComplemento() != null) {
			searchCriteria.add(Restrictions.ilike("complemento", filterEnderecoCorrida.getComplemento(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("complemento", filterEnderecoCorrida.getComplemento(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoCorrida.getBairro() != null) {
			searchCriteria.add(Restrictions.ilike("bairro", filterEnderecoCorrida.getBairro(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("bairro", filterEnderecoCorrida.getBairro(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoCorrida.getUf() != null) {
			searchCriteria.add(Restrictions.ilike("uf", filterEnderecoCorrida.getUf(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("uf", filterEnderecoCorrida.getUf(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoCorrida.getCidade() != null) {
			searchCriteria.add(Restrictions.ilike("cidade", filterEnderecoCorrida.getCidade(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cidade", filterEnderecoCorrida.getCidade(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoCorrida.getCep() != null) {
			searchCriteria.add(Restrictions.ilike("cep", filterEnderecoCorrida.getCep(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cep", filterEnderecoCorrida.getCep(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoCorrida.getReferencia() != null) {
			searchCriteria.add(Restrictions.ilike("referencia", filterEnderecoCorrida.getReferencia(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("referencia", filterEnderecoCorrida.getReferencia(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoCorrida.getDescricao() != null) {
			searchCriteria.add(Restrictions.ilike("descricao", filterEnderecoCorrida.getDescricao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("descricao", filterEnderecoCorrida.getDescricao(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoCorrida.getOrdem() != null) {
			searchCriteria.add(Restrictions.eq("ordem", filterEnderecoCorrida.getOrdem()));
			countCriteria.add(Restrictions.eq("ordem", filterEnderecoCorrida.getOrdem()));
		}				
		if (filterEnderecoCorrida.getLatitude() != null) {
			searchCriteria.add(Restrictions.eq("latitude", filterEnderecoCorrida.getLatitude()));
			countCriteria.add(Restrictions.eq("latitude", filterEnderecoCorrida.getLatitude()));
		}				
		if (filterEnderecoCorrida.getLongitude() != null) {
			searchCriteria.add(Restrictions.eq("longitude", filterEnderecoCorrida.getLongitude()));
			countCriteria.add(Restrictions.eq("longitude", filterEnderecoCorrida.getLongitude()));
		}				
		if (filterEnderecoCorrida.getEnderecoFormatado() != null) {
			searchCriteria.add(Restrictions.ilike("enderecoFormatado", filterEnderecoCorrida.getEnderecoFormatado(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("enderecoFormatado", filterEnderecoCorrida.getEnderecoFormatado(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoCorrida.getOrigem() != null) {
			searchCriteria.add(Restrictions.eq("origem", filterEnderecoCorrida.getOrigem()));
			countCriteria.add(Restrictions.eq("origem", filterEnderecoCorrida.getOrigem()));
		}				
		if (filterEnderecoCorrida.getCorrida() != null) {
			searchCriteria.createAlias("corrida", "corrida_");
			countCriteria.createAlias("corrida", "corrida_");
			searchCriteria.add(Restrictions.eq("corrida_.id", filterEnderecoCorrida.getCorrida()));
			countCriteria.add(Restrictions.eq("corrida_.id", filterEnderecoCorrida.getCorrida()));
		}

		return new Paginator<EnderecoCorrida>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<EnderecoCorrida> filter(PaginationParams paginationParams) {
		List<EnderecoCorrida> list = new ArrayList<EnderecoCorrida>();
		FilterEnderecoCorrida filterEnderecoCorrida = (FilterEnderecoCorrida) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterEnderecoCorrida.getEndereco() != null) {
			searchCriteria.add(Restrictions.eq("endereco", filterEnderecoCorrida.getEndereco()));
		}
		if (filterEnderecoCorrida.getLogradouro() != null) {
			searchCriteria.add(Restrictions.eq("logradouro", filterEnderecoCorrida.getLogradouro()));
		}
		if (filterEnderecoCorrida.getNumero() != null) {
			searchCriteria.add(Restrictions.eq("numero", filterEnderecoCorrida.getNumero()));
		}
		if (filterEnderecoCorrida.getComplemento() != null) {
			searchCriteria.add(Restrictions.eq("complemento", filterEnderecoCorrida.getComplemento()));
		}
		if (filterEnderecoCorrida.getBairro() != null) {
			searchCriteria.add(Restrictions.eq("bairro", filterEnderecoCorrida.getBairro()));
		}
		if (filterEnderecoCorrida.getUf() != null) {
			searchCriteria.add(Restrictions.eq("uf", filterEnderecoCorrida.getUf()));
		}
		if (filterEnderecoCorrida.getCidade() != null) {
			searchCriteria.add(Restrictions.eq("cidade", filterEnderecoCorrida.getCidade()));
		}
		if (filterEnderecoCorrida.getCep() != null) {
			searchCriteria.add(Restrictions.eq("cep", filterEnderecoCorrida.getCep()));
		}
		if (filterEnderecoCorrida.getReferencia() != null) {
			searchCriteria.add(Restrictions.eq("referencia", filterEnderecoCorrida.getReferencia()));
		}
		if (filterEnderecoCorrida.getDescricao() != null) {
			searchCriteria.add(Restrictions.eq("descricao", filterEnderecoCorrida.getDescricao()));
		}
		if (filterEnderecoCorrida.getOrdem() != null) {
			searchCriteria.add(Restrictions.eq("ordem", filterEnderecoCorrida.getOrdem()));
		}
		if (filterEnderecoCorrida.getLatitude() != null) {
			searchCriteria.add(Restrictions.eq("latitude", filterEnderecoCorrida.getLatitude()));
		}
		if (filterEnderecoCorrida.getLongitude() != null) {
			searchCriteria.add(Restrictions.eq("longitude", filterEnderecoCorrida.getLongitude()));
		}
		if (filterEnderecoCorrida.getEnderecoFormatado() != null) {
			searchCriteria.add(Restrictions.eq("enderecoFormatado", filterEnderecoCorrida.getEnderecoFormatado()));
		}
		if (filterEnderecoCorrida.getOrigem() != null) {
			searchCriteria.add(Restrictions.eq("origem", filterEnderecoCorrida.getOrigem()));
		}
		if (filterEnderecoCorrida.getCorrida() != null) {
			searchCriteria.createAlias("corrida", "corrida_");
			searchCriteria.add(Restrictions.eq("corrida_.id", filterEnderecoCorrida.getCorrida()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
