package br.com.invista.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDateTime;

import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.model.CentroCusto;
import br.com.invista.model.Corrida;
import br.com.invista.model.StatusCorrida;
import br.com.invista.model.filter.FilterCorrida;

/**
 * generated: 04/11/2016 11:29:26
 **/

@Named
@SuppressWarnings("rawtypes")
public class DaoCorrida extends AccessibleHibernateDao<Corrida> {
	private static final Logger LOGGER = Logger.getLogger(DaoCorrida.class);

	public DaoCorrida() {
		super(Corrida.class);
	}

	public List<Corrida> historicoCorridasCliente(int idCliente, boolean empresa) {
		List<Corrida> result = null;
		try {
			Criteria buscaHist = criteria();

			if (empresa) {
				buscaHist.add(Restrictions.eq("empresa.id", idCliente));
			} else {
				buscaHist.add(Restrictions.eq("cliente.id", idCliente));
			}

			buscaHist.add(Restrictions.le("dataCorrida", LocalDateTime.now()));
			
			buscaHist.add(Restrictions.eq("statusCorrida", StatusCorrida.FINALIZADA));

			buscaHist.addOrder(Order.desc("dataCorrida")).setMaxResults(20);

			buscaHist.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

			// buscaHist.setProjection(Projections.distinct(Projections.property("id")));

			result = new ArrayList<Corrida>();
			result.addAll(buscaHist.list());

		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro de corridas", e);
		}
		return result;
	}

	public List<Corrida> corridasAgendadasCliente(int idCliente, boolean empresa) {
		List<Corrida> result = null;
		try {
			Criteria buscaAgend = criteria();

			if (empresa) {
				buscaAgend.add(Restrictions.eq("empresa.id", idCliente));
			} else {
				buscaAgend.add(Restrictions.eq("cliente.id", idCliente));
			}

			buscaAgend.add(Restrictions.gt("dataCorrida", LocalDateTime.now()));

			buscaAgend.addOrder(Order.asc("dataCorrida")).setMaxResults(20);
			
			buscaAgend.add(Restrictions.eq("statusCorrida", StatusCorrida.AGENDADA));

			buscaAgend.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

			// buscaAgend.setProjection(Projections.distinct(Projections.property("id")));

			result = new ArrayList<Corrida>();
			result.addAll(buscaAgend.list());

		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro de corridas", e);
		}
		return result;
	}
	
	public List<Corrida> corridasPendentes() {
		List<Corrida> result = null;
		try {
			Criteria busca = criteria();

			busca.add(
					Restrictions.or(
							Restrictions.eq("statusCorrida", StatusCorrida.PENDENTE), 
							Restrictions.eq("statusCorrida", StatusCorrida.REENVIADA),
							Restrictions.eq("statusCorrida", StatusCorrida.AGENDADA)
							)
					);
			
			busca.add(Restrictions.eq("filaCriada", false));
			
			//Pendentes nos últimos 15 minutos.
			// Produção
			//busca.add(Restrictions.sqlRestriction(" ((DATEDIFF(MINUTE, dateadd(hh, -3 ,getdate()), data_corrida) <= 15 and DATEDIFF(MINUTE, dateadd(hh, -3 ,getdate()), data_corrida) > 0) "
					//+ "or (DATEDIFF(MINUTE, dateadd(hh, -3 ,getdate()), data_corrida) >= -15 and DATEDIFF(MINUTE, dateadd(hh, -3 ,getdate()), data_corrida) < 0)) "));
			
			// Desenvolvimento
			busca.add(Restrictions.sqlRestriction(" ((DATEDIFF(MINUTE, getdate(), data_corrida) <= 15 and DATEDIFF(MINUTE, getdate(), data_corrida) > 0) "
					+ "or (DATEDIFF(MINUTE, getdate(), data_corrida) >= -15 and DATEDIFF(MINUTE, getdate(), data_corrida) < 0)) "));
			
			busca.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

			result = new ArrayList<Corrida>();
			result.addAll(busca.list());

		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro de corridas", e);
		}
		return result;
	}

	@Override
	public Pagination<Corrida> getAll(PaginationParams paginationParams) {
		FilterCorrida filterCorrida = (FilterCorrida) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();

		if (filterCorrida.getDataCorrida() != null) {
			searchCriteria.add(Restrictions.eq("dataCorrida", filterCorrida.getDataCorrida()));
			countCriteria.add(Restrictions.eq("dataCorrida", filterCorrida.getDataCorrida()));
		}
		if (filterCorrida.getTipoPagamento() != null) {
			searchCriteria.add(Restrictions.eq("tipoPagamento", filterCorrida.getTipoPagamento()));
			countCriteria.add(Restrictions.eq("tipoPagamento", filterCorrida.getTipoPagamento()));
		}
		if (filterCorrida.getValor() != null) {
			searchCriteria.add(Restrictions.eq("valor", filterCorrida.getValor()));
			countCriteria.add(Restrictions.eq("valor", filterCorrida.getValor()));
		}
		if (filterCorrida.getObservacao() != null) {
			searchCriteria.add(Restrictions.ilike("observacao", filterCorrida.getObservacao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("observacao", filterCorrida.getObservacao(), MatchMode.ANYWHERE));
		}
		if (filterCorrida.getStatusCorrida() != null) {

			searchCriteria.add(Restrictions.ilike("statusCorrida", filterCorrida.getStatusCorrida(), MatchMode.EXACT));
			countCriteria.add(Restrictions.ilike("statusCorrida", filterCorrida.getStatusCorrida(), MatchMode.EXACT));

		}
		if (filterCorrida.getSeqEmAtendimento() != null) {
			searchCriteria.add(Restrictions.eq("seqEmAtendimento", filterCorrida.getSeqEmAtendimento()));
			countCriteria.add(Restrictions.eq("seqEmAtendimento", filterCorrida.getSeqEmAtendimento()));
		}
		if (filterCorrida.getMotoGrande() != null) {
			searchCriteria.add(Restrictions.eq("motoGrande", filterCorrida.getMotoGrande()));
			countCriteria.add(Restrictions.eq("motoGrande", filterCorrida.getMotoGrande()));
		}
		if (filterCorrida.getBau() != null) {
			searchCriteria.add(Restrictions.eq("bau", filterCorrida.getBau()));
			countCriteria.add(Restrictions.eq("bau", filterCorrida.getBau()));
		}
		if (filterCorrida.getPadronizada() != null) {
			searchCriteria.add(Restrictions.eq("padronizada", filterCorrida.getPadronizada()));
			countCriteria.add(Restrictions.eq("padronizada", filterCorrida.getPadronizada()));
		}
		if (filterCorrida.getFilaCriada() != null) {
			searchCriteria.add(Restrictions.eq("filaCriada", filterCorrida.getFilaCriada()));
			countCriteria.add(Restrictions.eq("filaCriada", filterCorrida.getFilaCriada()));
		}
		if (filterCorrida.getAtendente() != null) {
			searchCriteria.createAlias("atendente", "atendente_");
			countCriteria.createAlias("atendente", "atendente_");
			searchCriteria.add(Restrictions.eq("atendente_.id", filterCorrida.getAtendente()));
			countCriteria.add(Restrictions.eq("atendente_.id", filterCorrida.getAtendente()));
		}
		if (filterCorrida.getTelefone() != null) {
			searchCriteria.add(Restrictions.eq("telefone", filterCorrida.getTelefone()));
			countCriteria.add(Restrictions.eq("telefone", filterCorrida.getTelefone()));
		}
		if (filterCorrida.getId() != null) {
			searchCriteria.add(Restrictions.eq("id", filterCorrida.getId()));
			countCriteria.add(Restrictions.eq("id", filterCorrida.getId()));
		}
		if (filterCorrida.getCliente() != null) {
			searchCriteria.createAlias("cliente", "cliente_");
			countCriteria.createAlias("cliente", "cliente_");
			searchCriteria.add(Restrictions.eq("cliente_.id", filterCorrida.getCliente()));
			countCriteria.add(Restrictions.eq("cliente_.id", filterCorrida.getCliente()));
		}
		if (filterCorrida.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			countCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterCorrida.getMotorista()));
			countCriteria.add(Restrictions.eq("motorista_.id", filterCorrida.getMotorista()));
		}
		if (filterCorrida.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			countCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterCorrida.getEmpresa()));
			countCriteria.add(Restrictions.eq("empresa_.id", filterCorrida.getEmpresa()));
		}
		if (filterCorrida.getDataCorrida() != null) {
			searchCriteria.add(Restrictions.eq("dataCorrida", filterCorrida.getDataCorrida()));
			countCriteria.add(Restrictions.eq("dataCorrida", filterCorrida.getDataCorrida()));
		}				

		return new Paginator<Corrida>(searchCriteria, countCriteria).paginate(paginationParams);
	}

	public List<Corrida> filter(PaginationParams paginationParams) {
		List<Corrida> list = new ArrayList<Corrida>();
		FilterCorrida filterCorrida = (FilterCorrida) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterCorrida.getDataCorrida() != null) {
			searchCriteria.add(Restrictions.eq("dataCorrida", filterCorrida.getDataCorrida()));
		}
		if (filterCorrida.getTelefone() != null) {
			searchCriteria.add(Restrictions.eq("telefone", filterCorrida.getTelefone()));
		}
		if (filterCorrida.getTipoPagamento() != null) {
			searchCriteria.add(Restrictions.eq("tipoPagamento", filterCorrida.getTipoPagamento()));
		}
		if (filterCorrida.getValor() != null) {
			searchCriteria.add(Restrictions.eq("valor", filterCorrida.getValor()));
		}
		if (filterCorrida.getObservacao() != null) {
			searchCriteria.add(Restrictions.eq("observacao", filterCorrida.getObservacao()));
		}
		if (filterCorrida.getStatusCorrida() != null) {
			searchCriteria.add(Restrictions.eq("statusCorrida", filterCorrida.getStatusCorrida()));
		}
		if (filterCorrida.getSeqEmAtendimento() != null) {
			searchCriteria.add(Restrictions.eq("seqEmAtendimento", filterCorrida.getSeqEmAtendimento()));
		}
		if (filterCorrida.getMotoGrande() != null) {
			searchCriteria.add(Restrictions.eq("motoGrande", filterCorrida.getMotoGrande()));
		}
		if (filterCorrida.getBau() != null) {
			searchCriteria.add(Restrictions.eq("bau", filterCorrida.getBau()));
		}
		if (filterCorrida.getPadronizada() != null) {
			searchCriteria.add(Restrictions.eq("padronizada", filterCorrida.getPadronizada()));
		}
		if (filterCorrida.getFilaCriada() != null) {
			searchCriteria.add(Restrictions.eq("filaCriada", filterCorrida.getFilaCriada()));
		}
		if (filterCorrida.getAtendente() != null) {
			searchCriteria.createAlias("atendente", "atendente_");
			searchCriteria.add(Restrictions.eq("atendente_.id", filterCorrida.getAtendente()));
		}
		if (filterCorrida.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterCorrida.getMotorista()));
		}

		if (filterCorrida.getCliente() != null) {
			searchCriteria.createAlias("cliente", "cliente_");
			searchCriteria.add(Restrictions.eq("cliente_.id", filterCorrida.getCliente()));
		}

		if (filterCorrida.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterCorrida.getEmpresa()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}

}
