package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.TarifaAdicional;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterTarifaAdicional;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.TarifaAdicional;
/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoTarifaAdicional extends AccessibleHibernateDao<TarifaAdicional> {
	private static final Logger LOGGER = Logger.getLogger(DaoTarifaAdicional.class);

	public DaoTarifaAdicional() {
		super(TarifaAdicional.class);
	}

	@Override
	public Pagination<TarifaAdicional> getAll(PaginationParams paginationParams) {
		FilterTarifaAdicional filterTarifaAdicional = (FilterTarifaAdicional) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterTarifaAdicional.getDescricao() != null) {
			searchCriteria.add(Restrictions.ilike("descricao", filterTarifaAdicional.getDescricao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("descricao", filterTarifaAdicional.getDescricao(), MatchMode.ANYWHERE));
		}
		if (filterTarifaAdicional.getValor() != null) {
			searchCriteria.add(Restrictions.eq("valor", filterTarifaAdicional.getValor()));
			countCriteria.add(Restrictions.eq("valor", filterTarifaAdicional.getValor()));
		}				

		return new Paginator<TarifaAdicional>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<TarifaAdicional> filter(PaginationParams paginationParams) {
		List<TarifaAdicional> list = new ArrayList<TarifaAdicional>();
		FilterTarifaAdicional filterTarifaAdicional = (FilterTarifaAdicional) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterTarifaAdicional.getDescricao() != null) {
			searchCriteria.add(Restrictions.eq("descricao", filterTarifaAdicional.getDescricao()));
		}
		if (filterTarifaAdicional.getValor() != null) {
			searchCriteria.add(Restrictions.eq("valor", filterTarifaAdicional.getValor()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
