package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.PontosArea;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterPontosArea;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.PontosArea;
/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoPontosArea extends AccessibleHibernateDao<PontosArea> {
	private static final Logger LOGGER = Logger.getLogger(DaoPontosArea.class);

	public DaoPontosArea() {
		super(PontosArea.class);
	}

	@Override
	public Pagination<PontosArea> getAll(PaginationParams paginationParams) {
		FilterPontosArea filterPontosArea = (FilterPontosArea) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterPontosArea.getLatitude() != null) {
			searchCriteria.add(Restrictions.eq("latitude", filterPontosArea.getLatitude()));
			countCriteria.add(Restrictions.eq("latitude", filterPontosArea.getLatitude()));
		}				
		if (filterPontosArea.getLongitude() != null) {
			searchCriteria.add(Restrictions.eq("longitude", filterPontosArea.getLongitude()));
			countCriteria.add(Restrictions.eq("longitude", filterPontosArea.getLongitude()));
		}				
		if (filterPontosArea.getArea() != null) {
			searchCriteria.createAlias("area", "area_");
			countCriteria.createAlias("area", "area_");
			searchCriteria.add(Restrictions.eq("area_.id", filterPontosArea.getArea()));
			countCriteria.add(Restrictions.eq("area_.id", filterPontosArea.getArea()));
		}

		return new Paginator<PontosArea>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<PontosArea> filter(PaginationParams paginationParams) {
		List<PontosArea> list = new ArrayList<PontosArea>();
		FilterPontosArea filterPontosArea = (FilterPontosArea) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterPontosArea.getLatitude() != null) {
			searchCriteria.add(Restrictions.eq("latitude", filterPontosArea.getLatitude()));
		}
		if (filterPontosArea.getLongitude() != null) {
			searchCriteria.add(Restrictions.eq("longitude", filterPontosArea.getLongitude()));
		}
		if (filterPontosArea.getArea() != null) {
			searchCriteria.createAlias("area", "area_");
			searchCriteria.add(Restrictions.eq("area_.id", filterPontosArea.getArea()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
