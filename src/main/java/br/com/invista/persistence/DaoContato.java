package br.com.invista.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;
import br.com.invista.model.Contato;
import br.com.invista.model.filter.FilterContato;

/**
 * generated: 16/10/2016 15:27:08
 **/

@Named
@SuppressWarnings("rawtypes")
public class DaoContato extends AccessibleHibernateDao<Contato> {
	private static final Logger LOGGER = Logger.getLogger(DaoContato.class);

	public DaoContato() {
		super(Contato.class);
	}

	@Override
	public Pagination<Contato> getAll(PaginationParams paginationParams) {
		FilterContato filterContato = (FilterContato) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterContato.getNome() != null) {
			searchCriteria.add(Restrictions.ilike("nome", filterContato.getNome(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("nome", filterContato.getNome(), MatchMode.ANYWHERE));
		}
		if (filterContato.getDdd() != null) {
			searchCriteria.add(Restrictions.ilike("ddd", filterContato.getDdd(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("ddd", filterContato.getDdd(), MatchMode.ANYWHERE));
		}
		if (filterContato.getFone() != null) {
			searchCriteria.add(Restrictions.ilike("fone", filterContato.getFone(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("fone", filterContato.getFone(), MatchMode.ANYWHERE));
		}
		if (filterContato.getSetor() != null) {
			searchCriteria.add(Restrictions.ilike("setor", filterContato.getSetor(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("setor", filterContato.getSetor(), MatchMode.ANYWHERE));
		}
		if (filterContato.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			countCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterContato.getEmpresa()));
			countCriteria.add(Restrictions.eq("empresa_.id", filterContato.getEmpresa()));
		}

		return new Paginator<Contato>(searchCriteria, countCriteria).paginate(paginationParams);
	}

	public List<Contato> filter(PaginationParams paginationParams) {
		List<Contato> list = new ArrayList<Contato>();
		FilterContato filterContato = (FilterContato) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterContato.getNome() != null) {
			searchCriteria.add(Restrictions.eq("nome", filterContato.getNome()));
		}
		if (filterContato.getDdd() != null) {
			searchCriteria.add(Restrictions.eq("ddd", filterContato.getDdd()));
		}
		if (filterContato.getFone() != null) {
			searchCriteria.add(Restrictions.eq("fone", filterContato.getFone()));
		}
		if (filterContato.getSetor() != null) {
			searchCriteria.add(Restrictions.eq("setor", filterContato.getSetor()));
		}
		if (filterContato.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterContato.getEmpresa()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}

	public Contato getPorTelefone(String telefone) {
		Contato contato = null;

		try {
			contato = (Contato) criteria().add(Restrictions.eq("fone", telefone)).setMaxResults(1).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro pelo telefone," + telefone, e);
		}
		return contato;
	}

	public Boolean isValid(Integer id, String telefone) {
		Integer id_ = null;
		String telefone_ = null;
		// vitoriano : refactor
		try {
			id_ = (Integer) criteria().add(Restrictions.eq("fone", telefone)).setProjection(Projections.property("id"))
					.setMaxResults(1).uniqueResult();
			telefone_ = (String) criteria().add(Restrictions.eq("fone", telefone))
					.setProjection(Projections.property("fone")).setMaxResults(1).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro pelo telefone," + telefone, e);
		}
		// vitoriano
		if(id_ != null && id != null){
			return telefone_ != null && id_.intValue() != id.intValue() ? true : false;
		}
		
		return telefone_ != null && id_ != null && id_ != id ? true : false;
	}

}
