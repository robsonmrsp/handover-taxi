package br.com.invista.persistence;

import javax.inject.Named;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.Cliente;
import br.com.invista.model.Empresa;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterEmpresa;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;
import br.com.invista.model.Empresa;

/**
 * generated: 16/10/2016 15:27:08
 **/

@Named
@SuppressWarnings("rawtypes")
public class DaoEmpresa extends AccessibleHibernateDao<Empresa> {
	private static final Logger LOGGER = Logger.getLogger(DaoEmpresa.class);

	public DaoEmpresa() {
		super(Empresa.class);
	}
	
	public Empresa findByUserSenha(String username, String senha) {
		Empresa empresa = null;
		Criteria buscaEmpresa = criteria();
		try {
			buscaEmpresa.add(Restrictions.eq("senha", senha));
			buscaEmpresa.createAlias("telefonesFavoritos", "telefone").
			add(Restrictions.eq("telefone.fone", username));
			empresa = (Empresa) buscaEmpresa.setMaxResults(1).uniqueResult();
			/*empresa = (Empresa) criteria().add(Restrictions.eq("username", username))
					.add(Restrictions.eq("senha", senha)).setMaxResults(1).uniqueResult();*/
		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro pelo username e senha," + username, e);
		}
		return empresa;
	}
	
	public Empresa findByUserSenhaTwo(String username, String senha) {
		Empresa empresa = null;
		try {
			String mySQuery = "select top 1 {emp.*}, {tel.*} "+
								"from empresa emp "+
								"inner join telefone_favorito tel "+
								"on (emp.id = tel.id_empresa) "+
								"where replace(replace(replace(replace(tel.fone, '(', ''), ')',''), '-', ''), ' ', '') = :pTelefone and "
								+ "emp.senha = :pSenha";
			SQLQuery myQuery = nativeQuery(mySQuery);
			myQuery.setReadOnly(true);
			myQuery.addEntity("emp", Empresa.class)
			.addJoin("tel","emp.telefonesFavoritos")
			.addEntity("emp", Empresa.class)
			.setParameter("pTelefone", username)
			.setParameter("pSenha", senha)
			.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			List<Empresa> empresas = myQuery.list();
			if (empresas.size()>0){
				empresa = empresas.get(0);
			}
		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro pelo username e senha," + username, e);
		}
		return empresa;
	}

	@Override
	public Pagination<Empresa> getAll(PaginationParams paginationParams) {
		FilterEmpresa filterEmpresa = (FilterEmpresa) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterEmpresa.getId() != null) {
			searchCriteria.add(Restrictions.eq("id", filterEmpresa.getId()));
			countCriteria.add(Restrictions.eq("id", filterEmpresa.getId()));
		}
		if (filterEmpresa.getNomeFantasia() != null) {
			searchCriteria.add(Restrictions.ilike("nomeFantasia", filterEmpresa.getNomeFantasia(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("nomeFantasia", filterEmpresa.getNomeFantasia(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getCnpj() != null) {
			searchCriteria.add(Restrictions.ilike("cnpj", filterEmpresa.getCnpj(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cnpj", filterEmpresa.getCnpj(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getContrato() != null) {
			searchCriteria.add(Restrictions.ilike("contrato", filterEmpresa.getContrato(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("contrato", filterEmpresa.getContrato(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getUsername() != null) {
			searchCriteria.add(Restrictions.ilike("username", filterEmpresa.getUsername(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("username", filterEmpresa.getUsername(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getSenha() != null) {
			searchCriteria.add(Restrictions.ilike("senha", filterEmpresa.getSenha(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("senha", filterEmpresa.getSenha(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getRazaoSocial() != null) {
			searchCriteria.add(Restrictions.ilike("razaoSocial", filterEmpresa.getRazaoSocial(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("razaoSocial", filterEmpresa.getRazaoSocial(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getEndereco() != null) {
			searchCriteria.add(Restrictions.ilike("endereco", filterEmpresa.getEndereco(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("endereco", filterEmpresa.getEndereco(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getNumeroEndereco() != null) {
			searchCriteria
					.add(Restrictions.ilike("numeroEndereco", filterEmpresa.getNumeroEndereco(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("numeroEndereco", filterEmpresa.getNumeroEndereco(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getComplemento() != null) {
			searchCriteria.add(Restrictions.ilike("complemento", filterEmpresa.getComplemento(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("complemento", filterEmpresa.getComplemento(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getUf() != null) {
			searchCriteria.add(Restrictions.ilike("uf", filterEmpresa.getUf(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("uf", filterEmpresa.getUf(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getCidade() != null) {
			searchCriteria.add(Restrictions.ilike("cidade", filterEmpresa.getCidade(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cidade", filterEmpresa.getCidade(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getBairro() != null) {
			searchCriteria.add(Restrictions.ilike("bairro", filterEmpresa.getBairro(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("bairro", filterEmpresa.getBairro(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getCep() != null) {
			searchCriteria.add(Restrictions.ilike("cep", filterEmpresa.getCep(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cep", filterEmpresa.getCep(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getReferenciaEndereco() != null) {
			searchCriteria.add(Restrictions.ilike("referenciaEndereco", filterEmpresa.getReferenciaEndereco(),
					MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("referenciaEndereco", filterEmpresa.getReferenciaEndereco(),
					MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getDdd() != null) {
			searchCriteria.add(Restrictions.ilike("ddd", filterEmpresa.getDdd(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("ddd", filterEmpresa.getDdd(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getFone() != null) {
			searchCriteria.add(Restrictions.ilike("fone", filterEmpresa.getFone(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("fone", filterEmpresa.getFone(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getUtilizaVoucher() != null) {
			searchCriteria.add(Restrictions.eq("utilizaVoucher", filterEmpresa.getUtilizaVoucher()));
			countCriteria.add(Restrictions.eq("utilizaVoucher", filterEmpresa.getUtilizaVoucher()));
		}
		if (filterEmpresa.getUtilizaEticket() != null) {
			searchCriteria.add(Restrictions.eq("utilizaEticket", filterEmpresa.getUtilizaEticket()));
			countCriteria.add(Restrictions.eq("utilizaEticket", filterEmpresa.getUtilizaEticket()));
		}

		if (filterEmpresa.getObservacaoTaxista() != null) {
			searchCriteria.add(
					Restrictions.ilike("observacaoTaxista", filterEmpresa.getObservacaoTaxista(), MatchMode.ANYWHERE));
			countCriteria.add(
					Restrictions.ilike("observacaoTaxista", filterEmpresa.getObservacaoTaxista(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getObservacaoCentral() != null) {
			searchCriteria.add(
					Restrictions.ilike("observacaoCentral", filterEmpresa.getObservacaoCentral(), MatchMode.ANYWHERE));
			countCriteria.add(
					Restrictions.ilike("observacaoCentral", filterEmpresa.getObservacaoCentral(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getObservacaoFinanceiro() != null) {
			searchCriteria.add(Restrictions.ilike("observacaoFinanceiro", filterEmpresa.getObservacaoFinanceiro(),
					MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("observacaoFinanceiro", filterEmpresa.getObservacaoFinanceiro(),
					MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getPercentualDesconto() != null) {
			searchCriteria.add(Restrictions.eq("percentualDesconto", filterEmpresa.getPercentualDesconto()));
			countCriteria.add(Restrictions.eq("percentualDesconto", filterEmpresa.getPercentualDesconto()));
		}
		if (filterEmpresa.getInscricaoMunicipal() != null) {
			searchCriteria.add(Restrictions.ilike("inscricaoMunicipal", filterEmpresa.getInscricaoMunicipal(),
					MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("inscricaoMunicipal", filterEmpresa.getInscricaoMunicipal(),
					MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getInscricaoEstadual() != null) {
			searchCriteria.add(
					Restrictions.ilike("inscricaoEstadual", filterEmpresa.getInscricaoEstadual(), MatchMode.ANYWHERE));
			countCriteria.add(
					Restrictions.ilike("inscricaoEstadual", filterEmpresa.getInscricaoEstadual(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getEmiteNf() != null) {
			searchCriteria.add(Restrictions.eq("emiteNf", filterEmpresa.getEmiteNf()));
			countCriteria.add(Restrictions.eq("emiteNf", filterEmpresa.getEmiteNf()));
		}
		if (filterEmpresa.getPercentualIss() != null) {
			searchCriteria.add(Restrictions.eq("percentualIss", filterEmpresa.getPercentualIss()));
			countCriteria.add(Restrictions.eq("percentualIss", filterEmpresa.getPercentualIss()));
		}
		if (filterEmpresa.getPercentualIrf() != null) {
			searchCriteria.add(Restrictions.eq("percentualIrf", filterEmpresa.getPercentualIrf()));
			countCriteria.add(Restrictions.eq("percentualIrf", filterEmpresa.getPercentualIrf()));
		}
		if (filterEmpresa.getPercentualInss() != null) {
			searchCriteria.add(Restrictions.eq("percentualInss", filterEmpresa.getPercentualInss()));
			countCriteria.add(Restrictions.eq("percentualInss", filterEmpresa.getPercentualInss()));
		}
		if (filterEmpresa.getDiaVencimento() != null) {
			searchCriteria.add(Restrictions.eq("diaVencimento", filterEmpresa.getDiaVencimento()));
			countCriteria.add(Restrictions.eq("diaVencimento", filterEmpresa.getDiaVencimento()));
		}
		if (filterEmpresa.getPercentualMotorista() != null) {
			searchCriteria.add(Restrictions.eq("percentualMotorista", filterEmpresa.getPercentualMotorista()));
			countCriteria.add(Restrictions.eq("percentualMotorista", filterEmpresa.getPercentualMotorista()));
		}
		if (filterEmpresa.getBanco() != null) {
			searchCriteria.add(Restrictions.ilike("banco", filterEmpresa.getBanco(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("banco", filterEmpresa.getBanco(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getAgencia() != null) {
			searchCriteria.add(Restrictions.ilike("agencia", filterEmpresa.getAgencia(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("agencia", filterEmpresa.getAgencia(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getConta() != null) {
			searchCriteria.add(Restrictions.ilike("conta", filterEmpresa.getConta(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("conta", filterEmpresa.getConta(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getEmail() != null) {
			searchCriteria.add(Restrictions.ilike("email", filterEmpresa.getEmail(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("email", filterEmpresa.getEmail(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getUsuarioCadastro() != null) {
			searchCriteria.add(Restrictions.eq("usuarioCadastro", filterEmpresa.getUsuarioCadastro()));
			countCriteria.add(Restrictions.eq("usuarioCadastro", filterEmpresa.getUsuarioCadastro()));
		}
		if (filterEmpresa.getDataCadastro() != null) {
			searchCriteria.add(Restrictions.eq("dataCadastro", filterEmpresa.getDataCadastro()));
			countCriteria.add(Restrictions.eq("dataCadastro", filterEmpresa.getDataCadastro()));
		}
		if (filterEmpresa.getStatusEmpresa() != null) {
			searchCriteria
					.add(Restrictions.ilike("statusEmpresa", filterEmpresa.getStatusEmpresa(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("statusEmpresa", filterEmpresa.getStatusEmpresa(), MatchMode.ANYWHERE));
		}

		if (filterEmpresa.getNomeCliente1() != null) {
			searchCriteria.add(Restrictions.ilike("nomeCliente1", filterEmpresa.getNomeCliente1(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("nomeCliente1", filterEmpresa.getNomeCliente1(), MatchMode.ANYWHERE));
		}

		if (filterEmpresa.getFoneCliente1() != null) {
			searchCriteria.add(Restrictions.ilike("foneCliente1", filterEmpresa.getFoneCliente1(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("foneCliente1", filterEmpresa.getFoneCliente1(), MatchMode.ANYWHERE));
		}

		if (filterEmpresa.getNomeCliente2() != null) {
			searchCriteria.add(Restrictions.ilike("nomeCliente2", filterEmpresa.getNomeCliente2(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("nomeCliente2", filterEmpresa.getNomeCliente2(), MatchMode.ANYWHERE));
		}

		if (filterEmpresa.getFoneCliente2() != null) {
			searchCriteria.add(Restrictions.ilike("foneCliente2", filterEmpresa.getFoneCliente2(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("foneCliente2", filterEmpresa.getFoneCliente2(), MatchMode.ANYWHERE));
		}

		if (filterEmpresa.getNomeFornecedor1() != null) {
			searchCriteria
					.add(Restrictions.ilike("nomeFornecedor1", filterEmpresa.getNomeFornecedor1(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("nomeFornecedor1", filterEmpresa.getNomeFornecedor1(), MatchMode.ANYWHERE));
		}

		if (filterEmpresa.getFoneFornecedor1() != null) {
			searchCriteria
					.add(Restrictions.ilike("foneFornecedor1", filterEmpresa.getFoneFornecedor1(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("foneFornecedor1", filterEmpresa.getFoneFornecedor1(), MatchMode.ANYWHERE));
		}

		if (filterEmpresa.getNomeFornecedor2() != null) {
			searchCriteria
					.add(Restrictions.ilike("nomeFornecedor2", filterEmpresa.getNomeFornecedor2(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("nomeFornecedor2", filterEmpresa.getNomeFornecedor2(), MatchMode.ANYWHERE));
		}

		if (filterEmpresa.getFoneFornecedor2() != null) {
			searchCriteria
					.add(Restrictions.ilike("foneFornecedor2", filterEmpresa.getFoneFornecedor2(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("foneFornecedor2", filterEmpresa.getFoneFornecedor2(), MatchMode.ANYWHERE));
		}

		return new Paginator<Empresa>(searchCriteria, countCriteria).paginate(paginationParams);
	}

	public List<Empresa> filter(PaginationParams paginationParams) {
		List<Empresa> list = new ArrayList<Empresa>();
		FilterEmpresa filterEmpresa = (FilterEmpresa) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterEmpresa.getId() != null) {
			searchCriteria.add(Restrictions.eq("id", filterEmpresa.getId()));
		}
		if (filterEmpresa.getNomeFantasia() != null) {
			searchCriteria.add(Restrictions.eq("nomeFantasia", filterEmpresa.getNomeFantasia()));
		}
		if (filterEmpresa.getCnpj() != null) {
			searchCriteria.add(Restrictions.eq("cnpj", filterEmpresa.getCnpj()));
		}
		if (filterEmpresa.getContrato() != null) {
			searchCriteria.add(Restrictions.eq("contrato", filterEmpresa.getContrato()));
		}
		if (filterEmpresa.getRazaoSocial() != null) {
			searchCriteria.add(Restrictions.eq("razaoSocial", filterEmpresa.getRazaoSocial()));
		}
		if (filterEmpresa.getEndereco() != null) {
			searchCriteria.add(Restrictions.eq("endereco", filterEmpresa.getEndereco()));
		}
		if (filterEmpresa.getNumeroEndereco() != null) {
			searchCriteria.add(Restrictions.eq("numeroEndereco", filterEmpresa.getNumeroEndereco()));
		}
		if (filterEmpresa.getComplemento() != null) {
			searchCriteria.add(Restrictions.eq("complemento", filterEmpresa.getComplemento()));
		}
		if (filterEmpresa.getUf() != null) {
			searchCriteria.add(Restrictions.eq("uf", filterEmpresa.getUf()));
		}
		if (filterEmpresa.getUsername() != null) {
			searchCriteria.add(Restrictions.eq("username", filterEmpresa.getUsername()));
		}
		if (filterEmpresa.getSenha() != null) {
			searchCriteria.add(Restrictions.eq("senha", filterEmpresa.getSenha()));
		}
		if (filterEmpresa.getCidade() != null) {
			searchCriteria.add(Restrictions.eq("cidade", filterEmpresa.getCidade()));
		}
		if (filterEmpresa.getBairro() != null) {
			searchCriteria.add(Restrictions.eq("bairro", filterEmpresa.getBairro()));
		}
		if (filterEmpresa.getCep() != null) {
			searchCriteria.add(Restrictions.eq("cep", filterEmpresa.getCep()));
		}
		if (filterEmpresa.getReferenciaEndereco() != null) {
			searchCriteria.add(Restrictions.eq("referenciaEndereco", filterEmpresa.getReferenciaEndereco()));
		}
		if (filterEmpresa.getDdd() != null) {
			searchCriteria.add(Restrictions.eq("ddd", filterEmpresa.getDdd()));
		}
		if (filterEmpresa.getFone() != null) {
			searchCriteria.add(Restrictions.eq("fone", filterEmpresa.getFone()));
		}
		if (filterEmpresa.getUtilizaVoucher() != null) {
			searchCriteria.add(Restrictions.eq("utilizaVoucher", filterEmpresa.getUtilizaVoucher()));
		}
		if (filterEmpresa.getUtilizaEticket() != null) {
			searchCriteria.add(Restrictions.eq("utilizaEticket", filterEmpresa.getUtilizaEticket()));
		}
		if (filterEmpresa.getObservacaoTaxista() != null) {
			searchCriteria.add(Restrictions.eq("observacaoTaxista", filterEmpresa.getObservacaoTaxista()));
		}
		if (filterEmpresa.getObservacaoCentral() != null) {
			searchCriteria.add(Restrictions.eq("observacaoCentral", filterEmpresa.getObservacaoCentral()));
		}
		if (filterEmpresa.getObservacaoFinanceiro() != null) {
			searchCriteria.add(Restrictions.eq("observacaoFinanceiro", filterEmpresa.getObservacaoFinanceiro()));
		}
		if (filterEmpresa.getPercentualDesconto() != null) {
			searchCriteria.add(Restrictions.eq("percentualDesconto", filterEmpresa.getPercentualDesconto()));
		}
		if (filterEmpresa.getInscricaoMunicipal() != null) {
			searchCriteria.add(Restrictions.eq("inscricaoMunicipal", filterEmpresa.getInscricaoMunicipal()));
		}
		if (filterEmpresa.getInscricaoEstadual() != null) {
			searchCriteria.add(Restrictions.eq("inscricaoEstadual", filterEmpresa.getInscricaoEstadual()));
		}
		if (filterEmpresa.getEmiteNf() != null) {
			searchCriteria.add(Restrictions.eq("emiteNf", filterEmpresa.getEmiteNf()));
		}
		if (filterEmpresa.getPercentualIss() != null) {
			searchCriteria.add(Restrictions.eq("percentualIss", filterEmpresa.getPercentualIss()));
		}
		if (filterEmpresa.getPercentualIrf() != null) {
			searchCriteria.add(Restrictions.eq("percentualIrf", filterEmpresa.getPercentualIrf()));
		}
		if (filterEmpresa.getPercentualInss() != null) {
			searchCriteria.add(Restrictions.eq("percentualInss", filterEmpresa.getPercentualInss()));
		}
		if (filterEmpresa.getDiaVencimento() != null) {
			searchCriteria.add(Restrictions.eq("diaVencimento", filterEmpresa.getDiaVencimento()));
		}
		if (filterEmpresa.getPercentualMotorista() != null) {
			searchCriteria.add(Restrictions.eq("percentualMotorista", filterEmpresa.getPercentualMotorista()));
		}
		if (filterEmpresa.getBanco() != null) {
			searchCriteria.add(Restrictions.eq("banco", filterEmpresa.getBanco()));
		}
		if (filterEmpresa.getAgencia() != null) {
			searchCriteria.add(Restrictions.eq("agencia", filterEmpresa.getAgencia()));
		}
		if (filterEmpresa.getConta() != null) {
			searchCriteria.add(Restrictions.eq("conta", filterEmpresa.getConta()));
		}
		if (filterEmpresa.getEmail() != null) {
			searchCriteria.add(Restrictions.eq("email", filterEmpresa.getEmail()));
		}
		if (filterEmpresa.getUsuarioCadastro() != null) {
			searchCriteria.add(Restrictions.eq("usuarioCadastro", filterEmpresa.getUsuarioCadastro()));
		}
		if (filterEmpresa.getDataCadastro() != null) {
			searchCriteria.add(Restrictions.eq("dataCadastro", filterEmpresa.getDataCadastro()));
		}
		if (filterEmpresa.getStatusEmpresa() != null) {
			searchCriteria.add(Restrictions.eq("statusEmpresa", filterEmpresa.getStatusEmpresa()));
		}

		if (filterEmpresa.getNomeCliente1() != null) {
			searchCriteria.add(Restrictions.eq("nomeCliente1", filterEmpresa.getNomeCliente1()));
		}

		if (filterEmpresa.getFoneCliente1() != null) {
			searchCriteria.add(Restrictions.eq("foneCliente1", filterEmpresa.getFoneCliente1()));
		}

		if (filterEmpresa.getNomeCliente2() != null) {
			searchCriteria.add(Restrictions.eq("nomeCliente2", filterEmpresa.getNomeCliente2()));
		}

		if (filterEmpresa.getFoneCliente2() != null) {
			searchCriteria.add(Restrictions.eq("foneCliente2", filterEmpresa.getFoneCliente2()));
		}

		if (filterEmpresa.getNomeFornecedor1() != null) {
			searchCriteria.add(Restrictions.eq("nomeFornecedor1", filterEmpresa.getNomeFornecedor1()));
		}

		if (filterEmpresa.getFoneFornecedor1() != null) {
			searchCriteria.add(Restrictions.eq("foneFornecedor1", filterEmpresa.getFoneFornecedor1()));
		}

		if (filterEmpresa.getNomeFornecedor2() != null) {
			searchCriteria.add(Restrictions.eq("nomeFornecedor2", filterEmpresa.getNomeFornecedor2()));
		}

		if (filterEmpresa.getFoneFornecedor2() != null) {
			searchCriteria.add(Restrictions.eq("foneFornecedor2", filterEmpresa.getFoneFornecedor2()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
