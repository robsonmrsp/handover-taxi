package br.com.invista.persistence;

import javax.inject.Named;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.Cliente;
import br.com.invista.model.MotoristaBloqueado;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterMotoristaBloqueado;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;
import br.com.invista.model.MotoristaBloqueado;
/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoMotoristaBloqueado extends AccessibleHibernateDao<MotoristaBloqueado> {
	private static final Logger LOGGER = Logger.getLogger(DaoMotoristaBloqueado.class);

	public DaoMotoristaBloqueado() {
		super(MotoristaBloqueado.class);
	}


	@Override
	public Pagination<MotoristaBloqueado> getAll(PaginationParams paginationParams) {
		FilterMotoristaBloqueado filterMotoristaBloqueado = (FilterMotoristaBloqueado) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterMotoristaBloqueado.getDataBloqueio() != null) {
			searchCriteria.add(Restrictions.eq("dataBloqueio", filterMotoristaBloqueado.getDataBloqueio()));
			countCriteria.add(Restrictions.eq("dataBloqueio", filterMotoristaBloqueado.getDataBloqueio()));
		}				
		if (filterMotoristaBloqueado.getUsuarioBloqueio() != null) {
			searchCriteria.add(Restrictions.eq("usuarioBloqueio", filterMotoristaBloqueado.getUsuarioBloqueio()));
			countCriteria.add(Restrictions.eq("usuarioBloqueio", filterMotoristaBloqueado.getUsuarioBloqueio()));
		}				
		if (filterMotoristaBloqueado.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			countCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterMotoristaBloqueado.getEmpresa()));
			countCriteria.add(Restrictions.eq("empresa_.id", filterMotoristaBloqueado.getEmpresa()));
		}
		if (filterMotoristaBloqueado.getCliente() != null) {
			searchCriteria.createAlias("cliente", "cliente_");
			countCriteria.createAlias("cliente", "cliente_");
			searchCriteria.add(Restrictions.eq("cliente_.id", filterMotoristaBloqueado.getCliente()));
			countCriteria.add(Restrictions.eq("cliente_.id", filterMotoristaBloqueado.getCliente()));
		}
		if (filterMotoristaBloqueado.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			countCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterMotoristaBloqueado.getMotorista()));
			countCriteria.add(Restrictions.eq("motorista_.id", filterMotoristaBloqueado.getMotorista()));
		}

		return new Paginator<MotoristaBloqueado>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<MotoristaBloqueado> filter(PaginationParams paginationParams) {
		List<MotoristaBloqueado> list = new ArrayList<MotoristaBloqueado>();
		FilterMotoristaBloqueado filterMotoristaBloqueado = (FilterMotoristaBloqueado) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterMotoristaBloqueado.getDataBloqueio() != null) {
			searchCriteria.add(Restrictions.eq("dataBloqueio", filterMotoristaBloqueado.getDataBloqueio()));
		}
		if (filterMotoristaBloqueado.getUsuarioBloqueio() != null) {
			searchCriteria.add(Restrictions.eq("usuarioBloqueio", filterMotoristaBloqueado.getUsuarioBloqueio()));
		}
		if (filterMotoristaBloqueado.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterMotoristaBloqueado.getEmpresa()));
		}
		if (filterMotoristaBloqueado.getCliente() != null) {
			searchCriteria.createAlias("cliente", "cliente_");
			searchCriteria.add(Restrictions.eq("cliente_.id", filterMotoristaBloqueado.getCliente()));
		}
		if (filterMotoristaBloqueado.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterMotoristaBloqueado.getMotorista()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
