package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.Plantao;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterPlantao;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.Plantao;
/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoPlantao extends AccessibleHibernateDao<Plantao> {
	private static final Logger LOGGER = Logger.getLogger(DaoPlantao.class);

	public DaoPlantao() {
		super(Plantao.class);
	}

	@Override
	public Pagination<Plantao> getAll(PaginationParams paginationParams) {
		FilterPlantao filterPlantao = (FilterPlantao) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterPlantao.getDataInicio() != null) {
			searchCriteria.add(Restrictions.eq("dataInicio", filterPlantao.getDataInicio()));
			countCriteria.add(Restrictions.eq("dataInicio", filterPlantao.getDataInicio()));
		}				
		if (filterPlantao.getDataFim() != null) {
			searchCriteria.add(Restrictions.eq("dataFim", filterPlantao.getDataFim()));
			countCriteria.add(Restrictions.eq("dataFim", filterPlantao.getDataFim()));
		}				

		return new Paginator<Plantao>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<Plantao> filter(PaginationParams paginationParams) {
		List<Plantao> list = new ArrayList<Plantao>();
		FilterPlantao filterPlantao = (FilterPlantao) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterPlantao.getDataInicio() != null) {
			searchCriteria.add(Restrictions.eq("dataInicio", filterPlantao.getDataInicio()));
		}
		if (filterPlantao.getDataFim() != null) {
			searchCriteria.add(Restrictions.eq("dataFim", filterPlantao.getDataFim()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
