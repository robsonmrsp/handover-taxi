package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.VisitaTracker;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterVisitaTracker;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.VisitaTracker;
/**
*  generated: 04/11/2016 11:29:27
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoVisitaTracker extends AccessibleHibernateDao<VisitaTracker> {
	private static final Logger LOGGER = Logger.getLogger(DaoVisitaTracker.class);

	public DaoVisitaTracker() {
		super(VisitaTracker.class);
	}

	@Override
	public Pagination<VisitaTracker> getAll(PaginationParams paginationParams) {
		FilterVisitaTracker filterVisitaTracker = (FilterVisitaTracker) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterVisitaTracker.getLatitude() != null) {
			searchCriteria.add(Restrictions.eq("latitude", filterVisitaTracker.getLatitude()));
			countCriteria.add(Restrictions.eq("latitude", filterVisitaTracker.getLatitude()));
		}				
		if (filterVisitaTracker.getLongitude() != null) {
			searchCriteria.add(Restrictions.eq("longitude", filterVisitaTracker.getLongitude()));
			countCriteria.add(Restrictions.eq("longitude", filterVisitaTracker.getLongitude()));
		}				
		if (filterVisitaTracker.getTimestamp() != null) {
			searchCriteria.add(Restrictions.eq("timestamp", filterVisitaTracker.getTimestamp()));
			countCriteria.add(Restrictions.eq("timestamp", filterVisitaTracker.getTimestamp()));
		}				
		if (filterVisitaTracker.getSpeed() != null) {
			searchCriteria.add(Restrictions.eq("speed", filterVisitaTracker.getSpeed()));
			countCriteria.add(Restrictions.eq("speed", filterVisitaTracker.getSpeed()));
		}				
		if (filterVisitaTracker.getAccuracy() != null) {
			searchCriteria.add(Restrictions.eq("accuracy", filterVisitaTracker.getAccuracy()));
			countCriteria.add(Restrictions.eq("accuracy", filterVisitaTracker.getAccuracy()));
		}				
		if (filterVisitaTracker.getDirection() != null) {
			searchCriteria.add(Restrictions.eq("direction", filterVisitaTracker.getDirection()));
			countCriteria.add(Restrictions.eq("direction", filterVisitaTracker.getDirection()));
		}				
		if (filterVisitaTracker.getAltitude() != null) {
			searchCriteria.add(Restrictions.eq("altitude", filterVisitaTracker.getAltitude()));
			countCriteria.add(Restrictions.eq("altitude", filterVisitaTracker.getAltitude()));
		}				
		if (filterVisitaTracker.getBaterryLevel() != null) {
			searchCriteria.add(Restrictions.eq("baterryLevel", filterVisitaTracker.getBaterryLevel()));
			countCriteria.add(Restrictions.eq("baterryLevel", filterVisitaTracker.getBaterryLevel()));
		}				
		if (filterVisitaTracker.getGpsEnabled() != null) {
			searchCriteria.add(Restrictions.eq("gpsEnabled", filterVisitaTracker.getGpsEnabled()));
			countCriteria.add(Restrictions.eq("gpsEnabled", filterVisitaTracker.getGpsEnabled()));
		}				
		if (filterVisitaTracker.getWifiEnabled() != null) {
			searchCriteria.add(Restrictions.eq("wifiEnabled", filterVisitaTracker.getWifiEnabled()));
			countCriteria.add(Restrictions.eq("wifiEnabled", filterVisitaTracker.getWifiEnabled()));
		}				
		if (filterVisitaTracker.getMobileEnabled() != null) {
			searchCriteria.add(Restrictions.eq("mobileEnabled", filterVisitaTracker.getMobileEnabled()));
			countCriteria.add(Restrictions.eq("mobileEnabled", filterVisitaTracker.getMobileEnabled()));
		}				

		return new Paginator<VisitaTracker>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<VisitaTracker> filter(PaginationParams paginationParams) {
		List<VisitaTracker> list = new ArrayList<VisitaTracker>();
		FilterVisitaTracker filterVisitaTracker = (FilterVisitaTracker) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterVisitaTracker.getLatitude() != null) {
			searchCriteria.add(Restrictions.eq("latitude", filterVisitaTracker.getLatitude()));
		}
		if (filterVisitaTracker.getLongitude() != null) {
			searchCriteria.add(Restrictions.eq("longitude", filterVisitaTracker.getLongitude()));
		}
		if (filterVisitaTracker.getTimestamp() != null) {
			searchCriteria.add(Restrictions.eq("timestamp", filterVisitaTracker.getTimestamp()));
		}
		if (filterVisitaTracker.getSpeed() != null) {
			searchCriteria.add(Restrictions.eq("speed", filterVisitaTracker.getSpeed()));
		}
		if (filterVisitaTracker.getAccuracy() != null) {
			searchCriteria.add(Restrictions.eq("accuracy", filterVisitaTracker.getAccuracy()));
		}
		if (filterVisitaTracker.getDirection() != null) {
			searchCriteria.add(Restrictions.eq("direction", filterVisitaTracker.getDirection()));
		}
		if (filterVisitaTracker.getAltitude() != null) {
			searchCriteria.add(Restrictions.eq("altitude", filterVisitaTracker.getAltitude()));
		}
		if (filterVisitaTracker.getBaterryLevel() != null) {
			searchCriteria.add(Restrictions.eq("baterryLevel", filterVisitaTracker.getBaterryLevel()));
		}
		if (filterVisitaTracker.getGpsEnabled() != null) {
			searchCriteria.add(Restrictions.eq("gpsEnabled", filterVisitaTracker.getGpsEnabled()));
		}
		if (filterVisitaTracker.getWifiEnabled() != null) {
			searchCriteria.add(Restrictions.eq("wifiEnabled", filterVisitaTracker.getWifiEnabled()));
		}
		if (filterVisitaTracker.getMobileEnabled() != null) {
			searchCriteria.add(Restrictions.eq("mobileEnabled", filterVisitaTracker.getMobileEnabled()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
