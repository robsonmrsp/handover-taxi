package br.com.invista.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;
import br.com.invista.model.Cliente;
import br.com.invista.model.TipoCliente;
import br.com.invista.model.filter.FilterCliente;

/**
 * generated: 16/10/2016 15:27:07
 **/

@Named
@SuppressWarnings("rawtypes")
public class DaoCliente extends AccessibleHibernateDao<Cliente> {
	private static final Logger LOGGER = Logger.getLogger(DaoCliente.class);

	public DaoCliente() {
		super(Cliente.class);
	}

	public Cliente findByCpf(String cpf) {
		Cliente cliente = null;
		try {
			cliente = (Cliente) criteria().add(Restrictions.eq("cpf", cpf)).setMaxResults(1).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro pelo cpf," + cpf, e);
		}
		return cliente;
	}
	
	public Cliente findByEmail(String email) {
		Cliente cliente = null;
		try {
			cliente = (Cliente) criteria().add(Restrictions.eq("email", email)).setMaxResults(1).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro pelo email," + email, e);
		}
		return cliente;
	}

	public Cliente findByUserSenha(String username, String senha) {
		Cliente cliente = null;
		Criteria buscaCliente = criteria();
		try {
			buscaCliente.add(Restrictions.eq("senha", senha));
			buscaCliente.createAlias("telefonesFavoritos", "telefone").
			add(Restrictions.eq("telefone.fone", username));
			//add(Restrictions.sqlRestriction("replace(replace(replace(replace( {alias}.fone ,'(', ''),')',''),'-',''),' ','') like '"+username+"'"));
			cliente = (Cliente) buscaCliente.setMaxResults(1).uniqueResult();
			
			/*cliente = (Cliente) criteria().add(Restrictions.eq("username", username))
			.add(Restrictions.eq("senha", senha)).setMaxResults(1).uniqueResult();*/
		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro pelo username e senha," + username, e);
		}
		return cliente;
	}
	
	public Cliente findByUserSenhaTwo(String username, String senha) {
		Cliente cliente = null;
		try {
			String mySQuery = "select top 1 {cli.*}, {tel.*} "+
								"from cliente cli "+
								"left join telefone_favorito tel "+
								"on (cli.id = tel.id_cliente) "+
								"where replace(replace(replace(replace(cli.telefone, '(', ''), ')',''), '-', ''), ' ', '') = :pTelefone and "
								+ "cli.senha = :pSenha AND cli.inativo = 0";
			SQLQuery myQuery = nativeQuery(mySQuery);
			myQuery.setReadOnly(true);
			myQuery.addEntity("cli", Cliente.class)
			.addJoin("tel","cli.telefonesFavoritos")
			.addEntity("cli", Cliente.class)
			.setParameter("pTelefone", username)
			.setParameter("pSenha", senha)
			.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			List<Cliente> clientes = myQuery.list();
			if (clientes.size()>0){
				cliente = clientes.get(0);
			}
		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro pelo username e senha," + username, e);
		}
		return cliente;
	}

	public Cliente getFuncionarioPorTelefone(String telefone) {
		Cliente cliente = null;
		try {
			Criteria searchCriteria = criteria();
			cliente = (Cliente) searchCriteria.add(Restrictions.eq("telefone", telefone)).setMaxResults(1)
					.uniqueResult();

		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro pelo ctelefone," + telefone, e);
		}
		return cliente;
	}

	@Override
	public Pagination<Cliente> getAll(PaginationParams paginationParams) {
		FilterCliente filterCliente = (FilterCliente) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterCliente.getNome() != null) {
			searchCriteria.add(Restrictions.ilike("nome", filterCliente.getNome(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("nome", filterCliente.getNome(), MatchMode.ANYWHERE));
		}
		if (filterCliente.getTelefone() != null) {
			searchCriteria.add(Restrictions.ilike("telefone", filterCliente.getTelefone(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("telefone", filterCliente.getTelefone(), MatchMode.ANYWHERE));
		}
		if (filterCliente.getEmail() != null) {
			searchCriteria.add(Restrictions.ilike("email", filterCliente.getEmail(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("email", filterCliente.getEmail(), MatchMode.ANYWHERE));
		}
		if (filterCliente.getUsername() != null) {
			searchCriteria.add(Restrictions.ilike("username", filterCliente.getUsername(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("username", filterCliente.getUsername(), MatchMode.ANYWHERE));
		}
		if (filterCliente.getSenha() != null) {
			searchCriteria.add(Restrictions.ilike("senha", filterCliente.getSenha(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("senha", filterCliente.getSenha(), MatchMode.ANYWHERE));
		}
		if (filterCliente.getTipo() != null) {
			searchCriteria.add(Restrictions.eq("tipo", TipoCliente.byName(filterCliente.getTipo())));
			countCriteria.add(Restrictions.eq("tipo", TipoCliente.byName(filterCliente.getTipo())));
		}
		if (filterCliente.getInativo() != null) {
			searchCriteria.add(Restrictions.eq("inativo", filterCliente.getInativo()));
			countCriteria.add(Restrictions.eq("inativo", filterCliente.getInativo()));
		}
		if (filterCliente.getUsuarioCadastro() != null) {
			searchCriteria.add(Restrictions.eq("usuarioCadastro", filterCliente.getUsuarioCadastro()));
			countCriteria.add(Restrictions.eq("usuarioCadastro", filterCliente.getUsuarioCadastro()));
		}
		if (filterCliente.getDataCadastro() != null) {
			searchCriteria.add(Restrictions.eq("dataCadastro", filterCliente.getDataCadastro()));
			countCriteria.add(Restrictions.eq("dataCadastro", filterCliente.getDataCadastro()));
		}
		if (filterCliente.getMatricula() != null) {
			searchCriteria.add(Restrictions.eq("matricula", filterCliente.getMatricula()));
			countCriteria.add(Restrictions.eq("matricula", filterCliente.getMatricula()));
		}
		if (filterCliente.getLimiteMensal() != null) {
			searchCriteria.add(Restrictions.eq("limiteMensal", filterCliente.getLimiteMensal()));
			countCriteria.add(Restrictions.eq("limiteMensal", filterCliente.getLimiteMensal()));
		}
		if (filterCliente.getCnpjCpf() != null) {
			searchCriteria.add(Restrictions.ilike("cnpjCpf", filterCliente.getCnpjCpf(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cnpjCpf", filterCliente.getCnpjCpf(), MatchMode.ANYWHERE));
		}
		if (filterCliente.getCnpj() != null) {
			searchCriteria.add(Restrictions.ilike("cnpj", filterCliente.getCnpj(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cnpj", filterCliente.getCnpj(), MatchMode.ANYWHERE));
		}
		if (filterCliente.getCpf() != null) {
			searchCriteria.add(Restrictions.ilike("cpf", filterCliente.getCpf(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cpf", filterCliente.getCpf(), MatchMode.ANYWHERE));
		}
		if (filterCliente.getAutorizaEticket() != null) {
			searchCriteria.add(Restrictions.eq("autorizaEticket", filterCliente.getAutorizaEticket()));
			countCriteria.add(Restrictions.eq("autorizaEticket", filterCliente.getAutorizaEticket()));
		}

		if (filterCliente.getIdFacebook() != null) {
			searchCriteria.add(Restrictions.ilike("id_facebook", filterCliente.getIdFacebook(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("id_facebook", filterCliente.getIdFacebook(), MatchMode.ANYWHERE));
		}
		if (filterCliente.getEmailFacebook() != null) {
			searchCriteria.add(Restrictions.ilike("email_facebook", filterCliente.getEmailFacebook(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("email_facebook", filterCliente.getEmailFacebook(), MatchMode.ANYWHERE));
		}
		if (filterCliente.getObservacao() != null) {
			searchCriteria.add(Restrictions.ilike("observacao", filterCliente.getObservacao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("observacao", filterCliente.getObservacao(), MatchMode.ANYWHERE));
		}
		if (filterCliente.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			countCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterCliente.getEmpresa()));
			countCriteria.add(Restrictions.eq("empresa_.id", filterCliente.getEmpresa()));
		}
		if (filterCliente.getContrato() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			countCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.contrato", filterCliente.getContrato()));
			countCriteria.add(Restrictions.eq("empresa_.contrato", filterCliente.getContrato()));
		}
		if (filterCliente.getTelefonesFavoritos() != null) {
			searchCriteria.createAlias("telefonesFavoritos", "telefonesFavoritos_");
			countCriteria.createAlias("telefonesFavoritos", "telefonesFavoritos_");
			searchCriteria.add(Restrictions.eq("telefonesFavoritos_.fone", filterCliente.getTelefonesFavoritos()));
			countCriteria.add(Restrictions.eq("telefonesFavoritos_.fone", filterCliente.getTelefonesFavoritos()));
		}
		if (filterCliente.getCentroCusto() != null) {
			searchCriteria.createAlias("centroCusto", "centroCusto_");
			countCriteria.createAlias("centroCusto", "centroCusto_");
			searchCriteria.add(Restrictions.eq("centroCusto_.id", filterCliente.getCentroCusto()));
			countCriteria.add(Restrictions.eq("centroCusto_.id", filterCliente.getCentroCusto()));
		}

		return new Paginator<Cliente>(searchCriteria, countCriteria).paginate(paginationParams);
	}

	public List<Cliente> filter(FilterCliente filterCliente) {
		List<Cliente> list = new ArrayList<Cliente>();
		Criteria searchCriteria = criteria();
		if (filterCliente.getNome() != null) {
			searchCriteria.add(Restrictions.eq("nome", filterCliente.getNome()));
		}
		if (filterCliente.getTelefone() != null) {
			searchCriteria.add(Restrictions.eq("telefone", filterCliente.getTelefone()));
		}
		if (filterCliente.getEmail() != null) {
			searchCriteria.add(Restrictions.eq("email", filterCliente.getEmail()));
		}
		if (filterCliente.getSenha() != null) {
			searchCriteria.add(Restrictions.eq("senha", filterCliente.getSenha()));
		}
		if (filterCliente.getTipo() != null) {
			searchCriteria.add(Restrictions.eq("tipo", filterCliente.getTipo()));
		}
		if (filterCliente.getInativo() != null) {
			searchCriteria.add(Restrictions.eq("inativo", filterCliente.getInativo()));
		}
		if (filterCliente.getUsuarioCadastro() != null) {
			searchCriteria.add(Restrictions.eq("usuarioCadastro", filterCliente.getUsuarioCadastro()));
		}
		if (filterCliente.getDataCadastro() != null) {
			searchCriteria.add(Restrictions.eq("dataCadastro", filterCliente.getDataCadastro()));
		}
		if (filterCliente.getMatricula() != null) {
			searchCriteria.add(Restrictions.eq("matricula", filterCliente.getMatricula()));
		}
		if (filterCliente.getLimiteMensal() != null) {
			searchCriteria.add(Restrictions.eq("limiteMensal", filterCliente.getLimiteMensal()));
		}
		if (filterCliente.getCnpjCpf() != null) {
			searchCriteria.add(Restrictions.eq("cnpjCpf", filterCliente.getCnpjCpf()));
		}
		if (filterCliente.getCnpj() != null) {
			searchCriteria.add(Restrictions.eq("cnpj", filterCliente.getCnpj()));
		}
		if (filterCliente.getCpf() != null) {
			searchCriteria.add(Restrictions.eq("cpf", filterCliente.getCpf()));
		}
		if (filterCliente.getAutorizaEticket() != null) {
			searchCriteria.add(Restrictions.eq("autorizaEticket", filterCliente.getAutorizaEticket()));
		}
		if (filterCliente.getObservacao() != null) {
			searchCriteria.add(Restrictions.eq("observacao", filterCliente.getObservacao()));
		}
		if (filterCliente.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterCliente.getEmpresa()));
		}
		if (filterCliente.getContrato() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.contrato", filterCliente.getContrato()));
		}
		if (filterCliente.getTelefonesFavoritos() != null) {
			searchCriteria.createAlias("telefonesFavoritos", "telefonesFavoritos_");
			searchCriteria.add(Restrictions.eq("telefonesFavoritos_.fone", filterCliente.getTelefonesFavoritos()));
		}
		if (filterCliente.getCentroCusto() != null) {
			searchCriteria.createAlias("centroCusto", "centroCusto_");
			searchCriteria.add(Restrictions.eq("centroCusto_.id", filterCliente.getCentroCusto()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}

	public List<Cliente> filter(PaginationParams paginationParams) {
		List<Cliente> list = new ArrayList<Cliente>();
		FilterCliente filterCliente = (FilterCliente) paginationParams.getFilter();
		return filter(filterCliente);
	}

}
