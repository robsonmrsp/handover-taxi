package br.com.invista.persistence;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDateTime;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.model.Cliente;
import br.com.invista.model.ContaCorrente;
import br.com.invista.service.ClienteService;

/**
 * generated: 16/10/2016 15:27:07
 **/

@Named
@SuppressWarnings("rawtypes")
public class DaoContaCorrente extends AccessibleHibernateDao<ContaCorrente> {
	private static final Logger LOGGER = Logger.getLogger(DaoContaCorrente.class);

	@Inject
	ClienteService clienteService;
	
	public DaoContaCorrente() {
		super(ContaCorrente.class);
	}

	@Override
	public Pagination<ContaCorrente> getAll(PaginationParams paginationParams) {
		return null;
	}

	public List<ContaCorrente> filter(PaginationParams paginationParams) {
		return null;
	}

	public Double getSaldoPorFuncionario(Cliente cl)
	{
		LocalDateTime hoje = LocalDateTime.now();
		Integer diaVencimento = cl.getEmpresa().getDiaVencimento();
		LocalDateTime vencimento = LocalDateTime.now().withDayOfMonth(diaVencimento).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);

		if(hoje.getDayOfMonth() < diaVencimento)
			if(vencimento.getMonthOfYear() == 1)
				vencimento = vencimento.withMonthOfYear(12).withYear(vencimento.getYear()-1);
			else
				vencimento = vencimento.withMonthOfYear(vencimento.getMonthOfYear()-1);
		
		Double saldoConsumido = (double) 0;
		Cliente cliente = clienteService.get(cl.getId());
		Double limiteMensal = cliente.getLimiteMensal() == null ? (double) 0 : cliente.getLimiteMensal();
		List<ContaCorrente> extrato = new ArrayList<ContaCorrente>();
		Criteria buscaSaldo = criteria();
		buscaSaldo.add(Restrictions.eq("cliente.id", cl.getId()));
		buscaSaldo.add(Restrictions.between("dataCorrida", vencimento, hoje));
		extrato.addAll(buscaSaldo.list());
		
		for (ContaCorrente cc : extrato)
			saldoConsumido += cc.getValor();
		
		return limiteMensal - saldoConsumido;
	}
}
