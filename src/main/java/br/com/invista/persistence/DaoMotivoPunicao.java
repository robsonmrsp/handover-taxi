package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.MotivoPunicao;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterMotivoPunicao;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.MotivoPunicao;
/**
*  generated: 28/11/2016 23:13:05
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoMotivoPunicao extends AccessibleHibernateDao<MotivoPunicao> {
	private static final Logger LOGGER = Logger.getLogger(DaoMotivoPunicao.class);

	public DaoMotivoPunicao() {
		super(MotivoPunicao.class);
	}

	@Override
	public Pagination<MotivoPunicao> getAll(PaginationParams paginationParams) {
		FilterMotivoPunicao filterMotivoPunicao = (FilterMotivoPunicao) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterMotivoPunicao.getDescricao() != null) {
			searchCriteria.add(Restrictions.ilike("descricao", filterMotivoPunicao.getDescricao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("descricao", filterMotivoPunicao.getDescricao(), MatchMode.ANYWHERE));
		}

		return new Paginator<MotivoPunicao>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<MotivoPunicao> filter(PaginationParams paginationParams) {
		List<MotivoPunicao> list = new ArrayList<MotivoPunicao>();
		FilterMotivoPunicao filterMotivoPunicao = (FilterMotivoPunicao) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterMotivoPunicao.getDescricao() != null) {
			searchCriteria.add(Restrictions.eq("descricao", filterMotivoPunicao.getDescricao()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
