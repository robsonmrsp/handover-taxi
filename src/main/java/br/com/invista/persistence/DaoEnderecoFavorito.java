package br.com.invista.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;
import br.com.invista.model.EnderecoFavorito;
import br.com.invista.model.filter.FilterEnderecoFavorito;

/**
 * generated: 16/10/2016 15:27:07
 **/

@Named
@SuppressWarnings("rawtypes")
public class DaoEnderecoFavorito extends AccessibleHibernateDao<EnderecoFavorito> {
	private static final Logger LOGGER = Logger.getLogger(DaoEnderecoFavorito.class);

	public DaoEnderecoFavorito() {
		super(EnderecoFavorito.class);
	}

	public List<EnderecoFavorito> findByCliente(int idCliente, boolean empresa){
		List<EnderecoFavorito> resposta = null;
		try{
			Criteria searchCriteria = criteria();
			if (!empresa){
				searchCriteria.add(Restrictions.eq("cliente.id", idCliente));
			}else{
				searchCriteria.add(Restrictions.eq("empresa.id", idCliente));
			}
			
			searchCriteria.addOrder(Order.desc("id")).setMaxResults(4);
			
			resposta = new ArrayList<EnderecoFavorito>();
			resposta.addAll(searchCriteria.list());
		}catch(Exception e){
			LOGGER.error("Erro ao obter endereco favorito.", e);
		}
		return resposta;
	}
	
	@Override
	public Pagination<EnderecoFavorito> getAll(PaginationParams paginationParams) {
		FilterEnderecoFavorito filterEnderecoFavorito = (FilterEnderecoFavorito) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterEnderecoFavorito.getEndereco() != null) {
			searchCriteria.add(Restrictions.ilike("endereco", filterEnderecoFavorito.getEndereco(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("endereco", filterEnderecoFavorito.getEndereco(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoFavorito.getNumero() != null) {
			searchCriteria.add(Restrictions.ilike("numero", filterEnderecoFavorito.getNumero(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("numero", filterEnderecoFavorito.getNumero(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoFavorito.getComplemento() != null) {
			searchCriteria.add(Restrictions.ilike("complemento", filterEnderecoFavorito.getComplemento(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("complemento", filterEnderecoFavorito.getComplemento(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoFavorito.getUf() != null) {
			searchCriteria.add(Restrictions.ilike("uf", filterEnderecoFavorito.getUf(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("uf", filterEnderecoFavorito.getUf(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoFavorito.getCidade() != null) {
			searchCriteria.add(Restrictions.ilike("cidade", filterEnderecoFavorito.getCidade(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cidade", filterEnderecoFavorito.getCidade(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoFavorito.getBairro() != null) {
			searchCriteria.add(Restrictions.ilike("bairro", filterEnderecoFavorito.getBairro(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("bairro", filterEnderecoFavorito.getBairro(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoFavorito.getCep() != null) {
			searchCriteria.add(Restrictions.ilike("cep", filterEnderecoFavorito.getCep(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cep", filterEnderecoFavorito.getCep(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoFavorito.getReferencia() != null) {
			searchCriteria.add(Restrictions.ilike("referencia", filterEnderecoFavorito.getReferencia(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("referencia", filterEnderecoFavorito.getReferencia(), MatchMode.ANYWHERE));
		}
		if (filterEnderecoFavorito.getTipo() != null) {
			searchCriteria.add(Restrictions.ilike("tipo", filterEnderecoFavorito.getTipo(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("tipo", filterEnderecoFavorito.getTipo(), MatchMode.ANYWHERE));
		}

		if (filterEnderecoFavorito.getCliente() != null) {
			searchCriteria.createAlias("cliente", "cliente_");
			countCriteria.createAlias("cliente", "cliente_");
			searchCriteria.add(Restrictions.eq("cliente_.id", filterEnderecoFavorito.getCliente()));
			countCriteria.add(Restrictions.eq("cliente_.id", filterEnderecoFavorito.getCliente()));
		}

		if (filterEnderecoFavorito.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			countCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterEnderecoFavorito.getEmpresa()));
			countCriteria.add(Restrictions.eq("empresa_.id", filterEnderecoFavorito.getEmpresa()));
		}

		if (filterEnderecoFavorito.getLatitude() != null) {
			searchCriteria.add(Restrictions.eq("latitude", filterEnderecoFavorito.getLatitude()));
			countCriteria.add(Restrictions.eq("latitude", filterEnderecoFavorito.getLatitude()));
		}
		if (filterEnderecoFavorito.getLongitude() != null) {
			searchCriteria.add(Restrictions.eq("longitude", filterEnderecoFavorito.getLongitude()));
			countCriteria.add(Restrictions.eq("longitude", filterEnderecoFavorito.getLongitude()));
		}
		if (filterEnderecoFavorito.getEnderecoFormatado() != null) {
			searchCriteria.add(Restrictions.ilike("enderecoFormatado", filterEnderecoFavorito.getEnderecoFormatado(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("enderecoFormatado", filterEnderecoFavorito.getEnderecoFormatado(), MatchMode.ANYWHERE));
		}

		return new Paginator<EnderecoFavorito>(searchCriteria, countCriteria).paginate(paginationParams);
	}

	public List<EnderecoFavorito> filter(PaginationParams paginationParams) {
		List<EnderecoFavorito> list = new ArrayList<EnderecoFavorito>();
		FilterEnderecoFavorito filterEnderecoFavorito = (FilterEnderecoFavorito) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterEnderecoFavorito.getEndereco() != null) {
			searchCriteria.add(Restrictions.eq("endereco", filterEnderecoFavorito.getEndereco()));
		}
		if (filterEnderecoFavorito.getNumero() != null) {
			searchCriteria.add(Restrictions.eq("numero", filterEnderecoFavorito.getNumero()));
		}
		if (filterEnderecoFavorito.getComplemento() != null) {
			searchCriteria.add(Restrictions.eq("complemento", filterEnderecoFavorito.getComplemento()));
		}
		if (filterEnderecoFavorito.getUf() != null) {
			searchCriteria.add(Restrictions.eq("uf", filterEnderecoFavorito.getUf()));
		}
		if (filterEnderecoFavorito.getCidade() != null) {
			searchCriteria.add(Restrictions.eq("cidade", filterEnderecoFavorito.getCidade()));
		}
		if (filterEnderecoFavorito.getBairro() != null) {
			searchCriteria.add(Restrictions.eq("bairro", filterEnderecoFavorito.getBairro()));
		}
		if (filterEnderecoFavorito.getCep() != null) {
			searchCriteria.add(Restrictions.eq("cep", filterEnderecoFavorito.getCep()));
		}
		if (filterEnderecoFavorito.getReferencia() != null) {
			searchCriteria.add(Restrictions.eq("referencia", filterEnderecoFavorito.getReferencia()));
		}
		if (filterEnderecoFavorito.getTipo() != null) {
			searchCriteria.add(Restrictions.eq("tipo", filterEnderecoFavorito.getTipo()));
		}

		if (filterEnderecoFavorito.getCliente() != null) {
			searchCriteria.createAlias("cliente", "cliente_");
			searchCriteria.add(Restrictions.eq("cliente_.id", filterEnderecoFavorito.getCliente()));
		}

		if (filterEnderecoFavorito.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterEnderecoFavorito.getEmpresa()));
		}

		if (filterEnderecoFavorito.getLatitude() != null) {
			searchCriteria.add(Restrictions.eq("latitude", filterEnderecoFavorito.getLatitude()));
		}
		if (filterEnderecoFavorito.getLongitude() != null) {
			searchCriteria.add(Restrictions.eq("longitude", filterEnderecoFavorito.getLongitude()));
		}
		if (filterEnderecoFavorito.getEnderecoFormatado() != null) {
			searchCriteria.add(Restrictions.eq("enderecoFormatado", filterEnderecoFavorito.getEnderecoFormatado()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
