package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.ClienteEnderecoCoord;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterClienteEnderecoCoord;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.ClienteEnderecoCoord;
/**
*  generated: 16/10/2016 15:27:07
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoClienteEnderecoCoord extends AccessibleHibernateDao<ClienteEnderecoCoord> {
	private static final Logger LOGGER = Logger.getLogger(DaoClienteEnderecoCoord.class);

	public DaoClienteEnderecoCoord() {
		super(ClienteEnderecoCoord.class);
	}

	@Override
	public Pagination<ClienteEnderecoCoord> getAll(PaginationParams paginationParams) {
		FilterClienteEnderecoCoord filterClienteEnderecoCoord = (FilterClienteEnderecoCoord) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterClienteEnderecoCoord.getLatitude() != null) {
			searchCriteria.add(Restrictions.eq("latitude", filterClienteEnderecoCoord.getLatitude()));
			countCriteria.add(Restrictions.eq("latitude", filterClienteEnderecoCoord.getLatitude()));
		}				
		if (filterClienteEnderecoCoord.getLongitude() != null) {
			searchCriteria.add(Restrictions.eq("longitude", filterClienteEnderecoCoord.getLongitude()));
			countCriteria.add(Restrictions.eq("longitude", filterClienteEnderecoCoord.getLongitude()));
		}				
		if (filterClienteEnderecoCoord.getEnderecoFormatado() != null) {
			searchCriteria.add(Restrictions.ilike("enderecoFormatado", filterClienteEnderecoCoord.getEnderecoFormatado(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("enderecoFormatado", filterClienteEnderecoCoord.getEnderecoFormatado(), MatchMode.ANYWHERE));
		}
		if (filterClienteEnderecoCoord.getValido() != null) {
			searchCriteria.add(Restrictions.eq("valido", filterClienteEnderecoCoord.getValido()));
			countCriteria.add(Restrictions.eq("valido", filterClienteEnderecoCoord.getValido()));
		}				
		if (filterClienteEnderecoCoord.getClienteEndereco() != null) {
			searchCriteria.createAlias("clienteEndereco", "clienteEndereco_");
			countCriteria.createAlias("clienteEndereco", "clienteEndereco_");
			searchCriteria.add(Restrictions.eq("clienteEndereco_.id", filterClienteEnderecoCoord.getClienteEndereco()));
			countCriteria.add(Restrictions.eq("clienteEndereco_.id", filterClienteEnderecoCoord.getClienteEndereco()));
		}

		return new Paginator<ClienteEnderecoCoord>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<ClienteEnderecoCoord> filter(PaginationParams paginationParams) {
		List<ClienteEnderecoCoord> list = new ArrayList<ClienteEnderecoCoord>();
		FilterClienteEnderecoCoord filterClienteEnderecoCoord = (FilterClienteEnderecoCoord) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterClienteEnderecoCoord.getLatitude() != null) {
			searchCriteria.add(Restrictions.eq("latitude", filterClienteEnderecoCoord.getLatitude()));
		}
		if (filterClienteEnderecoCoord.getLongitude() != null) {
			searchCriteria.add(Restrictions.eq("longitude", filterClienteEnderecoCoord.getLongitude()));
		}
		if (filterClienteEnderecoCoord.getEnderecoFormatado() != null) {
			searchCriteria.add(Restrictions.eq("enderecoFormatado", filterClienteEnderecoCoord.getEnderecoFormatado()));
		}
		if (filterClienteEnderecoCoord.getValido() != null) {
			searchCriteria.add(Restrictions.eq("valido", filterClienteEnderecoCoord.getValido()));
		}
		if (filterClienteEnderecoCoord.getClienteEndereco() != null) {
			searchCriteria.createAlias("clienteEndereco", "clienteEndereco_");
			searchCriteria.add(Restrictions.eq("clienteEndereco_.id", filterClienteEnderecoCoord.getClienteEndereco()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
