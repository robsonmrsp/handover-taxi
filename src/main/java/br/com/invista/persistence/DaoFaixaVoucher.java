package br.com.invista.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;
import br.com.invista.model.FaixaVoucher;
import br.com.invista.model.filter.FilterFaixaVoucher;

/**
 * generated: 16/10/2016 15:27:08
 **/

@Named
@SuppressWarnings("rawtypes")
public class DaoFaixaVoucher extends AccessibleHibernateDao<FaixaVoucher> {
	private static final Logger LOGGER = Logger.getLogger(DaoFaixaVoucher.class);

	public DaoFaixaVoucher() {
		super(FaixaVoucher.class);
	}

	@Override
	public Pagination<FaixaVoucher> getAll(PaginationParams paginationParams) {
		FilterFaixaVoucher filterFaixaVoucher = (FilterFaixaVoucher) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterFaixaVoucher.getSerie() != null) {
			searchCriteria.add(Restrictions.ilike("serie", filterFaixaVoucher.getSerie(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("serie", filterFaixaVoucher.getSerie(), MatchMode.ANYWHERE));
		}
		if (filterFaixaVoucher.getNumeroInicial() != null) {
			searchCriteria.add(Restrictions.eq("numeroInicial", filterFaixaVoucher.getNumeroInicial()));
			countCriteria.add(Restrictions.eq("numeroInicial", filterFaixaVoucher.getNumeroInicial()));
		}
		if (filterFaixaVoucher.getNumeroFinal() != null) {
			searchCriteria.add(Restrictions.eq("numeroFinal", filterFaixaVoucher.getNumeroFinal()));
			countCriteria.add(Restrictions.eq("numeroFinal", filterFaixaVoucher.getNumeroFinal()));
		}
		if (filterFaixaVoucher.getInativo() != null) {
			searchCriteria.add(Restrictions.eq("inativo", filterFaixaVoucher.getInativo()));
			countCriteria.add(Restrictions.eq("inativo", filterFaixaVoucher.getInativo()));
		}
		if (filterFaixaVoucher.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			countCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterFaixaVoucher.getEmpresa()));
			countCriteria.add(Restrictions.eq("empresa_.id", filterFaixaVoucher.getEmpresa()));
		}
		if (filterFaixaVoucher.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			countCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterFaixaVoucher.getMotorista()));
			countCriteria.add(Restrictions.eq("motorista_.id", filterFaixaVoucher.getMotorista()));
		}

		return new Paginator<FaixaVoucher>(searchCriteria, countCriteria).paginate(paginationParams);
	}

	public List<FaixaVoucher> filter(PaginationParams paginationParams) {
		List<FaixaVoucher> list = new ArrayList<FaixaVoucher>();
		FilterFaixaVoucher filterFaixaVoucher = (FilterFaixaVoucher) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterFaixaVoucher.getSerie() != null) {
			searchCriteria.add(Restrictions.eq("serie", filterFaixaVoucher.getSerie()));
		}
		if (filterFaixaVoucher.getNumeroInicial() != null) {
			searchCriteria.add(Restrictions.eq("numeroInicial", filterFaixaVoucher.getNumeroInicial()));
		}
		if (filterFaixaVoucher.getNumeroFinal() != null) {
			searchCriteria.add(Restrictions.eq("numeroFinal", filterFaixaVoucher.getNumeroFinal()));
		}
		if (filterFaixaVoucher.getInativo() != null) {
			searchCriteria.add(Restrictions.eq("inativo", filterFaixaVoucher.getInativo()));
		}
		if (filterFaixaVoucher.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterFaixaVoucher.getEmpresa()));
		}
		if (filterFaixaVoucher.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterFaixaVoucher.getMotorista()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
	
	public List<FaixaVoucher> validaVoucher(FaixaVoucher voucher)
	{
		Criteria crt = criteria();
		
		if(voucher.getEmpresa() != null)
			crt.add(Restrictions.eq("empresa.id", voucher.getEmpresa().getId()));
		else if(voucher.getMotorista() != null)
			crt.add(Restrictions.eq("motorista.id", voucher.getMotorista().getId()));
		
		crt.add(Restrictions.neOrIsNotNull("id", voucher.getId()));

		Criterion in = Restrictions.or( 
			Restrictions.between("numeroInicial", voucher.getNumeroInicial(), voucher.getNumeroFinal()), 
			Restrictions.between("numeroFinal", voucher.getNumeroInicial(), voucher.getNumeroFinal())
		); 
		Criterion out = Restrictions.and( 
			Restrictions.lt("numeroInicial", voucher.getNumeroInicial()),
			Restrictions.gt("numeroFinal", voucher.getNumeroInicial()),
			Restrictions.lt("numeroInicial", voucher.getNumeroFinal()),
			Restrictions.gt("numeroFinal", voucher.getNumeroFinal())
		); 
		crt.add(Restrictions.or(in, out));
		return crt.list();
	}
}
