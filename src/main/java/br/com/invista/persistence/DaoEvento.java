package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.Evento;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterEvento;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.Evento;
/**
*  generated: 04/11/2016 11:29:26
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoEvento extends AccessibleHibernateDao<Evento> {
	private static final Logger LOGGER = Logger.getLogger(DaoEvento.class);

	public DaoEvento() {
		super(Evento.class);
	}

	@Override
	public Pagination<Evento> getAll(PaginationParams paginationParams) {
		FilterEvento filterEvento = (FilterEvento) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterEvento.getDescricao() != null) {
			searchCriteria.add(Restrictions.ilike("descricao", filterEvento.getDescricao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("descricao", filterEvento.getDescricao(), MatchMode.ANYWHERE));
		}
		if (filterEvento.getDataInicio() != null) {
			searchCriteria.add(Restrictions.eq("dataInicio", filterEvento.getDataInicio()));
			countCriteria.add(Restrictions.eq("dataInicio", filterEvento.getDataInicio()));
		}				
		if (filterEvento.getDataFim() != null) {
			searchCriteria.add(Restrictions.eq("dataFim", filterEvento.getDataFim()));
			countCriteria.add(Restrictions.eq("dataFim", filterEvento.getDataFim()));
		}				
		if (filterEvento.getDescricaoCurta() != null) {
			searchCriteria.add(Restrictions.ilike("descricaoCurta", filterEvento.getDescricaoCurta(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("descricaoCurta", filterEvento.getDescricaoCurta(), MatchMode.ANYWHERE));
		}
		if (filterEvento.getEnderecoCorrida() != null) {
			searchCriteria.createAlias("enderecoCorrida", "enderecoCorrida_");
			countCriteria.createAlias("enderecoCorrida", "enderecoCorrida_");
			searchCriteria.add(Restrictions.eq("enderecoCorrida_.id", filterEvento.getEnderecoCorrida()));
			countCriteria.add(Restrictions.eq("enderecoCorrida_.id", filterEvento.getEnderecoCorrida()));
		}
		if (filterEvento.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			countCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterEvento.getMotorista()));
			countCriteria.add(Restrictions.eq("motorista_.id", filterEvento.getMotorista()));
		}
		if (filterEvento.getVisitaTrackerSaida() != null) {
			searchCriteria.createAlias("visitaTrackerSaida", "visitaTrackerSaida_");
			countCriteria.createAlias("visitaTrackerSaida", "visitaTrackerSaida_");
			searchCriteria.add(Restrictions.eq("visitaTrackerSaida_.id", filterEvento.getVisitaTrackerSaida()));
			countCriteria.add(Restrictions.eq("visitaTrackerSaida_.id", filterEvento.getVisitaTrackerSaida()));
		}
		if (filterEvento.getVisitaTrackerChegada() != null) {
			searchCriteria.createAlias("visitaTrackerChegada", "visitaTrackerChegada_");
			countCriteria.createAlias("visitaTrackerChegada", "visitaTrackerChegada_");
			searchCriteria.add(Restrictions.eq("visitaTrackerChegada_.id", filterEvento.getVisitaTrackerChegada()));
			countCriteria.add(Restrictions.eq("visitaTrackerChegada_.id", filterEvento.getVisitaTrackerChegada()));
		}

		return new Paginator<Evento>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<Evento> filter(PaginationParams paginationParams) {
		List<Evento> list = new ArrayList<Evento>();
		FilterEvento filterEvento = (FilterEvento) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterEvento.getDescricao() != null) {
			searchCriteria.add(Restrictions.eq("descricao", filterEvento.getDescricao()));
		}
		if (filterEvento.getDataInicio() != null) {
			searchCriteria.add(Restrictions.eq("dataInicio", filterEvento.getDataInicio()));
		}
		if (filterEvento.getDataFim() != null) {
			searchCriteria.add(Restrictions.eq("dataFim", filterEvento.getDataFim()));
		}
		if (filterEvento.getDescricaoCurta() != null) {
			searchCriteria.add(Restrictions.eq("descricaoCurta", filterEvento.getDescricaoCurta()));
		}
		if (filterEvento.getEnderecoCorrida() != null) {
			searchCriteria.createAlias("enderecoCorrida", "enderecoCorrida_");
			searchCriteria.add(Restrictions.eq("enderecoCorrida_.id", filterEvento.getEnderecoCorrida()));
		}
		if (filterEvento.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterEvento.getMotorista()));
		}
		if (filterEvento.getVisitaTrackerSaida() != null) {
			searchCriteria.createAlias("visitaTrackerSaida", "visitaTrackerSaida_");
			searchCriteria.add(Restrictions.eq("visitaTrackerSaida_.id", filterEvento.getVisitaTrackerSaida()));
		}
		if (filterEvento.getVisitaTrackerChegada() != null) {
			searchCriteria.createAlias("visitaTrackerChegada", "visitaTrackerChegada_");
			searchCriteria.add(Restrictions.eq("visitaTrackerChegada_.id", filterEvento.getVisitaTrackerChegada()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
