package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.CorridaEndereco;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterCorridaEndereco;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.CorridaEndereco;

/**
 * generated: 16/10/2016 15:27:08
 **/

@Named
@SuppressWarnings("rawtypes")
public class DaoCorridaEndereco extends AccessibleHibernateDao<CorridaEndereco> {
	private static final Logger LOGGER = Logger.getLogger(DaoCorridaEndereco.class);

	public DaoCorridaEndereco() {
		super(CorridaEndereco.class);
	}

	@Override
	public Pagination<CorridaEndereco> getAll(PaginationParams paginationParams) {
		FilterCorridaEndereco filterCorridaEndereco = (FilterCorridaEndereco) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterCorridaEndereco.getEnderecoOrigem() != null) {
			searchCriteria.add(Restrictions.ilike("enderecoOrigem", filterCorridaEndereco.getEnderecoOrigem(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("enderecoOrigem", filterCorridaEndereco.getEnderecoOrigem(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getNumeroOrigem() != null) {
			searchCriteria.add(Restrictions.ilike("numeroOrigem", filterCorridaEndereco.getNumeroOrigem(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("numeroOrigem", filterCorridaEndereco.getNumeroOrigem(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getComplementoOrigem() != null) {
			searchCriteria.add(Restrictions.ilike("complementoOrigem", filterCorridaEndereco.getComplementoOrigem(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("complementoOrigem", filterCorridaEndereco.getComplementoOrigem(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getBairroOrigem() != null) {
			searchCriteria.add(Restrictions.ilike("bairroOrigem", filterCorridaEndereco.getBairroOrigem(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("bairroOrigem", filterCorridaEndereco.getBairroOrigem(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getUfOrigem() != null) {
			searchCriteria.add(Restrictions.ilike("ufOrigem", filterCorridaEndereco.getUfOrigem(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("ufOrigem", filterCorridaEndereco.getUfOrigem(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getCidadeOrigem() != null) {
			searchCriteria.add(Restrictions.ilike("cidadeOrigem", filterCorridaEndereco.getCidadeOrigem(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cidadeOrigem", filterCorridaEndereco.getCidadeOrigem(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getCepOrigem() != null) {
			searchCriteria.add(Restrictions.ilike("cepOrigem", filterCorridaEndereco.getCepOrigem(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cepOrigem", filterCorridaEndereco.getCepOrigem(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getReferenciaOrigem() != null) {
			searchCriteria.add(Restrictions.ilike("referenciaOrigem", filterCorridaEndereco.getReferenciaOrigem(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("referenciaOrigem", filterCorridaEndereco.getReferenciaOrigem(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getEnderecoDestino() != null) {
			searchCriteria.add(Restrictions.ilike("enderecoDestino", filterCorridaEndereco.getEnderecoDestino(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("enderecoDestino", filterCorridaEndereco.getEnderecoDestino(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getNumeroDestino() != null) {
			searchCriteria.add(Restrictions.ilike("numeroDestino", filterCorridaEndereco.getNumeroDestino(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("numeroDestino", filterCorridaEndereco.getNumeroDestino(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getComplementoDestino() != null) {
			searchCriteria.add(Restrictions.ilike("complementoDestino", filterCorridaEndereco.getComplementoDestino(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("complementoDestino", filterCorridaEndereco.getComplementoDestino(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getBairroDestino() != null) {
			searchCriteria.add(Restrictions.ilike("bairroDestino", filterCorridaEndereco.getBairroDestino(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("bairroDestino", filterCorridaEndereco.getBairroDestino(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getUfDestino() != null) {
			searchCriteria.add(Restrictions.ilike("ufDestino", filterCorridaEndereco.getUfDestino(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("ufDestino", filterCorridaEndereco.getUfDestino(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getCidadeDestino() != null) {
			searchCriteria.add(Restrictions.ilike("cidadeDestino", filterCorridaEndereco.getCidadeDestino(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cidadeDestino", filterCorridaEndereco.getCidadeDestino(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getCepDestino() != null) {
			searchCriteria.add(Restrictions.ilike("cepDestino", filterCorridaEndereco.getCepDestino(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cepDestino", filterCorridaEndereco.getCepDestino(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getReferenciaDestino() != null) {
			searchCriteria.add(Restrictions.ilike("referenciaDestino", filterCorridaEndereco.getReferenciaDestino(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("referenciaDestino", filterCorridaEndereco.getReferenciaDestino(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getDescricao() != null) {
			searchCriteria.add(Restrictions.ilike("descricao", filterCorridaEndereco.getDescricao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("descricao", filterCorridaEndereco.getDescricao(), MatchMode.ANYWHERE));
		}
		if (filterCorridaEndereco.getCorrida() != null) {
			searchCriteria.createAlias("corrida", "corrida_");
			countCriteria.createAlias("corrida", "corrida_");
			searchCriteria.add(Restrictions.eq("corrida_.id", filterCorridaEndereco.getCorrida()));
			countCriteria.add(Restrictions.eq("corrida_.id", filterCorridaEndereco.getCorrida()));
		}

		return new Paginator<CorridaEndereco>(searchCriteria, countCriteria).paginate(paginationParams);
	}

	public List<CorridaEndereco> filter(PaginationParams paginationParams) {
		List<CorridaEndereco> list = new ArrayList<CorridaEndereco>();
		FilterCorridaEndereco filterCorridaEndereco = (FilterCorridaEndereco) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterCorridaEndereco.getEnderecoOrigem() != null) {
			searchCriteria.add(Restrictions.eq("enderecoOrigem", filterCorridaEndereco.getEnderecoOrigem()));
		}
		if (filterCorridaEndereco.getNumeroOrigem() != null) {
			searchCriteria.add(Restrictions.eq("numeroOrigem", filterCorridaEndereco.getNumeroOrigem()));
		}
		if (filterCorridaEndereco.getComplementoOrigem() != null) {
			searchCriteria.add(Restrictions.eq("complementoOrigem", filterCorridaEndereco.getComplementoOrigem()));
		}
		if (filterCorridaEndereco.getBairroOrigem() != null) {
			searchCriteria.add(Restrictions.eq("bairroOrigem", filterCorridaEndereco.getBairroOrigem()));
		}
		if (filterCorridaEndereco.getUfOrigem() != null) {
			searchCriteria.add(Restrictions.eq("ufOrigem", filterCorridaEndereco.getUfOrigem()));
		}
		if (filterCorridaEndereco.getCidadeOrigem() != null) {
			searchCriteria.add(Restrictions.eq("cidadeOrigem", filterCorridaEndereco.getCidadeOrigem()));
		}
		if (filterCorridaEndereco.getCepOrigem() != null) {
			searchCriteria.add(Restrictions.eq("cepOrigem", filterCorridaEndereco.getCepOrigem()));
		}
		if (filterCorridaEndereco.getReferenciaOrigem() != null) {
			searchCriteria.add(Restrictions.eq("referenciaOrigem", filterCorridaEndereco.getReferenciaOrigem()));
		}
		if (filterCorridaEndereco.getEnderecoDestino() != null) {
			searchCriteria.add(Restrictions.eq("enderecoDestino", filterCorridaEndereco.getEnderecoDestino()));
		}
		if (filterCorridaEndereco.getNumeroDestino() != null) {
			searchCriteria.add(Restrictions.eq("numeroDestino", filterCorridaEndereco.getNumeroDestino()));
		}
		if (filterCorridaEndereco.getComplementoDestino() != null) {
			searchCriteria.add(Restrictions.eq("complementoDestino", filterCorridaEndereco.getComplementoDestino()));
		}
		if (filterCorridaEndereco.getBairroDestino() != null) {
			searchCriteria.add(Restrictions.eq("bairroDestino", filterCorridaEndereco.getBairroDestino()));
		}
		if (filterCorridaEndereco.getUfDestino() != null) {
			searchCriteria.add(Restrictions.eq("ufDestino", filterCorridaEndereco.getUfDestino()));
		}
		if (filterCorridaEndereco.getCidadeDestino() != null) {
			searchCriteria.add(Restrictions.eq("cidadeDestino", filterCorridaEndereco.getCidadeDestino()));
		}
		if (filterCorridaEndereco.getCepDestino() != null) {
			searchCriteria.add(Restrictions.eq("cepDestino", filterCorridaEndereco.getCepDestino()));
		}
		if (filterCorridaEndereco.getReferenciaDestino() != null) {
			searchCriteria.add(Restrictions.eq("referenciaDestino", filterCorridaEndereco.getReferenciaDestino()));
		}
		
		if (filterCorridaEndereco.getDescricao() != null) {
			searchCriteria.add(Restrictions.eq("descricao", filterCorridaEndereco.getDescricao()));
		}
		
		if (filterCorridaEndereco.getCorrida() != null) {
			searchCriteria.createAlias("corrida", "corrida_");
			searchCriteria.add(Restrictions.eq("corrida_.id", filterCorridaEndereco.getCorrida()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
