package br.com.invista.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;
import br.com.invista.model.Corrida;
import br.com.invista.model.DistanciaMotoCorrida;
import br.com.invista.model.filter.FilterDistanciaMotoCorrida;

/**
 * generated: 04/11/2016 11:29:26
 **/

@Named
@SuppressWarnings("rawtypes")
public class DaoDistanciaMotoCorrida extends AccessibleHibernateDao<DistanciaMotoCorrida> {
	private static final Logger LOGGER = Logger.getLogger(DaoDistanciaMotoCorrida.class);

	public DaoDistanciaMotoCorrida() {
		super(DistanciaMotoCorrida.class);
	}
	
	public void deleteDistanciaMotoCorridaByCorrida(Corrida corrida) {
		Query q = getSession().createQuery("delete DistanciaMotoCorrida where corrida.id = :pCorrida ");
		q.setParameter("pCorrida", corrida.getId());
		q.executeUpdate();
	}

	@Override
	public Pagination<DistanciaMotoCorrida> getAll(PaginationParams paginationParams) {
		FilterDistanciaMotoCorrida filterDistanciaMotoCorrida = (FilterDistanciaMotoCorrida) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterDistanciaMotoCorrida.getDistancia() != null) {
			searchCriteria.add(Restrictions.eq("distancia", filterDistanciaMotoCorrida.getDistancia()));
			countCriteria.add(Restrictions.eq("distancia", filterDistanciaMotoCorrida.getDistancia()));
		}
		if (filterDistanciaMotoCorrida.getSequencia() != null) {
			searchCriteria.add(Restrictions.eq("sequencia", filterDistanciaMotoCorrida.getSequencia()));
			countCriteria.add(Restrictions.eq("sequencia", filterDistanciaMotoCorrida.getSequencia()));
		}
		if (filterDistanciaMotoCorrida.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			countCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterDistanciaMotoCorrida.getMotorista()));
			countCriteria.add(Restrictions.eq("motorista_.id", filterDistanciaMotoCorrida.getMotorista()));
		}
		if (filterDistanciaMotoCorrida.getCorrida() != null) {
			searchCriteria.createAlias("corrida", "corrida_");
			countCriteria.createAlias("corrida", "corrida_");
			searchCriteria.add(Restrictions.eq("corrida_.id", filterDistanciaMotoCorrida.getCorrida()));
			countCriteria.add(Restrictions.eq("corrida_.id", filterDistanciaMotoCorrida.getCorrida()));
		}

		return new Paginator<DistanciaMotoCorrida>(searchCriteria, countCriteria).paginate(paginationParams);
	}

	public List<DistanciaMotoCorrida> filter(PaginationParams paginationParams) {
		FilterDistanciaMotoCorrida filterDistanciaMotoCorrida = (FilterDistanciaMotoCorrida) paginationParams.getFilter();

		return filter(filterDistanciaMotoCorrida);

	}

	public List<DistanciaMotoCorrida> filter(FilterDistanciaMotoCorrida filterDistanciaMotoCorrida) {
		List<DistanciaMotoCorrida> list = new ArrayList<DistanciaMotoCorrida>();

		Criteria searchCriteria = criteria();
		if (filterDistanciaMotoCorrida.getDistancia() != null) {
			searchCriteria.add(Restrictions.eq("distancia", filterDistanciaMotoCorrida.getDistancia()));
		}
		if (filterDistanciaMotoCorrida.getSequencia() != null) {
			searchCriteria.add(Restrictions.eq("sequencia", filterDistanciaMotoCorrida.getSequencia()));
		}
		if (filterDistanciaMotoCorrida.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterDistanciaMotoCorrida.getMotorista()));
		}
		if (filterDistanciaMotoCorrida.getCorrida() != null) {
			searchCriteria.createAlias("corrida", "corrida_");
			searchCriteria.add(Restrictions.eq("corrida_.id", filterDistanciaMotoCorrida.getCorrida()));
		}

		if (filterDistanciaMotoCorrida.getStatusCorrida() != null) {
			searchCriteria.createAlias("corrida", "corrida_");
			searchCriteria.add(Restrictions.eq("corrida_.statusCorrida", filterDistanciaMotoCorrida.getStatusCorrida()));
		}

		list.addAll(searchCriteria.list());

		return list;
	}
}
