package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.Socio;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterSocio;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.Socio;
/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoSocio extends AccessibleHibernateDao<Socio> {
	private static final Logger LOGGER = Logger.getLogger(DaoSocio.class);

	public DaoSocio() {
		super(Socio.class);
	}

	@Override
	public Pagination<Socio> getAll(PaginationParams paginationParams) {
		FilterSocio filterSocio = (FilterSocio) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterSocio.getNome() != null) {
			searchCriteria.add(Restrictions.ilike("nome", filterSocio.getNome(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("nome", filterSocio.getNome(), MatchMode.ANYWHERE));
		}
		if (filterSocio.getCpf() != null) {
			searchCriteria.add(Restrictions.ilike("cpf", filterSocio.getCpf(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cpf", filterSocio.getCpf(), MatchMode.ANYWHERE));
		}
		if (filterSocio.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			countCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterSocio.getEmpresa()));
			countCriteria.add(Restrictions.eq("empresa_.id", filterSocio.getEmpresa()));
		}

		return new Paginator<Socio>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<Socio> filter(PaginationParams paginationParams) {
		List<Socio> list = new ArrayList<Socio>();
		FilterSocio filterSocio = (FilterSocio) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterSocio.getNome() != null) {
			searchCriteria.add(Restrictions.eq("nome", filterSocio.getNome()));
		}
		if (filterSocio.getCpf() != null) {
			searchCriteria.add(Restrictions.eq("cpf", filterSocio.getCpf()));
		}
		if (filterSocio.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterSocio.getEmpresa()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
