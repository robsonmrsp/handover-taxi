package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.StatusTracker;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterStatusTracker;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.StatusTracker;
/**
*  generated: 04/11/2016 11:29:27
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoStatusTracker extends AccessibleHibernateDao<StatusTracker> {
	private static final Logger LOGGER = Logger.getLogger(DaoStatusTracker.class);

	public DaoStatusTracker() {
		super(StatusTracker.class);
	}

	@Override
	public Pagination<StatusTracker> getAll(PaginationParams paginationParams) {
		FilterStatusTracker filterStatusTracker = (FilterStatusTracker) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterStatusTracker.getLatitude() != null) {
			searchCriteria.add(Restrictions.eq("latitude", filterStatusTracker.getLatitude()));
			countCriteria.add(Restrictions.eq("latitude", filterStatusTracker.getLatitude()));
		}				
		if (filterStatusTracker.getLongitude() != null) {
			searchCriteria.add(Restrictions.eq("longitude", filterStatusTracker.getLongitude()));
			countCriteria.add(Restrictions.eq("longitude", filterStatusTracker.getLongitude()));
		}				
		if (filterStatusTracker.getTimestamp() != null) {
			searchCriteria.add(Restrictions.eq("timestamp", filterStatusTracker.getTimestamp()));
			countCriteria.add(Restrictions.eq("timestamp", filterStatusTracker.getTimestamp()));
		}				
		if (filterStatusTracker.getSpeed() != null) {
			searchCriteria.add(Restrictions.eq("speed", filterStatusTracker.getSpeed()));
			countCriteria.add(Restrictions.eq("speed", filterStatusTracker.getSpeed()));
		}				
		if (filterStatusTracker.getAccuracy() != null) {
			searchCriteria.add(Restrictions.eq("accuracy", filterStatusTracker.getAccuracy()));
			countCriteria.add(Restrictions.eq("accuracy", filterStatusTracker.getAccuracy()));
		}				
		if (filterStatusTracker.getDirection() != null) {
			searchCriteria.add(Restrictions.eq("direction", filterStatusTracker.getDirection()));
			countCriteria.add(Restrictions.eq("direction", filterStatusTracker.getDirection()));
		}				
		if (filterStatusTracker.getAltitude() != null) {
			searchCriteria.add(Restrictions.eq("altitude", filterStatusTracker.getAltitude()));
			countCriteria.add(Restrictions.eq("altitude", filterStatusTracker.getAltitude()));
		}				
		if (filterStatusTracker.getBateryLevel() != null) {
			searchCriteria.add(Restrictions.eq("bateryLevel", filterStatusTracker.getBateryLevel()));
			countCriteria.add(Restrictions.eq("bateryLevel", filterStatusTracker.getBateryLevel()));
		}				
		if (filterStatusTracker.getGpsEnabled() != null) {
			searchCriteria.add(Restrictions.eq("gpsEnabled", filterStatusTracker.getGpsEnabled()));
			countCriteria.add(Restrictions.eq("gpsEnabled", filterStatusTracker.getGpsEnabled()));
		}				
		if (filterStatusTracker.getWifiEnabled() != null) {
			searchCriteria.add(Restrictions.eq("wifiEnabled", filterStatusTracker.getWifiEnabled()));
			countCriteria.add(Restrictions.eq("wifiEnabled", filterStatusTracker.getWifiEnabled()));
		}				
		if (filterStatusTracker.getMobileEnabled() != null) {
			searchCriteria.add(Restrictions.eq("mobileEnabled", filterStatusTracker.getMobileEnabled()));
			countCriteria.add(Restrictions.eq("mobileEnabled", filterStatusTracker.getMobileEnabled()));
		}				
		if (filterStatusTracker.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			countCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterStatusTracker.getMotorista()));
			countCriteria.add(Restrictions.eq("motorista_.id", filterStatusTracker.getMotorista()));
		}

		return new Paginator<StatusTracker>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<StatusTracker> filter(PaginationParams paginationParams) {
		List<StatusTracker> list = new ArrayList<StatusTracker>();
		FilterStatusTracker filterStatusTracker = (FilterStatusTracker) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterStatusTracker.getLatitude() != null) {
			searchCriteria.add(Restrictions.eq("latitude", filterStatusTracker.getLatitude()));
		}
		if (filterStatusTracker.getLongitude() != null) {
			searchCriteria.add(Restrictions.eq("longitude", filterStatusTracker.getLongitude()));
		}
		if (filterStatusTracker.getTimestamp() != null) {
			searchCriteria.add(Restrictions.eq("timestamp", filterStatusTracker.getTimestamp()));
		}
		if (filterStatusTracker.getSpeed() != null) {
			searchCriteria.add(Restrictions.eq("speed", filterStatusTracker.getSpeed()));
		}
		if (filterStatusTracker.getAccuracy() != null) {
			searchCriteria.add(Restrictions.eq("accuracy", filterStatusTracker.getAccuracy()));
		}
		if (filterStatusTracker.getDirection() != null) {
			searchCriteria.add(Restrictions.eq("direction", filterStatusTracker.getDirection()));
		}
		if (filterStatusTracker.getAltitude() != null) {
			searchCriteria.add(Restrictions.eq("altitude", filterStatusTracker.getAltitude()));
		}
		if (filterStatusTracker.getBateryLevel() != null) {
			searchCriteria.add(Restrictions.eq("bateryLevel", filterStatusTracker.getBateryLevel()));
		}
		if (filterStatusTracker.getGpsEnabled() != null) {
			searchCriteria.add(Restrictions.eq("gpsEnabled", filterStatusTracker.getGpsEnabled()));
		}
		if (filterStatusTracker.getWifiEnabled() != null) {
			searchCriteria.add(Restrictions.eq("wifiEnabled", filterStatusTracker.getWifiEnabled()));
		}
		if (filterStatusTracker.getMobileEnabled() != null) {
			searchCriteria.add(Restrictions.eq("mobileEnabled", filterStatusTracker.getMobileEnabled()));
		}
		if (filterStatusTracker.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterStatusTracker.getMotorista()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
