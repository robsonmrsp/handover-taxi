package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.Mensagem;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterMensagem;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.Mensagem;
/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoMensagem extends AccessibleHibernateDao<Mensagem> {
	private static final Logger LOGGER = Logger.getLogger(DaoMensagem.class);

	public DaoMensagem() {
		super(Mensagem.class);
	}

	@Override
	public Pagination<Mensagem> getAll(PaginationParams paginationParams) {
		FilterMensagem filterMensagem = (FilterMensagem) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterMensagem.getDescricao() != null) {
			searchCriteria.add(Restrictions.ilike("descricao", filterMensagem.getDescricao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("descricao", filterMensagem.getDescricao(), MatchMode.ANYWHERE));
		}

		return new Paginator<Mensagem>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<Mensagem> filter(PaginationParams paginationParams) {
		List<Mensagem> list = new ArrayList<Mensagem>();
		FilterMensagem filterMensagem = (FilterMensagem) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterMensagem.getDescricao() != null) {
			searchCriteria.add(Restrictions.eq("descricao", filterMensagem.getDescricao()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
