package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.Atendente;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterAtendente;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.Atendente;
/**
*  generated: 16/10/2016 15:27:07
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoAtendente extends AccessibleHibernateDao<Atendente> {
	private static final Logger LOGGER = Logger.getLogger(DaoAtendente.class);

	public DaoAtendente() {
		super(Atendente.class);
	}

	@Override
	public Pagination<Atendente> getAll(PaginationParams paginationParams) {
		FilterAtendente filterAtendente = (FilterAtendente) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterAtendente.getNome() != null) {
			searchCriteria.add(Restrictions.ilike("nome", filterAtendente.getNome(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("nome", filterAtendente.getNome(), MatchMode.ANYWHERE));
		}
		if (filterAtendente.getSituacao() != null) {
			searchCriteria.add(Restrictions.ilike("situacao", filterAtendente.getSituacao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("situacao", filterAtendente.getSituacao(), MatchMode.ANYWHERE));
		}

		return new Paginator<Atendente>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<Atendente> filter(PaginationParams paginationParams) {
		List<Atendente> list = new ArrayList<Atendente>();
		FilterAtendente filterAtendente = (FilterAtendente) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterAtendente.getNome() != null) {
			searchCriteria.add(Restrictions.eq("nome", filterAtendente.getNome()));
		}
		if (filterAtendente.getSituacao() != null) {
			searchCriteria.add(Restrictions.eq("situacao", filterAtendente.getSituacao()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
