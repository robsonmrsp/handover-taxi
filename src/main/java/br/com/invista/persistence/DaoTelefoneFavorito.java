package br.com.invista.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Entity;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;
import br.com.invista.model.Contato;
import br.com.invista.model.TelefoneFavorito;
import br.com.invista.model.filter.FilterTelefoneFavorito;

/**
 * generated: 16/10/2016 15:27:08
 **/

@Named
@SuppressWarnings("rawtypes")
public class DaoTelefoneFavorito extends AccessibleHibernateDao<TelefoneFavorito> {
	private static final Logger LOGGER = Logger.getLogger(DaoTelefoneFavorito.class);

	@Inject
	SessionFactory sessionFactory;

	public DaoTelefoneFavorito() {
		super(TelefoneFavorito.class);
	}

	@Override
	public Pagination<TelefoneFavorito> getAll(PaginationParams paginationParams) {
		FilterTelefoneFavorito filterTelefoneFavorito = (FilterTelefoneFavorito) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterTelefoneFavorito.getDdd() != null) {
			searchCriteria.add(Restrictions.ilike("ddd", filterTelefoneFavorito.getDdd(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("ddd", filterTelefoneFavorito.getDdd(), MatchMode.ANYWHERE));
		}
		if (filterTelefoneFavorito.getFone() != null) {
			searchCriteria.add(Restrictions.ilike("fone", filterTelefoneFavorito.getFone(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("fone", filterTelefoneFavorito.getFone(), MatchMode.ANYWHERE));
		}
		if (filterTelefoneFavorito.getFavorito() != null) {
			searchCriteria.add(Restrictions.eq("favorito", filterTelefoneFavorito.getFavorito()));
			countCriteria.add(Restrictions.eq("favorito", filterTelefoneFavorito.getFavorito()));
		}
		if (filterTelefoneFavorito.getCliente() != null) {
			searchCriteria.createAlias("cliente", "cliente_");
			countCriteria.createAlias("cliente", "cliente_");
			searchCriteria.add(Restrictions.eq("cliente_.id", filterTelefoneFavorito.getCliente()));
			countCriteria.add(Restrictions.eq("cliente_.id", filterTelefoneFavorito.getCliente()));
		}
		if (filterTelefoneFavorito.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			countCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterTelefoneFavorito.getEmpresa()));
			countCriteria.add(Restrictions.eq("empresa_.id", filterTelefoneFavorito.getEmpresa()));
		}

		return new Paginator<TelefoneFavorito>(searchCriteria, countCriteria).paginate(paginationParams);
	}

	public List<TelefoneFavorito> filter(PaginationParams paginationParams) {
		List<TelefoneFavorito> list = new ArrayList<TelefoneFavorito>();
		FilterTelefoneFavorito filterTelefoneFavorito = (FilterTelefoneFavorito) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterTelefoneFavorito.getDdd() != null) {
			searchCriteria.add(Restrictions.eq("ddd", filterTelefoneFavorito.getDdd()));
		}
		if (filterTelefoneFavorito.getFone() != null) {
			searchCriteria.add(Restrictions.eq("fone", filterTelefoneFavorito.getFone()));
		}
		if (filterTelefoneFavorito.getFavorito() != null) {
			searchCriteria.add(Restrictions.eq("favorito", filterTelefoneFavorito.getFavorito()));
		}
		if (filterTelefoneFavorito.getCliente() != null) {
			searchCriteria.createAlias("cliente", "cliente_");
			searchCriteria.add(Restrictions.eq("cliente_.id", filterTelefoneFavorito.getCliente()));
		}
		if (filterTelefoneFavorito.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterTelefoneFavorito.getEmpresa()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}

	public Boolean isValid(Integer id, String telefone) {
		Integer id_ = null;
		String telefone_ = null;
		// vitoriano : refactor
		try {
			id_ = (Integer) criteria().add(Restrictions.eq("fone", telefone)).setProjection(Projections.property("id"))
					.setMaxResults(1).uniqueResult();
			telefone_ = (String) criteria().add(Restrictions.eq("fone", telefone))
					.setProjection(Projections.property("fone")).setMaxResults(1).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro pelo telefone," + telefone, e);
		}
		
		// vitoriano
		if(id_ != null && id != null){
			return telefone_ != null && id_.intValue() != id.intValue() ? true : false;
		}
		
		return telefone_ != null && id_ != id ? true : false;
	}
	
	public TelefoneFavorito getPorTelefone(String telefone)
	{
		TelefoneFavorito telefoneFavorito = null;
		try {
			telefoneFavorito = (TelefoneFavorito) criteria().add(Restrictions.eq("fone", telefone)).setMaxResults(1).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro pelo telefone," + telefone, e);
		}
		return telefoneFavorito;
	}
}
