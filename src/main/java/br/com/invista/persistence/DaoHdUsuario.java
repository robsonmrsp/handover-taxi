package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.HdUsuario;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterHdUsuario;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.HdUsuario;
/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoHdUsuario extends AccessibleHibernateDao<HdUsuario> {
	private static final Logger LOGGER = Logger.getLogger(DaoHdUsuario.class);

	public DaoHdUsuario() {
		super(HdUsuario.class);
	}

	@Override
	public Pagination<HdUsuario> getAll(PaginationParams paginationParams) {
		FilterHdUsuario filterHdUsuario = (FilterHdUsuario) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterHdUsuario.getNome() != null) {
			searchCriteria.add(Restrictions.ilike("nome", filterHdUsuario.getNome(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("nome", filterHdUsuario.getNome(), MatchMode.ANYWHERE));
		}
		if (filterHdUsuario.getEmail() != null) {
			searchCriteria.add(Restrictions.ilike("email", filterHdUsuario.getEmail(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("email", filterHdUsuario.getEmail(), MatchMode.ANYWHERE));
		}
		if (filterHdUsuario.getSenha() != null) {
			searchCriteria.add(Restrictions.ilike("senha", filterHdUsuario.getSenha(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("senha", filterHdUsuario.getSenha(), MatchMode.ANYWHERE));
		}
		if (filterHdUsuario.getTipo() != null) {
			searchCriteria.add(Restrictions.ilike("tipo", filterHdUsuario.getTipo(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("tipo", filterHdUsuario.getTipo(), MatchMode.ANYWHERE));
		}
		if (filterHdUsuario.getPerfil() != null) {
			searchCriteria.add(Restrictions.ilike("perfil", filterHdUsuario.getPerfil(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("perfil", filterHdUsuario.getPerfil(), MatchMode.ANYWHERE));
		}
		if (filterHdUsuario.getStatusUsuario() != null) {
			searchCriteria.add(Restrictions.ilike("statusUsuario", filterHdUsuario.getStatusUsuario(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("statusUsuario", filterHdUsuario.getStatusUsuario(), MatchMode.ANYWHERE));
		}
		if (filterHdUsuario.getDatainclusao() != null) {
			searchCriteria.add(Restrictions.eq("datainclusao", filterHdUsuario.getDatainclusao()));
			countCriteria.add(Restrictions.eq("datainclusao", filterHdUsuario.getDatainclusao()));
		}				
		if (filterHdUsuario.getUsuarioinclusao() != null) {
			searchCriteria.add(Restrictions.eq("usuarioinclusao", filterHdUsuario.getUsuarioinclusao()));
			countCriteria.add(Restrictions.eq("usuarioinclusao", filterHdUsuario.getUsuarioinclusao()));
		}				
		if (filterHdUsuario.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			countCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterHdUsuario.getEmpresa()));
			countCriteria.add(Restrictions.eq("empresa_.id", filterHdUsuario.getEmpresa()));
		}

		return new Paginator<HdUsuario>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<HdUsuario> filter(PaginationParams paginationParams) {
		List<HdUsuario> list = new ArrayList<HdUsuario>();
		FilterHdUsuario filterHdUsuario = (FilterHdUsuario) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterHdUsuario.getNome() != null) {
			searchCriteria.add(Restrictions.eq("nome", filterHdUsuario.getNome()));
		}
		if (filterHdUsuario.getEmail() != null) {
			searchCriteria.add(Restrictions.eq("email", filterHdUsuario.getEmail()));
		}
		if (filterHdUsuario.getSenha() != null) {
			searchCriteria.add(Restrictions.eq("senha", filterHdUsuario.getSenha()));
		}
		if (filterHdUsuario.getTipo() != null) {
			searchCriteria.add(Restrictions.eq("tipo", filterHdUsuario.getTipo()));
		}
		if (filterHdUsuario.getPerfil() != null) {
			searchCriteria.add(Restrictions.eq("perfil", filterHdUsuario.getPerfil()));
		}
		if (filterHdUsuario.getStatusUsuario() != null) {
			searchCriteria.add(Restrictions.eq("statusUsuario", filterHdUsuario.getStatusUsuario()));
		}
		if (filterHdUsuario.getDatainclusao() != null) {
			searchCriteria.add(Restrictions.eq("datainclusao", filterHdUsuario.getDatainclusao()));
		}
		if (filterHdUsuario.getUsuarioinclusao() != null) {
			searchCriteria.add(Restrictions.eq("usuarioinclusao", filterHdUsuario.getUsuarioinclusao()));
		}
		if (filterHdUsuario.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterHdUsuario.getEmpresa()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
