package br.com.invista.persistence;

import javax.inject.Named;
import javax.persistence.criteria.CriteriaQuery;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.MensagemMotorista;
import br.com.invista.model.Motorista;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterMensagemMotorista;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.MensagemMotorista;
/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoMensagemMotorista extends AccessibleHibernateDao<MensagemMotorista> {
	private static final Logger LOGGER = Logger.getLogger(DaoMensagemMotorista.class);

	public DaoMensagemMotorista() {
		super(MensagemMotorista.class);
	}

	@Override
	public Pagination<MensagemMotorista> getAll(PaginationParams paginationParams) {
		FilterMensagemMotorista filterMensagemMotorista = (FilterMensagemMotorista) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterMensagemMotorista.getDataEnvio() != null) {
			searchCriteria.add(Restrictions.eq("dataEnvio", filterMensagemMotorista.getDataEnvio()));
			countCriteria.add(Restrictions.eq("dataEnvio", filterMensagemMotorista.getDataEnvio()));
		}				
		if (filterMensagemMotorista.getDataLeitura() != null) {
			searchCriteria.add(Restrictions.eq("dataLeitura", filterMensagemMotorista.getDataLeitura()));
			countCriteria.add(Restrictions.eq("dataLeitura", filterMensagemMotorista.getDataLeitura()));
		}				
		if (filterMensagemMotorista.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			countCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterMensagemMotorista.getMotorista()));
			countCriteria.add(Restrictions.eq("motorista_.id", filterMensagemMotorista.getMotorista()));
		}
		if (filterMensagemMotorista.getMensagem() != null) {
			searchCriteria.createAlias("mensagem", "mensagem_");
			countCriteria.createAlias("mensagem", "mensagem_");
			searchCriteria.add(Restrictions.eq("mensagem_.id", filterMensagemMotorista.getMensagem()));
			countCriteria.add(Restrictions.eq("mensagem_.id", filterMensagemMotorista.getMensagem()));
		}

		return new Paginator<MensagemMotorista>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<MensagemMotorista> filter(PaginationParams paginationParams) {
		List<MensagemMotorista> list = new ArrayList<MensagemMotorista>();
		FilterMensagemMotorista filterMensagemMotorista = (FilterMensagemMotorista) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterMensagemMotorista.getDataEnvio() != null) {
			searchCriteria.add(Restrictions.eq("dataEnvio", filterMensagemMotorista.getDataEnvio()));
		}
		if (filterMensagemMotorista.getDataLeitura() != null) {
			searchCriteria.add(Restrictions.eq("dataLeitura", filterMensagemMotorista.getDataLeitura()));
		}
		if (filterMensagemMotorista.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterMensagemMotorista.getMotorista()));
		}
		if (filterMensagemMotorista.getMensagem() != null) {
			searchCriteria.createAlias("mensagem", "mensagem_");
			searchCriteria.add(Restrictions.eq("mensagem_.id", filterMensagemMotorista.getMensagem()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
	
	public List<MensagemMotorista> byMotorista(Integer id) {
		Criteria criteria = criteria();
		
		criteria.add(Restrictions.eq("motorista.id", id));
		
		return criteria.list();
	}
	
}
