package br.com.invista.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;
import br.com.invista.model.Corrida;

import br.com.invista.model.Motorista;
import br.com.invista.model.TipoPagamento;
import br.com.invista.model.filter.FilterMotorista;

/**
 * generated: 16/10/2016 15:27:08
 **/

@Named
@SuppressWarnings("rawtypes")
public class DaoMotorista extends AccessibleHibernateDao<Motorista> {

	private static final Logger LOGGER = Logger.getLogger(DaoMotorista.class);

	@Inject
	SessionFactory sessionFactory;

	public DaoMotorista() {
		super(Motorista.class);
	}

	public List<Motorista> motoristasOnline(){
		List<Motorista> motoristasOnline = new ArrayList<Motorista>();
		Criteria cr = criteria();
		cr.add(Restrictions.sqlRestriction("tempo_ultima_posicao >= dateadd(minute, -15 ,getdate())"));
		motoristasOnline.addAll(cr.list());
		return motoristasOnline;
	}
	
	/**
	 * Lasciate ogne speranza, voi ch'intrate 
	 * Abandon all hope, ye who enter here
	 * O buraco da gia é mais lá embaixo - 
	 * Valeu, Diego! Jamais esqueceremos de você, Israel.
	 */
	@SuppressWarnings("unchecked")
	public List<Motorista> findByCorridaConstraints(Corrida corrida) {

		List<Motorista> motoristas = null;
		StringBuilder sb = new StringBuilder();
		// sql padrao
		/*String sql = "select {a.*} from  "
				+ "(select b.* from motorista as b left join punicao as p on (b.id = p.id_motorista) where p.id_motorista is null or p.data_fim > getdate() and b.status_motorista = 'Ativo' and b.status_app = 'ONLINE' "
				+ "except "
				+ "select c.* from motorista as c inner join motorista_bloqueado as mb on (c.id = mb.id_motorista) where mb.id_cliente = :id_cliente and c.status_motorista = 'Ativo' and c.status_app = 'ONLINE') as a ";*/
		String sql = "select {d.*} from  "+
					"(select c.* "+
					"       from motorista c "+
					"	   where c.status_motorista = 'Ativo' and c.status_app = 'ONLINE' and (c.tempo_ultima_posicao >= dateadd(minute, -15 ,getdate())) "+
					"except "+
					"select b.* "+
					"       from motorista as b "+
					"	   inner join punicao p "+
					"	   on ( p.id_motorista = b.id ) "+
					//"	   where b.status_motorista = 'Ativo' and b.status_app = 'ONLINE' and (p.data_fim >= dateadd(hh, -3 ,getdate())) "+
					"	   where b.status_motorista = 'Ativo' and b.status_app = 'ONLINE' and (p.data_fim >= getdate()) "+
					"except "+
					"select a.* "+
					"from motorista as a "+
					"       inner join motorista_bloqueado as mb "+
				    "		on (a.id = mb.id_motorista) "+
					" 		where a.status_motorista = 'Ativo' and a.status_app = 'ONLINE' and mb.id_cliente = :id_cliente ) as d ";
		// cria as constraints
		String constraints = null;
		sb.append("1 = 1");
		Integer tipoPagamento = corrida.getTipoPagamento();

		if (tipoPagamento == TipoPagamento.CREDITO) {
			sb.append(" and d.cartao_credito = 1 ");
		}

		if (tipoPagamento == TipoPagamento.DEBITO) {
			sb.append(" and d.cartao_debito = 1 ");
		}

		if (tipoPagamento == TipoPagamento.VOUCHER) {
			sb.append(" and d.voucher = 1 ");
		}

		if (tipoPagamento == TipoPagamento.ETICKET) {
			sb.append(" and d.eticket = 1 ");
		}

		if (corrida.getBau()) {
			sb.append(" and d.bau = 1 ");
		}

		if (corrida.getPadronizada()) {
			sb.append(" and d.padronizada = 1 ");
		}

		if (corrida.getMotoGrande()) {
			sb.append(" and d.moto_grande = 1 ");
		}
		constraints = sb.toString();
		
		// realiza a query
		SQLQuery query = nativeQuery(sql + " where " + constraints);
		query.setReadOnly(true);
		query.setParameter("id_cliente", corrida.getCliente().getId());
		query.addEntity("d", Motorista.class);
		motoristas = query.list();

		// aqui o Israel chora
		return motoristas;
	}

	@Override
	public Pagination<Motorista> getAll(PaginationParams paginationParams) {
		FilterMotorista filterMotorista = (FilterMotorista) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterMotorista.getNome() != null) {
			searchCriteria.add(Restrictions.ilike("nome", filterMotorista.getNome(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("nome", filterMotorista.getNome(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getNomeReduzido() != null) {
			searchCriteria
					.add(Restrictions.ilike("nomeReduzido", filterMotorista.getNomeReduzido(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("nomeReduzido", filterMotorista.getNomeReduzido(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getViatura() != null) {
			searchCriteria.add(Restrictions.ilike("viatura", filterMotorista.getViatura(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("viatura", filterMotorista.getViatura(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getEmail() != null) {
			searchCriteria.add(Restrictions.ilike("email", filterMotorista.getEmail(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("email", filterMotorista.getEmail(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getObservacao() != null) {
			searchCriteria.add(Restrictions.ilike("observacao", filterMotorista.getObservacao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("observacao", filterMotorista.getObservacao(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getTempoUltimaPosicao() != null) {
			searchCriteria.add(Restrictions.eq("tempo_ultima_observacao", filterMotorista.getTempoUltimaPosicao()));
			countCriteria.add(Restrictions.eq("tempo_ultima_observacao", filterMotorista.getTempoUltimaPosicao()));
		}
		if (filterMotorista.getDdd() != null) {
			searchCriteria.add(Restrictions.ilike("ddd", filterMotorista.getDdd(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("ddd", filterMotorista.getDdd(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getFone() != null) {
			searchCriteria.add(Restrictions.ilike("fone", filterMotorista.getFone(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("fone", filterMotorista.getFone(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getStatusMotorista() != null) {
			searchCriteria
					.add(Restrictions.ilike("statusMotorista", filterMotorista.getStatusMotorista(), MatchMode.EXACT));
			countCriteria
					.add(Restrictions.ilike("statusMotorista", filterMotorista.getStatusMotorista(), MatchMode.EXACT));
		}
		if (filterMotorista.getStatusApp() != null) {
			searchCriteria.add(Restrictions.ilike("statusApp", filterMotorista.getStatusApp(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("statusApp", filterMotorista.getStatusApp(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getDdd2() != null) {
			searchCriteria.add(Restrictions.ilike("ddd2", filterMotorista.getDdd2(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("ddd2", filterMotorista.getDdd2(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getFone2() != null) {
			searchCriteria.add(Restrictions.ilike("fone2", filterMotorista.getFone2(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("fone2", filterMotorista.getFone2(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getDddWhatsapp() != null) {
			searchCriteria.add(Restrictions.ilike("dddWhatsapp", filterMotorista.getDddWhatsapp(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("dddWhatsapp", filterMotorista.getDddWhatsapp(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getWhatsapp() != null) {
			searchCriteria.add(Restrictions.ilike("whatsapp", filterMotorista.getWhatsapp(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("whatsapp", filterMotorista.getWhatsapp(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getDataNascimento() != null) {
			searchCriteria.add(Restrictions.eq("dataNascimento", filterMotorista.getDataNascimento()));
			countCriteria.add(Restrictions.eq("dataNascimento", filterMotorista.getDataNascimento()));
		}
		if (filterMotorista.getRg() != null) {
			searchCriteria.add(Restrictions.ilike("rg", filterMotorista.getRg(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("rg", filterMotorista.getRg(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getOrgaoExpedidor() != null) {
			searchCriteria
					.add(Restrictions.ilike("orgaoExpedidor", filterMotorista.getOrgaoExpedidor(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("orgaoExpedidor", filterMotorista.getOrgaoExpedidor(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getDataExpedicao() != null) {
			searchCriteria.add(Restrictions.eq("dataExpedicao", filterMotorista.getDataExpedicao()));
			countCriteria.add(Restrictions.eq("dataExpedicao", filterMotorista.getDataExpedicao()));
		}
		if (filterMotorista.getNaturalidade() != null) {
			searchCriteria
					.add(Restrictions.ilike("naturalidade", filterMotorista.getNaturalidade(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("naturalidade", filterMotorista.getNaturalidade(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getCpf() != null) {
			searchCriteria.add(Restrictions.ilike("cpf", filterMotorista.getCpf(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cpf", filterMotorista.getCpf(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getCnh() != null) {
			searchCriteria.add(Restrictions.ilike("cnh", filterMotorista.getCnh(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cnh", filterMotorista.getCnh(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getEmissaoCnh() != null) {
			searchCriteria.add(Restrictions.eq("emissaoCnh", filterMotorista.getEmissaoCnh()));
			countCriteria.add(Restrictions.eq("emissaoCnh", filterMotorista.getEmissaoCnh()));
		}
		if (filterMotorista.getCategoriaCnh() != null) {
			searchCriteria
					.add(Restrictions.ilike("categoriaCnh", filterMotorista.getCategoriaCnh(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("categoriaCnh", filterMotorista.getCategoriaCnh(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getValidadeCnh() != null) {
			searchCriteria.add(Restrictions.eq("validadeCnh", filterMotorista.getValidadeCnh()));
			countCriteria.add(Restrictions.eq("validadeCnh", filterMotorista.getValidadeCnh()));
		}
		if (filterMotorista.getFoto1() != null) {
			searchCriteria.add(Restrictions.ilike("foto1", filterMotorista.getFoto1(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("foto1", filterMotorista.getFoto1(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getFoto2() != null) {
			searchCriteria.add(Restrictions.ilike("foto2", filterMotorista.getFoto2(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("foto2", filterMotorista.getFoto2(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getFoto3() != null) {
			searchCriteria.add(Restrictions.ilike("foto3", filterMotorista.getFoto3(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("foto3", filterMotorista.getFoto3(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getEndereco() != null) {
			searchCriteria.add(Restrictions.ilike("endereco", filterMotorista.getEndereco(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("endereco", filterMotorista.getEndereco(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getNumeroEndereco() != null) {
			searchCriteria
					.add(Restrictions.ilike("numeroEndereco", filterMotorista.getNumeroEndereco(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("numeroEndereco", filterMotorista.getNumeroEndereco(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getComplemento() != null) {
			searchCriteria.add(Restrictions.ilike("complemento", filterMotorista.getComplemento(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("complemento", filterMotorista.getComplemento(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getUf() != null) {
			searchCriteria.add(Restrictions.ilike("uf", filterMotorista.getUf(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("uf", filterMotorista.getUf(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getCidade() != null) {
			searchCriteria.add(Restrictions.ilike("cidade", filterMotorista.getCidade(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cidade", filterMotorista.getCidade(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getBairro() != null) {
			searchCriteria.add(Restrictions.ilike("bairro", filterMotorista.getBairro(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("bairro", filterMotorista.getBairro(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getCep() != null) {
			searchCriteria.add(Restrictions.ilike("cep", filterMotorista.getCep(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cep", filterMotorista.getCep(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getReferenciaEndereco() != null) {
			searchCriteria.add(Restrictions.ilike("referenciaEndereco", filterMotorista.getReferenciaEndereco(),
					MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("referenciaEndereco", filterMotorista.getReferenciaEndereco(),
					MatchMode.ANYWHERE));
		}
		if (filterMotorista.getMae() != null) {
			searchCriteria.add(Restrictions.ilike("mae", filterMotorista.getMae(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("mae", filterMotorista.getMae(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getPai() != null) {
			searchCriteria.add(Restrictions.ilike("pai", filterMotorista.getPai(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("pai", filterMotorista.getPai(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getConjuge() != null) {
			searchCriteria.add(Restrictions.ilike("conjuge", filterMotorista.getConjuge(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("conjuge", filterMotorista.getConjuge(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getReferenciaPessoal1() != null) {
			searchCriteria.add(Restrictions.ilike("referenciaPessoal1", filterMotorista.getReferenciaPessoal1(),
					MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("referenciaPessoal1", filterMotorista.getReferenciaPessoal1(),
					MatchMode.ANYWHERE));
		}
		if (filterMotorista.getFoneReferencia1() != null) {
			searchCriteria.add(
					Restrictions.ilike("foneReferencia1", filterMotorista.getFoneReferencia1(), MatchMode.ANYWHERE));
			countCriteria.add(
					Restrictions.ilike("foneReferencia1", filterMotorista.getFoneReferencia1(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getParentescoReferencia1() != null) {
			searchCriteria.add(Restrictions.ilike("parentescoReferencia1", filterMotorista.getParentescoReferencia1(),
					MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("parentescoReferencia1", filterMotorista.getParentescoReferencia1(),
					MatchMode.ANYWHERE));
		}
		if (filterMotorista.getReferenciaPessoal2() != null) {
			searchCriteria.add(Restrictions.ilike("referenciaPessoal2", filterMotorista.getReferenciaPessoal2(),
					MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("referenciaPessoal2", filterMotorista.getReferenciaPessoal2(),
					MatchMode.ANYWHERE));
		}
		if (filterMotorista.getFoneReferencia2() != null) {
			searchCriteria.add(
					Restrictions.ilike("foneReferencia2", filterMotorista.getFoneReferencia2(), MatchMode.ANYWHERE));
			countCriteria.add(
					Restrictions.ilike("foneReferencia2", filterMotorista.getFoneReferencia2(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getParentescoReferencia2() != null) {
			searchCriteria.add(Restrictions.ilike("parentescoReferencia2", filterMotorista.getParentescoReferencia2(),
					MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("parentescoReferencia2", filterMotorista.getParentescoReferencia2(),
					MatchMode.ANYWHERE));
		}
		if (filterMotorista.getMarcaVeiculo() != null) {
			searchCriteria
					.add(Restrictions.ilike("marcaVeiculo", filterMotorista.getMarcaVeiculo(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("marcaVeiculo", filterMotorista.getMarcaVeiculo(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getModeloVeiculo() != null) {
			searchCriteria
					.add(Restrictions.ilike("modeloVeiculo", filterMotorista.getModeloVeiculo(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("modeloVeiculo", filterMotorista.getModeloVeiculo(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getPlacaVeiculo() != null) {
			searchCriteria
					.add(Restrictions.ilike("placaVeiculo", filterMotorista.getPlacaVeiculo(), MatchMode.ANYWHERE));
			countCriteria
					.add(Restrictions.ilike("placaVeiculo", filterMotorista.getPlacaVeiculo(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getCorVeiculo() != null) {
			searchCriteria.add(Restrictions.ilike("corVeiculo", filterMotorista.getCorVeiculo(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("corVeiculo", filterMotorista.getCorVeiculo(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getAnoVeiculo() != null) {
			searchCriteria.add(Restrictions.eq("anoVeiculo", filterMotorista.getAnoVeiculo()));
			countCriteria.add(Restrictions.eq("anoVeiculo", filterMotorista.getAnoVeiculo()));
		}
		if (filterMotorista.getRenavam() != null) {
			searchCriteria.add(Restrictions.ilike("renavam", filterMotorista.getRenavam(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("renavam", filterMotorista.getRenavam(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getChassi() != null) {
			searchCriteria.add(Restrictions.ilike("chassi", filterMotorista.getChassi(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("chassi", filterMotorista.getChassi(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getCategoria() != null) {
			searchCriteria.add(Restrictions.ilike("categoria", filterMotorista.getCategoria(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("categoria", filterMotorista.getCategoria(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getImei() != null) {
			searchCriteria.add(Restrictions.ilike("imei", filterMotorista.getImei(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("imei", filterMotorista.getImei(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getCartaoCredito() != null) {
			searchCriteria.add(Restrictions.eq("cartaoCredito", filterMotorista.getCartaoCredito()));
			countCriteria.add(Restrictions.eq("cartaoCredito", filterMotorista.getCartaoCredito()));
		}
		if (filterMotorista.getCartaoDebito() != null) {
			searchCriteria.add(Restrictions.eq("cartaoDebito", filterMotorista.getCartaoDebito()));
			countCriteria.add(Restrictions.eq("cartaoDebito", filterMotorista.getCartaoDebito()));
		}
		if (filterMotorista.getVoucher() != null) {
			searchCriteria.add(Restrictions.eq("voucher", filterMotorista.getVoucher()));
			countCriteria.add(Restrictions.eq("voucher", filterMotorista.getVoucher()));
		}
		if (filterMotorista.getEticket() != null) {
			searchCriteria.add(Restrictions.eq("eticket", filterMotorista.getEticket()));
			countCriteria.add(Restrictions.eq("eticket", filterMotorista.getEticket()));
		}
		if (filterMotorista.getUsuarioCadastro() != null) {
			searchCriteria.add(Restrictions.eq("usuarioCadastro", filterMotorista.getUsuarioCadastro()));
			countCriteria.add(Restrictions.eq("usuarioCadastro", filterMotorista.getUsuarioCadastro()));
		}
		if (filterMotorista.getDataCadastro() != null) {
			searchCriteria.add(Restrictions.eq("dataCadastro", filterMotorista.getDataCadastro()));
			countCriteria.add(Restrictions.eq("dataCadastro", filterMotorista.getDataCadastro()));
		}
		if (filterMotorista.getSenha() != null) {
			searchCriteria.add(Restrictions.ilike("senha", filterMotorista.getSenha(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("senha", filterMotorista.getSenha(), MatchMode.ANYWHERE));
		}
		if (filterMotorista.getPadronizada() != null) {
			searchCriteria.add(Restrictions.eq("padronizada", filterMotorista.getPadronizada()));
			countCriteria.add(Restrictions.eq("padronizada", filterMotorista.getPadronizada()));
		}
		if (filterMotorista.getBau() != null) {
			searchCriteria.add(Restrictions.eq("bau", filterMotorista.getBau()));
			countCriteria.add(Restrictions.eq("bau", filterMotorista.getBau()));
		}
		if (filterMotorista.getMotoGrande() != null) {
			searchCriteria.add(Restrictions.eq("motoGrande", filterMotorista.getMotoGrande()));
			countCriteria.add(Restrictions.eq("motoGrande", filterMotorista.getMotoGrande()));
		}

		return new Paginator<Motorista>(searchCriteria, countCriteria).paginate(paginationParams);
	}

	public Boolean isExistViatura(String viatura) {
		Motorista viaturaResult = null;
		viaturaResult = (Motorista) criteria().add(Restrictions.eq("viatura", viatura)).setMaxResults(1).uniqueResult();
		return viaturaResult != null;
	}

	public List<Motorista> filter(PaginationParams paginationParams) {
		List<Motorista> list = new ArrayList<Motorista>();
		FilterMotorista filterMotorista = (FilterMotorista) paginationParams.getFilter();

		Criteria searchCriteria = criteria();
		if (filterMotorista.getNome() != null) {
			searchCriteria.add(Restrictions.eq("nome", filterMotorista.getNome()));
		}
		if (filterMotorista.getNomeReduzido() != null) {
			searchCriteria.add(Restrictions.eq("nomeReduzido", filterMotorista.getNomeReduzido()));
		}
		if (filterMotorista.getViatura() != null) {
			searchCriteria.add(Restrictions.eq("viatura", filterMotorista.getViatura()));
		}
		if (filterMotorista.getEmail() != null) {
			searchCriteria.add(Restrictions.eq("email", filterMotorista.getEmail()));
		}
		if (filterMotorista.getDdd() != null) {
			searchCriteria.add(Restrictions.eq("ddd", filterMotorista.getDdd()));
		}
		if (filterMotorista.getFone() != null) {
			searchCriteria.add(Restrictions.eq("fone", filterMotorista.getFone()));
		}
		if (filterMotorista.getStatusMotorista() != null) {
			searchCriteria.add(Restrictions.ilike("statusMotorista", filterMotorista.getStatusMotorista()));
		}
		if (filterMotorista.getStatusApp() != null) {
			searchCriteria.add(Restrictions.eq("statusApp", filterMotorista.getStatusApp()));
		}
		if (filterMotorista.getDdd2() != null) {
			searchCriteria.add(Restrictions.eq("ddd2", filterMotorista.getDdd2()));
		}

		if (filterMotorista.getFone2() != null) {
			searchCriteria.add(Restrictions.eq("fone2", filterMotorista.getFone2()));
		}
		if (filterMotorista.getDddWhatsapp() != null) {
			searchCriteria.add(Restrictions.eq("dddWhatsapp", filterMotorista.getDddWhatsapp()));
		}
		if (filterMotorista.getWhatsapp() != null) {
			searchCriteria.add(Restrictions.eq("whatsapp", filterMotorista.getWhatsapp()));
		}
		if (filterMotorista.getDataNascimento() != null) {
			searchCriteria.add(Restrictions.eq("dataNascimento", filterMotorista.getDataNascimento()));
		}
		if (filterMotorista.getRg() != null) {
			searchCriteria.add(Restrictions.eq("rg", filterMotorista.getRg()));
		}
		if (filterMotorista.getOrgaoExpedidor() != null) {
			searchCriteria.add(Restrictions.eq("orgaoExpedidor", filterMotorista.getOrgaoExpedidor()));
		}
		if (filterMotorista.getDataExpedicao() != null) {
			searchCriteria.add(Restrictions.eq("dataExpedicao", filterMotorista.getDataExpedicao()));
		}
		if (filterMotorista.getNaturalidade() != null) {
			searchCriteria.add(Restrictions.eq("naturalidade", filterMotorista.getNaturalidade()));
		}
		if (filterMotorista.getCpf() != null) {
			searchCriteria.add(Restrictions.eq("cpf", filterMotorista.getCpf()));
		}
		if (filterMotorista.getCnh() != null) {
			searchCriteria.add(Restrictions.eq("cnh", filterMotorista.getCnh()));
		}
		if (filterMotorista.getEmissaoCnh() != null) {
			searchCriteria.add(Restrictions.eq("emissaoCnh", filterMotorista.getEmissaoCnh()));
		}
		if (filterMotorista.getCategoriaCnh() != null) {
			searchCriteria.add(Restrictions.eq("categoriaCnh", filterMotorista.getCategoriaCnh()));
		}
		if (filterMotorista.getValidadeCnh() != null) {
			searchCriteria.add(Restrictions.eq("validadeCnh", filterMotorista.getValidadeCnh()));
		}
		if (filterMotorista.getFoto1() != null) {
			searchCriteria.add(Restrictions.eq("foto1", filterMotorista.getFoto1()));
		}
		if (filterMotorista.getFoto2() != null) {
			searchCriteria.add(Restrictions.eq("foto2", filterMotorista.getFoto2()));
		}
		if (filterMotorista.getFoto3() != null) {
			searchCriteria.add(Restrictions.eq("foto3", filterMotorista.getFoto3()));
		}
		if (filterMotorista.getEndereco() != null) {
			searchCriteria.add(Restrictions.eq("endereco", filterMotorista.getEndereco()));
		}
		if (filterMotorista.getNumeroEndereco() != null) {
			searchCriteria.add(Restrictions.eq("numeroEndereco", filterMotorista.getNumeroEndereco()));
		}
		if (filterMotorista.getComplemento() != null) {
			searchCriteria.add(Restrictions.eq("complemento", filterMotorista.getComplemento()));
		}
		if (filterMotorista.getUf() != null) {
			searchCriteria.add(Restrictions.eq("uf", filterMotorista.getUf()));
		}
		if (filterMotorista.getCidade() != null) {
			searchCriteria.add(Restrictions.eq("cidade", filterMotorista.getCidade()));
		}
		if (filterMotorista.getBairro() != null) {
			searchCriteria.add(Restrictions.eq("bairro", filterMotorista.getBairro()));
		}
		if (filterMotorista.getCep() != null) {
			searchCriteria.add(Restrictions.eq("cep", filterMotorista.getCep()));
		}
		if (filterMotorista.getReferenciaEndereco() != null) {
			searchCriteria.add(Restrictions.eq("referenciaEndereco", filterMotorista.getReferenciaEndereco()));
		}
		if (filterMotorista.getMae() != null) {
			searchCriteria.add(Restrictions.eq("mae", filterMotorista.getMae()));
		}
		if (filterMotorista.getPai() != null) {
			searchCriteria.add(Restrictions.eq("pai", filterMotorista.getPai()));
		}
		if (filterMotorista.getConjuge() != null) {
			searchCriteria.add(Restrictions.eq("conjuge", filterMotorista.getConjuge()));
		}
		if (filterMotorista.getReferenciaPessoal1() != null) {
			searchCriteria.add(Restrictions.eq("referenciaPessoal1", filterMotorista.getReferenciaPessoal1()));
		}
		if (filterMotorista.getFoneReferencia1() != null) {
			searchCriteria.add(Restrictions.eq("foneReferencia1", filterMotorista.getFoneReferencia1()));
		}
		if (filterMotorista.getParentescoReferencia1() != null) {
			searchCriteria.add(Restrictions.eq("parentescoReferencia1", filterMotorista.getParentescoReferencia1()));
		}
		if (filterMotorista.getReferenciaPessoal2() != null) {
			searchCriteria.add(Restrictions.eq("referenciaPessoal2", filterMotorista.getReferenciaPessoal2()));
		}
		if (filterMotorista.getFoneReferencia2() != null) {
			searchCriteria.add(Restrictions.eq("foneReferencia2", filterMotorista.getFoneReferencia2()));
		}
		if (filterMotorista.getParentescoReferencia2() != null) {
			searchCriteria.add(Restrictions.eq("parentescoReferencia2", filterMotorista.getParentescoReferencia2()));
		}
		if (filterMotorista.getMarcaVeiculo() != null) {
			searchCriteria.add(Restrictions.eq("marcaVeiculo", filterMotorista.getMarcaVeiculo()));
		}
		if (filterMotorista.getModeloVeiculo() != null) {
			searchCriteria.add(Restrictions.eq("modeloVeiculo", filterMotorista.getModeloVeiculo()));
		}
		if (filterMotorista.getPlacaVeiculo() != null) {
			searchCriteria.add(Restrictions.eq("placaVeiculo", filterMotorista.getPlacaVeiculo()));
		}
		if (filterMotorista.getCorVeiculo() != null) {
			searchCriteria.add(Restrictions.eq("corVeiculo", filterMotorista.getCorVeiculo()));
		}
		if (filterMotorista.getAnoVeiculo() != null) {
			searchCriteria.add(Restrictions.eq("anoVeiculo", filterMotorista.getAnoVeiculo()));
		}
		if (filterMotorista.getRenavam() != null) {
			searchCriteria.add(Restrictions.eq("renavam", filterMotorista.getRenavam()));
		}
		if (filterMotorista.getChassi() != null) {
			searchCriteria.add(Restrictions.eq("chassi", filterMotorista.getChassi()));
		}
		if (filterMotorista.getCategoria() != null) {
			searchCriteria.add(Restrictions.eq("categoria", filterMotorista.getCategoria()));
		}
		if (filterMotorista.getImei() != null) {
			searchCriteria.add(Restrictions.eq("imei", filterMotorista.getImei()));
		}
		if (filterMotorista.getCartaoCredito() != null) {
			searchCriteria.add(Restrictions.eq("cartaoCredito", filterMotorista.getCartaoCredito()));
		}
		if (filterMotorista.getCartaoDebito() != null) {
			searchCriteria.add(Restrictions.eq("cartaoDebito", filterMotorista.getCartaoDebito()));
		}
		if (filterMotorista.getVoucher() != null) {
			searchCriteria.add(Restrictions.eq("voucher", filterMotorista.getVoucher()));
		}
		if (filterMotorista.getEticket() != null) {
			searchCriteria.add(Restrictions.eq("eticket", filterMotorista.getEticket()));
		}
		if (filterMotorista.getUsuarioCadastro() != null) {
			searchCriteria.add(Restrictions.eq("usuarioCadastro", filterMotorista.getUsuarioCadastro()));
		}
		if (filterMotorista.getDataCadastro() != null) {
			searchCriteria.add(Restrictions.eq("dataCadastro", filterMotorista.getDataCadastro()));
		}
		if (filterMotorista.getSenha() != null) {
			searchCriteria.add(Restrictions.eq("senha", filterMotorista.getSenha()));
		}
		if (filterMotorista.getPadronizada() != null) {
			searchCriteria.add(Restrictions.eq("padronizada", filterMotorista.getPadronizada()));
		}
		if (filterMotorista.getBau() != null) {
			searchCriteria.add(Restrictions.eq("bau", filterMotorista.getBau()));
		}
		if (filterMotorista.getMotoGrande() != null) {
			searchCriteria.add(Restrictions.eq("motoGrande", filterMotorista.getMotoGrande()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
