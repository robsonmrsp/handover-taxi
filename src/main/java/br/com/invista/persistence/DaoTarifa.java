package br.com.invista.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;
import br.com.invista.model.Tarifa;
import br.com.invista.model.filter.FilterTarifa;

/**
 * generated: 16/10/2016 15:27:08
 **/

@Named
@SuppressWarnings("rawtypes")
public class DaoTarifa extends AccessibleHibernateDao<Tarifa> {
	private static final Logger LOGGER = Logger.getLogger(DaoTarifa.class);

	public DaoTarifa() {
		super(Tarifa.class);
	}

	public Tarifa findByOrigemDestino(String origem, String destino) {
		Tarifa tarifa = null;
		try {
			Criteria buscaTarifa = criteria();
			buscaTarifa.add(Restrictions.eq("bairroOrigem", origem));
			buscaTarifa.add(Restrictions.eq("bairroDestino", destino));
			tarifa = (Tarifa) buscaTarifa.setMaxResults(1).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("Erro ao obter registro pela origem e destino," + origem + " - " + destino, e);
		}
		return tarifa;
	}

	@Override
	public Pagination<Tarifa> getAll(PaginationParams paginationParams) {
		FilterTarifa filterTarifa = (FilterTarifa) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterTarifa.getBairroOrigem() != null) {
			searchCriteria.add(Restrictions.ilike("bairroOrigem", filterTarifa.getBairroOrigem(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("bairroOrigem", filterTarifa.getBairroOrigem(), MatchMode.ANYWHERE));
		}
		if (filterTarifa.getBairroDestino() != null) {
			searchCriteria
					.add(Restrictions.ilike("bairroDestino", filterTarifa.getBairroDestino(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("bairroDestino", filterTarifa.getBairroDestino(), MatchMode.ANYWHERE));
		}
		if (filterTarifa.getValor() != null) {
			searchCriteria.add(Restrictions.eq("valor", filterTarifa.getValor()));
			countCriteria.add(Restrictions.eq("valor", filterTarifa.getValor()));
		}

		return new Paginator<Tarifa>(searchCriteria, countCriteria).paginate(paginationParams);
	}

	public List<Tarifa> filter(PaginationParams paginationParams) {
		List<Tarifa> list = new ArrayList<Tarifa>();
		FilterTarifa filterTarifa = (FilterTarifa) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterTarifa.getBairroOrigem() != null) {
			searchCriteria.add(Restrictions.eq("bairroOrigem", filterTarifa.getBairroOrigem()));
		}
		if (filterTarifa.getBairroDestino() != null) {
			searchCriteria.add(Restrictions.eq("bairroDestino", filterTarifa.getBairroDestino()));
		}
		if (filterTarifa.getValor() != null) {
			searchCriteria.add(Restrictions.eq("valor", filterTarifa.getValor()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
