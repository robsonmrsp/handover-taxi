package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.Punicao;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterPunicao;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.Punicao;
/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoPunicao extends AccessibleHibernateDao<Punicao> {
	private static final Logger LOGGER = Logger.getLogger(DaoPunicao.class);

	public DaoPunicao() {
		super(Punicao.class);
	}

	@Override
	public Pagination<Punicao> getAll(PaginationParams paginationParams) {
		FilterPunicao filterPunicao = (FilterPunicao) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterPunicao.getMotivo() != null) {
			searchCriteria.add(Restrictions.ilike("motivo", filterPunicao.getMotivo(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("motivo", filterPunicao.getMotivo(), MatchMode.ANYWHERE));
		}
		if (filterPunicao.getDataInicio() != null) {
			searchCriteria.add(Restrictions.eq("dataInicio", filterPunicao.getDataInicio()));
			countCriteria.add(Restrictions.eq("dataInicio", filterPunicao.getDataInicio()));
		}				
		if (filterPunicao.getDataFim() != null) {
			searchCriteria.add(Restrictions.eq("dataFim", filterPunicao.getDataFim()));
			countCriteria.add(Restrictions.eq("dataFim", filterPunicao.getDataFim()));
		}				
		if (filterPunicao.getQuantidadetempo() != null) {
			searchCriteria.add(Restrictions.eq("quantidadetempo", filterPunicao.getQuantidadetempo()));
			countCriteria.add(Restrictions.eq("quantidadetempo", filterPunicao.getQuantidadetempo()));
		}				
		if (filterPunicao.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			countCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterPunicao.getMotorista()));
			countCriteria.add(Restrictions.eq("motorista_.id", filterPunicao.getMotorista()));
		}

		return new Paginator<Punicao>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<Punicao> filter(PaginationParams paginationParams) {
		List<Punicao> list = new ArrayList<Punicao>();
		FilterPunicao filterPunicao = (FilterPunicao) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterPunicao.getMotivo() != null) {
			searchCriteria.add(Restrictions.eq("motivo", filterPunicao.getMotivo()));
		}
		if (filterPunicao.getDataInicio() != null) {
			searchCriteria.add(Restrictions.eq("dataInicio", filterPunicao.getDataInicio()));
		}
		if (filterPunicao.getDataFim() != null) {
			searchCriteria.add(Restrictions.eq("dataFim", filterPunicao.getDataFim()));
		}
		if (filterPunicao.getQuantidadetempo() != null) {
			searchCriteria.add(Restrictions.eq("quantidadetempo", filterPunicao.getQuantidadetempo()));
		}
		if (filterPunicao.getMotorista() != null) {
			searchCriteria.createAlias("motorista", "motorista_");
			searchCriteria.add(Restrictions.eq("motorista_.id", filterPunicao.getMotorista()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
