package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.Area;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterArea;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.Area;
/**
*  generated: 16/10/2016 15:27:07
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoArea extends AccessibleHibernateDao<Area> {
	private static final Logger LOGGER = Logger.getLogger(DaoArea.class);

	public DaoArea() {
		super(Area.class);
	}

	@Override
	public Pagination<Area> getAll(PaginationParams paginationParams) {
		FilterArea filterArea = (FilterArea) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterArea.getDescricao() != null) {
			searchCriteria.add(Restrictions.ilike("descricao", filterArea.getDescricao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("descricao", filterArea.getDescricao(), MatchMode.ANYWHERE));
		}
		if (filterArea.getCoordenadas() != null) {
			searchCriteria.add(Restrictions.ilike("coordenadas", filterArea.getCoordenadas(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("coordenadas", filterArea.getCoordenadas(), MatchMode.ANYWHERE));
		}

		return new Paginator<Area>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<Area> filter(PaginationParams paginationParams) {
		List<Area> list = new ArrayList<Area>();
		FilterArea filterArea = (FilterArea) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterArea.getDescricao() != null) {
			searchCriteria.add(Restrictions.eq("descricao", filterArea.getDescricao()));
		}
		if (filterArea.getCoordenadas() != null) {
			searchCriteria.add(Restrictions.eq("coordenadas", filterArea.getCoordenadas()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
