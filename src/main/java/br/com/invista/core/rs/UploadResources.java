package br.com.invista.core.rs;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import javax.activation.DataHandler;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

import br.com.invista.core.json.JsonError;
import br.com.invista.core.utils.ImageUtils;
import br.com.invista.core.utils.Util;
import br.com.invista.model.Company;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlobDirectory;
import com.microsoft.azure.storage.blob.CloudBlockBlob;

@Path("/crud/uploads/")
public class UploadResources {

	private static final int THUMBNAIL_WIDTH = 240;
	public static final String storageConnectionString = "DefaultEndpointsProtocol=http;"
			+ "AccountName=portalvhdsjg19kbq1q36k1;"
			+ "AccountKey=x3cW/427NHEp3OtXtuSYMTM2IgvhuC00JkASFPHJx6YcXFXEu9qvx2YWxm4cuRUsL7uwtN5bO7CK0P4hSu8wZg==";
	public static final String urlAzure = "http://portalvhdsjg19kbq1q36k1.blob.core.windows.net/";
	
	
	@POST
	@Path("/file/{tenant}/{folder}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadFile(@PathParam("tenant") String tenant, @PathParam("folder") String folders, List<Attachment> attachments, @Context HttpServletRequest httpServletRequest) {
		DataUpload dataUpload = null;
		String uploadFolder1 = System.getProperty( "catalina.base" );
		System.out.println("BBB--------------------------------------------"+uploadFolder1);
		String uploadFolder = httpServletRequest.getServletContext().getRealPath("/uploads");
		System.out.println("AAA--------------------------------------------"+uploadFolder);
		for (Attachment attr : attachments) {
			DataHandler handler = attr.getDataHandler();
			String nameFile = "";
			try {
				InputStream stream = handler.getInputStream();//Mandar esse cara para o azure

				String contentType = handler.getContentType();
				nameFile = System.currentTimeMillis() + "_" + Util.removeNonUnicodeCharAndSpaces(handler.getName());
				if (nameFile.length() <= 60){
					String folder = uploadFolder + File.separator;
					ByteArrayOutputStream bos = null;
					if (contentType != null) {
						if (contentType.toLowerCase().contains("image")) {
							//BufferedImage image = ImageIO.read(stream);
							
							//Manda a imagem para o Azure
							CloudStorageAccount account = CloudStorageAccount.parse(storageConnectionString);
							CloudBlobClient serviceClient = account.createCloudBlobClient();
	
							// Container name must be lower case.
							CloudBlobContainer container = serviceClient
									.getContainerReference(tenant);
							container.createIfNotExists();
							
							CloudBlobDirectory folder2 = container.getDirectoryReference(folders);
	
							// Upload an image file.
							CloudBlockBlob blob = folder2.getBlockBlobReference(nameFile);
							blob.upload(stream, stream.available());
							
							/*BufferedImage image = ImageIO.read(stream);
							ImageUtils.createThumbnail(image, THUMBNAIL_WIDTH, contentType, folder, nameFile);
	
							ImageUtils.saveImage(image, contentType, folder, nameFile);*/
						} else {
							bos = new ByteArrayOutputStream();
							int read = 0;
							byte[] bytes = new byte[1024];
							while ((read = stream.read(bytes)) != -1) {
								bos.write(bytes);
							}
							FileOutputStream fileOutputStream = new FileOutputStream(new File(folder + nameFile));
							fileOutputStream.write(bos.toByteArray());
							fileOutputStream.close();
							bos.close();
						}
						//dataUpload = new DataUpload(contentType, "uploads/" + nameFile);
						dataUpload = new DataUpload(contentType, urlAzure + tenant + "/" + folders + "/" + nameFile);
						String teste = "";
					}
				}else { 
					return Response.serverError().entity(new JsonError("O tamanho do nome da imagem não pode exceder 60 caracteres.", nameFile)).build();
				}
				stream.close();
			} catch (Exception e) {
				e.printStackTrace();
				return Response.serverError().entity(new JsonError("Problema durante upload da mídia [ " + nameFile + " ] error [" + e.getMessage() + "]", nameFile)).build();
			}
		}
		return Response.ok(dataUpload).build();
	}
}
