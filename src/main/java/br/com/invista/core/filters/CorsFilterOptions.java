package br.com.invista.core.filters;
import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;



public class CorsFilterOptions extends OncePerRequestFilter  {
   static final String ORIGIN = "Origin";

   @Override
   protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
           throws ServletException, IOException {
       
       //System.out.println("teste");
       
       
       if (request.getMethod().equals("OPTIONS")) {
    	   response.setHeader("Access-Control-Allow-Origin", "*");
           response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
           try {
               response.getWriter().print("OK");
               response.flushBuffer();
           } catch (IOException e) {
               e.printStackTrace();
           }
       } else {
           filterChain.doFilter(request, response);
       }
       
   }
}
