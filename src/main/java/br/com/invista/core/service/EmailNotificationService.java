package br.com.invista.core.service;


public interface EmailNotificationService {
	
	public void sendEmail(String emailOrigem, String emailDestino, String titulo, String corpo) throws Exception;
	
}
