package br.com.invista.core.service;

import java.util.Properties;

import javax.inject.Named;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.transaction.annotation.Transactional;

@Named
@Transactional
public class EmailNotificationServiceImp implements EmailNotificationService{
	
	public void sendEmail(String emailOrigem, String emailDestino, String titulo, String corpo) throws Exception{
		 Properties props = new Properties();
		 configEmail(props);
		 Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
		 protected PasswordAuthentication getPasswordAuthentication() {
		 //aqui os dados da sua conta do gmail
		 String login = "invistatester"; //n�o precisa colocar @gmail.com
		 String password = "testandoinvista";
		 return new PasswordAuthentication(login,password);
		 }
		 });
		 
		 try {
		 
		 email(emailOrigem, emailDestino, titulo, corpo, session);
		 
		 
		 } catch (MessagingException e) {
		 throw new Exception(e);
		 }
	 }
	 
	
	 private void email(String emailOrigem, String emailDestino, String titulo, String corpo, Session session) throws MessagingException, AddressException {
		 MimeMessage message = new MimeMessage(session);
		 String fromEmail = emailOrigem;//aqui o email que vai enviar as informa��es exemplo: noreply@suaempresa.com
		 String subject = titulo;
		 String bodyEmail = corpo;
		 String toEmail = emailDestino; //o email de destino que vem da tela;
		 message.setFrom(new InternetAddress(fromEmail));
		 message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(toEmail));
		 message.setSubject(subject);
		 message.setText(bodyEmail, "utf-8", "html");
		 Transport.send(message);
	 }
	 //configura��es do Gmail
	 private void configEmail(Properties props) {
		 props.put("mail.smtp.host", "smtp.gmail.com");
		 props.put("mail.smtp.socketFactory.port", "465");
		 props.put("mail.smtp.socketFactory.class",
		 "javax.net.ssl.SSLSocketFactory");
		 props.put("mail.smtp.auth", "true");
		 props.put("mail.smtp.port", "465");
	 }
}
