
package br.com.invista.core.utils;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.invista.json.JsonArea;
import br.com.invista.json.JsonAtendente;
import br.com.invista.json.JsonBairro;
import br.com.invista.json.JsonCentroCusto;
import br.com.invista.json.JsonCep;
import br.com.invista.json.JsonCidade;
import br.com.invista.json.JsonCliente;
import br.com.invista.json.JsonClienteEnderecoCoord;
import br.com.invista.json.JsonConsumidorCorrida;
import br.com.invista.json.JsonContato;
import br.com.invista.json.JsonCorrida;
import br.com.invista.json.JsonDistanciaMotoCorrida;
import br.com.invista.json.JsonEmpresa;
import br.com.invista.json.JsonEnderecoCorrida;
import br.com.invista.json.JsonEnderecoFavorito;
import br.com.invista.json.JsonEstado;
import br.com.invista.json.JsonEvento;
import br.com.invista.json.JsonFaixaVoucher;
import br.com.invista.json.JsonHdUsuario;
import br.com.invista.json.JsonInformacaoTarifaAdicional;
import br.com.invista.json.JsonItem;
import br.com.invista.json.JsonItemType;
import br.com.invista.json.JsonMensagem;
import br.com.invista.json.JsonMensagemMotorista;
import br.com.invista.json.JsonMotivoPunicao;
import br.com.invista.json.JsonMotorista;
import br.com.invista.json.JsonMotoristaBloqueado;
import br.com.invista.json.JsonOperation;
import br.com.invista.json.JsonPais;
import br.com.invista.json.JsonPermission;
import br.com.invista.json.JsonPlantao;
import br.com.invista.json.JsonPontosArea;
import br.com.invista.json.JsonPunicao;
import br.com.invista.json.JsonRole;
import br.com.invista.json.JsonSession;
import br.com.invista.json.JsonSocio;
import br.com.invista.json.JsonStatusTracker;
import br.com.invista.json.JsonTarifa;
import br.com.invista.json.JsonTarifaAdicional;
import br.com.invista.json.JsonTelefoneFavorito;
import br.com.invista.json.JsonUser;
import br.com.invista.json.JsonVisitaTracker;
import br.com.invista.model.Area;
import br.com.invista.model.Atendente;
import br.com.invista.model.Bairro;
import br.com.invista.model.CentroCusto;
import br.com.invista.model.Cep;
import br.com.invista.model.Cidade;
import br.com.invista.model.Cliente;
import br.com.invista.model.ClienteEnderecoCoord;
import br.com.invista.model.Contato;
import br.com.invista.model.Corrida;
import br.com.invista.model.DistanciaMotoCorrida;
import br.com.invista.model.Empresa;
import br.com.invista.model.EnderecoCorrida;
import br.com.invista.model.EnderecoFavorito;
import br.com.invista.model.Estado;
import br.com.invista.model.Evento;
import br.com.invista.model.FaixaVoucher;
import br.com.invista.model.HdUsuario;
import br.com.invista.model.InformacaoTarifaAdicional;
import br.com.invista.model.Item;
import br.com.invista.model.ItemType;
import br.com.invista.model.Mensagem;
import br.com.invista.model.MensagemMotorista;
import br.com.invista.model.MotivoPunicao;
import br.com.invista.model.Motorista;
import br.com.invista.model.MotoristaBloqueado;
import br.com.invista.model.Operation;
import br.com.invista.model.Pais;
import br.com.invista.model.Permission;
import br.com.invista.model.Plantao;
import br.com.invista.model.PontosArea;
import br.com.invista.model.Punicao;
import br.com.invista.model.Role;
import br.com.invista.model.Session;
import br.com.invista.model.Socio;
import br.com.invista.model.StatusTracker;
import br.com.invista.model.Tarifa;
import br.com.invista.model.TarifaAdicional;
import br.com.invista.model.TelefoneFavorito;
import br.com.invista.model.User;
import br.com.invista.model.VisitaTracker;
import br.com.invista.rs.ConsumidorCorrida;

public class Parser {

	private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy");
	private static final DateTimeFormatter HOUR_FORMAT = DateTimeFormat.forPattern("HH:mm");

	public static String getHourAsString(LocalDateTime date) {
		String format = "";
		try {
			format = HOUR_FORMAT.print(date);
		} catch (Exception e) {
			format = "00:00";
		}
		return format;
	}

	public static String getDateTimeAsString(LocalDateTime date) {
		String format = "";
		try {
			format = DATE_TIME_FORMAT.print(date);
		} catch (Exception e) {
			format = DATE_TIME_FORMAT.print(new DateTime());
		}
		return format;
	}

	public static String getDateAsString(LocalDateTime date) {
		String format = "";
		try {
			format = DATE_FORMAT.print(date);
		} catch (Exception e) {
			format = DATE_FORMAT.print(new DateTime());
		}
		return format;
	}

	//
	private static DateTime getHour(String date) {
		if (!date.isEmpty()) {
			try {
				return HOUR_FORMAT.parseDateTime(date);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private static LocalDateTime getDate(String date) {
		if (!date.isEmpty()) {
			try {
				LocalDateTime dateTime = DATE_FORMAT.parseLocalDateTime(date);
				return dateTime;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private static LocalDateTime getDateTime(String date) {
		if (!date.isEmpty()) {
			try {
				LocalDateTime dateTime = DATE_TIME_FORMAT.parseLocalDateTime(date);
				return dateTime;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	// converte de entidade para json --------------------
	private static JsonArea toBasicJson(Area area) {
		JsonArea jsonArea = new JsonArea();
		applyBasicJsonValues(jsonArea, area);
		return jsonArea;
	}

	private static Area toBasicEntity(JsonArea jsonArea) {
		Area area = new Area();
		applyBasicEntityValues(area, jsonArea);
		return area;
	}

	private static void applyBasicJsonValues(JsonArea jsonArea, Area area) {
		jsonArea.setId(area.getId());
		jsonArea.setOrdem(area.getOrdem());
		jsonArea.setDescricao(area.getDescricao());
		jsonArea.setCoordenadas(area.getCoordenadas());
	}

	private static void applyBasicEntityValues(Area area, JsonArea jsonArea) {
		area.setId(jsonArea.getId());
		area.setOrdem(jsonArea.getOrdem());
		area.setDescricao(jsonArea.getDescricao());
		area.setCoordenadas(jsonArea.getCoordenadas());
	}

	public static JsonArea toJson(Area area) {
		JsonArea jsonArea = new JsonArea();

		applyBasicJsonValues(jsonArea, area);

		List<PontosArea> listPontosAreas = area.getPontosAreas();
		if (listPontosAreas != null) {
			for (PontosArea loopPontosArea : listPontosAreas) {
				jsonArea.getPontosAreas().add(toBasicJson(loopPontosArea));
			}
		}
		return jsonArea;
	}

	public static Area apply(Area area, JsonArea jsonArea) {

		if (area == null)
			area = new Area();

		applyBasicEntityValues(area, jsonArea);

		ArrayList<JsonPontosArea> listPontosAreas = jsonArea.getPontosAreas();
		if (listPontosAreas != null) {
			for (JsonPontosArea loopJsonPontosArea : listPontosAreas) {
				area.addPontosAreas(toBasicEntity(loopJsonPontosArea));
			}
		}

		return area;

	}

	public static Area toEntity(JsonArea jsonArea) {
		Area area = new Area();

		return apply(area, jsonArea);
	}

	public static List<JsonArea> toListJsonAreas(List<Area> all) {
		List<JsonArea> jsonAreas = new ArrayList<JsonArea>();
		for (Area area : all) {
			jsonAreas.add(toJson(area));
		}
		return jsonAreas;
	}

	// converte de entidade para json --------------------
	private static JsonAtendente toBasicJson(Atendente atendente) {
		JsonAtendente jsonAtendente = new JsonAtendente();
		applyBasicJsonValues(jsonAtendente, atendente);
		return jsonAtendente;
	}

	private static Atendente toBasicEntity(JsonAtendente jsonAtendente) {
		Atendente atendente = new Atendente();
		applyBasicEntityValues(atendente, jsonAtendente);
		return atendente;
	}

	private static void applyBasicJsonValues(JsonAtendente jsonAtendente, Atendente atendente) {
		jsonAtendente.setId(atendente.getId());
		jsonAtendente.setNome(atendente.getNome());
		jsonAtendente.setSituacao(atendente.getSituacao());
	}

	private static void applyBasicEntityValues(Atendente atendente, JsonAtendente jsonAtendente) {
		atendente.setId(jsonAtendente.getId());
		atendente.setNome(jsonAtendente.getNome());
		atendente.setSituacao(jsonAtendente.getSituacao());
	}

	public static JsonAtendente toJson(Atendente atendente) {
		JsonAtendente jsonAtendente = new JsonAtendente();

		applyBasicJsonValues(jsonAtendente, atendente);

		List<Corrida> listCorridas = atendente.getCorridas();
		if (listCorridas != null) {
			for (Corrida loopCorrida : listCorridas) {
				jsonAtendente.getCorridas().add(toBasicJson(loopCorrida));
			}
		}
		return jsonAtendente;
	}

	public static Atendente apply(Atendente atendente, JsonAtendente jsonAtendente) {

		if (atendente == null)
			atendente = new Atendente();

		applyBasicEntityValues(atendente, jsonAtendente);

		ArrayList<JsonCorrida> listCorridas = jsonAtendente.getCorridas();
		if (listCorridas != null) {
			for (JsonCorrida loopJsonCorrida : listCorridas) {
				atendente.addCorridas(toBasicEntity(loopJsonCorrida));
			}
		}

		return atendente;

	}

	public static Atendente toEntity(JsonAtendente jsonAtendente) {
		Atendente atendente = new Atendente();

		return apply(atendente, jsonAtendente);
	}

	public static List<JsonAtendente> toListJsonAtendentes(List<Atendente> all) {
		List<JsonAtendente> jsonAtendentes = new ArrayList<JsonAtendente>();
		for (Atendente atendente : all) {
			jsonAtendentes.add(toJson(atendente));
		}
		return jsonAtendentes;
	}

	// converte de entidade para json --------------------
	private static JsonCentroCusto toBasicJson(CentroCusto centroCusto) {
		JsonCentroCusto jsonCentroCusto = new JsonCentroCusto();
		applyBasicJsonValues(jsonCentroCusto, centroCusto);
		return jsonCentroCusto;
	}

	private static CentroCusto toBasicEntity(JsonCentroCusto jsonCentroCusto) {
		CentroCusto centroCusto = new CentroCusto();
		applyBasicEntityValues(centroCusto, jsonCentroCusto);
		return centroCusto;
	}

	private static void applyBasicJsonValues(JsonCentroCusto jsonCentroCusto, CentroCusto centroCusto) {
		jsonCentroCusto.setId(centroCusto.getId());
		jsonCentroCusto.setDescricao(centroCusto.getDescricao());
		jsonCentroCusto.setValorLimite(centroCusto.getValorLimite());
		jsonCentroCusto.setValorAindaDisponivel(centroCusto.getValorAindaDisponivel());
		jsonCentroCusto.setObservacao(centroCusto.getObservacao());
		jsonCentroCusto.setInativo(centroCusto.getInativo());
	}

	private static void applyBasicEntityValues(CentroCusto centroCusto, JsonCentroCusto jsonCentroCusto) {
		centroCusto.setId(jsonCentroCusto.getId());
		centroCusto.setDescricao(jsonCentroCusto.getDescricao());
		centroCusto.setValorLimite(jsonCentroCusto.getValorLimite());
		centroCusto.setValorAindaDisponivel(jsonCentroCusto.getValorAindaDisponivel());
		centroCusto.setObservacao(jsonCentroCusto.getObservacao());
		centroCusto.setInativo(jsonCentroCusto.getInativo());
	}

	public static JsonCentroCusto toJson(CentroCusto centroCusto) {
		JsonCentroCusto jsonCentroCusto = new JsonCentroCusto();

		applyBasicJsonValues(jsonCentroCusto, centroCusto);

		List<Cliente> listClientes = centroCusto.getClientes();
		if (listClientes != null) {
			for (Cliente loopCliente : listClientes) {
				jsonCentroCusto.getClientes().add(toBasicJson(loopCliente));
			}
		}
		Empresa empresa_ = centroCusto.getEmpresa();
		if (empresa_ != null) {
			jsonCentroCusto.setEmpresa(toBasicJson(empresa_));
		}
		return jsonCentroCusto;
	}

	public static CentroCusto apply(CentroCusto centroCusto, JsonCentroCusto jsonCentroCusto) {

		if (centroCusto == null)
			centroCusto = new CentroCusto();

		applyBasicEntityValues(centroCusto, jsonCentroCusto);

		ArrayList<JsonCliente> listClientes = jsonCentroCusto.getClientes();
		if (listClientes != null) {
			for (JsonCliente loopJsonCliente : listClientes) {
				centroCusto.addClientes(toBasicEntity(loopJsonCliente));
			}
		}

		JsonEmpresa empresa_ = jsonCentroCusto.getEmpresa();
		if (empresa_ != null) {
			centroCusto.setEmpresa(toEntity(empresa_));
		}
		return centroCusto;

	}

	public static CentroCusto toEntity(JsonCentroCusto jsonCentroCusto) {
		CentroCusto centroCusto = new CentroCusto();

		return apply(centroCusto, jsonCentroCusto);
	}

	public static List<JsonCentroCusto> toListJsonCentroCustos(List<CentroCusto> all) {
		List<JsonCentroCusto> jsonCentroCustos = new ArrayList<JsonCentroCusto>();
		for (CentroCusto centroCusto : all) {
			jsonCentroCustos.add(toJson(centroCusto));
		}
		return jsonCentroCustos;
	}

	// converte de entidade para json --------------------
	private static JsonCliente toBasicJson(Cliente cliente) {
		JsonCliente jsonCliente = new JsonCliente();
		applyBasicJsonValues(jsonCliente, cliente);
		return jsonCliente;
	}

	private static Cliente toBasicEntity(JsonCliente jsonCliente) {
		Cliente cliente = new Cliente();
		applyBasicEntityValues(cliente, jsonCliente);
		return cliente;
	}

	private static void applyBasicJsonValues(JsonCliente jsonCliente, Cliente cliente) {
		jsonCliente.setId(cliente.getId());
		jsonCliente.setNome(cliente.getNome());
		jsonCliente.setEmail(cliente.getEmail());
		jsonCliente.setUsername(cliente.getUsername());
		jsonCliente.setSenha(cliente.getSenha());
		jsonCliente.setTipo(cliente.getTipo());
		jsonCliente.setTelefone(cliente.getTelefone());
		jsonCliente.setInativo(cliente.getInativo());
		jsonCliente.setUsuarioCadastro(cliente.getUsuarioCadastro());
		jsonCliente.setDataCadastro(DateUtil.localDateAsString(cliente.getDataCadastro()));
		jsonCliente.setMatricula(cliente.getMatricula());
		jsonCliente.setLimiteMensal(cliente.getLimiteMensal());
		jsonCliente.setCnpjCpf(cliente.getCnpjCpf());
		jsonCliente.setCnpj(cliente.getCnpj());
		jsonCliente.setCpf(cliente.getCpf());
		jsonCliente.setAutorizaEticket(cliente.getAutorizaEticket());
		jsonCliente.setIdFacebook(cliente.getIdFacebook());
		jsonCliente.setEmailFacebook(cliente.getEmailFacebook());
		jsonCliente.setObservacao(cliente.getObservacao());
	}

	private static void applyBasicEntityValues(Cliente cliente, JsonCliente jsonCliente) {
		cliente.setId(jsonCliente.getId());
		cliente.setNome(jsonCliente.getNome());
		cliente.setEmail(jsonCliente.getEmail());
		cliente.setUsername(jsonCliente.getUsername());
		cliente.setSenha(jsonCliente.getSenha());
		cliente.setTelefone(jsonCliente.getTelefone());
		cliente.setTipo(jsonCliente.getTipo());
		cliente.setInativo(jsonCliente.getInativo());
		cliente.setUsuarioCadastro(jsonCliente.getUsuarioCadastro());
		cliente.setDataCadastro(DateUtil.stringAsLocalDate(jsonCliente.getDataCadastro()));
		cliente.setMatricula(jsonCliente.getMatricula());
		cliente.setLimiteMensal(jsonCliente.getLimiteMensal());
		cliente.setCnpjCpf(jsonCliente.getCnpjCpf());
		cliente.setCnpj(jsonCliente.getCnpj());
		cliente.setCpf(jsonCliente.getCpf());
		cliente.setAutorizaEticket(jsonCliente.getAutorizaEticket());
		cliente.setIdFacebook(jsonCliente.getIdFacebook());
		cliente.setEmailFacebook(jsonCliente.getEmailFacebook());
		cliente.setObservacao(jsonCliente.getObservacao());
	}

	public static JsonCliente toJson(Cliente cliente) {
		JsonCliente jsonCliente = new JsonCliente();

		applyBasicJsonValues(jsonCliente, cliente);

		List<EnderecoFavorito> listEnderecosFavoritos = cliente.getEnderecosFavoritos();
		if (listEnderecosFavoritos != null) {
			for (EnderecoFavorito loopEnderecoFavorito : listEnderecosFavoritos) {
				jsonCliente.getEnderecosFavoritos().add(toBasicJson(loopEnderecoFavorito));
			}
		}
		List<TelefoneFavorito> listTelefonesFavoritos = cliente.getTelefonesFavoritos();
		if (listTelefonesFavoritos != null) {
			for (TelefoneFavorito loopTelefoneFavorito : listTelefonesFavoritos) {
				jsonCliente.getTelefonesFavoritos().add(toBasicJson(loopTelefoneFavorito));
			}
		}
		List<MotoristaBloqueado> listMotoristaBloqueados = cliente.getMotoristaBloqueados();
		if (listMotoristaBloqueados != null) {
			for (MotoristaBloqueado loopMotoristaBloqueado : listMotoristaBloqueados) {
				jsonCliente.getMotoristaBloqueados().add(toBasicJson(loopMotoristaBloqueado));
			}
		}
		Empresa empresa_ = cliente.getEmpresa();
		if (empresa_ != null) {
			jsonCliente.setEmpresa(toBasicJson(empresa_));
		}
		CentroCusto centroCusto_ = cliente.getCentroCusto();
		if (centroCusto_ != null) {
			jsonCliente.setCentroCusto(toJson(centroCusto_));
		}
		return jsonCliente;
	}

	public static Cliente apply(Cliente cliente, JsonCliente jsonCliente) {

		if (cliente == null)
			cliente = new Cliente();

		applyBasicEntityValues(cliente, jsonCliente);

		ArrayList<JsonEnderecoFavorito> listEnderecosFavoritos = jsonCliente.getEnderecosFavoritos();
		if (listEnderecosFavoritos != null) {
			for (JsonEnderecoFavorito loopJsonEnderecoFavoritos : listEnderecosFavoritos) {
				cliente.addEnderecosFavoritos(toBasicEntity(loopJsonEnderecoFavoritos));
			}
		}

		ArrayList<JsonTelefoneFavorito> listTelefonesFavoritos = jsonCliente.getTelefonesFavoritos();
		if (listTelefonesFavoritos != null) {
			for (JsonTelefoneFavorito loopJsonTelefoneFavorito : listTelefonesFavoritos) {
				cliente.addTelefoneFavorito(toBasicEntity(loopJsonTelefoneFavorito));
			}
		}

		ArrayList<JsonMotoristaBloqueado> listMotoristaBloqueados = jsonCliente.getMotoristaBloqueados();
		if (listMotoristaBloqueados != null) {
			for (JsonMotoristaBloqueado loopJsonMotoristaBloqueado : listMotoristaBloqueados) {
				cliente.addMotoristaBloqueados(toBasicEntity(loopJsonMotoristaBloqueado));
			}
		}

		JsonEmpresa empresa_ = jsonCliente.getEmpresa();
		if (empresa_ != null) {
			cliente.setEmpresa(toEntity(empresa_));
		}
		JsonCentroCusto centroCusto_ = jsonCliente.getCentroCusto();
		if (centroCusto_ != null) {
			cliente.setCentroCusto(toEntity(centroCusto_));
		}
		return cliente;

	}

	public static Cliente toEntity(JsonCliente jsonCliente) {
		Cliente cliente = new Cliente();

		return apply(cliente, jsonCliente);
	}

	public static List<JsonCliente> toListJsonClientes(List<Cliente> all) {
		List<JsonCliente> jsonClientes = new ArrayList<JsonCliente>();
		for (Cliente cliente : all) {
			jsonClientes.add(toJson(cliente));
		}
		return jsonClientes;
	}

	// converte de entidade para json --------------------
	private static JsonEnderecoFavorito toBasicJson(EnderecoFavorito enderecoFavorito) {
		JsonEnderecoFavorito jsonEnderecoFavorito = new JsonEnderecoFavorito();
		applyBasicJsonValues(jsonEnderecoFavorito, enderecoFavorito);
		return jsonEnderecoFavorito;
	}

	private static EnderecoFavorito toBasicEntity(JsonEnderecoFavorito jsonEnderecoFavorito) {
		EnderecoFavorito enderecoFavorito = new EnderecoFavorito();
		applyBasicEntityValues(enderecoFavorito, jsonEnderecoFavorito);
		return enderecoFavorito;
	}

	private static void applyBasicJsonValues(JsonEnderecoFavorito jsonEnderecoFavorito, EnderecoFavorito enderecoFavorito) {
		jsonEnderecoFavorito.setId(enderecoFavorito.getId());
		jsonEnderecoFavorito.setEndereco(enderecoFavorito.getEndereco());
		jsonEnderecoFavorito.setNumero(enderecoFavorito.getNumero());
		jsonEnderecoFavorito.setComplemento(enderecoFavorito.getComplemento());
		jsonEnderecoFavorito.setUf(enderecoFavorito.getUf());
		jsonEnderecoFavorito.setCidade(enderecoFavorito.getCidade());
		jsonEnderecoFavorito.setBairro(enderecoFavorito.getBairro());
		jsonEnderecoFavorito.setCep(enderecoFavorito.getCep());
		jsonEnderecoFavorito.setReferencia(enderecoFavorito.getReferencia());
		jsonEnderecoFavorito.setTipo(enderecoFavorito.getTipo());
		jsonEnderecoFavorito.setFavorito(enderecoFavorito.getFavorito());
		jsonEnderecoFavorito.setLatitude(enderecoFavorito.getLatitude());
		jsonEnderecoFavorito.setLongitude(enderecoFavorito.getLongitude());
		jsonEnderecoFavorito.setEnderecoFormatado(enderecoFavorito.getEnderecoFormatado());
	}

	private static void applyBasicEntityValues(EnderecoFavorito enderecoFavorito, JsonEnderecoFavorito jsonEnderecoFavorito) {
		enderecoFavorito.setId(jsonEnderecoFavorito.getId());
		enderecoFavorito.setEndereco(jsonEnderecoFavorito.getEndereco());
		enderecoFavorito.setNumero(jsonEnderecoFavorito.getNumero());
		enderecoFavorito.setComplemento(jsonEnderecoFavorito.getComplemento());
		enderecoFavorito.setUf(jsonEnderecoFavorito.getUf());
		enderecoFavorito.setCidade(jsonEnderecoFavorito.getCidade());
		enderecoFavorito.setBairro(jsonEnderecoFavorito.getBairro());
		enderecoFavorito.setCep(jsonEnderecoFavorito.getCep());
		enderecoFavorito.setReferencia(jsonEnderecoFavorito.getReferencia());
		enderecoFavorito.setTipo(jsonEnderecoFavorito.getTipo());
		enderecoFavorito.setFavorito(jsonEnderecoFavorito.getFavorito());
		enderecoFavorito.setLatitude(jsonEnderecoFavorito.getLatitude());
		enderecoFavorito.setLongitude(jsonEnderecoFavorito.getLongitude());
		enderecoFavorito.setEnderecoFormatado(jsonEnderecoFavorito.getEnderecoFormatado());
	}

	public static JsonEnderecoFavorito toJson(EnderecoFavorito enderecoFavorito) {
		JsonEnderecoFavorito jsonEnderecoFavorito = new JsonEnderecoFavorito();

		applyBasicJsonValues(jsonEnderecoFavorito, enderecoFavorito);

		List<ClienteEnderecoCoord> listClienteEnderecoCoords = enderecoFavorito.getClienteEnderecoCoords();
		if (listClienteEnderecoCoords != null) {
			for (ClienteEnderecoCoord loopClienteEnderecoCoord : listClienteEnderecoCoords) {
				jsonEnderecoFavorito.getClienteEnderecoCoords().add(toBasicJson(loopClienteEnderecoCoord));
			}
		}
		Cliente cliente_ = enderecoFavorito.getCliente();
		if (cliente_ != null) {
			jsonEnderecoFavorito.setCliente(toBasicJson(cliente_));
		}

		Empresa empresa_ = enderecoFavorito.getEmpresa();
		if (empresa_ != null) {
			jsonEnderecoFavorito.setEmpresa(toBasicJson(empresa_));
		}
		return jsonEnderecoFavorito;
	}

	public static EnderecoFavorito apply(EnderecoFavorito enderecoFavorito, JsonEnderecoFavorito jsonEnderecoFavorito) {

		if (enderecoFavorito == null)
			enderecoFavorito = new EnderecoFavorito();

		applyBasicEntityValues(enderecoFavorito, jsonEnderecoFavorito);

		ArrayList<JsonClienteEnderecoCoord> listClienteEnderecoCoords = jsonEnderecoFavorito.getClienteEnderecoCoords();
		if (listClienteEnderecoCoords != null) {
			for (JsonClienteEnderecoCoord loopJsonClienteEnderecoCoord : listClienteEnderecoCoords) {
				enderecoFavorito.addClienteEnderecoCoords(toBasicEntity(loopJsonClienteEnderecoCoord));
			}
		}

		JsonCliente cliente_ = jsonEnderecoFavorito.getCliente();
		if (cliente_ != null) {
			enderecoFavorito.setCliente(toEntity(cliente_));
		}

		JsonEmpresa empresa_ = jsonEnderecoFavorito.getEmpresa();
		if (empresa_ != null) {
			enderecoFavorito.setEmpresa(toEntity(empresa_));
		}
		return enderecoFavorito;

	}

	public static EnderecoFavorito toEntity(JsonEnderecoFavorito jsonEnderecoFavorito) {
		EnderecoFavorito enderecoFavorito = new EnderecoFavorito();

		return apply(enderecoFavorito, jsonEnderecoFavorito);
	}

	public static List<JsonEnderecoFavorito> toListJsonEnderecosFavoritos(List<EnderecoFavorito> all) {
		List<JsonEnderecoFavorito> jsonEnderecosFavoritos = new ArrayList<JsonEnderecoFavorito>();
		for (EnderecoFavorito enderecoFavorito : all) {
			jsonEnderecosFavoritos.add(toJson(enderecoFavorito));
		}
		return jsonEnderecosFavoritos;
	}

	// converte de entidade para json --------------------
	private static JsonClienteEnderecoCoord toBasicJson(ClienteEnderecoCoord clienteEnderecoCoord) {
		JsonClienteEnderecoCoord jsonClienteEnderecoCoord = new JsonClienteEnderecoCoord();
		applyBasicJsonValues(jsonClienteEnderecoCoord, clienteEnderecoCoord);
		return jsonClienteEnderecoCoord;
	}

	private static ClienteEnderecoCoord toBasicEntity(JsonClienteEnderecoCoord jsonClienteEnderecoCoord) {
		ClienteEnderecoCoord clienteEnderecoCoord = new ClienteEnderecoCoord();
		applyBasicEntityValues(clienteEnderecoCoord, jsonClienteEnderecoCoord);
		return clienteEnderecoCoord;
	}

	private static void applyBasicJsonValues(JsonClienteEnderecoCoord jsonClienteEnderecoCoord, ClienteEnderecoCoord clienteEnderecoCoord) {
		jsonClienteEnderecoCoord.setId(clienteEnderecoCoord.getId());
		jsonClienteEnderecoCoord.setLatitude(clienteEnderecoCoord.getLatitude());
		jsonClienteEnderecoCoord.setLongitude(clienteEnderecoCoord.getLongitude());
		jsonClienteEnderecoCoord.setEnderecoFormatado(clienteEnderecoCoord.getEnderecoFormatado());
		jsonClienteEnderecoCoord.setValido(clienteEnderecoCoord.getValido());
	}

	private static void applyBasicEntityValues(ClienteEnderecoCoord clienteEnderecoCoord, JsonClienteEnderecoCoord jsonClienteEnderecoCoord) {
		clienteEnderecoCoord.setId(jsonClienteEnderecoCoord.getId());
		clienteEnderecoCoord.setLatitude(jsonClienteEnderecoCoord.getLatitude());
		clienteEnderecoCoord.setLongitude(jsonClienteEnderecoCoord.getLongitude());
		clienteEnderecoCoord.setEnderecoFormatado(jsonClienteEnderecoCoord.getEnderecoFormatado());
		clienteEnderecoCoord.setValido(jsonClienteEnderecoCoord.getValido());
	}

	public static JsonClienteEnderecoCoord toJson(ClienteEnderecoCoord clienteEnderecoCoord) {
		JsonClienteEnderecoCoord jsonClienteEnderecoCoord = new JsonClienteEnderecoCoord();

		applyBasicJsonValues(jsonClienteEnderecoCoord, clienteEnderecoCoord);

		EnderecoFavorito clienteEndereco_ = clienteEnderecoCoord.getClienteEndereco();
		if (clienteEndereco_ != null) {
			jsonClienteEnderecoCoord.setClienteEndereco(toBasicJson(clienteEndereco_));
		}
		return jsonClienteEnderecoCoord;
	}

	public static ClienteEnderecoCoord apply(ClienteEnderecoCoord clienteEnderecoCoord, JsonClienteEnderecoCoord jsonClienteEnderecoCoord) {

		if (clienteEnderecoCoord == null)
			clienteEnderecoCoord = new ClienteEnderecoCoord();

		applyBasicEntityValues(clienteEnderecoCoord, jsonClienteEnderecoCoord);

		JsonEnderecoFavorito enderecoFavorito_ = jsonClienteEnderecoCoord.getClienteEndereco();
		if (enderecoFavorito_ != null) {
			clienteEnderecoCoord.setClienteEndereco(toEntity(enderecoFavorito_));
		}
		return clienteEnderecoCoord;

	}

	public static ClienteEnderecoCoord toEntity(JsonClienteEnderecoCoord jsonClienteEnderecoCoord) {
		ClienteEnderecoCoord clienteEnderecoCoord = new ClienteEnderecoCoord();

		return apply(clienteEnderecoCoord, jsonClienteEnderecoCoord);
	}

	public static List<JsonClienteEnderecoCoord> toListJsonClienteEnderecoCoords(List<ClienteEnderecoCoord> all) {
		List<JsonClienteEnderecoCoord> jsonClienteEnderecoCoords = new ArrayList<JsonClienteEnderecoCoord>();
		for (ClienteEnderecoCoord clienteEnderecoCoord : all) {
			jsonClienteEnderecoCoords.add(toJson(clienteEnderecoCoord));
		}
		return jsonClienteEnderecoCoords;
	}

	// converte de entidade para json --------------------
	private static JsonTelefoneFavorito toBasicJson(TelefoneFavorito telefoneFavorito) {
		JsonTelefoneFavorito jsonTelefoneFavorito = new JsonTelefoneFavorito();
		applyBasicJsonValues(jsonTelefoneFavorito, telefoneFavorito);
		return jsonTelefoneFavorito;
	}

	private static TelefoneFavorito toBasicEntity(JsonTelefoneFavorito jsonTelefoneFavorito) {
		TelefoneFavorito telefoneFavorito = new TelefoneFavorito();
		applyBasicEntityValues(telefoneFavorito, jsonTelefoneFavorito);
		return telefoneFavorito;
	}

	private static void applyBasicJsonValues(JsonTelefoneFavorito jsonTelefoneFavorito, TelefoneFavorito telefoneFavorito) {
		jsonTelefoneFavorito.setId(telefoneFavorito.getId());
		jsonTelefoneFavorito.setDdd(telefoneFavorito.getDdd());
		jsonTelefoneFavorito.setFone(telefoneFavorito.getFone());
		jsonTelefoneFavorito.setFavorito(telefoneFavorito.getFavorito());
	}

	private static void applyBasicEntityValues(TelefoneFavorito telefoneFavorito, JsonTelefoneFavorito jsonTelefoneFavorito) {
		telefoneFavorito.setId(jsonTelefoneFavorito.getId());
		telefoneFavorito.setDdd(jsonTelefoneFavorito.getDdd());
		telefoneFavorito.setFone(jsonTelefoneFavorito.getFone());
		telefoneFavorito.setFavorito(jsonTelefoneFavorito.getFavorito());
	}

	public static JsonTelefoneFavorito toJson(TelefoneFavorito telefoneFavorito) {
		JsonTelefoneFavorito jsonTelefoneFavorito = new JsonTelefoneFavorito();

		applyBasicJsonValues(jsonTelefoneFavorito, telefoneFavorito);

		Cliente cliente_ = telefoneFavorito.getCliente();
		if (cliente_ != null) {
			jsonTelefoneFavorito.setCliente(toBasicJson(cliente_));
		}
		Empresa empresa_ = telefoneFavorito.getEmpresa();
		if (empresa_ != null) {
			jsonTelefoneFavorito.setEmpresa(toBasicJson(empresa_));
		}
		return jsonTelefoneFavorito;
	}

	public static TelefoneFavorito apply(TelefoneFavorito telefoneFavorito, JsonTelefoneFavorito jsonTelefoneFavorito) {

		if (telefoneFavorito == null)
			telefoneFavorito = new TelefoneFavorito();

		applyBasicEntityValues(telefoneFavorito, jsonTelefoneFavorito);

		JsonCliente cliente_ = jsonTelefoneFavorito.getCliente();
		if (cliente_ != null) {
			telefoneFavorito.setCliente(toEntity(cliente_));
		}
		JsonEmpresa empresa_ = jsonTelefoneFavorito.getEmpresa();
		if (empresa_ != null) {
			telefoneFavorito.setEmpresa(toEntity(empresa_));
		}
		return telefoneFavorito;

	}

	public static TelefoneFavorito toEntity(JsonTelefoneFavorito jsonTelefoneFavorito) {
		TelefoneFavorito telefoneFavorito = new TelefoneFavorito();

		return apply(telefoneFavorito, jsonTelefoneFavorito);
	}

	public static List<JsonTelefoneFavorito> toListJsonTelefonesFavoritos(List<TelefoneFavorito> all) {
		List<JsonTelefoneFavorito> jsonTelefonesFavoritos = new ArrayList<JsonTelefoneFavorito>();
		for (TelefoneFavorito telefoneFavorito : all) {
			jsonTelefonesFavoritos.add(toJson(telefoneFavorito));
		}
		return jsonTelefonesFavoritos;
	}

	// converte de entidade para json --------------------
	private static JsonContato toBasicJson(Contato contato) {
		JsonContato jsonContato = new JsonContato();
		applyBasicJsonValues(jsonContato, contato);
		return jsonContato;
	}

	private static Contato toBasicEntity(JsonContato jsonContato) {
		Contato contato = new Contato();
		applyBasicEntityValues(contato, jsonContato);
		return contato;
	}

	private static void applyBasicJsonValues(JsonContato jsonContato, Contato contato) {
		jsonContato.setId(contato.getId());
		jsonContato.setNome(contato.getNome());
		jsonContato.setDdd(contato.getDdd());
		jsonContato.setFone(contato.getFone());
		jsonContato.setSetor(contato.getSetor());
	}

	private static void applyBasicEntityValues(Contato contato, JsonContato jsonContato) {
		contato.setId(jsonContato.getId());
		contato.setNome(jsonContato.getNome());
		contato.setDdd(jsonContato.getDdd());
		contato.setFone(jsonContato.getFone());
		contato.setSetor(jsonContato.getSetor());
	}

	public static JsonContato toJson(Contato contato) {
		JsonContato jsonContato = new JsonContato();

		applyBasicJsonValues(jsonContato, contato);

		Empresa empresa_ = contato.getEmpresa();
		if (empresa_ != null) {
			jsonContato.setEmpresa(toBasicJson(empresa_));
		}
		return jsonContato;
	}

	public static Contato apply(Contato contato, JsonContato jsonContato) {

		if (contato == null)
			contato = new Contato();

		applyBasicEntityValues(contato, jsonContato);

		JsonEmpresa empresa_ = jsonContato.getEmpresa();
		if (empresa_ != null) {
			contato.setEmpresa(toEntity(empresa_));
		}
		return contato;

	}

	public static Contato toEntity(JsonContato jsonContato) {
		Contato contato = new Contato();

		return apply(contato, jsonContato);
	}

	public static List<JsonContato> toListJsonContatos(List<Contato> all) {
		List<JsonContato> jsonContatos = new ArrayList<JsonContato>();
		for (Contato contato : all) {
			jsonContatos.add(toJson(contato));
		}
		return jsonContatos;
	}

	// converte de entidade para json --------------------
	private static JsonCorrida toBasicJson(Corrida corrida) {
		JsonCorrida jsonCorrida = new JsonCorrida();
		applyBasicJsonValues(jsonCorrida, corrida);
		return jsonCorrida;
	}

	private static Corrida toBasicEntity(JsonCorrida jsonCorrida) {
		Corrida corrida = new Corrida();
		applyBasicEntityValues(corrida, jsonCorrida);
		return corrida;
	}

	private static void applyBasicJsonValues(JsonCorrida jsonCorrida, Corrida corrida) {
		jsonCorrida.setId(corrida.getId());
		jsonCorrida.setDataCorrida(DateUtil.localDateTimeAsString(corrida.getDataCorrida()));
		jsonCorrida.setDataAgendamento(DateUtil.localDateTimeAsString(corrida.getDataAgendamento()));
		jsonCorrida.setTipoPagamento(corrida.getTipoPagamento());
		jsonCorrida.setValor(corrida.getValor());
		jsonCorrida.setObservacao(corrida.getObservacao());
		jsonCorrida.setObservacaoGeral(corrida.getObservacaoGeral());
		jsonCorrida.setStatusCorrida(corrida.getStatusCorrida());
		jsonCorrida.setSeqEmAtendimento(corrida.getSeqEmAtendimento());
		jsonCorrida.setMotoGrande(corrida.getMotoGrande());
		jsonCorrida.setBau(corrida.getBau());
		jsonCorrida.setPadronizada(corrida.getPadronizada());
		jsonCorrida.setFilaCriada(corrida.getFilaCriada());
		jsonCorrida.setTelefone(corrida.getTelefone());
	}

	private static void applyBasicEntityValues(Corrida corrida, JsonCorrida jsonCorrida) {
		corrida.setId(jsonCorrida.getId());
		corrida.setDataCorrida(DateUtil.stringAsLocalDateTime(jsonCorrida.getDataCorrida()));
		corrida.setDataAgendamento(DateUtil.stringAsLocalDateTime(jsonCorrida.getDataAgendamento()));
		corrida.setTipoPagamento(jsonCorrida.getTipoPagamento());
		corrida.setValor(jsonCorrida.getValor());
		corrida.setObservacao(jsonCorrida.getObservacao());
		corrida.setObservacaoGeral(jsonCorrida.getObservacaoGeral());
		corrida.setStatusCorrida(jsonCorrida.getStatusCorrida());
		corrida.setSeqEmAtendimento(jsonCorrida.getSeqEmAtendimento());
		corrida.setMotoGrande(jsonCorrida.getMotoGrande());
		corrida.setBau(jsonCorrida.getBau());
		corrida.setPadronizada(jsonCorrida.getPadronizada());
		corrida.setFilaCriada(jsonCorrida.getFilaCriada());
		corrida.setTelefone(jsonCorrida.getTelefone());
	}

	public static JsonCorrida toJson(Corrida corrida) {
		JsonCorrida jsonCorrida = new JsonCorrida();

		applyBasicJsonValues(jsonCorrida, corrida);

		List<EnderecoCorrida> listEnderecoCorridas = corrida.getEnderecoCorridas();
		if (listEnderecoCorridas != null) {
			for (EnderecoCorrida loopEnderecoCorrida : listEnderecoCorridas) {
				jsonCorrida.getEnderecoCorridas().add(toBasicJson(loopEnderecoCorrida));
			}
		}

		List<DistanciaMotoCorrida> listDistanciaMotoCorridas = corrida.getDistanciaMotoCorridas();
		if (listDistanciaMotoCorridas != null) {
			for (DistanciaMotoCorrida loopDistanciaMotoCorrida : listDistanciaMotoCorridas) {
				jsonCorrida.getDistanciaMotoCorridas().add(toBasicJson(loopDistanciaMotoCorrida));
			}
		}
		List<InformacaoTarifaAdicional> listInformacaoTarifaAdicionals = corrida.getInformacaoTarifas();
		if (listInformacaoTarifaAdicionals != null) {
			for (InformacaoTarifaAdicional loopInformacaoTarifaAdicional : listInformacaoTarifaAdicionals) {
				jsonCorrida.getInformacaoTarifas().add(toBasicJson(loopInformacaoTarifaAdicional));
			}
		}

		Atendente atendente_ = corrida.getAtendente();
		if (atendente_ != null) {
			jsonCorrida.setAtendente(toBasicJson(atendente_));
		}
		Motorista motorista_ = corrida.getMotorista();
		if (motorista_ != null) {
			jsonCorrida.setMotorista(toBasicJson(motorista_));
		}

		Empresa empresa_ = corrida.getEmpresa();
		if (empresa_ != null) {
			jsonCorrida.setEmpresa(toBasicJson(empresa_));
		}
		Cliente cliente_ = corrida.getCliente();
		if (cliente_ != null) {
			jsonCorrida.setCliente(toBasicJson(cliente_));
		}
		return jsonCorrida;
	}

	public static Corrida apply(Corrida corrida, JsonCorrida jsonCorrida) {

		if (corrida == null)
			corrida = new Corrida();

		applyBasicEntityValues(corrida, jsonCorrida);

		ArrayList<JsonEnderecoCorrida> listEnderecoCorridas = jsonCorrida.getEnderecoCorridas();
		if (listEnderecoCorridas != null) {
			for (JsonEnderecoCorrida loopJsonEnderecoCorrida : listEnderecoCorridas) {
				corrida.addEnderecoCorridas(toBasicEntity(loopJsonEnderecoCorrida));
			}
		}

		ArrayList<JsonDistanciaMotoCorrida> listDistanciaMotoCorridas = jsonCorrida.getDistanciaMotoCorridas();
		if (listDistanciaMotoCorridas != null) {
			for (JsonDistanciaMotoCorrida loopJsonDistanciaMotoCorrida : listDistanciaMotoCorridas) {
				corrida.addDistanciaMotoCorridas(toBasicEntity(loopJsonDistanciaMotoCorrida));
			}
		}

		List<JsonInformacaoTarifaAdicional> listInformacaoTarifaAdicionals = jsonCorrida.getInformacaoTarifas();
		if (listInformacaoTarifaAdicionals != null) {
			for (JsonInformacaoTarifaAdicional loopJsonInformacaoTarifaAdicional : listInformacaoTarifaAdicionals) {
				corrida.addInformacaoTarifaAdicionals(toBasicEntity(loopJsonInformacaoTarifaAdicional));
			}
		}

		JsonAtendente atendente_ = jsonCorrida.getAtendente();
		if (atendente_ != null) {
			corrida.setAtendente(toEntity(atendente_));
		}
		JsonMotorista motorista_ = jsonCorrida.getMotorista();
		if (motorista_ != null) {
			corrida.setMotorista(toEntity(motorista_));
		}

		JsonEmpresa empresa_ = jsonCorrida.getEmpresa();
		if (empresa_ != null) {
			corrida.setEmpresa(toBasicEntity(empresa_));
		}

		JsonCliente cliente_ = jsonCorrida.getCliente();
		if (cliente_ != null) {
			corrida.setCliente(toBasicEntity(cliente_));
		}

		return corrida;

	}

	public static Corrida toEntity(JsonCorrida jsonCorrida) {
		Corrida corrida = new Corrida();

		return apply(corrida, jsonCorrida);
	}

	public static List<JsonCorrida> toListJsonCorridas(List<Corrida> all) {
		List<JsonCorrida> jsonCorridas = new ArrayList<JsonCorrida>();
		for (Corrida corrida : all) {
			jsonCorridas.add(toJson(corrida));
		}
		return jsonCorridas;
	}

	// converte de entidade para json --------------------
	private static JsonDistanciaMotoCorrida toBasicJson(DistanciaMotoCorrida distanciaMotoCorrida) {
		JsonDistanciaMotoCorrida jsonDistanciaMotoCorrida = new JsonDistanciaMotoCorrida();
		applyBasicJsonValues(jsonDistanciaMotoCorrida, distanciaMotoCorrida);
		return jsonDistanciaMotoCorrida;
	}

	private static DistanciaMotoCorrida toBasicEntity(JsonDistanciaMotoCorrida jsonDistanciaMotoCorrida) {
		DistanciaMotoCorrida distanciaMotoCorrida = new DistanciaMotoCorrida();
		applyBasicEntityValues(distanciaMotoCorrida, jsonDistanciaMotoCorrida);
		return distanciaMotoCorrida;
	}

	private static void applyBasicJsonValues(JsonDistanciaMotoCorrida jsonDistanciaMotoCorrida, DistanciaMotoCorrida distanciaMotoCorrida) {
		jsonDistanciaMotoCorrida.setId(distanciaMotoCorrida.getId());
		jsonDistanciaMotoCorrida.setDistancia(distanciaMotoCorrida.getDistancia());
		jsonDistanciaMotoCorrida.setSequencia(distanciaMotoCorrida.getSequencia());
	}

	private static void applyBasicEntityValues(DistanciaMotoCorrida distanciaMotoCorrida, JsonDistanciaMotoCorrida jsonDistanciaMotoCorrida) {
		distanciaMotoCorrida.setId(jsonDistanciaMotoCorrida.getId());
		distanciaMotoCorrida.setDistancia(jsonDistanciaMotoCorrida.getDistancia());
		distanciaMotoCorrida.setSequencia(jsonDistanciaMotoCorrida.getSequencia());
	}

	public static JsonDistanciaMotoCorrida toJson(DistanciaMotoCorrida distanciaMotoCorrida) {
		JsonDistanciaMotoCorrida jsonDistanciaMotoCorrida = new JsonDistanciaMotoCorrida();

		applyBasicJsonValues(jsonDistanciaMotoCorrida, distanciaMotoCorrida);

		Motorista motorista_ = distanciaMotoCorrida.getMotorista();
		if (motorista_ != null) {
			jsonDistanciaMotoCorrida.setMotorista(toBasicJson(motorista_));
		}
		Corrida corrida_ = distanciaMotoCorrida.getCorrida();
		if (corrida_ != null) {
			jsonDistanciaMotoCorrida.setCorrida(toBasicJson(corrida_));
		}
		return jsonDistanciaMotoCorrida;
	}

	public static DistanciaMotoCorrida apply(DistanciaMotoCorrida distanciaMotoCorrida, JsonDistanciaMotoCorrida jsonDistanciaMotoCorrida) {

		if (distanciaMotoCorrida == null)
			distanciaMotoCorrida = new DistanciaMotoCorrida();

		applyBasicEntityValues(distanciaMotoCorrida, jsonDistanciaMotoCorrida);

		JsonMotorista motorista_ = jsonDistanciaMotoCorrida.getMotorista();
		if (motorista_ != null) {
			distanciaMotoCorrida.setMotorista(toEntity(motorista_));
		}
		JsonCorrida corrida_ = jsonDistanciaMotoCorrida.getCorrida();
		if (corrida_ != null) {
			distanciaMotoCorrida.setCorrida(toEntity(corrida_));
		}
		return distanciaMotoCorrida;

	}

	public static DistanciaMotoCorrida toEntity(JsonDistanciaMotoCorrida jsonDistanciaMotoCorrida) {
		DistanciaMotoCorrida distanciaMotoCorrida = new DistanciaMotoCorrida();

		return apply(distanciaMotoCorrida, jsonDistanciaMotoCorrida);
	}

	public static List<JsonDistanciaMotoCorrida> toListJsonDistanciaMotoCorridas(List<DistanciaMotoCorrida> all) {
		List<JsonDistanciaMotoCorrida> jsonDistanciaMotoCorridas = new ArrayList<JsonDistanciaMotoCorrida>();
		for (DistanciaMotoCorrida distanciaMotoCorrida : all) {
			jsonDistanciaMotoCorridas.add(toJson(distanciaMotoCorrida));
		}
		return jsonDistanciaMotoCorridas;
	}

	// converte de entidade para json --------------------
	private static JsonEmpresa toBasicJson(Empresa empresa) {
		JsonEmpresa jsonEmpresa = new JsonEmpresa();
		applyBasicJsonValues(jsonEmpresa, empresa);
		return jsonEmpresa;
	}

	private static Empresa toBasicEntity(JsonEmpresa jsonEmpresa) {
		Empresa empresa = new Empresa();
		applyBasicEntityValues(empresa, jsonEmpresa);
		return empresa;
	}

	private static void applyBasicJsonValues(JsonEmpresa jsonEmpresa, Empresa empresa) {
		jsonEmpresa.setId(empresa.getId());
		jsonEmpresa.setNomeFantasia(empresa.getNomeFantasia());
		jsonEmpresa.setCnpj(empresa.getCnpj());
		jsonEmpresa.setContrato(empresa.getContrato());
		jsonEmpresa.setRazaoSocial(empresa.getRazaoSocial());
		jsonEmpresa.setEndereco(empresa.getEndereco());
		jsonEmpresa.setNumeroEndereco(empresa.getNumeroEndereco());
		jsonEmpresa.setComplemento(empresa.getComplemento());
		jsonEmpresa.setUf(empresa.getUf());
		jsonEmpresa.setCidade(empresa.getCidade());
		jsonEmpresa.setBairro(empresa.getBairro());
		jsonEmpresa.setCep(empresa.getCep());
		jsonEmpresa.setReferenciaEndereco(empresa.getReferenciaEndereco());
		jsonEmpresa.setDdd(empresa.getDdd());
		jsonEmpresa.setFone(empresa.getFone());
		jsonEmpresa.setUtilizaVoucher(empresa.getUtilizaVoucher());
		jsonEmpresa.setUtilizaEticket(empresa.getUtilizaEticket());
		jsonEmpresa.setObservacaoTaxista(empresa.getObservacaoTaxista());
		jsonEmpresa.setObservacaoCentral(empresa.getObservacaoCentral());
		jsonEmpresa.setObservacaoFinanceiro(empresa.getObservacaoFinanceiro());
		jsonEmpresa.setPercentualDesconto(empresa.getPercentualDesconto());
		jsonEmpresa.setInscricaoMunicipal(empresa.getInscricaoMunicipal());
		jsonEmpresa.setInscricaoEstadual(empresa.getInscricaoEstadual());
		jsonEmpresa.setEmiteNf(empresa.getEmiteNf());
		jsonEmpresa.setPercentualIss(empresa.getPercentualIss());
		jsonEmpresa.setPercentualIrf(empresa.getPercentualIrf());
		jsonEmpresa.setPercentualInss(empresa.getPercentualInss());
		jsonEmpresa.setDiaVencimento(empresa.getDiaVencimento());
		jsonEmpresa.setPercentualMotorista(empresa.getPercentualMotorista());
		jsonEmpresa.setBanco(empresa.getBanco());
		jsonEmpresa.setAgencia(empresa.getAgencia());
		jsonEmpresa.setConta(empresa.getConta());
		jsonEmpresa.setEmail(empresa.getEmail());
		jsonEmpresa.setUsuarioCadastro(empresa.getUsuarioCadastro());
		jsonEmpresa.setDataCadastro(DateUtil.localDateTimeAsString(empresa.getDataCadastro()));
		jsonEmpresa.setStatusEmpresa(empresa.getStatusEmpresa());
		jsonEmpresa.setNomeCliente1(empresa.getNomeCliente1());
		jsonEmpresa.setFoneCliente1(empresa.getFoneCliente1());
		jsonEmpresa.setNomeCliente2(empresa.getNomeCliente2());
		jsonEmpresa.setFoneCliente2(empresa.getFoneCliente2());
		jsonEmpresa.setNomeFornecedor1(empresa.getNomeFornecedor1());
		jsonEmpresa.setFoneFornecedor1(empresa.getFoneFornecedor1());
		jsonEmpresa.setNomeFornecedor2(empresa.getNomeFornecedor2());
		jsonEmpresa.setFoneFornecedor2(empresa.getFoneFornecedor2());
		jsonEmpresa.setUsername(empresa.getUsername());
		jsonEmpresa.setSenha(empresa.getSenha());

	}

	private static void applyBasicEntityValues(Empresa empresa, JsonEmpresa jsonEmpresa) {
		empresa.setId(jsonEmpresa.getId());
		empresa.setNomeFantasia(jsonEmpresa.getNomeFantasia());
		empresa.setCnpj(jsonEmpresa.getCnpj());
		empresa.setContrato(jsonEmpresa.getContrato());
		empresa.setRazaoSocial(jsonEmpresa.getRazaoSocial());
		empresa.setEndereco(jsonEmpresa.getEndereco());
		empresa.setNumeroEndereco(jsonEmpresa.getNumeroEndereco());
		empresa.setComplemento(jsonEmpresa.getComplemento());
		empresa.setUf(jsonEmpresa.getUf());
		empresa.setCidade(jsonEmpresa.getCidade());
		empresa.setBairro(jsonEmpresa.getBairro());
		empresa.setCep(jsonEmpresa.getCep());
		empresa.setReferenciaEndereco(jsonEmpresa.getReferenciaEndereco());
		empresa.setDdd(jsonEmpresa.getDdd());
		empresa.setFone(jsonEmpresa.getFone());
		empresa.setUtilizaVoucher(jsonEmpresa.getUtilizaVoucher());
		empresa.setUtilizaEticket(jsonEmpresa.getUtilizaEticket());
		empresa.setObservacaoTaxista(jsonEmpresa.getObservacaoTaxista());
		empresa.setObservacaoCentral(jsonEmpresa.getObservacaoCentral());
		empresa.setObservacaoFinanceiro(jsonEmpresa.getObservacaoFinanceiro());
		empresa.setPercentualDesconto(jsonEmpresa.getPercentualDesconto());
		empresa.setInscricaoMunicipal(jsonEmpresa.getInscricaoMunicipal());
		empresa.setInscricaoEstadual(jsonEmpresa.getInscricaoEstadual());
		empresa.setEmiteNf(jsonEmpresa.getEmiteNf());
		empresa.setPercentualIss(jsonEmpresa.getPercentualIss());
		empresa.setPercentualIrf(jsonEmpresa.getPercentualIrf());
		empresa.setPercentualInss(jsonEmpresa.getPercentualInss());
		empresa.setDiaVencimento(jsonEmpresa.getDiaVencimento());
		empresa.setPercentualMotorista(jsonEmpresa.getPercentualMotorista());
		empresa.setBanco(jsonEmpresa.getBanco());
		empresa.setAgencia(jsonEmpresa.getAgencia());
		empresa.setConta(jsonEmpresa.getConta());
		empresa.setEmail(jsonEmpresa.getEmail());
		empresa.setUsuarioCadastro(jsonEmpresa.getUsuarioCadastro());
		empresa.setDataCadastro(DateUtil.stringAsLocalDateTime(jsonEmpresa.getDataCadastro()));
		empresa.setStatusEmpresa(jsonEmpresa.getStatusEmpresa());
		empresa.setNomeCliente1(jsonEmpresa.getNomeCliente1());
		empresa.setFoneCliente1(jsonEmpresa.getFoneCliente1());
		empresa.setNomeCliente2(jsonEmpresa.getNomeCliente2());
		empresa.setFoneCliente2(jsonEmpresa.getFoneCliente2());
		empresa.setNomeFornecedor1(jsonEmpresa.getNomeFornecedor1());
		empresa.setFoneFornecedor1(jsonEmpresa.getFoneFornecedor1());
		empresa.setNomeFornecedor2(jsonEmpresa.getNomeFornecedor2());
		empresa.setFoneFornecedor2(jsonEmpresa.getFoneFornecedor2());
		empresa.setUsername(jsonEmpresa.getUsername());
		empresa.setSenha(jsonEmpresa.getSenha());

	}

	public static JsonEmpresa toJson(Empresa empresa) {
		JsonEmpresa jsonEmpresa = new JsonEmpresa();

		applyBasicJsonValues(jsonEmpresa, empresa);

		List<EnderecoFavorito> listEnderecosFavoritos = empresa.getEnderecosFavoritos();
		if (listEnderecosFavoritos != null) {
			for (EnderecoFavorito loopEnderecoFavorito : listEnderecosFavoritos) {
				jsonEmpresa.getEnderecosFavoritos().add(toBasicJson(loopEnderecoFavorito));
			}
		}
		List<TelefoneFavorito> listTelefonesFavoritos = empresa.getTelefonesFavoritos();
		if (listTelefonesFavoritos != null) {
			for (TelefoneFavorito loopTelefoneFavorito : listTelefonesFavoritos) {
				jsonEmpresa.getTelefonesFavoritos().add(toBasicJson(loopTelefoneFavorito));
			}
		}
		List<CentroCusto> listCentroCustos = empresa.getCentroCustos();
		if (listCentroCustos != null) {
			for (CentroCusto loopCentroCusto : listCentroCustos) {
				jsonEmpresa.getCentroCustos().add(toBasicJson(loopCentroCusto));
			}
		}
		List<HdUsuario> listHdUsuarios = empresa.getHdUsuarios();
		if (listHdUsuarios != null) {
			for (HdUsuario loopHdUsuario : listHdUsuarios) {
				jsonEmpresa.getHdUsuarios().add(toBasicJson(loopHdUsuario));
			}
		}
		List<Contato> listContatos = empresa.getContatos();
		if (listContatos != null) {
			for (Contato loopContato : listContatos) {
				jsonEmpresa.getContatos().add(toBasicJson(loopContato));
			}
		}
		List<Socio> listSocios = empresa.getSocios();
		if (listSocios != null) {
			for (Socio loopSocio : listSocios) {
				jsonEmpresa.getSocios().add(toBasicJson(loopSocio));
			}
		}
		List<FaixaVoucher> listFaixaVouchers = empresa.getFaixaVouchers();
		if (listFaixaVouchers != null) {
			for (FaixaVoucher loopFaixaVoucher : listFaixaVouchers) {
				jsonEmpresa.getFaixaVouchers().add(toBasicJson(loopFaixaVoucher));
			}
		}
		List<Cliente> listClientes = empresa.getClientes();
		if (listClientes != null) {
			for (Cliente loopCliente : listClientes) {
				jsonEmpresa.getClientes().add(toBasicJson(loopCliente));
			}
		}
		return jsonEmpresa;
	}

	public static Empresa apply(Empresa empresa, JsonEmpresa jsonEmpresa) {

		if (empresa == null)
			empresa = new Empresa();

		applyBasicEntityValues(empresa, jsonEmpresa);

		ArrayList<JsonEnderecoFavorito> listEnderecosFavoritos = jsonEmpresa.getEnderecosFavoritos();
		if (listEnderecosFavoritos != null) {
			for (JsonEnderecoFavorito loopJsonEnderecoFavoritos : listEnderecosFavoritos) {
				empresa.addEnderecosFavoritos(toBasicEntity(loopJsonEnderecoFavoritos));
			}
		}

		// Aqui que xabuga
		ArrayList<JsonTelefoneFavorito> listTelefonesFavoritos = jsonEmpresa.getTelefonesFavoritos();
		if (listTelefonesFavoritos != null) {
			for (JsonTelefoneFavorito loopJsonTelefoneFavorito : listTelefonesFavoritos) {
				empresa.addTelefoneFavorito(toBasicEntity(loopJsonTelefoneFavorito));
			}
		}

		ArrayList<JsonCentroCusto> listCentroCustos = jsonEmpresa.getCentroCustos();
		if (listCentroCustos != null) {
			for (JsonCentroCusto loopJsonCentroCusto : listCentroCustos) {
				empresa.addCentroCustos(toBasicEntity(loopJsonCentroCusto));
			}
		}

		ArrayList<JsonHdUsuario> listHdUsuarios = jsonEmpresa.getHdUsuarios();
		if (listHdUsuarios != null) {
			for (JsonHdUsuario loopJsonHdUsuario : listHdUsuarios) {
				empresa.addHdUsuarios(toBasicEntity(loopJsonHdUsuario));
			}
		}

		ArrayList<JsonContato> listContatos = jsonEmpresa.getContatos();
		if (listContatos != null) {
			for (JsonContato loopJsonContato : listContatos) {
				empresa.addContatos(toBasicEntity(loopJsonContato));
			}
		}

		ArrayList<JsonSocio> listSocios = jsonEmpresa.getSocios();
		if (listSocios != null) {
			for (JsonSocio loopJsonSocio : listSocios) {
				empresa.addSocios(toBasicEntity(loopJsonSocio));
			}
		}

		ArrayList<JsonFaixaVoucher> listFaixaVouchers = jsonEmpresa.getFaixaVouchers();
		if (listFaixaVouchers != null) {
			for (JsonFaixaVoucher loopJsonFaixaVoucher : listFaixaVouchers) {
				empresa.addFaixaVouchers(toBasicEntity(loopJsonFaixaVoucher));
			}
		}

		ArrayList<JsonCliente> listClientes = jsonEmpresa.getClientes();
		if (listClientes != null) {
			for (JsonCliente loopJsonCliente : listClientes) {
				empresa.addClientes(toBasicEntity(loopJsonCliente));
			}
		}

		return empresa;

	}

	public static Empresa toEntity(JsonEmpresa jsonEmpresa) {
		Empresa empresa = new Empresa();

		return apply(empresa, jsonEmpresa);
	}

	public static List<JsonEmpresa> toListJsonEmpresas(List<Empresa> all) {
		List<JsonEmpresa> jsonEmpresas = new ArrayList<JsonEmpresa>();
		for (Empresa empresa : all) {
			jsonEmpresas.add(toJson(empresa));
		}
		return jsonEmpresas;
	}

	// converte de entidade para json --------------------
	private static JsonEnderecoCorrida toBasicJson(EnderecoCorrida enderecoCorrida) {
		JsonEnderecoCorrida jsonEnderecoCorrida = new JsonEnderecoCorrida();
		applyBasicJsonValues(jsonEnderecoCorrida, enderecoCorrida);
		return jsonEnderecoCorrida;
	}

	private static EnderecoCorrida toBasicEntity(JsonEnderecoCorrida jsonEnderecoCorrida) {
		EnderecoCorrida enderecoCorrida = new EnderecoCorrida();
		applyBasicEntityValues(enderecoCorrida, jsonEnderecoCorrida);
		return enderecoCorrida;
	}

	private static void applyBasicJsonValues(JsonEnderecoCorrida jsonEnderecoCorrida, EnderecoCorrida enderecoCorrida) {
		jsonEnderecoCorrida.setId(enderecoCorrida.getId());
		jsonEnderecoCorrida.setEndereco(enderecoCorrida.getEndereco());
		jsonEnderecoCorrida.setLogradouro(enderecoCorrida.getLogradouro());
		jsonEnderecoCorrida.setNumero(enderecoCorrida.getNumero());
		jsonEnderecoCorrida.setComplemento(enderecoCorrida.getComplemento());
		jsonEnderecoCorrida.setContato(enderecoCorrida.getContato());
		jsonEnderecoCorrida.setTarefa(enderecoCorrida.getTarefa());
		jsonEnderecoCorrida.setBairro(enderecoCorrida.getBairro());
		jsonEnderecoCorrida.setUf(enderecoCorrida.getUf());
		jsonEnderecoCorrida.setCidade(enderecoCorrida.getCidade());
		jsonEnderecoCorrida.setCep(enderecoCorrida.getCep());
		jsonEnderecoCorrida.setReferencia(enderecoCorrida.getReferencia());
		jsonEnderecoCorrida.setDescricao(enderecoCorrida.getDescricao());
		jsonEnderecoCorrida.setOrdem(enderecoCorrida.getOrdem());
		jsonEnderecoCorrida.setLatitude(enderecoCorrida.getLatitude());
		jsonEnderecoCorrida.setLongitude(enderecoCorrida.getLongitude());
		jsonEnderecoCorrida.setEnderecoFormatado(enderecoCorrida.getEnderecoFormatado());
		jsonEnderecoCorrida.setOrigem(enderecoCorrida.getOrigem());
	}

	private static void applyBasicEntityValues(EnderecoCorrida enderecoCorrida, JsonEnderecoCorrida jsonEnderecoCorrida) {
		enderecoCorrida.setId(jsonEnderecoCorrida.getId());
		enderecoCorrida.setEndereco(jsonEnderecoCorrida.getEndereco());
		enderecoCorrida.setLogradouro(jsonEnderecoCorrida.getLogradouro());
		enderecoCorrida.setNumero(jsonEnderecoCorrida.getNumero());
		enderecoCorrida.setComplemento(jsonEnderecoCorrida.getComplemento());
		enderecoCorrida.setContato(jsonEnderecoCorrida.getContato());
		enderecoCorrida.setTarefa(jsonEnderecoCorrida.getTarefa());

		enderecoCorrida.setBairro(jsonEnderecoCorrida.getBairro());
		enderecoCorrida.setUf(jsonEnderecoCorrida.getUf());
		enderecoCorrida.setCidade(jsonEnderecoCorrida.getCidade());
		enderecoCorrida.setCep(jsonEnderecoCorrida.getCep());
		enderecoCorrida.setReferencia(jsonEnderecoCorrida.getReferencia());
		enderecoCorrida.setDescricao(jsonEnderecoCorrida.getDescricao());
		enderecoCorrida.setOrdem(jsonEnderecoCorrida.getOrdem());
		enderecoCorrida.setLatitude(jsonEnderecoCorrida.getLatitude());
		enderecoCorrida.setLongitude(jsonEnderecoCorrida.getLongitude());
		enderecoCorrida.setEnderecoFormatado(jsonEnderecoCorrida.getEnderecoFormatado());
		enderecoCorrida.setOrigem(jsonEnderecoCorrida.getOrigem());
	}

	public static JsonEnderecoCorrida toJson(EnderecoCorrida enderecoCorrida) {
		JsonEnderecoCorrida jsonEnderecoCorrida = new JsonEnderecoCorrida();

		applyBasicJsonValues(jsonEnderecoCorrida, enderecoCorrida);

		List<Evento> listEventos = enderecoCorrida.getEventos();
		if (listEventos != null) {
			for (Evento loopEvento : listEventos) {
				jsonEnderecoCorrida.getEventos().add(toBasicJson(loopEvento));
			}
		}
		Corrida corrida_ = enderecoCorrida.getCorrida();
		if (corrida_ != null) {
			jsonEnderecoCorrida.setCorrida(toBasicJson(corrida_));
		}
		return jsonEnderecoCorrida;
	}

	public static EnderecoCorrida apply(EnderecoCorrida enderecoCorrida, JsonEnderecoCorrida jsonEnderecoCorrida) {

		if (enderecoCorrida == null)
			enderecoCorrida = new EnderecoCorrida();

		applyBasicEntityValues(enderecoCorrida, jsonEnderecoCorrida);

		ArrayList<JsonEvento> listEventos = jsonEnderecoCorrida.getEventos();
		if (listEventos != null) {
			for (JsonEvento loopJsonEvento : listEventos) {
				enderecoCorrida.addEventos(toBasicEntity(loopJsonEvento));
			}
		}

		JsonCorrida corrida_ = jsonEnderecoCorrida.getCorrida();
		if (corrida_ != null) {
			enderecoCorrida.setCorrida(toEntity(corrida_));
		}
		return enderecoCorrida;

	}

	public static EnderecoCorrida toEntity(JsonEnderecoCorrida jsonEnderecoCorrida) {
		EnderecoCorrida enderecoCorrida = new EnderecoCorrida();

		return apply(enderecoCorrida, jsonEnderecoCorrida);
	}

	public static List<JsonEnderecoCorrida> toListJsonEnderecoCorridas(List<EnderecoCorrida> all) {
		List<JsonEnderecoCorrida> jsonEnderecoCorridas = new ArrayList<JsonEnderecoCorrida>();
		for (EnderecoCorrida enderecoCorrida : all) {
			jsonEnderecoCorridas.add(toJson(enderecoCorrida));
		}
		return jsonEnderecoCorridas;
	}

	// converte de entidade para json --------------------
	private static JsonEvento toBasicJson(Evento evento) {
		JsonEvento jsonEvento = new JsonEvento();
		applyBasicJsonValues(jsonEvento, evento);
		return jsonEvento;
	}

	private static Evento toBasicEntity(JsonEvento jsonEvento) {
		Evento evento = new Evento();
		applyBasicEntityValues(evento, jsonEvento);
		return evento;
	}

	private static void applyBasicJsonValues(JsonEvento jsonEvento, Evento evento) {
		jsonEvento.setId(evento.getId());
		jsonEvento.setDescricao(evento.getDescricao());
		jsonEvento.setDataInicio(evento.getDataInicio());
		jsonEvento.setDataFim(evento.getDataFim());
		jsonEvento.setDescricaoCurta(evento.getDescricaoCurta());
	}

	private static void applyBasicEntityValues(Evento evento, JsonEvento jsonEvento) {
		evento.setId(jsonEvento.getId());
		evento.setDescricao(jsonEvento.getDescricao());
		evento.setDataInicio(jsonEvento.getDataInicio());
		evento.setDataFim(jsonEvento.getDataFim());
		evento.setDescricaoCurta(jsonEvento.getDescricaoCurta());
	}

	public static JsonEvento toJson(Evento evento) {
		JsonEvento jsonEvento = new JsonEvento();

		applyBasicJsonValues(jsonEvento, evento);

		EnderecoCorrida enderecoCorrida_ = evento.getEnderecoCorrida();
		if (enderecoCorrida_ != null) {
			jsonEvento.setEnderecoCorrida(toBasicJson(enderecoCorrida_));
		}
		Motorista motorista_ = evento.getMotorista();
		if (motorista_ != null) {
			jsonEvento.setMotorista(toBasicJson(motorista_));
		}
		VisitaTracker visitaTrackerSaida_ = evento.getVisitaTrackerSaida();
		if (visitaTrackerSaida_ != null) {
			jsonEvento.setVisitaTrackerSaida(toBasicJson(visitaTrackerSaida_));
		}
		VisitaTracker visitaTrackerChegada_ = evento.getVisitaTrackerChegada();
		if (visitaTrackerChegada_ != null) {
			jsonEvento.setVisitaTrackerChegada(toBasicJson(visitaTrackerChegada_));
		}
		return jsonEvento;
	}

	public static Evento apply(Evento evento, JsonEvento jsonEvento) {

		if (evento == null)
			evento = new Evento();

		applyBasicEntityValues(evento, jsonEvento);

		JsonEnderecoCorrida enderecoCorrida_ = jsonEvento.getEnderecoCorrida();
		if (enderecoCorrida_ != null) {
			evento.setEnderecoCorrida(toEntity(enderecoCorrida_));
		}
		JsonMotorista motorista_ = jsonEvento.getMotorista();
		if (motorista_ != null) {
			evento.setMotorista(toEntity(motorista_));
		}
		JsonVisitaTracker visitaTrackerChegada_ = jsonEvento.getVisitaTrackerChegada();
		if (visitaTrackerChegada_ != null) {
			evento.setVisitaTrackerChegada(toEntity(visitaTrackerChegada_));
		}

		JsonVisitaTracker visitaTrackerSaida_ = jsonEvento.getVisitaTrackerSaida();
		if (visitaTrackerSaida_ != null) {
			evento.setVisitaTrackerSaida(toEntity(visitaTrackerSaida_));
		}

		return evento;

	}

	public static Evento toEntity(JsonEvento jsonEvento) {
		Evento evento = new Evento();

		return apply(evento, jsonEvento);
	}

	public static List<JsonEvento> toListJsonEventos(List<Evento> all) {
		List<JsonEvento> jsonEventos = new ArrayList<JsonEvento>();
		for (Evento evento : all) {
			jsonEventos.add(toJson(evento));
		}
		return jsonEventos;
	}

	// converte de entidade para json --------------------
	private static JsonFaixaVoucher toBasicJson(FaixaVoucher faixaVoucher) {
		JsonFaixaVoucher jsonFaixaVoucher = new JsonFaixaVoucher();
		applyBasicJsonValues(jsonFaixaVoucher, faixaVoucher);
		return jsonFaixaVoucher;
	}

	private static FaixaVoucher toBasicEntity(JsonFaixaVoucher jsonFaixaVoucher) {
		FaixaVoucher faixaVoucher = new FaixaVoucher();
		applyBasicEntityValues(faixaVoucher, jsonFaixaVoucher);
		return faixaVoucher;
	}

	private static void applyBasicJsonValues(JsonFaixaVoucher jsonFaixaVoucher, FaixaVoucher faixaVoucher) {
		jsonFaixaVoucher.setId(faixaVoucher.getId());
		jsonFaixaVoucher.setSerie(faixaVoucher.getSerie());
		jsonFaixaVoucher.setNumeroInicial(faixaVoucher.getNumeroInicial());
		jsonFaixaVoucher.setNumeroFinal(faixaVoucher.getNumeroFinal());
		jsonFaixaVoucher.setInativo(faixaVoucher.getInativo());
	}

	private static void applyBasicEntityValues(FaixaVoucher faixaVoucher, JsonFaixaVoucher jsonFaixaVoucher) {
		faixaVoucher.setId(jsonFaixaVoucher.getId());
		faixaVoucher.setSerie(jsonFaixaVoucher.getSerie());
		faixaVoucher.setNumeroInicial(jsonFaixaVoucher.getNumeroInicial());
		faixaVoucher.setNumeroFinal(jsonFaixaVoucher.getNumeroFinal());
		faixaVoucher.setInativo(jsonFaixaVoucher.getInativo());
	}

	public static JsonFaixaVoucher toJson(FaixaVoucher faixaVoucher) {
		JsonFaixaVoucher jsonFaixaVoucher = new JsonFaixaVoucher();

		applyBasicJsonValues(jsonFaixaVoucher, faixaVoucher);

		Empresa empresa_ = faixaVoucher.getEmpresa();
		if (empresa_ != null) {
			jsonFaixaVoucher.setEmpresa(toBasicJson(empresa_));
		}

		Motorista motorista_ = faixaVoucher.getMotorista();
		if (motorista_ != null) {
			jsonFaixaVoucher.setMotorista(toBasicJson(motorista_));
		}

		return jsonFaixaVoucher;
	}

	public static FaixaVoucher apply(FaixaVoucher faixaVoucher, JsonFaixaVoucher jsonFaixaVoucher) {

		if (faixaVoucher == null)
			faixaVoucher = new FaixaVoucher();

		applyBasicEntityValues(faixaVoucher, jsonFaixaVoucher);

		JsonEmpresa empresa_ = jsonFaixaVoucher.getEmpresa();
		if (empresa_ != null) {
			faixaVoucher.setEmpresa(toEntity(empresa_));
		}

		JsonMotorista motorista_ = jsonFaixaVoucher.getMotorista();
		if (motorista_ != null)
			faixaVoucher.setMotorista(toEntity(motorista_));

		return faixaVoucher;

	}

	public static FaixaVoucher toEntity(JsonFaixaVoucher jsonFaixaVoucher) {
		FaixaVoucher faixaVoucher = new FaixaVoucher();

		return apply(faixaVoucher, jsonFaixaVoucher);
	}

	public static List<JsonFaixaVoucher> toListJsonFaixaVouchers(List<FaixaVoucher> all) {
		List<JsonFaixaVoucher> jsonFaixaVouchers = new ArrayList<JsonFaixaVoucher>();
		for (FaixaVoucher faixaVoucher : all) {
			jsonFaixaVouchers.add(toJson(faixaVoucher));
		}
		return jsonFaixaVouchers;
	}

	// converte de entidade para json --------------------
	private static JsonHdUsuario toBasicJson(HdUsuario hdUsuario) {
		JsonHdUsuario jsonHdUsuario = new JsonHdUsuario();
		applyBasicJsonValues(jsonHdUsuario, hdUsuario);
		return jsonHdUsuario;
	}

	private static HdUsuario toBasicEntity(JsonHdUsuario jsonHdUsuario) {
		HdUsuario hdUsuario = new HdUsuario();
		applyBasicEntityValues(hdUsuario, jsonHdUsuario);
		return hdUsuario;
	}

	private static void applyBasicJsonValues(JsonHdUsuario jsonHdUsuario, HdUsuario hdUsuario) {
		jsonHdUsuario.setId(hdUsuario.getId());
		jsonHdUsuario.setNome(hdUsuario.getNome());
		jsonHdUsuario.setEmail(hdUsuario.getEmail());
		jsonHdUsuario.setSenha(hdUsuario.getSenha());

		jsonHdUsuario.setTipo(hdUsuario.getTipo());
		jsonHdUsuario.setPerfil(hdUsuario.getPerfil());
		jsonHdUsuario.setStatusUsuario(hdUsuario.getStatusUsuario());
		jsonHdUsuario.setDatainclusao(DateUtil.localDateTimeAsString(hdUsuario.getDatainclusao()));
		jsonHdUsuario.setUsuarioinclusao(hdUsuario.getUsuarioinclusao());
	}

	private static void applyBasicEntityValues(HdUsuario hdUsuario, JsonHdUsuario jsonHdUsuario) {
		hdUsuario.setId(jsonHdUsuario.getId());
		hdUsuario.setNome(jsonHdUsuario.getNome());
		hdUsuario.setEmail(jsonHdUsuario.getEmail());
		hdUsuario.setSenha(jsonHdUsuario.getSenha());
		hdUsuario.setTipo(jsonHdUsuario.getTipo());
		hdUsuario.setPerfil(jsonHdUsuario.getPerfil());
		hdUsuario.setStatusUsuario(jsonHdUsuario.getStatusUsuario());
		hdUsuario.setDatainclusao(DateUtil.stringAsLocalDateTime(jsonHdUsuario.getDatainclusao()));
		hdUsuario.setUsuarioinclusao(jsonHdUsuario.getUsuarioinclusao());
	}

	public static JsonHdUsuario toJson(HdUsuario hdUsuario) {
		JsonHdUsuario jsonHdUsuario = new JsonHdUsuario();

		applyBasicJsonValues(jsonHdUsuario, hdUsuario);

		Empresa empresa_ = hdUsuario.getEmpresa();
		if (empresa_ != null) {
			jsonHdUsuario.setEmpresa(toBasicJson(empresa_));
		}
		return jsonHdUsuario;
	}

	public static HdUsuario apply(HdUsuario hdUsuario, JsonHdUsuario jsonHdUsuario) {

		if (hdUsuario == null)
			hdUsuario = new HdUsuario();

		applyBasicEntityValues(hdUsuario, jsonHdUsuario);

		JsonEmpresa empresa_ = jsonHdUsuario.getEmpresa();
		if (empresa_ != null) {
			hdUsuario.setEmpresa(toEntity(empresa_));
		}
		return hdUsuario;

	}

	public static HdUsuario toEntity(JsonHdUsuario jsonHdUsuario) {
		HdUsuario hdUsuario = new HdUsuario();

		return apply(hdUsuario, jsonHdUsuario);
	}

	public static List<JsonHdUsuario> toListJsonHdUsuarios(List<HdUsuario> all) {
		List<JsonHdUsuario> jsonHdUsuarios = new ArrayList<JsonHdUsuario>();
		for (HdUsuario hdUsuario : all) {
			jsonHdUsuarios.add(toJson(hdUsuario));
		}
		return jsonHdUsuarios;
	}

	// converte de entidade para json --------------------
	private static JsonMensagem toBasicJson(Mensagem mensagem) {
		JsonMensagem jsonMensagem = new JsonMensagem();
		applyBasicJsonValues(jsonMensagem, mensagem);
		return jsonMensagem;
	}

	private static Mensagem toBasicEntity(JsonMensagem jsonMensagem) {
		Mensagem mensagem = new Mensagem();
		applyBasicEntityValues(mensagem, jsonMensagem);
		return mensagem;
	}

	private static void applyBasicJsonValues(JsonMensagem jsonMensagem, Mensagem mensagem) {
		jsonMensagem.setId(mensagem.getId());
		jsonMensagem.setDescricao(mensagem.getDescricao());
	}

	private static void applyBasicEntityValues(Mensagem mensagem, JsonMensagem jsonMensagem) {
		mensagem.setId(jsonMensagem.getId());
		mensagem.setDescricao(jsonMensagem.getDescricao());
	}

	public static JsonMensagem toJson(Mensagem mensagem) {
		JsonMensagem jsonMensagem = new JsonMensagem();

		applyBasicJsonValues(jsonMensagem, mensagem);

		List<MensagemMotorista> listMensagemMotoristas = mensagem.getMensagemMotoristas();
		if (listMensagemMotoristas != null) {
			for (MensagemMotorista loopMensagemMotorista : listMensagemMotoristas) {
				jsonMensagem.getMensagemMotoristas().add(toBasicJson(loopMensagemMotorista));
			}
		}
		return jsonMensagem;
	}

	public static Mensagem apply(Mensagem mensagem, JsonMensagem jsonMensagem) {

		if (mensagem == null)
			mensagem = new Mensagem();

		applyBasicEntityValues(mensagem, jsonMensagem);

		ArrayList<JsonMensagemMotorista> listMensagemMotoristas = jsonMensagem.getMensagemMotoristas();
		if (listMensagemMotoristas != null) {
			for (JsonMensagemMotorista loopJsonMensagemMotorista : listMensagemMotoristas) {
				mensagem.addMensagemMotoristas(toBasicEntity(loopJsonMensagemMotorista));
			}
		}

		return mensagem;

	}

	public static Mensagem toEntity(JsonMensagem jsonMensagem) {
		Mensagem mensagem = new Mensagem();

		return apply(mensagem, jsonMensagem);
	}

	public static List<JsonMensagem> toListJsonMensagems(List<Mensagem> all) {
		List<JsonMensagem> jsonMensagems = new ArrayList<JsonMensagem>();
		for (Mensagem mensagem : all) {
			jsonMensagems.add(toJson(mensagem));
		}
		return jsonMensagems;
	}

	// converte de entidade para json --------------------
	private static JsonMensagemMotorista toBasicJson(MensagemMotorista mensagemMotorista) {
		JsonMensagemMotorista jsonMensagemMotorista = new JsonMensagemMotorista();
		applyBasicJsonValues(jsonMensagemMotorista, mensagemMotorista);
		return jsonMensagemMotorista;
	}

	private static MensagemMotorista toBasicEntity(JsonMensagemMotorista jsonMensagemMotorista) {
		MensagemMotorista mensagemMotorista = new MensagemMotorista();
		applyBasicEntityValues(mensagemMotorista, jsonMensagemMotorista);
		return mensagemMotorista;
	}

	private static void applyBasicJsonValues(JsonMensagemMotorista jsonMensagemMotorista, MensagemMotorista mensagemMotorista) {
		jsonMensagemMotorista.setId(mensagemMotorista.getId());
		jsonMensagemMotorista.setDataEnvio(DateUtil.localDateTimeWithSecondsAsString(mensagemMotorista.getDataEnvio()));
		jsonMensagemMotorista.setDataLeitura(DateUtil.localDateTimeWithSecondsAsString(mensagemMotorista.getDataLeitura()));
	}

	private static void applyBasicEntityValues(MensagemMotorista mensagemMotorista, JsonMensagemMotorista jsonMensagemMotorista) {
		mensagemMotorista.setId(jsonMensagemMotorista.getId());
		mensagemMotorista.setDataEnvio(DateUtil.stringAsLocalDateTime(jsonMensagemMotorista.getDataEnvio()));
		mensagemMotorista.setDataLeitura(DateUtil.stringAsLocalDateTime(jsonMensagemMotorista.getDataLeitura()));
	}

	public static JsonMensagemMotorista toJson(MensagemMotorista mensagemMotorista) {
		JsonMensagemMotorista jsonMensagemMotorista = new JsonMensagemMotorista();

		applyBasicJsonValues(jsonMensagemMotorista, mensagemMotorista);

		Motorista motorista_ = mensagemMotorista.getMotorista();
		if (motorista_ != null) {
			jsonMensagemMotorista.setMotorista(toBasicJson(motorista_));
		}
		Mensagem mensagem_ = mensagemMotorista.getMensagem();
		if (mensagem_ != null) {
			jsonMensagemMotorista.setMensagem(toBasicJson(mensagem_));
		}
		return jsonMensagemMotorista;
	}

	public static MensagemMotorista apply(MensagemMotorista mensagemMotorista, JsonMensagemMotorista jsonMensagemMotorista) {

		if (mensagemMotorista == null)
			mensagemMotorista = new MensagemMotorista();

		applyBasicEntityValues(mensagemMotorista, jsonMensagemMotorista);

		JsonMotorista motorista_ = jsonMensagemMotorista.getMotorista();
		if (motorista_ != null) {
			mensagemMotorista.setMotorista(toEntity(motorista_));
		}
		JsonMensagem mensagem_ = jsonMensagemMotorista.getMensagem();
		if (mensagem_ != null) {
			mensagemMotorista.setMensagem(toEntity(mensagem_));
		}
		return mensagemMotorista;

	}

	public static MensagemMotorista toEntity(JsonMensagemMotorista jsonMensagemMotorista) {
		MensagemMotorista mensagemMotorista = new MensagemMotorista();

		return apply(mensagemMotorista, jsonMensagemMotorista);
	}

	public static List<JsonMensagemMotorista> toListJsonMensagemMotoristas(List<MensagemMotorista> all) {
		List<JsonMensagemMotorista> jsonMensagemMotoristas = new ArrayList<JsonMensagemMotorista>();
		for (MensagemMotorista mensagemMotorista : all) {
			jsonMensagemMotoristas.add(toJson(mensagemMotorista));
		}
		return jsonMensagemMotoristas;
	}

	// converte de entidade para json --------------------
	private static JsonMotorista toBasicJson(Motorista motorista) {
		JsonMotorista jsonMotorista = new JsonMotorista();
		applyBasicJsonValues(jsonMotorista, motorista);
		return jsonMotorista;
	}

	private static Motorista toBasicEntity(JsonMotorista jsonMotorista) {
		Motorista motorista = new Motorista();
		applyBasicEntityValues(motorista, jsonMotorista);
		return motorista;
	}

	private static void applyBasicJsonValues(JsonMotorista jsonMotorista, Motorista motorista) {
		jsonMotorista.setId(motorista.getId());
		jsonMotorista.setNome(motorista.getNome());
		jsonMotorista.setNomeReduzido(motorista.getNomeReduzido());
		jsonMotorista.setViatura(motorista.getViatura());
		jsonMotorista.setEmail(motorista.getEmail());
		jsonMotorista.setDdd(motorista.getDdd());
		jsonMotorista.setFone(motorista.getFone());
		jsonMotorista.setStatusMotorista(motorista.getStatusMotorista());
		jsonMotorista.setStatusApp(motorista.getStatusApp());
		jsonMotorista.setDdd2(motorista.getDdd2());
		jsonMotorista.setFone2(motorista.getFone2());
		jsonMotorista.setLatitude(motorista.getLatitude());
		jsonMotorista.setLongitude(motorista.getLongitude());
		jsonMotorista.setDddWhatsapp(motorista.getDddWhatsapp());
		jsonMotorista.setWhatsapp(motorista.getWhatsapp());
		jsonMotorista.setDataNascimento(DateUtil.localDateAsString(motorista.getDataNascimento()));
		jsonMotorista.setRg(motorista.getRg());
		jsonMotorista.setOrgaoExpedidor(motorista.getOrgaoExpedidor());
		jsonMotorista.setDataExpedicao(DateUtil.localDateAsString(motorista.getDataExpedicao()));
		jsonMotorista.setNaturalidade(motorista.getNaturalidade());
		jsonMotorista.setCpf(motorista.getCpf());
		jsonMotorista.setCnh(motorista.getCnh());
		jsonMotorista.setEmissaoCnh(DateUtil.localDateAsString(motorista.getEmissaoCnh()));
		jsonMotorista.setCategoriaCnh(motorista.getCategoriaCnh());
		jsonMotorista.setValidadeCnh(DateUtil.localDateAsString(motorista.getValidadeCnh()));
		jsonMotorista.setFoto1(motorista.getFoto1());
		jsonMotorista.setFoto2(motorista.getFoto2());
		jsonMotorista.setFoto3(motorista.getFoto3());
		jsonMotorista.setEndereco(motorista.getEndereco());
		jsonMotorista.setNumeroEndereco(motorista.getNumeroEndereco());
		jsonMotorista.setComplemento(motorista.getComplemento());
		jsonMotorista.setUf(motorista.getUf());
		jsonMotorista.setCidade(motorista.getCidade());
		jsonMotorista.setBairro(motorista.getBairro());
		jsonMotorista.setCep(motorista.getCep());
		jsonMotorista.setReferenciaEndereco(motorista.getReferenciaEndereco());
		jsonMotorista.setMae(motorista.getMae());
		jsonMotorista.setPai(motorista.getPai());
		jsonMotorista.setObservacao(motorista.getObservacao());
		jsonMotorista.setTempoUltimaPosicao(DateUtil.localDateAsString(motorista.getTempoUltimaPosicao()));
		jsonMotorista.setConjuge(motorista.getConjuge());
		jsonMotorista.setReferenciaPessoal1(motorista.getReferenciaPessoal1());
		jsonMotorista.setFoneReferencia1(motorista.getFoneReferencia1());
		jsonMotorista.setParentescoReferencia1(motorista.getParentescoReferencia1());
		jsonMotorista.setReferenciaPessoal2(motorista.getReferenciaPessoal2());
		jsonMotorista.setFoneReferencia2(motorista.getFoneReferencia2());
		jsonMotorista.setParentescoReferencia2(motorista.getParentescoReferencia2());
		jsonMotorista.setMarcaVeiculo(motorista.getMarcaVeiculo());
		jsonMotorista.setModeloVeiculo(motorista.getModeloVeiculo());
		jsonMotorista.setPlacaVeiculo(motorista.getPlacaVeiculo());
		jsonMotorista.setCorVeiculo(motorista.getCorVeiculo());
		jsonMotorista.setAnoVeiculo(motorista.getAnoVeiculo());
		jsonMotorista.setRenavam(motorista.getRenavam());
		jsonMotorista.setChassi(motorista.getChassi());
		jsonMotorista.setCategoria(motorista.getCategoria());
		jsonMotorista.setImei(motorista.getImei());
		jsonMotorista.setCartaoCredito(motorista.getCartaoCredito());
		jsonMotorista.setCartaoDebito(motorista.getCartaoDebito());
		jsonMotorista.setVoucher(motorista.getVoucher());
		jsonMotorista.setEticket(motorista.getEticket());
		jsonMotorista.setSenha(motorista.getSenha());
		jsonMotorista.setPadronizada(motorista.getPadronizada());
		jsonMotorista.setBau(motorista.getBau());
		jsonMotorista.setMotoGrande(motorista.getMotoGrande());
	}

	private static void applyBasicEntityValues(Motorista motorista, JsonMotorista jsonMotorista) {
		motorista.setId(jsonMotorista.getId());
		motorista.setNome(jsonMotorista.getNome());
		motorista.setNomeReduzido(jsonMotorista.getNomeReduzido());
		motorista.setViatura(jsonMotorista.getViatura());
		motorista.setEmail(jsonMotorista.getEmail());
		motorista.setDdd(jsonMotorista.getDdd());
		motorista.setFone(jsonMotorista.getFone());
		motorista.setLatitude(jsonMotorista.getLatitude());
		motorista.setLongitude(jsonMotorista.getLongitude());
		motorista.setStatusMotorista(jsonMotorista.getStatusMotorista());
		motorista.setStatusApp(jsonMotorista.getStatusApp());
		motorista.setDdd2(jsonMotorista.getDdd2());
		motorista.setFone2(jsonMotorista.getFone2());
		motorista.setDddWhatsapp(jsonMotorista.getDddWhatsapp());
		motorista.setWhatsapp(jsonMotorista.getWhatsapp());
		motorista.setDataNascimento(DateUtil.stringAsLocalDate(jsonMotorista.getDataNascimento()));
		motorista.setRg(jsonMotorista.getRg());
		motorista.setOrgaoExpedidor(jsonMotorista.getOrgaoExpedidor());
		motorista.setDataExpedicao(DateUtil.stringAsLocalDate(jsonMotorista.getDataExpedicao()));
		motorista.setNaturalidade(jsonMotorista.getNaturalidade());
		motorista.setCpf(jsonMotorista.getCpf());
		motorista.setCnh(jsonMotorista.getCnh());
		motorista.setEmissaoCnh(DateUtil.stringAsLocalDate(jsonMotorista.getEmissaoCnh()));
		motorista.setCategoriaCnh(jsonMotorista.getCategoriaCnh());
		motorista.setValidadeCnh(DateUtil.stringAsLocalDate(jsonMotorista.getValidadeCnh()));
		motorista.setFoto1(jsonMotorista.getFoto1());
		motorista.setFoto2(jsonMotorista.getFoto2());
		motorista.setFoto3(jsonMotorista.getFoto3());
		motorista.setEndereco(jsonMotorista.getEndereco());
		motorista.setNumeroEndereco(jsonMotorista.getNumeroEndereco());
		motorista.setComplemento(jsonMotorista.getComplemento());
		motorista.setUf(jsonMotorista.getUf());
		motorista.setCidade(jsonMotorista.getCidade());
		motorista.setBairro(jsonMotorista.getBairro());
		motorista.setCep(jsonMotorista.getCep());
		motorista.setReferenciaEndereco(jsonMotorista.getReferenciaEndereco());
		motorista.setMae(jsonMotorista.getMae());
		motorista.setPai(jsonMotorista.getPai());
		motorista.setConjuge(jsonMotorista.getConjuge());
		motorista.setReferenciaPessoal1(jsonMotorista.getReferenciaPessoal1());
		motorista.setFoneReferencia1(jsonMotorista.getFoneReferencia1());
		motorista.setParentescoReferencia1(jsonMotorista.getParentescoReferencia1());
		motorista.setReferenciaPessoal2(jsonMotorista.getReferenciaPessoal2());
		motorista.setFoneReferencia2(jsonMotorista.getFoneReferencia2());
		motorista.setParentescoReferencia2(jsonMotorista.getParentescoReferencia2());
		motorista.setMarcaVeiculo(jsonMotorista.getMarcaVeiculo());
		motorista.setModeloVeiculo(jsonMotorista.getModeloVeiculo());
		motorista.setPlacaVeiculo(jsonMotorista.getPlacaVeiculo());
		motorista.setObservacao(jsonMotorista.getObservacao());
		motorista.setTempoUltimaPosicao(DateUtil.stringAsLocalDate(jsonMotorista.getTempoUltimaPosicao()));
		motorista.setCorVeiculo(jsonMotorista.getCorVeiculo());
		motorista.setAnoVeiculo(jsonMotorista.getAnoVeiculo());
		motorista.setRenavam(jsonMotorista.getRenavam());
		motorista.setChassi(jsonMotorista.getChassi());
		motorista.setCategoria(jsonMotorista.getCategoria());
		motorista.setImei(jsonMotorista.getImei());
		motorista.setCartaoCredito(jsonMotorista.getCartaoCredito());
		motorista.setCartaoDebito(jsonMotorista.getCartaoDebito());
		motorista.setVoucher(jsonMotorista.getVoucher());
		motorista.setEticket(jsonMotorista.getEticket());
		motorista.setSenha(jsonMotorista.getSenha());
		motorista.setPadronizada(jsonMotorista.getPadronizada());
		motorista.setBau(jsonMotorista.getBau());
		motorista.setMotoGrande(jsonMotorista.getMotoGrande());
	}

	public static JsonMotorista toJson(Motorista motorista) {
		JsonMotorista jsonMotorista = new JsonMotorista();

		applyBasicJsonValues(jsonMotorista, motorista);

		List<Punicao> listPunicaos = motorista.getPunicaos();
		if (listPunicaos != null) {
			for (Punicao loopPunicao : listPunicaos) {
				jsonMotorista.getPunicaos().add(toBasicJson(loopPunicao));
			}
		}
		List<MensagemMotorista> listMensagemMotoristas = motorista.getMensagemMotoristas();
		if (listMensagemMotoristas != null) {
			for (MensagemMotorista loopMensagemMotorista : listMensagemMotoristas) {
				jsonMotorista.getMensagemMotoristas().add(toBasicJson(loopMensagemMotorista));
			}
		}
		List<MotoristaBloqueado> listMotoristaBloqueados = motorista.getMotoristaBloqueados();
		if (listMotoristaBloqueados != null) {
			for (MotoristaBloqueado loopMotoristaBloqueado : listMotoristaBloqueados) {
				jsonMotorista.getMotoristaBloqueados().add(toBasicJson(loopMotoristaBloqueado));
			}
		}
		List<Corrida> listCorridas = motorista.getCorridas();
		if (listCorridas != null) {
			for (Corrida loopCorrida : listCorridas) {
				jsonMotorista.getCorridas().add(toBasicJson(loopCorrida));
			}
		}
		List<Plantao> listPlantoes = motorista.getPlantoes();
		if (listPlantoes != null) {
			for (Plantao loopPlantao : listPlantoes) {
				jsonMotorista.getPlantoes().add(toBasicJson(loopPlantao));
			}
		}

		return jsonMotorista;
	}

	public static Motorista apply(Motorista motorista, JsonMotorista jsonMotorista) {

		if (motorista == null)
			motorista = new Motorista();

		applyBasicEntityValues(motorista, jsonMotorista);

		ArrayList<JsonPunicao> listPunicaos = jsonMotorista.getPunicaos();
		if (listPunicaos != null) {
			for (JsonPunicao loopJsonPunicao : listPunicaos) {
				motorista.addPunicaos(toBasicEntity(loopJsonPunicao));
			}
		}

		ArrayList<JsonMensagemMotorista> listMensagemMotoristas = jsonMotorista.getMensagemMotoristas();
		if (listMensagemMotoristas != null) {
			for (JsonMensagemMotorista loopJsonMensagemMotorista : listMensagemMotoristas) {
				motorista.addMensagemMotoristas(toBasicEntity(loopJsonMensagemMotorista));
			}
		}

		ArrayList<JsonMotoristaBloqueado> listMotoristaBloqueados = jsonMotorista.getMotoristaBloqueados();
		if (listMotoristaBloqueados != null) {
			for (JsonMotoristaBloqueado loopJsonMotoristaBloqueado : listMotoristaBloqueados) {
				motorista.addMotoristaBloqueados(toBasicEntity(loopJsonMotoristaBloqueado));
			}
		}

		ArrayList<JsonCorrida> listCorridas = jsonMotorista.getCorridas();
		if (listCorridas != null) {
			for (JsonCorrida loopJsonCorrida : listCorridas) {
				motorista.addCorridas(toBasicEntity(loopJsonCorrida));
			}
		}

		ArrayList<JsonPlantao> listPlantoes = jsonMotorista.getPlantoes();
		if (listPlantoes != null) {
			for (JsonPlantao loopJsonPlantao : listPlantoes) {
				motorista.addPlantoes(toBasicEntity(loopJsonPlantao));
			}
		}

		return motorista;

	}

	public static Motorista toEntity(JsonMotorista jsonMotorista) {
		Motorista motorista = new Motorista();

		return apply(motorista, jsonMotorista);
	}

	public static List<JsonMotorista> toListJsonMotoristas(List<Motorista> all) {
		List<JsonMotorista> jsonMotoristas = new ArrayList<JsonMotorista>();
		for (Motorista motorista : all) {
			jsonMotoristas.add(toJson(motorista));
		}
		return jsonMotoristas;
	}

	// converte de entidade para json --------------------
	private static JsonMotoristaBloqueado toBasicJson(MotoristaBloqueado motoristaBloqueado) {
		JsonMotoristaBloqueado jsonMotoristaBloqueado = new JsonMotoristaBloqueado();
		applyBasicJsonValues(jsonMotoristaBloqueado, motoristaBloqueado);
		return jsonMotoristaBloqueado;
	}

	private static MotoristaBloqueado toBasicEntity(JsonMotoristaBloqueado jsonMotoristaBloqueado) {
		MotoristaBloqueado motoristaBloqueado = new MotoristaBloqueado();
		applyBasicEntityValues(motoristaBloqueado, jsonMotoristaBloqueado);
		return motoristaBloqueado;
	}

	private static void applyBasicJsonValues(JsonMotoristaBloqueado jsonMotoristaBloqueado, MotoristaBloqueado motoristaBloqueado) {
		jsonMotoristaBloqueado.setId(motoristaBloqueado.getId());
		jsonMotoristaBloqueado.setDataBloqueio(DateUtil.localDateTimeAsString(motoristaBloqueado.getDataBloqueio()));
		jsonMotoristaBloqueado.setUsuarioBloqueio(motoristaBloqueado.getUsuarioBloqueio());
	}

	private static void applyBasicEntityValues(MotoristaBloqueado motoristaBloqueado, JsonMotoristaBloqueado jsonMotoristaBloqueado) {
		motoristaBloqueado.setId(jsonMotoristaBloqueado.getId());
		motoristaBloqueado.setDataBloqueio(DateUtil.stringAsLocalDateTime(jsonMotoristaBloqueado.getDataBloqueio()));
		motoristaBloqueado.setUsuarioBloqueio(jsonMotoristaBloqueado.getUsuarioBloqueio());
	}

	public static JsonMotoristaBloqueado toJson(MotoristaBloqueado motoristaBloqueado) {
		JsonMotoristaBloqueado jsonMotoristaBloqueado = new JsonMotoristaBloqueado();

		applyBasicJsonValues(jsonMotoristaBloqueado, motoristaBloqueado);

		Empresa empresa_ = motoristaBloqueado.getEmpresa();
		if (empresa_ != null)
			jsonMotoristaBloqueado.setEmpresa(toBasicJson(empresa_));

		Cliente cliente_ = motoristaBloqueado.getCliente();
		if (cliente_ != null) {
			jsonMotoristaBloqueado.setCliente(toBasicJson(cliente_));
		}
		Motorista motorista_ = motoristaBloqueado.getMotorista();
		if (motorista_ != null) {
			jsonMotoristaBloqueado.setMotorista(toBasicJson(motorista_));
		}
		return jsonMotoristaBloqueado;
	}

	public static MotoristaBloqueado apply(MotoristaBloqueado motoristaBloqueado, JsonMotoristaBloqueado jsonMotoristaBloqueado) {

		if (motoristaBloqueado == null)
			motoristaBloqueado = new MotoristaBloqueado();

		applyBasicEntityValues(motoristaBloqueado, jsonMotoristaBloqueado);

		JsonEmpresa empresa_ = jsonMotoristaBloqueado.getEmpresa();
		if (empresa_ != null)
			motoristaBloqueado.setEmpresa(toEntity(empresa_));

		JsonCliente cliente_ = jsonMotoristaBloqueado.getCliente();
		if (cliente_ != null) {
			motoristaBloqueado.setCliente(toEntity(cliente_));
		}
		JsonMotorista motorista_ = jsonMotoristaBloqueado.getMotorista();
		if (motorista_ != null) {
			motoristaBloqueado.setMotorista(toEntity(motorista_));
		}
		return motoristaBloqueado;

	}

	public static MotoristaBloqueado toEntity(JsonMotoristaBloqueado jsonMotoristaBloqueado) {
		MotoristaBloqueado motoristaBloqueado = new MotoristaBloqueado();

		return apply(motoristaBloqueado, jsonMotoristaBloqueado);
	}

	public static List<JsonMotoristaBloqueado> toListJsonMotoristaBloqueados(List<MotoristaBloqueado> all) {
		List<JsonMotoristaBloqueado> jsonMotoristaBloqueados = new ArrayList<JsonMotoristaBloqueado>();
		for (MotoristaBloqueado motoristaBloqueado : all) {
			jsonMotoristaBloqueados.add(toJson(motoristaBloqueado));
		}
		return jsonMotoristaBloqueados;
	}

	// converte de entidade para json --------------------
	private static JsonPlantao toBasicJson(Plantao plantao) {
		JsonPlantao jsonPlantao = new JsonPlantao();
		applyBasicJsonValues(jsonPlantao, plantao);
		return jsonPlantao;
	}

	private static Plantao toBasicEntity(JsonPlantao jsonPlantao) {
		Plantao plantao = new Plantao();
		applyBasicEntityValues(plantao, jsonPlantao);
		return plantao;
	}

	private static void applyBasicJsonValues(JsonPlantao jsonPlantao, Plantao plantao) {
		jsonPlantao.setId(plantao.getId());
		jsonPlantao.setDataInicio(DateUtil.localDateTimeAsString(plantao.getDataInicio()));
		jsonPlantao.setDataFim(DateUtil.localDateTimeAsString(plantao.getDataFim()));
	}

	private static void applyBasicEntityValues(Plantao plantao, JsonPlantao jsonPlantao) {
		plantao.setId(jsonPlantao.getId());
		plantao.setDataInicio(DateUtil.stringAsLocalDateTime(jsonPlantao.getDataInicio()));
		plantao.setDataFim(DateUtil.stringAsLocalDateTime(jsonPlantao.getDataFim()));
	}

	public static JsonPlantao toJson(Plantao plantao) {
		JsonPlantao jsonPlantao = new JsonPlantao();

		applyBasicJsonValues(jsonPlantao, plantao);

		List<Motorista> listMotoristas = plantao.getMotoristas();
		if (listMotoristas != null) {
			for (Motorista loopMotorista : listMotoristas) {
				jsonPlantao.getMotoristas().add(toBasicJson(loopMotorista));
			}
		}

		return jsonPlantao;
	}

	public static Plantao apply(Plantao plantao, JsonPlantao jsonPlantao) {

		if (plantao == null)
			plantao = new Plantao();

		applyBasicEntityValues(plantao, jsonPlantao);

		ArrayList<JsonMotorista> listMotoristas = jsonPlantao.getMotoristas();
		if (listMotoristas != null) {
			for (JsonMotorista loopJsonMotorista : listMotoristas) {
				plantao.addMotoristas(toEntity(loopJsonMotorista));
			}
		}
		return plantao;

	}

	public static Plantao toEntity(JsonPlantao jsonPlantao) {
		Plantao plantao = new Plantao();

		return apply(plantao, jsonPlantao);
	}

	public static List<JsonPlantao> toListJsonPlantaos(List<Plantao> all) {
		List<JsonPlantao> jsonPlantaos = new ArrayList<JsonPlantao>();
		for (Plantao plantao : all) {
			jsonPlantaos.add(toJson(plantao));
		}
		return jsonPlantaos;
	}

	// converte de entidade para json --------------------
	private static JsonPontosArea toBasicJson(PontosArea pontosArea) {
		JsonPontosArea jsonPontosArea = new JsonPontosArea();
		applyBasicJsonValues(jsonPontosArea, pontosArea);
		return jsonPontosArea;
	}

	private static PontosArea toBasicEntity(JsonPontosArea jsonPontosArea) {
		PontosArea pontosArea = new PontosArea();
		applyBasicEntityValues(pontosArea, jsonPontosArea);
		return pontosArea;
	}

	private static void applyBasicJsonValues(JsonPontosArea jsonPontosArea, PontosArea pontosArea) {
		jsonPontosArea.setId(pontosArea.getId());
		jsonPontosArea.setLatitude(pontosArea.getLatitude());
		jsonPontosArea.setLongitude(pontosArea.getLongitude());
	}

	private static void applyBasicEntityValues(PontosArea pontosArea, JsonPontosArea jsonPontosArea) {
		pontosArea.setId(jsonPontosArea.getId());
		pontosArea.setLatitude(jsonPontosArea.getLatitude());
		pontosArea.setLongitude(jsonPontosArea.getLongitude());
	}

	public static JsonPontosArea toJson(PontosArea pontosArea) {
		JsonPontosArea jsonPontosArea = new JsonPontosArea();

		applyBasicJsonValues(jsonPontosArea, pontosArea);

		Area area_ = pontosArea.getArea();
		if (area_ != null) {
			jsonPontosArea.setArea(toBasicJson(area_));
		}
		return jsonPontosArea;
	}

	public static PontosArea apply(PontosArea pontosArea, JsonPontosArea jsonPontosArea) {

		if (pontosArea == null)
			pontosArea = new PontosArea();

		applyBasicEntityValues(pontosArea, jsonPontosArea);

		JsonArea area_ = jsonPontosArea.getArea();
		if (area_ != null) {
			pontosArea.setArea(toEntity(area_));
		}
		return pontosArea;

	}

	public static PontosArea toEntity(JsonPontosArea jsonPontosArea) {
		PontosArea pontosArea = new PontosArea();

		return apply(pontosArea, jsonPontosArea);
	}

	public static List<JsonPontosArea> toListJsonPontosAreas(List<PontosArea> all) {
		List<JsonPontosArea> jsonPontosAreas = new ArrayList<JsonPontosArea>();
		for (PontosArea pontosArea : all) {
			jsonPontosAreas.add(toJson(pontosArea));
		}
		return jsonPontosAreas;
	}

	// converte de entidade para json --------------------
	private static JsonPunicao toBasicJson(Punicao punicao) {
		JsonPunicao jsonPunicao = new JsonPunicao();
		applyBasicJsonValues(jsonPunicao, punicao);
		return jsonPunicao;
	}

	private static Punicao toBasicEntity(JsonPunicao jsonPunicao) {
		Punicao punicao = new Punicao();
		applyBasicEntityValues(punicao, jsonPunicao);
		return punicao;
	}

	private static void applyBasicJsonValues(JsonPunicao jsonPunicao, Punicao punicao) {
		jsonPunicao.setId(punicao.getId());

		jsonPunicao.setDataInicio(DateUtil.localDateTimeAsString(punicao.getDataInicio()));
		jsonPunicao.setDataFim(DateUtil.localDateTimeAsString(punicao.getDataFim()));
		jsonPunicao.setQuantidadetempo(punicao.getQuantidadetempo());
		jsonPunicao.setStatusPunicao(punicao.getStatusPunicao());
	}

	private static void applyBasicEntityValues(Punicao punicao, JsonPunicao jsonPunicao) {
		punicao.setId(jsonPunicao.getId());

		punicao.setDataInicio(DateUtil.stringAsLocalDateTime(jsonPunicao.getDataInicio()));
		punicao.setDataFim(DateUtil.stringAsLocalDateTime(jsonPunicao.getDataFim()));
		punicao.setQuantidadetempo(jsonPunicao.getQuantidadetempo());
		punicao.setStatusPunicao(jsonPunicao.getStatusPunicao());
	}

	public static JsonPunicao toJson(Punicao punicao) {
		JsonPunicao jsonPunicao = new JsonPunicao();

		applyBasicJsonValues(jsonPunicao, punicao);

		MotivoPunicao _motivo = punicao.getMotivo();
		if (_motivo != null) {
			jsonPunicao.setMotivo(toBasicJson(_motivo));
		}
		Motorista motorista_ = punicao.getMotorista();
		if (motorista_ != null) {
			jsonPunicao.setMotorista(toBasicJson(motorista_));
		}
		return jsonPunicao;
	}

	public static Punicao apply(Punicao punicao, JsonPunicao jsonPunicao) {

		if (punicao == null)
			punicao = new Punicao();

		applyBasicEntityValues(punicao, jsonPunicao);

		JsonMotivoPunicao _motivo = jsonPunicao.getMotivo();
		if (_motivo != null) {
			punicao.setMotivo(toBasicEntity(_motivo));
		}
		JsonMotorista motorista_ = jsonPunicao.getMotorista();
		if (motorista_ != null) {
			punicao.setMotorista(toEntity(motorista_));
		}
		return punicao;

	}

	public static Punicao toEntity(JsonPunicao jsonPunicao) {
		Punicao punicao = new Punicao();

		return apply(punicao, jsonPunicao);
	}

	public static List<JsonPunicao> toListJsonPunicaos(List<Punicao> all) {
		List<JsonPunicao> jsonPunicaos = new ArrayList<JsonPunicao>();
		for (Punicao punicao : all) {
			jsonPunicaos.add(toJson(punicao));
		}
		return jsonPunicaos;
	}

	// converte de entidade para json --------------------
	private static JsonSocio toBasicJson(Socio socio) {
		JsonSocio jsonSocio = new JsonSocio();
		applyBasicJsonValues(jsonSocio, socio);
		return jsonSocio;
	}

	private static Socio toBasicEntity(JsonSocio jsonSocio) {
		Socio socio = new Socio();
		applyBasicEntityValues(socio, jsonSocio);
		return socio;
	}

	private static void applyBasicJsonValues(JsonSocio jsonSocio, Socio socio) {
		jsonSocio.setId(socio.getId());
		jsonSocio.setNome(socio.getNome());
		jsonSocio.setCpf(socio.getCpf());
	}

	private static void applyBasicEntityValues(Socio socio, JsonSocio jsonSocio) {
		socio.setId(jsonSocio.getId());
		socio.setNome(jsonSocio.getNome());
		socio.setCpf(jsonSocio.getCpf());
	}

	public static JsonSocio toJson(Socio socio) {
		JsonSocio jsonSocio = new JsonSocio();

		applyBasicJsonValues(jsonSocio, socio);

		Empresa empresa_ = socio.getEmpresa();
		if (empresa_ != null) {
			jsonSocio.setEmpresa(toBasicJson(empresa_));
		}
		return jsonSocio;
	}

	public static Socio apply(Socio socio, JsonSocio jsonSocio) {

		if (socio == null)
			socio = new Socio();

		applyBasicEntityValues(socio, jsonSocio);

		JsonEmpresa empresa_ = jsonSocio.getEmpresa();
		if (empresa_ != null) {
			socio.setEmpresa(toEntity(empresa_));
		}
		return socio;

	}

	public static Socio toEntity(JsonSocio jsonSocio) {
		Socio socio = new Socio();

		return apply(socio, jsonSocio);
	}

	public static List<JsonSocio> toListJsonSocios(List<Socio> all) {
		List<JsonSocio> jsonSocios = new ArrayList<JsonSocio>();
		for (Socio socio : all) {
			jsonSocios.add(toJson(socio));
		}
		return jsonSocios;
	}

	// converte de entidade para json --------------------
	private static JsonStatusTracker toBasicJson(StatusTracker statusTracker) {
		JsonStatusTracker jsonStatusTracker = new JsonStatusTracker();
		applyBasicJsonValues(jsonStatusTracker, statusTracker);
		return jsonStatusTracker;
	}

	private static StatusTracker toBasicEntity(JsonStatusTracker jsonStatusTracker) {
		StatusTracker statusTracker = new StatusTracker();
		applyBasicEntityValues(statusTracker, jsonStatusTracker);
		return statusTracker;
	}

	private static void applyBasicJsonValues(JsonStatusTracker jsonStatusTracker, StatusTracker statusTracker) {
		jsonStatusTracker.setLatitude(statusTracker.getLatitude());
		jsonStatusTracker.setLongitude(statusTracker.getLongitude());
		jsonStatusTracker.setTimestamp(statusTracker.getTimestamp());
		jsonStatusTracker.setSpeed(statusTracker.getSpeed());
		jsonStatusTracker.setAccuracy(statusTracker.getAccuracy());
		jsonStatusTracker.setDirection(statusTracker.getDirection());
		jsonStatusTracker.setAltitude(statusTracker.getAltitude());
		jsonStatusTracker.setBateryLevel(statusTracker.getBateryLevel());
		jsonStatusTracker.setGpsEnabled(statusTracker.getGpsEnabled());
		jsonStatusTracker.setWifiEnabled(statusTracker.getWifiEnabled());
		jsonStatusTracker.setMobileEnabled(statusTracker.getMobileEnabled());
	}

	private static void applyBasicEntityValues(StatusTracker statusTracker, JsonStatusTracker jsonStatusTracker) {
		statusTracker.setLatitude(jsonStatusTracker.getLatitude());
		statusTracker.setLongitude(jsonStatusTracker.getLongitude());
		statusTracker.setTimestamp(jsonStatusTracker.getTimestamp());
		statusTracker.setSpeed(jsonStatusTracker.getSpeed());
		statusTracker.setAccuracy(jsonStatusTracker.getAccuracy());
		statusTracker.setDirection(jsonStatusTracker.getDirection());
		statusTracker.setAltitude(jsonStatusTracker.getAltitude());
		statusTracker.setBateryLevel(jsonStatusTracker.getBateryLevel());
		statusTracker.setGpsEnabled(jsonStatusTracker.getGpsEnabled());
		statusTracker.setWifiEnabled(jsonStatusTracker.getWifiEnabled());
		statusTracker.setMobileEnabled(jsonStatusTracker.getMobileEnabled());
	}

	public static JsonStatusTracker toJson(StatusTracker statusTracker) {
		JsonStatusTracker jsonStatusTracker = new JsonStatusTracker();

		applyBasicJsonValues(jsonStatusTracker, statusTracker);

		Motorista motorista_ = statusTracker.getMotorista();
		if (motorista_ != null) {
			jsonStatusTracker.setMotorista(toBasicJson(motorista_));
		}
		return jsonStatusTracker;
	}

	public static StatusTracker apply(StatusTracker statusTracker, JsonStatusTracker jsonStatusTracker) {

		if (statusTracker == null)
			statusTracker = new StatusTracker();

		applyBasicEntityValues(statusTracker, jsonStatusTracker);

		JsonMotorista motorista_ = jsonStatusTracker.getMotorista();
		if (motorista_ != null) {
			statusTracker.setMotorista(toEntity(motorista_));
		}
		return statusTracker;

	}

	public static StatusTracker toEntity(JsonStatusTracker jsonStatusTracker) {
		StatusTracker statusTracker = new StatusTracker();

		return apply(statusTracker, jsonStatusTracker);
	}

	public static List<JsonStatusTracker> toListJsonStatusTrackers(List<StatusTracker> all) {
		List<JsonStatusTracker> jsonStatusTrackers = new ArrayList<JsonStatusTracker>();
		for (StatusTracker statusTracker : all) {
			jsonStatusTrackers.add(toJson(statusTracker));
		}
		return jsonStatusTrackers;
	}

	// converte de entidade para json --------------------
	private static JsonTarifa toBasicJson(Tarifa tarifa) {
		JsonTarifa jsonTarifa = new JsonTarifa();
		applyBasicJsonValues(jsonTarifa, tarifa);
		return jsonTarifa;
	}

	private static Tarifa toBasicEntity(JsonTarifa jsonTarifa) {
		Tarifa tarifa = new Tarifa();
		applyBasicEntityValues(tarifa, jsonTarifa);
		return tarifa;
	}

	private static void applyBasicJsonValues(JsonTarifa jsonTarifa, Tarifa tarifa) {
		jsonTarifa.setId(tarifa.getId());
		jsonTarifa.setBairroOrigem(tarifa.getBairroOrigem());
		jsonTarifa.setBairroDestino(tarifa.getBairroDestino());
		jsonTarifa.setValor(tarifa.getValor());
	}

	private static void applyBasicEntityValues(Tarifa tarifa, JsonTarifa jsonTarifa) {
		tarifa.setId(jsonTarifa.getId());
		tarifa.setBairroOrigem(jsonTarifa.getBairroOrigem());
		tarifa.setBairroDestino(jsonTarifa.getBairroDestino());
		tarifa.setValor(jsonTarifa.getValor());
	}

	public static JsonTarifa toJson(Tarifa tarifa) {
		JsonTarifa jsonTarifa = new JsonTarifa();

		applyBasicJsonValues(jsonTarifa, tarifa);

		return jsonTarifa;
	}

	public static Tarifa apply(Tarifa tarifa, JsonTarifa jsonTarifa) {

		if (tarifa == null)
			tarifa = new Tarifa();

		applyBasicEntityValues(tarifa, jsonTarifa);

		return tarifa;

	}

	public static Tarifa toEntity(JsonTarifa jsonTarifa) {
		Tarifa tarifa = new Tarifa();

		return apply(tarifa, jsonTarifa);
	}

	public static List<JsonTarifa> toListJsonTarifas(List<Tarifa> all) {
		List<JsonTarifa> jsonTarifas = new ArrayList<JsonTarifa>();
		for (Tarifa tarifa : all) {
			jsonTarifas.add(toJson(tarifa));
		}
		return jsonTarifas;
	}

	// converte de entidade para json --------------------
	private static JsonTarifaAdicional toBasicJson(TarifaAdicional tarifaAdicional) {
		JsonTarifaAdicional jsonTarifaAdicional = new JsonTarifaAdicional();
		applyBasicJsonValues(jsonTarifaAdicional, tarifaAdicional);
		return jsonTarifaAdicional;
	}

	private static TarifaAdicional toBasicEntity(JsonTarifaAdicional jsonTarifaAdicional) {
		TarifaAdicional tarifaAdicional = new TarifaAdicional();
		applyBasicEntityValues(tarifaAdicional, jsonTarifaAdicional);
		return tarifaAdicional;
	}

	private static void applyBasicJsonValues(JsonTarifaAdicional jsonTarifaAdicional, TarifaAdicional tarifaAdicional) {
		jsonTarifaAdicional.setId(tarifaAdicional.getId());
		jsonTarifaAdicional.setDescricao(tarifaAdicional.getDescricao());
		jsonTarifaAdicional.setValor(tarifaAdicional.getValor());
	}

	private static void applyBasicEntityValues(TarifaAdicional tarifaAdicional, JsonTarifaAdicional jsonTarifaAdicional) {
		tarifaAdicional.setId(jsonTarifaAdicional.getId());
		tarifaAdicional.setDescricao(jsonTarifaAdicional.getDescricao());
		tarifaAdicional.setValor(jsonTarifaAdicional.getValor());
	}

	public static JsonTarifaAdicional toJson(TarifaAdicional tarifaAdicional) {
		JsonTarifaAdicional jsonTarifaAdicional = new JsonTarifaAdicional();

		applyBasicJsonValues(jsonTarifaAdicional, tarifaAdicional);

		return jsonTarifaAdicional;
	}

	public static TarifaAdicional apply(TarifaAdicional tarifaAdicional, JsonTarifaAdicional jsonTarifaAdicional) {

		if (tarifaAdicional == null)
			tarifaAdicional = new TarifaAdicional();

		applyBasicEntityValues(tarifaAdicional, jsonTarifaAdicional);

		return tarifaAdicional;

	}

	public static TarifaAdicional toEntity(JsonTarifaAdicional jsonTarifaAdicional) {
		TarifaAdicional tarifaAdicional = new TarifaAdicional();

		return apply(tarifaAdicional, jsonTarifaAdicional);
	}

	public static List<JsonTarifaAdicional> toListJsonTarifaAdicionals(List<TarifaAdicional> all) {
		List<JsonTarifaAdicional> jsonTarifaAdicionals = new ArrayList<JsonTarifaAdicional>();
		for (TarifaAdicional tarifaAdicional : all) {
			jsonTarifaAdicionals.add(toJson(tarifaAdicional));
		}
		return jsonTarifaAdicionals;
	}

	// converte de entidade para json --------------------
	private static JsonVisitaTracker toBasicJson(VisitaTracker visitaTracker) {
		JsonVisitaTracker jsonVisitaTracker = new JsonVisitaTracker();
		applyBasicJsonValues(jsonVisitaTracker, visitaTracker);
		return jsonVisitaTracker;
	}

	private static VisitaTracker toBasicEntity(JsonVisitaTracker jsonVisitaTracker) {
		VisitaTracker visitaTracker = new VisitaTracker();
		applyBasicEntityValues(visitaTracker, jsonVisitaTracker);
		return visitaTracker;
	}

	private static void applyBasicJsonValues(JsonVisitaTracker jsonVisitaTracker, VisitaTracker visitaTracker) {
		jsonVisitaTracker.setId(visitaTracker.getId());
		jsonVisitaTracker.setLatitude(visitaTracker.getLatitude());
		jsonVisitaTracker.setLongitude(visitaTracker.getLongitude());
		jsonVisitaTracker.setTimestamp(visitaTracker.getTimestamp());
		jsonVisitaTracker.setSpeed(visitaTracker.getSpeed());
		jsonVisitaTracker.setAccuracy(visitaTracker.getAccuracy());
		jsonVisitaTracker.setDirection(visitaTracker.getDirection());
		jsonVisitaTracker.setAltitude(visitaTracker.getAltitude());
		jsonVisitaTracker.setBaterryLevel(visitaTracker.getBaterryLevel());
		jsonVisitaTracker.setGpsEnabled(visitaTracker.getGpsEnabled());
		jsonVisitaTracker.setWifiEnabled(visitaTracker.getWifiEnabled());
		jsonVisitaTracker.setMobileEnabled(visitaTracker.getMobileEnabled());
	}

	private static void applyBasicEntityValues(VisitaTracker visitaTracker, JsonVisitaTracker jsonVisitaTracker) {
		visitaTracker.setId(jsonVisitaTracker.getId());
		visitaTracker.setLatitude(jsonVisitaTracker.getLatitude());
		visitaTracker.setLongitude(jsonVisitaTracker.getLongitude());
		visitaTracker.setTimestamp(jsonVisitaTracker.getTimestamp());
		visitaTracker.setSpeed(jsonVisitaTracker.getSpeed());
		visitaTracker.setAccuracy(jsonVisitaTracker.getAccuracy());
		visitaTracker.setDirection(jsonVisitaTracker.getDirection());
		visitaTracker.setAltitude(jsonVisitaTracker.getAltitude());
		visitaTracker.setBaterryLevel(jsonVisitaTracker.getBaterryLevel());
		visitaTracker.setGpsEnabled(jsonVisitaTracker.getGpsEnabled());
		visitaTracker.setWifiEnabled(jsonVisitaTracker.getWifiEnabled());
		visitaTracker.setMobileEnabled(jsonVisitaTracker.getMobileEnabled());
	}

	public static JsonVisitaTracker toJson(VisitaTracker visitaTracker) {
		JsonVisitaTracker jsonVisitaTracker = new JsonVisitaTracker();

		applyBasicJsonValues(jsonVisitaTracker, visitaTracker);

		List<Evento> listEventos = visitaTracker.getEventosSaida();
		if (listEventos != null) {
			for (Evento loopEvento : listEventos) {
				jsonVisitaTracker.getEventosSaida().add(toBasicJson(loopEvento));
			}
		}

		return jsonVisitaTracker;
	}

	public static VisitaTracker apply(VisitaTracker visitaTracker, JsonVisitaTracker jsonVisitaTracker) {

		if (visitaTracker == null)
			visitaTracker = new VisitaTracker();

		applyBasicEntityValues(visitaTracker, jsonVisitaTracker);

		ArrayList<JsonEvento> listEventos = jsonVisitaTracker.getEventosSaida();
		if (listEventos != null) {
			for (JsonEvento loopJsonEvento : listEventos) {
				visitaTracker.addEventoSaida(toBasicEntity(loopJsonEvento));
			}
		}

		return visitaTracker;

	}

	public static VisitaTracker toEntity(JsonVisitaTracker jsonVisitaTracker) {
		VisitaTracker visitaTracker = new VisitaTracker();

		return apply(visitaTracker, jsonVisitaTracker);
	}

	public static List<JsonVisitaTracker> toListJsonVisitaTrackers(List<VisitaTracker> all) {
		List<JsonVisitaTracker> jsonVisitaTrackers = new ArrayList<JsonVisitaTracker>();
		for (VisitaTracker visitaTracker : all) {
			jsonVisitaTrackers.add(toJson(visitaTracker));
		}
		return jsonVisitaTrackers;
	}

	// converte de entidade para json --------------------
	private static JsonBairro toBasicJson(Bairro bairro) {
		JsonBairro jsonBairro = new JsonBairro();
		applyBasicJsonValues(jsonBairro, bairro);
		return jsonBairro;
	}

	private static Bairro toBasicEntity(JsonBairro jsonBairro) {
		Bairro bairro = new Bairro();
		applyBasicEntityValues(bairro, jsonBairro);
		return bairro;
	}

	private static void applyBasicJsonValues(JsonBairro jsonBairro, Bairro bairro) {
		jsonBairro.setId(bairro.getId());
		jsonBairro.setNome(bairro.getNome());
	}

	private static void applyBasicEntityValues(Bairro bairro, JsonBairro jsonBairro) {
		bairro.setId(jsonBairro.getId());
		bairro.setNome(jsonBairro.getNome());
	}

	public static JsonBairro toJson(Bairro bairro) {
		JsonBairro jsonBairro = new JsonBairro();

		applyBasicJsonValues(jsonBairro, bairro);

		Cidade cidade_ = bairro.getCidade();
		if (cidade_ != null) {
			jsonBairro.setCidade(toBasicJson(cidade_));
		}
		Estado estado_ = bairro.getEstado();
		if (estado_ != null) {
			jsonBairro.setEstado(toBasicJson(estado_));
		}
		return jsonBairro;
	}

	public static Bairro apply(Bairro bairro, JsonBairro jsonBairro) {

		if (bairro == null)
			bairro = new Bairro();

		applyBasicEntityValues(bairro, jsonBairro);

		JsonCidade cidade_ = jsonBairro.getCidade();
		if (cidade_ != null) {
			bairro.setCidade(toEntity(cidade_));
		}
		JsonEstado estado_ = jsonBairro.getEstado();
		if (estado_ != null) {
			bairro.setEstado(toEntity(estado_));
		}
		return bairro;

	}

	public static Bairro toEntity(JsonBairro jsonBairro) {
		Bairro bairro = new Bairro();

		return apply(bairro, jsonBairro);
	}

	public static List<JsonBairro> toListJsonBairros(List<Bairro> all) {
		List<JsonBairro> jsonBairros = new ArrayList<JsonBairro>();
		for (Bairro bairro : all) {
			jsonBairros.add(toJson(bairro));
		}
		return jsonBairros;
	}

	// converte de entidade para json --------------------
	private static JsonCep toBasicJson(Cep cep) {
		JsonCep jsonCep = new JsonCep();
		applyBasicJsonValues(jsonCep, cep);
		return jsonCep;
	}

	private static Cep toBasicEntity(JsonCep jsonCep) {
		Cep cep = new Cep();
		applyBasicEntityValues(cep, jsonCep);
		return cep;
	}

	private static void applyBasicJsonValues(JsonCep jsonCep, Cep cep) {
		jsonCep.setId(cep.getId());
		jsonCep.setLogradouro(cep.getLogradouro());
		jsonCep.setNumero(cep.getNumero());
	}

	private static void applyBasicEntityValues(Cep cep, JsonCep jsonCep) {
		cep.setId(jsonCep.getId());
		cep.setLogradouro(jsonCep.getLogradouro());
		cep.setNumero(jsonCep.getNumero());
	}

	public static JsonCep toJson(Cep cep) {
		JsonCep jsonCep = new JsonCep();

		applyBasicJsonValues(jsonCep, cep);

		Bairro bairro_ = cep.getBairro();
		if (bairro_ != null) {
			jsonCep.setBairro(toBasicJson(bairro_));
		}
		Cidade cidade_ = cep.getCidade();
		if (cidade_ != null) {
			jsonCep.setCidade(toBasicJson(cidade_));
		}
		Estado estado_ = cep.getEstado();
		if (estado_ != null) {
			jsonCep.setEstado(toBasicJson(estado_));
		}
		return jsonCep;
	}

	public static Cep apply(Cep cep, JsonCep jsonCep) {

		if (cep == null)
			cep = new Cep();

		applyBasicEntityValues(cep, jsonCep);

		JsonBairro bairro_ = jsonCep.getBairro();
		if (bairro_ != null) {
			cep.setBairro(toEntity(bairro_));
		}
		JsonCidade cidade_ = jsonCep.getCidade();
		if (cidade_ != null) {
			cep.setCidade(toEntity(cidade_));
		}
		JsonEstado estado_ = jsonCep.getEstado();
		if (estado_ != null) {
			cep.setEstado(toEntity(estado_));
		}
		return cep;

	}

	public static Cep toEntity(JsonCep jsonCep) {
		Cep cep = new Cep();

		return apply(cep, jsonCep);
	}

	public static List<JsonCep> toListJsonCeps(List<Cep> all) {
		List<JsonCep> jsonCeps = new ArrayList<JsonCep>();
		for (Cep cep : all) {
			jsonCeps.add(toJson(cep));
		}
		return jsonCeps;
	}

	// converte de entidade para json --------------------
	private static JsonCidade toBasicJson(Cidade cidade) {
		JsonCidade jsonCidade = new JsonCidade();
		applyBasicJsonValues(jsonCidade, cidade);
		return jsonCidade;
	}

	private static Cidade toBasicEntity(JsonCidade jsonCidade) {
		Cidade cidade = new Cidade();
		applyBasicEntityValues(cidade, jsonCidade);
		return cidade;
	}

	private static void applyBasicJsonValues(JsonCidade jsonCidade, Cidade cidade) {
		jsonCidade.setId(cidade.getId());
		jsonCidade.setNome(cidade.getNome());
		jsonCidade.setCep(cidade.getCep());
	}

	private static void applyBasicEntityValues(Cidade cidade, JsonCidade jsonCidade) {
		cidade.setId(jsonCidade.getId());
		cidade.setNome(jsonCidade.getNome());
		cidade.setCep(jsonCidade.getCep());
	}

	public static JsonCidade toJson(Cidade cidade) {
		JsonCidade jsonCidade = new JsonCidade();

		applyBasicJsonValues(jsonCidade, cidade);

		Estado estado_ = cidade.getEstado();
		if (estado_ != null) {
			jsonCidade.setEstado(toBasicJson(estado_));
		}
		return jsonCidade;
	}

	public static Cidade apply(Cidade cidade, JsonCidade jsonCidade) {

		if (cidade == null)
			cidade = new Cidade();

		applyBasicEntityValues(cidade, jsonCidade);

		JsonEstado estado_ = jsonCidade.getEstado();
		if (estado_ != null) {
			cidade.setEstado(toEntity(estado_));
		}
		return cidade;

	}

	public static Cidade toEntity(JsonCidade jsonCidade) {
		Cidade cidade = new Cidade();

		return apply(cidade, jsonCidade);
	}

	public static List<JsonCidade> toListJsonCidades(List<Cidade> all) {
		List<JsonCidade> jsonCidades = new ArrayList<JsonCidade>();
		for (Cidade cidade : all) {
			jsonCidades.add(toJson(cidade));
		}
		return jsonCidades;
	}

	// converte de entidade para json --------------------
	private static JsonEstado toBasicJson(Estado estado) {
		JsonEstado jsonEstado = new JsonEstado();
		applyBasicJsonValues(jsonEstado, estado);
		return jsonEstado;
	}

	private static Estado toBasicEntity(JsonEstado jsonEstado) {
		Estado estado = new Estado();
		applyBasicEntityValues(estado, jsonEstado);
		return estado;
	}

	private static void applyBasicJsonValues(JsonEstado jsonEstado, Estado estado) {
		jsonEstado.setId(estado.getId());
		jsonEstado.setNome(estado.getNome());
		jsonEstado.setFaixaCep1Ini(estado.getFaixaCep1Ini());
		jsonEstado.setFaixaCep1Fim(estado.getFaixaCep1Fim());
		jsonEstado.setFaixaCep2Ini(estado.getFaixaCep2Ini());
		jsonEstado.setFaixaCep2Fim(estado.getFaixaCep2Fim());
	}

	private static void applyBasicEntityValues(Estado estado, JsonEstado jsonEstado) {
		estado.setId(jsonEstado.getId());
		estado.setNome(jsonEstado.getNome());
		estado.setFaixaCep1Ini(jsonEstado.getFaixaCep1Ini());
		estado.setFaixaCep1Fim(jsonEstado.getFaixaCep1Fim());
		estado.setFaixaCep2Ini(jsonEstado.getFaixaCep2Ini());
		estado.setFaixaCep2Fim(jsonEstado.getFaixaCep2Fim());
	}

	public static JsonEstado toJson(Estado estado) {
		JsonEstado jsonEstado = new JsonEstado();

		applyBasicJsonValues(jsonEstado, estado);

		return jsonEstado;
	}

	public static Estado apply(Estado estado, JsonEstado jsonEstado) {

		if (estado == null)
			estado = new Estado();

		applyBasicEntityValues(estado, jsonEstado);

		return estado;

	}

	public static Estado toEntity(JsonEstado jsonEstado) {
		Estado estado = new Estado();

		return apply(estado, jsonEstado);
	}

	public static List<JsonEstado> toListJsonEstados(List<Estado> all) {
		List<JsonEstado> jsonEstados = new ArrayList<JsonEstado>();
		for (Estado estado : all) {
			jsonEstados.add(toJson(estado));
		}
		return jsonEstados;
	}

	// converte de entidade para json --------------------
	private static JsonPais toBasicJson(Pais pais) {
		JsonPais jsonPais = new JsonPais();
		applyBasicJsonValues(jsonPais, pais);
		return jsonPais;
	}

	private static Pais toBasicEntity(JsonPais jsonPais) {
		Pais pais = new Pais();
		applyBasicEntityValues(pais, jsonPais);
		return pais;
	}

	private static void applyBasicJsonValues(JsonPais jsonPais, Pais pais) {
		jsonPais.setId(pais.getId());
		jsonPais.setCodigo(pais.getCodigo());
		jsonPais.setNome(pais.getNome());
	}

	private static void applyBasicEntityValues(Pais pais, JsonPais jsonPais) {
		pais.setId(jsonPais.getId());
		pais.setCodigo(jsonPais.getCodigo());
		pais.setNome(jsonPais.getNome());
	}

	public static JsonPais toJson(Pais pais) {
		JsonPais jsonPais = new JsonPais();

		applyBasicJsonValues(jsonPais, pais);

		return jsonPais;
	}

	public static Pais apply(Pais pais, JsonPais jsonPais) {

		if (pais == null)
			pais = new Pais();

		applyBasicEntityValues(pais, jsonPais);

		return pais;

	}

	public static Pais toEntity(JsonPais jsonPais) {
		Pais pais = new Pais();

		return apply(pais, jsonPais);
	}

	public static List<JsonPais> toListJsonPaiss(List<Pais> all) {
		List<JsonPais> jsonPaiss = new ArrayList<JsonPais>();
		for (Pais pais : all) {
			jsonPaiss.add(toJson(pais));
		}
		return jsonPaiss;
	}

	// converte de entidade para json --------------------
	private static JsonItem toBasicJson(Item item) {
		JsonItem jsonItem = new JsonItem();
		applyBasicJsonValues(jsonItem, item);
		return jsonItem;
	}

	private static Item toBasicEntity(JsonItem jsonItem) {
		Item item = new Item();
		applyBasicEntityValues(item, jsonItem);
		return item;
	}

	private static void applyBasicJsonValues(JsonItem jsonItem, Item item) {
		jsonItem.setId(item.getId());
		jsonItem.setName(item.getName());
		jsonItem.setDescription(item.getDescription());
	}

	private static void applyBasicEntityValues(Item item, JsonItem jsonItem) {
		item.setId(jsonItem.getId());
		item.setName(jsonItem.getName());
		item.setDescription(jsonItem.getDescription());
	}

	public static JsonItem toJson(Item item) {
		JsonItem jsonItem = new JsonItem();

		applyBasicJsonValues(jsonItem, item);

		ItemType type_ = item.getType();
		if (type_ != null) {
			jsonItem.setType(toBasicJson(type_));
		}
		List<Permission> listPermissions = item.getPermissions();
		if (listPermissions != null) {
			for (Permission loopPermission : listPermissions) {
				jsonItem.getPermissions().add(toBasicJson(loopPermission));
			}
		}
		return jsonItem;
	}

	public static Item apply(Item item, JsonItem jsonItem) {

		if (item == null)
			item = new Item();

		applyBasicEntityValues(item, jsonItem);

		JsonItemType type_ = jsonItem.getType();
		if (type_ != null) {
			item.setType(toEntity(type_));
		}
		ArrayList<JsonPermission> listPermissions = jsonItem.getPermissions();
		if (listPermissions != null) {
			for (JsonPermission loopJsonPermission : listPermissions) {
				item.addPermissions(toBasicEntity(loopJsonPermission));
			}
		}

		return item;

	}

	public static Item toEntity(JsonItem jsonItem) {
		Item item = new Item();

		return apply(item, jsonItem);
	}

	public static List<JsonItem> toListJsonItems(List<Item> all) {
		List<JsonItem> jsonItems = new ArrayList<JsonItem>();
		for (Item item : all) {
			jsonItems.add(toJson(item));
		}
		return jsonItems;
	}

	// converte de entidade para json --------------------
	private static JsonItemType toBasicJson(ItemType itemType) {
		JsonItemType jsonItemType = new JsonItemType();
		applyBasicJsonValues(jsonItemType, itemType);
		return jsonItemType;
	}

	private static ItemType toBasicEntity(JsonItemType jsonItemType) {
		ItemType itemType = new ItemType();
		applyBasicEntityValues(itemType, jsonItemType);
		return itemType;
	}

	private static void applyBasicJsonValues(JsonItemType jsonItemType, ItemType itemType) {
		jsonItemType.setId(itemType.getId());
		jsonItemType.setName(itemType.getName());
		jsonItemType.setDescription(itemType.getDescription());
	}

	private static void applyBasicEntityValues(ItemType itemType, JsonItemType jsonItemType) {
		itemType.setId(jsonItemType.getId());
		itemType.setName(jsonItemType.getName());
		itemType.setDescription(jsonItemType.getDescription());
	}

	public static JsonItemType toJson(ItemType itemType) {
		JsonItemType jsonItemType = new JsonItemType();

		applyBasicJsonValues(jsonItemType, itemType);

		return jsonItemType;
	}

	public static ItemType apply(ItemType itemType, JsonItemType jsonItemType) {

		if (itemType == null)
			itemType = new ItemType();

		applyBasicEntityValues(itemType, jsonItemType);

		return itemType;

	}

	public static ItemType toEntity(JsonItemType jsonItemType) {
		ItemType itemType = new ItemType();

		return apply(itemType, jsonItemType);
	}

	public static List<JsonItemType> toListJsonItemTypes(List<ItemType> all) {
		List<JsonItemType> jsonItemTypes = new ArrayList<JsonItemType>();
		for (ItemType itemType : all) {
			jsonItemTypes.add(toJson(itemType));
		}
		return jsonItemTypes;
	}

	// converte de entidade para json --------------------
	private static JsonOperation toBasicJson(Operation operation) {
		JsonOperation jsonOperation = new JsonOperation();
		applyBasicJsonValues(jsonOperation, operation);
		return jsonOperation;
	}

	private static Operation toBasicEntity(JsonOperation jsonOperation) {
		Operation operation = new Operation();
		applyBasicEntityValues(operation, jsonOperation);
		return operation;
	}

	private static void applyBasicJsonValues(JsonOperation jsonOperation, Operation operation) {
		jsonOperation.setId(operation.getId());
		jsonOperation.setName(operation.getName());
		jsonOperation.setCanEdit(operation.getCanEdit());
		jsonOperation.setCanRead(operation.getCanRead());
		jsonOperation.setCanUpdate(operation.getCanUpdate());
		jsonOperation.setCanDelete(operation.getCanDelete());
		jsonOperation.setCanExecute(operation.getCanExecute());
	}

	private static void applyBasicEntityValues(Operation operation, JsonOperation jsonOperation) {
		operation.setId(jsonOperation.getId());
		operation.setName(jsonOperation.getName());
		operation.setCanEdit(jsonOperation.getCanEdit());
		operation.setCanRead(jsonOperation.getCanRead());
		operation.setCanUpdate(jsonOperation.getCanUpdate());
		operation.setCanDelete(jsonOperation.getCanDelete());
		operation.setCanExecute(jsonOperation.getCanExecute());
	}

	public static JsonOperation toJson(Operation operation) {
		JsonOperation jsonOperation = new JsonOperation();

		applyBasicJsonValues(jsonOperation, operation);

		List<Permission> listPermissions = operation.getPermissions();
		if (listPermissions != null) {
			for (Permission loopPermission : listPermissions) {
				jsonOperation.getPermissions().add(toBasicJson(loopPermission));
			}
		}
		return jsonOperation;
	}

	public static Operation apply(Operation operation, JsonOperation jsonOperation) {

		if (operation == null)
			operation = new Operation();

		applyBasicEntityValues(operation, jsonOperation);

		ArrayList<JsonPermission> listPermissions = jsonOperation.getPermissions();
		if (listPermissions != null) {
			for (JsonPermission loopJsonPermission : listPermissions) {
				operation.addPermissions(toBasicEntity(loopJsonPermission));
			}
		}

		return operation;

	}

	public static Operation toEntity(JsonOperation jsonOperation) {
		Operation operation = new Operation();

		return apply(operation, jsonOperation);
	}

	public static List<JsonOperation> toListJsonOperations(List<Operation> all) {
		List<JsonOperation> jsonOperations = new ArrayList<JsonOperation>();
		for (Operation operation : all) {
			jsonOperations.add(toJson(operation));
		}
		return jsonOperations;
	}

	// converte de entidade para json --------------------
	private static JsonPermission toBasicJson(Permission permission) {
		JsonPermission jsonPermission = new JsonPermission();
		applyBasicJsonValues(jsonPermission, permission);
		return jsonPermission;
	}

	private static Permission toBasicEntity(JsonPermission jsonPermission) {
		Permission permission = new Permission();
		applyBasicEntityValues(permission, jsonPermission);
		return permission;
	}

	private static void applyBasicJsonValues(JsonPermission jsonPermission, Permission permission) {
		jsonPermission.setId(permission.getId());
		jsonPermission.setName(permission.getName());
	}

	private static void applyBasicEntityValues(Permission permission, JsonPermission jsonPermission) {
		permission.setId(jsonPermission.getId());
		permission.setName(jsonPermission.getName());
	}

	public static JsonPermission toJson(Permission permission) {
		JsonPermission jsonPermission = new JsonPermission();

		applyBasicJsonValues(jsonPermission, permission);

		List<Role> listRoles = permission.getRoles();
		if (listRoles != null) {
			for (Role loopRole : listRoles) {
				jsonPermission.getRoles().add(toBasicJson(loopRole));
			}
		}

		Operation operation_ = permission.getOperation();
		if (operation_ != null) {
			jsonPermission.setOperation(toBasicJson(operation_));
		}
		Item item_ = permission.getItem();
		if (item_ != null) {
			jsonPermission.setItem(toBasicJson(item_));
		}
		return jsonPermission;
	}

	public static Permission apply(Permission permission, JsonPermission jsonPermission) {

		if (permission == null)
			permission = new Permission();

		applyBasicEntityValues(permission, jsonPermission);

		ArrayList<JsonRole> listRoles = jsonPermission.getRoles();
		if (listRoles != null) {
			for (JsonRole loopJsonRole : listRoles) {
				permission.addRoles(toBasicEntity(loopJsonRole));
			}
		}

		JsonOperation operation_ = jsonPermission.getOperation();
		if (operation_ != null) {
			permission.setOperation(toEntity(operation_));
		}
		JsonItem item_ = jsonPermission.getItem();
		if (item_ != null) {
			permission.setItem(toEntity(item_));
		}
		return permission;

	}

	public static Permission toEntity(JsonPermission jsonPermission) {
		Permission permission = new Permission();

		return apply(permission, jsonPermission);
	}

	public static List<JsonPermission> toListJsonPermissions(List<Permission> all) {
		List<JsonPermission> jsonPermissions = new ArrayList<JsonPermission>();
		for (Permission permission : all) {
			jsonPermissions.add(toJson(permission));
		}
		return jsonPermissions;
	}

	// converte de entidade para json --------------------
	private static JsonRole toBasicJson(Role role) {
		JsonRole jsonRole = new JsonRole();
		applyBasicJsonValues(jsonRole, role);
		return jsonRole;
	}

	private static Role toBasicEntity(JsonRole jsonRole) {
		Role role = new Role();
		applyBasicEntityValues(role, jsonRole);
		return role;
	}

	private static void applyBasicJsonValues(JsonRole jsonRole, Role role) {
		jsonRole.setId(role.getId());
		jsonRole.setAuthority(role.getAuthority());
		jsonRole.setDescription(role.getDescription());
	}

	private static void applyBasicEntityValues(Role role, JsonRole jsonRole) {
		role.setId(jsonRole.getId());
		role.setAuthority(jsonRole.getAuthority());
		role.setDescription(jsonRole.getDescription());
	}

	public static JsonRole toJson(Role role) {
		JsonRole jsonRole = new JsonRole();

		applyBasicJsonValues(jsonRole, role);

		List<Session> listSessions = role.getSessions();
		if (listSessions != null) {
			for (Session loopSession : listSessions) {
				jsonRole.getSessions().add(toBasicJson(loopSession));
			}
		}

		List<User> listUsers = role.getUsers();
		if (listUsers != null) {
			for (User loopUser : listUsers) {
				jsonRole.getUsers().add(toBasicJson(loopUser));
			}
		}

		List<Permission> listPermissions = role.getPermissions();
		if (listPermissions != null) {
			for (Permission loopPermission : listPermissions) {
				jsonRole.getPermissions().add(toBasicJson(loopPermission));
			}
		}

		return jsonRole;
	}

	public static Role apply(Role role, JsonRole jsonRole) {

		if (role == null)
			role = new Role();

		applyBasicEntityValues(role, jsonRole);

		ArrayList<JsonSession> listSessions = jsonRole.getSessions();
		if (listSessions != null) {
			for (JsonSession loopJsonSession : listSessions) {
				role.addSessions(toBasicEntity(loopJsonSession));
			}
		}

		ArrayList<JsonUser> listUsers = jsonRole.getUsers();
		if (listUsers != null) {
			for (JsonUser loopJsonUser : listUsers) {
				role.addUsers(toBasicEntity(loopJsonUser));
			}
		}

		ArrayList<JsonPermission> listPermissions = jsonRole.getPermissions();
		if (listPermissions != null) {
			for (JsonPermission loopJsonPermission : listPermissions) {
				role.addPermissions(toEntity(loopJsonPermission));
			}
		}
		return role;

	}

	public static Role toEntity(JsonRole jsonRole) {
		Role role = new Role();

		return apply(role, jsonRole);
	}

	public static List<JsonRole> toListJsonRoles(List<Role> all) {
		List<JsonRole> jsonRoles = new ArrayList<JsonRole>();
		for (Role role : all) {
			jsonRoles.add(toJson(role));
		}
		return jsonRoles;
	}

	// converte de entidade para json --------------------
	private static JsonSession toBasicJson(Session session) {
		JsonSession jsonSession = new JsonSession();
		applyBasicJsonValues(jsonSession, session);
		return jsonSession;
	}

	private static Session toBasicEntity(JsonSession jsonSession) {
		Session session = new Session();
		applyBasicEntityValues(session, jsonSession);
		return session;
	}

	private static void applyBasicJsonValues(JsonSession jsonSession, Session session) {
		jsonSession.setId(session.getId());
		jsonSession.setName(session.getName());
		jsonSession.setCreationDate(DateUtil.localDateTimeAsString(session.getCreationDate()));
	}

	private static void applyBasicEntityValues(Session session, JsonSession jsonSession) {
		session.setId(jsonSession.getId());
		session.setName(jsonSession.getName());
		session.setCreationDate(DateUtil.stringAsLocalDateTime(jsonSession.getCreationDate()));
	}

	public static JsonSession toJson(Session session) {
		JsonSession jsonSession = new JsonSession();

		applyBasicJsonValues(jsonSession, session);

		List<Role> listRoles = session.getRoles();
		if (listRoles != null) {
			for (Role loopRole : listRoles) {
				jsonSession.getRoles().add(toBasicJson(loopRole));
			}
		}

		User user_ = session.getUser();
		if (user_ != null) {
			jsonSession.setUser(toBasicJson(user_));
		}
		return jsonSession;
	}

	public static Session apply(Session session, JsonSession jsonSession) {

		if (session == null)
			session = new Session();

		applyBasicEntityValues(session, jsonSession);

		ArrayList<JsonRole> listRoles = jsonSession.getRoles();
		if (listRoles != null) {
			for (JsonRole loopJsonRole : listRoles) {
				session.addRoles(toEntity(loopJsonRole));
			}
		}
		JsonUser user_ = jsonSession.getUser();
		if (user_ != null) {
			session.setUser(toEntity(user_));
		}

		return session;

	}

	public static Session toEntity(JsonSession jsonSession) {
		Session session = new Session();

		return apply(session, jsonSession);
	}

	public static List<JsonSession> toListJsonSessions(List<Session> all) {
		List<JsonSession> jsonSessions = new ArrayList<JsonSession>();
		for (Session session : all) {
			jsonSessions.add(toJson(session));
		}
		return jsonSessions;
	}

	// converte de entidade para json --------------------
	private static JsonUser toBasicJson(User user) {
		JsonUser jsonUser = new JsonUser();
		applyBasicJsonValues(jsonUser, user);
		return jsonUser;
	}

	private static User toBasicEntity(JsonUser jsonUser) {
		User user = new User();
		applyBasicEntityValues(user, jsonUser);
		return user;
	}

	private static void applyBasicJsonValues(JsonUser jsonUser, User user) {
		jsonUser.setId(user.getId());
		jsonUser.setName(user.getName());
		jsonUser.setUsername(user.getUsername());
		jsonUser.setPassword(user.getPassword());
		jsonUser.setEnable(user.getEnable());
		jsonUser.setImage(user.getImage());
		jsonUser.setEmail(user.getEmail());
		jsonUser.setPerfil(user.getPerfil());

		jsonUser.setTipo(user.getTipo());
		jsonUser.setStatusUsuario(user.getStatusUsuario());
	}

	private static void applyBasicEntityValues(User user, JsonUser jsonUser) {
		user.setId(jsonUser.getId());
		user.setName(jsonUser.getName());
		user.setUsername(jsonUser.getUsername());
		user.setPassword(jsonUser.getPassword());
		user.setEnable(jsonUser.getEnable());
		user.setImage(jsonUser.getImage());
		user.setEmail(jsonUser.getEmail());
		user.setPerfil(jsonUser.getPerfil());

		user.setTipo(jsonUser.getTipo());
		user.setStatusUsuario(jsonUser.getStatusUsuario());
	}

	public static JsonUser toJson(User user) {
		JsonUser jsonUser = new JsonUser();

		applyBasicJsonValues(jsonUser, user);

		List<Role> listRoles = user.getRoles();
		if (listRoles != null) {
			for (Role loopRole : listRoles) {
				jsonUser.getRoles().add(toBasicJson(loopRole));
			}
		}
		Empresa empresa_ = user.getEmpresa();
		if (empresa_ != null) {
			jsonUser.setEmpresa(toJson(empresa_));
		}
		return jsonUser;
	}

	public static User apply(User user, JsonUser jsonUser) {

		if (user == null)
			user = new User();

		applyBasicEntityValues(user, jsonUser);

		ArrayList<JsonRole> listRoles = jsonUser.getRoles();
		if (listRoles != null) {
			for (JsonRole loopJsonRole : listRoles) {
				user.addRoles(toEntity(loopJsonRole));
			}
		}
		JsonEmpresa empresa_ = jsonUser.getEmpresa();
		if (empresa_ != null) {
			user.setEmpresa(toEntity(empresa_));
		}
		return user;

	}

	public static User toEntity(JsonUser jsonUser) {
		User user = new User();

		return apply(user, jsonUser);
	}

	public static List<JsonUser> toListJsonUsers(List<User> all) {
		List<JsonUser> jsonUsers = new ArrayList<JsonUser>();
		for (User user : all) {
			jsonUsers.add(toJson(user));
		}
		return jsonUsers;
	}

	public static JsonConsumidorCorrida toJson(ConsumidorCorrida dto) {
		JsonConsumidorCorrida json = new JsonConsumidorCorrida();

		json.setTipo(dto.getTipo());

		Cliente cliente = dto.getCliente();
		if (cliente != null)
			json.setCliente(toBasicJson(cliente));
		Empresa empresa = dto.getEmpresa();
		if (empresa != null) {
			json.setEmpresa(toBasicJson(empresa));
		}

		return json;
	}

	// converte de entidade para json --------------------
	private static JsonMotivoPunicao toBasicJson(MotivoPunicao motivoPunicao) {
		JsonMotivoPunicao jsonMotivoPunicao = new JsonMotivoPunicao();
		applyBasicJsonValues(jsonMotivoPunicao, motivoPunicao);
		return jsonMotivoPunicao;
	}

	private static MotivoPunicao toBasicEntity(JsonMotivoPunicao jsonMotivoPunicao) {
		MotivoPunicao motivoPunicao = new MotivoPunicao();
		applyBasicEntityValues(motivoPunicao, jsonMotivoPunicao);
		return motivoPunicao;
	}

	private static void applyBasicJsonValues(JsonMotivoPunicao jsonMotivoPunicao, MotivoPunicao motivoPunicao) {
		jsonMotivoPunicao.setId(motivoPunicao.getId());
		jsonMotivoPunicao.setDescricao(motivoPunicao.getDescricao());
	}

	private static void applyBasicEntityValues(MotivoPunicao motivoPunicao, JsonMotivoPunicao jsonMotivoPunicao) {
		motivoPunicao.setId(jsonMotivoPunicao.getId());
		motivoPunicao.setDescricao(jsonMotivoPunicao.getDescricao());
	}

	public static JsonMotivoPunicao toJson(MotivoPunicao motivoPunicao) {
		JsonMotivoPunicao jsonMotivoPunicao = new JsonMotivoPunicao();

		applyBasicJsonValues(jsonMotivoPunicao, motivoPunicao);

		return jsonMotivoPunicao;
	}

	public static MotivoPunicao apply(MotivoPunicao motivoPunicao, JsonMotivoPunicao jsonMotivoPunicao) {

		if (motivoPunicao == null)
			motivoPunicao = new MotivoPunicao();

		applyBasicEntityValues(motivoPunicao, jsonMotivoPunicao);

		return motivoPunicao;

	}

	public static MotivoPunicao toEntity(JsonMotivoPunicao jsonMotivoPunicao) {
		MotivoPunicao motivoPunicao = new MotivoPunicao();

		return apply(motivoPunicao, jsonMotivoPunicao);
	}

	public static List<JsonMotivoPunicao> toListJsonMotivoPunicaos(List<MotivoPunicao> all) {
		List<JsonMotivoPunicao> jsonMotivoPunicaos = new ArrayList<JsonMotivoPunicao>();
		for (MotivoPunicao motivoPunicao : all) {
			jsonMotivoPunicaos.add(toJson(motivoPunicao));
		}
		return jsonMotivoPunicaos;
	}

	public static List<JsonEnderecoFavorito> toListJsonEnderecoFavoritos(List<EnderecoFavorito> all) {
		List<JsonEnderecoFavorito> jsonEnderecos = new ArrayList<JsonEnderecoFavorito>();

		for (EnderecoFavorito enderecoFavorito : all) {
			jsonEnderecos.add(toJson(enderecoFavorito));
		}
		return jsonEnderecos;
	}

	private static JsonInformacaoTarifaAdicional toBasicJson(InformacaoTarifaAdicional informacaoTarifaAdicional) {
		JsonInformacaoTarifaAdicional jsonInformacaoTarifaAdicional = new JsonInformacaoTarifaAdicional();
		jsonInformacaoTarifaAdicional.setId(informacaoTarifaAdicional.getId());
		jsonInformacaoTarifaAdicional.setQuantidade(informacaoTarifaAdicional.getQuantidade());

		jsonInformacaoTarifaAdicional.setDescricao(informacaoTarifaAdicional.getDescricao());
		jsonInformacaoTarifaAdicional.setValorTotal(informacaoTarifaAdicional.getValorTotal());

		TarifaAdicional tarifaAdicional = informacaoTarifaAdicional.getTarifaAdicional();

		if (tarifaAdicional != null) {
			jsonInformacaoTarifaAdicional.setTarifaAdicional(toBasicJson(tarifaAdicional));
		}

		return jsonInformacaoTarifaAdicional;
	}

	private static InformacaoTarifaAdicional toBasicEntity(JsonInformacaoTarifaAdicional jsonInformacaoTarifaAdicional) {
		InformacaoTarifaAdicional informacaoTarifaAdicional = new InformacaoTarifaAdicional();

		informacaoTarifaAdicional.setId(jsonInformacaoTarifaAdicional.getId());
		informacaoTarifaAdicional.setQuantidade(jsonInformacaoTarifaAdicional.getQuantidade());

		informacaoTarifaAdicional.setDescricao(jsonInformacaoTarifaAdicional.getDescricao());
		informacaoTarifaAdicional.setValorTotal(jsonInformacaoTarifaAdicional.getValorTotal());

		JsonTarifaAdicional jsonTarifaAdicional = jsonInformacaoTarifaAdicional.getTarifaAdicional();

		if (jsonTarifaAdicional != null) {
			informacaoTarifaAdicional.setTarifaAdicional(toBasicEntity(jsonTarifaAdicional));
		}

		return informacaoTarifaAdicional;
	}
}
