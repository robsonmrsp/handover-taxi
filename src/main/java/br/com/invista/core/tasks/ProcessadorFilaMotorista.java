package br.com.invista.core.tasks;

import br.com.invista.model.Corrida;

public interface ProcessadorFilaMotorista {

	Boolean criaFila(Corrida corrida);

}
