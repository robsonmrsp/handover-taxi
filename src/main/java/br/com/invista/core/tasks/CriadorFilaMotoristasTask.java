package br.com.invista.core.tasks;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.com.invista.model.Corrida;
import br.com.invista.service.CorridaService;
import br.com.invista.utils.BufferCorridas;
import br.com.invista.utils.CorridaEntityForBuffer;
//ProcessadorFilasMotoristasTask
public class CriadorFilaMotoristasTask {
	private static final Logger LOGGER = Logger.getLogger(CriadorFilaMotoristasTask.class);

	@Inject
	private ProcessadorFilaMotorista processadorFilaMotoristas;
	
	@Inject
	private CorridaService corridaService;

	static final LinkedBlockingQueue<Corrida> corridas = new LinkedBlockingQueue<Corrida>();

	public void execute() {
		
		List<Corrida> corridasPendentes = corridaService.corridasPendentes();
		
		for (Corrida corrida: corridasPendentes){
			BufferCorridas buffer = BufferCorridas.getInstance();
			if (!buffer.getSetBuffer().containsKey(corrida.getId())){
				buffer.addCorridaEntity(new CorridaEntityForBuffer(corrida));
				corrida.setFilaCriada(new Boolean(true));
				corridaService.update(corrida);
				processadorFilaMotoristas.criaFila(corrida);
			}
		}
		
		//LOGGER.info("Executando criação de filas [ " + corridas.size() + " ] corridas acumuladas.");
		LOGGER.info("Executando criação de filas [ " + corridasPendentes.size() + " ] corridas acumuladas.");
		/*while (corridas.peek() != null) {
			try {

				Corrida corrida = corridas.take();

				processadorFilaMotoristas.criaFila(corrida);

			} catch (InterruptedException e) {
				LOGGER.error("Problema consumindo lista de corridas", e);
			}
		}*/
		
		/*try {
			Corrida corrida = corridas.poll();
			while (corrida != null) {
	
					processadorFilaMotoristas.criaFila(corrida);
					corridas.remove(corrida);
				    Thread.sleep(10);
				    corrida = corridas.poll();
	
			}
		} catch (InterruptedException e) {
			LOGGER.error("Problema consumindo lista de corridas", e);
		}*/
	}

	public static void addCorrida(Corrida corrida) {
		/*try {

			corridas.put(corrida);

		} catch (InterruptedException e) {
			LOGGER.warn("Não foi possivel adicionar uma corrida a para criação da fila de motoristas", e);
		}*/
	}

}