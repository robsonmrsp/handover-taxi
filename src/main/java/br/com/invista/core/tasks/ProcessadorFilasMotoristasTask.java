package br.com.invista.core.tasks;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.com.invista.comunicacao.Action;
import br.com.invista.comunicacao.FirebaseAdapter;
import br.com.invista.comunicacao.NotificaoCorrida;
import br.com.invista.model.Corrida;
import br.com.invista.model.DistanciaMotoCorrida;
import br.com.invista.model.SituacaoMotoristaEmFila;
import br.com.invista.model.StatusCorrida;
import br.com.invista.model.filter.FilterDistanciaMotoCorrida;
import br.com.invista.service.CorridaService;
import br.com.invista.service.DistanciaMotoCorridaService;

public class ProcessadorFilasMotoristasTask {
	private static final Logger LOGGER = Logger.getLogger(ProcessadorFilasMotoristasTask.class);

	@Inject
	private DistanciaMotoCorridaService distanciaMotoCorridaService;
	
	@Inject
	private CorridaService corridaService;

	FirebaseAdapter firebaseAdapter = new FirebaseAdapter();

	public void execute() {
		LOGGER.info("Executando Processamento de filas em corridas sem atendimento");
		
		limpaFilas();

		FilterDistanciaMotoCorrida filterDistanciaMotoCorrida = new FilterDistanciaMotoCorrida();
		FilterDistanciaMotoCorrida filterDistanciaMotoCorridaReenvio = new FilterDistanciaMotoCorrida();
		FilterDistanciaMotoCorrida filterDistanciaMotoCorridaAgendamento = new FilterDistanciaMotoCorrida();

		filterDistanciaMotoCorrida.setStatusCorrida(StatusCorrida.PENDENTE);
		filterDistanciaMotoCorridaReenvio.setStatusCorrida(StatusCorrida.REENVIADA);
		filterDistanciaMotoCorridaAgendamento.setStatusCorrida(StatusCorrida.AGENDADA);

		Map<Corrida, List<DistanciaMotoCorrida>> map = distanciaMotoCorridaService.getMap(filterDistanciaMotoCorrida);
		Map<Corrida, List<DistanciaMotoCorrida>> mapReenvio = distanciaMotoCorridaService.getMap(filterDistanciaMotoCorridaReenvio);
		Map<Corrida, List<DistanciaMotoCorrida>> mapAgendamento = distanciaMotoCorridaService.getMap(filterDistanciaMotoCorridaAgendamento);

		if(!mapReenvio.isEmpty())
			map.putAll(mapReenvio);

		if(!mapAgendamento.isEmpty())
			map.putAll(mapAgendamento);
		
		Set<Corrida> corridas = map.keySet();

		List<DistanciaMotoCorrida> list = null;
		for (Corrida corrida : corridas) {
			list = map.get(corrida);
			int countEnviadas = 0;
			for (final DistanciaMotoCorrida distanciaMotoCorrida : list) {

				// Deve pegar a primeira posição da fila que que apresenta o
				// motorista como pendente;
				if (SituacaoMotoristaEmFila.PENDENTE.equals(distanciaMotoCorrida.getSituacao())) {
					
					LOGGER.info("Data de hoje [ " + new Date() + " ]");
					
					Date dateServer = new Date();
					LOGGER.info("Date server [ " + dateServer + " ]");
					long timeCeara = dateServer.getTime() - (3*60*60*1000);
					Date dateCeara = new Date(timeCeara);
					LOGGER.info("Date Ceara [ " + dateCeara + " ]");
					
					
					
					firebaseAdapter.send(new NotificaoCorrida(corrida.getId(), dateCeara.getTime(), distanciaMotoCorrida.getMotorista().getId()),
							/* ação em caso de sucesso */new Action() {
								public void execute() {
									distanciaMotoCorrida.setSituacao(SituacaoMotoristaEmFila.ENVIADA);
									distanciaMotoCorridaService.save(distanciaMotoCorrida);

								}
							}, /* ação em caso de erro */new Action() {
								public void execute() {
									LOGGER.error("Houve um erro ao enviar mensagem para firebase...");

								}
							});
					break;
				}else{
					if (SituacaoMotoristaEmFila.ENVIADA.equals(distanciaMotoCorrida.getSituacao())) {
						countEnviadas++;
					}
				}
			}
			if (countEnviadas == list.size()){//Se a fila acabou ele muda o status da corrida para DISPON�VEL 'D'
				corrida.setStatusCorrida(StatusCorrida.DISPONIVEL);
				corridaService.update(corrida);
			}
		}
	}
	
	public void limpaFilas(){
		firebaseAdapter.limpaFilas(
				/* ação em caso de sucesso */new Action() {
					public void execute() {
						

					}
				}, /* ação em caso de erro */new Action() {
					public void execute() {
						LOGGER.error("Houve um erro ao apagar filas no firebase...");

					}
				});
	}
}