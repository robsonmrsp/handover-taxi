package br.com.invista.core.tasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import com.vividsolutions.jts.geom.Coordinate;

import br.com.invista.model.Corrida;
import br.com.invista.model.DistanciaMotoCorrida;
import br.com.invista.model.EnderecoCorrida;
import br.com.invista.model.Motorista;
import br.com.invista.model.SituacaoMotoristaEmFila;
import br.com.invista.model.StatusCorrida;
import br.com.invista.service.CorridaService;
import br.com.invista.service.DistanciaMotoCorridaService;
import br.com.invista.service.MotoristaService;
import br.com.invista.utils.ConstantesOperacao;
import br.com.invista.utils.GeoUtils;

@Named
@Transactional
public class ProcessadorFilaMotoristaImp implements ProcessadorFilaMotorista {

	@Inject
	private DistanciaMotoCorridaService distanciaMotoCorridaService;

	@Inject
	private MotoristaService motoristaService;

	@Inject
	private CorridaService corridaService;

	@Override

	public Boolean criaFila(Corrida c) {

		Corrida corrida = corridaService.get(c.getId());

		// List<Motorista> motoristas = motoristaService.all();
		List<Motorista> motoristas = motoristaService.getByCorridaConstraints(corrida);

		List<DistanciaMotoCorrida> distanciaMotoCorridas = new ArrayList<DistanciaMotoCorrida>();

		for (Motorista motorista : motoristas) {
			// Checagem motorista... Se ele pode atender a corrida...
			if (motorista.getLongitude() != null && motorista.getLatitude() != null) {
				EnderecoCorrida enderecoCorrida = corrida.getEnderecoCorridas().get(0);
				Coordinate cordenadaCorrida = new Coordinate(enderecoCorrida.getLongitude(), enderecoCorrida.getLatitude());
				Coordinate cordenadaMotorista = new Coordinate(motorista.getLongitude(), motorista.getLatitude());
				Double distancia = GeoUtils.getDistancia(cordenadaCorrida, cordenadaMotorista);

				if (distancia < ConstantesOperacao.PROXIMIDADE_MINIMA_CORRIDA) {
					distanciaMotoCorridas.add(new DistanciaMotoCorrida(corrida, motorista, distancia, SituacaoMotoristaEmFila.PENDENTE));
				}
			}
		}

		if (distanciaMotoCorridas.size() == 0) { // Caso não haja nenhum
													// motorista no raio a
													// corrida vai para o status
													// de disponível
													// imediatamente.
			corrida.setStatusCorrida(StatusCorrida.DISPONIVEL);
			corridaService.save(corrida);
		} else {
			// Estou ordenando a lista pela distancia
			Collections.sort(distanciaMotoCorridas, new Comparator<DistanciaMotoCorrida>() {
				@Override
				public int compare(DistanciaMotoCorrida o1, DistanciaMotoCorrida o2) {
					return o1.getDistancia().compareTo(o2.getDistancia());
				}
			});

			int index = 0;
			for (DistanciaMotoCorrida distanciaMotoCorrida : distanciaMotoCorridas) {
				distanciaMotoCorrida.setSequencia(index++);
				distanciaMotoCorridaService.save(distanciaMotoCorrida);

				if (index == ConstantesOperacao.NUMERO_DE_MOTORISTAS_POR_FILA) {
					break;
				}
			}
		}

		return null;
	}

}
