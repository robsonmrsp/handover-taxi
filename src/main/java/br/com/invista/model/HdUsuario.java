package br.com.invista.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.model.AbstractTimestampEntity;
/**
* generated: 16/10/2016 15:27:08
**/
@Entity
@Audited
@Table(name = "HD_USUARIO")
public class HdUsuario extends AbstractTimestampEntity{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Integer id;
		
	@Column(name = "NOME" , length=100)
	private String nome;		
		
	@Column(name = "EMAIL" , length=100)
	private String email;		
		
	@Column(name = "SENHA" , length=100)
	private String senha;		
		
	@Column(name = "TIPO" , length=20)
	private String tipo;		
		
	@Column(name = "PERFIL" , length=20)
	private String perfil;		
		
	@Column(name = "STATUS_USUARIO" , length=10)
	private String statusUsuario;		
		
	@Column(name = "DATAINCLUSAO")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	private LocalDateTime datainclusao;
		
	@Column(name = "USUARIOINCLUSAO")
	private Integer usuarioinclusao;  			
	
	@ManyToOne
	@JoinColumn(name = "ID_EMPRESA")
	private Empresa empresa;		

	
	public  HdUsuario() {
		
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public String getStatusUsuario() {
		return statusUsuario;
	}

	public void setStatusUsuario(String statusUsuario) {
		this.statusUsuario = statusUsuario;
	}
	public LocalDateTime getDatainclusao() {
		return datainclusao;
	}

	public void setDatainclusao(LocalDateTime datainclusao) {
		this.datainclusao = datainclusao;
	}
	public Integer getUsuarioinclusao() {
		return usuarioinclusao;
	}

	public void setUsuarioinclusao(Integer usuarioinclusao) {
		this.usuarioinclusao = usuarioinclusao;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	
}
