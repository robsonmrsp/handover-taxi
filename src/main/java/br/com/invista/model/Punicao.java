package br.com.invista.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.model.AbstractTimestampEntity;

/**
 * generated: 16/10/2016 15:27:08
 **/
@Entity
@Audited
@Table(name = "PUNICAO")
public class Punicao extends AbstractTimestampEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "ID_MOTIVO_PUNICAO")
	private MotivoPunicao motivo;

	@Column(name = "STATUS_PUNICAO", length = 10)
	private String statusPunicao;

	@Column(name = "DATA_INICIO")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	private LocalDateTime dataInicio;

	@Column(name = "DATA_FIM")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	private LocalDateTime dataFim;

	@Column(name = "QUANTIDADETEMPO")
	private Integer quantidadetempo;

	@ManyToOne
	@JoinColumn(name = "ID_MOTORISTA")
	private Motorista motorista;

	public Punicao() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDateTime dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDateTime getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDateTime dataFim) {
		this.dataFim = dataFim;
	}

	public Integer getQuantidadetempo() {
		return quantidadetempo;
	}

	public void setQuantidadetempo(Integer quantidadetempo) {
		this.quantidadetempo = quantidadetempo;
	}

	public Motorista getMotorista() {
		return motorista;
	}

	public void setMotorista(Motorista motorista) {
		this.motorista = motorista;
	}

	public String getStatusPunicao() {
		return statusPunicao;
	}

	public void setStatusPunicao(String statusPunicao) {
		this.statusPunicao = statusPunicao;
	}

	public MotivoPunicao getMotivo() {
		return motivo;
	}

	public void setMotivo(MotivoPunicao motivo) {
		this.motivo = motivo;
	}

}
