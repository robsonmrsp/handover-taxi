package br.com.invista.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.model.AbstractTimestampEntity;
/**
* generated: 16/10/2016 15:27:08
**/
@Entity
@Audited
@Table(name = "MENSAGEM")
public class Mensagem extends AbstractTimestampEntity{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Integer id;
		
	@Column(name = "DESCRICAO" , length=400)
	private String descricao;		
	
	@OneToMany(mappedBy="mensagem")
	private List<MensagemMotorista> mensagemMotoristas;		
		
	public  Mensagem() {
		
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setMensagemMotoristas(List<MensagemMotorista> mensagemMotoristas){
		this.mensagemMotoristas = mensagemMotoristas;
	}
	
	public List<MensagemMotorista>  getMensagemMotoristas() {
		if(this.mensagemMotoristas == null){
			setMensagemMotoristas(new ArrayList<MensagemMotorista>());
		}
		return this.mensagemMotoristas;
	}
		
	public boolean addMensagemMotoristas(MensagemMotorista mensagemMotorista){
		mensagemMotorista.setMensagem(this);
		return getMensagemMotoristas().add(mensagemMotorista);
	}
	
	public boolean removeMensagemMotoristas(MensagemMotorista mensagemMotorista){
		mensagemMotorista.setMensagem(null);
		return getMensagemMotoristas().remove(mensagemMotorista);
	}
	
	
}
