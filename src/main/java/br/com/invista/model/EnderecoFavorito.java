package br.com.invista.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.model.AbstractTimestampEntity;
/**
* generated: 16/10/2016 15:27:07
**/
@Entity
@Audited
@Table(name = "ENDERECO_FAVORITO")
public class EnderecoFavorito extends AbstractTimestampEntity{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Integer id;
		
	@Column(name = "ENDERECO" , length=200)
	private String endereco;		
		
	@Column(name = "NUMERO" , length=10)
	private String numero;		
		
	@Column(name = "COMPLEMENTO" , length=200)
	private String complemento;		
		
	@Column(name = "UF" , length=2)
	private String uf;		
		
	@Column(name = "CIDADE" , length=50)
	private String cidade;		
		
	@Column(name = "BAIRRO" , length=100)
	private String bairro;		
		
	@Column(name = "CEP" , length=9)
	private String cep;		
		
	@Column(name = "REFERENCIA" , length=200)
	private String referencia;		
		
	@Column(name = "TIPO" , length=10)
	private String tipo;		
		
	@Column(name = "FAVORITO")
	private Boolean favorito;  			
	
	@Column(name = "LATITUDE")
	private Double latitude;  			
		
	@Column(name = "LONGITUDE")
	private Double longitude;  			
		
	@Column(name = "ENDERECO_FORMATADO")
	private String enderecoFormatado;		
	
	@OneToMany(mappedBy="clienteEndereco")
	private List<ClienteEnderecoCoord> clienteEnderecoCoords;		
	
	@ManyToOne
	@JoinColumn(name = "ID_CLIENTE")
	private Cliente cliente;		

	@ManyToOne
	@JoinColumn(name = "ID_EMPRESA")
	private Empresa empresa;		
		
	public  EnderecoFavorito() {
		
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Boolean getFavorito() {
		return favorito;
	}

	public void setFavorito(Boolean favorito) {
		this.favorito = favorito;
	}
	public void setClienteEnderecoCoords(List<ClienteEnderecoCoord> clienteEnderecoCoords){
		this.clienteEnderecoCoords = clienteEnderecoCoords;
	}
	
	public List<ClienteEnderecoCoord>  getClienteEnderecoCoords() {
		if(this.clienteEnderecoCoords == null){
			setClienteEnderecoCoords(new ArrayList<ClienteEnderecoCoord>());
		}
		return this.clienteEnderecoCoords;
	}
		
	public boolean addClienteEnderecoCoords(ClienteEnderecoCoord clienteEnderecoCoord){
		clienteEnderecoCoord.setClienteEndereco(this);
		return getClienteEnderecoCoords().add(clienteEnderecoCoord);
	}
	
	public boolean removeClienteEnderecoCoords(ClienteEnderecoCoord clienteEnderecoCoord){
		clienteEnderecoCoord.setClienteEndereco(null);
		return getClienteEnderecoCoords().remove(clienteEnderecoCoord);
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getEnderecoFormatado() {
		return enderecoFormatado;
	}
	public void setEnderecoFormatado(String enderecoFormatado) {
		this.enderecoFormatado = enderecoFormatado;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	
}
