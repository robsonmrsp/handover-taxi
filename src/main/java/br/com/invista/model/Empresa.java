package br.com.invista.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.invista.core.model.AbstractTimestampEntity;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;

/**
 * generated: 16/10/2016 15:27:08
 **/
@Entity
@Audited
@Table(name = "EMPRESA")
public class Empresa extends AbstractTimestampEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "NOME_FANTASIA", length = 200)
	private String nomeFantasia;

	@Column(name = "CNPJ", length = 18)
	private String cnpj;

	@Column(name = "CONTRATO", length = 20)
	private String contrato;

	@Column(name = "RAZAO_SOCIAL", length = 200)
	private String razaoSocial;

	@Column(name = "ENDERECO", length = 200)
	private String endereco;

	@Column(name = "NUMERO_ENDERECO", length = 10)
	private String numeroEndereco;

	@Column(name = "COMPLEMENTO", length = 100)
	private String complemento;

	@Column(name = "UF", length = 2)
	private String uf;

	@Column(name = "CIDADE", length = 50)
	private String cidade;

	@Column(name = "BAIRRO", length = 100)
	private String bairro;

	@Column(name = "CEP", length = 9)
	private String cep;

	@Column(name = "REFERENCIA_ENDERECO", length = 200)
	private String referenciaEndereco;

	@Column(name = "DDD", length = 3)
	private String ddd;

	@Column(name = "FONE", length = 15)
	private String fone;

	@Column(name = "UTILIZA_VOUCHER")
	private Boolean utilizaVoucher;

	@Column(name = "UTILIZA_ETICKET")
	private Boolean utilizaEticket;

	@Column(name = "OBSERVACAO_TAXISTA", length = 1000)
	private String observacaoTaxista;

	@Column(name = "OBSERVACAO_CENTRAL", length = 1000)
	private String observacaoCentral;

	@Column(name = "OBSERVACAO_FINANCEIRO", length = 1000)
	private String observacaoFinanceiro;

	@Column(name = "PERCENTUAL_DESCONTO")
	private Double percentualDesconto;

	@Column(name = "INSCRICAO_MUNICIPAL", length = 14)
	private String inscricaoMunicipal;

	@Column(name = "INSCRICAO_ESTADUAL", length = 14)
	private String inscricaoEstadual;

	@Column(name = "EMITE_NF")
	private Boolean emiteNf;

	@Column(name = "PERCENTUAL_ISS")
	private Double percentualIss;

	@Column(name = "PERCENTUAL_IRF")
	private Double percentualIrf;

	@Column(name = "PERCENTUAL_INSS")
	private Double percentualInss;

	@Column(name = "DIA_VENCIMENTO")
	private Integer diaVencimento;

	@Column(name = "PERCENTUAL_MOTORISTA")
	private Double percentualMotorista;

	@Column(name = "BANCO", length = 10)
	private String banco;

	@Column(name = "AGENCIA", length = 10)
	private String agencia;

	@Column(name = "CONTA", length = 20)
	private String conta;

	@Column(name = "EMAIL", length = 100)
	private String email;

	@Column(name = "USUARIO_CADASTRO")
	private Integer usuarioCadastro;

	@Column(name = "DATA_CADASTRO")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	private LocalDateTime dataCadastro;

	@Column(name = "NOME_CLIENTE1", length = 200)
	private String nomeCliente1;

	@Column(name = "FONE_CLIENTE1", length = 15)
	private String foneCliente1;

	@Column(name = "NOME_CLIENTE2", length = 200)
	private String nomeCliente2;

	@Column(name = "FONE_CLIENTE2", length = 15)
	private String foneCliente2;

	@Column(name = "NOME_FORNECEDOR1", length = 200)
	private String nomeFornecedor1;

	@Column(name = "FONE_FORNECEDOR1", length = 15)
	private String foneFornecedor1;

	@Column(name = "NOME_FORNECEDOR2", length = 200)
	private String nomeFornecedor2;

	@Column(name = "FONE_FORNECEDOR2", length = 15)
	private String foneFornecedor2;

	@Column(name = "STATUS_EMPRESA", length = 10)
	private String statusEmpresa;
	
	@Column(name = "USERNAME", length = 100)
	private String username;

	@Column(name = "SENHA", length = 100)
	private String senha;

	@OneToMany(mappedBy = "empresa")
	private List<CentroCusto> centroCustos;

	@OneToMany(mappedBy = "empresa")
	private List<HdUsuario> hdUsuarios;

	@OneToMany(mappedBy = "empresa")
	private List<Contato> contatos;

	@OneToMany(mappedBy = "empresa")
	private List<Socio> socios;

	@OneToMany(mappedBy = "empresa")
	private List<FaixaVoucher> faixaVouchers;

	@OneToMany(mappedBy = "empresa")
	private List<Cliente> clientes;

	@OneToMany(mappedBy = "empresa")
	private List<MotoristaBloqueado> motoristaBloqueados;

	@OneToMany(mappedBy = "empresa")
	private List<EnderecoFavorito> enderecosFavoritos;

	@OneToMany(mappedBy = "empresa")
	private List<TelefoneFavorito> telefonesFavoritos;

	public Empresa() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumeroEndereco() {
		return numeroEndereco;
	}

	public void setNumeroEndereco(String numeroEndereco) {
		this.numeroEndereco = numeroEndereco;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getReferenciaEndereco() {
		return referenciaEndereco;
	}

	public void setReferenciaEndereco(String referenciaEndereco) {
		this.referenciaEndereco = referenciaEndereco;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public Boolean getUtilizaVoucher() {
		return utilizaVoucher;
	}

	public void setUtilizaVoucher(Boolean utilizaVoucher) {
		this.utilizaVoucher = utilizaVoucher;
	}

	public Boolean getUtilizaEticket() {
		return utilizaEticket;
	}

	public void setUtilizaEticket(Boolean utilizaEticket) {
		this.utilizaEticket = utilizaEticket;
	}

	public String getObservacaoTaxista() {
		return observacaoTaxista;
	}

	public void setObservacaoTaxista(String observacaoTaxista) {
		this.observacaoTaxista = observacaoTaxista;
	}

	public String getObservacaoCentral() {
		return observacaoCentral;
	}

	public void setObservacaoCentral(String observacaoCentral) {
		this.observacaoCentral = observacaoCentral;
	}

	public Double getPercentualDesconto() {
		return percentualDesconto;
	}

	public void setPercentualDesconto(Double percentualDesconto) {
		this.percentualDesconto = percentualDesconto;
	}

	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public Boolean getEmiteNf() {
		return emiteNf;
	}

	public void setEmiteNf(Boolean emiteNf) {
		this.emiteNf = emiteNf;
	}

	public Double getPercentualIss() {
		return percentualIss;
	}

	public void setPercentualIss(Double percentualIss) {
		this.percentualIss = percentualIss;
	}

	public Double getPercentualIrf() {
		return percentualIrf;
	}

	public void setPercentualIrf(Double percentualIrf) {
		this.percentualIrf = percentualIrf;
	}

	public Double getPercentualInss() {
		return percentualInss;
	}

	public void setPercentualInss(Double percentualInss) {
		this.percentualInss = percentualInss;
	}

	public Integer getDiaVencimento() {
		return diaVencimento;
	}

	public void setDiaVencimento(Integer diaVencimento) {
		this.diaVencimento = diaVencimento;
	}

	public Double getPercentualMotorista() {
		return percentualMotorista;
	}

	public void setPercentualMotorista(Double percentualMotorista) {
		this.percentualMotorista = percentualMotorista;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getUsuarioCadastro() {
		return usuarioCadastro;
	}

	public void setUsuarioCadastro(Integer usuarioCadastro) {
		this.usuarioCadastro = usuarioCadastro;
	}

	public LocalDateTime getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDateTime dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getStatusEmpresa() {
		return statusEmpresa;
	}

	public void setStatusEmpresa(String statusEmpresa) {
		this.statusEmpresa = statusEmpresa;
	}

	public void setCentroCustos(List<CentroCusto> centroCustos) {
		this.centroCustos = centroCustos;
	}

	public List<CentroCusto> getCentroCustos() {
		if (this.centroCustos == null) {
			setCentroCustos(new ArrayList<CentroCusto>());
		}
		return this.centroCustos;
	}

	public boolean addCentroCustos(CentroCusto centroCusto) {
		centroCusto.setEmpresa(this);
		return getCentroCustos().add(centroCusto);
	}

	public boolean removeCentroCustos(CentroCusto centroCusto) {
		centroCusto.setEmpresa(null);
		return getCentroCustos().remove(centroCusto);
	}

	public void setHdUsuarios(List<HdUsuario> hdUsuarios) {
		this.hdUsuarios = hdUsuarios;
	}

	public List<HdUsuario> getHdUsuarios() {
		if (this.hdUsuarios == null) {
			setHdUsuarios(new ArrayList<HdUsuario>());
		}
		return this.hdUsuarios;
	}

	public boolean addHdUsuarios(HdUsuario hdUsuario) {
		hdUsuario.setEmpresa(this);
		return getHdUsuarios().add(hdUsuario);
	}

	public boolean removeHdUsuarios(HdUsuario hdUsuario) {
		hdUsuario.setEmpresa(null);
		return getHdUsuarios().remove(hdUsuario);
	}

	public void setContatos(List<Contato> contatos) {
		this.contatos = contatos;
	}

	public List<Contato> getContatos() {
		if (this.contatos == null) {
			setContatos(new ArrayList<Contato>());
		}
		return this.contatos;
	}

	public boolean addContatos(Contato contato) {
		contato.setEmpresa(this);
		return getContatos().add(contato);
	}

	public boolean removeContatos(Contato contato) {
		contato.setEmpresa(null);
		return getContatos().remove(contato);
	}

	public void setSocios(List<Socio> socios) {
		this.socios = socios;
	}

	public List<Socio> getSocios() {
		if (this.socios == null) {
			setSocios(new ArrayList<Socio>());
		}
		return this.socios;
	}

	public boolean addSocios(Socio socio) {
		socio.setEmpresa(this);
		return getSocios().add(socio);
	}

	public boolean removeSocios(Socio socio) {
		socio.setEmpresa(null);
		return getSocios().remove(socio);
	}

	public void setFaixaVouchers(List<FaixaVoucher> faixaVouchers) {
		this.faixaVouchers = faixaVouchers;
	}

	public List<FaixaVoucher> getFaixaVouchers() {
		if (this.faixaVouchers == null) {
			setFaixaVouchers(new ArrayList<FaixaVoucher>());
		}
		return this.faixaVouchers;
	}

	public boolean addFaixaVouchers(FaixaVoucher faixaVoucher) {
		faixaVoucher.setEmpresa(this);
		return getFaixaVouchers().add(faixaVoucher);
	}

	public boolean removeFaixaVouchers(FaixaVoucher faixaVoucher) {
		faixaVoucher.setEmpresa(null);
		return getFaixaVouchers().remove(faixaVoucher);
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public List<Cliente> getClientes() {
		if (this.clientes == null) {
			setClientes(new ArrayList<Cliente>());
		}
		return this.clientes;
	}

	public boolean addClientes(Cliente cliente) {
		cliente.setEmpresa(this);
		return getClientes().add(cliente);
	}

	public boolean removeClientes(Cliente cliente) {
		cliente.setEmpresa(null);
		return getClientes().remove(cliente);
	}

	// vitoriano
	public void setMotoristaBloqueados(List<MotoristaBloqueado> motoristaBloqueados) {
		this.motoristaBloqueados = motoristaBloqueados;
	}

	public List<MotoristaBloqueado> getMotoristaBloqueados() {
		if (this.motoristaBloqueados == null) {
			setMotoristaBloqueados(new ArrayList<MotoristaBloqueado>());
		}
		return this.motoristaBloqueados;
	}

	public boolean addMotoristaBloqueados(MotoristaBloqueado motoristaBloqueado) {
		motoristaBloqueado.setEmpresa(this);
		return getMotoristaBloqueados().add(motoristaBloqueado);
	}

	public boolean removeMotoristaBloqueados(MotoristaBloqueado motoristaBloqueado) {
		motoristaBloqueado.setEmpresa(null);
		return getMotoristaBloqueados().remove(motoristaBloqueado);
	}

	//
	public String getObservacaoFinanceiro() {
		return observacaoFinanceiro;
	}

	public void setObservacaoFinanceiro(String observacaoFinanceiro) {
		this.observacaoFinanceiro = observacaoFinanceiro;
	}

	public List<EnderecoFavorito> getEnderecosFavoritos() {
		if(enderecosFavoritos == null)
			setEnderecosFavoritos(new ArrayList<EnderecoFavorito>());
		return enderecosFavoritos;
	}

	public void setEnderecosFavoritos(List<EnderecoFavorito> enderecosFavoritos) {
		this.enderecosFavoritos = enderecosFavoritos;
	}

	public boolean addEnderecosFavoritos(EnderecoFavorito enderecoFavorito) {
		enderecoFavorito.setEmpresa(this);
		return getEnderecosFavoritos().add(enderecoFavorito);
	}

	public String getNomeCliente1() {
		return nomeCliente1;
	}

	public void setNomeCliente1(String nomeCliente1) {
		this.nomeCliente1 = nomeCliente1;
	}

	public String getFoneCliente1() {
		return foneCliente1;
	}

	public void setFoneCliente1(String foneCliente1) {
		this.foneCliente1 = foneCliente1;
	}

	public String getNomeCliente2() {
		return nomeCliente2;
	}

	public void setNomeCliente2(String nomeCliente2) {
		this.nomeCliente2 = nomeCliente2;
	}

	public String getFoneCliente2() {
		return foneCliente2;
	}

	public void setFoneCliente2(String foneCliente2) {
		this.foneCliente2 = foneCliente2;
	}

	public String getNomeFornecedor1() {
		return nomeFornecedor1;
	}

	public void setNomeFornecedor1(String nomeFornecedor1) {
		this.nomeFornecedor1 = nomeFornecedor1;
	}

	public String getFoneFornecedor1() {
		return foneFornecedor1;
	}

	public void setFoneFornecedor1(String foneFornecedor1) {
		this.foneFornecedor1 = foneFornecedor1;
	}

	public String getNomeFornecedor2() {
		return nomeFornecedor2;
	}

	public void setNomeFornecedor2(String nomeFornecedor2) {
		this.nomeFornecedor2 = nomeFornecedor2;
	}

	public String getFoneFornecedor2() {
		return foneFornecedor2;
	}

	public void setFoneFornecedor2(String foneFornecedor2) {
		this.foneFornecedor2 = foneFornecedor2;
	}

	public List<TelefoneFavorito> getTelefonesFavoritos() {
		if(telefonesFavoritos == null){
			setTelefonesFavoritos(new ArrayList<TelefoneFavorito>());
		}
		return telefonesFavoritos;
	}

	public void setTelefonesFavoritos(List<TelefoneFavorito> telefonesFavoritos) {
		this.telefonesFavoritos = telefonesFavoritos;
	}

	public boolean addTelefoneFavorito(TelefoneFavorito telefoneFavorito) {
		telefoneFavorito.setEmpresa(this);
		return getTelefonesFavoritos().add(telefoneFavorito);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}
