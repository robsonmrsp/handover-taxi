package br.com.invista.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.model.AbstractTimestampEntity;

/**
 * generated: 16/10/2016 15:27:07
 **/
@Entity
@Audited
@Table(name = "AREA")
public class Area extends AbstractTimestampEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "ORDEM")
	private Integer ordem;

	@Column(name = "DESCRICAO", length = 100)
	private String descricao;

	@Column(name = "COORDENADAS", length = 500)
	private String coordenadas;

	@OneToMany(mappedBy = "area")
	private List<PontosArea> pontosAreas;

	public Area() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(String coordenadas) {
		this.coordenadas = coordenadas;
	}

	public void setPontosAreas(List<PontosArea> pontosAreas) {
		this.pontosAreas = pontosAreas;
	}

	public List<PontosArea> getPontosAreas() {
		if (this.pontosAreas == null) {
			setPontosAreas(new ArrayList<PontosArea>());
		}
		return this.pontosAreas;
	}

	public boolean addPontosAreas(PontosArea pontosArea) {
		pontosArea.setArea(this);
		return getPontosAreas().add(pontosArea);
	}

	public boolean removePontosAreas(PontosArea pontosArea) {
		pontosArea.setArea(null);
		return getPontosAreas().remove(pontosArea);
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

}
