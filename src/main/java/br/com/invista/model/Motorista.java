package br.com.invista.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.invista.core.model.AbstractTimestampEntity;
import br.com.invista.core.serialization.CustomLocalDateSerializer;

/**
 * generated: 18/10/2016 23:48:44
 **/
@Entity
@Audited
@Table(name = "MOTORISTA")
public class Motorista extends AbstractTimestampEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "NOME", length = 200)
	private String nome;

	@Column(name = "NOME_REDUZIDO", length = 50)
	private String nomeReduzido;

	@Column(name = "VIATURA", length = 10)
	private String viatura;

	@Column(name = "EMAIL", length = 100)
	private String email;

	@Column(name = "DDD", length = 3)
	private String ddd;

	@Column(name = "FONE", length = 15)
	private String fone;

	@Column(name = "STATUS_MOTORISTA", length = 20)
	private String statusMotorista;

	@Column(name = "STATUS_APP", length = 20)
	private String statusApp;

	@Column(name = "DDD2", length = 3)
	private String ddd2;

	@Column(name = "FONE2", length = 15)
	private String fone2;

	@Column(name = "DDD_WHATSAPP", length = 3)
	private String dddWhatsapp;

	@Column(name = "WHATSAPP", length = 15)
	private String whatsapp;

	@Column(name = "DATA_NASCIMENTO")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomLocalDateSerializer.class)
	private LocalDate dataNascimento;

	@Column(name = "RG", length = 15)
	private String rg;

	@Column(name = "ORGAO_EXPEDIDOR", length = 10)
	private String orgaoExpedidor;

	@Column(name = "DATA_EXPEDICAO")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomLocalDateSerializer.class)
	private LocalDate dataExpedicao;

	@Column(name = "NATURALIDADE", length = 50)
	private String naturalidade;

	@Column(name = "CPF", length = 14)
	private String cpf;

	@Column(name = "CNH", length = 12)
	private String cnh;

	@Column(name = "OBSERVACAO", length = 1000)
	private String observacao;

	@Column(name = "TEMPO_ULTIMA_POSICAO")@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomLocalDateSerializer.class)
	private LocalDate tempoUltimaPosicao;

	@Column(name = "EMISSAO_CNH")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomLocalDateSerializer.class)
	private LocalDate emissaoCnh;

	@Column(name = "CATEGORIA_CNH", length = 2)
	private String categoriaCnh;

	@Column(name = "VALIDADE_CNH")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomLocalDateSerializer.class)
	private LocalDate validadeCnh;

	@Column(name = "FOTO1", length = 150)
	private String foto1;

	@Column(name = "FOTO2", length = 150)
	private String foto2;

	@Column(name = "FOTO3", length = 150)
	private String foto3;

	@Column(name = "ENDERECO", length = 200)
	private String endereco;

	@Column(name = "NUMERO_ENDERECO", length = 10)
	private String numeroEndereco;

	@Column(name = "COMPLEMENTO", length = 200)
	private String complemento;

	@Column(name = "UF", length = 2)
	private String uf;

	@Column(name = "CIDADE", length = 50)
	private String cidade;

	@Column(name = "BAIRRO", length = 100)
	private String bairro;

	@Column(name = "CEP", length = 9)
	private String cep;

	@Column(name = "REFERENCIA_ENDERECO", length = 200)
	private String referenciaEndereco;

	@Column(name = "MAE", length = 200)
	private String mae;

	@Column(name = "PAI", length = 200)
	private String pai;

	@Column(name = "CONJUGE", length = 200)
	private String conjuge;

	@Column(name = "REFERENCIA_PESSOAL1", length = 200)
	private String referenciaPessoal1;

	@Column(name = "FONE_REFERENCIA1", length = 15)
	private String foneReferencia1;

	@Column(name = "PARENTESCO_REFERENCIA1", length = 20)
	private String parentescoReferencia1;

	@Column(name = "REFERENCIA_PESSOAL2", length = 200)
	private String referenciaPessoal2;

	@Column(name = "FONE_REFERENCIA2", length = 15)
	private String foneReferencia2;

	@Column(name = "PARENTESCO_REFERENCIA2", length = 20)
	private String parentescoReferencia2;

	@Column(name = "MARCA_VEICULO", length = 20)
	private String marcaVeiculo;

	@Column(name = "MODELO_VEICULO", length = 50)
	private String modeloVeiculo;

	@Column(name = "PLACA_VEICULO", length = 8)
	private String placaVeiculo;

	@Column(name = "COR_VEICULO", length = 20)
	private String corVeiculo;

	@Column(name = "ANO_VEICULO")
	private Integer anoVeiculo;

	@Column(name = "RENAVAM", length = 20)
	private String renavam;

	@Column(name = "CHASSI", length = 20)
	private String chassi;

	@Column(name = "CATEGORIA", length = 20)
	private String categoria;

	@Column(name = "IMEI", length = 20)
	private String imei;

	@Column(name = "CARTAO_CREDITO")
	private Boolean cartaoCredito;

	@Column(name = "CARTAO_DEBITO")
	private Boolean cartaoDebito;

	@Column(name = "VOUCHER")
	private Boolean voucher;

	@Column(name = "LATITUDE")
	private Double latitude;

	@Column(name = "LONGITUDE")
	private Double longitude;

	@Column(name = "ETICKET")
	private Boolean eticket;

	@Column(name = "SENHA", length = 100)
	private String senha;

	@Column(name = "PADRONIZADA")
	private Boolean padronizada;

	@Column(name = "BAU")
	private Boolean bau;

	@Column(name = "MOTO_GRANDE")
	private Boolean motoGrande;

	@OneToMany(mappedBy = "motorista")
	private List<Punicao> punicaos;

	@OneToMany(mappedBy = "motorista")
	private List<MensagemMotorista> mensagemMotoristas;

	@OneToMany(mappedBy = "motorista")
	private List<MotoristaBloqueado> motoristaBloqueados;

	@OneToMany(mappedBy = "motorista")
	private List<Corrida> corridas;

	@ManyToMany(mappedBy = "motoristas")
	private List<Plantao> plantoes;

	@OneToMany(mappedBy = "motorista")
	private List<FaixaVoucher> faixaVouchers;

	public List<FaixaVoucher> getFaixaVouchers() {
		return faixaVouchers;
	}

	public void setFaixaVouchers(List<FaixaVoucher> faixaVouchers) {
		this.faixaVouchers = faixaVouchers;
	}

	public Motorista() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeReduzido() {
		return nomeReduzido;
	}

	public void setNomeReduzido(String nomeReduzido) {
		this.nomeReduzido = nomeReduzido;
	}

	public String getViatura() {
		return viatura;
	}

	public void setViatura(String viatura) {
		this.viatura = viatura;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public String getStatusMotorista() {
		return statusMotorista;
	}

	public void setStatusMotorista(String statusMotorista) {
		this.statusMotorista = statusMotorista;
	}

	public String getDdd2() {
		return ddd2;
	}

	public void setDdd2(String ddd2) {
		this.ddd2 = ddd2;
	}

	public String getFone2() {
		return fone2;
	}

	public void setFone2(String fone2) {
		this.fone2 = fone2;
	}

	public String getDddWhatsapp() {
		return dddWhatsapp;
	}

	public void setDddWhatsapp(String dddWhatsapp) {
		this.dddWhatsapp = dddWhatsapp;
	}

	public String getWhatsapp() {
		return whatsapp;
	}

	public void setWhatsapp(String whatsapp) {
		this.whatsapp = whatsapp;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getOrgaoExpedidor() {
		return orgaoExpedidor;
	}

	public void setOrgaoExpedidor(String orgaoExpedidor) {
		this.orgaoExpedidor = orgaoExpedidor;
	}

	public LocalDate getDataExpedicao() {
		return dataExpedicao;
	}

	public void setDataExpedicao(LocalDate dataExpedicao) {
		this.dataExpedicao = dataExpedicao;
	}

	public String getNaturalidade() {
		return naturalidade;
	}

	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnh() {
		return cnh;
	}

	public void setCnh(String cnh) {
		this.cnh = cnh;
	}

	public LocalDate getEmissaoCnh() {
		return emissaoCnh;
	}

	public void setEmissaoCnh(LocalDate emissaoCnh) {
		this.emissaoCnh = emissaoCnh;
	}

	public String getCategoriaCnh() {
		return categoriaCnh;
	}

	public void setCategoriaCnh(String categoriaCnh) {
		this.categoriaCnh = categoriaCnh;
	}

	public LocalDate getValidadeCnh() {
		return validadeCnh;
	}

	public void setValidadeCnh(LocalDate validadeCnh) {
		this.validadeCnh = validadeCnh;
	}

	public String getFoto1() {
		return foto1;
	}

	public void setFoto1(String foto1) {
		this.foto1 = foto1;
	}

	public String getFoto2() {
		return foto2;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public LocalDate getTempoUltimaPosicao() {
		return tempoUltimaPosicao;
	}

	public void setTempoUltimaPosicao(LocalDate tempoUltimaPosicao) {
		this.tempoUltimaPosicao = tempoUltimaPosicao;
	}

	public void setFoto2(String foto2) {
		this.foto2 = foto2;
	}

	public String getFoto3() {
		return foto3;
	}

	public void setFoto3(String foto3) {
		this.foto3 = foto3;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumeroEndereco() {
		return numeroEndereco;
	}

	public void setNumeroEndereco(String numeroEndereco) {
		this.numeroEndereco = numeroEndereco;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getReferenciaEndereco() {
		return referenciaEndereco;
	}

	public void setReferenciaEndereco(String referenciaEndereco) {
		this.referenciaEndereco = referenciaEndereco;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getConjuge() {
		return conjuge;
	}

	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}

	public String getReferenciaPessoal1() {
		return referenciaPessoal1;
	}

	public void setReferenciaPessoal1(String referenciaPessoal1) {
		this.referenciaPessoal1 = referenciaPessoal1;
	}

	public String getFoneReferencia1() {
		return foneReferencia1;
	}

	public void setFoneReferencia1(String foneReferencia1) {
		this.foneReferencia1 = foneReferencia1;
	}

	public String getParentescoReferencia1() {
		return parentescoReferencia1;
	}

	public void setParentescoReferencia1(String parentescoReferencia1) {
		this.parentescoReferencia1 = parentescoReferencia1;
	}

	public String getReferenciaPessoal2() {
		return referenciaPessoal2;
	}

	public void setReferenciaPessoal2(String referenciaPessoal2) {
		this.referenciaPessoal2 = referenciaPessoal2;
	}

	public String getFoneReferencia2() {
		return foneReferencia2;
	}

	public void setFoneReferencia2(String foneReferencia2) {
		this.foneReferencia2 = foneReferencia2;
	}

	public String getParentescoReferencia2() {
		return parentescoReferencia2;
	}

	public void setParentescoReferencia2(String parentescoReferencia2) {
		this.parentescoReferencia2 = parentescoReferencia2;
	}

	public String getMarcaVeiculo() {
		return marcaVeiculo;
	}

	public void setMarcaVeiculo(String marcaVeiculo) {
		this.marcaVeiculo = marcaVeiculo;
	}

	public String getModeloVeiculo() {
		return modeloVeiculo;
	}

	public void setModeloVeiculo(String modeloVeiculo) {
		this.modeloVeiculo = modeloVeiculo;
	}

	public String getPlacaVeiculo() {
		return placaVeiculo;
	}

	public void setPlacaVeiculo(String placaVeiculo) {
		this.placaVeiculo = placaVeiculo;
	}

	public String getCorVeiculo() {
		return corVeiculo;
	}

	public void setCorVeiculo(String corVeiculo) {
		this.corVeiculo = corVeiculo;
	}

	public Integer getAnoVeiculo() {
		return anoVeiculo;
	}

	public void setAnoVeiculo(Integer anoVeiculo) {
		this.anoVeiculo = anoVeiculo;
	}

	public String getRenavam() {
		return renavam;
	}

	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}

	public String getChassi() {
		return chassi;
	}

	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public Boolean getCartaoCredito() {
		return cartaoCredito;
	}

	public void setCartaoCredito(Boolean cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}

	public Boolean getCartaoDebito() {
		return cartaoDebito;
	}

	public void setCartaoDebito(Boolean cartaoDebito) {
		this.cartaoDebito = cartaoDebito;
	}

	public Boolean getVoucher() {
		return voucher;
	}

	public void setVoucher(Boolean voucher) {
		this.voucher = voucher;
	}

	public Boolean getEticket() {
		return eticket;
	}

	public void setEticket(Boolean eticket) {
		this.eticket = eticket;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Boolean getPadronizada() {
		return padronizada;
	}

	public void setPadronizada(Boolean padronizada) {
		this.padronizada = padronizada;
	}

	public Boolean getBau() {
		return bau;
	}

	public void setBau(Boolean bau) {
		this.bau = bau;
	}

	public Boolean getMotoGrande() {
		return motoGrande;
	}

	public void setMotoGrande(Boolean motoGrande) {
		this.motoGrande = motoGrande;
	}

	public void setPunicaos(List<Punicao> punicaos) {
		this.punicaos = punicaos;
	}

	public List<Punicao> getPunicaos() {
		if (this.punicaos == null) {
			setPunicaos(new ArrayList<Punicao>());
		}
		return this.punicaos;
	}

	public boolean addPunicaos(Punicao punicao) {
		punicao.setMotorista(this);
		return getPunicaos().add(punicao);
	}

	public boolean removePunicaos(Punicao punicao) {
		punicao.setMotorista(null);
		return getPunicaos().remove(punicao);
	}

	public void setMensagemMotoristas(List<MensagemMotorista> mensagemMotoristas) {
		this.mensagemMotoristas = mensagemMotoristas;
	}

	public List<MensagemMotorista> getMensagemMotoristas() {
		if (this.mensagemMotoristas == null) {
			setMensagemMotoristas(new ArrayList<MensagemMotorista>());
		}
		return this.mensagemMotoristas;
	}

	public boolean addMensagemMotoristas(MensagemMotorista mensagemMotorista) {
		mensagemMotorista.setMotorista(this);
		return getMensagemMotoristas().add(mensagemMotorista);
	}

	public boolean removeMensagemMotoristas(MensagemMotorista mensagemMotorista) {
		mensagemMotorista.setMotorista(null);
		return getMensagemMotoristas().remove(mensagemMotorista);
	}

	public void setMotoristaBloqueados(List<MotoristaBloqueado> motoristaBloqueados) {
		this.motoristaBloqueados = motoristaBloqueados;
	}

	public List<MotoristaBloqueado> getMotoristaBloqueados() {
		if (this.motoristaBloqueados == null) {
			setMotoristaBloqueados(new ArrayList<MotoristaBloqueado>());
		}
		return this.motoristaBloqueados;
	}

	public boolean addMotoristaBloqueados(MotoristaBloqueado motoristaBloqueado) {
		motoristaBloqueado.setMotorista(this);
		return getMotoristaBloqueados().add(motoristaBloqueado);
	}

	public boolean removeMotoristaBloqueados(MotoristaBloqueado motoristaBloqueado) {
		motoristaBloqueado.setMotorista(null);
		return getMotoristaBloqueados().remove(motoristaBloqueado);
	}

	public void setCorridas(List<Corrida> corridas) {
		this.corridas = corridas;
	}

	public List<Corrida> getCorridas() {
		if (this.corridas == null) {
			setCorridas(new ArrayList<Corrida>());
		}
		return this.corridas;
	}

	public boolean addCorridas(Corrida corrida) {
		corrida.setMotorista(this);
		return getCorridas().add(corrida);
	}

	public boolean removeCorridas(Corrida corrida) {
		corrida.setMotorista(null);
		return getCorridas().remove(corrida);
	}

	public void setPlantoes(List<Plantao> plantoes) {
		this.plantoes = plantoes;
	}

	public List<Plantao> getPlantoes() {
		if (this.plantoes == null) {
			setPlantoes(new ArrayList<Plantao>());
		}
		return this.plantoes;
	}

	public boolean addPlantoes(Plantao plantao) {
		return getPlantoes().add(plantao);
	}

	public boolean removePlantoes(Plantao plantao) {
		return getPlantoes().remove(plantao);
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getStatusApp() {
		return statusApp;
	}

	public void setStatusApp(String statusApp) {
		this.statusApp = statusApp;
	}

}
