package br.com.invista.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.model.AbstractTimestampEntity;
/**
* generated: 18/10/2016 23:48:44
**/
@Entity
@Audited
@Table(name = "PLANTAO")
public class Plantao extends AbstractTimestampEntity{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Integer id;
		
	@Column(name = "DATA_INICIO")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	private LocalDateTime dataInicio;
		
	@Column(name = "DATA_FIM")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	private LocalDateTime dataFim;
	
    @ManyToMany
    @Cascade(value = CascadeType.ALL)
    @JoinTable(name="PLANTAO_MOTORISTA", joinColumns = @JoinColumn(name = "PLANTAO_ID", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "MOTORISTA_ID", referencedColumnName = "ID") )
    private List<Motorista> motoristas;
		
	public  Plantao() {
		
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDateTime dataInicio) {
		this.dataInicio = dataInicio;
	}
	public LocalDateTime getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDateTime dataFim) {
		this.dataFim = dataFim;
	}
	public void setMotoristas(List<Motorista> motoristas){
		this.motoristas = motoristas;
	}
	
	public List<Motorista>  getMotoristas() {
		if(this.motoristas == null){
			setMotoristas(new ArrayList<Motorista>());
		}
		return this.motoristas; 
	}
		
	public boolean addMotoristas(Motorista motorista){	
		return getMotoristas().add(motorista);
	}
	
	public boolean removeMotoristas(Motorista motorista){	
		return getMotoristas().remove(motorista);
	}
	
}
