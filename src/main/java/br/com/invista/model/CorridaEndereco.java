package br.com.invista.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.model.AbstractTimestampEntity;
/**
* generated: 16/10/2016 15:27:08
**/
@Entity
@Audited
@Table(name = "CORRIDA_ENDERECO")
public class CorridaEndereco extends AbstractTimestampEntity{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Integer id;
		
	@Column(name = "ENDERECO_ORIGEM" , length=200)
	private String enderecoOrigem;		
		
	@Column(name = "NUMERO_ORIGEM" , length=10)
	private String numeroOrigem;		
		
	@Column(name = "COMPLEMENTO_ORIGEM" , length=50)
	private String complementoOrigem;		
		
	@Column(name = "BAIRRO_ORIGEM" , length=100)
	private String bairroOrigem;		
		
	@Column(name = "UF_ORIGEM" , length=2)
	private String ufOrigem;		
		
	@Column(name = "CIDADE_ORIGEM" , length=50)
	private String cidadeOrigem;		
		
	@Column(name = "CEP_ORIGEM" , length=9)
	private String cepOrigem;		
		
	@Column(name = "REFERENCIA_ORIGEM" , length=200)
	private String referenciaOrigem;		
		
	@Column(name = "ENDERECO_DESTINO" , length=200)
	private String enderecoDestino;		
		
	@Column(name = "NUMERO_DESTINO" , length=10)
	private String numeroDestino;		
		
	@Column(name = "COMPLEMENTO_DESTINO" , length=50)
	private String complementoDestino;		
		
	@Column(name = "BAIRRO_DESTINO" , length=100)
	private String bairroDestino;		
		
	@Column(name = "UF_DESTINO" , length=2)
	private String ufDestino;		
		
	@Column(name = "CIDADE_DESTINO" , length=50)
	private String cidadeDestino;		
		
	@Column(name = "CEP_DESTINO" , length=9)
	private String cepDestino;
	
	@Column(name = "DESCRICAO" , length=400)
	private String descricao;		
		
	@Column(name = "REFERENCIA_DESTINO" , length=200)
	private String referenciaDestino;		
	
	@ManyToOne
	@JoinColumn(name = "ID_CORRIDA")
	private Corrida corrida;		
		
	public  CorridaEndereco() {
		
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getEnderecoOrigem() {
		return enderecoOrigem;
	}

	public void setEnderecoOrigem(String enderecoOrigem) {
		this.enderecoOrigem = enderecoOrigem;
	}
	public String getNumeroOrigem() {
		return numeroOrigem;
	}

	public void setNumeroOrigem(String numeroOrigem) {
		this.numeroOrigem = numeroOrigem;
	}
	public String getComplementoOrigem() {
		return complementoOrigem;
	}

	public void setComplementoOrigem(String complementoOrigem) {
		this.complementoOrigem = complementoOrigem;
	}
	public String getBairroOrigem() {
		return bairroOrigem;
	}

	public void setBairroOrigem(String bairroOrigem) {
		this.bairroOrigem = bairroOrigem;
	}
	public String getUfOrigem() {
		return ufOrigem;
	}

	public void setUfOrigem(String ufOrigem) {
		this.ufOrigem = ufOrigem;
	}
	public String getCidadeOrigem() {
		return cidadeOrigem;
	}

	public void setCidadeOrigem(String cidadeOrigem) {
		this.cidadeOrigem = cidadeOrigem;
	}
	public String getCepOrigem() {
		return cepOrigem;
	}

	public void setCepOrigem(String cepOrigem) {
		this.cepOrigem = cepOrigem;
	}
	public String getReferenciaOrigem() {
		return referenciaOrigem;
	}

	public void setReferenciaOrigem(String referenciaOrigem) {
		this.referenciaOrigem = referenciaOrigem;
	}
	public String getEnderecoDestino() {
		return enderecoDestino;
	}

	public void setEnderecoDestino(String enderecoDestino) {
		this.enderecoDestino = enderecoDestino;
	}
	public String getNumeroDestino() {
		return numeroDestino;
	}

	public void setNumeroDestino(String numeroDestino) {
		this.numeroDestino = numeroDestino;
	}
	public String getComplementoDestino() {
		return complementoDestino;
	}

	public void setComplementoDestino(String complementoDestino) {
		this.complementoDestino = complementoDestino;
	}
	public String getBairroDestino() {
		return bairroDestino;
	}

	public void setBairroDestino(String bairroDestino) {
		this.bairroDestino = bairroDestino;
	}
	public String getUfDestino() {
		return ufDestino;
	}

	public void setUfDestino(String ufDestino) {
		this.ufDestino = ufDestino;
	}
	public String getCidadeDestino() {
		return cidadeDestino;
	}

	public void setCidadeDestino(String cidadeDestino) {
		this.cidadeDestino = cidadeDestino;
	}
	public String getCepDestino() {
		return cepDestino;
	}

	public void setCepDestino(String cepDestino) {
		this.cepDestino = cepDestino;
	}
	public String getReferenciaDestino() {
		return referenciaDestino;
	}

	public void setReferenciaDestino(String referenciaDestino) {
		this.referenciaDestino = referenciaDestino;
	}
	public Corrida getCorrida() {
		return corrida;
	}
	
	public void setCorrida(Corrida corrida) {
		this.corrida = corrida;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
