package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;


/**
*  generated: 16/10/2016 15:27:08
**/
public class FilterMotoristaHasPlantao implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer motorista;		
	private Integer plantao;		
	
	public  FilterMotoristaHasPlantao() {
		
	}
	

		
	public Integer getMotorista() {
		return motorista;
	}
	
	public void setMotorista(Integer motorista) {
		this.motorista = motorista;
	}
	public Integer getPlantao() {
		return plantao;
	}
	
	public void setPlantao(Integer plantao) {
		this.plantao = plantao;
	}
	
}