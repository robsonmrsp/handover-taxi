package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;


/**
*  generated: 04/11/2016 11:29:27
**/
public class FilterVisitaTracker implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Double latitude;  			
	
	private Double longitude;  			
	
	private Long timestamp;  			
	
	private Double speed;  			
	
	private Double accuracy;  			
	
	private Double direction;  			
	
	private Double altitude;  			
	
	private Boolean baterryLevel;  			
	
	private Boolean gpsEnabled;  			
	
	private Boolean wifiEnabled;  			
	
	private Boolean mobileEnabled;  			

	
	public  FilterVisitaTracker() {
		
	}
	

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}
	public Double getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(Double accuracy) {
		this.accuracy = accuracy;
	}
	public Double getDirection() {
		return direction;
	}

	public void setDirection(Double direction) {
		this.direction = direction;
	}
	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}
	public Boolean getBaterryLevel() {
		return baterryLevel;
	}

	public void setBaterryLevel(Boolean baterryLevel) {
		this.baterryLevel = baterryLevel;
	}
	public Boolean getGpsEnabled() {
		return gpsEnabled;
	}

	public void setGpsEnabled(Boolean gpsEnabled) {
		this.gpsEnabled = gpsEnabled;
	}
	public Boolean getWifiEnabled() {
		return wifiEnabled;
	}

	public void setWifiEnabled(Boolean wifiEnabled) {
		this.wifiEnabled = wifiEnabled;
	}
	public Boolean getMobileEnabled() {
		return mobileEnabled;
	}

	public void setMobileEnabled(Boolean mobileEnabled) {
		this.mobileEnabled = mobileEnabled;
	}
		
	
}