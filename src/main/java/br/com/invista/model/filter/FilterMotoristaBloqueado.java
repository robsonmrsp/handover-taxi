package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;


/**
*  generated: 16/10/2016 15:27:08
**/
public class FilterMotoristaBloqueado implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	@JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
	private LocalDateTime dataBloqueio;
	
	private Integer usuarioBloqueio;  			

	private Integer cliente;		
	private Integer empresa;
	private Integer motorista;		
	
	public  FilterMotoristaBloqueado() {
		
	}
	

	public LocalDateTime getDataBloqueio() {
		return dataBloqueio;
	}

	public void setDataBloqueio(LocalDateTime dataBloqueio) {
		this.dataBloqueio = dataBloqueio;
	}
	public Integer getUsuarioBloqueio() {
		return usuarioBloqueio;
	}

	public void setUsuarioBloqueio(Integer usuarioBloqueio) {
		this.usuarioBloqueio = usuarioBloqueio;
	}
		
	public Integer getCliente() {
		return cliente;
	}
	
	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}
	public Integer getMotorista() {
		return motorista;
	}
	
	public void setMotorista(Integer motorista) {
		this.motorista = motorista;
	}

	public Integer getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	
}