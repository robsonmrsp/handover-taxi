package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;


/**
*  generated: 16/10/2016 15:27:08
**/
public class FilterContato implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nome;  			
	
	private String ddd;  			
	
	private String fone;  			
	
	private String setor;  			

	private Integer empresa;		
	
	public  FilterContato() {
		
	}
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}
	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}
	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}
		
	public Integer getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	
}