package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;


/**
*  generated: 16/10/2016 15:27:08
**/
public class FilterMensagemMotorista implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	@JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
	private LocalDateTime dataEnvio;
	
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	@JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
	private LocalDateTime dataLeitura;

	private Integer motorista;		
	private Integer mensagem;		
	
	public  FilterMensagemMotorista() {
		
	}
	

	public LocalDateTime getDataEnvio() {
		return dataEnvio;
	}

	public void setDataEnvio(LocalDateTime dataEnvio) {
		this.dataEnvio = dataEnvio;
	}
	public LocalDateTime getDataLeitura() {
		return dataLeitura;
	}

	public void setDataLeitura(LocalDateTime dataLeitura) {
		this.dataLeitura = dataLeitura;
	}
		
	public Integer getMotorista() {
		return motorista;
	}
	
	public void setMotorista(Integer motorista) {
		this.motorista = motorista;
	}
	public Integer getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(Integer mensagem) {
		this.mensagem = mensagem;
	}
	
}