package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;

/**
 * generated: 04/11/2016 11:29:26
 **/
public class FilterDistanciaMotoCorrida implements Serializable {
	private static final long serialVersionUID = 1L;

	private Double distancia;

	private Integer sequencia;

	private Integer motorista;

	private Integer corrida;

	private String statusCorrida;

	public FilterDistanciaMotoCorrida() {

	}

	public Double getDistancia() {
		return distancia;
	}

	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}

	public Integer getSequencia() {
		return sequencia;
	}

	public void setSequencia(Integer sequencia) {
		this.sequencia = sequencia;
	}

	public Integer getMotorista() {
		return motorista;
	}

	public void setMotorista(Integer motorista) {
		this.motorista = motorista;
	}

	public Integer getCorrida() {
		return corrida;
	}

	public void setCorrida(Integer corrida) {
		this.corrida = corrida;
	}

	public String getStatusCorrida() {
		return statusCorrida;
	}

	public void setStatusCorrida(String statusCorrida) {
		this.statusCorrida = statusCorrida;
	}

}