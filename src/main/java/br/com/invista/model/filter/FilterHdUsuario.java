package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;


/**
*  generated: 16/10/2016 15:27:08
**/
public class FilterHdUsuario implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nome;  			
	
	private String email;  			
	
	private String senha;  			
	
	private String tipo;  			
	
	private String perfil;  			
	
	private String statusUsuario;  			
	
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	@JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
	private LocalDateTime datainclusao;
	
	private Integer usuarioinclusao;  			

	private Integer empresa;		
	
	public  FilterHdUsuario() {
		
	}
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public String getStatusUsuario() {
		return statusUsuario;
	}

	public void setStatusUsuario(String statusUsuario) {
		this.statusUsuario = statusUsuario;
	}
	public LocalDateTime getDatainclusao() {
		return datainclusao;
	}

	public void setDatainclusao(LocalDateTime datainclusao) {
		this.datainclusao = datainclusao;
	}
	public Integer getUsuarioinclusao() {
		return usuarioinclusao;
	}

	public void setUsuarioinclusao(Integer usuarioinclusao) {
		this.usuarioinclusao = usuarioinclusao;
	}
		
	public Integer getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	
}