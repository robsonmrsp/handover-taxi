package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;


/**
*  generated: 04/11/2016 11:29:26
**/
public class FilterCorrida implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	@JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
	private LocalDateTime dataCorrida;
	
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	@JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
	private LocalDateTime dataAgendamento;

	private Integer tipoPagamento;
	
	private Integer id;
	
	private Double valor;  			
	
	private String observacao;  			
	
	private String statusCorrida;  			
	
	private Integer seqEmAtendimento;  			
	
	private Boolean motoGrande;  			
	
	private Boolean bau;
	
	private Boolean padronizada;
	
	private Boolean filaCriada;

	private Integer atendente;		
	private Integer motorista;
	private Integer cliente;
	private Integer empresa;
	private String telefone;
	
	public  FilterCorrida() {
		
	}
	

	public LocalDateTime getDataCorrida() {
		return dataCorrida;
	}

	public void setDataCorrida(LocalDateTime dataCorrida) {
		this.dataCorrida = dataCorrida;
	}
	public Integer getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(Integer tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getStatusCorrida() {
		return statusCorrida;
	}

	public void setStatusCorrida(String statusCorrida) {
		this.statusCorrida = statusCorrida;
	}
	public Integer getSeqEmAtendimento() {
		return seqEmAtendimento;
	}

	public void setSeqEmAtendimento(Integer seqEmAtendimento) {
		this.seqEmAtendimento = seqEmAtendimento;
	}
	public Boolean getMotoGrande() {
		return motoGrande;
	}

	public void setMotoGrande(Boolean motoGrande) {
		this.motoGrande = motoGrande;
	}
	public Boolean getBau() {
		return bau;
	}

	public void setBau(Boolean bau) {
		this.bau = bau;
	}
		
	public Integer getAtendente() {
		return atendente;
	}
	
	public void setAtendente(Integer atendente) {
		this.atendente = atendente;
	}
	public Integer getMotorista() {
		return motorista;
	}
	
	public void setMotorista(Integer motorista) {
		this.motorista = motorista;
	}


	public Integer getCliente() {
		return cliente;
	}


	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}


	public Integer getEmpresa() {
		return empresa;
	}


	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}


	public String getTelefone() {
		return telefone;
	}


	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Boolean getFilaCriada() {
		return filaCriada;
	}


	public void setFilaCriada(Boolean filaCriada) {
		this.filaCriada = filaCriada;
	}


	public Boolean getPadronizada() {
		return padronizada;
	}


	public void setPadronizada(Boolean padronizada) {
		this.padronizada = padronizada;
	}


	public LocalDateTime getDataAgendamento() {
		return dataAgendamento;
	}


	public void setDataAgendamento(LocalDateTime dataAgendamento) {
		this.dataAgendamento = dataAgendamento;
	}
	
}