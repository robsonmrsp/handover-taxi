package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;


/**
*  generated: 16/10/2016 15:27:08
**/
public class FilterCorridaEndereco implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String enderecoOrigem;  			
	
	private String numeroOrigem;  			
	
	private String complementoOrigem;  			
	
	private String bairroOrigem;  			
	
	private String ufOrigem;  			
	
	private String cidadeOrigem;  			
	
	private String cepOrigem;  			
	
	private String referenciaOrigem;  			
	
	private String enderecoDestino;  			
	
	private String numeroDestino;  			
	
	private String complementoDestino;  			
	
	private String bairroDestino;  			
	
	private String descricao;  			
	
	private String ufDestino;  			
	
	private String cidadeDestino;  			
	
	private String cepDestino;  			
	
	private String referenciaDestino;  			

	private Integer corrida;		
	
	public  FilterCorridaEndereco() {
		
	}
	

	public String getEnderecoOrigem() {
		return enderecoOrigem;
	}

	public void setEnderecoOrigem(String enderecoOrigem) {
		this.enderecoOrigem = enderecoOrigem;
	}
	public String getNumeroOrigem() {
		return numeroOrigem;
	}

	public void setNumeroOrigem(String numeroOrigem) {
		this.numeroOrigem = numeroOrigem;
	}
	public String getComplementoOrigem() {
		return complementoOrigem;
	}

	public void setComplementoOrigem(String complementoOrigem) {
		this.complementoOrigem = complementoOrigem;
	}
	public String getBairroOrigem() {
		return bairroOrigem;
	}

	public void setBairroOrigem(String bairroOrigem) {
		this.bairroOrigem = bairroOrigem;
	}
	public String getUfOrigem() {
		return ufOrigem;
	}

	public void setUfOrigem(String ufOrigem) {
		this.ufOrigem = ufOrigem;
	}
	public String getCidadeOrigem() {
		return cidadeOrigem;
	}

	public void setCidadeOrigem(String cidadeOrigem) {
		this.cidadeOrigem = cidadeOrigem;
	}
	public String getCepOrigem() {
		return cepOrigem;
	}

	public void setCepOrigem(String cepOrigem) {
		this.cepOrigem = cepOrigem;
	}
	public String getReferenciaOrigem() {
		return referenciaOrigem;
	}

	public void setReferenciaOrigem(String referenciaOrigem) {
		this.referenciaOrigem = referenciaOrigem;
	}
	public String getEnderecoDestino() {
		return enderecoDestino;
	}

	public void setEnderecoDestino(String enderecoDestino) {
		this.enderecoDestino = enderecoDestino;
	}
	public String getNumeroDestino() {
		return numeroDestino;
	}

	public void setNumeroDestino(String numeroDestino) {
		this.numeroDestino = numeroDestino;
	}
	public String getComplementoDestino() {
		return complementoDestino;
	}

	public void setComplementoDestino(String complementoDestino) {
		this.complementoDestino = complementoDestino;
	}
	public String getBairroDestino() {
		return bairroDestino;
	}

	public void setBairroDestino(String bairroDestino) {
		this.bairroDestino = bairroDestino;
	}
	public String getUfDestino() {
		return ufDestino;
	}

	public void setUfDestino(String ufDestino) {
		this.ufDestino = ufDestino;
	}
	public String getCidadeDestino() {
		return cidadeDestino;
	}

	public void setCidadeDestino(String cidadeDestino) {
		this.cidadeDestino = cidadeDestino;
	}
	public String getCepDestino() {
		return cepDestino;
	}

	public void setCepDestino(String cepDestino) {
		this.cepDestino = cepDestino;
	}
	public String getReferenciaDestino() {
		return referenciaDestino;
	}

	public void setReferenciaDestino(String referenciaDestino) {
		this.referenciaDestino = referenciaDestino;
	}
		
	public Integer getCorrida() {
		return corrida;
	}
	
	public void setCorrida(Integer corrida) {
		this.corrida = corrida;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}