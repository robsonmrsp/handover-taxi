package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;

/**
 * generated: 04/11/2016 11:29:26
 **/
public class FilterEvento implements Serializable {
	private static final long serialVersionUID = 1L;

	private String descricao;

	private Long dataInicio;

	private Long dataFim;

	private String descricaoCurta;

	private Integer enderecoCorrida;
	private Integer motorista;
	private Integer visitaTrackerSaida;
	private Integer visitaTrackerChegada;

	public FilterEvento() {

	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Long dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Long getDataFim() {
		return dataFim;
	}

	public void setDataFim(Long dataFim) {
		this.dataFim = dataFim;
	}

	public String getDescricaoCurta() {
		return descricaoCurta;
	}

	public void setDescricaoCurta(String descricaoCurta) {
		this.descricaoCurta = descricaoCurta;
	}

	public Integer getEnderecoCorrida() {
		return enderecoCorrida;
	}

	public void setEnderecoCorrida(Integer enderecoCorrida) {
		this.enderecoCorrida = enderecoCorrida;
	}

	public Integer getMotorista() {
		return motorista;
	}

	public void setMotorista(Integer motorista) {
		this.motorista = motorista;
	}

	public Integer getVisitaTrackerSaida() {
		return visitaTrackerSaida;
	}

	public void setVisitaTrackerSaida(Integer visitaTrackerSaida) {
		this.visitaTrackerSaida = visitaTrackerSaida;
	}

	public Integer getVisitaTrackerChegada() {
		return visitaTrackerChegada;
	}

	public void setVisitaTrackerChegada(Integer visitaTrackerChegada) {
		this.visitaTrackerChegada = visitaTrackerChegada;
	}

}