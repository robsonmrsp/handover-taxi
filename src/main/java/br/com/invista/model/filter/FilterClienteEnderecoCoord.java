package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;


/**
*  generated: 16/10/2016 15:27:07
**/
public class FilterClienteEnderecoCoord implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Double latitude;  			
	
	private Double longitude;  			
	
	private String enderecoFormatado;  			
	
	private Boolean valido;  			

	private Integer clienteEndereco;		
	
	public  FilterClienteEnderecoCoord() {
		
	}
	

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getEnderecoFormatado() {
		return enderecoFormatado;
	}

	public void setEnderecoFormatado(String enderecoFormatado) {
		this.enderecoFormatado = enderecoFormatado;
	}
	public Boolean getValido() {
		return valido;
	}

	public void setValido(Boolean valido) {
		this.valido = valido;
	}
		
	public Integer getClienteEndereco() {
		return clienteEndereco;
	}
	
	public void setClienteEndereco(Integer clienteEndereco) {
		this.clienteEndereco = clienteEndereco;
	}
	
}