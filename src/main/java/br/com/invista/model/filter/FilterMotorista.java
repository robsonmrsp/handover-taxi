package br.com.invista.model.filter;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;

/**
 * generated: 16/10/2016 15:27:08
 **/
public class FilterMotorista implements Serializable {
	private static final long serialVersionUID = 1L;

	private String nome;

	private String nomeReduzido;

	private String viatura;

	private String email;

	private String ddd;

	private String fone;

	private String statusMotorista;

	private String statusApp;

	private String ddd2;

	private String fone2;

	private String observacao;

	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	@JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
	private LocalDateTime tempoUltimaPosicao;

	private String dddWhatsapp;

	private String whatsapp;

	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	@JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
	private LocalDateTime dataNascimento;

	private String rg;

	private String orgaoExpedidor;

	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	@JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
	private LocalDateTime dataExpedicao;

	private String naturalidade;

	private String cpf;

	private String cnh;

	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	@JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
	private LocalDateTime emissaoCnh;

	private String categoriaCnh;

	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	@JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
	private LocalDateTime validadeCnh;

	private String foto1;

	private String foto2;

	private String foto3;

	private String endereco;

	private String numeroEndereco;

	private String complemento;

	private String uf;

	private String cidade;

	private String bairro;

	private String cep;

	private String referenciaEndereco;

	private String mae;

	private String pai;

	private String conjuge;

	private String referenciaPessoal1;

	private String foneReferencia1;

	private String parentescoReferencia1;

	private String referenciaPessoal2;

	private String foneReferencia2;

	private String parentescoReferencia2;

	private String marcaVeiculo;

	private String modeloVeiculo;

	private String placaVeiculo;

	private String corVeiculo;

	private Integer anoVeiculo;

	private String renavam;

	private String chassi;

	private String categoria;

	private String imei;

	private Boolean cartaoCredito;

	private Boolean cartaoDebito;

	private Boolean voucher;

	private Boolean eticket;

	private Integer usuarioCadastro;

	@JsonSerialize(using = CustomLocalDateSerializer.class)
	@JsonDeserialize(using = CustomLocalDateDeserializer.class)
	private LocalDate dataCadastro;

	private String senha;

	private Boolean padronizada;

	private Boolean bau;

	private Boolean motoGrande;

	public FilterMotorista() {

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeReduzido() {
		return nomeReduzido;
	}

	public void setNomeReduzido(String nomeReduzido) {
		this.nomeReduzido = nomeReduzido;
	}

	public String getViatura() {
		return viatura;
	}

	public void setViatura(String viatura) {
		this.viatura = viatura;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public String getStatusMotorista() {
		return statusMotorista;
	}

	public void setStatusMotorista(String statusMotorista) {
		this.statusMotorista = statusMotorista;
	}

	public String getDdd2() {
		return ddd2;
	}

	public void setDdd2(String ddd2) {
		this.ddd2 = ddd2;
	}

	public String getFone2() {
		return fone2;
	}

	public void setFone2(String fone2) {
		this.fone2 = fone2;
	}

	public String getDddWhatsapp() {
		return dddWhatsapp;
	}

	public void setDddWhatsapp(String dddWhatsapp) {
		this.dddWhatsapp = dddWhatsapp;
	}

	public String getWhatsapp() {
		return whatsapp;
	}

	public void setWhatsapp(String whatsapp) {
		this.whatsapp = whatsapp;
	}

	public LocalDateTime getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDateTime dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getOrgaoExpedidor() {
		return orgaoExpedidor;
	}

	public void setOrgaoExpedidor(String orgaoExpedidor) {
		this.orgaoExpedidor = orgaoExpedidor;
	}

	public LocalDateTime getDataExpedicao() {
		return dataExpedicao;
	}

	public void setDataExpedicao(LocalDateTime dataExpedicao) {
		this.dataExpedicao = dataExpedicao;
	}

	public String getNaturalidade() {
		return naturalidade;
	}

	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnh() {
		return cnh;
	}

	public void setCnh(String cnh) {
		this.cnh = cnh;
	}

	public LocalDateTime getEmissaoCnh() {
		return emissaoCnh;
	}

	public void setEmissaoCnh(LocalDateTime emissaoCnh) {
		this.emissaoCnh = emissaoCnh;
	}

	public String getCategoriaCnh() {
		return categoriaCnh;
	}

	public void setCategoriaCnh(String categoriaCnh) {
		this.categoriaCnh = categoriaCnh;
	}

	public LocalDateTime getValidadeCnh() {
		return validadeCnh;
	}

	public void setValidadeCnh(LocalDateTime validadeCnh) {
		this.validadeCnh = validadeCnh;
	}

	public String getFoto1() {
		return foto1;
	}

	public void setFoto1(String foto1) {
		this.foto1 = foto1;
	}

	public String getFoto2() {
		return foto2;
	}

	public void setFoto2(String foto2) {
		this.foto2 = foto2;
	}

	public String getFoto3() {
		return foto3;
	}

	public void setFoto3(String foto3) {
		this.foto3 = foto3;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumeroEndereco() {
		return numeroEndereco;
	}

	public void setNumeroEndereco(String numeroEndereco) {
		this.numeroEndereco = numeroEndereco;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getReferenciaEndereco() {
		return referenciaEndereco;
	}

	public void setReferenciaEndereco(String referenciaEndereco) {
		this.referenciaEndereco = referenciaEndereco;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getConjuge() {
		return conjuge;
	}

	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}

	public String getReferenciaPessoal1() {
		return referenciaPessoal1;
	}

	public void setReferenciaPessoal1(String referenciaPessoal1) {
		this.referenciaPessoal1 = referenciaPessoal1;
	}

	public String getFoneReferencia1() {
		return foneReferencia1;
	}

	public void setFoneReferencia1(String foneReferencia1) {
		this.foneReferencia1 = foneReferencia1;
	}

	public String getParentescoReferencia1() {
		return parentescoReferencia1;
	}

	public void setParentescoReferencia1(String parentescoReferencia1) {
		this.parentescoReferencia1 = parentescoReferencia1;
	}

	public String getReferenciaPessoal2() {
		return referenciaPessoal2;
	}

	public void setReferenciaPessoal2(String referenciaPessoal2) {
		this.referenciaPessoal2 = referenciaPessoal2;
	}

	public String getFoneReferencia2() {
		return foneReferencia2;
	}

	public void setFoneReferencia2(String foneReferencia2) {
		this.foneReferencia2 = foneReferencia2;
	}

	public String getParentescoReferencia2() {
		return parentescoReferencia2;
	}

	public void setParentescoReferencia2(String parentescoReferencia2) {
		this.parentescoReferencia2 = parentescoReferencia2;
	}

	public String getMarcaVeiculo() {
		return marcaVeiculo;
	}

	public void setMarcaVeiculo(String marcaVeiculo) {
		this.marcaVeiculo = marcaVeiculo;
	}

	public String getModeloVeiculo() {
		return modeloVeiculo;
	}

	public void setModeloVeiculo(String modeloVeiculo) {
		this.modeloVeiculo = modeloVeiculo;
	}

	public String getPlacaVeiculo() {
		return placaVeiculo;
	}

	public void setPlacaVeiculo(String placaVeiculo) {
		this.placaVeiculo = placaVeiculo;
	}

	public String getCorVeiculo() {
		return corVeiculo;
	}

	public void setCorVeiculo(String corVeiculo) {
		this.corVeiculo = corVeiculo;
	}

	public Integer getAnoVeiculo() {
		return anoVeiculo;
	}

	public void setAnoVeiculo(Integer anoVeiculo) {
		this.anoVeiculo = anoVeiculo;
	}

	public String getRenavam() {
		return renavam;
	}

	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}

	public String getChassi() {
		return chassi;
	}

	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public Boolean getCartaoCredito() {
		return cartaoCredito;
	}

	public void setCartaoCredito(Boolean cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}

	public Boolean getCartaoDebito() {
		return cartaoDebito;
	}

	public void setCartaoDebito(Boolean cartaoDebito) {
		this.cartaoDebito = cartaoDebito;
	}

	public Boolean getVoucher() {
		return voucher;
	}

	public void setVoucher(Boolean voucher) {
		this.voucher = voucher;
	}

	public Boolean getEticket() {
		return eticket;
	}

	public void setEticket(Boolean eticket) {
		this.eticket = eticket;
	}

	public Integer getUsuarioCadastro() {
		return usuarioCadastro;
	}

	public void setUsuarioCadastro(Integer usuarioCadastro) {
		this.usuarioCadastro = usuarioCadastro;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Boolean getPadronizada() {
		return padronizada;
	}

	public void setPadronizada(Boolean padronizada) {
		this.padronizada = padronizada;
	}

	public Boolean getBau() {
		return bau;
	}

	public void setBau(Boolean bau) {
		this.bau = bau;
	}

	public Boolean getMotoGrande() {
		return motoGrande;
	}

	public void setMotoGrande(Boolean motoGrande) {
		this.motoGrande = motoGrande;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public LocalDateTime getTempoUltimaPosicao() {
		return tempoUltimaPosicao;
	}

	public void setTempoUltimaPosicao(LocalDateTime tempoUltimaPosicao) {
		this.tempoUltimaPosicao = tempoUltimaPosicao;
	}

	public String getStatusApp() {
		return statusApp;
	}

	public void setStatusApp(String statusApp) {
		this.statusApp = statusApp;
	}

}