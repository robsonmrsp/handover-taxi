package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;

/**
 * generated: 16/10/2016 15:27:07
 **/
public class FilterCliente implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nome;

	private String email;

	private String username;

	private String senha;

	private String telefone;

	private String tipo;

	private Boolean inativo;

	private Integer usuarioCadastro;

	@JsonSerialize(using = CustomLocalDateSerializer.class)
	@JsonDeserialize(using = CustomLocalDateDeserializer.class)
	private LocalDate dataCadastro;

	private Integer matricula;

	private Double limiteMensal;

	private String cnpjCpf;

	private String cnpj;

	private String cpf;

	private Boolean autorizaEticket;

	private String idFacebook;

	private String emailFacebook;

	private String observacao;

	private Integer empresa;
	private Integer centroCusto;
	private String contrato;
	
	private String telefonesFavoritos;

	public FilterCliente() {

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Boolean getInativo() {
		return inativo;
	}

	public void setInativo(Boolean inativo) {
		this.inativo = inativo;
	}

	public Integer getUsuarioCadastro() {
		return usuarioCadastro;
	}

	public void setUsuarioCadastro(Integer usuarioCadastro) {
		this.usuarioCadastro = usuarioCadastro;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public Double getLimiteMensal() {
		return limiteMensal;
	}

	public void setLimiteMensal(Double limiteMensal) {
		this.limiteMensal = limiteMensal;
	}

	public String getCnpjCpf() {
		return cnpjCpf;
	}

	public void setCnpjCpf(String cnpjCpf) {
		this.cnpjCpf = cnpjCpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Boolean getAutorizaEticket() {
		return autorizaEticket;
	}

	public void setAutorizaEticket(Boolean autorizaEticket) {
		this.autorizaEticket = autorizaEticket;
	}

	public String getIdFacebook() {
		return idFacebook;
	}

	public void setIdFacebook(String idFacebook) {
		this.idFacebook = idFacebook;
	}

	public String getEmailFacebook() {
		return emailFacebook;
	}

	public void setEmailFacebook(String emailFacebook) {
		this.emailFacebook = emailFacebook;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}

	public Integer getCentroCusto() {
		return centroCusto;
	}

	public void setCentroCusto(Integer centroCusto) {
		this.centroCusto = centroCusto;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getTelefonesFavoritos() {
		return telefonesFavoritos;
	}

	public void setTelefonesFavoritos(String telefonesFavoritos) {
		this.telefonesFavoritos = telefonesFavoritos;
	}

}