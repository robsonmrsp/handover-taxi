package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;

/**
 * generated: 04/11/2016 11:29:27
 **/
public class FilterPunicao implements Serializable {
	private static final long serialVersionUID = 1L;

	private String motivo;

	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	@JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
	private LocalDateTime dataInicio;

	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	@JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
	private LocalDateTime dataFim;

	private Integer quantidadetempo;

	private String statusPunicao;

	private Integer motorista;

	public FilterPunicao() {

	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDateTime dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDateTime getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDateTime dataFim) {
		this.dataFim = dataFim;
	}

	public Integer getQuantidadetempo() {
		return quantidadetempo;
	}

	public void setQuantidadetempo(Integer quantidadetempo) {
		this.quantidadetempo = quantidadetempo;
	}

	public String getStatusPunicao() {
		return statusPunicao;
	}

	public void setStatusPunicao(String statusPunicao) {
		this.statusPunicao = statusPunicao;
	}

	public Integer getMotorista() {
		return motorista;
	}

	public void setMotorista(Integer motorista) {
		this.motorista = motorista;
	}

}