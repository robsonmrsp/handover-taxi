package br.com.invista.model;

public interface TipoPagamento {

	public static final int DINHEIRO = 1;
	
	public static final int CREDITO = 2;
	
	public static final int DEBITO = 3;
	
	public static final int VOUCHER = 4;
	
	public static final int ETICKET = 5;
	
}
