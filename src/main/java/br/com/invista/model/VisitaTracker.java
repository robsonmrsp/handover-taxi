package br.com.invista.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.model.AbstractTimestampEntity;

/**
 * generated: 04/11/2016 11:29:27
 **/
@Entity
@Audited
@Table(name = "VISITA_TRACKER")
public class VisitaTracker extends AbstractTimestampEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "LATITUDE")
	private Double latitude;

	@Column(name = "LONGITUDE")
	private Double longitude;

	@Column(name = "TIMESTAMP" , columnDefinition="BIGINT")
	private Long timestamp;

	@Column(name = "SPEED")
	private Double speed;

	@Column(name = "ACCURACY")
	private Double accuracy;

	@Column(name = "DIRECTION")
	private Double direction;

	@Column(name = "ALTITUDE")
	private Double altitude;

	@Column(name = "BATERRY_LEVEL")
	private Boolean baterryLevel;

	@Column(name = "GPS_ENABLED")
	private Boolean gpsEnabled;

	@Column(name = "WIFI_ENABLED")
	private Boolean wifiEnabled;

	@Column(name = "MOBILE_ENABLED")
	private Boolean mobileEnabled;

	@OneToMany(mappedBy = "visitaTrackerSaida")
	private List<Evento> eventosSaida;

	public boolean addEventoSaida(Evento arg0) {

		return getEventosSaida().add(arg0);
	}

	@OneToMany(mappedBy = "visitaTrackerChegada")
	private List<Evento> eventosChegada;

	public VisitaTracker() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Double getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(Double accuracy) {
		this.accuracy = accuracy;
	}

	public Double getDirection() {
		return direction;
	}

	public void setDirection(Double direction) {
		this.direction = direction;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public Boolean getBaterryLevel() {
		return baterryLevel;
	}

	public void setBaterryLevel(Boolean baterryLevel) {
		this.baterryLevel = baterryLevel;
	}

	public Boolean getGpsEnabled() {
		return gpsEnabled;
	}

	public void setGpsEnabled(Boolean gpsEnabled) {
		this.gpsEnabled = gpsEnabled;
	}

	public Boolean getWifiEnabled() {
		return wifiEnabled;
	}

	public void setWifiEnabled(Boolean wifiEnabled) {
		this.wifiEnabled = wifiEnabled;
	}

	public Boolean getMobileEnabled() {
		return mobileEnabled;
	}

	public void setMobileEnabled(Boolean mobileEnabled) {
		this.mobileEnabled = mobileEnabled;
	}

	public List<Evento> getEventosSaida() {
		if (eventosSaida == null) {
			setEventosSaida(new ArrayList<Evento>());
		}
		return eventosSaida;
	}

	public void setEventosSaida(List<Evento> eventosSaida) {
		this.eventosSaida = eventosSaida;
	}

	public List<Evento> getEventosChegada() {
		return eventosChegada;
	}

	public void setEventosChegada(List<Evento> eventosChegada) {
		this.eventosChegada = eventosChegada;
	}

}
