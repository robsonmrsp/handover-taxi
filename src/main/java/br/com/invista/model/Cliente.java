package br.com.invista.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.invista.core.model.AbstractTimestampEntity;
import br.com.invista.core.serialization.CustomLocalDateSerializer;

/**
 * generated: 16/10/2016 15:27:07
 **/
@Entity
@Audited
@Table(name = "CLIENTE")
public class Cliente extends AbstractTimestampEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "NOME", length = 200)
	private String nome;

	@Column(name = "EMAIL", length = 100)
	private String email;

	@Column(name = "USERNAME", length = 100)
	private String username;

	@Column(name = "SENHA", length = 100)
	private String senha;


	@Column(name = "TELEFONE", length = 100)
	private String telefone;

	@Column(name = "TIPO", length = 20)
	@Enumerated(EnumType.STRING)
	private TipoCliente tipo;

	@Column(name = "INATIVO")
	private Boolean inativo;

	@Column(name = "USUARIO_CADASTRO")
	private Integer usuarioCadastro;

	@Column(name = "DATA_CADASTRO")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomLocalDateSerializer.class)
	private LocalDate dataCadastro;

	@Column(name = "MATRICULA")
	private Integer matricula;

	@Column(name = "LIMITE_MENSAL")
	private Double limiteMensal;

	@Column(name = "CNPJ_CPF", length = 18)
	private String cnpjCpf;

	@Column(name = "CNPJ", length = 18)
	private String cnpj;

	@Column(name = "CPF", length = 14)
	private String cpf;

	@Column(name = "AUTORIZA_ETICKET")
	private Boolean autorizaEticket;

	@Column(name = "ID_FACEBOOK", length = 100)
	private String idFacebook;
	
	@Column(name = "EMAIL_FACEBOOK", length = 100)
	private String emailFacebook;
	
	@Column(name = "OBSERVACAO", length = 1000)
	private String observacao;

	@OneToMany(mappedBy = "cliente")
	private List<EnderecoFavorito> enderecosFavoritos;

	@OneToMany(mappedBy = "cliente")
	private List<TelefoneFavorito> telefonesFavoritos;

	@OneToMany(mappedBy = "cliente")
	private List<MotoristaBloqueado> motoristaBloqueados;

	@ManyToOne
	@JoinColumn(name = "ID_EMPRESA")
	private Empresa empresa;

	@ManyToOne
	@JoinColumn(name = "ID_CENTROCUSTO")
	private CentroCusto centroCusto;

	public Cliente() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Boolean getInativo() {
		return inativo;
	}

	public void setInativo(Boolean inativo) {
		this.inativo = inativo;
	}

	public Integer getUsuarioCadastro() {
		return usuarioCadastro;
	}

	public void setUsuarioCadastro(Integer usuarioCadastro) {
		this.usuarioCadastro = usuarioCadastro;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public Double getLimiteMensal() {
		return limiteMensal;
	}

	public void setLimiteMensal(Double limiteMensal) {
		this.limiteMensal = limiteMensal;
	}

	public String getCnpjCpf() {
		return cnpjCpf;
	}

	public void setCnpjCpf(String cnpjCpf) {
		this.cnpjCpf = cnpjCpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Boolean getAutorizaEticket() {
		return autorizaEticket;
	}

	public void setAutorizaEticket(Boolean autorizaEticket) {
		this.autorizaEticket = autorizaEticket;
	}

	public String getIdFacebook() {
		return idFacebook;
	}

	public void setIdFacebook(String idFacebook) {
		this.idFacebook = idFacebook;
	}

	public String getEmailFacebook() {
		return emailFacebook;
	}

	public void setEmailFacebook(String emailFacebook) {
		this.emailFacebook = emailFacebook;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setEnderecosFavoritos(List<EnderecoFavorito> enderecosFavoritos) {
		this.enderecosFavoritos = enderecosFavoritos;
	}

	public List<EnderecoFavorito> getEnderecosFavoritos() {
		if (this.enderecosFavoritos == null) {
			setEnderecosFavoritos(new ArrayList<EnderecoFavorito>());
		}
		return this.enderecosFavoritos;
	}

	public boolean addEnderecosFavoritos(EnderecoFavorito enderecoFavorito) {
		enderecoFavorito.setCliente(this);
		return getEnderecosFavoritos().add(enderecoFavorito);
	}

	public boolean removeEnderecosFavoritos(EnderecoFavorito enderecoFavorito) {
		enderecoFavorito.setCliente(null);
		return getEnderecosFavoritos().remove(enderecoFavorito);
	}

	public void setTelefonesFavoritos(List<TelefoneFavorito> telefonesFavoritos) {
		this.telefonesFavoritos = telefonesFavoritos;
	}

	public List<TelefoneFavorito> getTelefonesFavoritos() {
		if (this.telefonesFavoritos == null) {
			setTelefonesFavoritos(new ArrayList<TelefoneFavorito>());
		}
		return this.telefonesFavoritos;
	}

	public boolean addTelefoneFavorito(TelefoneFavorito telefoneFavorito) {
		telefoneFavorito.setCliente(this);
		return getTelefonesFavoritos().add(telefoneFavorito);
	}

	public boolean removeTelefoneFavorito(TelefoneFavorito telefoneFavorito) {
		telefoneFavorito.setCliente(null);
		return getTelefonesFavoritos().remove(telefoneFavorito);
	}

	public void setMotoristaBloqueados(List<MotoristaBloqueado> motoristaBloqueados) {
		this.motoristaBloqueados = motoristaBloqueados;
	}

	public List<MotoristaBloqueado> getMotoristaBloqueados() {
		if (this.motoristaBloqueados == null) {
			setMotoristaBloqueados(new ArrayList<MotoristaBloqueado>());
		}
		return this.motoristaBloqueados;
	}

	public boolean addMotoristaBloqueados(MotoristaBloqueado motoristaBloqueado) {
		motoristaBloqueado.setCliente(this);
		return getMotoristaBloqueados().add(motoristaBloqueado);
	}

	public boolean removeMotoristaBloqueados(MotoristaBloqueado motoristaBloqueado) {
		motoristaBloqueado.setCliente(null);
		return getMotoristaBloqueados().remove(motoristaBloqueado);
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public CentroCusto getCentroCusto() {
		return centroCusto;
	}

	public void setCentroCusto(CentroCusto centroCusto) {
		this.centroCusto = centroCusto;
	}

	public TipoCliente getTipo() {
		return tipo;
	}

	public void setTipo(TipoCliente tipo) {
		this.tipo = tipo;
	}


	public String getTelefone() {
		return telefone;
	}


	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

}
