package br.com.invista.model;

public enum TipoCliente {
	CADASTRO_CENTRAL, APP, FUNCIONARIO, ESTABELECIMENTO, NONE;

	public static TipoCliente byName(String tipo) {
		TipoCliente[] values = values();
		for (TipoCliente tipoCliente : values) {
			if (tipoCliente.name().equals(tipo)) {
				return tipoCliente;
			}
		}
		return null;
	}
}
