package br.com.invista.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.com.invista.core.model.AbstractTimestampEntity;

/**
 * generated: 04/11/2016 11:29:26
 **/
@Entity
@Audited
@Table(name = "ENDERECO_CORRIDA")
public class EnderecoCorrida extends AbstractTimestampEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "ENDERECO", length = 200)
	private String endereco;

	@Column(name = "LOGRADOURO", length = 200)
	private String logradouro;

	@Column(name = "NUMERO", length = 10)
	private String numero;

	@Column(name = "COMPLEMENTO", length = 100)
	private String complemento;

	@Column(name = "CONTATO", length = 100)
	private String contato;

	@Column(name = "TAREFA", length = 100)
	private String tarefa;

	@Column(name = "BAIRRO", length = 100)
	private String bairro;

	@Column(name = "UF", length = 2)
	private String uf;

	@Column(name = "CIDADE", length = 50)
	private String cidade;

	@Column(name = "CEP", length = 9)
	private String cep;

	@Column(name = "REFERENCIA", length = 200)
	private String referencia;

	@Column(name = "DESCRICAO", length = 400)
	private String descricao;

	@Column(name = "ORDEM")
	private Integer ordem;

	@Column(name = "LATITUDE")
	private Double latitude;

	@Column(name = "LONGITUDE")
	private Double longitude;

	@Column(name = "ENDERECO_FORMATADO", length = 100)
	private String enderecoFormatado;

	@Column(name = "ORIGEM")
	private Boolean origem;

	@OneToMany(mappedBy = "enderecoCorrida")
	private List<Evento> eventos;

	@ManyToOne
	@JoinColumn(name = "ID_CORRIDA")
	private Corrida corrida;

	public EnderecoCorrida() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getEnderecoFormatado() {
		return enderecoFormatado;
	}

	public void setEnderecoFormatado(String enderecoFormatado) {
		this.enderecoFormatado = enderecoFormatado;
	}

	public Boolean getOrigem() {
		return origem;
	}

	public void setOrigem(Boolean origem) {
		this.origem = origem;
	}

	public void setEventos(List<Evento> eventos) {
		this.eventos = eventos;
	}

	public List<Evento> getEventos() {
		if (this.eventos == null) {
			setEventos(new ArrayList<Evento>());
		}
		return this.eventos;
	}

	public boolean addEventos(Evento evento) {
		evento.setEnderecoCorrida(this);
		return getEventos().add(evento);
	}

	public boolean removeEventos(Evento evento) {
		evento.setEnderecoCorrida(null);
		return getEventos().remove(evento);
	}

	public Corrida getCorrida() {
		return corrida;
	}

	public void setCorrida(Corrida corrida) {
		this.corrida = corrida;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getTarefa() {
		return tarefa;
	}

	public void setTarefa(String tarefa) {
		this.tarefa = tarefa;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

}
