package br.com.invista.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 * generated: 01/01/2017 11:29:26
 **/
@Entity
@Audited
@Table(name = "INFORMACAO_TARIFA_ADICIONAL")

public class InformacaoTarifaAdicional {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String descricao;

	private Double valorTotal;

	private Integer quantidade;

	@ManyToOne
	@JoinColumn(name = "ID_CORRIDA")
	private Corrida corrida;

	@ManyToOne
	@JoinColumn(name = "ID_TARIFA_ADICIONAL")
	private TarifaAdicional tarifaAdicional;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public TarifaAdicional getTarifaAdicional() {
		return tarifaAdicional;
	}

	public void setTarifaAdicional(TarifaAdicional tarifaAdicional) {
		this.tarifaAdicional = tarifaAdicional;
	}

	public Corrida getCorrida() {
		return corrida;
	}

	public void setCorrida(Corrida corrida) {
		this.corrida = corrida;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

}
