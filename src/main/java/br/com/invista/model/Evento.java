package br.com.invista.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.model.AbstractTimestampEntity;

/**
 * generated: 04/11/2016 11:29:26
 **/
@Entity
@Audited
@Table(name = "EVENTO")
public class Evento extends AbstractTimestampEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "DESCRICAO")
	private String descricao;

	@Column(name = "DATA_INICIO" , columnDefinition="BIGINT")
	private Long dataInicio;

	@Column(name = "DATA_FIM" , columnDefinition="BIGINT")
	private Long dataFim;

	@Column(name = "DESCRICAO_CURTA")
	private String descricaoCurta;

	@ManyToOne
	@JoinColumn(name = "ID_ENDERECOCORRIDA")
	private EnderecoCorrida enderecoCorrida;

	@ManyToOne
	@JoinColumn(name = "ID_MOTORISTA")
	private Motorista motorista;

	@ManyToOne
	@JoinColumn(name = "ID_CHEGADA")
	private VisitaTracker visitaTrackerChegada;

	@ManyToOne
	@JoinColumn(name = "ID_SAIDA")
	private VisitaTracker visitaTrackerSaida;

	public Evento() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Long dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Long getDataFim() {
		return dataFim;
	}

	public void setDataFim(Long dataFim) {
		this.dataFim = dataFim;
	}

	public String getDescricaoCurta() {
		return descricaoCurta;
	}

	public void setDescricaoCurta(String descricaoCurta) {
		this.descricaoCurta = descricaoCurta;
	}

	public EnderecoCorrida getEnderecoCorrida() {
		return enderecoCorrida;
	}

	public void setEnderecoCorrida(EnderecoCorrida enderecoCorrida) {
		this.enderecoCorrida = enderecoCorrida;
	}

	public Motorista getMotorista() {
		return motorista;
	}

	public void setMotorista(Motorista motorista) {
		this.motorista = motorista;
	}

	public VisitaTracker getVisitaTrackerSaida() {
		return visitaTrackerSaida;
	}

	public void setVisitaTrackerSaida(VisitaTracker visitaTrackerSaida) {
		this.visitaTrackerSaida = visitaTrackerSaida;
	}


	public VisitaTracker getVisitaTrackerChegada() {
		return visitaTrackerChegada;
	}

	public void setVisitaTrackerChegada(VisitaTracker visitaTrackerChegada) {
		this.visitaTrackerChegada = visitaTrackerChegada;
	}

}
