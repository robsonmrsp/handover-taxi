package br.com.invista.model;

public interface SituacaoMotoristaEmFila {
	public static final String PENDENTE = "PENDENTE";
	public static final String ENVIADA = "ENVIADA";
}
