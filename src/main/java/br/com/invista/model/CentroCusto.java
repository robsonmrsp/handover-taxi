package br.com.invista.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.model.AbstractTimestampEntity;

/**
 * generated: 16/10/2016 15:27:07
 **/
@Entity
@Audited
@Table(name = "CENTRO_CUSTO")
public class CentroCusto extends AbstractTimestampEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "DESCRICAO", length = 200)
	private String descricao;

	@Column(name = "VALOR_LIMITE")
	private Double valorLimite;

	@Column(name = "VALOR_AINDA_DISPONIVEL")
	private Double valorAindaDisponivel;

	@Column(name = "OBSERVACAO", length = 1000)
	private String observacao;

	@Column(name = "INATIVO")
	private Boolean inativo;

	@OneToMany(mappedBy = "centroCusto")
	private List<Cliente> clientes;

	@ManyToOne
	@JoinColumn(name = "ID_EMPRESA")
	private Empresa empresa;

	public CentroCusto() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValorLimite() {
		return valorLimite;
	}

	public void setValorLimite(Double valorLimite) {
		this.valorLimite = valorLimite;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Boolean getInativo() {
		return inativo;
	}

	public void setInativo(Boolean inativo) {
		this.inativo = inativo;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public List<Cliente> getClientes() {
		if (this.clientes == null) {
			setClientes(new ArrayList<Cliente>());
		}
		return this.clientes;
	}

	public boolean addClientes(Cliente cliente) {
		cliente.setCentroCusto(this);
		return getClientes().add(cliente);
	}

	public boolean removeClientes(Cliente cliente) {
		cliente.setCentroCusto(null);
		return getClientes().remove(cliente);
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Double getValorAindaDisponivel() {
		return valorAindaDisponivel;
	}

	public void setValorAindaDisponivel(Double valorAindaDisponivel) {
		this.valorAindaDisponivel = valorAindaDisponivel;
	}

}
