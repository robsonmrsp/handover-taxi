package br.com.invista.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import br.com.invista.core.model.AbstractTimestampEntity;
/**
* generated: 16/10/2016 15:27:08 
**/
@Entity
@Audited
@Table(name = "TARIFA_ADICIONAL")
public class TarifaAdicional extends AbstractTimestampEntity{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Integer id;
		
	@Column(name = "DESCRICAO" , length=200)
	private String descricao;		
		
	@Column(name = "VALOR")
	private Double valor;  			
		
	public  TarifaAdicional() {
		
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
}
