package br.com.invista.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDateTime;

import br.com.invista.core.model.AbstractTimestampEntity;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * generated: 04/11/2016 11:29:26
 **/
@Entity
@Audited
@Table(name = "CORRIDA")
public class Corrida extends AbstractTimestampEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "DATA_CORRIDA")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	private LocalDateTime dataCorrida;

	@Column(name = "DATA_AGENDAMENTO")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomLocalDateTimeSerializer.class)
	private LocalDateTime dataAgendamento;

	@Column(name = "TIPO_PAGAMENTO")
	private Integer tipoPagamento;

	@Column(name = "VALOR")
	private Double valor;

	@Column(name = "OBSERVACAO", length = 1000)
	private String observacao;

	@Column(name = "OBSERVACAO_GERAL", length = 1000)
	private String observacaoGeral;

	@Column(name = "STATUS_CORRIDA", length = 1)
	private String statusCorrida;

	@Column(name = "SEQ_EM_ATENDIMENTO")
	private Integer seqEmAtendimento;

	@Column(name = "MOTO_GRANDE")
	private Boolean motoGrande;

	@Column(name = "BAU")
	private Boolean bau;

	@Column(name = "PADRONIZADA")
	private Boolean padronizada;
	
	@Column(name = "FILA_CRIADA")
	private Boolean filaCriada;

	@OneToMany(mappedBy = "corrida")
	@Cascade(CascadeType.ALL)
	private List<EnderecoCorrida> enderecoCorridas;

	@OneToMany(mappedBy = "corrida")
	@OrderBy("sequencia ASC")
	private List<DistanciaMotoCorrida> distanciaMotoCorridas;

	@OneToMany(mappedBy = "corrida")
	@Cascade(CascadeType.ALL)
	private List<InformacaoTarifaAdicional> informacaoTarifas;

	@ManyToOne
	@JoinColumn(name = "ID_ATENDENTE")
	private Atendente atendente;

	@ManyToOne
	@JoinColumn(name = "ID_MOTORISTA")
	private Motorista motorista;

	@ManyToOne
	@JoinColumn(name = "ID_CLIENTE")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name = "ID_EMPRESA")
	private Empresa empresa;

	@Column(name = "TELEFONE", length = 15)
	private String telefone;

	public Corrida() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getDataCorrida() {
		return dataCorrida;
	}

	public void setDataCorrida(LocalDateTime dataCorrida) {
		this.dataCorrida = dataCorrida;
	}

	public Integer getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(Integer tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getStatusCorrida() {
		return statusCorrida;
	}

	public void setStatusCorrida(String statusCorrida) {
		this.statusCorrida = statusCorrida;
	}

	public Integer getSeqEmAtendimento() {
		return seqEmAtendimento;
	}

	public void setSeqEmAtendimento(Integer seqEmAtendimento) {
		this.seqEmAtendimento = seqEmAtendimento;
	}

	public Boolean getMotoGrande() {
		return motoGrande;
	}

	public void setMotoGrande(Boolean motoGrande) {
		this.motoGrande = motoGrande;
	}

	public Boolean getBau() {
		return bau;
	}

	public void setBau(Boolean bau) {
		this.bau = bau;
	}

	public void setEnderecoCorridas(List<EnderecoCorrida> enderecoCorridas) {
		this.enderecoCorridas = enderecoCorridas;
	}

	public List<EnderecoCorrida> getEnderecoCorridas() {
		if (this.enderecoCorridas == null) {
			setEnderecoCorridas(new ArrayList<EnderecoCorrida>());
		}
		return this.enderecoCorridas;
	}

	public boolean addEnderecoCorridas(EnderecoCorrida enderecoCorrida) {
		enderecoCorrida.setCorrida(this);
		return getEnderecoCorridas().add(enderecoCorrida);
	}

	public boolean removeEnderecoCorridas(EnderecoCorrida enderecoCorrida) {
		enderecoCorrida.setCorrida(null);
		return getEnderecoCorridas().remove(enderecoCorrida);
	}

	public void setDistanciaMotoCorridas(List<DistanciaMotoCorrida> distanciaMotoCorridas) {
		this.distanciaMotoCorridas = distanciaMotoCorridas;
	}

	public List<DistanciaMotoCorrida> getDistanciaMotoCorridas() {
		if (this.distanciaMotoCorridas == null) {
			setDistanciaMotoCorridas(new ArrayList<DistanciaMotoCorrida>());
		}
		return this.distanciaMotoCorridas;
	}

	public boolean addDistanciaMotoCorridas(DistanciaMotoCorrida distanciaMotoCorrida) {
		distanciaMotoCorrida.setCorrida(this);
		return getDistanciaMotoCorridas().add(distanciaMotoCorrida);
	}

	public boolean removeDistanciaMotoCorridas(DistanciaMotoCorrida distanciaMotoCorrida) {
		distanciaMotoCorrida.setCorrida(null);
		return getDistanciaMotoCorridas().remove(distanciaMotoCorrida);
	}

	public List<InformacaoTarifaAdicional> getInformacaoTarifaAdicionals() {
		if (this.informacaoTarifas == null) {
			setInformacaoTarifas(new ArrayList<InformacaoTarifaAdicional>());
		}
		return this.informacaoTarifas;
	}

	public boolean addInformacaoTarifaAdicionals(InformacaoTarifaAdicional informacaoTarifaAdicional) {
		informacaoTarifaAdicional.setCorrida(this);
		return getInformacaoTarifaAdicionals().add(informacaoTarifaAdicional);
	}

	public boolean removeInformacaoTarifaAdicionals(InformacaoTarifaAdicional informacaoTarifaAdicional) {
		informacaoTarifaAdicional.setCorrida(null);
		return getInformacaoTarifaAdicionals().remove(informacaoTarifaAdicional);
	}

	public Atendente getAtendente() {
		return atendente;
	}

	public void setAtendente(Atendente atendente) {
		this.atendente = atendente;
	}

	public Motorista getMotorista() {
		return motorista;
	}

	public void setMotorista(Motorista motorista) {
		this.motorista = motorista;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Corrida other = (Corrida) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getObservacaoGeral() {
		return observacaoGeral;
	}

	public void setObservacaoGeral(String observacaoGeral) {
		this.observacaoGeral = observacaoGeral;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public List<InformacaoTarifaAdicional> getInformacaoTarifas() {
		return informacaoTarifas;
	}

	public void setInformacaoTarifas(List<InformacaoTarifaAdicional> informacaoTarifas) {
		this.informacaoTarifas = informacaoTarifas;
	}

	public Boolean getFilaCriada() {
		return filaCriada;
	}

	public void setFilaCriada(Boolean filaCriada) {
		this.filaCriada = filaCriada;
	}

	public Boolean getPadronizada() {
		return padronizada;
	}

	public void setPadronizada(Boolean padronizada) {
		this.padronizada = padronizada;
	}

	public LocalDateTime getDataAgendamento() {
		return dataAgendamento;
	}

	public void setDataAgendamento(LocalDateTime dataAgendamento) {
		this.dataAgendamento = dataAgendamento;
	}
	
}
