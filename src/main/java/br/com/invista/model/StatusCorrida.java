package br.com.invista.model;

public interface StatusCorrida {

	/**
	 * (A) AGENDADA (CORRIDA AGENDADA)
	 */
	public static final String AGENDADA = "A";
	/**
	 * (C) A CAMINHO (Um motorista pegou mas não fez nada)
	 */
	public static final String A_CAMINHO = "C";
	/**
	 * (D) DISPONIVEL (Nenhum motorista pegou a atividade)
	 */
	public static final String DISPONIVEL = "D";
	/**
	 * (E) EM ANDAMENTO (O motorista começou as atividades)
	 */
	public static final String EM_ANDAMENTO = "E";
	/**
	 * (F) FINALIZADA (O motorista concluiu a atividade)
	 */
	public static final String FINALIZADA = "F";
	/**
	 * (K) CANCELADA (Corrida cancelada)
	 */
	public static final String CANCELADA = "K";
	/**
	 * (P) PENDENTE (Nenhum motorista pegou)
	 */
	public static final String PENDENTE = "P";
	/**
	 * (R) REENVIADA (Corrida reenviada)
	 */
	public static final String REENVIADA = "R";

}
