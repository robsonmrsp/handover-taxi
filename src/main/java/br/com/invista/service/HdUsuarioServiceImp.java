package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.HdUsuario;
import br.com.invista.persistence.DaoHdUsuario;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@Transactional
public class HdUsuarioServiceImp implements HdUsuarioService {

	private static final Logger LOGGER = Logger.getLogger(HdUsuarioServiceImp.class);
	
	@Inject
	DaoHdUsuario daoHdUsuario;

	@Override
	public HdUsuario get(Integer id) {
		return daoHdUsuario.find(id);
	}
	

	@Override
	public Pager<HdUsuario> all(PaginationParams paginationParams) {
		Pagination<HdUsuario> pagination = daoHdUsuario.getAll(paginationParams);
		return new Pager<HdUsuario>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<HdUsuario> filter(PaginationParams paginationParams) {
		List<HdUsuario> list = daoHdUsuario.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<HdUsuario> all() {
		return daoHdUsuario.getAll();
	}

	@Override
	public List<HdUsuario> search(String description) {
		return new ArrayList<HdUsuario>();
	}
	
	public List<HdUsuario> last(LocalDateTime lastSyncDate){
		return daoHdUsuario.last(lastSyncDate);
	}
			
	@Override
	public HdUsuario save(HdUsuario entity) {
		return daoHdUsuario.save(entity);
	}

	@Override
	public HdUsuario update(HdUsuario entity) {
		return daoHdUsuario.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoHdUsuario.delete(id);
	}


}
