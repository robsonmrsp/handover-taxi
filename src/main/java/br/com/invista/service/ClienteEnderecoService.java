package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.EnderecoFavorito;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:07
**/
public interface ClienteEnderecoService {

	EnderecoFavorito get(Integer id);

	List<EnderecoFavorito> all();
	
	Pager<EnderecoFavorito> all(PaginationParams paginationParams);

	List<EnderecoFavorito> filter(PaginationParams paginationParams);
	
	List<EnderecoFavorito> search(String searchText);

	EnderecoFavorito save(EnderecoFavorito entity);

	EnderecoFavorito update(EnderecoFavorito entity);
    List<EnderecoFavorito> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
