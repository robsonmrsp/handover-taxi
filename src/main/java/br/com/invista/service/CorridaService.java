package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Corrida;
import br.com.invista.rs.ConsumidorCorrida;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 04/11/2016 11:29:26
**/
public interface CorridaService {

	Corrida get(Integer id);

	List<Corrida> all();
	
	Pager<Corrida> all(PaginationParams paginationParams);

	List<Corrida> filter(PaginationParams paginationParams);
	
	List<Corrida> search(String searchText);

	Corrida save(Corrida entity);

	Corrida update(Corrida entity);
    List<Corrida> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);

	ConsumidorCorrida consumidorPorTelefone(String telefone);
	
	List<Corrida> historicoCorridasCliente(int idCliente, boolean empresa);
	
	List<Corrida> corridasAgendadasCliente(int idCliente, boolean empresa);
	
	List<Corrida> corridasPendentes();
}
