package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.FaixaVoucher;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface FaixaVoucherService {

	FaixaVoucher get(Integer id);

	List<FaixaVoucher> all();
	
	Pager<FaixaVoucher> all(PaginationParams paginationParams);

	List<FaixaVoucher> filter(PaginationParams paginationParams);
	
	List<FaixaVoucher> search(String searchText);

	FaixaVoucher save(FaixaVoucher entity);
	
	Boolean isValid(FaixaVoucher voucher);

	FaixaVoucher update(FaixaVoucher entity);
    List<FaixaVoucher> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
