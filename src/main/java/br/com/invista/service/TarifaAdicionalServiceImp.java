package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;

import br.com.invista.model.TarifaAdicional;
import br.com.invista.persistence.DaoTarifaAdicional;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
 * generated: 16/10/2016 15:27:08
 **/

@Named
@Transactional
public class TarifaAdicionalServiceImp implements TarifaAdicionalService {

	private static final Logger LOGGER = Logger.getLogger(TarifaAdicionalServiceImp.class);

	@Inject
	DaoTarifaAdicional daoTarifaAdicional;

	@Override
	public TarifaAdicional get(Integer id) {
		return daoTarifaAdicional.find(id);
	}

	@Override
	public Pager<TarifaAdicional> all(PaginationParams paginationParams) {
		Pagination<TarifaAdicional> pagination = daoTarifaAdicional.getAll(paginationParams);
		return new Pager<TarifaAdicional>(pagination.getResults(), 0, pagination.getTotalRecords());
	}

	@Override
	public List<TarifaAdicional> filter(PaginationParams paginationParams) {
		List<TarifaAdicional> list = daoTarifaAdicional.filter(paginationParams);
		return list;
	}

	@Override
	public List<TarifaAdicional> all() {
		return daoTarifaAdicional.getAll();
	}

	@Override
	public List<TarifaAdicional> search(String description) {
		return new ArrayList<TarifaAdicional>();
	}

	public List<TarifaAdicional> last(LocalDateTime lastSyncDate) {
		return daoTarifaAdicional.last(lastSyncDate);
	}

	@Override
	public TarifaAdicional save(TarifaAdicional entity) {
		return daoTarifaAdicional.save(entity);
	}

	@Override
	public TarifaAdicional update(TarifaAdicional entity) {
		return daoTarifaAdicional.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoTarifaAdicional.delete(id);
	}

}
