package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Plantao;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface PlantaoService {

	Plantao get(Integer id);

	List<Plantao> all();
	
	Pager<Plantao> all(PaginationParams paginationParams);

	List<Plantao> filter(PaginationParams paginationParams);
	
	List<Plantao> search(String searchText);

	Plantao save(Plantao entity);

	Plantao update(Plantao entity);
    List<Plantao> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
