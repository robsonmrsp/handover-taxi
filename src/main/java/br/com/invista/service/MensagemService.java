package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Mensagem;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface MensagemService {

	Mensagem get(Integer id);

	List<Mensagem> all();
	
	Pager<Mensagem> all(PaginationParams paginationParams);

	List<Mensagem> filter(PaginationParams paginationParams);
	
	List<Mensagem> search(String searchText);

	Mensagem save(Mensagem entity);

	Mensagem update(Mensagem entity);
    List<Mensagem> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
