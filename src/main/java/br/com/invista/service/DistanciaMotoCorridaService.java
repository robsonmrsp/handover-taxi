package br.com.invista.service;

import java.util.List;
import java.util.Map;

import org.joda.time.LocalDateTime;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.model.Corrida;
import br.com.invista.model.DistanciaMotoCorrida;
import br.com.invista.model.filter.FilterDistanciaMotoCorrida;

/**
 * generated: 04/11/2016 11:29:26
 **/
public interface DistanciaMotoCorridaService {

	DistanciaMotoCorrida get(Integer id);

	List<DistanciaMotoCorrida> all();

	Pager<DistanciaMotoCorrida> all(PaginationParams paginationParams);

	List<DistanciaMotoCorrida> filter(PaginationParams paginationParams);

	List<DistanciaMotoCorrida> filter(FilterDistanciaMotoCorrida filter);

	Map<Corrida, List<DistanciaMotoCorrida>> getMap(FilterDistanciaMotoCorrida filter);

	List<DistanciaMotoCorrida> search(String searchText);

	DistanciaMotoCorrida save(DistanciaMotoCorrida entity);

	DistanciaMotoCorrida update(DistanciaMotoCorrida entity);

	List<DistanciaMotoCorrida> last(LocalDateTime lastSyncDate);

	Boolean delete(Integer id);
	
	void deleteDistanciaMotoCorridaByCorrida(Corrida corrida);

}
