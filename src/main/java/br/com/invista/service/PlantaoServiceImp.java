package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.Plantao;
import br.com.invista.persistence.DaoPlantao;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@Transactional
public class PlantaoServiceImp implements PlantaoService {

	private static final Logger LOGGER = Logger.getLogger(PlantaoServiceImp.class);
	
	@Inject
	DaoPlantao daoPlantao;

	@Override
	public Plantao get(Integer id) {
		return daoPlantao.find(id);
	}
	

	@Override
	public Pager<Plantao> all(PaginationParams paginationParams) {
		Pagination<Plantao> pagination = daoPlantao.getAll(paginationParams);
		return new Pager<Plantao>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<Plantao> filter(PaginationParams paginationParams) {
		List<Plantao> list = daoPlantao.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<Plantao> all() {
		return daoPlantao.getAll();
	}

	@Override
	public List<Plantao> search(String description) {
		return new ArrayList<Plantao>();
	}
	
	public List<Plantao> last(LocalDateTime lastSyncDate){
		return daoPlantao.last(lastSyncDate);
	}
			
	@Override
	public Plantao save(Plantao entity) {
		return daoPlantao.save(entity);
	}

	@Override
	public Plantao update(Plantao entity) {
		return daoPlantao.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoPlantao.delete(id);
	}


}
