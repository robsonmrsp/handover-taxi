package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.VisitaTracker;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 04/11/2016 11:29:27
**/
public interface VisitaTrackerService {

	VisitaTracker get(Integer id);

	List<VisitaTracker> all();
	
	Pager<VisitaTracker> all(PaginationParams paginationParams);

	List<VisitaTracker> filter(PaginationParams paginationParams);
	
	List<VisitaTracker> search(String searchText);

	VisitaTracker save(VisitaTracker entity);

	VisitaTracker update(VisitaTracker entity);
    List<VisitaTracker> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
