package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.MotivoPunicao;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 28/11/2016 23:13:05
**/
public interface MotivoPunicaoService {

	MotivoPunicao get(Integer id);

	List<MotivoPunicao> all();
	
	Pager<MotivoPunicao> all(PaginationParams paginationParams);

	List<MotivoPunicao> filter(PaginationParams paginationParams);
	
	List<MotivoPunicao> search(String searchText);

	MotivoPunicao save(MotivoPunicao entity);

	MotivoPunicao update(MotivoPunicao entity);
    List<MotivoPunicao> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
