package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;

import br.com.invista.model.CentroCusto;
import br.com.invista.model.Cliente;
import br.com.invista.persistence.DaoCentroCusto;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
 * generated: 16/10/2016 15:27:07
 **/

@Named
@Transactional
public class CentroCustoServiceImp implements CentroCustoService {

	private static final Logger LOGGER = Logger.getLogger(CentroCustoServiceImp.class);

	@Inject
	DaoCentroCusto daoCentroCusto;

	@Override
	public CentroCusto get(Integer id) {
		return daoCentroCusto.find(id);
	}

	@Override
	public Pager<CentroCusto> all(PaginationParams paginationParams) {
		Pagination<CentroCusto> pagination = daoCentroCusto.getAll(paginationParams);
		return new Pager<CentroCusto>(pagination.getResults(), 0, pagination.getTotalRecords());
	}

	@Override
	public List<CentroCusto> filter(PaginationParams paginationParams) {
		List<CentroCusto> list = daoCentroCusto.filter(paginationParams);
		return list;
	}

	@Override
	public List<CentroCusto> all() {
		return daoCentroCusto.getAll();
	}

	@Override
	public List<CentroCusto> search(String description) {

		return new ArrayList<CentroCusto>();
	}

	public List<CentroCusto> last(LocalDateTime lastSyncDate) {
		return daoCentroCusto.last(lastSyncDate);
	}

	@Override
	public CentroCusto save(CentroCusto entity) {
		entity.setValorAindaDisponivel(entity.getValorLimite());
		return daoCentroCusto.save(entity);
	}

	@Override
	public CentroCusto update(CentroCusto entity) {
		return daoCentroCusto.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoCentroCusto.delete(id);
	}

	@Override
	public Boolean semLimiteDisponivelParaValor(Integer idCentroCusto, Cliente cliente) {
		return !possuiLimiteDisponivelParaValor(idCentroCusto, cliente);
	}

	@Override
	public Boolean possuiLimiteDisponivelParaValor(Integer idCentroCusto, Cliente cliente) {

		CentroCusto centroCusto = get(idCentroCusto);
		Double valor = cliente.getLimiteMensal();
		Double totalUtilizado = daoCentroCusto.totalUtilizado(idCentroCusto, cliente.getId());
		Double valorDisponivel = centroCusto.getValorLimite() - totalUtilizado;
		if(valor == null){
			valor = 0.00;
		}
		return valorDisponivel >= valor;
	}
}
