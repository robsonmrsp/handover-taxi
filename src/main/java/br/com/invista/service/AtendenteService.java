package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Atendente;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:07
**/
public interface AtendenteService {

	Atendente get(Integer id);

	List<Atendente> all();
	
	Pager<Atendente> all(PaginationParams paginationParams);

	List<Atendente> filter(PaginationParams paginationParams);
	
	List<Atendente> search(String searchText);

	Atendente save(Atendente entity);

	Atendente update(Atendente entity);
    List<Atendente> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
