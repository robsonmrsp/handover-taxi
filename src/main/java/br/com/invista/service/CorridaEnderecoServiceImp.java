package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.CorridaEndereco;
import br.com.invista.persistence.DaoCorridaEndereco;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@Transactional
public class CorridaEnderecoServiceImp implements CorridaEnderecoService {

	private static final Logger LOGGER = Logger.getLogger(CorridaEnderecoServiceImp.class);
	
	@Inject
	DaoCorridaEndereco daoCorridaEndereco;

	@Override
	public CorridaEndereco get(Integer id) {
		return daoCorridaEndereco.find(id);
	}
	

	@Override
	public Pager<CorridaEndereco> all(PaginationParams paginationParams) {
		Pagination<CorridaEndereco> pagination = daoCorridaEndereco.getAll(paginationParams);
		return new Pager<CorridaEndereco>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<CorridaEndereco> filter(PaginationParams paginationParams) {
		List<CorridaEndereco> list = daoCorridaEndereco.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<CorridaEndereco> all() {
		return daoCorridaEndereco.getAll();
	}

	@Override
	public List<CorridaEndereco> search(String description) {
		return new ArrayList<CorridaEndereco>();
	}
	
	public List<CorridaEndereco> last(LocalDateTime lastSyncDate){
		return daoCorridaEndereco.last(lastSyncDate);
	}
			
	@Override
	public CorridaEndereco save(CorridaEndereco entity) {
		return daoCorridaEndereco.save(entity);
	}

	@Override
	public CorridaEndereco update(CorridaEndereco entity) {
		return daoCorridaEndereco.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoCorridaEndereco.delete(id);
	}


}
