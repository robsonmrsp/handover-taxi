package br.com.invista.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.transaction.annotation.Transactional;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.model.Cliente;
import br.com.invista.model.Empresa;
import br.com.invista.model.EnderecoFavorito;
import br.com.invista.persistence.DaoEnderecoFavorito;

/**
*  generated: 16/10/2016 15:27:07
**/

@Named
@Transactional
public class EnderecoFavoritoServiceImp implements EnderecoFavoritoService {

	private static final Logger LOGGER = Logger.getLogger(EnderecoFavoritoServiceImp.class);
	
	@Inject
	DaoEnderecoFavorito daoEnderecoFavorito;
	
	@Inject
	ClienteService clienteService;
	
	@Inject
	EmpresaService empresaService;

	@Override
	public EnderecoFavorito get(Integer id) {
		return daoEnderecoFavorito.find(id);
	}
	

	@Override
	public Pager<EnderecoFavorito> all(PaginationParams paginationParams) {
		Pagination<EnderecoFavorito> pagination = daoEnderecoFavorito.getAll(paginationParams);
		return new Pager<EnderecoFavorito>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<EnderecoFavorito> filter(PaginationParams paginationParams) {
		List<EnderecoFavorito> list = daoEnderecoFavorito.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<EnderecoFavorito> all() {
		return daoEnderecoFavorito.getAll();
	}

	@Override
	public List<EnderecoFavorito> search(String description) {
		return new ArrayList<EnderecoFavorito>();
	}
	
	public List<EnderecoFavorito> last(LocalDateTime lastSyncDate){
		return daoEnderecoFavorito.last(lastSyncDate);
	}
			
	@Override
	public EnderecoFavorito save(EnderecoFavorito entity) {
		if (entity.getCliente() != null){
			Cliente tmpC = clienteService.get(entity.getCliente().getId());
			entity.setCliente(tmpC);
		}
		
		if (entity.getEmpresa() != null){
			Empresa tmpE = empresaService.get(entity.getEmpresa().getId());
			entity.setEmpresa(tmpE);
		}
		return daoEnderecoFavorito.save(entity);
	}

	@Override
	public EnderecoFavorito update(EnderecoFavorito entity) {
		return daoEnderecoFavorito.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoEnderecoFavorito.delete(id);
	}


	@Override
	public List<EnderecoFavorito> findByCliente(int idCliente, boolean empresa) {
		return daoEnderecoFavorito.findByCliente(idCliente, empresa);
	}


}
