package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.HdUsuario;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface HdUsuarioService {

	HdUsuario get(Integer id);

	List<HdUsuario> all();
	
	Pager<HdUsuario> all(PaginationParams paginationParams);

	List<HdUsuario> filter(PaginationParams paginationParams);
	
	List<HdUsuario> search(String searchText);

	HdUsuario save(HdUsuario entity);

	HdUsuario update(HdUsuario entity);
    List<HdUsuario> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
