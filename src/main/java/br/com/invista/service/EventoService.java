package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Evento;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 04/11/2016 11:29:26
**/
public interface EventoService {

	Evento get(Integer id);

	List<Evento> all();
	
	Pager<Evento> all(PaginationParams paginationParams);

	List<Evento> filter(PaginationParams paginationParams);
	
	List<Evento> search(String searchText);

	Evento save(Evento entity);

	Evento update(Evento entity);
    List<Evento> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
