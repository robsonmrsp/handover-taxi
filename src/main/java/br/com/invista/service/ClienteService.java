package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Cliente;
import br.com.invista.model.filter.FilterCliente;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;

/**
 * generated: 16/10/2016 15:27:07
 **/
public interface ClienteService {

	Cliente get(Integer id);

	Cliente getFuncionario(String telefone);

	List<Cliente> all();

	Pager<Cliente> all(PaginationParams paginationParams);

	List<Cliente> filter(PaginationParams paginationParams);

	List<Cliente> search(String searchText);

	Cliente save(Cliente entity);

	Cliente update(Cliente entity);

	List<Cliente> last(LocalDateTime lastSyncDate);

	Boolean delete(Integer id);
	
	Cliente getClienteAutenticacao(String username, String senha);
	
	Cliente getClientePorEmail(String email);
}
