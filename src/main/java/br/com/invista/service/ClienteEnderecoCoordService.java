package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.ClienteEnderecoCoord;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:07
**/
public interface ClienteEnderecoCoordService {

	ClienteEnderecoCoord get(Integer id);

	List<ClienteEnderecoCoord> all();
	
	Pager<ClienteEnderecoCoord> all(PaginationParams paginationParams);

	List<ClienteEnderecoCoord> filter(PaginationParams paginationParams);
	
	List<ClienteEnderecoCoord> search(String searchText);

	ClienteEnderecoCoord save(ClienteEnderecoCoord entity);

	ClienteEnderecoCoord update(ClienteEnderecoCoord entity);
    List<ClienteEnderecoCoord> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
