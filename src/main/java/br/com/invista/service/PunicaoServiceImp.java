package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.Punicao;
import br.com.invista.persistence.DaoPunicao;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@Transactional
public class PunicaoServiceImp implements PunicaoService {

	private static final Logger LOGGER = Logger.getLogger(PunicaoServiceImp.class);
	
	@Inject
	DaoPunicao daoPunicao;

	@Override
	public Punicao get(Integer id) {
		return daoPunicao.find(id);
	}
	

	@Override
	public Pager<Punicao> all(PaginationParams paginationParams) {
		Pagination<Punicao> pagination = daoPunicao.getAll(paginationParams);
		return new Pager<Punicao>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<Punicao> filter(PaginationParams paginationParams) {
		List<Punicao> list = daoPunicao.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<Punicao> all() {
		return daoPunicao.getAll();
	}

	@Override
	public List<Punicao> search(String description) {
		return new ArrayList<Punicao>();
	}
	
	public List<Punicao> last(LocalDateTime lastSyncDate){
		return daoPunicao.last(lastSyncDate);
	}
			
	@Override
	public Punicao save(Punicao entity) {
		return daoPunicao.save(entity);
	}

	@Override
	public Punicao update(Punicao entity) {
		return daoPunicao.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoPunicao.delete(id);
	}


}
