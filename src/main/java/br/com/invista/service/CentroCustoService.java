package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.CentroCusto;
import br.com.invista.model.Cliente;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:07
**/
public interface CentroCustoService {

	CentroCusto get(Integer id);

	List<CentroCusto> all();
	
	Pager<CentroCusto> all(PaginationParams paginationParams);

	List<CentroCusto> filter(PaginationParams paginationParams);
	
	List<CentroCusto> search(String searchText);

	CentroCusto save(CentroCusto entity);

	CentroCusto update(CentroCusto entity);
    List<CentroCusto> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);

	Boolean semLimiteDisponivelParaValor(Integer idCentroCusto, Cliente cliente);

	Boolean possuiLimiteDisponivelParaValor(Integer idCentroCusto, Cliente cliente);
}
