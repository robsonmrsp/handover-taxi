package br.com.invista.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.transaction.annotation.Transactional;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.model.FaixaVoucher;
import br.com.invista.persistence.DaoFaixaVoucher;

/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@Transactional
public class FaixaVoucherServiceImp implements FaixaVoucherService {

	private static final Logger LOGGER = Logger.getLogger(FaixaVoucherServiceImp.class);
	
	@Inject
	DaoFaixaVoucher daoFaixaVoucher;

	@Override
	public FaixaVoucher get(Integer id) {
		return daoFaixaVoucher.find(id);
	}
	

	@Override
	public Pager<FaixaVoucher> all(PaginationParams paginationParams) {
		Pagination<FaixaVoucher> pagination = daoFaixaVoucher.getAll(paginationParams);
		return new Pager<FaixaVoucher>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<FaixaVoucher> filter(PaginationParams paginationParams) {
		List<FaixaVoucher> list = daoFaixaVoucher.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<FaixaVoucher> all() {
		return daoFaixaVoucher.getAll();
	}

	@Override
	public List<FaixaVoucher> search(String description) {
		return new ArrayList<FaixaVoucher>();
	}
	
	public List<FaixaVoucher> last(LocalDateTime lastSyncDate){
		return daoFaixaVoucher.last(lastSyncDate);
	}
			
	@Override
	public FaixaVoucher save(FaixaVoucher entity) {
		if(!isValid(entity))
			throw new ValidationException("Faixa de voucher " + entity.getNumeroInicial() + " - " + entity.getNumeroFinal() + " inválida!");
		return daoFaixaVoucher.save(entity);
	}

	@Override
	public FaixaVoucher update(FaixaVoucher entity) {
		return daoFaixaVoucher.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoFaixaVoucher.delete(id);
	}


	@Override
	public Boolean isValid(FaixaVoucher voucher) {
		List<FaixaVoucher> list = daoFaixaVoucher.validaVoucher(voucher);
		return list.isEmpty();
	}


}
