package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Contato;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface ContatoService {

	Contato get(Integer id);

	List<Contato> all();
	
	Pager<Contato> all(PaginationParams paginationParams);

	List<Contato> filter(PaginationParams paginationParams);
	
	List<Contato> search(String searchText);

	Contato save(Contato entity);

	Contato update(Contato entity);
    List<Contato> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);

	Contato getPorTelefone(String telefone);
}
