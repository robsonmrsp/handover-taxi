package br.com.invista.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.transaction.annotation.Transactional;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.model.TelefoneFavorito;
import br.com.invista.model.Contato;
import br.com.invista.persistence.DaoTelefoneFavorito;
import br.com.invista.persistence.DaoContato;

/**
 * generated: 16/10/2016 15:27:08
 **/

@Named
@Transactional
public class TelefoneFavoritoServiceImp implements TelefoneFavoritoService {

	private static final Logger LOGGER = Logger.getLogger(TelefoneFavoritoServiceImp.class);

	@Inject
	DaoTelefoneFavorito daoTelefoneFavorito;

	@Inject
	DaoContato daoContato;

	@Override
	public TelefoneFavorito get(Integer id) {
		return daoTelefoneFavorito.find(id);
	}

	@Override
	public Pager<TelefoneFavorito> all(PaginationParams paginationParams) {
		Pagination<TelefoneFavorito> pagination = daoTelefoneFavorito.getAll(paginationParams);
		return new Pager<TelefoneFavorito>(pagination.getResults(), 0, pagination.getTotalRecords());
	}

	@Override
	public List<TelefoneFavorito> filter(PaginationParams paginationParams) {
		List<TelefoneFavorito> list = daoTelefoneFavorito.filter(paginationParams);
		return list;
	}

	@Override
	public List<TelefoneFavorito> all() {
		return daoTelefoneFavorito.getAll();
	}

	@Override
	public List<TelefoneFavorito> search(String description) {
		return new ArrayList<TelefoneFavorito>();
	}

	public List<TelefoneFavorito> last(LocalDateTime lastSyncDate) {
		return daoTelefoneFavorito.last(lastSyncDate);
	}

	@Override
	public TelefoneFavorito save(TelefoneFavorito entity) {
		// vitoriano : não salva se houver telefone igual
		if (this.isValid(entity.getId(), entity.getFone())) {
			throw new ValidationException(
					"Telefone " + entity.getFone() + " já está sendo utilizado por outro contato/cliente!");
		}
		// vitoriano
		return daoTelefoneFavorito.save(entity);
	}

	@Override
	public TelefoneFavorito update(TelefoneFavorito entity) {
		return daoTelefoneFavorito.save(entity);
	}

	@Override
	public TelefoneFavorito getPorTelefone(String telefone) {
		return daoTelefoneFavorito.getPorTelefone(telefone);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoTelefoneFavorito.delete(id);
	}

	public Boolean isValid(Integer id, String telefone) {
		return daoTelefoneFavorito.isValid(id, telefone);
	}

}
