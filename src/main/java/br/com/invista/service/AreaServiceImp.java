package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.Area;
import br.com.invista.persistence.DaoArea;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 16/10/2016 15:27:07
**/

@Named
@Transactional
public class AreaServiceImp implements AreaService {

	private static final Logger LOGGER = Logger.getLogger(AreaServiceImp.class);
	
	@Inject
	DaoArea daoArea;

	@Override
	public Area get(Integer id) {
		return daoArea.find(id);
	}
	

	@Override
	public Pager<Area> all(PaginationParams paginationParams) {
		Pagination<Area> pagination = daoArea.getAll(paginationParams);
		return new Pager<Area>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<Area> filter(PaginationParams paginationParams) {
		List<Area> list = daoArea.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<Area> all() {
		return daoArea.getAll();
	}

	@Override
	public List<Area> search(String description) {
		return new ArrayList<Area>();
	}
	
	public List<Area> last(LocalDateTime lastSyncDate){
		return daoArea.last(lastSyncDate);
	}
			
	@Override
	public Area save(Area entity) {
		return daoArea.save(entity);
	}

	@Override
	public Area update(Area entity) {
		return daoArea.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoArea.delete(id);
	}


}
