package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.StatusTracker;
import br.com.invista.persistence.DaoStatusTracker;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 04/11/2016 11:29:27
**/

@Named
@Transactional
public class StatusTrackerServiceImp implements StatusTrackerService {

	private static final Logger LOGGER = Logger.getLogger(StatusTrackerServiceImp.class);
	
	@Inject
	DaoStatusTracker daoStatusTracker;

	@Override
	public StatusTracker get(Integer id) {
		return daoStatusTracker.find(id);
	}
	

	@Override
	public Pager<StatusTracker> all(PaginationParams paginationParams) {
		Pagination<StatusTracker> pagination = daoStatusTracker.getAll(paginationParams);
		return new Pager<StatusTracker>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<StatusTracker> filter(PaginationParams paginationParams) {
		List<StatusTracker> list = daoStatusTracker.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<StatusTracker> all() {
		return daoStatusTracker.getAll();
	}

	@Override
	public List<StatusTracker> search(String description) {
		return new ArrayList<StatusTracker>();
	}
	
	public List<StatusTracker> last(LocalDateTime lastSyncDate){
		return daoStatusTracker.last(lastSyncDate);
	}
			
	@Override
	public StatusTracker save(StatusTracker entity) {
		return daoStatusTracker.save(entity);
	}

	@Override
	public StatusTracker update(StatusTracker entity) {
		return daoStatusTracker.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoStatusTracker.delete(id);
	}


}
