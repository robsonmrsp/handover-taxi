package br.com.invista.service;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;

import br.com.invista.model.Corrida;
import br.com.invista.model.DistanciaMotoCorrida;
import br.com.invista.model.filter.FilterDistanciaMotoCorrida;
import br.com.invista.persistence.DaoDistanciaMotoCorrida;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
 * generated: 04/11/2016 11:29:26
 **/

@Named
@Transactional
public class DistanciaMotoCorridaServiceImp implements DistanciaMotoCorridaService {

	private static final Logger LOGGER = Logger.getLogger(DistanciaMotoCorridaServiceImp.class);

	@Inject
	DaoDistanciaMotoCorrida daoDistanciaMotoCorrida;

	@Override
	public DistanciaMotoCorrida get(Integer id) {
		return daoDistanciaMotoCorrida.find(id);
	}

	@Override
	public Pager<DistanciaMotoCorrida> all(PaginationParams paginationParams) {
		Pagination<DistanciaMotoCorrida> pagination = daoDistanciaMotoCorrida.getAll(paginationParams);
		return new Pager<DistanciaMotoCorrida>(pagination.getResults(), 0, pagination.getTotalRecords());
	}

	@Override
	public List<DistanciaMotoCorrida> filter(PaginationParams paginationParams) {
		List<DistanciaMotoCorrida> list = daoDistanciaMotoCorrida.filter(paginationParams);
		return list;
	}

	@Override
	public List<DistanciaMotoCorrida> all() {
		return daoDistanciaMotoCorrida.getAll();
	}

	@Override
	public List<DistanciaMotoCorrida> search(String description) {
		return new ArrayList<DistanciaMotoCorrida>();
	}

	public List<DistanciaMotoCorrida> last(LocalDateTime lastSyncDate) {
		return daoDistanciaMotoCorrida.last(lastSyncDate);
	}

	@Override
	public DistanciaMotoCorrida save(DistanciaMotoCorrida entity) {
		return daoDistanciaMotoCorrida.save(entity);
	}

	@Override
	public DistanciaMotoCorrida update(DistanciaMotoCorrida entity) {
		return daoDistanciaMotoCorrida.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoDistanciaMotoCorrida.delete(id);
	}

	@Override
	public List<DistanciaMotoCorrida> filter(FilterDistanciaMotoCorrida filter) {

		List<DistanciaMotoCorrida> listaCompleta = daoDistanciaMotoCorrida.filter(filter);

		return listaCompleta;
	}

	public static Map<Corrida, List<DistanciaMotoCorrida>> getMapFrom(List<DistanciaMotoCorrida> listaCompleta) {
		Map<Corrida, List<DistanciaMotoCorrida>> map = new HashMap<Corrida, List<DistanciaMotoCorrida>>();
		for (DistanciaMotoCorrida distanciaMotoCorrida : listaCompleta) {
			Corrida corrida = distanciaMotoCorrida.getCorrida();
			if (map.containsKey(corrida)) {
				List<DistanciaMotoCorrida> list = map.get(corrida);
				list.add(distanciaMotoCorrida);
				map.put(corrida, list);
			} else {
				List<DistanciaMotoCorrida> list = new ArrayList<DistanciaMotoCorrida>();
				list.add(distanciaMotoCorrida);
				map.put(corrida, list);
			}
		}
		return map;
	}

	@Override
	public Map<Corrida, List<DistanciaMotoCorrida>> getMap(FilterDistanciaMotoCorrida filter) {
		return getMapFrom(filter(filter));
	}

	@Override
	public void deleteDistanciaMotoCorridaByCorrida(Corrida corrida) {
		daoDistanciaMotoCorrida.deleteDistanciaMotoCorridaByCorrida(corrida);
	}

}
