package br.com.invista.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;

import javax.inject.Inject;
import javax.inject.Named;

import java.util.ArrayList;

import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;

import br.com.invista.model.CentroCusto;
import br.com.invista.model.Cliente;
import br.com.invista.model.User;
import br.com.invista.model.filter.FilterCliente;
import br.com.invista.persistence.DaoCliente;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
 * generated: 16/10/2016 15:27:07
 **/

@Named
@Transactional
public class ClienteServiceImp implements ClienteService {

	private static final Logger LOGGER = Logger.getLogger(ClienteServiceImp.class);
	
	@Inject
	DaoCliente daoCliente;

	@Inject
	CentroCustoService centroCustoService;

	@Override
	public Cliente get(Integer id) {
		return daoCliente.find(id);
	}

	@Override
	public Pager<Cliente> all(PaginationParams paginationParams) {
		Pagination<Cliente> pagination = daoCliente.getAll(paginationParams);
		return new Pager<Cliente>(pagination.getResults(), 0, pagination.getTotalRecords());
	}

	@Override
	public List<Cliente> filter(PaginationParams paginationParams) {
		List<Cliente> list = daoCliente.filter(paginationParams);
		return list;
	}

	@Override
	public List<Cliente> all() {
		return daoCliente.getAll();
	}

	@Override
	public List<Cliente> search(String description) {
		return new ArrayList<Cliente>();
	}

	public List<Cliente> last(LocalDateTime lastSyncDate) {
		return daoCliente.last(lastSyncDate);
	}

	@Override
	public Cliente save(Cliente entity) {
		CentroCusto centroCusto = entity.getCentroCusto();
		if (centroCusto!= null && centroCustoService.semLimiteDisponivelParaValor(centroCusto.getId(), entity)) {
			throw new ValidationException("Valor de limite mensal excede o valor total do centro de custo");
		}

		return daoCliente.save(entity);
	}

	@Override
	public Cliente update(Cliente entity) {
		return daoCliente.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoCliente.delete(id);
	}

	@Override
	public Cliente getFuncionario(String telefone) {
		return daoCliente.getFuncionarioPorTelefone(telefone);
	}
	
	@Override
	public Cliente getClienteAutenticacao(String username, String senha) {
		return daoCliente.findByUserSenhaTwo(username, senha);
	}

	@Override
	public Cliente getClientePorEmail(String email) {
		return daoCliente.findByEmail(email);
	}
}
