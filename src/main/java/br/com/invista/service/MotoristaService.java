package br.com.invista.service;

import java.util.List;

import org.joda.time.LocalDateTime;

import br.com.invista.model.Corrida;
import br.com.invista.model.Motorista;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface MotoristaService {

	Motorista get(Integer id);

	List<Motorista> all();
	
	List<Motorista> motoristasOnline();

	Pager<Motorista> all(PaginationParams paginationParams);
	
	List<Motorista> getByCorridaConstraints(Corrida corrida);

	List<Motorista> filter(PaginationParams paginationParams);
	
	List<Motorista> search(String searchText);

	Motorista save(Motorista entity);

	Motorista update(Motorista entity);
    List<Motorista> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
