package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.MotoristaBloqueado;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface MotoristaBloqueadoService {

	MotoristaBloqueado get(Integer id);

	List<MotoristaBloqueado> all();
	
	Pager<MotoristaBloqueado> all(PaginationParams paginationParams);

	List<MotoristaBloqueado> filter(PaginationParams paginationParams);
	
	List<MotoristaBloqueado> search(String searchText);

	MotoristaBloqueado save(MotoristaBloqueado entity);

	MotoristaBloqueado update(MotoristaBloqueado entity);
    List<MotoristaBloqueado> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
