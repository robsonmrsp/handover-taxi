package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.EnderecoFavorito;
import br.com.invista.persistence.DaoEnderecoFavorito;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 16/10/2016 15:27:07
**/

@Named
@Transactional
public class ClienteEnderecoServiceImp implements ClienteEnderecoService {

	private static final Logger LOGGER = Logger.getLogger(ClienteEnderecoServiceImp.class);
	
	@Inject
	DaoEnderecoFavorito daoClienteEndereco;

	@Override
	public EnderecoFavorito get(Integer id) {
		return daoClienteEndereco.find(id);
	}
	

	@Override
	public Pager<EnderecoFavorito> all(PaginationParams paginationParams) {
		Pagination<EnderecoFavorito> pagination = daoClienteEndereco.getAll(paginationParams);
		return new Pager<EnderecoFavorito>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<EnderecoFavorito> filter(PaginationParams paginationParams) {
		List<EnderecoFavorito> list = daoClienteEndereco.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<EnderecoFavorito> all() {
		return daoClienteEndereco.getAll();
	}

	@Override
	public List<EnderecoFavorito> search(String description) {
		return new ArrayList<EnderecoFavorito>();
	}
	
	public List<EnderecoFavorito> last(LocalDateTime lastSyncDate){
		return daoClienteEndereco.last(lastSyncDate);
	}
			
	@Override
	public EnderecoFavorito save(EnderecoFavorito entity) {
		return daoClienteEndereco.save(entity);
	}

	@Override
	public EnderecoFavorito update(EnderecoFavorito entity) {
		return daoClienteEndereco.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoClienteEndereco.delete(id);
	}


}
