package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.StatusTracker;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 04/11/2016 11:29:27
**/
public interface StatusTrackerService {

	StatusTracker get(Integer id);

	List<StatusTracker> all();
	
	Pager<StatusTracker> all(PaginationParams paginationParams);

	List<StatusTracker> filter(PaginationParams paginationParams);
	
	List<StatusTracker> search(String searchText);

	StatusTracker save(StatusTracker entity);

	StatusTracker update(StatusTracker entity);
    List<StatusTracker> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
