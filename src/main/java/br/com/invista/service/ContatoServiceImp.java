package br.com.invista.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.transaction.annotation.Transactional;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.model.Contato;
import br.com.invista.persistence.DaoContato;

/**
 * generated: 16/10/2016 15:27:08
 **/

@Named
@Transactional
public class ContatoServiceImp implements ContatoService {

	private static final Logger LOGGER = Logger.getLogger(ContatoServiceImp.class);

	@Inject
	DaoContato daoContato;

	@Inject
	TelefoneFavoritoService clienteFoneService;

	@Override
	public Contato get(Integer id) {
		return daoContato.find(id);
	}

	@Override
	public Pager<Contato> all(PaginationParams paginationParams) {
		Pagination<Contato> pagination = daoContato.getAll(paginationParams);
		return new Pager<Contato>(pagination.getResults(), 0, pagination.getTotalRecords());
	}

	@Override
	public List<Contato> filter(PaginationParams paginationParams) {
		List<Contato> list = daoContato.filter(paginationParams);
		return list;
	}

	@Override
	public List<Contato> all() {
		return daoContato.getAll();
	}

	@Override
	public List<Contato> search(String description) {
		return new ArrayList<Contato>();
	}

	public List<Contato> last(LocalDateTime lastSyncDate) {
		return daoContato.last(lastSyncDate);
	}

	@Override
	public Contato save(Contato entity) {
		// vitoriano : não salva se houver telefone igual
		Boolean bol = this.isValid(entity.getId(), entity.getFone());
		if (bol) {
			throw new ValidationException(
					"Telefone " + entity.getFone() + " já está sendo utilizado por outro contato/cliente!");
		}
		// vitoriano
		return daoContato.save(entity);
	}

	@Override
	public Contato update(Contato entity) {
		// vitoriano : não salva se houver telefone igual
		if (this.isValid(entity.getId(), entity.getFone())) {
			throw new ValidationException(
					"Telefone " + entity.getFone() + " já está sendo utilizado por outro contato/cliente!");
		}
		// vitoriano
		return daoContato.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoContato.delete(id);
	}

	@Override
	public Contato getPorTelefone(String telefone) {
		return daoContato.getPorTelefone(telefone);
	}

	public Boolean isValid(Integer id, String telefone) {
		return daoContato.isValid(id, telefone);
	}

}
