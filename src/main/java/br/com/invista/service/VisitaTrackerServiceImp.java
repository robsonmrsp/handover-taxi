package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.VisitaTracker;
import br.com.invista.persistence.DaoVisitaTracker;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 04/11/2016 11:29:27
**/

@Named
@Transactional
public class VisitaTrackerServiceImp implements VisitaTrackerService {

	private static final Logger LOGGER = Logger.getLogger(VisitaTrackerServiceImp.class);
	
	@Inject
	DaoVisitaTracker daoVisitaTracker;

	@Override
	public VisitaTracker get(Integer id) {
		return daoVisitaTracker.find(id);
	}
	

	@Override
	public Pager<VisitaTracker> all(PaginationParams paginationParams) {
		Pagination<VisitaTracker> pagination = daoVisitaTracker.getAll(paginationParams);
		return new Pager<VisitaTracker>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<VisitaTracker> filter(PaginationParams paginationParams) {
		List<VisitaTracker> list = daoVisitaTracker.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<VisitaTracker> all() {
		return daoVisitaTracker.getAll();
	}

	@Override
	public List<VisitaTracker> search(String description) {
		return new ArrayList<VisitaTracker>();
	}
	
	public List<VisitaTracker> last(LocalDateTime lastSyncDate){
		return daoVisitaTracker.last(lastSyncDate);
	}
			
	@Override
	public VisitaTracker save(VisitaTracker entity) {
		return daoVisitaTracker.save(entity);
	}

	@Override
	public VisitaTracker update(VisitaTracker entity) {
		return daoVisitaTracker.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoVisitaTracker.delete(id);
	}


}
