package br.com.invista.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.joda.time.LocalDateTime;
import org.springframework.transaction.annotation.Transactional;

import com.eclipsesource.restfuse.Status;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.core.tasks.CriadorFilaMotoristasTask;
import br.com.invista.model.CentroCusto;
import br.com.invista.model.Cliente;
import br.com.invista.model.ContaCorrente;
import br.com.invista.model.TelefoneFavorito;
import br.com.invista.model.TipoCliente;
import br.com.invista.model.TipoPagamento;
import br.com.invista.model.Corrida;
import br.com.invista.model.Empresa;
import br.com.invista.model.StatusCorrida;
import br.com.invista.persistence.DaoCentroCusto;
import br.com.invista.persistence.DaoContaCorrente;
import br.com.invista.persistence.DaoCorrida;
import br.com.invista.rs.ConsumidorCorrida;
import br.com.invista.utils.BufferCorridas;
import br.com.invista.utils.CorridaEntityForBuffer;

/**
 * generated: 04/11/2016 11:29:26
 **/

@Named
@Transactional
public class CorridaServiceImp implements CorridaService {

	private static final Logger LOGGER = Logger.getLogger(CorridaServiceImp.class);

	@Inject
	SessionFactory sessionFactory;

	@Inject
	DaoCorrida daoCorrida;

	@Inject
	DaoCentroCusto daoCentroCusto;

	@Inject
	ContaCorrenteService contaCorrenteService;
	
	@Inject
	ClienteService clienteService;

	@Inject
	ContatoService contatoService;

	@Inject
	TelefoneFavoritoService telefoneFavoritoService;
	
	@Inject
	DistanciaMotoCorridaService distanciaMotoCorridaService;

	@Override
	public Corrida get(Integer id) {
		return daoCorrida.find(id);
	}

	@Override
	public Pager<Corrida> all(PaginationParams paginationParams) {
		Pagination<Corrida> pagination = daoCorrida.getAll(paginationParams);
		return new Pager<Corrida>(pagination.getResults(), 0, pagination.getTotalRecords());
	}

	@Override
	public List<Corrida> filter(PaginationParams paginationParams) {
		List<Corrida> list = daoCorrida.filter(paginationParams);
		return list;
	}

	@Override
	public List<Corrida> all() {
		return daoCorrida.getAll();
	}

	@Override
	public List<Corrida> search(String description) {
		return new ArrayList<Corrida>();
	}

	public List<Corrida> last(LocalDateTime lastSyncDate) {
		return daoCorrida.last(lastSyncDate);
	}

	@Override
	public Corrida save(Corrida entity) {
			
		if(entity.getDataCorrida() != null && entity.getStatusCorrida().equals(StatusCorrida.PENDENTE))
			entity.setStatusCorrida(StatusCorrida.AGENDADA);

		Boolean isReenvio = entity.getStatusCorrida().equals(StatusCorrida.REENVIADA);
		Boolean isCancelamento = entity.getStatusCorrida().equals(StatusCorrida.CANCELADA);
		Boolean isAgendamento = entity.getStatusCorrida().equals(StatusCorrida.AGENDADA);
		BufferCorridas bufferCorridas = BufferCorridas.getInstance();

		
		if (isReenvio || isCancelamento || isAgendamento){
			distanciaMotoCorridaService.deleteDistanciaMotoCorridaByCorrida(entity);
		}
		
		if((isCancelamento || isReenvio) && bufferCorridas.getSetBuffer().containsKey(entity.getId()))
			bufferCorridas.remCorridaEntity(new CorridaEntityForBuffer(entity));
		
		if(entity.getStatusCorrida().equals(StatusCorrida.PENDENTE) || isReenvio || isCancelamento || isAgendamento)
			entity.setFilaCriada(new Boolean(false));
		
		if (entity.getDataCorrida() == null || isReenvio){
			// Soluçao temporária para os testes no mobile taxi...
			// Deve ser apagada depois quando este campo for colocado na tela...
			Date dateServer = LocalDateTime.now().toDate();
			LOGGER.info("Date server [ " + dateServer + " ]");
			long timeCeara = dateServer.getTime() - (3 * 60 * 60 * 1000);
			Date dateCeara = new Date(timeCeara);
			LOGGER.info("Date Ceara [ " + dateCeara + " ]");
			
			// Produção
			//entity.setDataCorrida(new LocalDateTime(dateCeara));
			// Desenvolvimento
			entity.setDataCorrida(new LocalDateTime(dateServer));
			//entity.setDataCorrida(LocalDateTime.now());
		}
		
		if(entity.getCliente() != null){
			Cliente teste = clienteService.get(entity.getCliente().getId());
			entity.setCliente(teste);
		}

		if( entity.getCliente().getTipo() == TipoCliente.FUNCIONARIO  && entity.getTipoPagamento() == TipoPagamento.VOUCHER)
		{
			Double saldoFuncionario = contaCorrenteService.getSaldoPorFuncionario(entity.getCliente());
			if(saldoFuncionario < entity.getValor())
				throw new ValidationException("O cliente " + entity.getCliente().getNome() + " não possui saldo disponível para solicitar a corrida no valor de R$" + entity.getValor());
		}
		
		Corrida save = daoCorrida.save(entity);

		// Somente será adicionará na fila se o motorista não tiver sido
		// escolhico na tela de solicitação.
		if (save.getMotorista() == null) {
			CriadorFilaMotoristasTask.addCorrida(save);
		}
		return save;
	}

	@Override
	public Corrida update(Corrida entity) {
		return daoCorrida.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoCorrida.delete(id);
	}

	@Override
	public ConsumidorCorrida consumidorPorTelefone(String telefone) {

		ConsumidorCorrida consumidorCorrida = new ConsumidorCorrida();

		TelefoneFavorito telefoneFavorito = telefoneFavoritoService.getPorTelefone(telefone);

		if (telefoneFavorito != null) {

			Empresa empresa = telefoneFavorito.getEmpresa();
			Cliente cliente = telefoneFavorito.getCliente();

			if (empresa != null) {
				consumidorCorrida.setTipo("EMPRESA");
				consumidorCorrida.setEmpresa(empresa);
				consumidorCorrida.setCliente(cliente);
			} else if (cliente != null) {
				Empresa funcEmpresa = cliente.getEmpresa();
				consumidorCorrida.setCliente(cliente);
				if (funcEmpresa != null) {
					consumidorCorrida.setTipo("FUNCIONARIO");
					consumidorCorrida.setEmpresa(funcEmpresa);
				} else
					consumidorCorrida.setTipo("CLIENTE");
			}

		}
		return consumidorCorrida;
	}

	@Override
	public List<Corrida> historicoCorridasCliente(int idCliente, boolean empresa) {
		return daoCorrida.historicoCorridasCliente(idCliente, empresa);
	}

	@Override
	public List<Corrida> corridasAgendadasCliente(int idCliente, boolean empresa) {
		return daoCorrida.corridasAgendadasCliente(idCliente, empresa);
	}
	
	@Override
	public List<Corrida> corridasPendentes() {
		return daoCorrida.corridasPendentes();
	}

}
