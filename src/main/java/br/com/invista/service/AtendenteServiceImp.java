package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.Atendente;
import br.com.invista.persistence.DaoAtendente;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 16/10/2016 15:27:07
**/

@Named
@Transactional
public class AtendenteServiceImp implements AtendenteService {

	private static final Logger LOGGER = Logger.getLogger(AtendenteServiceImp.class);
	
	@Inject
	DaoAtendente daoAtendente;

	@Override
	public Atendente get(Integer id) {
		return daoAtendente.find(id);
	}
	

	@Override
	public Pager<Atendente> all(PaginationParams paginationParams) {
		Pagination<Atendente> pagination = daoAtendente.getAll(paginationParams);
		return new Pager<Atendente>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<Atendente> filter(PaginationParams paginationParams) {
		List<Atendente> list = daoAtendente.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<Atendente> all() {
		return daoAtendente.getAll();
	}

	@Override
	public List<Atendente> search(String description) {
		return new ArrayList<Atendente>();
	}
	
	public List<Atendente> last(LocalDateTime lastSyncDate){
		return daoAtendente.last(lastSyncDate);
	}
			
	@Override
	public Atendente save(Atendente entity) {
		return daoAtendente.save(entity);
	}

	@Override
	public Atendente update(Atendente entity) {
		return daoAtendente.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoAtendente.delete(id);
	}


}
