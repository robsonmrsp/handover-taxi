package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.MotivoPunicao;
import br.com.invista.persistence.DaoMotivoPunicao;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 28/11/2016 23:13:05
**/

@Named
@Transactional
public class MotivoPunicaoServiceImp implements MotivoPunicaoService {

	private static final Logger LOGGER = Logger.getLogger(MotivoPunicaoServiceImp.class);
	
	@Inject
	DaoMotivoPunicao daoMotivoPunicao;

	@Override
	public MotivoPunicao get(Integer id) {
		return daoMotivoPunicao.find(id);
	}
	

	@Override
	public Pager<MotivoPunicao> all(PaginationParams paginationParams) {
		Pagination<MotivoPunicao> pagination = daoMotivoPunicao.getAll(paginationParams);
		return new Pager<MotivoPunicao>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<MotivoPunicao> filter(PaginationParams paginationParams) {
		List<MotivoPunicao> list = daoMotivoPunicao.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<MotivoPunicao> all() {
		return daoMotivoPunicao.getAll();
	}

	@Override
	public List<MotivoPunicao> search(String description) {
		return new ArrayList<MotivoPunicao>();
	}
	
	public List<MotivoPunicao> last(LocalDateTime lastSyncDate){
		return daoMotivoPunicao.last(lastSyncDate);
	}
			
	@Override
	public MotivoPunicao save(MotivoPunicao entity) {
		return daoMotivoPunicao.save(entity);
	}

	@Override
	public MotivoPunicao update(MotivoPunicao entity) {
		return daoMotivoPunicao.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoMotivoPunicao.delete(id);
	}


}
