package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.TarifaAdicional;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface TarifaAdicionalService {

	TarifaAdicional get(Integer id);

	List<TarifaAdicional> all();
	
	Pager<TarifaAdicional> all(PaginationParams paginationParams);

	List<TarifaAdicional> filter(PaginationParams paginationParams);
	
	List<TarifaAdicional> search(String searchText);

	TarifaAdicional save(TarifaAdicional entity);

	TarifaAdicional update(TarifaAdicional entity);
    List<TarifaAdicional> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
