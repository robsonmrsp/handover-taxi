package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Socio;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface SocioService {

	Socio get(Integer id);

	List<Socio> all();
	
	Pager<Socio> all(PaginationParams paginationParams);

	List<Socio> filter(PaginationParams paginationParams);
	
	List<Socio> search(String searchText);

	Socio save(Socio entity);

	Socio update(Socio entity);
    List<Socio> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
