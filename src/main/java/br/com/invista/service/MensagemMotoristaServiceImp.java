package br.com.invista.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.transaction.annotation.Transactional;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.model.MensagemMotorista;
import br.com.invista.persistence.DaoMensagemMotorista;

/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@Transactional
public class MensagemMotoristaServiceImp implements MensagemMotoristaService {

	private static final Logger LOGGER = Logger.getLogger(MensagemMotoristaServiceImp.class);
	
	@Inject
	DaoMensagemMotorista daoMensagemMotorista;

	@Override
	public MensagemMotorista get(Integer id) {
		return daoMensagemMotorista.find(id);
	}
	

	@Override
	public Pager<MensagemMotorista> all(PaginationParams paginationParams) {
		Pagination<MensagemMotorista> pagination = daoMensagemMotorista.getAll(paginationParams);
		return new Pager<MensagemMotorista>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<MensagemMotorista> filter(PaginationParams paginationParams) {
		List<MensagemMotorista> list = daoMensagemMotorista.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<MensagemMotorista> all() {
		return daoMensagemMotorista.getAll();
	}

	@Override
	public List<MensagemMotorista> search(String description) {
		return new ArrayList<MensagemMotorista>();
	}
	
	public List<MensagemMotorista> last(LocalDateTime lastSyncDate){
		return daoMensagemMotorista.last(lastSyncDate);
	}
			
	@Override
	public MensagemMotorista save(MensagemMotorista entity) {
		entity.setDataEnvio(LocalDateTime.now());
		
		return daoMensagemMotorista.save(entity);
	}

	@Override
	public MensagemMotorista update(MensagemMotorista entity) {
		return daoMensagemMotorista.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoMensagemMotorista.delete(id);
	}
	
	@Override
	public List<MensagemMotorista> byMotorista(Integer id) {
		return daoMensagemMotorista.byMotorista(id);
	}


}
