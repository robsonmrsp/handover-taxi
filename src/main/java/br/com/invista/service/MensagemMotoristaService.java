package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.MensagemMotorista;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface MensagemMotoristaService {

	MensagemMotorista get(Integer id);

	List<MensagemMotorista> all();
	
	Pager<MensagemMotorista> all(PaginationParams paginationParams);

	List<MensagemMotorista> filter(PaginationParams paginationParams);
	
	List<MensagemMotorista> search(String searchText);

	MensagemMotorista save(MensagemMotorista entity);

	MensagemMotorista update(MensagemMotorista entity);
    List<MensagemMotorista> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
	
	List<MensagemMotorista> byMotorista(Integer id);
}
