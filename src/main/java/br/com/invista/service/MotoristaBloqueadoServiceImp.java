package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.MotoristaBloqueado;
import br.com.invista.persistence.DaoMotoristaBloqueado;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@Transactional
public class MotoristaBloqueadoServiceImp implements MotoristaBloqueadoService {

	private static final Logger LOGGER = Logger.getLogger(MotoristaBloqueadoServiceImp.class);
	
	@Inject
	DaoMotoristaBloqueado daoMotoristaBloqueado;

	@Override
	public MotoristaBloqueado get(Integer id) {
		return daoMotoristaBloqueado.find(id);
	}
	

	@Override
	public Pager<MotoristaBloqueado> all(PaginationParams paginationParams) {
		Pagination<MotoristaBloqueado> pagination = daoMotoristaBloqueado.getAll(paginationParams);
		return new Pager<MotoristaBloqueado>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<MotoristaBloqueado> filter(PaginationParams paginationParams) {
		List<MotoristaBloqueado> list = daoMotoristaBloqueado.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<MotoristaBloqueado> all() {
		return daoMotoristaBloqueado.getAll();
	}

	@Override
	public List<MotoristaBloqueado> search(String description) {
		return new ArrayList<MotoristaBloqueado>();
	}
	
	public List<MotoristaBloqueado> last(LocalDateTime lastSyncDate){
		return daoMotoristaBloqueado.last(lastSyncDate);
	}
			
	@Override
	public MotoristaBloqueado save(MotoristaBloqueado entity) {
		return daoMotoristaBloqueado.save(entity);
	}

	@Override
	public MotoristaBloqueado update(MotoristaBloqueado entity) {
		return daoMotoristaBloqueado.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoMotoristaBloqueado.delete(id);
	}


}
