package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.Mensagem;
import br.com.invista.persistence.DaoMensagem;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@Transactional
public class MensagemServiceImp implements MensagemService {

	private static final Logger LOGGER = Logger.getLogger(MensagemServiceImp.class);
	
	@Inject
	DaoMensagem daoMensagem;

	@Override
	public Mensagem get(Integer id) {
		return daoMensagem.find(id);
	}
	

	@Override
	public Pager<Mensagem> all(PaginationParams paginationParams) {
		Pagination<Mensagem> pagination = daoMensagem.getAll(paginationParams);
		return new Pager<Mensagem>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<Mensagem> filter(PaginationParams paginationParams) {
		List<Mensagem> list = daoMensagem.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<Mensagem> all() {
		return daoMensagem.getAll();
	}

	@Override
	public List<Mensagem> search(String description) {
		return new ArrayList<Mensagem>();
	}
	
	public List<Mensagem> last(LocalDateTime lastSyncDate){
		return daoMensagem.last(lastSyncDate);
	}
			
	@Override
	public Mensagem save(Mensagem entity) {
		return daoMensagem.save(entity);
	}

	@Override
	public Mensagem update(Mensagem entity) {
		return daoMensagem.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoMensagem.delete(id);
	}


}
