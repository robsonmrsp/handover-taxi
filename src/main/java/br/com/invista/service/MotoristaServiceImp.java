package br.com.invista.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.transaction.annotation.Transactional;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.model.Corrida;
import br.com.invista.model.Motorista;
import br.com.invista.persistence.DaoMotorista;

/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@Transactional
public class MotoristaServiceImp implements MotoristaService {

	private static final Logger LOGGER = Logger.getLogger(MotoristaServiceImp.class);
	
	@Inject
	DaoMotorista daoMotorista;

	@Override
	public Motorista get(Integer id) {
		return daoMotorista.find(id);
	}
	

	@Override
	public Pager<Motorista> all(PaginationParams paginationParams) {
		Pagination<Motorista> pagination = daoMotorista.getAll(paginationParams);
		return new Pager<Motorista>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<Motorista> filter(PaginationParams paginationParams) {
		List<Motorista> list = daoMotorista.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<Motorista> all() {
		return daoMotorista.getAll();
	}
	
	public List<Motorista> getByCorridaConstraints(Corrida corrida){
		return daoMotorista.findByCorridaConstraints(corrida);
	}

	@Override
	public List<Motorista> search(String description) {
		return new ArrayList<Motorista>();
	}
	
	public List<Motorista> last(LocalDateTime lastSyncDate){
		return daoMotorista.last(lastSyncDate);
	}
			
	@Override
	public Motorista save(Motorista entity) {
		if(entity.getId() == null && daoMotorista.isExistViatura(entity.getViatura()) )
			throw new ValidationException("O número da viatura já está sendo utilizado!");
		return daoMotorista.save(entity);
	}

	@Override
	public Motorista update(Motorista entity) {
		return daoMotorista.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoMotorista.delete(id);
	}


	@Override
	public List<Motorista> motoristasOnline() {
		return daoMotorista.motoristasOnline();
	}


}
