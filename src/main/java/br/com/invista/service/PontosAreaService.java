package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.PontosArea;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface PontosAreaService {

	PontosArea get(Integer id);

	List<PontosArea> all();
	
	Pager<PontosArea> all(PaginationParams paginationParams);

	List<PontosArea> filter(PaginationParams paginationParams);
	
	List<PontosArea> search(String searchText);

	PontosArea save(PontosArea entity);

	PontosArea update(PontosArea entity);
    List<PontosArea> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
