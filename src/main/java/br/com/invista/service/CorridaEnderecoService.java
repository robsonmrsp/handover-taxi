package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.CorridaEndereco;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface CorridaEnderecoService {

	CorridaEndereco get(Integer id);

	List<CorridaEndereco> all();
	
	Pager<CorridaEndereco> all(PaginationParams paginationParams);

	List<CorridaEndereco> filter(PaginationParams paginationParams);
	
	List<CorridaEndereco> search(String searchText);

	CorridaEndereco save(CorridaEndereco entity);

	CorridaEndereco update(CorridaEndereco entity);
    List<CorridaEndereco> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
