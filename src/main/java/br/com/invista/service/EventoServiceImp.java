package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.Evento;
import br.com.invista.persistence.DaoEvento;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 04/11/2016 11:29:26
**/

@Named
@Transactional
public class EventoServiceImp implements EventoService {

	private static final Logger LOGGER = Logger.getLogger(EventoServiceImp.class);
	
	@Inject
	DaoEvento daoEvento;

	@Override
	public Evento get(Integer id) {
		return daoEvento.find(id);
	}
	

	@Override
	public Pager<Evento> all(PaginationParams paginationParams) {
		Pagination<Evento> pagination = daoEvento.getAll(paginationParams);
		return new Pager<Evento>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<Evento> filter(PaginationParams paginationParams) {
		List<Evento> list = daoEvento.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<Evento> all() {
		return daoEvento.getAll();
	}

	@Override
	public List<Evento> search(String description) {
		return new ArrayList<Evento>();
	}
	
	public List<Evento> last(LocalDateTime lastSyncDate){
		return daoEvento.last(lastSyncDate);
	}
			
	@Override
	public Evento save(Evento entity) {
		return daoEvento.save(entity);
	}

	@Override
	public Evento update(Evento entity) {
		return daoEvento.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoEvento.delete(id);
	}


}
