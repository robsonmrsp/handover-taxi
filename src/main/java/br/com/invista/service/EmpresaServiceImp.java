package br.com.invista.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.transaction.annotation.Transactional;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.model.Empresa;
import br.com.invista.model.EnderecoFavorito;
import br.com.invista.persistence.DaoEmpresa;

/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@Transactional
public class EmpresaServiceImp implements EmpresaService {

	private static final Logger LOGGER = Logger.getLogger(EmpresaServiceImp.class);
	
	@Inject
	DaoEmpresa daoEmpresa;

	@Override
	public Empresa get(Integer id) {
		return daoEmpresa.find(id);
	}
	

	@Override
	public Pager<Empresa> all(PaginationParams paginationParams) {
		Pagination<Empresa> pagination = daoEmpresa.getAll(paginationParams);
		return new Pager<Empresa>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<Empresa> filter(PaginationParams paginationParams) {
		List<Empresa> list = daoEmpresa.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<Empresa> all() {
		return daoEmpresa.getAll();
	}

	@Override
	public List<Empresa> search(String description) {
		return new ArrayList<Empresa>();
	}
	
	public List<Empresa> last(LocalDateTime lastSyncDate){
		return daoEmpresa.last(lastSyncDate);
	}
			
	@Override
	public Empresa save(Empresa entity) {
		
		
		
		// empresa : endereco, numeroEndereco, complemento, cep, bairro, cidade, uf, referenciaEndereco
		// corrida : enderecoPartida, enderecoNumero, enderecoCep, enderecoBairro, enderecoCidade, enderecoUf 
		// 
		// nao contempla lat long

//		if(){
//			EnderecoFavorito ef = new EnderecoCorridaServiceImp();
//			ef.
//			
//			daoEnderecoFavorito.save()
//		}
		
		
		
		return daoEmpresa.save(entity);
	}

	@Override
	public Empresa update(Empresa entity) {
		return daoEmpresa.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoEmpresa.delete(id);
	}


	@Override
	public Empresa getEmpresaAutenticacao(String username, String senha) {
		return daoEmpresa.findByUserSenhaTwo(username, senha);
	}


}
