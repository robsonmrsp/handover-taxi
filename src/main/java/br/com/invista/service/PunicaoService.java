package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Punicao;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface PunicaoService {

	Punicao get(Integer id);

	List<Punicao> all();
	
	Pager<Punicao> all(PaginationParams paginationParams);

	List<Punicao> filter(PaginationParams paginationParams);
	
	List<Punicao> search(String searchText);

	Punicao save(Punicao entity);

	Punicao update(Punicao entity);
    List<Punicao> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
