package br.com.invista.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.transaction.annotation.Transactional;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.model.Cliente;
import br.com.invista.model.ContaCorrente;
import br.com.invista.persistence.DaoContaCorrente;

/**
 * Edilson Leitão 16/01/2017
 **/

@Named
@Transactional
public class ContaCorrenteServiceImp implements ContaCorrenteService {

	private static final Logger LOGGER = Logger.getLogger(ContaCorrenteServiceImp.class);

	@Inject
	DaoContaCorrente daoContaCorrente;

	@Override
	public ContaCorrente get(Integer id) 
	{
		return daoContaCorrente.find(id);
	}

	@Override
	public List<ContaCorrente> all() 
	{
		return daoContaCorrente.getAll();
	}

	@Override
	public Pager<ContaCorrente> all(PaginationParams paginationParams) 
	{
		Pagination<ContaCorrente> pagination = daoContaCorrente.getAll(paginationParams);
		return new Pager<ContaCorrente>(pagination.getResults(), 0, pagination.getTotalRecords());
	}

	@Override
	public List<ContaCorrente> filter(PaginationParams paginationParams) 
	{
		List<ContaCorrente> list = daoContaCorrente.filter(paginationParams);
		return list;
	}

	@Override
	public List<ContaCorrente> search(String description) 
	{
		return new ArrayList<ContaCorrente>();
	}

	@Override
	public ContaCorrente save(ContaCorrente entity) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContaCorrente update(ContaCorrente entity) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ContaCorrente> last(LocalDateTime lastSyncDate) 
	{
		return daoContaCorrente.last(lastSyncDate);
	}

	@Override
	public Boolean delete(Integer id) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getSaldoPorFuncionario(Cliente cliente) 
	{
		return daoContaCorrente.getSaldoPorFuncionario(cliente);
	}
}
