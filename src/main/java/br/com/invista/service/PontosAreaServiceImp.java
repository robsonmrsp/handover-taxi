package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.PontosArea;
import br.com.invista.persistence.DaoPontosArea;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@Transactional
public class PontosAreaServiceImp implements PontosAreaService {

	private static final Logger LOGGER = Logger.getLogger(PontosAreaServiceImp.class);
	
	@Inject
	DaoPontosArea daoPontosArea;

	@Override
	public PontosArea get(Integer id) {
		return daoPontosArea.find(id);
	}
	

	@Override
	public Pager<PontosArea> all(PaginationParams paginationParams) {
		Pagination<PontosArea> pagination = daoPontosArea.getAll(paginationParams);
		return new Pager<PontosArea>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<PontosArea> filter(PaginationParams paginationParams) {
		List<PontosArea> list = daoPontosArea.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<PontosArea> all() {
		return daoPontosArea.getAll();
	}

	@Override
	public List<PontosArea> search(String description) {
		return new ArrayList<PontosArea>();
	}
	
	public List<PontosArea> last(LocalDateTime lastSyncDate){
		return daoPontosArea.last(lastSyncDate);
	}
			
	@Override
	public PontosArea save(PontosArea entity) {
		return daoPontosArea.save(entity);
	}

	@Override
	public PontosArea update(PontosArea entity) {
		return daoPontosArea.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoPontosArea.delete(id);
	}


}
