package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Area;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:07
**/
public interface AreaService {

	Area get(Integer id);

	List<Area> all();
	
	Pager<Area> all(PaginationParams paginationParams);

	List<Area> filter(PaginationParams paginationParams);
	
	List<Area> search(String searchText);

	Area save(Area entity);

	Area update(Area entity);
    List<Area> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
