package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.Socio;
import br.com.invista.persistence.DaoSocio;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 16/10/2016 15:27:08
**/

@Named
@Transactional
public class SocioServiceImp implements SocioService {

	private static final Logger LOGGER = Logger.getLogger(SocioServiceImp.class);
	
	@Inject
	DaoSocio daoSocio;

	@Override
	public Socio get(Integer id) {
		return daoSocio.find(id);
	}
	

	@Override
	public Pager<Socio> all(PaginationParams paginationParams) {
		Pagination<Socio> pagination = daoSocio.getAll(paginationParams);
		return new Pager<Socio>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<Socio> filter(PaginationParams paginationParams) {
		List<Socio> list = daoSocio.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<Socio> all() {
		return daoSocio.getAll();
	}

	@Override
	public List<Socio> search(String description) {
		return new ArrayList<Socio>();
	}
	
	public List<Socio> last(LocalDateTime lastSyncDate){
		return daoSocio.last(lastSyncDate);
	}
			
	@Override
	public Socio save(Socio entity) {
		return daoSocio.save(entity);
	}

	@Override
	public Socio update(Socio entity) {
		return daoSocio.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoSocio.delete(id);
	}


}
