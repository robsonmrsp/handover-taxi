package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.TelefoneFavorito;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface TelefoneFavoritoService {

	TelefoneFavorito get(Integer id);

	List<TelefoneFavorito> all();
	
	Pager<TelefoneFavorito> all(PaginationParams paginationParams);

	List<TelefoneFavorito> filter(PaginationParams paginationParams);
	
	List<TelefoneFavorito> search(String searchText);

	TelefoneFavorito save(TelefoneFavorito entity);

	TelefoneFavorito update(TelefoneFavorito entity);
    List<TelefoneFavorito> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);

	TelefoneFavorito getPorTelefone(String telefone);
}
