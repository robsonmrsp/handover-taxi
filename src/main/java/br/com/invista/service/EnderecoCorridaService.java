package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.EnderecoCorrida;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 04/11/2016 11:29:26
**/
public interface EnderecoCorridaService {

	EnderecoCorrida get(Integer id);

	List<EnderecoCorrida> all();
	
	Pager<EnderecoCorrida> all(PaginationParams paginationParams);

	List<EnderecoCorrida> filter(PaginationParams paginationParams);
	
	List<EnderecoCorrida> search(String searchText);

	EnderecoCorrida save(EnderecoCorrida entity);

	EnderecoCorrida update(EnderecoCorrida entity);
    List<EnderecoCorrida> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
