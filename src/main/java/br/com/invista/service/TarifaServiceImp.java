package br.com.invista.service;

import java.util.List;

import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;

import java.util.ArrayList;

import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;

import br.com.invista.model.Tarifa;
import br.com.invista.persistence.DaoTarifa;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
 * generated: 16/10/2016 15:27:08
 **/

@Named
@Transactional
public class TarifaServiceImp implements TarifaService {

	private static final Logger LOGGER = Logger.getLogger(TarifaServiceImp.class);

	@Inject
	DaoTarifa daoTarifa;

	@Override
	public Tarifa get(Integer id) {
		return daoTarifa.find(id);
	}

	@Override
	public Pager<Tarifa> all(PaginationParams paginationParams) {
		Pagination<Tarifa> pagination = daoTarifa.getAll(paginationParams);
		return new Pager<Tarifa>(pagination.getResults(), 0, pagination.getTotalRecords());
	}

	@Override
	public List<Tarifa> filter(PaginationParams paginationParams) {
		List<Tarifa> list = daoTarifa.filter(paginationParams);
		return list;
	}

	@Override
	public List<Tarifa> all() {
		return daoTarifa.getAll();
	}

	@Override
	public List<Tarifa> search(String description) {
		return new ArrayList<Tarifa>();
	}

	public List<Tarifa> last(LocalDateTime lastSyncDate) {
		return daoTarifa.last(lastSyncDate);
	}

	@Override
	public Tarifa save(Tarifa entity) {
		return daoTarifa.save(entity);
	}

	@Override
	public Tarifa update(Tarifa entity) {
		return daoTarifa.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoTarifa.delete(id);
	}

	@Override
	public Tarifa buscaPorOrigemDestino(String origem, String destino) {
		Tarifa t = daoTarifa.findByOrigemDestino(origem, destino);
		if (t == null) {
			t = daoTarifa.findByOrigemDestino(destino, origem);
		}
		return t;
	}
}
