package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Tarifa;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 16/10/2016 15:27:08
**/
public interface TarifaService {

	Tarifa get(Integer id);

	List<Tarifa> all();
	
	Pager<Tarifa> all(PaginationParams paginationParams);

	List<Tarifa> filter(PaginationParams paginationParams);
	
	List<Tarifa> search(String searchText);

	Tarifa save(Tarifa entity);

	Tarifa update(Tarifa entity);
    List<Tarifa> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
	
	Tarifa buscaPorOrigemDestino(String origem, String destino);
}
