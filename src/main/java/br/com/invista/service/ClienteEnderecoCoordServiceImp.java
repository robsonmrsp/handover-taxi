package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.ClienteEnderecoCoord;
import br.com.invista.persistence.DaoClienteEnderecoCoord;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 16/10/2016 15:27:07
**/

@Named
@Transactional
public class ClienteEnderecoCoordServiceImp implements ClienteEnderecoCoordService {

	private static final Logger LOGGER = Logger.getLogger(ClienteEnderecoCoordServiceImp.class);
	
	@Inject
	DaoClienteEnderecoCoord daoClienteEnderecoCoord;

	@Override
	public ClienteEnderecoCoord get(Integer id) {
		return daoClienteEnderecoCoord.find(id);
	}
	

	@Override
	public Pager<ClienteEnderecoCoord> all(PaginationParams paginationParams) {
		Pagination<ClienteEnderecoCoord> pagination = daoClienteEnderecoCoord.getAll(paginationParams);
		return new Pager<ClienteEnderecoCoord>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<ClienteEnderecoCoord> filter(PaginationParams paginationParams) {
		List<ClienteEnderecoCoord> list = daoClienteEnderecoCoord.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<ClienteEnderecoCoord> all() {
		return daoClienteEnderecoCoord.getAll();
	}

	@Override
	public List<ClienteEnderecoCoord> search(String description) {
		return new ArrayList<ClienteEnderecoCoord>();
	}
	
	public List<ClienteEnderecoCoord> last(LocalDateTime lastSyncDate){
		return daoClienteEnderecoCoord.last(lastSyncDate);
	}
			
	@Override
	public ClienteEnderecoCoord save(ClienteEnderecoCoord entity) {
		return daoClienteEnderecoCoord.save(entity);
	}

	@Override
	public ClienteEnderecoCoord update(ClienteEnderecoCoord entity) {
		return daoClienteEnderecoCoord.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoClienteEnderecoCoord.delete(id);
	}


}
