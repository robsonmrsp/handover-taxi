package br.com.invista.service;

import java.util.List;

import org.joda.time.LocalDateTime;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.model.Cliente;
import br.com.invista.model.ContaCorrente;
/**
*  Edilson Leitão 16/01/2017
**/
public interface ContaCorrenteService {

	ContaCorrente get(Integer id);

	List<ContaCorrente> all();
	
	Pager<ContaCorrente> all(PaginationParams paginationParams);

	List<ContaCorrente> filter(PaginationParams paginationParams);
	
	List<ContaCorrente> search(String searchText);

	ContaCorrente save(ContaCorrente entity);

	ContaCorrente update(ContaCorrente entity);
    List<ContaCorrente> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);

	Double getSaldoPorFuncionario(Cliente cliente);
}
