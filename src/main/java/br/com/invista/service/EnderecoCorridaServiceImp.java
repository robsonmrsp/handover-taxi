package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.EnderecoCorrida;
import br.com.invista.persistence.DaoEnderecoCorrida;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 04/11/2016 11:29:26
**/

@Named
@Transactional
public class EnderecoCorridaServiceImp implements EnderecoCorridaService {

	private static final Logger LOGGER = Logger.getLogger(EnderecoCorridaServiceImp.class);
	
	@Inject
	DaoEnderecoCorrida daoEnderecoCorrida;

	@Override
	public EnderecoCorrida get(Integer id) {
		return daoEnderecoCorrida.find(id);
	}
	

	@Override
	public Pager<EnderecoCorrida> all(PaginationParams paginationParams) {
		Pagination<EnderecoCorrida> pagination = daoEnderecoCorrida.getAll(paginationParams);
		return new Pager<EnderecoCorrida>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<EnderecoCorrida> filter(PaginationParams paginationParams) {
		List<EnderecoCorrida> list = daoEnderecoCorrida.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<EnderecoCorrida> all() {
		return daoEnderecoCorrida.getAll();
	}

	@Override
	public List<EnderecoCorrida> search(String description) {
		return new ArrayList<EnderecoCorrida>();
	}
	
	public List<EnderecoCorrida> last(LocalDateTime lastSyncDate){
		return daoEnderecoCorrida.last(lastSyncDate);
	}
			
	@Override
	public EnderecoCorrida save(EnderecoCorrida entity) {
		return daoEnderecoCorrida.save(entity);
	}

	@Override
	public EnderecoCorrida update(EnderecoCorrida entity) {
		return daoEnderecoCorrida.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoEnderecoCorrida.delete(id);
	}


}
