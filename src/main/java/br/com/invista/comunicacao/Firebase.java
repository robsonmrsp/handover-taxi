package br.com.invista.comunicacao;

import java.io.InputStream;

public class Firebase {
	public static final InputStream SERVICE_ACOUNT_JSON = Firebase.class.getResourceAsStream("/keys/chat-eeb48-firebase-adminsdk-bjwds-a97cc1566e.json");;

	public static final String DATABASE_URL = "https://chat-eeb48.firebaseio.com/";
}
