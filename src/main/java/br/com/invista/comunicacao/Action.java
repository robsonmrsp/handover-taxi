package br.com.invista.comunicacao;

public interface Action {
	void execute();
}
