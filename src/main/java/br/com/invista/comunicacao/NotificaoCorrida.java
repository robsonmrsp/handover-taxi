package br.com.invista.comunicacao;

public class NotificaoCorrida {

	private int idCorrida;
	private int idMotorista;
	private Long timestamp;

	public NotificaoCorrida(int idCorrida, Long timestamp, int idMotorista) {
		this.idCorrida = idCorrida;
		this.timestamp = timestamp;
		this.idMotorista = idMotorista;
	}

	public int getIdCorrida() {
		return idCorrida;
	}

	public void setIdCorrida(int idCorrida) {
		this.idCorrida = idCorrida;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public int getIdMotorista() {
		return idMotorista;
	}

	public void setIdMotorista(int idMotorista) {
		this.idMotorista = idMotorista;
	}
	
}