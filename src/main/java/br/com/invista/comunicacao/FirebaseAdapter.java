package br.com.invista.comunicacao;

import java.io.ByteArrayInputStream;

import org.apache.log4j.Logger;

import br.com.invista.model.Company;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Ainda Falta definir corretamente os paths
 * 
 * 
 * Outras informações serão definidas no melhor local no futuro.
 * 
 * @author robson
 *
 */
public class FirebaseAdapter {

	private static final Logger LOGGER = Logger.getLogger(FirebaseAdapter.class);

	private DatabaseReference usersRef;

	public FirebaseAdapter() {

		try {
			FirebaseOptions options = new FirebaseOptions.Builder()//
					.setServiceAccount(Firebase.SERVICE_ACOUNT_JSON)//
					.setDatabaseUrl(Firebase.DATABASE_URL).build();
			FirebaseApp.initializeApp(options);
		} catch (Exception e) {

		}

		final FirebaseDatabase database = FirebaseDatabase.getInstance();
		// TODO
		// Desenvolvimento
		//DatabaseReference ref = database.getReference("notificacao-corrida-"+CompanyFirebase.COMPANY);
		// Produção
		DatabaseReference ref = database.getReference("notificacao-corrida");

		usersRef = ref;
	}

	/**
	 * Ainda está confuso para mim como a lista de motorista será atualizada
	 * 
	 * @param corrida
	 */
	public void send(NotificaoCorrida corrida, final Action sucesso, final Action erro) {
		DatabaseReference ref = usersRef.child(corrida.getIdMotorista()+"").child("corridas");
		ref.child("" + corrida.getIdCorrida()).setValue(corrida, new DatabaseReference.CompletionListener() {
			@Override
			public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
				if (databaseError != null) {
					LOGGER.error(databaseError.getMessage());
					erro.execute();
				} else {
					sucesso.execute();
				}
			}
		});
	}
	
	public void limpaFilas(final Action sucesso, final Action erro) {
		usersRef.setValue(null, new DatabaseReference.CompletionListener() {
			@Override
			public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
				if (databaseError != null) {
					LOGGER.error(databaseError.getMessage());
					erro.execute();
				} else {
					sucesso.execute();
				}
			}
		});
	}
}
