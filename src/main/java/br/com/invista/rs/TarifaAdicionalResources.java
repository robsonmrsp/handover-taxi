package br.com.invista.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.invista.core.json.JsonError;
import br.com.invista.core.json.JsonPaginator;
import br.com.invista.json.JsonTarifaAdicional;

import br.com.invista.model.TarifaAdicional;

import br.com.invista.service.TarifaAdicionalService;
import br.com.invista.model.filter.FilterTarifaAdicional;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.service.UserService;
import br.com.invista.core.utils.Parser;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.core.security.SpringSecurityUserContext;
/**
*  generated: 16/10/2016 15:27:08
**/

@Path("/crud/tarifaAdicionals")
public class TarifaAdicionalResources {

	@Inject
	TarifaAdicionalService tarifaAdicionalService;
	
	
	public static final Logger LOGGER = Logger.getLogger(TarifaAdicionalResources.class);

	@GET
	@Path("filter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterTarifaAdicional> paginationParams = new PaginationParams<FilterTarifaAdicional>(uriInfo, FilterTarifaAdicional.class);

			List<JsonTarifaAdicional> jsonTarifaAdicionals = Parser.toListJsonTarifaAdicionals(tarifaAdicionalService.filter(paginationParams));
			response = Response.ok(jsonTarifaAdicionals).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all() {
		Response response = null;
		try {
			List<JsonTarifaAdicional> jsonTarifaAdicionals = Parser.toListJsonTarifaAdicionals(tarifaAdicionalService.all());
			response = Response.ok(jsonTarifaAdicionals).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<TarifaAdicional> tarifaAdicionals = null;

		try {
			PaginationParams<FilterTarifaAdicional> paginationParams = new PaginationParams<FilterTarifaAdicional>(uriInfo, FilterTarifaAdicional.class);
			tarifaAdicionals = tarifaAdicionalService.all(paginationParams);
			JsonPaginator<JsonTarifaAdicional> paginator = new JsonPaginator<JsonTarifaAdicional>(Parser.toListJsonTarifaAdicionals(tarifaAdicionals.getItens()), tarifaAdicionals.getActualPage(), tarifaAdicionals.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar tarifaAdicionals para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			TarifaAdicional tarifaAdicional = tarifaAdicionalService.get(id);

			return Response.ok().entity(Parser.toJson(tarifaAdicional)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(JsonTarifaAdicional jsonTarifaAdicional) {
		try {

			TarifaAdicional tarifaAdicional = Parser.toEntity(jsonTarifaAdicional);
			tarifaAdicional = tarifaAdicionalService.save(tarifaAdicional);
			return Response.ok().entity(Parser.toJson(tarifaAdicional)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonTarifaAdicional.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonTarifaAdicional, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  tarifaAdicional [ %s ] parametros [ %s ]", e.getMessage(), jsonTarifaAdicional.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonTarifaAdicional)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") Integer id, JsonTarifaAdicional jsonTarifaAdicional) {
		try {
			TarifaAdicional tarifaAdicional = Parser.toEntity(jsonTarifaAdicional);

			tarifaAdicional = tarifaAdicionalService.save(tarifaAdicional);
			return Response.ok().entity(Parser.toJson(tarifaAdicional)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonTarifaAdicional.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonTarifaAdicional, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonTarifaAdicional.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonTarifaAdicional)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(tarifaAdicionalService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
