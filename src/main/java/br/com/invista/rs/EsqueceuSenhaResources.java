package br.com.invista.rs;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.com.invista.core.json.JsonError;
import br.com.invista.core.service.EmailNotificationService;

/**
*  created: 02/01/2016 16:08:07
**/
@Path("/crud/senha")
public class EsqueceuSenhaResources {
	@Inject
	EmailNotificationService emailNotificationService;
	
	public static final Logger LOGGER = Logger.getLogger(EsqueceuSenhaResources.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{email}/{senha}")
	public Response enviarEmailRecuperacao(@PathParam("email") String email, @PathParam("senha") String senha) {
		try {
			String titulo = "Recupera��o de senha";
			String emailOrigem = "noreply@invistatech.com.br";
			String corpo = "Ol�! <br> &emsp; A senha do seu usu�rio �: " + senha +
					"<br><br><b>Atenciosamente,</b> <br>"+
					"<img src=\"http://portalvhdsjg19kbq1q36k1.blob.core.windows.net/handover-storage/zass/logotmt.png\" width=\"263\" height=\"258\">";
			
			emailNotificationService.sendEmail(emailOrigem, email, titulo, corpo);
			
			return Response.ok().build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %s ]", e.getMessage(), email);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, email)).build();
		}
	}
}
