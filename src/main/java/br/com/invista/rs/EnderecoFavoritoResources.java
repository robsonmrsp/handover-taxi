package br.com.invista.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.invista.core.json.JsonError;
import br.com.invista.core.json.JsonPaginator;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.core.utils.Parser;
import br.com.invista.json.JsonEnderecoFavorito;
import br.com.invista.json.JsonUserApp;
import br.com.invista.model.EnderecoFavorito;
import br.com.invista.model.filter.FilterEnderecoFavorito;
import br.com.invista.service.EnderecoFavoritoService;

/**
 * generated: 16/10/2016 15:27:07
 **/

@Path("/crud/enderecoFavoritos")
public class EnderecoFavoritoResources {

	@Inject
	EnderecoFavoritoService enderecoFavoritoService;

	public static final Logger LOGGER = Logger.getLogger(EnderecoFavoritoResources.class);

	@GET
	@Path("filter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterEnderecoFavorito> paginationParams = new PaginationParams<FilterEnderecoFavorito>(uriInfo, FilterEnderecoFavorito.class);

			List<JsonEnderecoFavorito> jsonEnderecoFavoritos = Parser.toListJsonEnderecoFavoritos(enderecoFavoritoService.filter(paginationParams));
			response = Response.ok(jsonEnderecoFavoritos).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all() {
		Response response = null;
		try {
			List<JsonEnderecoFavorito> jsonEnderecoFavoritos = Parser.toListJsonEnderecoFavoritos(enderecoFavoritoService.all());
			response = Response.ok(jsonEnderecoFavoritos).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<EnderecoFavorito> enderecoFavoritos = null;

		try {
			PaginationParams<FilterEnderecoFavorito> paginationParams = new PaginationParams<FilterEnderecoFavorito>(uriInfo, FilterEnderecoFavorito.class);
			enderecoFavoritos = enderecoFavoritoService.all(paginationParams);
			JsonPaginator<JsonEnderecoFavorito> paginator = new JsonPaginator<JsonEnderecoFavorito>(Parser.toListJsonEnderecoFavoritos(enderecoFavoritos.getItens()), enderecoFavoritos.getActualPage(), enderecoFavoritos.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar enderecoFavoritos para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			EnderecoFavorito enderecoFavorito = enderecoFavoritoService.get(id);

			return Response.ok().entity(Parser.toJson(enderecoFavorito)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("bycliente")
	public Response getByCliente(JsonUserApp user) {
		try {

			List<EnderecoFavorito> enderecoFavoritos = enderecoFavoritoService.findByCliente(user.getId(),user.getEmpresa());

			return Response.ok().entity(Parser.toListJsonEnderecoFavoritos(enderecoFavoritos)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), user.getId());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, user.getId())).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(JsonEnderecoFavorito jsonEnderecoFavorito) {
		try {

			EnderecoFavorito enderecoFavorito = Parser.toEntity(jsonEnderecoFavorito);
			enderecoFavorito = enderecoFavoritoService.save(enderecoFavorito);
			return Response.ok().entity(Parser.toJson(enderecoFavorito)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonEnderecoFavorito.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonEnderecoFavorito, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  enderecoFavorito [ %s ] parametros [ %s ]", e.getMessage(), jsonEnderecoFavorito.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonEnderecoFavorito)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") Integer id, JsonEnderecoFavorito jsonEnderecoFavorito) {
		try {
			EnderecoFavorito enderecoFavorito = Parser.toEntity(jsonEnderecoFavorito);

			enderecoFavorito = enderecoFavoritoService.save(enderecoFavorito);
			return Response.ok().entity(Parser.toJson(enderecoFavorito)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonEnderecoFavorito.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonEnderecoFavorito, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonEnderecoFavorito.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonEnderecoFavorito)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(enderecoFavoritoService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
