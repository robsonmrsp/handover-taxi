package br.com.invista.rs;

import br.com.invista.model.Cliente;
import br.com.invista.model.Empresa;

public class ConsumidorCorrida {

	String tipo; // funcionario, contato, cliente

	Empresa empresa;

	Cliente cliente;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
