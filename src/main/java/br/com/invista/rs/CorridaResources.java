package br.com.invista.rs;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.invista.core.json.JsonError;
import br.com.invista.core.json.JsonPaginator;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.core.utils.Parser;
import br.com.invista.json.JsonCorrida;
import br.com.invista.json.JsonUserApp;
import br.com.invista.model.Corrida;
import br.com.invista.model.filter.FilterCorrida;
import br.com.invista.service.CorridaService;

/**
 * generated: 04/11/2016 11:29:26
 **/

@Path("/crud/corridas")
public class CorridaResources {

	@Inject
	CorridaService corridaService;

	public static final Logger LOGGER = Logger.getLogger(CorridaResources.class);

	@GET
	@Path("filter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterCorrida> paginationParams = new PaginationParams<FilterCorrida>(uriInfo, FilterCorrida.class);

			List<JsonCorrida> jsonCorridas = Parser.toListJsonCorridas(corridaService.filter(paginationParams));
			response = Response.ok(jsonCorridas).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all() {
		Response response = null;
		try {
			List<JsonCorrida> jsonCorridas = Parser.toListJsonCorridas(corridaService.all());
			response = Response.ok(jsonCorridas).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Path("clienteCorrida")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response clientCorridaPorTelefone(@QueryParam("telefone") String telefone) {
		Response response = null;
		try {
			ConsumidorCorrida dto = corridaService.consumidorPorTelefone(telefone);

			response = Response.ok(Parser.toJson(dto)).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Corrida> corridas = null;

		try {
			PaginationParams<FilterCorrida> paginationParams = new PaginationParams<FilterCorrida>(uriInfo, FilterCorrida.class);
			corridas = corridaService.all(paginationParams);
			JsonPaginator<JsonCorrida> paginator = new JsonPaginator<JsonCorrida>(Parser.toListJsonCorridas(corridas.getItens()), corridas.getActualPage(), corridas.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar corridas para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Corrida corrida = corridaService.get(id);

			return Response.ok().entity(Parser.toJson(corrida)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(JsonCorrida jsonCorrida) {
		try {

			Corrida corrida = Parser.toEntity(jsonCorrida);
			corrida = corridaService.save(corrida);
			return Response.ok().entity(Parser.toJson(corrida)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonCorrida.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonCorrida, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  corrida [ %s ] parametros [ %s ]", e.getMessage(), jsonCorrida.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonCorrida)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") Integer id, JsonCorrida jsonCorrida) {
		try {
			Corrida corrida = Parser.toEntity(jsonCorrida);

			corrida = corridaService.save(corrida);
			return Response.ok().entity(Parser.toJson(corrida)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonCorrida.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonCorrida, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonCorrida.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonCorrida)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(corridaService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("hist")
	public Response historicoCorridasCliente(JsonUserApp user) {
		try {

			List<Corrida> corridas = corridaService.historicoCorridasCliente(user.getId(), user.getEmpresa());
			if (corridas == null)
				corridas = new ArrayList<Corrida>();
			return Response.ok().entity(Parser.toListJsonCorridas(corridas)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), user.getId());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, user.getId())).build();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("agend")
	public Response corridasAgendadasCliente(JsonUserApp user) {
		try {

			List<Corrida> corridas = corridaService.corridasAgendadasCliente(user.getId(), user.getEmpresa());
			if (corridas == null)
				corridas = new ArrayList<Corrida>();
			return Response.ok().entity(Parser.toListJsonCorridas(corridas)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), user.getId());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, user.getId())).build();
		}
	}
}
