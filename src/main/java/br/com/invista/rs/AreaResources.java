package br.com.invista.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.invista.core.json.JsonError;
import br.com.invista.core.json.JsonPaginator;
import br.com.invista.json.JsonArea;

import br.com.invista.model.Area;

import br.com.invista.service.AreaService;
import br.com.invista.model.filter.FilterArea;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.service.UserService;
import br.com.invista.core.utils.Parser;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.core.security.SpringSecurityUserContext;
/**
*  generated: 16/10/2016 15:27:07
**/

@Path("/crud/areas")
public class AreaResources {

	@Inject
	AreaService areaService;
	
	
	public static final Logger LOGGER = Logger.getLogger(AreaResources.class);

	@GET
	@Path("filter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterArea> paginationParams = new PaginationParams<FilterArea>(uriInfo, FilterArea.class);

			List<JsonArea> jsonAreas = Parser.toListJsonAreas(areaService.filter(paginationParams));
			response = Response.ok(jsonAreas).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all() {
		Response response = null;
		try {
			List<JsonArea> jsonAreas = Parser.toListJsonAreas(areaService.all());
			response = Response.ok(jsonAreas).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Area> areas = null;

		try {
			PaginationParams<FilterArea> paginationParams = new PaginationParams<FilterArea>(uriInfo, FilterArea.class);
			areas = areaService.all(paginationParams);
			JsonPaginator<JsonArea> paginator = new JsonPaginator<JsonArea>(Parser.toListJsonAreas(areas.getItens()), areas.getActualPage(), areas.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar areas para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Area area = areaService.get(id);

			return Response.ok().entity(Parser.toJson(area)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(JsonArea jsonArea) {
		try {

			Area area = Parser.toEntity(jsonArea);
			area = areaService.save(area);
			return Response.ok().entity(Parser.toJson(area)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonArea.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonArea, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  area [ %s ] parametros [ %s ]", e.getMessage(), jsonArea.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonArea)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") Integer id, JsonArea jsonArea) {
		try {
			Area area = Parser.toEntity(jsonArea);

			area = areaService.save(area);
			return Response.ok().entity(Parser.toJson(area)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonArea.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonArea, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonArea.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonArea)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(areaService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
