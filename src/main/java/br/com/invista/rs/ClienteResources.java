package br.com.invista.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.invista.core.json.JsonError;
import br.com.invista.core.json.JsonPaginator;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.core.utils.Parser;
import br.com.invista.json.JsonCliente;
import br.com.invista.json.JsonTelefoneFavorito;
import br.com.invista.model.Cliente;
import br.com.invista.model.TelefoneFavorito;
import br.com.invista.model.filter.FilterCliente;
import br.com.invista.service.ClienteService;
import br.com.invista.service.TelefoneFavoritoService;
/**
*  generated: 16/10/2016 15:27:07
**/

@Path("/crud/clientes")
public class ClienteResources {

	@Inject
	ClienteService clienteService;
	
	@Inject
	TelefoneFavoritoService telefoneFavoritoService; 
	
	
	public static final Logger LOGGER = Logger.getLogger(ClienteResources.class);

	@GET
	@Path("filter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterCliente> paginationParams = new PaginationParams<FilterCliente>(uriInfo, FilterCliente.class);

			List<JsonCliente> jsonClientes = Parser.toListJsonClientes(clienteService.filter(paginationParams));
			response = Response.ok(jsonClientes).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all() {
		Response response = null;
		try {
			List<JsonCliente> jsonClientes = Parser.toListJsonClientes(clienteService.all());
			response = Response.ok(jsonClientes).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Cliente> clientes = null;

		try {
			PaginationParams<FilterCliente> paginationParams = new PaginationParams<FilterCliente>(uriInfo, FilterCliente.class);
			clientes = clienteService.all(paginationParams);
			JsonPaginator<JsonCliente> paginator = new JsonPaginator<JsonCliente>(Parser.toListJsonClientes(clientes.getItens()), clientes.getActualPage(), clientes.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar clientes para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Cliente cliente = clienteService.get(id);

			return Response.ok().entity(Parser.toJson(cliente)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{user}/{senha}")
	public Response getCliente(@PathParam("user") String user, @PathParam("senha") String senha) {
		try {

			Cliente cliente = clienteService.getClienteAutenticacao(user, senha);

			return Response.ok().entity(Parser.toJson(cliente)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), user);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, user)).build();
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("byemail/{email}")
	public Response getCliente(@PathParam("email") String email) {
		try {

			Cliente cliente = clienteService.getClientePorEmail(email);

			return Response.ok().entity(Parser.toJson(cliente)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %s ]", e.getMessage(), email);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, email)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(JsonCliente jsonCliente) {
		try {
			TelefoneFavorito tmpTel = null;
			if (jsonCliente.getTelefone() != null)
				tmpTel = telefoneFavoritoService.getPorTelefone(jsonCliente.getTelefone());
			if (tmpTel == null){
				Cliente cliente = Parser.toEntity(jsonCliente);
				
				cliente = clienteService.save(cliente);
				
				if (jsonCliente.getTelefonesFavoritos() != null){
					for (JsonTelefoneFavorito tf: jsonCliente.getTelefonesFavoritos()){
						tf.setCliente(Parser.toJson(cliente));
						TelefoneFavorito tf2 = Parser.toEntity(tf);
						telefoneFavoritoService.save(tf2);
					}
					
				}				
				
				
				return Response.ok().entity(Parser.toJson(cliente)).build();
			}else{
				if (jsonCliente.getId() != null && jsonCliente.getId() != 0){
					Cliente cliente = Parser.toEntity(jsonCliente);
					
					cliente = clienteService.save(cliente);
					return Response.ok().entity(Parser.toJson(cliente)).build();
				}else
					return Response.serverError().build();
			}
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonCliente.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonCliente, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  cliente [ %s ] parametros [ %s ]", e.getMessage(), jsonCliente.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonCliente)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") Integer id, JsonCliente jsonCliente) {
		try {
			Cliente cliente = Parser.toEntity(jsonCliente);

			cliente = clienteService.save(cliente);
			return Response.ok().entity(Parser.toJson(cliente)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonCliente.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonCliente, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonCliente.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonCliente)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(clienteService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
