package br.com.invista.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.invista.core.json.JsonError;
import br.com.invista.core.json.JsonPaginator;
import br.com.invista.json.JsonMotivoPunicao;

import br.com.invista.model.MotivoPunicao;

import br.com.invista.service.MotivoPunicaoService;
import br.com.invista.model.filter.FilterMotivoPunicao;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.service.UserService;
import br.com.invista.core.utils.Parser;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.core.security.SpringSecurityUserContext;
/**
*  generated: 28/11/2016 23:13:05
**/

@Path("/crud/motivoPunicaos")
public class MotivoPunicaoResources {

	@Inject
	MotivoPunicaoService motivoPunicaoService;
	
	
	public static final Logger LOGGER = Logger.getLogger(MotivoPunicaoResources.class);

	@GET
	@Path("filter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterMotivoPunicao> paginationParams = new PaginationParams<FilterMotivoPunicao>(uriInfo, FilterMotivoPunicao.class);

			List<JsonMotivoPunicao> jsonMotivoPunicaos = Parser.toListJsonMotivoPunicaos(motivoPunicaoService.filter(paginationParams));
			response = Response.ok(jsonMotivoPunicaos).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all() {
		Response response = null;
		try {
			List<JsonMotivoPunicao> jsonMotivoPunicaos = Parser.toListJsonMotivoPunicaos(motivoPunicaoService.all());
			response = Response.ok(jsonMotivoPunicaos).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<MotivoPunicao> motivoPunicaos = null;

		try {
			PaginationParams<FilterMotivoPunicao> paginationParams = new PaginationParams<FilterMotivoPunicao>(uriInfo, FilterMotivoPunicao.class);
			motivoPunicaos = motivoPunicaoService.all(paginationParams);
			JsonPaginator<JsonMotivoPunicao> paginator = new JsonPaginator<JsonMotivoPunicao>(Parser.toListJsonMotivoPunicaos(motivoPunicaos.getItens()), motivoPunicaos.getActualPage(), motivoPunicaos.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar motivoPunicaos para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			MotivoPunicao motivoPunicao = motivoPunicaoService.get(id);

			return Response.ok().entity(Parser.toJson(motivoPunicao)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(JsonMotivoPunicao jsonMotivoPunicao) {
		try {

			MotivoPunicao motivoPunicao = Parser.toEntity(jsonMotivoPunicao);
			motivoPunicao = motivoPunicaoService.save(motivoPunicao);
			return Response.ok().entity(Parser.toJson(motivoPunicao)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonMotivoPunicao.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonMotivoPunicao, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  motivoPunicao [ %s ] parametros [ %s ]", e.getMessage(), jsonMotivoPunicao.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonMotivoPunicao)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") Integer id, JsonMotivoPunicao jsonMotivoPunicao) {
		try {
			MotivoPunicao motivoPunicao = Parser.toEntity(jsonMotivoPunicao);

			motivoPunicao = motivoPunicaoService.save(motivoPunicao);
			return Response.ok().entity(Parser.toJson(motivoPunicao)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonMotivoPunicao.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonMotivoPunicao, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonMotivoPunicao.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonMotivoPunicao)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(motivoPunicaoService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
