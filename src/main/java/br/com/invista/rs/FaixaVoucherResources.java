package br.com.invista.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.invista.core.json.JsonError;
import br.com.invista.core.json.JsonPaginator;
import br.com.invista.json.JsonFaixaVoucher;

import br.com.invista.model.FaixaVoucher;

import br.com.invista.service.FaixaVoucherService;
import br.com.invista.model.filter.FilterFaixaVoucher;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.service.UserService;
import br.com.invista.core.utils.Parser;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.core.security.SpringSecurityUserContext;
/**
*  generated: 16/10/2016 15:27:08
**/

@Path("/crud/faixaVouchers")
public class FaixaVoucherResources {

	@Inject
	FaixaVoucherService faixaVoucherService;
	
	
	public static final Logger LOGGER = Logger.getLogger(FaixaVoucherResources.class);

	@GET
	@Path("filter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterFaixaVoucher> paginationParams = new PaginationParams<FilterFaixaVoucher>(uriInfo, FilterFaixaVoucher.class);

			List<JsonFaixaVoucher> jsonFaixaVouchers = Parser.toListJsonFaixaVouchers(faixaVoucherService.filter(paginationParams));
			response = Response.ok(jsonFaixaVouchers).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all() {
		Response response = null;
		try {
			List<JsonFaixaVoucher> jsonFaixaVouchers = Parser.toListJsonFaixaVouchers(faixaVoucherService.all());
			response = Response.ok(jsonFaixaVouchers).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<FaixaVoucher> faixaVouchers = null;

		try {
			PaginationParams<FilterFaixaVoucher> paginationParams = new PaginationParams<FilterFaixaVoucher>(uriInfo, FilterFaixaVoucher.class);
			faixaVouchers = faixaVoucherService.all(paginationParams);
			JsonPaginator<JsonFaixaVoucher> paginator = new JsonPaginator<JsonFaixaVoucher>(Parser.toListJsonFaixaVouchers(faixaVouchers.getItens()), faixaVouchers.getActualPage(), faixaVouchers.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar faixaVouchers para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			FaixaVoucher faixaVoucher = faixaVoucherService.get(id);

			return Response.ok().entity(Parser.toJson(faixaVoucher)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(JsonFaixaVoucher jsonFaixaVoucher) {
		try {

			FaixaVoucher faixaVoucher = Parser.toEntity(jsonFaixaVoucher);
			faixaVoucher = faixaVoucherService.save(faixaVoucher);
			return Response.ok().entity(Parser.toJson(faixaVoucher)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonFaixaVoucher.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonFaixaVoucher, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  faixaVoucher [ %s ] parametros [ %s ]", e.getMessage(), jsonFaixaVoucher.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonFaixaVoucher)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") Integer id, JsonFaixaVoucher jsonFaixaVoucher) {
		try {
			FaixaVoucher faixaVoucher = Parser.toEntity(jsonFaixaVoucher);

			faixaVoucher = faixaVoucherService.save(faixaVoucher);
			return Response.ok().entity(Parser.toJson(faixaVoucher)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonFaixaVoucher.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonFaixaVoucher, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonFaixaVoucher.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonFaixaVoucher)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(faixaVoucherService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
