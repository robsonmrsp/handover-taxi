package br.com.invista.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.invista.core.json.JsonError;
import br.com.invista.core.json.JsonPaginator;
import br.com.invista.json.JsonTarifa;

import br.com.invista.model.Tarifa;

import br.com.invista.service.TarifaService;
import br.com.invista.model.filter.FilterTarifa;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.service.UserService;
import br.com.invista.core.utils.Parser;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.core.security.SpringSecurityUserContext;

/**
 * generated: 16/10/2016 15:27:08
 **/

@Path("/crud/tarifas")
public class TarifaResources {

	@Inject
	TarifaService tarifaService;

	public static final Logger LOGGER = Logger.getLogger(TarifaResources.class);

	@GET
	@Path("filter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterTarifa> paginationParams = new PaginationParams<FilterTarifa>(uriInfo,
					FilterTarifa.class);

			List<JsonTarifa> jsonTarifas = Parser.toListJsonTarifas(tarifaService.filter(paginationParams));
			response = Response.ok(jsonTarifas).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all() {
		Response response = null;
		try {
			List<JsonTarifa> jsonTarifas = Parser.toListJsonTarifas(tarifaService.all());
			response = Response.ok(jsonTarifas).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Tarifa> tarifas = null;

		try {
			PaginationParams<FilterTarifa> paginationParams = new PaginationParams<FilterTarifa>(uriInfo,
					FilterTarifa.class);
			tarifas = tarifaService.all(paginationParams);
			JsonPaginator<JsonTarifa> paginator = new JsonPaginator<JsonTarifa>(
					Parser.toListJsonTarifas(tarifas.getItens()), tarifas.getActualPage(), tarifas.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar tarifas para os parametros %s [%s]",
					uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString()))
					.build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Tarifa tarifa = tarifaService.get(id);

			return Response.ok().entity(Parser.toJson(tarifa)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]",
					e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("byorigdest")
	public Response buscaPorOrigemDestino(JsonTarifa tarifaIn) {
		try {
			// Pesquisa Origem > Destino e Destino > Origem
			Tarifa tarifa = tarifaService.buscaPorOrigemDestino(tarifaIn.getBairroOrigem(),
					tarifaIn.getBairroDestino());
			return Response.ok().entity(Parser.toJson(tarifa)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %s ]",
					e.getMessage(), tarifaIn.getBairroOrigem());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, tarifaIn.getBairroOrigem())).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(JsonTarifa jsonTarifa) {
		try {

			Tarifa tarifa = Parser.toEntity(jsonTarifa);
			tarifa = tarifaService.save(tarifa);
			return Response.ok().entity(Parser.toJson(tarifa)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]",
					e.getOrigem().getMessage(), jsonTarifa.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonTarifa, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  tarifa [ %s ] parametros [ %s ]", e.getMessage(),
					jsonTarifa.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonTarifa)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") Integer id, JsonTarifa jsonTarifa) {
		try {
			Tarifa tarifa = Parser.toEntity(jsonTarifa);

			tarifa = tarifaService.save(tarifa);
			return Response.ok().entity(Parser.toJson(tarifa)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]",
					e.getOrigem().getMessage(), jsonTarifa.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonTarifa, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]",
					e.getMessage(), jsonTarifa.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonTarifa)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(tarifaService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]",
					e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]",
					e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
