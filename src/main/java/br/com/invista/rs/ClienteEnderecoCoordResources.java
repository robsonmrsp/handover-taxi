package br.com.invista.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.invista.core.json.JsonError;
import br.com.invista.core.json.JsonPaginator;
import br.com.invista.json.JsonClienteEnderecoCoord;

import br.com.invista.model.ClienteEnderecoCoord;

import br.com.invista.service.ClienteEnderecoCoordService;
import br.com.invista.model.filter.FilterClienteEnderecoCoord;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.service.UserService;
import br.com.invista.core.utils.Parser;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.core.security.SpringSecurityUserContext;
/**
*  generated: 16/10/2016 15:27:07
**/

@Path("/crud/clienteEnderecoCoords")
public class ClienteEnderecoCoordResources {

	@Inject
	ClienteEnderecoCoordService clienteEnderecoCoordService;
	
	
	public static final Logger LOGGER = Logger.getLogger(ClienteEnderecoCoordResources.class);

	@GET
	@Path("filter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterClienteEnderecoCoord> paginationParams = new PaginationParams<FilterClienteEnderecoCoord>(uriInfo, FilterClienteEnderecoCoord.class);

			List<JsonClienteEnderecoCoord> jsonClienteEnderecoCoords = Parser.toListJsonClienteEnderecoCoords(clienteEnderecoCoordService.filter(paginationParams));
			response = Response.ok(jsonClienteEnderecoCoords).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all() {
		Response response = null;
		try {
			List<JsonClienteEnderecoCoord> jsonClienteEnderecoCoords = Parser.toListJsonClienteEnderecoCoords(clienteEnderecoCoordService.all());
			response = Response.ok(jsonClienteEnderecoCoords).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<ClienteEnderecoCoord> clienteEnderecoCoords = null;

		try {
			PaginationParams<FilterClienteEnderecoCoord> paginationParams = new PaginationParams<FilterClienteEnderecoCoord>(uriInfo, FilterClienteEnderecoCoord.class);
			clienteEnderecoCoords = clienteEnderecoCoordService.all(paginationParams);
			JsonPaginator<JsonClienteEnderecoCoord> paginator = new JsonPaginator<JsonClienteEnderecoCoord>(Parser.toListJsonClienteEnderecoCoords(clienteEnderecoCoords.getItens()), clienteEnderecoCoords.getActualPage(), clienteEnderecoCoords.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar clienteEnderecoCoords para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			ClienteEnderecoCoord clienteEnderecoCoord = clienteEnderecoCoordService.get(id);

			return Response.ok().entity(Parser.toJson(clienteEnderecoCoord)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(JsonClienteEnderecoCoord jsonClienteEnderecoCoord) {
		try {

			ClienteEnderecoCoord clienteEnderecoCoord = Parser.toEntity(jsonClienteEnderecoCoord);
			clienteEnderecoCoord = clienteEnderecoCoordService.save(clienteEnderecoCoord);
			return Response.ok().entity(Parser.toJson(clienteEnderecoCoord)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonClienteEnderecoCoord.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonClienteEnderecoCoord, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  clienteEnderecoCoord [ %s ] parametros [ %s ]", e.getMessage(), jsonClienteEnderecoCoord.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonClienteEnderecoCoord)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") Integer id, JsonClienteEnderecoCoord jsonClienteEnderecoCoord) {
		try {
			ClienteEnderecoCoord clienteEnderecoCoord = Parser.toEntity(jsonClienteEnderecoCoord);

			clienteEnderecoCoord = clienteEnderecoCoordService.save(clienteEnderecoCoord);
			return Response.ok().entity(Parser.toJson(clienteEnderecoCoord)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonClienteEnderecoCoord.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonClienteEnderecoCoord, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonClienteEnderecoCoord.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonClienteEnderecoCoord)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(clienteEnderecoCoordService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
