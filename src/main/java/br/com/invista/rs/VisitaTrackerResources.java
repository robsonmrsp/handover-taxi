package br.com.invista.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.invista.core.json.JsonError;
import br.com.invista.core.json.JsonPaginator;
import br.com.invista.json.JsonVisitaTracker;

import br.com.invista.model.VisitaTracker;

import br.com.invista.service.VisitaTrackerService;
import br.com.invista.model.filter.FilterVisitaTracker;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.service.UserService;
import br.com.invista.core.utils.Parser;
import br.com.invista.core.rs.exception.ValidationException;
import br.com.invista.core.security.SpringSecurityUserContext;
/**
*  generated: 04/11/2016 11:29:27
**/

@Path("/crud/visitaTrackers")
public class VisitaTrackerResources {

	@Inject
	VisitaTrackerService visitaTrackerService;
	
	
	public static final Logger LOGGER = Logger.getLogger(VisitaTrackerResources.class);

	@GET
	@Path("filter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterVisitaTracker> paginationParams = new PaginationParams<FilterVisitaTracker>(uriInfo, FilterVisitaTracker.class);

			List<JsonVisitaTracker> jsonVisitaTrackers = Parser.toListJsonVisitaTrackers(visitaTrackerService.filter(paginationParams));
			response = Response.ok(jsonVisitaTrackers).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all() {
		Response response = null;
		try {
			List<JsonVisitaTracker> jsonVisitaTrackers = Parser.toListJsonVisitaTrackers(visitaTrackerService.all());
			response = Response.ok(jsonVisitaTrackers).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<VisitaTracker> visitaTrackers = null;

		try {
			PaginationParams<FilterVisitaTracker> paginationParams = new PaginationParams<FilterVisitaTracker>(uriInfo, FilterVisitaTracker.class);
			visitaTrackers = visitaTrackerService.all(paginationParams);
			JsonPaginator<JsonVisitaTracker> paginator = new JsonPaginator<JsonVisitaTracker>(Parser.toListJsonVisitaTrackers(visitaTrackers.getItens()), visitaTrackers.getActualPage(), visitaTrackers.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar visitaTrackers para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			VisitaTracker visitaTracker = visitaTrackerService.get(id);

			return Response.ok().entity(Parser.toJson(visitaTracker)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(JsonVisitaTracker jsonVisitaTracker) {
		try {

			VisitaTracker visitaTracker = Parser.toEntity(jsonVisitaTracker);
			visitaTracker = visitaTrackerService.save(visitaTracker);
			return Response.ok().entity(Parser.toJson(visitaTracker)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonVisitaTracker.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonVisitaTracker, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  visitaTracker [ %s ] parametros [ %s ]", e.getMessage(), jsonVisitaTracker.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonVisitaTracker)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") Integer id, JsonVisitaTracker jsonVisitaTracker) {
		try {
			VisitaTracker visitaTracker = Parser.toEntity(jsonVisitaTracker);

			visitaTracker = visitaTrackerService.save(visitaTracker);
			return Response.ok().entity(Parser.toJson(visitaTracker)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonVisitaTracker.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonVisitaTracker, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonVisitaTracker.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonVisitaTracker)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(visitaTrackerService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
