package br.com.invista.rs;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.com.invista.core.json.JsonError;
import br.com.invista.core.utils.Parser;
import br.com.invista.json.JsonUserApp;
import br.com.invista.model.Cliente;
import br.com.invista.model.Empresa;
import br.com.invista.service.ClienteService;
import br.com.invista.service.EmpresaService;

/**
*  created: 19/12/2016 14:45:07
**/
@Path("/crud/autenticacao")
public class AutenticacaoResources {

		@Inject
		ClienteService clienteService;
		
		@Inject
		EmpresaService empresaService;
		
		
		public static final Logger LOGGER = Logger.getLogger(ClienteResources.class);
		
		@POST
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		@Path("get")
		public Response getUsuarioApp(JsonUserApp usuario) {
			try {
				JsonUserApp userApp = null;
				Cliente cliente = clienteService.getClienteAutenticacao(usuario.getUsername(), usuario.getSenha());
				
				if (cliente == null){
					/*Empresa empresa = empresaService.getEmpresaAutenticacao(usuario.getUsername(), usuario.getSenha());
					if (empresa != null){
						userApp = new JsonUserApp();
						userApp.setEmpresa(true);
						userApp.setId(empresa.getId());
						userApp.setNome(empresa.getNomeFantasia());
						userApp.setEmail(empresa.getEmail());
						userApp.setUsername(usuario.getUsername());
						userApp.setSenha(empresa.getSenha());
						//Email e id facebook...
					}*/
				}else{
					userApp = new JsonUserApp();
					userApp.setEmpresa(false);
					userApp.setId(cliente.getId());
					userApp.setNome(cliente.getNome());
					userApp.setEmail(cliente.getEmail());
					userApp.setUsername(usuario.getUsername());
					userApp.setSenha(cliente.getSenha());
					userApp.setEmailFacebook(cliente.getEmailFacebook());
					userApp.setIdFacebook(cliente.getIdFacebook());
					userApp.setTipo(cliente.getTipo());
					if (cliente.getTelefonesFavoritos().size()>0)
						userApp.setTelefone(cliente.getTelefonesFavoritos().get(0).getFone());
					if (cliente.getEmpresa()!=null){
						userApp.setEmpresaJson(Parser.toJson(cliente.getEmpresa()));
					}
				}

				return Response.ok().entity(userApp).build();

			} catch (Exception e) {
				String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %s ]", e.getMessage(), usuario.getUsername());
				LOGGER.error(message, e);
				return Response.serverError().entity(new JsonError(e, message, usuario.getUsername())).build();
			}
		}

		/*@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response save(JsonCliente jsonCliente) {
			try {

				Cliente cliente = Parser.toEntity(jsonCliente);
				cliente = clienteService.save(cliente);
				return Response.ok().entity(Parser.toJson(cliente)).build();
			} catch (ValidationException e) {
				String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonCliente.toString());
				LOGGER.error(message, e.getOrigem());
				return Response.serverError().entity(new JsonError(e, message, jsonCliente, e.getLegalMessage())).build();
			} catch (Exception e) {
				String message = String.format("Não foi possivel salvar  cliente [ %s ] parametros [ %s ]", e.getMessage(), jsonCliente.toString());
				LOGGER.error(message, e);
				return Response.serverError().entity(new JsonError(e, message, jsonCliente)).build();
			}
		}*/

	}

