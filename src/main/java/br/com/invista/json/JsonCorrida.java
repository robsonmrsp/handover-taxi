package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.model.InformacaoTarifaAdicional;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
 * generated: 04/11/2016 11:29:26
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonCorrida implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private String dataCorrida;
	private String dataAgendamento;
	private Integer tipoPagamento;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double valor;
	private String observacao;
	private String observacaoGeral;
	private String statusCorrida;
	private Integer seqEmAtendimento;
	private Boolean motoGrande;
	private Boolean bau;
	private Boolean filaCriada;
	private Boolean padronizada;
	private ArrayList<JsonEnderecoCorrida> enderecoCorridas = new ArrayList<JsonEnderecoCorrida>();
	private ArrayList<JsonDistanciaMotoCorrida> distanciaMotoCorridas = new ArrayList<JsonDistanciaMotoCorrida>();

	private JsonAtendente atendente;
	private JsonMotorista motorista;
	private JsonCliente cliente;
	private JsonEmpresa empresa;
	private String telefone;

	private List<JsonInformacaoTarifaAdicional> informacaoTarifas = new ArrayList<JsonInformacaoTarifaAdicional>();

	public JsonCorrida() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDataCorrida() {
		return dataCorrida;
	}

	public void setDataCorrida(String dataCorrida) {
		this.dataCorrida = dataCorrida;
	}

	public Integer getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(Integer tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getStatusCorrida() {
		return statusCorrida;
	}

	public void setStatusCorrida(String statusCorrida) {
		this.statusCorrida = statusCorrida;
	}

	public Integer getSeqEmAtendimento() {
		return seqEmAtendimento;
	}

	public void setSeqEmAtendimento(Integer seqEmAtendimento) {
		this.seqEmAtendimento = seqEmAtendimento;
	}

	public Boolean getMotoGrande() {
		return motoGrande;
	}

	public void setMotoGrande(Boolean motoGrande) {
		this.motoGrande = motoGrande;
	}

	public Boolean getBau() {
		return bau;
	}

	public void setBau(Boolean bau) {
		this.bau = bau;
	}

	public ArrayList<JsonEnderecoCorrida> getEnderecoCorridas() {
		return enderecoCorridas;
	}

	public void setEnderecoCorridas(ArrayList<JsonEnderecoCorrida> enderecoCorrida) {
		this.enderecoCorridas = enderecoCorrida;
	}

	public ArrayList<JsonDistanciaMotoCorrida> getDistanciaMotoCorridas() {
		return distanciaMotoCorridas;
	}

	public void setDistanciaMotoCorridas(ArrayList<JsonDistanciaMotoCorrida> distanciaMotoCorrida) {
		this.distanciaMotoCorridas = distanciaMotoCorrida;
	}

	public JsonAtendente getAtendente() {
		return atendente;
	}

	public void setAtendente(JsonAtendente atendente) {
		this.atendente = atendente;
	}

	public JsonMotorista getMotorista() {
		return motorista;
	}

	public void setMotorista(JsonMotorista motorista) {
		this.motorista = motorista;
	}

	public SyncOperation getSyncOperation() {
		if (syncOperation == null) {
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}

	public void setSyncOperation(SyncOperation syncOperation) {
		this.syncOperation = syncOperation;
	}

	public JsonCliente getCliente() {
		return cliente;
	}

	public void setCliente(JsonCliente cliente) {
		this.cliente = cliente;
	}

	public JsonEmpresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(JsonEmpresa empresa) {
		this.empresa = empresa;
	}

	public String getObservacaoGeral() {
		return observacaoGeral;
	}

	public void setObservacaoGeral(String observacaoGeral) {
		this.observacaoGeral = observacaoGeral;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public List<JsonInformacaoTarifaAdicional> getInformacaoTarifas() {
		return informacaoTarifas;
	}

	public void setInformacaoTarifas(List<JsonInformacaoTarifaAdicional> informacaoTarifas) {
		this.informacaoTarifas = informacaoTarifas;
	}

	public Boolean getFilaCriada() {
		return filaCriada;
	}

	public void setFilaCriada(Boolean filaCriada) {
		this.filaCriada = filaCriada;
	}

	public Boolean getPadronizada() {
		return padronizada;
	}

	public void setPadronizada(Boolean padronizada) {
		this.padronizada = padronizada;
	}

	public String getDataAgendamento() {
		return dataAgendamento;
	}

	public void setDataAgendamento(String dataAgendamento) {
		this.dataAgendamento = dataAgendamento;
	}

}