package br.com.invista.json;

public class JsonConsumidorCorrida {

	String tipo;
	JsonEmpresa empresa;
	JsonCliente cliente;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public JsonEmpresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(JsonEmpresa empresa) {
		this.empresa = empresa;
	}

	public JsonCliente getCliente() {
		return cliente;
	}

	public void setCliente(JsonCliente cliente) {
		this.cliente = cliente;
	}

}
