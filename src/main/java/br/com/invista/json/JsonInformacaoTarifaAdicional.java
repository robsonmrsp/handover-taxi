package br.com.invista.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.serialization.CustomDoubleDeserializer;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;

/**
 * generated: 01/01/2017 11:29:26
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonInformacaoTarifaAdicional {
	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;

	private Integer quantidade;
	private String descricao;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double valorTotal;

	private JsonTarifaAdicional tarifaAdicional;

	public JsonTarifaAdicional getTarifaAdicional() {
		return tarifaAdicional;
	}

	public void setTarifaAdicional(JsonTarifaAdicional tarifaAdicional) {
		this.tarifaAdicional = tarifaAdicional;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}
}
