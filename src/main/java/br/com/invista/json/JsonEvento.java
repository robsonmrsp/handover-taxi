package br.com.invista.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;

/**
*  generated: 04/11/2016 11:29:26
**/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonEvento implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private String descricao;
	private Long dataInicio;
	private Long dataFim;
	private String descricaoCurta;
	private JsonEnderecoCorrida enderecoCorrida;		
	private JsonMotorista motorista;		
	private JsonVisitaTracker visitaTrackerChegada;		
	private JsonVisitaTracker visitaTrackerSaida;		
	
	public  JsonEvento() {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Long getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Long dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Long getDataFim() {
		return dataFim;
	}

	public void setDataFim(Long dataFim) {
		this.dataFim = dataFim;
	}
	public String getDescricaoCurta() {
		return descricaoCurta;
	}

	public void setDescricaoCurta(String descricaoCurta) {
		this.descricaoCurta = descricaoCurta;
	}
	
	public JsonEnderecoCorrida getEnderecoCorrida() {
		return enderecoCorrida;
	}
	
	public void setEnderecoCorrida(JsonEnderecoCorrida enderecoCorrida) {
		this.enderecoCorrida = enderecoCorrida;
	}
	public JsonMotorista getMotorista() {
		return motorista;
	}
	
	public void setMotorista(JsonMotorista motorista) {
		this.motorista = motorista;
	}

	public SyncOperation getSyncOperation (){
		if(syncOperation == null){
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}
	
	public void setSyncOperation (SyncOperation  syncOperation){
		this.syncOperation = syncOperation;
	}

	public JsonVisitaTracker getVisitaTrackerChegada() {
		return visitaTrackerChegada;
	}

	public void setVisitaTrackerChegada(JsonVisitaTracker visitaTrackerChegada) {
		this.visitaTrackerChegada = visitaTrackerChegada;
	}

	public JsonVisitaTracker getVisitaTrackerSaida() {
		return visitaTrackerSaida;
	}

	public void setVisitaTrackerSaida(JsonVisitaTracker visitaTrackerSaida) {
		this.visitaTrackerSaida = visitaTrackerSaida;
	}
	
}