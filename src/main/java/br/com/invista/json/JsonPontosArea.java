package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
*  generated: 16/10/2016 15:27:08
**/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonPontosArea implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double latitude;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double longitude;
	private JsonArea area;		
	
	public  JsonPontosArea() {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public JsonArea getArea() {
		return area;
	}
	
	public void setArea(JsonArea area) {
		this.area = area;
	}
	public SyncOperation getSyncOperation (){
		if(syncOperation == null){
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}
	
	public void setSyncOperation (SyncOperation  syncOperation){
		this.syncOperation = syncOperation;
	}
	
}