package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.model.Empresa;
import br.com.invista.core.serialization.CustomDoubleDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;

/**
 * generated: 16/10/2016 15:27:08
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private String name;
	private String username;
	private String password;
	private Boolean enable;
	private String image;
	private String tipo;
	private String perfil;
	private String email;

	private String statusUsuario;

	private Integer usuarioinclusao;

	private JsonEmpresa empresa;

	private ArrayList<JsonRole> roles = new ArrayList<JsonRole>();

	public String getStatusUsuario() {
		return statusUsuario;
	}

	public void setStatusUsuario(String statusUsuario) {
		this.statusUsuario = statusUsuario;
	}

	public Integer getUsuarioinclusao() {
		return usuarioinclusao;
	}

	public void setUsuarioinclusao(Integer usuarioinclusao) {
		this.usuarioinclusao = usuarioinclusao;
	}

	public JsonEmpresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(JsonEmpresa empresa) {
		this.empresa = empresa;
	}

	public JsonUser() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public ArrayList<JsonRole> getRoles() {
		return roles;
	}

	public void setRoles(ArrayList<JsonRole> role) {
		this.roles = role;
	}

	public SyncOperation getSyncOperation() {
		if (syncOperation == null) {
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}

	public void setSyncOperation(SyncOperation syncOperation) {
		this.syncOperation = syncOperation;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

}