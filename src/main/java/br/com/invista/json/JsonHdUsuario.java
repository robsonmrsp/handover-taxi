package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
*  generated: 16/10/2016 15:27:08
**/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonHdUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private String nome;
	private String email;
	private String senha;
	private String tipo;
	private String perfil;
	private String statusUsuario;
	private String datainclusao;
	private Integer usuarioinclusao;
	private JsonEmpresa empresa;		
	
	public  JsonHdUsuario() {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public String getStatusUsuario() {
		return statusUsuario;
	}

	public void setStatusUsuario(String statusUsuario) {
		this.statusUsuario = statusUsuario;
	}
	public String getDatainclusao() {
		return datainclusao;
	}

	public void setDatainclusao(String datainclusao) {
		this.datainclusao = datainclusao;
	}				
	public Integer getUsuarioinclusao() {
		return usuarioinclusao;
	}

	public void setUsuarioinclusao(Integer usuarioinclusao) {
		this.usuarioinclusao = usuarioinclusao;
	}
	
	public JsonEmpresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(JsonEmpresa empresa) {
		this.empresa = empresa;
	}
	public SyncOperation getSyncOperation (){
		if(syncOperation == null){
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}
	
	public void setSyncOperation (SyncOperation  syncOperation){
		this.syncOperation = syncOperation;
	}
	
}