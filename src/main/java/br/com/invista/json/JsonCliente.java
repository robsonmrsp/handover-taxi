package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.model.TipoCliente;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
 * generated: 16/10/2016 15:27:07
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private String nome;
	private String email;
	private String username;
	private String senha;
	private String telefone;
	private TipoCliente tipo;
	private Boolean inativo;
	private Integer usuarioCadastro;
	private String dataCadastro;
	private Integer matricula;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double limiteMensal;
	private String cnpjCpf;
	private String cnpj;
	private String cpf;
	private Boolean autorizaEticket;
	private String emailFacebook;
	private String idFacebook;
	private String observacao;
	private ArrayList<JsonEnderecoFavorito> enderecosFavoritos = new ArrayList<JsonEnderecoFavorito>();
	private ArrayList<JsonTelefoneFavorito> telefonesFavoritos = new ArrayList<JsonTelefoneFavorito>();
	private ArrayList<JsonMotoristaBloqueado> motoristaBloqueados = new ArrayList<JsonMotoristaBloqueado>();
	private JsonEmpresa empresa;
	private JsonCentroCusto centroCusto;

	public JsonCliente() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Boolean getInativo() {
		return inativo;
	}

	public void setInativo(Boolean inativo) {
		this.inativo = inativo;
	}

	public Integer getUsuarioCadastro() {
		return usuarioCadastro;
	}

	public void setUsuarioCadastro(Integer usuarioCadastro) {
		this.usuarioCadastro = usuarioCadastro;
	}

	public String getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(String dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public Double getLimiteMensal() {
		return limiteMensal;
	}

	public void setLimiteMensal(Double limiteMensal) {
		this.limiteMensal = limiteMensal;
	}

	public String getCnpjCpf() {
		return cnpjCpf;
	}

	public void setCnpjCpf(String cnpjCpf) {
		this.cnpjCpf = cnpjCpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Boolean getAutorizaEticket() {
		return autorizaEticket;
	}

	public void setAutorizaEticket(Boolean autorizaEticket) {
		this.autorizaEticket = autorizaEticket;
	}

	public String getEmailFacebook() {
		return emailFacebook;
	}

	public void setEmailFacebook(String emailFacebook) {
		this.emailFacebook = emailFacebook;
	}

	public String getIdFacebook() {
		return idFacebook;
	}

	public void setIdFacebook(String idFacebook) {
		this.idFacebook = idFacebook;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public ArrayList<JsonEnderecoFavorito> getEnderecosFavoritos() {
		return enderecosFavoritos;
	}

	public void setEnderecosFavoritos(ArrayList<JsonEnderecoFavorito> enderecoFavoritos) {
		this.enderecosFavoritos = enderecoFavoritos;
	}

	public ArrayList<JsonTelefoneFavorito> getTelefonesFavoritos() {
		return telefonesFavoritos;
	}

	public void setTelefonesFavoritos(ArrayList<JsonTelefoneFavorito> telefonesFavoritos) {
		this.telefonesFavoritos = telefonesFavoritos;
	}

	public ArrayList<JsonMotoristaBloqueado> getMotoristaBloqueados() {
		return motoristaBloqueados;
	}

	public void setMotoristaBloqueados(ArrayList<JsonMotoristaBloqueado> motoristaBloqueado) {
		this.motoristaBloqueados = motoristaBloqueado;
	}

	public JsonEmpresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(JsonEmpresa empresa) {
		this.empresa = empresa;
	}

	public JsonCentroCusto getCentroCusto() {
		return centroCusto;
	}

	public void setCentroCusto(JsonCentroCusto centroCusto) {
		this.centroCusto = centroCusto;
	}

	public SyncOperation getSyncOperation() {
		if (syncOperation == null) {
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}

	public void setSyncOperation(SyncOperation syncOperation) {
		this.syncOperation = syncOperation;
	}

	public TipoCliente getTipo() {
		return tipo;
	}

	public void setTipo(TipoCliente tipo) {
		this.tipo = tipo;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

}