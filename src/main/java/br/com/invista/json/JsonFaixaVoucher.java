package br.com.invista.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;

/**
 * generated: 16/10/2016 15:27:08
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonFaixaVoucher implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private String serie;
	private Integer numeroInicial;
	private Integer numeroFinal;
	private Boolean inativo;
	private JsonEmpresa empresa;
	private JsonMotorista motorista;

	public JsonFaixaVoucher() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Integer getNumeroInicial() {
		return numeroInicial;
	}

	public void setNumeroInicial(Integer numeroInicial) {
		this.numeroInicial = numeroInicial;
	}

	public Integer getNumeroFinal() {
		return numeroFinal;
	}

	public void setNumeroFinal(Integer numeroFinal) {
		this.numeroFinal = numeroFinal;
	}

	public Boolean getInativo() {
		return inativo;
	}

	public void setInativo(Boolean inativo) {
		this.inativo = inativo;
	}

	public JsonEmpresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(JsonEmpresa empresa) {
		this.empresa = empresa;
	}
	
	public JsonMotorista getMotorista() {
		return motorista;
	}

	public void setMotorista(JsonMotorista motorista) {
		this.motorista = motorista;
	}

	public SyncOperation getSyncOperation() {
		if (syncOperation == null) {
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}

	public void setSyncOperation(SyncOperation syncOperation) {
		this.syncOperation = syncOperation;
	}

}