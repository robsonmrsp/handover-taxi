package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
 * generated: 16/10/2016 15:27:08
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonEmpresa implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private String nomeFantasia;
	private String cnpj;
	private String contrato;
	private String razaoSocial;
	private String endereco;
	private String numeroEndereco;
	private String complemento;
	private String uf;
	private String cidade;
	private String bairro;
	private String cep;
	private String referenciaEndereco;
	private String ddd;
	private String fone;
	private Boolean utilizaVoucher;
	private Boolean utilizaEticket;
	private String observacaoTaxista;
	private String observacaoCentral;
	private String observacaoFinanceiro;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double percentualDesconto;
	private String inscricaoMunicipal;
	private String inscricaoEstadual;
	private Boolean emiteNf;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double percentualIss;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double percentualIrf;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double percentualInss;
	private Integer diaVencimento;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double percentualMotorista;
	private String banco;
	private String agencia;
	private String conta;
	private String email;
	private Integer usuarioCadastro;
	private String dataCadastro;
	private String statusEmpresa;
	private String nomeCliente1;
	private String foneCliente1;
	private String nomeCliente2;
	private String foneCliente2;
	private String nomeFornecedor1;
	private String foneFornecedor1;
	private String nomeFornecedor2;
	private String foneFornecedor2;
	private String username;
	private String senha;
	private ArrayList<JsonEnderecoFavorito> enderecosFavoritos = new ArrayList<JsonEnderecoFavorito>();
	private ArrayList<JsonCentroCusto> centroCustos = new ArrayList<JsonCentroCusto>();
	private ArrayList<JsonHdUsuario> hdUsuarios = new ArrayList<JsonHdUsuario>();
	private ArrayList<JsonContato> contatos = new ArrayList<JsonContato>();
	private ArrayList<JsonSocio> socios = new ArrayList<JsonSocio>();
	private ArrayList<JsonFaixaVoucher> faixaVouchers = new ArrayList<JsonFaixaVoucher>();
	private ArrayList<JsonCliente> clientes = new ArrayList<JsonCliente>();
	private ArrayList<JsonTelefoneFavorito> telefonesFavoritos = new ArrayList<JsonTelefoneFavorito>();


	public JsonEmpresa() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumeroEndereco() {
		return numeroEndereco;
	}

	public void setNumeroEndereco(String numeroEndereco) {
		this.numeroEndereco = numeroEndereco;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getReferenciaEndereco() {
		return referenciaEndereco;
	}

	public void setReferenciaEndereco(String referenciaEndereco) {
		this.referenciaEndereco = referenciaEndereco;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public Boolean getUtilizaVoucher() {
		return utilizaVoucher;
	}

	public void setUtilizaVoucher(Boolean utilizaVoucher) {
		this.utilizaVoucher = utilizaVoucher;
	}

	public Boolean getUtilizaEticket() {
		return utilizaEticket;
	}

	public void setUtilizaEticket(Boolean utilizaEticket) {
		this.utilizaEticket = utilizaEticket;
	}

	public String getObservacaoTaxista() {
		return observacaoTaxista;
	}

	public void setObservacaoTaxista(String observacaoTaxista) {
		this.observacaoTaxista = observacaoTaxista;
	}

	public String getObservacaoCentral() {
		return observacaoCentral;
	}

	public void setObservacaoCentral(String observacaoCentral) {
		this.observacaoCentral = observacaoCentral;
	}

	public Double getPercentualDesconto() {
		return percentualDesconto;
	}

	public void setPercentualDesconto(Double percentualDesconto) {
		this.percentualDesconto = percentualDesconto;
	}

	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public Boolean getEmiteNf() {
		return emiteNf;
	}

	public void setEmiteNf(Boolean emiteNf) {
		this.emiteNf = emiteNf;
	}

	public Double getPercentualIss() {
		return percentualIss;
	}

	public void setPercentualIss(Double percentualIss) {
		this.percentualIss = percentualIss;
	}

	public Double getPercentualIrf() {
		return percentualIrf;
	}

	public void setPercentualIrf(Double percentualIrf) {
		this.percentualIrf = percentualIrf;
	}

	public Double getPercentualInss() {
		return percentualInss;
	}

	public void setPercentualInss(Double percentualInss) {
		this.percentualInss = percentualInss;
	}

	public Integer getDiaVencimento() {
		return diaVencimento;
	}

	public void setDiaVencimento(Integer diaVencimento) {
		this.diaVencimento = diaVencimento;
	}

	public Double getPercentualMotorista() {
		return percentualMotorista;
	}

	public void setPercentualMotorista(Double percentualMotorista) {
		this.percentualMotorista = percentualMotorista;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getUsuarioCadastro() {
		return usuarioCadastro;
	}

	public void setUsuarioCadastro(Integer usuarioCadastro) {
		this.usuarioCadastro = usuarioCadastro;
	}

	public String getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(String dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getStatusEmpresa() {
		return statusEmpresa;
	}

	public void setStatusEmpresa(String statusEmpresa) {
		this.statusEmpresa = statusEmpresa;
	}

	public String getNomeCliente1() {
		return nomeCliente1;
	}

	public void setNomeCliente1(String nomeCliente1) {
		this.nomeCliente1 = nomeCliente1;
	}

	public String getFoneCliente1() {
		return foneCliente1;
	}

	public void setFoneCliente1(String foneCliente1) {
		this.foneCliente1 = foneCliente1;
	}

	public String getNomeCliente2() {
		return nomeCliente2;
	}

	public void setNomeCliente2(String nomeCliente2) {
		this.nomeCliente2 = nomeCliente2;
	}

	public String getFoneCliente2() {
		return foneCliente2;
	}

	public void setFoneCliente2(String foneCliente2) {
		this.foneCliente2 = foneCliente2;
	}

	public String getNomeFornecedor1() {
		return nomeFornecedor1;
	}

	public void setNomeFornecedor1(String nomeFornecedor1) {
		this.nomeFornecedor1 = nomeFornecedor1;
	}

	public String getFoneFornecedor1() {
		return foneFornecedor1;
	}

	public void setFoneFornecedor1(String foneFornecedor1) {
		this.foneFornecedor1 = foneFornecedor1;
	}

	public String getNomeFornecedor2() {
		return nomeFornecedor2;
	}

	public void setNomeFornecedor2(String nomeFornecedor2) {
		this.nomeFornecedor2 = nomeFornecedor2;
	}

	public String getFoneFornecedor2() {
		return foneFornecedor2;
	}

	public void setFoneFornecedor2(String foneFornecedor2) {
		this.foneFornecedor2 = foneFornecedor2;
	}

	public ArrayList<JsonCentroCusto> getCentroCustos() {
		return centroCustos;
	}

	public void setCentroCustos(ArrayList<JsonCentroCusto> centroCusto) {
		this.centroCustos = centroCusto;
	}

	public ArrayList<JsonHdUsuario> getHdUsuarios() {
		return hdUsuarios;
	}

	public void setHdUsuarios(ArrayList<JsonHdUsuario> hdUsuario) {
		this.hdUsuarios = hdUsuario;
	}

	public ArrayList<JsonContato> getContatos() {
		return contatos;
	}

	public void setContatos(ArrayList<JsonContato> contato) {
		this.contatos = contato;
	}

	public ArrayList<JsonSocio> getSocios() {
		return socios;
	}

	public void setSocios(ArrayList<JsonSocio> socio) {
		this.socios = socio;
	}

	public ArrayList<JsonFaixaVoucher> getFaixaVouchers() {
		return faixaVouchers;
	}

	public void setFaixaVouchers(ArrayList<JsonFaixaVoucher> faixaVoucher) {
		this.faixaVouchers = faixaVoucher;
	}

	public ArrayList<JsonCliente> getClientes() {
		return clientes;
	}

	public void setClientes(ArrayList<JsonCliente> cliente) {
		this.clientes = cliente;
	}

	public SyncOperation getSyncOperation() {
		if (syncOperation == null) {
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}

	public void setSyncOperation(SyncOperation syncOperation) {
		this.syncOperation = syncOperation;
	}

	public String getObservacaoFinanceiro() {
		return observacaoFinanceiro;
	}

	public void setObservacaoFinanceiro(String observacaoFinanceiro) {
		this.observacaoFinanceiro = observacaoFinanceiro;
	}

	public ArrayList<JsonEnderecoFavorito> getEnderecosFavoritos() {
		return enderecosFavoritos;
	}

	public void setEnderecosFavoritos(ArrayList<JsonEnderecoFavorito> enderecosFavoritos) {
		this.enderecosFavoritos = enderecosFavoritos;
	}

	public ArrayList<JsonTelefoneFavorito> getTelefonesFavoritos() {
		return telefonesFavoritos;
	}

	public void setTelefonesFavoritos(ArrayList<JsonTelefoneFavorito> telefonesFavoritos) {
		this.telefonesFavoritos = telefonesFavoritos;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}