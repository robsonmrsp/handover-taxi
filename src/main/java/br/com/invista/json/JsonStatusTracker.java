package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
 * generated: 04/11/2016 11:29:27
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonStatusTracker implements Serializable {
	private static final long serialVersionUID = 1L;

	private SyncOperation syncOperation;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double latitude;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double longitude;
	private Long timestamp;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double speed;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double accuracy;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double direction;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double altitude;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double bateryLevel;
	private Boolean gpsEnabled;
	private Boolean wifiEnabled;
	private Boolean mobileEnabled;
	private JsonMotorista motorista;

	public JsonStatusTracker() {

	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Double getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(Double accuracy) {
		this.accuracy = accuracy;
	}

	public Double getDirection() {
		return direction;
	}

	public void setDirection(Double direction) {
		this.direction = direction;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public Double getBateryLevel() {
		return bateryLevel;
	}

	public void setBateryLevel(Double bateryLevel) {
		this.bateryLevel = bateryLevel;
	}

	public Boolean getGpsEnabled() {
		return gpsEnabled;
	}

	public void setGpsEnabled(Boolean gpsEnabled) {
		this.gpsEnabled = gpsEnabled;
	}

	public Boolean getWifiEnabled() {
		return wifiEnabled;
	}

	public void setWifiEnabled(Boolean wifiEnabled) {
		this.wifiEnabled = wifiEnabled;
	}

	public Boolean getMobileEnabled() {
		return mobileEnabled;
	}

	public void setMobileEnabled(Boolean mobileEnabled) {
		this.mobileEnabled = mobileEnabled;
	}

	public JsonMotorista getMotorista() {
		return motorista;
	}

	public void setMotorista(JsonMotorista motorista) {
		this.motorista = motorista;
	}

	public SyncOperation getSyncOperation() {
		if (syncOperation == null) {
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}

	public void setSyncOperation(SyncOperation syncOperation) {
		this.syncOperation = syncOperation;
	}

}