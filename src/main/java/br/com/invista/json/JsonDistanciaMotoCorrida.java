package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
*  generated: 04/11/2016 11:29:26
**/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonDistanciaMotoCorrida implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double distancia;
	private Integer sequencia;
	private JsonMotorista motorista;		
	private JsonCorrida corrida;		
	
	public  JsonDistanciaMotoCorrida() {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public Double getDistancia() {
		return distancia;
	}

	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}
	public Integer getSequencia() {
		return sequencia;
	}

	public void setSequencia(Integer sequencia) {
		this.sequencia = sequencia;
	}
	
	public JsonMotorista getMotorista() {
		return motorista;
	}
	
	public void setMotorista(JsonMotorista motorista) {
		this.motorista = motorista;
	}
	public JsonCorrida getCorrida() {
		return corrida;
	}
	
	public void setCorrida(JsonCorrida corrida) {
		this.corrida = corrida;
	}
	public SyncOperation getSyncOperation (){
		if(syncOperation == null){
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}
	
	public void setSyncOperation (SyncOperation  syncOperation){
		this.syncOperation = syncOperation;
	}
	
}