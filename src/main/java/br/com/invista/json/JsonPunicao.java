package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
*  generated: 04/11/2016 11:29:27
**/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonPunicao implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private JsonMotivoPunicao motivo;
	private String dataInicio;
	private String dataFim;
	private Integer quantidadetempo;
	private String statusPunicao;
	private JsonMotorista motorista;		
	
	public  JsonPunicao() {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}				
	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}				
	public Integer getQuantidadetempo() {
		return quantidadetempo;
	}

	public void setQuantidadetempo(Integer quantidadetempo) {
		this.quantidadetempo = quantidadetempo;
	}
	public String getStatusPunicao() {
		return statusPunicao;
	}

	public void setStatusPunicao(String statusPunicao) {
		this.statusPunicao = statusPunicao;
	}
	
	public JsonMotorista getMotorista() {
		return motorista;
	}
	
	public void setMotorista(JsonMotorista motorista) {
		this.motorista = motorista;
	}
	public SyncOperation getSyncOperation (){
		if(syncOperation == null){
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}
	
	public void setSyncOperation (SyncOperation  syncOperation){
		this.syncOperation = syncOperation;
	}

	public JsonMotivoPunicao getMotivo() {
		return motivo;
	}

	public void setMotivo(JsonMotivoPunicao motivo) {
		this.motivo = motivo;
	}
	
}