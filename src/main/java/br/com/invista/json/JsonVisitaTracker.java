package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
 * generated: 04/11/2016 11:29:27
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonVisitaTracker implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double latitude;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double longitude;
	private Long timestamp;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double speed;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double accuracy;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double direction;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double altitude;
	private Boolean baterryLevel;
	private Boolean gpsEnabled;
	private Boolean wifiEnabled;
	private Boolean mobileEnabled;
	private ArrayList<JsonEvento> eventosSaida = new ArrayList<JsonEvento>();
	private ArrayList<JsonEvento> eventosChegada = new ArrayList<JsonEvento>();

	public JsonVisitaTracker() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Double getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(Double accuracy) {
		this.accuracy = accuracy;
	}

	public Double getDirection() {
		return direction;
	}

	public void setDirection(Double direction) {
		this.direction = direction;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public Boolean getBaterryLevel() {
		return baterryLevel;
	}

	public void setBaterryLevel(Boolean baterryLevel) {
		this.baterryLevel = baterryLevel;
	}

	public Boolean getGpsEnabled() {
		return gpsEnabled;
	}

	public void setGpsEnabled(Boolean gpsEnabled) {
		this.gpsEnabled = gpsEnabled;
	}

	public Boolean getWifiEnabled() {
		return wifiEnabled;
	}

	public void setWifiEnabled(Boolean wifiEnabled) {
		this.wifiEnabled = wifiEnabled;
	}

	public Boolean getMobileEnabled() {
		return mobileEnabled;
	}

	public void setMobileEnabled(Boolean mobileEnabled) {
		this.mobileEnabled = mobileEnabled;
	}

	public SyncOperation getSyncOperation() {
		if (syncOperation == null) {
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}

	public void setSyncOperation(SyncOperation syncOperation) {
		this.syncOperation = syncOperation;
	}

	public ArrayList<JsonEvento> getEventosSaida() {
		return eventosSaida;
	}

	public void setEventosSaida(ArrayList<JsonEvento> eventosSaida) {
		this.eventosSaida = eventosSaida;
	}

	public ArrayList<JsonEvento> getEventosChegada() {
		return eventosChegada;
	}

	public void setEventosChegada(ArrayList<JsonEvento> eventosChegada) {
		this.eventosChegada = eventosChegada;
	}

}