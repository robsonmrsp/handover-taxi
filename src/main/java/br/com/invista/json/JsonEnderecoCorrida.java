package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
 * generated: 04/11/2016 11:29:26
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonEnderecoCorrida implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private String endereco;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String uf;
	private String contato;
	private String tarefa;

	private String cidade;
	private String cep;
	private String referencia;
	private String descricao;
	private Integer ordem;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double latitude;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double longitude;
	private String enderecoFormatado;
	private Boolean origem;
	private ArrayList<JsonEvento> eventos = new ArrayList<JsonEvento>();
	private JsonCorrida corrida;

	public JsonEnderecoCorrida() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getEnderecoFormatado() {
		return enderecoFormatado;
	}

	public void setEnderecoFormatado(String enderecoFormatado) {
		this.enderecoFormatado = enderecoFormatado;
	}

	public Boolean getOrigem() {
		return origem;
	}

	public void setOrigem(Boolean origem) {
		this.origem = origem;
	}

	public ArrayList<JsonEvento> getEventos() {
		return eventos;
	}

	public void setEventos(ArrayList<JsonEvento> evento) {
		this.eventos = evento;
	}

	public JsonCorrida getCorrida() {
		return corrida;
	}

	public void setCorrida(JsonCorrida corrida) {
		this.corrida = corrida;
	}

	public SyncOperation getSyncOperation() {
		if (syncOperation == null) {
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}

	public void setSyncOperation(SyncOperation syncOperation) {
		this.syncOperation = syncOperation;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getTarefa() {
		return tarefa;
	}

	public void setTarefa(String tarefa) {
		this.tarefa = tarefa;
	}

}