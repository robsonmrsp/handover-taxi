package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
*  generated: 16/10/2016 15:27:08
**/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonMotoristaBloqueado implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private String dataBloqueio;
	private Integer usuarioBloqueio;
	private JsonCliente cliente;		
	private JsonEmpresa empresa;		
	private JsonMotorista motorista;		
	
	public  JsonMotoristaBloqueado() {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getDataBloqueio() {
		return dataBloqueio;
	}

	public void setDataBloqueio(String dataBloqueio) {
		this.dataBloqueio = dataBloqueio;
	}				
	public Integer getUsuarioBloqueio() {
		return usuarioBloqueio;
	}

	public void setUsuarioBloqueio(Integer usuarioBloqueio) {
		this.usuarioBloqueio = usuarioBloqueio;
	}
	
	public JsonCliente getCliente() {
		return cliente;
	}
	
	public void setCliente(JsonCliente cliente) {
		this.cliente = cliente;
	}
	public JsonMotorista getMotorista() {
		return motorista;
	}
	
	public void setMotorista(JsonMotorista motorista) {
		this.motorista = motorista;
	}
	public SyncOperation getSyncOperation (){
		if(syncOperation == null){
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}
	
	public void setSyncOperation (SyncOperation  syncOperation){
		this.syncOperation = syncOperation;
	}

	public JsonEmpresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(JsonEmpresa empresa) {
		this.empresa = empresa;
	}
	
}