package br.com.invista.json;

import java.io.Serializable;

import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.model.TipoCliente;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonUserApp implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private String username;
	private String senha;
	private String nome;
	private String email;
	private String emailFacebook;
	private String idFacebook;
	private Boolean empresa;
	private String telefone;
	TipoCliente tipo;
	private JsonEmpresa empresaJson;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmailFacebook() {
		return emailFacebook;
	}
	public void setEmailFacebook(String emailFacebook) {
		this.emailFacebook = emailFacebook;
	}
	public String getIdFacebook() {
		return idFacebook;
	}
	public void setIdFacebook(String idFacebook) {
		this.idFacebook = idFacebook;
	}
	public Boolean getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Boolean empresa) {
		this.empresa = empresa;
	}
	public TipoCliente getTipo() {
		return tipo;
	}
	public void setTipo(TipoCliente tipo) {
		this.tipo = tipo;
	}
	public JsonEmpresa getEmpresaJson() {
		return empresaJson;
	}
	public void setEmpresaJson(JsonEmpresa empresaJson) {
		this.empresaJson = empresaJson;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
}
