package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
*  generated: 16/10/2016 15:27:08
**/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonContato implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private String nome;
	private String ddd;
	private String fone;
	private String setor;
	private JsonEmpresa empresa;		
	
	public  JsonContato() {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}
	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}
	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}
	
	public JsonEmpresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(JsonEmpresa empresa) {
		this.empresa = empresa;
	}
	public SyncOperation getSyncOperation (){
		if(syncOperation == null){
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}
	
	public void setSyncOperation (SyncOperation  syncOperation){
		this.syncOperation = syncOperation;
	}
	
}