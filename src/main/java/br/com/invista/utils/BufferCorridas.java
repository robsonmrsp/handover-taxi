package br.com.invista.utils;

import java.util.TreeMap;


/**
 * Created by Israel on 12/01/2017.
 */
public class BufferCorridas {
	    private TreeMap<Integer, CorridaEntityForBuffer> mapSt;
	    private int maxSize = 1000;
	    private static BufferCorridas sig = null;

	    private BufferCorridas(){
	        mapSt = new TreeMap<Integer, CorridaEntityForBuffer>();
	    }

	    public static BufferCorridas getInstance(){
	        if (sig == null){
	            sig = new BufferCorridas();
	        }
	        return sig;
	    }

	    public void addCorridaEntity(CorridaEntityForBuffer status){
	        if (!mapSt.containsKey(status.getId())) {
	            if (mapSt.size() == maxSize) {
	                for (int i = 0; i < maxSize / 2; i++) {
	                    mapSt.remove(mapSt.firstKey());
	                }
	            }
	            mapSt.put(status.getId(), status);
	        }else{
	        }
	    }


	    public void remCorridaEntity(CorridaEntityForBuffer status){
	            mapSt.remove(status.getId());
	    }

	    public void clearBuffer(){
	        //ORMDBHelper.getHelper(context.getApplicationContext()).truncateStatusTracker();
	        for (int i=0; i < mapSt.size(); i++) {
	            mapSt.remove(mapSt.firstKey());
	        }
	    }

	    public int getSize(){
	        return mapSt.size();
	    }


	    public TreeMap<Integer, CorridaEntityForBuffer> getSetBuffer(){
	        return mapSt;
	    }
}

