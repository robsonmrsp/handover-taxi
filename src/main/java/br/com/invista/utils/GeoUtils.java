package br.com.invista.utils;

import org.apache.log4j.Logger;

import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.geotools.referencing.GeodeticCalculator;
import org.opengis.referencing.crs.CRSAuthorityFactory;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;

import com.vividsolutions.jts.geom.Coordinate;

public class GeoUtils {
	private static final Logger LOGGER = Logger.getLogger(GeoUtils.class);

	static CRSAuthorityFactory factory = CRS.getAuthorityFactory(true);
	static CoordinateReferenceSystem crs = null;
	static {
		try {
			crs = CRS.decode("EPSG:4326");
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	public static Double getDistancia(Coordinate origem, Coordinate destino) {
		double distance = 0.0;
		try {

			GeodeticCalculator gc = new GeodeticCalculator(crs);
			gc.setStartingPosition(JTS.toDirectPosition(origem, crs));
			gc.setDestinationPosition(JTS.toDirectPosition(destino, crs));
			distance = gc.getOrthodromicDistance();
		} catch (TransformException e) {
			LOGGER.error(e);
		}

		return distance;
	}
}
