package br.com.invista.utils;

import br.com.invista.model.Corrida;

public class CorridaEntityForBuffer {
	private Integer id;
	
	public CorridaEntityForBuffer(Integer id){
		this.id = id;
	}

	public CorridaEntityForBuffer(Corrida corrida){
		this.id = corrida.getId();
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}
