-- ALTER TABLE user_role ADD PRIMARY KEY (role_id, user_id);

-- Tabela de usuários
 
-- INSERT INTO app_user(   enable, image, name, password, username) VALUES ( 1, '', 'mr', '$2a$10$teJrCEnsxNT49ZpXU7n22O27aCGbVYYe/RG6/XxdWPJbOLZubLIi2', 'mr');
-- INSERT INTO app_user(   enable, image, name, password, username) VALUES ( 1, '', 'mr2', '$2a$10$teJrCEnsxNT49ZpXU7n22O27aCGbVYYe/RG6/XxdWPJbOLZubLIi2', 'mr2');
-- INSERT INTO app_user(   enable, image, name, password, username) VALUES ( 1, '', 'clienteapp', '$2a$10$iBrWcNLn4sG7d3xcSKRWMOmAEplwlikNGqLe1gXXnvSOkQH8giv5e', 'clienteapp');
 
-- INSERT INTO role_user(role_id, user_id) values (2, 1);
-- INSERT INTO user_role(role_id, user_id) values (1, 1);
-- INSERT INTO user_role(role_id, user_id) values (2, 2);
-- INSERT INTO user_role(role_id, user_id) values (1, 3);

-- INSERT INTO role(  authority, description)  VALUES ( 'ROLE_USER', 'Usuário do sistema');
-- INSERT INTO role(  authority, description)  VALUES ( 'ROLE_ADMIN', 'Administrador do sistema');
-- INSERT INTO role(  authority, description)  VALUES ( 'ROLE_GESTOR', 'Gestor');
-- INSERT INTO role(  authority, description)  VALUES ( 'ROLE_OPERADOR', 'Operador');
-- INSERT INTO role(  authority, description)  VALUES ( 'ROLE_MOTOQUEIRO', 'Motoqueiro');

-- INSERT INTO role(  authority, description)  VALUES ( 'ROLE_USER', 'Usuário do sistema');
-- INSERT INTO role(  authority, description)  VALUES ( 'ROLE_ADMIN', 'Administrador do sistema');
 
-- INSERT INTO [dbo].[role]("create_datetime","last_update_datetime","user_change","user_create","authority","description")
-- VALUES
-- (NULL,NULL,NULL,NULL,'ROLE_USER','Usuário do sistema'),
-- (NULL,NULL,NULL,NULL,'ROLE_ADMIN','Administrador do sistema'),

-- (NULL,NULL,NULL,NULL,'ROLE_EMPRESA','Permite ver empresas'),
-- (NULL,NULL,NULL,NULL,'ROLE_EMPRESA_CRIAR','Permite criar empresa'),
-- (NULL,NULL,NULL,NULL,'ROLE_EMPRESA_EDITAR','Permite editar empresa'),
-- (NULL,NULL,NULL,NULL,'ROLE_EMPRESA_DELETAR','Permite deletar empresa')

-- (NULL,NULL,NULL,NULL,'ROLE_MOTOQUEIRO','Permite ver motoqueiros'),
-- (NULL,NULL,NULL,NULL,'ROLE_MOTOQUEIRO_CRIAR','Permite criar motoqueiro'),
-- (NULL,NULL,NULL,NULL,'ROLE_MOTOQUEIRO_EDITAR','Permite editar  motoqueiro'),
-- (NULL,NULL,NULL,NULL,'ROLE_MOTOQUEIRO_DELETAR','Permite deletar  motoqueiro')

-- (NULL,NULL,NULL,NULL,'ROLE_CLIENTE','Permite ver clientes'),
-- (NULL,NULL,NULL,NULL,'ROLE_CLIENTE_CRIAR','Permite criar cliente'),
-- (NULL,NULL,NULL,NULL,'ROLE_CLIENTE_EDITAR','Permite editar cliente'),
-- (NULL,NULL,NULL,NULL,'ROLE_CLIENTE_DELETAR','Permite deletar cliente')

-- (NULL,NULL,NULL,NULL,'ROLE_TARIFAADICIONAL','Permite ver tarifas adicionais'),
-- (NULL,NULL,NULL,NULL,'ROLE_TARIFAADICIONAL_CRIAR','Permite criar tarifa adicional'),
-- (NULL,NULL,NULL,NULL,'ROLE_TARIFAADICIONAL_EDITAR','Permite editar tarifa adicional'),
-- (NULL,NULL,NULL,NULL,'ROLE_TARIFAADICIONAL_DELETAR','Permite deletar tarifa adicional')

-- (NULL,NULL,NULL,NULL,'ROLE_TARIFA','Permite ver tarifas'),
-- (NULL,NULL,NULL,NULL,'ROLE_TARIFA_CRIAR','Permite criar tarifa'),
-- (NULL,NULL,NULL,NULL,'ROLE_TARIFA_EDITAR','Permite editar  tarifa'),
-- (NULL,NULL,NULL,NULL,'ROLE_TARIFA_DELETAR','Permite deletar  tarifa')

-- (NULL,NULL,NULL,NULL,'ROLE_MOTIVOPUNICAO','Permite ver punições'),
-- (NULL,NULL,NULL,NULL,'ROLE_MOTIVOPUNICAO_CRIAR','Permite criar punição'),
-- (NULL,NULL,NULL,NULL,'ROLE_MOTIVOPUNICAO_EDITAR','Permite editar punição'),
-- (NULL,NULL,NULL,NULL,'ROLE_MOTIVOPUNICAO_DELETAR','Permite deletar punição')

-- (NULL,NULL,NULL,NULL,'ROLE_CENTROCUSTO','Permite ver centro de custo'),
-- (NULL,NULL,NULL,NULL,'ROLE_CENTROCUSTO_CRIAR','Permite criar centro de custo'),
-- (NULL,NULL,NULL,NULL,'ROLE_CENTROCUSTO_EDITAR','Permite editar centro de custo'),
-- (NULL,NULL,NULL,NULL,'ROLE_CENTROCUSTO_DELETAR','Permite deletar centro de custo')

-- (NULL,NULL,NULL,NULL,'ROLE_FUNCIONARIO','Permite ver funcionários'),
-- (NULL,NULL,NULL,NULL,'ROLE_FUNCIONARIO_CRIAR','Permite criar funcionário'),
-- (NULL,NULL,NULL,NULL,'ROLE_FUNCIONARIO_EDITAR','Permite editar funcionário'),
-- (NULL,NULL,NULL,NULL,'ROLE_FUNCIONARIO_DELETAR','Permite deletar funcionário')

-- (NULL,NULL,NULL,NULL,'ROLE_CONTATO','Permite ver contatos'),
-- (NULL,NULL,NULL,NULL,'ROLE_CONTATO_CRIAR','Permite criar contato'),
-- (NULL,NULL,NULL,NULL,'ROLE_CONTATO_EDITAR','Permite editar contato'),
-- (NULL,NULL,NULL,NULL,'ROLE_CONTATO_DELETAR','Permite deletar contato')

-- (NULL,NULL,NULL,NULL,'ROLE_MOTOQUEIROBLOQUEADO','Permite ver motoqueiros bloqueados'),
-- (NULL,NULL,NULL,NULL,'ROLE_MOTOQUEIROBLOQUEADO_CRIAR','Permite criar motoqueiro bloqueado'),
-- (NULL,NULL,NULL,NULL,'ROLE_MOTOQUEIROBLOQUEADO_EDITAR','Permite editar motoqueiro bloqueado'),
-- (NULL,NULL,NULL,NULL,'ROLE_MOTOQUEIROBLOQUEADO_DELETAR','Permite deletar motoqueiro bloqueado')

-- (NULL,NULL,NULL,NULL,'ROLE_SOCIO','Permite ver sócios'),
-- (NULL,NULL,NULL,NULL,'ROLE_SOCIO_CRIAR','Permite criar sócio'),
-- (NULL,NULL,NULL,NULL,'ROLE_SOCIO_EDITAR','Permite editar sócio'),
-- (NULL,NULL,NULL,NULL,'ROLE_SOCIO_DELETAR','Permite deletar sócio')

-- (NULL,NULL,NULL,NULL,'ROLE_VOUCHER','Permite ver vouchers de empresa'),
-- (NULL,NULL,NULL,NULL,'ROLE_VOUCHER_CRIAR','Permite criar voucher de empresa'),
-- (NULL,NULL,NULL,NULL,'ROLE_VOUCHER_EDITAR','Permite editar voucher de empresa'),
-- (NULL,NULL,NULL,NULL,'ROLE_VOUCHER_DELETAR','Permite deletar voucher de empresa')

-- (NULL,NULL,NULL,NULL,'ROLE_TELEFONE','Permite ver telefones de cliente'),
-- (NULL,NULL,NULL,NULL,'ROLE_TELEFONE_CRIAR','Permite criar telefone de cliente'),
-- (NULL,NULL,NULL,NULL,'ROLE_TELEFONE_EDITAR','Permite editar telefone de cliente'),
-- (NULL,NULL,NULL,NULL,'ROLE_TELEFONE_DELETAR','Permite deletar telefone de cliente')

-- (NULL,NULL,NULL,NULL,'ROLE_ENDERECO','Permite ver endereços de cliente'),
-- (NULL,NULL,NULL,NULL,'ROLE_ENDERECO_CRIAR','Permite criar endereço de cliente'),
-- (NULL,NULL,NULL,NULL,'ROLE_ENDERECO_EDITAR','Permite editar endereço de cliente'),
-- (NULL,NULL,NULL,NULL,'ROLE_ENDERECO_DELETAR','Permite deletar endereço de cliente')

-- (NULL,NULL,NULL,NULL,'ROLE_BLOQUEIOS','Permite ver bloqueios de motoqueiros'),
-- (NULL,NULL,NULL,NULL,'ROLE_BLOQUEIOS_CRIAR','Permite criar bloqueio de motoqueiro'),
-- (NULL,NULL,NULL,NULL,'ROLE_BLOQUEIOS_EDITAR','Permite editar bloqueio de motoqueiro'),
-- (NULL,NULL,NULL,NULL,'ROLE_BLOQUEIOS_DELETAR','Permite deletar bloqueio de motoqueiro')

-- (NULL,NULL,NULL,NULL,'ROLE_PUNICOES','Permite ver punições de motoqueiros'),
-- (NULL,NULL,NULL,NULL,'ROLE_PUNICOES_CRIAR','Permite criar punição'),
-- (NULL,NULL,NULL,NULL,'ROLE_PUNICOES_EDITAR','Permite editar punição'),
-- (NULL,NULL,NULL,NULL,'ROLE_PUNICOES_DELETAR','Permite deletar punição')

-- (NULL,NULL,NULL,NULL,'ROLE_MOTOQUEIROVOUCHER','Permite ver vouchers de motoqueiros'),
-- (NULL,NULL,NULL,NULL,'ROLE_MOTOQUEIROVOUCHER_CRIAR','Permite criar voucher de motoqueiro'),
-- (NULL,NULL,NULL,NULL,'ROLE_MOTOQUEIROVOUCHER_EDITAR','Permite editar voucher de motoqueiro'),
-- (NULL,NULL,NULL,NULL,'ROLE_MOTOQUEIROVOUCHER_DELETAR','Permite deletar voucher de motoqueiro')

-- (NULL,NULL,NULL,NULL,'ROLE_CORRIDA','Permite ver corridas'),
-- (NULL,NULL,NULL,NULL,'ROLE_CORRIDA_CRIAR','Permite criar corrida'),
-- (NULL,NULL,NULL,NULL,'ROLE_CORRIDA_EDITAR','Permite editar corrida'),
-- (NULL,NULL,NULL,NULL,'ROLE_CORRIDA_DELETAR','Permite deletar corrida')

-- (NULL,NULL,NULL,NULL,'ROLE_MENSAGEM','Permite ver mensagens'),
-- (NULL,NULL,NULL,NULL,'ROLE_MENSAGEM_CRIAR','Permite criar mensagem'),
-- (NULL,NULL,NULL,NULL,'ROLE_MENSAGEM_EDITAR','Permite editar mensagem'),
-- (NULL,NULL,NULL,NULL,'ROLE_MENSAGEM_DELETAR','Permite deletar mensagem') 
 
-- =============================================
-- Author:		<Edilson Leitao>
-- Create date: <17/01/2017>
-- Description:	<Controle de de limite mensal do funcionario para corrida>
-- =============================================
CREATE TRIGGER [dbo].[auto_create_nc]
	ON [dbo].[CORRIDA]
	AFTER INSERT
AS
BEGIN

INSERT INTO dbo.conta_corrente (id_empresa, id_centrocusto, valor, data_corrida, id_cliente)
SELECT inserted.id_empresa, cliente.id_centrocusto, valor, data_corrida, id_cliente 
FROM inserted JOIN cliente ON (inserted.id_cliente = cliente.id)
WHERE inserted.TIPO_PAGAMENTO = 4 
ORDER BY inserted.id DESC
END 
GO
-- =============================================
-- Author:		<Diego Vitoriano>
-- Create date: <17/01/2017>
-- Description:	<Creates cc when a empresa is created>
-- =============================================
CREATE TRIGGER [dbo].[auto_create_cc] 
	ON [dbo].[EMPRESA]
	AFTER INSERT 
AS
BEGIN
INSERT INTO centro_custo
		(create_datetime, descricao, inativo, observacao, valor_ainda_disponivel, valor_limite, id_empresa)
	VALUES 
		(getdate(), CONCAT((SELECT NOME_FANTASIA FROM inserted), ' - ADMINISTRATIVO'), 0, 'Criado pelo sistema.', 0, 0, (SELECT ID FROM inserted));
END
GO
-- =============================================
-- Author:		<Diego Vitoriano>
-- Create date: <17/01/2017>
-- Description:	<Creates cl when a centro_custo is created>
-- =============================================
CREATE TRIGGER [dbo].[auto_create_cl]
	ON [dbo].[centro_custo]
	AFTER INSERT 
AS
DECLARE @obs varchar(20) = (SELECT observacao FROM inserted)
DECLARE @descricao varchar(200) = (SELECT descricao FROM inserted)
IF @obs <> 'Criado pelo sistema.'
BEGIN
	SET @descricao = CONCAT((SELECT nome_fantasia FROM empresa WHERE id=(SELECT id_empresa FROM inserted)), ' - ', @descricao)
END
BEGIN
INSERT INTO cliente
		(create_datetime, inativo, limite_mensal, nome, observacao, tipo, id_centrocusto, id_empresa)
		SELECT getdate(), 0, 0, @descricao, 'Criado pelo sistema.', 'FUNCIONARIO',inserted.id, inserted.id_empresa FROM inserted
END
GO

-- =============================================
-- Author:		<Edilson Leitao>
-- Create date: <23/01/2017>
-- Description:	<Adiciona telefone ao cliente criado pelo sistema>
-- =============================================
CREATE TRIGGER [dbo].[auto_create_tl] 
   ON  [dbo].[CLIENTE]
   AFTER INSERT
AS 
DECLARE @obs varchar(20) = (SELECT observacao FROM inserted);
DECLARE @fone varchar(15) = (SELECT e.fone FROM inserted AS i LEFT JOIN empresa AS e ON (i.id_empresa = e.id));
IF @obs = 'Criado pelo sistema.'
BEGIN
	IF @fone is not null
	BEGIN
		IF NOT EXISTS (SELECT fone FROM TELEFONE_FAVORITO WHERE fone = @fone)
		BEGIN
			INSERT INTO TELEFONE_FAVORITO (FONE, ID_CLIENTE)
			VALUES(@fone, (select id from inserted))
		END
	END
END
ELSE IF (NOT EXISTS (SELECT fone FROM TELEFONE_FAVORITO WHERE fone = (SELECT telefone FROM inserted)) AND (@obs <> 'APP'))
BEGIN
	INSERT INTO TELEFONE_FAVORITO (FONE, ID_CLIENTE)
	SELECT telefone, id FROM inserted
END
GO

-- =============================================
-- Author:		<Israel Santos>
-- Create date: <17/01/2017>
-- Description:	<Coisas inerentes ao Handover>
-- =============================================
 CREATE FUNCTION [dbo].[datetime2unixtime]
( 
    @dt DATETIME 
) 
RETURNS BIGINT 
AS 
BEGIN 
    DECLARE @diff BIGINT 
    IF @dt >= '20380119' 
    BEGIN 
        SET @diff = CONVERT(BIGINT, DATEDIFF(S, '19700101', '20380119')) 
            + CONVERT(BIGINT, DATEDIFF(S, '20380119', @dt)) 
    END 
    ELSE 
        SET @diff = DATEDIFF(S, '19700101', @dt)
    RETURN @diff * 1000
END
go
CREATE function [dbo].[unixtime2datetime](@unixtime bigint)
returns datetime
as
begin
return dateadd(hh, -2, (select dateadd(S, @unixtime / 1000, '1970-01-01')))
end
go
create VIEW [dbo].[view_get_eventos]
AS
SELECT 	 c.id										   AS corrida,
		 m.imei										   AS imei,
		 -e.[id]                                       AS [id],
		 1											   AS [evento],
         ec.ordem									   AS [ordemPlanejada],
         ec.endereco_formatado						   AS [nome],
		 NULL								     	   AS [horaEntrega],
		 e.descricao_curta							   AS [shortDs],
         ec.endereco_formatado + ', ' + ec.complemento AS [endereco],
		 ec.descricao 								   AS [descricao],
		 e.data_inicio								   AS [dtinicio],
		 e.data_fim  								   AS [dtfim],
		 1											   AS [tipoevento],
		 ec.contato									   AS [contato],
		 ec.tarefa									   AS [tarefa],
         vt1.latitude                                  AS [chegada.point.latitude],
         vt1.longitude                                 AS [chegada.point.longitude],
         vt1.timestamp                                 AS [chegada.timestamp],
         vt1.speed                                     AS [chegada.speed],
         vt1.accuracy                                  AS [chegada.accuracy],
         vt1.direction                                 AS [chegada.direction],
         vt1.altitude                                  AS [chegada.altitude],
         vt1.baterry_level                             AS [chegada.batteryLevel],
         vt1.gps_enabled                               AS [chegada.gpsEnabled],
         vt1.wifi_enabled                              AS [chegada.wifiEnabled],
         vt1.mobile_enabled                            AS [chegada.mobileEnabled],
         vt2.latitude                                  AS [saida.point.latitude],
         vt2.longitude                                 AS [saida.point.longitude],
         vt2.timestamp                                 AS [saida.timestamp],
         vt2.speed                                     AS [saida.speed],
         vt2.accuracy                                  AS [saida.accuracy],
         vt2.direction                                 AS [saida.direction],
         vt2.altitude                                  AS [saida.altitude],
         vt2.baterry_level                             AS [saida.batteryLevel],
         vt2.gps_enabled                               AS [saida.gpsEnabled],
         vt2.wifi_enabled                              AS [saida.wifiEnabled],
         vt2.mobile_enabled                            AS [saida.mobileEnabled],
         ec.latitude								   AS [coordenada.latitude],
         ec.longitude								   AS [coordenada.longitude],
         NULL                                          AS [documentos.id]
from evento e
		INNER JOIN endereco_corrida ec
				ON ( e.id_enderecocorrida = ec.id )
		INNER JOIN corrida c
				ON ( c.id = ec.id_corrida )
		INNER JOIN motorista m
				ON ( c.id_motorista = m.id )
		LEFT JOIN visita_tracker vt1
                ON ( e.id_chegada = vt1.id )
         LEFT JOIN visita_tracker vt2
                ON ( e.id_saida = vt2.id )
	/*where dbo.unixtime2datetime(e.data_inicio) >= cast(Getdate() as date)
		  AND dbo.unixtime2datetime(e.data_fim) <= cast(Dateadd(mi, 1, Dateadd(dd, 1, Getdate())) as date)*/
go

create TRIGGER [dbo].[ADD_CORRIDA_evento] ON [dbo].[ENDERECO_CORRIDA] AFTER INSERT AS
BEGIN
	DECLARE
	@DESCRICAO VARCHAR(MAX),
	@ID_ENDERECOCORRIDA INT,
	@ID_CORRIDA INT

	SELECT @ID_ENDERECOCORRIDA = id, @ID_CORRIDA = ID_CORRIDA, @DESCRICAO = DESCRICAO  FROM INSERTED
	
	INSERT INTO evento(descricao, data_inicio, DATA_FIM, ID_ENDERECOCORRIDA ) VALUES
	(@DESCRICAO, dbo.datetime2unixtime(GETDATE()), dbo.datetime2unixtime(GETDATE()), @ID_ENDERECOCORRIDA)
	
END
go
create PROCEDURE [dbo].[upsertEventoSaida] (@id_evento INT,
                                             @imei            VARCHAR(20),
                                             @latitude        FLOAT,
                                             @longitude       FLOAT,
                                             @timestamp       BIGINT,
                                             @speed           FLOAT,
                                             @accuracy        FLOAT,
                                             @direction       FLOAT,
                                             @altitude        FLOAT,
                                             @battery_level   FLOAT,
                                             @gps_enabled     BIT,
                                             @wifi_enabled    BIT,
                                             @mobile_enabled  BIT)
AS
	declare @vt1 table (id int)

      INSERT visita_tracker ( latitude, longitude, timestamp, speed, accuracy, direction, altitude, BATERRY_LEVEL, gps_enabled, WIFI_ENABLED, MOBILE_ENABLED)
	  output inserted.id
	  into @vt1
      SELECT top 1 
               @latitude,
               @longitude,
               @timestamp,
               @speed,
               @accuracy,
               @direction,
               @altitude,
               @battery_level,
               @gps_enabled,
               @wifi_enabled,
               @mobile_enabled
		FROM motorista ra
				where imei = @imei

    UPDATE evento
    SET    id_saida = (SELECT *
                                FROM   @vt1)
    WHERE  id = @id_evento

	/****** Object:  View [dbo].[view_get_eventos]    Script Date: 18/04/2016 12:54:13 ******/
go
create PROCEDURE [dbo].[upsertEventoChegada] (@id_evento INT,
                                             @imei            VARCHAR(20),
                                             @latitude        FLOAT,
                                             @longitude       FLOAT,
                                             @timestamp       BIGINT,
                                             @speed           FLOAT,
                                             @accuracy        FLOAT,
                                             @direction       FLOAT,
                                             @altitude        FLOAT,
                                             @battery_level   FLOAT,
                                             @gps_enabled     BIT,
                                             @wifi_enabled    BIT,
                                             @mobile_enabled  BIT)
AS
	declare @vt1 table (id int)

      INSERT visita_tracker ( latitude, longitude, timestamp, speed, accuracy, direction, altitude, BATERRY_LEVEL, gps_enabled, WIFI_ENABLED, MOBILE_ENABLED)
	  output inserted.id
	  into @vt1
      SELECT top 1
               @latitude,
               @longitude,
               @timestamp,
               @speed,
               @accuracy,
               @direction,
               @altitude,
               @battery_level,
               @gps_enabled,
               @wifi_enabled,
               @mobile_enabled
		FROM motorista ra
				where imei = @imei

    UPDATE evento
    SET    id_chegada = (SELECT *
                                FROM   @vt1)
    WHERE  id = @id_evento
    
GO

create function [dbo].[func_get_corrida_disp] (@imei varchar)
RETURNS TABLE
as
return
(
select c.id as id, 
			   c.DATA_CORRIDA as dataCorrida, 
			   c.TIPO_PAGAMENTO as tipoPagamento, 
			   c.VALOR as valor, 
			   c.OBSERVACAO as observacao, 
			   c.STATUS_CORRIDA as statusCorrida, 
			   c.ID_MOTORISTA as idMotorista, 
			   c.LAST_UPDATE_DATETIME as criacaoCorrida, 
			   ec.ENDERECO_FORMATADO as enderecoOrigem, 
			   ec.LATITUDE as latitude, 
			   ec.LONGITUDE as longitude, 
			   CONCAT(ec.LOGRADOURO,', ', ec.BAIRRO) as enderecoOrigemResumido
				 from corrida c 
					inner join ENDERECO_CORRIDA ec 
				 	on (c.id = ec.id_corrida) 
				where ( c.STATUS_CORRIDA = 'D' )
EXCEPT
select c.id as id, 
			   c.DATA_CORRIDA as dataCorrida, 
			   c.TIPO_PAGAMENTO as tipoPagamento, 
			   c.VALOR as valor, 
			   c.OBSERVACAO as observacao, 
			   c.STATUS_CORRIDA as statusCorrida, 
			   c.ID_MOTORISTA as idMotorista, 
			   c.LAST_UPDATE_DATETIME as criacaoCorrida, 
			   ec.ENDERECO_FORMATADO as enderecoOrigem, 
			   ec.LATITUDE as latitude, 
			   ec.LONGITUDE as longitude, 
			   CONCAT(ec.LOGRADOURO,', ', ec.BAIRRO) as enderecoOrigemResumido
			   from corrida c 
					inner join ENDERECO_CORRIDA ec 
				 	on (c.id = ec.id_corrida) 
				 	inner join cliente cl 
					on (c.ID_CLIENTE = cl.id) 
					inner join motorista_bloqueado mb
					on (mb.id_cliente = cl.id)
					inner join motorista m
					on (m.id = mb.id_motorista)
				where ( c.STATUS_CORRIDA = 'D' ) AND ( m.imei = @imei )
);

GO